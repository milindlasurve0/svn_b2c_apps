﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Device.Location;
using Microsoft.Phone.Maps.Services;
using Windows.Devices.Geolocation;
using Microsoft.Phone.Maps.Controls;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Microsoft.Phone.Maps.Toolkit;

namespace Pay1B2C.Views
    {
    public partial class ShopRoute: PhoneApplicationPage
        {List<GeoCoordinate> MyCoordinates = new List<GeoCoordinate>();
        RouteQuery MyQuery = null;
        String shopName;
        ProgressBarManager ProgressBarManager;
        GeocodeQuery Mygeocodequery = null;
        public ShopRoute()
            {
            InitializeComponent();

            ProgressIndicator prog = new ProgressIndicator();
            ProgressBarManager = new ProgressBarManager();
            SystemTray.SetProgressIndicator(this, prog);
            ProgressBarManager.Hook(prog);
            ProgressBarManager.Show(55, "Please wait...", false, 10);
            MyMap.Center=new GeoCoordinate(Constant.LoadPersistent<double>(Constant.SETTINGS_USER_LATTITUDE_DOUBLE), Constant.LoadPersistent<double>(Constant.SETTINGS_USER_LONGITUDE_DOUBLE));
            }

        protected override void OnNavigatedTo(NavigationEventArgs e)
            {
            if(NavigationContext.QueryString.ContainsKey("address"))
                {
                string address = NavigationContext.QueryString["address"];
                if(string.IsNullOrEmpty(address))
                    {
                    }
                else
                    {
                    shopName=NavigationContext.QueryString["shopName"];
                    String dealLocationData=PhoneApplicationService.Current.State["LocationData"].ToString();
                    try
                        {
                        JObject jObj=(JObject)JsonConvert.DeserializeObject(dealLocationData);
                      //  shopName=jObj[""].ToString();
                        }
                    catch(JsonException je)
                        {
                        }
                    catch(Exception ee)
                        {
                        }
                    if(MyMap.Layers.Count()>=1)
                        {
                        MyMap.Layers.Clear();
                        }
                    this.GetCoordinates(address);
                    }
                }
            }
 
        private void myMapControl_Loaded(object sender, RoutedEventArgs e)
            {
            Microsoft.Phone.Maps.MapsSettings.ApplicationContext.ApplicationId = "730762d9-a6df-4bcf-8458-c9bc8873dd56";
            Microsoft.Phone.Maps.MapsSettings.ApplicationContext.AuthenticationToken = "v6G8RFopHc2NbpS0p6rWIQ";
            }

        private async void GetCoordinates(String address)
            {
            Geolocator MyGeolocator = new Geolocator();
            MyGeolocator.DesiredAccuracyInMeters = 5;
            Geoposition MyGeoPosition = null;
            try
                {
                MyGeoPosition = await MyGeolocator.GetGeopositionAsync(TimeSpan.FromMinutes(1), TimeSpan.FromSeconds(10));
                MyCoordinates.Add(new GeoCoordinate(Constant.LoadPersistent<double>(Constant.SETTINGS_USER_LATTITUDE_DOUBLE), Constant.LoadPersistent<double>(Constant.SETTINGS_USER_LONGITUDE_DOUBLE)));
               
                Mygeocodequery = new GeocodeQuery();
                Mygeocodequery.SearchTerm = address;
                Mygeocodequery.GeoCoordinate = new GeoCoordinate(MyGeoPosition.Coordinate.Latitude, MyGeoPosition.Coordinate.Longitude);

                Mygeocodequery.QueryCompleted += Mygeocodequery_QueryCompleted;
                Mygeocodequery.QueryAsync();

                }
            catch(UnauthorizedAccessException)
                {
                MessageBox.Show("Location is disabled in phone settings or capabilities are not checked.");
                }
            catch(Exception ex)
                {
                // Something else happened while acquiring the location.
                MessageBox.Show(ex.Message);
                }
            }

        void Mygeocodequery_QueryCompleted(object sender, QueryCompletedEventArgs<IList<MapLocation>> e)
            {
            try
                {
                if(e.Error == null)
                    {
                    Deployment.Current.Dispatcher.BeginInvoke(() =>
                    {
                        MyQuery = new RouteQuery();
                        MyCoordinates.Add(e.Result[0].GeoCoordinate);
                        MyQuery.Waypoints = MyCoordinates;
                        MyQuery.QueryCompleted += MyQuery_QueryCompleted;
                        MyQuery.QueryAsync();

                        Mygeocodequery.Dispose();
                    });
                    }
                }
            catch(Exception exc)
                {
                ProgressBarManager.Hide(55);
                }
            }


        void MyQuery_QueryCompleted(object sender, QueryCompletedEventArgs<Route> e)
            {
          //  if(e.Error == null)
            try
                {
                Route MyRoute = e.Result;
                MapRoute MyMapRoute = new MapRoute(MyRoute);
                MyMap.AddRoute(MyMapRoute);
                MyMap.ZoomLevel=15;
                List<string> RouteList = new List<string>();
                foreach(RouteLeg leg in MyRoute.Legs)
                    {
                    foreach(RouteManeuver maneuver in leg.Maneuvers)
                        {
                        RouteList.Add(maneuver.InstructionText);
                        }
                    }

                //RouteLLS.ItemsSource = RouteList;
               // addPushPin(MyCoordinates.First().Latitude, MyCoordinates.First().Longitude, "/Images/ic_home_locate.png");
                addPushPin(MyCoordinates.Last().Latitude, MyCoordinates.Last().Longitude, "/Images/ic_gift_pin.png");
                ProgressBarManager.Hide(55);
                MyMap.Center=new GeoCoordinate(Constant.LoadPersistent<double>(Constant.SETTINGS_USER_LATTITUDE_DOUBLE), Constant.LoadPersistent<double>(Constant.SETTINGS_USER_LONGITUDE_DOUBLE));
                MyQuery.Dispose();
                }
            catch(Exception ee)
                {
                }
            }

        public void addPushPin(double lattitude, double longitude, String uri)
            {

          /*  Uri uri = new Uri("/Images/ic_gift_pin.png", UriKind.RelativeOrAbsolute);
            BitmapImage imgSource = new BitmapImage(uri);

            MapOverlay overlay = new MapOverlay
            {
                GeoCoordinate = new GeoCoordinate(lattitude, longitude),
                Content = new Image
                {

                    Source=imgSource
                    // this.image.Source = imgSource
                }
            };
            MapLayer layer = new MapLayer();
            layer.Add(overlay);

            MyMap.Center=new GeoCoordinate(lattitude, longitude);
            MyMap.Layers.Add(layer);*/

          

            Image img=new Image{ Height=55,Width=55,Source=new BitmapImage( new Uri(uri,UriKind.RelativeOrAbsolute))};
          
            MapOverlay overlay = new MapOverlay()
            {
                PositionOrigin = new Point(0.5, 0.5),
                GeoCoordinate = new GeoCoordinate(lattitude, longitude),

                Content =img
            };
           
            MapLayer ml = new MapLayer { overlay };
           
           MyMap.Layers.Add(ml);
            
            }


        }
    }   