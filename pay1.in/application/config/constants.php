<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ','rb');
define('FOPEN_READ_WRITE','r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE','wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE','w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE','ab');
define('FOPEN_READ_WRITE_CREATE','a+b');
define('FOPEN_WRITE_CREATE_STRICT','xb');
define('FOPEN_READ_WRITE_CREATE_STRICT','x+b');

define("PANEL_URL","http://panel.pay1.in");
define("CDEV_URL","http://b2c.pay1.in");
define("MAX_WALLET_BALANCE_ALLOWED",5000);

//define("CDEV_URL","http://cdev.pay1.in");
define('CIRCLE', '[
{
        "type": "States",
        "id": 0,
        "name": "Select Circle",
        "code": "SC"
    },
    {
        "type": "States",
        "id": 1,
        "name": "Andhra Pradesh",
        "code": "AP"
    },
    {
        "type": "States",
        "id": 2,
        "name": "Assam",
        "code": "AS"
    },
    {
        "type": "States",
        "id": 3,
        "name": "Jharkhand",
        "code": "BR"
    },
    {
        "type": "Metros",
        "id": 4,
        "name": "Chennai",
        "code": "CH"
    },
    {
        "type": "Metros",
        "id": 5,
        "name": "Delhi NCR",
        "code": "DL"
    },
    {
        "type": "States",
        "id": 6,
        "name": "Gujarat",
        "code": "GJ"
    },
    {
        "type": "States",
        "id": 7,
        "name": "Haryana",
        "code": "NE"
    },
    {
        "type": "States",
        "id": 8,
        "name": "Himachal Pradesh",
        "code": "HP"
    },
    {
        "type": "States",
        "id": 9,
        "name": "Jammu & Kashmir",
        "code": "JK"
    },
    {
        "type": "States",
        "id": 10,
        "name": "Karnataka",
        "code": "KA"
    },
    {
        "type": "States",
        "id": 11,
        "name": "Kerala",
        "code": "KL"
    },
    {
        "type": "Metros",
        "id": 12,
        "name": "Kolkata",
        "code": "KO"
    },
    {
        "type": "States",
        "id": 13,
        "name": "Maharashtra",
        "code": "MH"
    },
    {
        "type": "States",
        "id": 14,
        "name": "Madhya Pradesh",
        "code": "MP"
    },
    {
        "type": "Metros",
        "id": 15,
        "name": "Mumbai",
        "code": "MU"
    },
    {
        "type": "States",
        "id": 16,
        "name": "Tripura",
        "code": "NE"
    },
    {
        "type": "States",
        "id": 17,
        "name": "Orissa",
        "code": "OR"
    },
    {
        "type": "States",
        "id": 18,
        "name": "Punjab",
        "code": "PB"
    },
    {
        "type": "States",
        "id": 19,
        "name": "Rajasthan",
        "code": "RJ"
    },
    {
        "type": "States",
        "id": 20,
        "name": "Tamil Nadu",
        "code": "TN"
    },
    {
        "type": "States",
        "id": 21,
        "name": "Uttar Pradesh (East)",
        "code": "UE"
    },
    {
        "type": "States",
        "id": 22,
        "name": "Uttarakhand",
        "code": "UW"
    },
    {
        "type": "States",
        "id": 23,
        "name": "West Bengal",
        "code": "WB"
    }
]');


/* End of file constants.php */
/* Location: ./application/config/constants.php */