﻿using Pay1B2C.Model;
using SQLite;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pay1B2C.ViewModel
    {
    class NumberCircleHelper
        {
        SQLiteConnection dbConn;


        public async Task<bool> onCreate(string DB_PATH)
            {
            try
                {
                if(!CheckFileExists(App.PAY1_DB_PATH).Result)
                    {
                    using(dbConn = new SQLiteConnection(App.PAY1_DB_PATH))
                        {
                        dbConn.CreateTable<NumberAndCircle>();
                        }
                    }
                return true;
                }
            catch
                {
                return false;
                }
            }
        private async Task<bool> CheckFileExists(string fileName)
            {
            try
                {
                var store = await Windows.Storage.ApplicationData.Current.LocalFolder.GetFileAsync(fileName);
                return true;
                }
            catch
                {
                return false;
                }
            }


        public NumberAndCircle ReadNumberAndCircleByFlag(String number)
            {
            number=number.Substring(0, 4);
            using(var dbConn = new SQLiteConnection(App.PAY1_DB_PATH))
                {
                var existingconact = dbConn.Query<NumberAndCircle>("SELECT * from NumberAndCircle where number = '"+number+"'").FirstOrDefault();
                return existingconact;
                }
            }

        

       

        public void Insert(NumberAndCircle numberCircle)
            {
            using(var dbConn = new SQLiteConnection(App.PAY1_DB_PATH))
                {
                dbConn.RunInTransaction(() =>
                {
                dbConn.Insert(numberCircle);
                });
                }
            }


        public void DeleteMisscallById(String number)
            {
            using(var dbConn = new SQLiteConnection(App.PAY1_DB_PATH))
                {
                var existingconact = dbConn.Query<NumberAndCircle>("select * from NumberAndCircle where number =" + number).FirstOrDefault();
                if(existingconact != null)
                    {
                    dbConn.RunInTransaction(() =>
                    {
                        dbConn.Delete(existingconact);
                    });
                    }
                }
            }

        public void DeleteAllMissCall()
            {
            using(var dbConn = new SQLiteConnection(App.PAY1_DB_PATH))
                {

                dbConn.DropTable<NumberAndCircle>();
                dbConn.CreateTable<NumberAndCircle>();
                dbConn.Dispose();
                dbConn.Close();

                }
            }
        }

    }
