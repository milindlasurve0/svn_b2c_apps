<?php

class Home extends CI_Controller {
	
	public function index() {
                $this->load->library('user_agent');
                $app_string = $this->agent->agent;
                $this->load->model('payment_model');
                
                if($this->agent->is_mobile() && strpos($app_string, 'Windows') || strpos($app_string, 'Android')){
                    $this->load->view('home/pay1app');
                }else{
                    $this->load->view('home/index');
                }
               //echo $this->input->get('status');
	}

	public function setUserInfo() {

		if ($this->input->is_ajax_request()) {
			$sessiondata = $this->session->userdata('user');
			if (empty($sessiondata)) {
				$sessiondata = $this->input->post();
				$data = array('isLoggedIn' => TRUE);
				foreach ($sessiondata as $k => $v) {
					$data['user'][$k] = $v;
					if (in_array($k, $this->input->post())) {
						$data['user'][$k] = $this->input->post($k);
					}
				}
			} else {
				$data = array('isLoggedIn' => TRUE);
				foreach ($sessiondata as $k => $v) {
					$data['user'][$k] = $v;
					if (array_key_exists($k, $this->input->post())) {
						$data['user'][$k] = $this->input->post($k);
					}
				}
			}

			 $this->sess_expiration = 60*60*24 *7;
			$this->session->set_userdata($data);
			echo json_encode(array("status" => "success", "description" => ""));
		}
	}

	function logout(){
		$this->session->sess_destroy();
		redirect('home');
	}
	
	function success(){
                 $this->load->model('payment_model');
                 
		error_reporting(E_ALL);
		$parameters = $_REQUEST;
                $status = isset($parameters["status"]) ? $parameters["status"] : "";
		$txnid = isset($parameters["txnid"]) ? $parameters["txnid"] : "" ;
		$closingbal = isset($parameters["closing"]) ? $parameters["closing"] : "";
		$balance = isset($parameters["balance"]) ? $parameters["balance"] : "";
                $user_id = $this->payment_model->get_transaction($txnid); 
                
                $walletbalance = 0;
                if(!empty($user_id))
                $walletbalance = $this->payment_model->get_walletbalance($user_id); 
                $parameters["balance"] = $walletbalance;  
//		if (isset($closingbal) && !empty($closingbal)) {
//			$parameters["balance"] = $closingbal;
//		} else if (isset($balance) && !empty($balance)){
//			$parameters["balance"] = $balance;
//		}
                if(!empty($txnid)){
                     $url=$this->config->item('api_domain')."index.php/api_new/action/api/true/actiontype/transaction_details_web/?id=$txnid";
                    $data['transaction'] = json_decode($this->curl->get($url),TRUE);
                }
                $data['parameters'] = $parameters;
               // print_r($parameters);die;
		//redirect('/home/success/status/'.urlencode($status).'/txnid/'.urlencode($txnid).'/balance/'.urlencode($totalbal));
            
                $this->load->view('home/success',$data);
	}
	
	
}
