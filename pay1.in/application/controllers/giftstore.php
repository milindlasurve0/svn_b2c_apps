<?php

class Giftstore extends CI_Controller
{
//	function __construct() {
//        parent::__construct();
//        $isLoggedIn = $this->session->userdata('isLoggedIn');
//		if($isLoggedIn != 1 ){
//		redirect('/home/');
//		}
//    }

    public function index()
    {
        $user_id = isset($this->session->userdata['user']['user_id']) ? $this->session->userdata['user']['user_id'] : 0;
        $url=$this->config->item('api_domain')."index.php/api_new/action/api/true/actiontype/GetGiftsByType_WEB/?type=gift&type_id=1&displayLimit=4&api_version=3";
        if(!empty($user_id)){
            $url.="&user_id=$user_id";
        }
        $data['featured'] = json_decode($this->curl->get($url),TRUE);
        $url=$this->config->item('api_domain')."index.php/api_new/action/api/true/actiontype/GetGiftsByType_WEB/?type=gift&type_id=4&displayLimit=4&api_version=3";
        if(!empty($user_id)){
            $url.="&user_id=$user_id";
        }
        $data['recommended'] = json_decode($this->curl->get($url),TRUE);
        $url=$this->config->item('api_domain')."index.php/api_new/action/api/true/actiontype/GetGiftsByType_WEB/?type=category&type_id=2&displayLimit=4&api_version=3";
        if(!empty($user_id)){
            $url.="&user_id=$user_id";
        }
        $data['category'] = json_decode($this->curl->get($url),TRUE);
        $this->load->view('giftstore/index',$data);
    }
    
    public function gifttypes($id)
    {
        $data['sidebarCategories']=parent::setSideBarCategories();
        $user_id = isset($this->session->userdata['user']['user_id']) ? $this->session->userdata['user']['user_id'] : 0;
        
        $url=$this->config->item('api_domain')."index.php/api_new/action/api/true/actiontype/GetGiftsByType_WEB/?type=gift&next=-1&type_id={$id}";
        
        if(!empty($user_id)){
            $url.="&user_id=$user_id";
        }
        $data['response'] = json_decode($this->curl->get($url),TRUE);
        $data['id'] = $id;
        $this->load->view('giftstore/gifttypes',$data);
    }
   
    public function near_you($lt_lg)
    {
        $data = array();
         $lat = "";
         $lng = "";
        if(!empty($lt_lg)){
            $lat_lng =  explode("_",urldecode ($lt_lg));
            if(!empty($lat_lng) && isset($lat_lng[0]) && !empty($lat_lng[0])){
                $lat = $lat_lng[0];
                $lng = $lat_lng[1];
            }
            $data['sidebarCategories']=parent::setSideBarCategories();
            $distance=$this->config->item('distance');
            
            $url=$this->config->item('api_domain')."index.php/api_new/action/api/true/actiontype/get_deallist/?latitude=$lat&longitude=$lng&distance=$distance&api_version=3&next=-1";
            
            $data['response'] = json_decode($this->curl->get($url),TRUE);
        }
        
        $this->load->view('giftstore/near_you',$data);
        
    }
    
    public function giftdetails($id_area)
    {   
        
        $data = array();
        $id_area =  explode("/",urldecode($id_area));
        $id = $id_area;
        if(is_array($id_area))
        $id = $id_area[0];
       // echo $id_area[2];die;
        
        if(isset($_REQUEST['status'])){
            $expiryDate = '';
            $code = '';
            $pin = '';
            if(isset($_REQUEST['expiry_date']) && !empty($_REQUEST['expiry_date'])){
                $expiryDate = $_REQUEST['expiry_date'];
                $expiryDate = new DateTime($expiryDate);
                $expiryDate = $expiryDate->format('Y-m-d');
            }
            if(isset($_REQUEST['code']) && !empty($_REQUEST['code'])){
                $code = $_REQUEST['code'];
            }
            if(isset($_REQUEST['pin']) && !empty($_REQUEST['pin'])){
                $pin = trim($_REQUEST['pin']);
            }
            $status = trim($_REQUEST['status']);
            if($status=='success')
            $data['gift_status'] = array('status'=>$status,'expiry_date'=>$expiryDate,'code'=>$code,'pin'=>$pin);
           
        }
        
//        if(!empty($id))
//         {
          $user_id = $this->session->userdata['user']['user_id'];
          echo $detail_url = $this->config->item('api_domain')."index.php/api_new/action/actiontype/get_deal_details/api/true/?id=".$id."&latitude=&longitude=&api_version=3&user_id=".$user_id;
          $response = $this->curl->get($detail_url);
          $data['response'] = json_decode($response,true);
          //print_r( $data['response']); die;
          $detail_url = $this->config->item('api_domain')."index.php/api_new/action/api/true/actiontype/get_similar_WebDeal/?id=".$id."";
          $similar_deal = $this->curl->get($detail_url);
          $data['similar_deal'] = json_decode($similar_deal,true);
          
          
          $detail_url = $this->config->item('api_domain')."index.php/api_new/action/api/true/actiontype/get_deal_WebImg/?id=".$id."";
          $Img_url = $this->curl->get($detail_url);
          $data['Img_url'] = json_decode($Img_url,true);
         //}  
         //print_r($data); die;
        $this->load->view('giftstore/giftdetails',$data);
    }
    
    public function success(){
        $parameters = $_REQUEST;
        $status = $parameters['status'];
        $web_url = $parameters['web_url'];
        if($status=='success'){
            $expiry_date = $parameters['expiry_date'];
            $code = $parameters['code'];
            $pin = $parameters['pin'];
            $web_url .="_".$status."_".$expiry_date."_".$code."_".$pin;
        }else{
            $web_url .="_".$status;
        }
        
        redirect($web_url);
    }
    
    public function mygifts()
    {
        $user_id = $this->session->userdata['user']['user_id'];
        $url=$this->config->item('api_domain')."index.php/api_new/action/api/true/actiontype/GetMyGifts_WEB/?user_id={$user_id}";
        $data['response'] = json_decode($this->curl->get($url),TRUE);
        $this->load->view('giftstore/mygifts',$data);
    }
    
    public function mylikes()
    {
        $user_id = $this->session->userdata['user']['user_id'];
        $url=$this->config->item('api_domain')."index.php/api_new/action/api/true/actiontype/GetMyLikes_WEB/?user_id={$user_id}&next=-1";
        $data['response'] = json_decode($this->curl->get($url),TRUE);
        $this->load->view('giftstore/mylikes',$data);
    }
    
    public function mygiftdetails($id_area)
    {   
        $data = array();
        $id_area =  explode("_",urldecode($id_area));
        $id = $id_area[0];
        $txn_id = $id_area[1];
       
        if(!empty($id))
         {
          $user_id = $this->session->userdata['user']['user_id'];
          $detail_url = $this->config->item('api_domain')."index.php/api_new/action/actiontype/get_deal_details/api/true/?id=".$id."&latitude=&longitude=&api_version=3&user_id=".$user_id."&request_via=web";
          $response = $this->curl->get($detail_url);
          $data['response'] = json_decode($response,true);
          //print_r( $data['response']); die;
          $data['txn_id'] =$txn_id;
          $coupon_url = $this->config->item('api_domain')."index.php/api_new/action/actiontype/get_coupondetails/api/true/?txn_id=".$txn_id."&latitude=&longitude=&api_version=3&user_id=".$user_id;
          $response = $this->curl->get($coupon_url);
          $data['coupon_details'] = json_decode($response,true);
          
          $detail_url = $this->config->item('api_domain')."index.php/api_new/action/api/true/actiontype/get_deal_WebImg/?id=".$id."";
          $Img_url = $this->curl->get($detail_url);
          $data['Img_url'] = json_decode($Img_url,true);
         }  //print_r($data); die;
        $this->load->view('giftstore/mygiftdetails',$data);
    }
    
    
}
