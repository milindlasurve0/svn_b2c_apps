package com.pay1.databasehandler;

public class QuickPay {
	public class Profile {

		private int QuickPayID;
		private String Name;
		private String Mobile;
		private int Amount;
		private int MissCallFlag;
		private int AcknowladgeFlag;

		public int getQuickPayID() {
			return QuickPayID;
		}

		public void setQuickPayID(int quickPayID) {
			QuickPayID = quickPayID;
		}

		public String getName() {
			return Name;
		}

		public void setName(String name) {
			Name = name;
		}

		public String getMobile() {
			return Mobile;
		}

		public void setMobile(String mobile) {
			Mobile = mobile;
		}

		public int getAmount() {
			return Amount;
		}

		public void setAmount(int amount) {
			Amount = amount;
		}

		public int getMissCallFlag() {
			return MissCallFlag;
		}

		public void setMissCallFlag(int missCallFlag) {
			MissCallFlag = missCallFlag;
		}

		public int getAcknowladgeFlag() {
			return AcknowladgeFlag;
		}

		public void setAcknowladgeFlag(int acknowladgeFlag) {
			AcknowladgeFlag = acknowladgeFlag;
		}
	}

}
