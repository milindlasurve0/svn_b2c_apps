package com.pay1;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.pay1.constants.Constants;
import com.pay1.requesthandler.RequestClass;
import com.pay1.utilities.Utility;

public class NotificationActivity extends Activity {

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_full_one_button);
		try {
			Typeface Reguler = Typeface.createFromAsset(getAssets(),
					"Roboto-Regular.ttf");

			Typeface Medium = Typeface.createFromAsset(getAssets(),
					"Roboto-Regular.ttf");

			// set the custom dialog components - text, image and button
			TextView textView_Title = (TextView) findViewById(R.id.textView_Title);
			textView_Title.setText("Notification");
			textView_Title.setTypeface(Reguler);

			TextView textView_Message = (TextView) findViewById(R.id.textView_Message);
			textView_Message.setText(getIntent().getExtras().getString("MSG"));
			textView_Message.setTypeface(Medium);

			Button button_Ok = (Button) findViewById(R.id.button_Ok);
			button_Ok.setTypeface(Reguler);
			// if button is clicked, close the custom dialog
			button_Ok.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					finish();
				}
			});
			new CheckBalanceTask().execute();
		} catch (Exception e) {
		}
	}

	public class CheckBalanceTask extends AsyncTask<String, String, String> {
		// MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {

			String response = RequestClass.getInstance().readPay1B2CRequest(
					NotificationActivity.this, Constants.B2C_URL + "check_bal");
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			// if (this.dialog.isShowing()) {
			// this.dialog.dismiss();
			// }
			super.onPostExecute(result);
			try {
				// JSONArray array = new JSONArray(result);
				JSONObject jsonObject = new JSONObject(result);
				String status = jsonObject.getString("status");
				if (status.equalsIgnoreCase("SUCCESS")) {
					JSONObject description = jsonObject
							.getJSONObject("description");
					String account_balance = description
							.getString("account_balance");
					Utility.setBalance(NotificationActivity.this,
							Constants.SHAREDPREFERENCE_BALANCE,
							account_balance.trim());
					Utility.setBalance(NotificationActivity.this,
							Constants.SHAREDPREFERENCE_LOYALTY_POINTS,
							description.getString("loyalty_points"));
				}
			} catch (JSONException e) {
				// e.printStackTrace();
				// Constants.showCustomToast(getActivity(),
				// result);
			} catch (Exception e) {
				// TODO: handle exception
				// Constants.showCustomToast(getActivity(),
				// result);
			}

		}

		@Override
		protected void onPreExecute() {
			// dialog = new MyProgressDialog(getActivity());
			// // this.dialog.setMessage("Please wait.....");
			// this.dialog.show();
			super.onPreExecute();

		}

	}

}
