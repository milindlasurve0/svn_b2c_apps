package com.pay1;

import java.util.ArrayList;
import java.util.WeakHashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.pay1.adapterhandler.DealDetailsAdapter;
import com.pay1.constants.Constants;
import com.pay1.customviews.MyProgressDialog;
import com.pay1.requesthandler.RequestClass;

public class DealDetailsActivity extends AppBaseActivity {

	private static final String SCREEN_LABEL = "Deal Details Screen";
	private EasyTracker easyTracker = null;

	private final String TAG = "Deal";

	private ImageView imageView_Back, imageView_Share;
	private Button button_Buy;
	private TextView textView_Title;
	private ListView listView_DealDetails;
	private ArrayList<WeakHashMap<String, String>> data;
	private ArrayList<WeakHashMap<String, String>> data_location;
	private ArrayList<WeakHashMap<String, String>> data_image;
	private DealDetailsAdapter adapter;

	public static final String CONTENT_TYPE = "type";
	public static final String TITLE = "title";
	public static final String CONTENT = "content";
	public static final String DEAL_NAME = "dealname";
	public static final String OFFER_PRICE = "offer_price";
	public static final String CATAGORY = "category";
	public static final String CATAGORY_ID = "category_id";
	public static final String DISTANCE = "distance";
	public static final String ACTUAL_PRICE = "actual_price";
	public static final String DISCOUNT = "discount";
	public static final String TOTAL_STOCK = "total_stock";
	public static final String URL = "img_url";
	public static final String STOCK_SOLD = "stock_sold";
	public static final String LATITUDE = "latitude";
	public static final String LONGITUDE = "longitude";
	public static final String ADDRESS = "address";
	public static final String CITY = "city";
	public static final String STATE = "state";
	public static final String GALARY_URL = "gal_image";
	public static final String GALARY_INFO = "image_info";

	private String deal_ID, deal_name, lat, lng;

	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.deal_details_activity);
		try {

			easyTracker = EasyTracker.getInstance(DealDetailsActivity.this);
			easyTracker.set(Fields.SCREEN_NAME, SCREEN_LABEL);

			registerBaseActivityReceiver();

			Typeface Reguler = Typeface.createFromAsset(getAssets(),
					"EncodeSansNormal-400-Regular.ttf");
			Typeface Normal = Typeface.createFromAsset(getAssets(),
					"EncodeSansNormal-100-Thin.ttf");

			imageView_Back = (ImageView) findViewById(R.id.imageView_Back);
			imageView_Back.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onBackPressed();
				}
			});

			button_Buy = (Button) findViewById(R.id.button_Buy);
			button_Buy.setTypeface(Normal);
			button_Buy.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent intent = new Intent(DealDetailsActivity.this,
							DealOfferActivity.class);
					intent.putExtra("DEAL_ID", deal_ID);
					intent.putExtra("DEAL_NAME", deal_name);
					startActivity(intent);
				}
			});

			imageView_Share = (ImageView) findViewById(R.id.imageView_Share);
			imageView_Share.setVisibility(View.GONE);
			imageView_Share.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent sendIntent = new Intent();
					sendIntent.setAction(Intent.ACTION_SEND);
					sendIntent.putExtra(Intent.EXTRA_TEXT, deal_name);
					sendIntent.setType("text/plain");
					startActivity(Intent
							.createChooser(sendIntent, "Share with"));
				}
			});
			data = new ArrayList<WeakHashMap<String, String>>();
			data_image = new ArrayList<WeakHashMap<String, String>>();
			data_location = new ArrayList<WeakHashMap<String, String>>();
			listView_DealDetails = (ListView) findViewById(R.id.listView_DealDetails);

			adapter = new DealDetailsAdapter(DealDetailsActivity.this, data,
					data_image, data_location);
			listView_DealDetails.setAdapter(adapter);
			textView_Title = (TextView) findViewById(R.id.textView_Title);
			textView_Title.setTypeface(Reguler);

			Bundle bundle = getIntent().getExtras();
			if (bundle != null) {
				data.clear();
				WeakHashMap<String, String> map = new WeakHashMap<String, String>();
				map.put(CONTENT_TYPE, "HEADER");
				map.put(URL, bundle.getString("URL"));
				map.put(DEAL_NAME, bundle.getString("DEAL_NAME"));
				map.put(ACTUAL_PRICE, bundle.getString("ACTUAL_PRICE"));
				map.put(OFFER_PRICE, bundle.getString("OFFER_PRICE"));
				map.put(STOCK_SOLD, bundle.getString("STOCK_SOLD"));
				map.put(DISCOUNT, bundle.getString("DISCOUNT"));

				data.add(map);
				deal_ID = bundle.getString("DEAL_ID");
				deal_name = bundle.getString("DEAL_NAME");
				lat = bundle.getString("LAT");
				lng = bundle.getString("LNG");

				new DealDetailsTask().execute(deal_ID, lat, lng);
				// String lat = Utility.getCurrentLatitude(
				// DealDetailsActivity.this,
				// Constants.SHAREDPREFERENCE_CURRENT_LATITUDE);
				// String lng = Utility.getCurrentLongitude(
				// DealDetailsActivity.this,
				// Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE);
				// if (lat != null && lng != null) {
				// new DealDetailsTask().execute(deal_ID, lat, lng);
				// } else {
				// new DealDetailsTask().execute(deal_ID, Utility.getLatitude(
				// DealDetailsActivity.this,
				// Constants.SHAREDPREFERENCE_LATITUDE), Utility
				// .getLongitude(DealDetailsActivity.this,
				// Constants.SHAREDPREFERENCE_LONGITUDE));
				// }

				if (bundle.getInt("IS_BUY") == 0) {
					button_Buy.setVisibility(View.GONE);
				}
			}

		} catch (Exception e) {
		}
		overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}

	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView().build());
		// EasyTracker.getInstance(this).activityStart(this);
		easyTracker.send(MapBuilder.createAppView().build());
	}

	@Override
	public void onStop() {
		super.onStop();
		// EasyTracker.getInstance(this).activityStop(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		// Constants.showNavigationBar(DealDetailsActivity.this);
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unRegisterBaseActivityReceiver();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (Constants.isExpanded) {
			Constants.collapsView(this);
		} else {
			super.onBackPressed();
			overridePendingTransition(R.anim.slide_in_left,
					R.anim.slide_out_right);
		}
	}

	public class DealDetailsTask extends AsyncTask<String, String, String>
			implements OnDismissListener {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			try {
				String response = RequestClass.getInstance()
						.readPay1B2CRequest(
								DealDetailsActivity.this,
								Constants.B2C_URL + "get_deal_details/?id="
										+ params[0] + "&latitude=" + params[1]
										+ "&longitude=" + params[2]);

				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			try {

				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONObject jsonObject = new JSONObject(replaced);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {

						JSONObject jsonObject_Desc = jsonObject
								.getJSONObject("description");

						JSONArray jsonArrayDetails = jsonObject_Desc
								.getJSONArray("deal_detail");

						for (int i = 0; i < jsonArrayDetails.length(); i++) {
							JSONObject jsonObjectInner = jsonArrayDetails
									.getJSONObject(i);
							WeakHashMap<String, String> map = new WeakHashMap<String, String>();
							map.put(CONTENT_TYPE, "DEAL");
							// map.put(TITLE,
							// jsonObjectInner.getString("title"));
							map.put(CONTENT,
									jsonObjectInner.getString("content_txt"));

							data.add(map);
						}

						data_image.clear();
						JSONArray jsonArrayImage = jsonObject_Desc
								.getJSONArray("image_list");

						for (int i = 0; i < jsonArrayImage.length(); i++) {
							JSONObject jsonObjectInner = jsonArrayImage
									.getJSONObject(i);
							WeakHashMap<String, String> map = new WeakHashMap<String, String>();
							map.put(GALARY_URL,
									jsonObjectInner.getString("image_url"));
							map.put(GALARY_INFO,
									jsonObjectInner.getString("image_info"));
							data_image.add(map);
							if (i == 0) {
								map.put(CONTENT_TYPE, "IMAGE");
								data.add(map);
							}
						}

						data_location.clear();
						JSONArray jsonArrayLocation = jsonObject_Desc
								.getJSONArray("location_detail");

						for (int i = 0; i < jsonArrayLocation.length(); i++) {
							JSONObject jsonObjectInner = jsonArrayLocation
									.getJSONObject(i);
							WeakHashMap<String, String> map = new WeakHashMap<String, String>();
							map.put(LATITUDE, jsonObjectInner.getString("lat"));
							map.put(LONGITUDE, jsonObjectInner.getString("lng"));
							map.put(ADDRESS,
									jsonObjectInner.getString("address"));
							map.put(CITY, jsonObjectInner.getString("city"));
							map.put(STATE, jsonObjectInner.getString("state"));

							data_location.add(map);
							if (i == 0) {
								map.put(CONTENT_TYPE, "LOCATION");
								data.add(map);
							}
						}

						// data.clear();
						//
						// JSONArray jsonArrayDesc = jsonObject
						// .getJSONArray("description");
						//
						// for (int i = 0; i < jsonArrayDesc.length(); i++) {
						// WeakHashMap<String, String> map = new WeakHashMap<String,
						// String>();
						// map.put(DEAL_ID, jsonArrayDesc.getJSONObject(i)
						// .getString("id"));
						// map.put(DEAL_NAME, jsonArrayDesc.getJSONObject(i)
						// .getString("dealname"));
						// map.put(OFFER_PRICE, jsonArrayDesc.getJSONObject(i)
						// .getString("offer_price"));
						// map.put(CATAGORY, jsonArrayDesc.getJSONObject(i)
						// .getString("category"));
						// map.put(CATAGORY_ID, jsonArrayDesc.getJSONObject(i)
						// .getString("category_id"));
						// map.put(LATITUDE, jsonArrayDesc.getJSONObject(i)
						// .getString("latitude"));
						// map.put(LONGITUDE, jsonArrayDesc.getJSONObject(i)
						// .getString("longitude"));
						// map.put(DISTANCE, jsonArrayDesc.getJSONObject(i)
						// .getString("distance"));
						// map.put(ACTUAL_PRICE, jsonArrayDesc
						// .getJSONObject(i).getString("actual_price"));
						// map.put(DISCOUNT, jsonArrayDesc.getJSONObject(i)
						// .getString("discount"));
						// map.put(TOTAL_STOCK, jsonArrayDesc.getJSONObject(i)
						// .getString("total_stock"));
						// map.put(URL, jsonArrayDesc.getJSONObject(i)
						// .getString("img_url"));
						// map.put(STOCK_SOLD, jsonArrayDesc.getJSONObject(i)
						// .getString("stock_sold"));
						//
						// data.add(map);
						// }
						//
						DealDetailsActivity.this.runOnUiThread(new Runnable() {

							@Override
							public void run() {
								// TODO Auto-generated method stub
								adapter.notifyDataSetChanged();
							}
						});

					} else {
						Constants.showOneButtonDialog(DealDetailsActivity.this,
								TAG, Constants.checkCode(replaced),
								Constants.DIALOG_CLOSE);
					}

				} else {
					// Constants.showOneButtonDialog(DealDetailsActivity.this,
					// TAG, Constants.ERROR_INTERNET,
					// Constants.DIALOG_CLOSE);
					Intent intent = new Intent(DealDetailsActivity.this,
							ConnectivityErrorActivity.class);
					intent.putExtra(Constants.RECHARGE_FOR,
							Constants.SUBSCRIBE_DEAL);
					startActivity(intent);

				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonDialog(DealDetailsActivity.this, TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonDialog(DealDetailsActivity.this, TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(DealDetailsActivity.this);
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							DealDetailsTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			DealDetailsTask.this.cancel(true);
			dialog.cancel();
		}

	}

}
