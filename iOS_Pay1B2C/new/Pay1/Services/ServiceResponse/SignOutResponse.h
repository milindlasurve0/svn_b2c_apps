//
//  SignOutResponse.h
//  Pay1
//
//  Created by webninjaz on 03/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SignOutResponse : NSObject

@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) NSString *errCode;
@property (nonatomic, strong) NSString *descriptionSignOutResponse;

@end
