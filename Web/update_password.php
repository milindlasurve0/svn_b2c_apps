<?php
session_start();
require_once 'function.php';
require_once 'config.php';
include 'left.php';


?>


	<!-- //close navigation -->
       
<div class="shiftbox">
	<div class="hutpart" style="height:100%;width:100%; display:block; position:absolute;">
		<div class="container">
			<div class="row mT20">
				<div class="col-md-6 col-md-offset-3">
					<div class="signlogBOX">
                                            <span style="color:red;" id="msg"><?php echo $succMsg;?></span>
<!--						<span class="btn-info">Login</span>-->
<!--						<form class="form mT20" role="form">-->
                                      <form method="post" class="form mT20" id ="target" action="update_password.php">
						  <div class="form-group">
						    <label class="" for="">One Time Password</label>
						    <div class="input-group">
						     
						      <input class="form-control" onkeypress="return isNumberKey(event)" id ="otp" name="otp" type="text" size="50" maxlength="10">
						    </div>
                                                    <span style="color:red;"> <?php echo $mobErr;?></span>
                                                    <br>
						  </div>
						  <div class="form-group">
						  	<label class="" for="exampleInputEmail2">Enter Password</label>
						    <div class="input-group">
						      
						      <input class="form-control" name ="enter-password" id ="enter-password" size="50" type="password">
						    </div>
                                                        <span style="color:red;"><?php echo $passErr;?></span>
                                                    
						  </div>
                                                         <div class="form-group">
						  	<label class="" for="exampleInputEmail2">Confirm Password</label>
						    <div class="input-group">
						      
						      <input class="form-control" name ="confirm-password" size="50" id ="confirm-password" type="password">
						    </div>
                                                        <span style="color:red;"><?php echo $passErr;?></span>
                                                    
						  </div>
						  <div class="form-group">
						  	<button type="button" id="login" class="btn btn-primary btn-lg btn-block">Submit</button>
						  	
						  </div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div><!-- //shiftbox -->
        <!--</form>-->
</div>
<script src="js/jquery.min.2.1.1.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script>
    $('#login').click(function(){
        var mobileno = localStorage.getItem("mob_number");
        var gender = localStorage.getItem("gender");
        var otp = $("#otp").val();
        var enterpassword = $("#enter-password").val();
        var confirmpassword = $("#confirm-password").val();
        if(otp=='')
        {
            alert("Please enter a one time password.");
            return false;
        }
        else if(enterpassword=='')
        {
            alert("Enter Password can not be blank");
            return false;
        }
        else if(confirmpassword=='')
        {
            alert("confirm Password can not be blank");
            return false;
        }
        
        else if(enterpassword!=confirmpassword)
        {
            alert("Password does not match");
            return false;
        }
        else
        {
            var url = "<?php echo CDEV_URL; ?>/index.php/api_new/action/api/true/actiontype/update_password/?";
            $.ajax({
            url: url,
            type:"GET",
            data:{mobile_number:mobileno,
                  password:enterpassword,
                  confirm_password:enterpassword,
                  code:otp,
                  vtype:"1",
                  res_format : "jsonp"
                 },
            timeout: 50000,
            dataType: "jsonp",
            crossDomain: true,
            success:function(data){
            if(data.status=="success")
             {
            var loginurl = "<?php echo CDEV_URL; ?>/index.php/api_new/action/actiontype/signin/api/true/?";
            $.ajax({
            url: loginurl,
            type:"GET",
            data:{username:mobileno,
                  password:enterpassword,
                  uuid:"",
                  latitude:"",
                  logitude:"",
                  device_type:"",
                  res_format : "jsonp"
                 },
            timeout: 50000,
            dataType: "jsonp",
            jsonpCallback: 'callback',
            crossDomain: true,
            success:function(data){
               if(data.status=="success")
               {    
                   setdatavalue(data);
                   setpassword(enterpassword,gender);
               }
                else {
                  //  alert(data.description);
                   
                }

            },
            error: function (xhr,error) {
             },  
         
            });
              } else {
                   $("#msg").html(data.description);
              }
               
            },
            error: function (xhr,error) {
             },  
         
            });
        }

  
});
    function isNumberKey(evt){
     var charCode = (evt.which) ? evt.which : evt.keyCode
             if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

             return true
         }
     
    function setdatavalue(data)
    {
    var mobileNo = data.description["mobile"];
    var name = data.description["name"];
    var email = data.description["email"];
    var dob = data.description["date_of_birth"];
    var longitude =  data.description["longitude"]; 
    var latitude =  data.description["latitude"]; 
    var walletbal = data.description["wallet_balance"];
    var device_type = data.description["device_type"];
       $.ajax({
            url: "info.php",
            type:"POST",
            data:{username:mobileNo,
                  latitude:latitude,
                  logitude:longitude,
                  device_type:device_type,
                  walletbal : walletbal
                 },
            timeout: 50000,
            dataType: "json",
            success:function(data){
                
              if(data.status=="success")
              {
                   // window.location = "index.php";
              }
              
            },
            error: function (error) {
              

          }
            });
  
}

function setpassword(password,gender)
{
    
     $.ajax({
            url: "info.php",
            type:"POST",
            data:{password:password,
                  gender:gender
                 },
            timeout: 50000,
            dataType: "json",
            success:function(data){
               window.location = "index.php";

            },
            error: function (error) {
          }
            });
    
}

</script>



</body>
</html>