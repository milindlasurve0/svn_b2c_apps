//
//  CoinHistoryCriteria.h
//  Pay1
//
//  Created by webninjaz on 09/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CoinHistoryCriteria : NSObject

@property (nonatomic,strong) NSString *coinHistoryParamCriteria;
@property (nonatomic,strong) NSString *coinHistoryPageCriteria;
@property (nonatomic,strong) NSString *coinHistoryLimitCriteria;


@end
