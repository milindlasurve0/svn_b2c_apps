﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Media.Imaging;

namespace Pay1B2C.Views
    {
    public partial class Profile: PhoneApplicationPage
        {
        public Profile()
            {
            InitializeComponent();


           
            textMobile.Text=Constant.getUserNumber();

            string pinn = Constant.LoadPersistent<string>(Constant.SETTINGS_USER_PIN);
            string samplePin = "";
            for(int i = 1;i <= pinn.Length;i++)
                {
                samplePin = samplePin + "•";
                }
            textPassword.Text = samplePin;
            if(Constant.LoadPersistent<string>(Constant.SETTINGS_USER_GENDER).StartsWith("m"))
                {
                textGender.Text="MALE";
                }
            else
                {
                textGender.Text="FEMALE";
                }

            string ima=Constant.getUserimage();
            if(string.IsNullOrEmpty(Constant.getUserimage()))
                {
                userImage.Source=new BitmapImage( new Uri("/Images/ic_pay1_launcher.png",UriKind.RelativeOrAbsolute));
                }
            else
                {

                userImage.Source=new BitmapImage(new Uri(Constant.getUserimage(),UriKind.RelativeOrAbsolute));
                }
            textDOB.Text=Constant.LoadPersistent<string>(Constant.SETTINGS_USER_DOB);
            textName.Text=Constant.getUserName();
            textEmail.Text=Constant.getEmail();
            this.textEditAndSave.Tap+=(a, b) =>
            {
            editLayout();
            };
            }

        private void editLayout()
            {
            if(textEditAndSave.Text.Equals("EDIT"))
                {
                NavigationService.Navigate(new Uri("/Views/ProfileEdit.xaml?",UriKind.RelativeOrAbsolute));
                NavigationService.RemoveBackEntry();
                }
           
            }

        private void ButtonImage_Click(object sender, RoutedEventArgs e)
            {
            NavigationService.GoBack();
            }
        }
    }