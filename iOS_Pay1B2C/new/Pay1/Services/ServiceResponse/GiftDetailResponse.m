//
//  GiftDetailResponse.m
//  Pay1
//
//  Created by Annapurna on 18/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "GiftDetailResponse.h"

@implementation GiftDetailResponse

-(id)init {
    self = [super init];
    if(self) {
        self.imageListArray = [[NSMutableArray alloc] init] ;
        self.offerDetailArray = [[NSMutableArray alloc]init];
        self.locationDetailArray = [[NSMutableArray alloc]init];
    }
    return self;
}

@end
