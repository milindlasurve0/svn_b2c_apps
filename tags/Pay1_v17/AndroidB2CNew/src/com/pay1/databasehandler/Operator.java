package com.pay1.databasehandler;

public class Operator {

	private int oID;
	private int operatorID;
	private String operatorCode;
	private int operatorProductID;
	private String operatorName;
	private int operatorServiceID;
	private int operatorFlag;
	private int operatorDelegate;
	private int operatorSTV;
	private int operatorType;
	private long operatorUptateTime;
	private int operatorMaxAmount;
	private int operatorMinAmount;
	private String operatorChargeSlab;
	private int operatorServiceChargeAmount;
	private int operatorServiceChargePercent;
	private int operatorServiceTaxPercent;
	private int operatorLength;
	private String operatorPrefix;
	/**
	 * @return the oID
	 */
	public int getoID() {
		return oID;
	}
	/**
	 * @param oID the oID to set
	 */
	public void setoID(int oID) {
		this.oID = oID;
	}
	/**
	 * @return the operatorID
	 */
	public int getOperatorID() {
		return operatorID;
	}
	/**
	 * @param operatorID the operatorID to set
	 */
	public void setOperatorID(int operatorID) {
		this.operatorID = operatorID;
	}
	/**
	 * @return the operatorCode
	 */
	public String getOperatorCode() {
		return operatorCode;
	}
	/**
	 * @param operatorCode the operatorCode to set
	 */
	public void setOperatorCode(String operatorCode) {
		this.operatorCode = operatorCode;
	}
	/**
	 * @return the operatorProductID
	 */
	public int getOperatorProductID() {
		return operatorProductID;
	}
	/**
	 * @param operatorProductID the operatorProductID to set
	 */
	public void setOperatorProductID(int operatorProductID) {
		this.operatorProductID = operatorProductID;
	}
	/**
	 * @return the operatorName
	 */
	public String getOperatorName() {
		return operatorName;
	}
	/**
	 * @param operatorName the operatorName to set
	 */
	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}
	/**
	 * @return the operatorServiceID
	 */
	public int getOperatorServiceID() {
		return operatorServiceID;
	}
	/**
	 * @param operatorServiceID the operatorServiceID to set
	 */
	public void setOperatorServiceID(int operatorServiceID) {
		this.operatorServiceID = operatorServiceID;
	}
	/**
	 * @return the operatorFlag
	 */
	public int getOperatorFlag() {
		return operatorFlag;
	}
	/**
	 * @param operatorFlag the operatorFlag to set
	 */
	public void setOperatorFlag(int operatorFlag) {
		this.operatorFlag = operatorFlag;
	}
	/**
	 * @return the operatorDelegate
	 */
	public int getOperatorDelegate() {
		return operatorDelegate;
	}
	/**
	 * @param operatorDelegate the operatorDelegate to set
	 */
	public void setOperatorDelegate(int operatorDelegate) {
		this.operatorDelegate = operatorDelegate;
	}
	/**
	 * @return the operatorSTV
	 */
	public int getOperatorSTV() {
		return operatorSTV;
	}
	/**
	 * @param operatorSTV the operatorSTV to set
	 */
	public void setOperatorSTV(int operatorSTV) {
		this.operatorSTV = operatorSTV;
	}
	/**
	 * @return the operatorType
	 */
	public int getOperatorType() {
		return operatorType;
	}
	/**
	 * @param operatorType the operatorType to set
	 */
	public void setOperatorType(int operatorType) {
		this.operatorType = operatorType;
	}
	/**
	 * @return the operatorUptateTime
	 */
	public long getOperatorUptateTime() {
		return operatorUptateTime;
	}
	/**
	 * @param operatorUptateTime the operatorUptateTime to set
	 */
	public void setOperatorUptateTime(long operatorUptateTime) {
		this.operatorUptateTime = operatorUptateTime;
	}
	/**
	 * @return the operatorMaxAmount
	 */
	public int getOperatorMaxAmount() {
		return operatorMaxAmount;
	}
	/**
	 * @param operatorMaxAmount the operatorMaxAmount to set
	 */
	public void setOperatorMaxAmount(int operatorMaxAmount) {
		this.operatorMaxAmount = operatorMaxAmount;
	}
	/**
	 * @return the operatorMinAmount
	 */
	public int getOperatorMinAmount() {
		return operatorMinAmount;
	}
	/**
	 * @param operatorMinAmount the operatorMinAmount to set
	 */
	public void setOperatorMinAmount(int operatorMinAmount) {
		this.operatorMinAmount = operatorMinAmount;
	}
	/**
	 * @return the operatorChargeSlab
	 */
	public String getOperatorChargeSlab() {
		return operatorChargeSlab;
	}
	/**
	 * @param operatorChargeSlab the operatorChargeSlab to set
	 */
	public void setOperatorChargeSlab(String operatorChargeSlab) {
		this.operatorChargeSlab = operatorChargeSlab;
	}
	/**
	 * @return the operatorServiceChargeAmount
	 */
	public int getOperatorServiceChargeAmount() {
		return operatorServiceChargeAmount;
	}
	/**
	 * @param operatorServiceChargeAmount the operatorServiceChargeAmount to set
	 */
	public void setOperatorServiceChargeAmount(int operatorServiceChargeAmount) {
		this.operatorServiceChargeAmount = operatorServiceChargeAmount;
	}
	/**
	 * @return the operatorServiceChargePercent
	 */
	public int getOperatorServiceChargePercent() {
		return operatorServiceChargePercent;
	}
	/**
	 * @param operatorServiceChargePercent the operatorServiceChargePercent to set
	 */
	public void setOperatorServiceChargePercent(int operatorServiceChargePercent) {
		this.operatorServiceChargePercent = operatorServiceChargePercent;
	}
	/**
	 * @return the operatorServiceTaxPercent
	 */
	public int getOperatorServiceTaxPercent() {
		return operatorServiceTaxPercent;
	}
	/**
	 * @param operatorServiceTaxPercent the operatorServiceTaxPercent to set
	 */
	public void setOperatorServiceTaxPercent(int operatorServiceTaxPercent) {
		this.operatorServiceTaxPercent = operatorServiceTaxPercent;
	}
	/**
	 * @return the operatorLength
	 */
	public int getOperatorLength() {
		return operatorLength;
	}
	/**
	 * @param operatorLength the operatorLength to set
	 */
	public void setOperatorLength(int operatorLength) {
		this.operatorLength = operatorLength;
	}
	/**
	 * @return the operatorPrefix
	 */
	public String getOperatorPrefix() {
		return operatorPrefix;
	}
	/**
	 * @param operatorPrefix the operatorPrefix to set
	 */
	public void setOperatorPrefix(String operatorPrefix) {
		this.operatorPrefix = operatorPrefix;
	}

}