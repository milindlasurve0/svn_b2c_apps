//
//  StoreLocationViewController.h
//  Pay1
//
//  Created by Annapurna on 11/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "BaseViewController.h"

@class StoreAlertView;
@interface StoreLocationViewController : BaseViewController<GMSMapViewDelegate>{
    IBOutlet GMSMapView *_mapView;
    
    
    CLLocationDistance distanceY;
    
    CLLocationDistance distanceX;
    
    NSNumber *myDoubleNumber;
    
    CGFloat initialX;
    CGFloat initialY;
    
    BOOL hasLaunchedFirstTime;
    
    NSMutableArray *storeDataArray;
    
    
    UIView *coverView;
    UIActivityIndicatorView *activityIndicator;
    
    StoreAlertView *objStoreAlertView;
   
    NSMutableArray *shopDataArray;

}

@end
