﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Pay1B2C.Model;
using Pay1B2C.ViewModel;
using Pay1B2C.UserControls;
using System.Windows.Media.Imaging;
using SQLite;
using Facebook;
using Facebook.Client;
using System.ComponentModel;
using System.Globalization;
using System.Xml;
using Microsoft.Phone.Tasks;

namespace Pay1B2C
    {
    public partial class HomePage: PhoneApplicationPage
        {
        ProgressBarManager ProgressBarManager;
        public HomePage()
            {
            InitializeComponent();

           // Uri _callbackUri = WebAuthenticationBroker.GetCurrentApplicationCallbackUri();
            GoogleAnalytics.EasyTracker.GetTracker().SendView("HomePage");
            ProgressIndicator prog = new ProgressIndicator();
            ProgressBarManager = new ProgressBarManager();
            SystemTray.SetProgressIndicator(this, prog);
            ProgressBarManager.Hook(prog);
            
            Constant.laodNavigationBar(TitlePanel, LayoutRoot, canvas, "Home Page");

            try
                {
                  String feature=Constant.LoadPersistent<string>(Constant.FEATURED_DEALS);
                  JObject jObj=(JObject)JsonConvert.DeserializeObject(feature);
                  JArray jAr=(JArray)JsonConvert.DeserializeObject(jObj["offer_details"].ToString());
                  String offer_details=jAr[0]["details"].ToString();
                  string name=jAr[0]["name"].ToString();
                  string id=jAr[0]["id"].ToString();
                  seeAllPanel.Tap+=(a, b) =>
                  {
                      (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri("/Views/AllOffersByType.xaml?catName="+name+"&catId="+id+"&catType=0", UriKind.RelativeOrAbsolute));
                  };

                  loadTopOffers(offer_details);
                }
            catch(JsonException je)
                {
                }
            catch(Exception exc)
                {
                }
           
           // getQuickPayTask();
            this.Loaded+=(a, b) =>
            {
          
          
            StartLoadingData();
            };


            locationPanel.Tap+=(a, b) =>
            {
                NavigationService.Navigate(new Uri("/NearByRetailers.xaml?", UriKind.RelativeOrAbsolute));
            };
          
            }





        private async void CheckForUpdatedVersion()
            {
            var currentVersion = new Version(GetManifestAttributeValue("Version"));
            var updatedVersion = await GetUpdatedVersion();
            this.Dispatcher.BeginInvoke(() =>
            {

            if(updatedVersion != currentVersion
				&& MessageBox.Show("Do you want to install the new version now?", "Pay1 App New Update Available", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
                {
                new MarketplaceDetailTask().Show();
                }

            });
           
            }


        public static string GetManifestAttributeValue(string attributeName)
            {
            var xmlReaderSettings = new XmlReaderSettings
            {
                XmlResolver = new XmlXapResolver()
            };

            using(var xmlReader = XmlReader.Create("WMAppManifest.xml", xmlReaderSettings))
                {
                xmlReader.ReadToDescendant("App");

                return xmlReader.GetAttribute(attributeName);
                }
            }

        private Task<Version> GetUpdatedVersion()
            {
            var cultureInfoName = CultureInfo.CurrentUICulture.Name;

            var url = string.Format("http://marketplaceedgeservice.windowsphone.com/v8/catalog/apps/{0}?os={1}&cc={2}&oc=&lang={3}​",
                GetManifestAttributeValue("ProductID"),
                Environment.OSVersion.Version,
                cultureInfoName.Substring(cultureInfoName.Length - 2).ToUpperInvariant(),
                cultureInfoName);

            var request = WebRequest.Create(url);

            return Task.Factory.FromAsync(request.BeginGetResponse, result =>
            {
                var response = (HttpWebResponse)request.EndGetResponse(result);

                if(response.StatusCode != HttpStatusCode.OK)
                    {
                    throw new WebException("Http Error: " + response.StatusCode);
                    }

                using(var outputStream = response.GetResponseStream())
                    {
                    using(var reader = XmlReader.Create(outputStream))
                        {
                        reader.MoveToContent();

                        var aNamespace = reader.LookupNamespace("a");

                        reader.ReadToFollowing("entry", aNamespace);

                        reader.ReadToDescendant("version");

                        return new Version(reader.ReadElementContentAsString());
                        }
                    }
            }, null);
            }






        private BackgroundWorker backroungWorker;

        private void StartLoadingData()
            {

          
            backroungWorker = new BackgroundWorker();
            backroungWorker.DoWork += new DoWorkEventHandler(backroungWorker_DoWork);
            backroungWorker.RunWorkerCompleted +=
new RunWorkerCompletedEventHandler(backroungWorker_RunWorkerCompleted);
            backroungWorker.RunWorkerAsync();
            }

        void backroungWorker_DoWork(object sender, DoWorkEventArgs e)
            {
            DateTime dateTimeSynced=Constant.LoadPersistent<DateTime>("isGiftSynced");
            TimeSpan diff = DateTime.Now - dateTimeSynced;
            if(diff.Minutes>30)
                {
                SynncData();
                }
            if(diff.Hours>2)
                {
                CheckForUpdatedVersion();
                }
            }

        void backroungWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
            {
            this.Dispatcher.BeginInvoke(() =>
            {
                //(Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri("/HomePage.xaml?", UriKind.RelativeOrAbsolute));
            });
            }

        private async void SynncData()
            {
            await Task.Run(() => updateGiftData());
          
            //await Task.Run(() => getQuickPayTask());
            }

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
            {
            var result = MessageBox.Show("Are you sure you want to exit?", "Please confirm", MessageBoxButton.OKCancel);
            e.Cancel = result != MessageBoxResult.OK;
            while(this.NavigationService.BackStack.Any())
                {
                this.NavigationService.RemoveBackEntry();
                }
            base.OnBackKeyPress(e);
            }

        private void loadTopOffers(String order)
            {

           
            topOfferPanel.Children.Clear();
            GiftDataHelper gdh=new GiftDataHelper();
            List<GiftsTable> lst=   gdh.ReadGiftByOfferIds(order);
            foreach(GiftsTable gifts in lst)
                {
                HomeTopOffers hto=new HomeTopOffers();
                hto.offerImage.Source=new BitmapImage(new Uri(gifts.freebieURL));
                hto.offerText.Text=gifts.freebieShortDesc.ToString();
                hto.Tap+=(a, b) =>
                {
                (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri("/Views/GiftDetailsPage.xaml?offerId="+gifts.freebieOfferID+"&fromMyDeal=0", UriKind.RelativeOrAbsolute));
                };
                topOfferPanel.Children.Add(hto);
                }
           
            }

        private async Task<String> updateGiftData()
            {

           // ProgressBarManager.Show(20, "Please wait...", false, 10);
          
            var values = new List<KeyValuePair<string, string>>();
            values.Add(new KeyValuePair<string, string>("actiontype", "GetupdatedDeal"));
            values.Add(new KeyValuePair<string, string>("user_mobile", Constant.getUserNumber()));
        
            values.Add(new KeyValuePair<string, string>("latitude", ""));
            values.Add(new KeyValuePair<string, string>("longitude", ""));
            values.Add(new KeyValuePair<string, string>("next", "-1"));
            values.Add(new KeyValuePair<string, string>("api_version", Constant.API_VERSION));
            values.Add(new KeyValuePair<string, string>("api", "true"));
            try
                {
                DateTime strDate=       Constant.LoadPersistent<DateTime>(Constant.FREEBIE_LIST_UPDATE_TIME);
                string dat= strDate.ToString("yyyy-MM-dd HH:mm:ss");
                values.Add(new KeyValuePair<string, string>("updatedTime",dat));
                }
            catch(Exception exc)
                {
                }
            FormUrlEncodedContent values1 = new FormUrlEncodedContent(values);
            Task<string> response = Constant.postParamsAndgetResponse(Constant.B2C_URL, values1);


            string result = await response;
            if(string.IsNullOrEmpty(result))
                {
                NavigationService.Navigate(new Uri("/Views/NoConnection.xaml?", UriKind.RelativeOrAbsolute));
                }
            else
                {
                var dict = (JObject)JsonConvert.DeserializeObject(result);
                string status = dict["status"].ToString();

                if(status.Equals("success"))
                    {

                    var description = dict["description"];
                    Constant.SavePersistent("isGiftSynced", DateTime.Now);
                    String ExpiredOffers=description["ExpiredOffers"].ToString();
                    deleteAllExpiredOffers(ExpiredOffers);
                    JArray Alldeals = (JArray)JsonConvert.DeserializeObject(description["Alldeals"].ToString());
                    insertDataForGifts(Alldeals);
                  
                    }
                }

          //  ProgressBarManager.Hide(20);
            return "";
            }

        private void deleteAllExpiredOffers(string ExpiredOffers)
            {
            if(ExpiredOffers.Length>=1)
                {
                GiftDataHelper gdh=new GiftDataHelper();
                string[] ExpiredOffersArray=ExpiredOffers.Split(',');
                if(ExpiredOffersArray.Length!=0)
                    {
                    foreach(string id in ExpiredOffersArray)
                        {
                        gdh.DeleteGifts(Convert.ToInt16(id));
                        }
                    }
                }
            }



          protected override void OnNavigatedTo(NavigationEventArgs e)
            {
        //NavigationService.Navigate(new Uri(string.Format(NavigationService.Source +
        //                            "?Refresh=true&random={0}", Guid.NewGuid())));
            if(e.NavigationMode== NavigationMode.New)
                {
                if(NavigationContext.QueryString.ContainsKey("msg"))
                    {
                    string msg = NavigationContext.QueryString["msg"];
                    if(string.IsNullOrEmpty(msg))
                        {
                        }
                    else
                        {
                        MessageBox.Show(msg);
                        }
                    }
                }
            Constant.laodNavigationBar(TitlePanel, LayoutRoot, canvas, "Home Page");
              }
        private void insertDataForGifts(JArray Alldeals)
            {
            try
                {
                int j=0;
                GiftDataHelper gdh=new GiftDataHelper();
                foreach(JObject offers in Alldeals)
                    {
                    GiftsTable gifts=new GiftsTable();
                    gifts.freebieCategory=offers["cat"].ToString();
                    gifts.freebieCategoryID=(int)offers["cat_id"];
                    gifts.freebieDealerMobile=offers["dealer_contact"].ToString();
                    gifts.freebieOfferName=offers["of_name"].ToString();
                    gifts.freebieOfferID=(int)offers["of_id"];

                    gifts.freebieValidity=offers["valid"].ToString();
                    gifts.freebieShortDesc=offers["desc"].ToString();
                    gifts.freebieOfferName=offers["offer_desc"].ToString();
                    gifts.freebieDealName=offers["name"].ToString();
                    gifts.freebieMinAmount=(int)offers["min"];
                    gifts.freebieURL=offers["i_url"].ToString();
                    gifts.freebieLogoURL=offers["logo_url"].ToString();
                    gifts.freebieID=(int)offers["id"];
                    gifts.freebieDealID=(int)offers["id"];


                    gifts.price=(int)offers["price"];
                    gifts.offer_price=(int)offers["offer_price"];
                    gifts.by_voucher=(int)offers["by_voucher"];

                    JArray locationArray=(JArray)offers["locs"];
                    gifts.freebieLocationCount=locationArray.Count;
                    int i=0;
                    gdh.DeleteGiftsForLocation((int)offers["id"]);
                    foreach(JObject locObj in locationArray)
                        {
                        if(i==0)
                            {
                            gifts.freebieLat=(double)locObj["lat"];
                            gifts.freebieLng=(double)locObj["lng"];
                            gifts.freebieAddress=locObj["addr"].ToString();
                            gifts.freebieArea=locObj["area"].ToString();
                            gifts.freebieCity=locObj["city"].ToString();
                            gifts.freebieState=locObj["state"].ToString();
                            //  gifts.freebiePin=locObj["state"].ToString();
                            gifts.freebieUptateTime=1236547896545856;
                            }

                        LocationTable giftLocations=new LocationTable();
                        giftLocations.locationLat=(double)locObj["lat"];
                        giftLocations.locationLng=(double)locObj["lng"];
                        giftLocations.locationDealID=(int)offers["id"];
                        giftLocations.locationAddress=locObj["addr"].ToString();
                        giftLocations.locationArea=locObj["area"].ToString();
                        giftLocations.locationCity=locObj["city"].ToString();
                        giftLocations.locationState=locObj["state"].ToString();
                        giftLocations.locationUpdatedTime=1236547896545856;


                        gdh.InsertGiftLocation(giftLocations);


                        i++;
                        }


                    List<GiftsTable> lst= gdh.ReadGiftByOfferIds(offers["of_id"].ToString());
                    if(lst.Count==0)
                        {
                        gdh.InsertGifts(gifts);
                        }
                    else
                        {
                        gdh.DeleteGifts((int)offers["of_id"]);
                        gdh.InsertGifts(gifts);
                        }
                    System.Diagnostics.Debug.WriteLine(j++ +"-----"+offers["of_id"]);
                    }
                Constant.SavePersistent(Constant.FREEBIE_LIST_UPDATE_TIME, DateTime.Now);
                try
                    {
                    String feature=Constant.LoadPersistent<string>(Constant.FEATURED_DEALS);
                    JObject jObj=(JObject)JsonConvert.DeserializeObject(feature);
                    JArray jAr=(JArray)JsonConvert.DeserializeObject(jObj["offer_details"].ToString());
                    String offer_details=jAr[0]["details"].ToString();
                    loadTopOffers(offer_details);
                    }
                catch(JsonException je)
                    {
                    }
                catch(Exception exc)
                    {
                    }
                }
            catch(SQLiteException exc1)
                {
                }
            catch(Exception exc)
                {
                }
            }

    /*    private async void getQuickPayTask()
            {
            //ProgressBarManager.Show(40, "Please wait...", false, 10);
            var values = new List<KeyValuePair<string, string>>();
            values.Add(new KeyValuePair<string, string>("actiontype", "get_quickpaylist"));
            values.Add(new KeyValuePair<string, string>("api", "true"));
            values.Add(new KeyValuePair<string, string>("api_version", Constant.API_VERSION));
            FormUrlEncodedContent values1 = new FormUrlEncodedContent(values);
            Task<string> response = Constant.postParamsAndgetResponse(Constant.B2C_URL, values1);

            string result = await response;
            if(string.IsNullOrEmpty(result))
                {
                NavigationService.Navigate(new Uri("/Views/NoConnection.xaml?", UriKind.RelativeOrAbsolute));
                }
            else
                {
                var dict = (JObject)JsonConvert.DeserializeObject(result);
                string status = dict["status"].ToString();

                if(status.Equals("success"))
                    {

                    try
                        {

                        int ii = 0;
                        var description = dict["description"];
                        foreach(var item in description)
                            {
                            if(!string.IsNullOrEmpty(item["product"].ToString()) && !item["product"].ToString().Equals("0"))
                                {
                                QuickPayAllDataTable quickPayAllDataTable = new QuickPayAllDataTable();
                                quickPayAllDataTable.id = item["id"].ToString();
                                quickPayAllDataTable.missed_number = item["missed_number"].ToString();
                                quickPayAllDataTable.name = item["name"].ToString();
                                quickPayAllDataTable.number = item["number"].ToString();
                                quickPayAllDataTable.operator_code = item["operator_code"].ToString();
                                quickPayAllDataTable.operator_id = item["operator_id"].ToString();
                                quickPayAllDataTable.operator_name = item["operator_name"].ToString();
                                quickPayAllDataTable.product = item["product"].ToString();
                                quickPayAllDataTable.stv = item["stv"].ToString();
                                quickPayAllDataTable.transaction_flag = item["transaction_flag"].ToString();
                                quickPayAllDataTable.users_id = item["users_id"].ToString();
                                quickPayAllDataTable.amount = item["amount"].ToString();
                                quickPayAllDataTable.confirmation = item["confirmation"].ToString();
                                quickPayAllDataTable.datetime = item["datetime"].ToString();
                                quickPayAllDataTable.delegate_flag = item["delegate_flag"].ToString();
                                quickPayAllDataTable.flag = item["flag"].ToString();





                                try
                                    {
                                    MisscallDataHelper misscallHelper=new MisscallDataHelper();
                                    misscallHelper.DeleteMisscallById(item["id"].ToString());

                                    misscallHelper.Insert(quickPayAllDataTable);
                                    }
                                catch(SQLiteException swe)
                                    {
                                    }
                                catch(Exception es)
                                    {
                                    }
                                }
                            }
                        }
                    catch(SQLiteException see)
                        {
                        }

                    }

                else
                    {
                    Constant.checkForErrorCode(result);
                    }
                }
            Dispatcher.BeginInvoke(delegate()
            {
                //chaekcForMissCallData();
                //LoadMissCallData (dict["description"].ToString ());

            });
           // ProgressBarManager.Hide(40);
            }*/

        private void buttonImage_Click_Gift(object sender, System.Windows.Input.MouseEventArgs e)
            {
            NavigationService.Navigate(new Uri("/GiftMainPivot.xaml?", UriKind.RelativeOrAbsolute));
            }
        private void buttonImage_Click_Recharge(object sender, System.Windows.Input.MouseEventArgs e)
            {
        
            NavigationService.Navigate(new Uri("/Views/RechargeMain.xaml?", UriKind.RelativeOrAbsolute));
            //FacebookSessionClient fb = new FacebookSessionClient("563659977051507");
            //fb.LoginWithApp("basic_info,publish_actions,read_stream", "custom_state_string");
            }

        private void Button_Click(object sender, RoutedEventArgs e)
            {
            NavigationService.Navigate(new Uri("/NearByRetailers.xaml?", UriKind.RelativeOrAbsolute));
            
            }

        private void OnSessionStateChanged(object sender, Facebook.Client.Controls.SessionStateChangedEventArgs e)
            {
            //this.ContentPanel.Visibility = (e.SessionState == Facebook.Client.Controls.FacebookSessionState.Opened) ?
            //                    Visibility.Visible : Visibility.Collapsed;
            }
        
        }
    }