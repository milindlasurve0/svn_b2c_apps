//
//  OfferView.h
//  Pay1
//
//  Created by Annapurna on 05/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OfferView : UIView



@property (nonatomic, copy) void (^onMoneyClick)();
@property (nonatomic, copy) void (^onGiftClick)();
@property (nonatomic, copy) void (^onRechargeClick)();
@property (nonatomic, copy) void (^onHomeClick)();


@end
