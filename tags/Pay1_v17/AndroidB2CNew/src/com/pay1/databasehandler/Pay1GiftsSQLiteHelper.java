package com.pay1.databasehandler;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Pay1GiftsSQLiteHelper extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = "pay1b2cgiftsdatabase.db";
	private static final int DATABASE_VERSION = 3;

	public static final String TABLE_GIFTS = "GiftsTable";

	public static final String FG_ID = "freebieID";
	public static final String FG_DEAL_ID = "freebieDealID";
	public static final String FG_DEAL_NAME = "freebieDealName";
	public static final String FG_URL = "freebieURL";
	public static final String FG_CATAGORY = "freebieCategory";
	public static final String FG_CATAGORY_ID = "freebieCategoryID";
	public static final String FG_LAT = "freebieLat";
	public static final String FG_LNG = "freebieLng";
	public static final String FG_ADDRESS = "freebieAddress";
	public static final String FG_AREA = "freebieArea";
	public static final String FG_CITY = "freebieCity";
	public static final String FG_STATE = "freebieState";
	public static final String FG_PIN = "freebiePin";
	public static final String FG_LOCATION_COUNT = "freebieLocationCount";
	public static final String FG_OFFER_ID = "freebieOfferID";
	public static final String FG_OFFER_NAME = "freebieOfferName";
	public static final String FG_VALIDITY = "freebieValidity";
	public static final String FG_DESC = "freebieShortDesc";
	public static final String FG_MIN_AMOUNT = "freebieMinAmount";
	public static final String FG_UPDATED_TIME = "freebieUptateTime";
	public static final String FG_LIKE = "freebieLike";
	public static final String FG_LOGO_URL = "freebieLogoURL";
	public static final String FG_DEALER_CONTACT = "freebieDealerMobile";

	public static final String TABLE_LOCATION = "LocationTable";

	public static final String LOCATION_ID = "locationID";
	public static final String LOCATION_DEAL_ID = "locationDealID";
	public static final String LOCATION_LAT = "locationLat";
	public static final String LOCATION_LNG = "locationLng";
	public static final String LOCATION_ADDRESS = "locationAddress";
	public static final String LOCATION_AREA = "locationArea";
	public static final String LOCATION_CITY = "locationCity";
	public static final String LOCATION_STATE = "locationState";
	public static final String LOCATION_PIN = "locationPin";
	public static final String LOCATION_DISTANCE = "locationDistance";
	public static final String LOCATION_UPDATED_TIME = "locationUpdatedTime";

	private final String CREATE_GIFTS = "CREATE TABLE " + TABLE_GIFTS + " ( "
			+ FG_ID + " integer primary key autoincrement, " + FG_DEAL_ID
			+ " integer, " + FG_DEAL_NAME + " text, " + FG_URL + " text, "
			+ FG_CATAGORY + " text, " + FG_CATAGORY_ID + " integer, " + FG_LAT
			+ " integer, " + FG_LNG + " integer, " + FG_ADDRESS + " text, "
			+ FG_AREA + " text, " + FG_CITY + " text, " + FG_STATE + " text, "
			+ FG_PIN + " integer, " + FG_LOCATION_COUNT + " integer, "
			+ FG_OFFER_ID + " integer, " + FG_OFFER_NAME + " text, "
			+ FG_VALIDITY + " text, " + FG_DESC + " text, " + FG_MIN_AMOUNT
			+ " integer, " + FG_UPDATED_TIME + " integer, " + FG_LIKE
			+ " integer, " + FG_LOGO_URL + " text, " + FG_DEALER_CONTACT
			+ " text );";

	private final String CREATE_LOCATION = "CREATE TABLE " + TABLE_LOCATION
			+ " ( " + LOCATION_ID + " integer primary key autoincrement, "
			+ LOCATION_DEAL_ID + " integer, " + LOCATION_LAT + " integer, "
			+ LOCATION_LNG + " integer, " + LOCATION_ADDRESS + " text, "
			+ LOCATION_AREA + " text, " + LOCATION_CITY + " text, "
			+ LOCATION_STATE + " text, " + LOCATION_PIN + " integer, "
			+ LOCATION_DISTANCE + " integer, " + LOCATION_UPDATED_TIME
			+ " integer );";

	public Pay1GiftsSQLiteHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL(CREATE_GIFTS);
		db.execSQL(CREATE_LOCATION);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_GIFTS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_LOCATION);
		onCreate(db);
	}

}
