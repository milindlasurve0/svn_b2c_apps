package com.pay1.databasehandler;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class FreeBiesOrderDataSource {
	// Database fields
	private SQLiteDatabase database;
	private Pay1SQLiteHelper dbHelper;

	public FreeBiesOrderDataSource(Context context) {
		dbHelper = new Pay1SQLiteHelper(context);
	}

	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
		System.out.println("Released " + SQLiteDatabase.releaseMemory());
	}

	public void createFreeBiesOrder(int orderId, String orderData) {

		ContentValues values = new ContentValues();
		values.put(Pay1SQLiteHelper.ORDER_ID, orderId);
		values.put(Pay1SQLiteHelper.ORDER_DATA, orderData);

		database.insert(Pay1SQLiteHelper.TABLE_GIFTORDER, null, values);
		// Cursor cursor =
		// database.query(Pay1SQLiteHelper.TABLE_FreeBiesOrder,
		// null, Pay1SQLiteHelper.FreeBiesOrder_ID + " = " + insertId,
		// null,
		// null, null, null);
		// cursor.moveToFirst();
		// FreeBiesOrder neFreeBiesOrder =
		// cursorToFreeBiesOrder(cursor);
		// cursor.close();
		// return newFreeBiesOrder;
	}

	public void deleteFreeBiesOrder() {
		database.delete(Pay1SQLiteHelper.TABLE_GIFTORDER, null, null);
	}

	public List<FreeBiesOrder> getAllFreeBiesOrder() {
		List<FreeBiesOrder> FreeBiesOrder = new ArrayList<FreeBiesOrder>();

		Cursor cursor = database.query(Pay1SQLiteHelper.TABLE_GIFTORDER, null,
				null, null, null, null, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			FreeBiesOrder freebie = cursorToFreeBiesOrder(cursor);
			FreeBiesOrder.add(freebie);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return FreeBiesOrder;
	}

	public FreeBiesOrder getFreeBiesOrder(int orderId) {

		Cursor cursor = database.query(Pay1SQLiteHelper.TABLE_GIFTORDER, null,
				Pay1SQLiteHelper.ORDER_ID + " = " + orderId, null, null, null,
				null);

		if (cursor.getCount() > 0) {
			cursor.moveToFirst();
			FreeBiesOrder FreeBiesOrder = cursorToFreeBiesOrder(cursor);
			cursor.moveToNext();
			// make sure to close the cursor
			cursor.close();
			return FreeBiesOrder;
		} else {
			return null;
		}
	}

	public FreeBiesOrder getFreeBiesOrder() {

		Cursor cursor = database.query(Pay1SQLiteHelper.TABLE_GIFTORDER, null,
				null, null, null, null, Pay1SQLiteHelper.ORDER_ID + " limit 1");

		if (cursor.getCount() > 0) {
			cursor.moveToFirst();
			FreeBiesOrder FreeBiesOrder = cursorToFreeBiesOrder(cursor);
			cursor.moveToNext();
			// make sure to close the cursor
			cursor.close();
			return FreeBiesOrder;
		} else {
			return null;
		}
	}

	private FreeBiesOrder cursorToFreeBiesOrder(Cursor cursor) {
		FreeBiesOrder FreeBiesOrder = new FreeBiesOrder();
		FreeBiesOrder.setOrderID(cursor.getInt(0));
		FreeBiesOrder.setOrderData(cursor.getString(1));
		return FreeBiesOrder;
	}
}
