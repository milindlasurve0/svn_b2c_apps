package com.pay1.model;

import com.google.android.gms.maps.model.LatLng;

public class Spot {
	private String title;
	private String content;
	private LatLng position;

	/**
	 * @return the title
	 */
	public final String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public final void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the content
	 */
	public final String getContent() {
		return content;
	}

	/**
	 * @param content
	 *            the content to set
	 */
	public final void setContent(String content) {
		this.content = content;
	}

	/**
	 * @return the position
	 */
	public final LatLng getPosition() {
		return position;
	}

	/**
	 * @param position
	 *            the position to set
	 */
	public final void setPosition(LatLng position) {
		this.position = position;
	}
}
