package com.pay1.databasehandler;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.Context;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

public class Pay1DataGiftHelper extends SQLiteAssetHelper {
	private Context dbContext;

	// public static String dbPath = "/mnt/sdcard/Download";

	public Pay1DataGiftHelper(Context context) {
		super(context, Pay1SyncDataHelper.DATABASE_NAME_GIFT, null,
				Pay1SyncDataHelper.DATABASE_VERSION);
		setForcedUpgrade(Pay1SyncDataHelper.DATABASE_VERSION);
		dbContext = context;
	}

	/*
	 * public Pay1SyncDataHelper(Context context, String databaseName) {
	 * super(context, databaseName, null, DATABASE_VERSION); dbContext =
	 * context; }
	 */

	public boolean cloneDatabase(String inputDBName, String outputDBName) {
		try {
			File dbFile = dbContext.getDatabasePath(inputDBName);// new
																	// File(dbPath
																	// + "/" +
																	// inputDBName);
			InputStream input = new FileInputStream(dbFile);

			OutputStream output = new FileOutputStream(dbFile.getAbsoluteFile()
					.getParentFile().getAbsolutePath()
					+ "/" + outputDBName);

			byte[] buffer = new byte[1024];
			int length;
			while ((length = input.read(buffer)) > 0) {
				output.write(buffer, 0, length);
			}

			output.flush();
			output.close();
			input.close();
			return true;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}
}
