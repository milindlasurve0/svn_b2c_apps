package com.pay1;

import java.text.DecimalFormat;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.SQLException;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.larvalabs.svgandroid.SVG;
import com.larvalabs.svgandroid.SVGParser;
import com.pay1.constants.Constants;
import com.pay1.customviews.GiftCellLayout;
import com.pay1.customviews.GiftImageSpecialOffersLayout;
import com.pay1.databasehandler.FreeBies;
import com.pay1.databasehandler.FreeBiesDataSource;
import com.pay1.databasehandler.FreeBiesLocationDataSource;
import com.pay1.databasehandler.FreeBiesOrderDataSource;
import com.pay1.databasehandler.QuickPay;
import com.pay1.databasehandler.QuickPayDataSource;
import com.pay1.utilities.Utility;

public class QuickPayFragmentNew extends Fragment {
	private static final String SCREEN_LABEL = "Quick Pay Screen";
	private EasyTracker easyTracker = null;
	LinearLayout linearLayout_Main,empty_layout;
	Typeface Medium;
	TextView textViewBalance, textViewGiftCoins;
	private FreeBiesDataSource freeBiesDataSource;
	private FreeBiesLocationDataSource freeBiesLocationDataSource;
	private FreeBiesOrderDataSource freeBiesOrderDataSource;
	Button buttonAddMoney, buttonPocketAGift, buttonRecharge, buttonnearMe;
	ImageView imageViewRecharge, imageViewNearMe, imageViewBalance,
			imageViewCoins;
	LinearLayout layout_recharge, layout_add_money, layout_gift,
			layout_near_me;
	private QuickPayDataSource quickPayDataSource;
	public static QuickPayFragmentNew newInstance() {
		QuickPayFragmentNew fragment = new QuickPayFragmentNew();
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	@SuppressLint("NewApi")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.quickpay_fragment_new, container,
				false);
		try {

			easyTracker = EasyTracker.getInstance(getActivity());
			easyTracker.set(Fields.SCREEN_NAME, SCREEN_LABEL);

			Medium = Typeface.createFromAsset(getActivity().getAssets(),
					"EncodeSansNarrow-500-Medium.ttf");
			quickPayDataSource = new QuickPayDataSource(getActivity());
			freeBiesDataSource = new FreeBiesDataSource(getActivity());
			freeBiesLocationDataSource = new FreeBiesLocationDataSource(
					getActivity());
			freeBiesOrderDataSource = new FreeBiesOrderDataSource(getActivity());
			linearLayout_Main = (LinearLayout) view
					.findViewById(R.id.gift_layout);
			
			textViewBalance = (TextView) view
					.findViewById(R.id.textViewBalance);
			textViewGiftCoins = (TextView) view
					.findViewById(R.id.textViewGiftCoins);
			imageViewRecharge = (ImageView) view
					.findViewById(R.id.imageViewRecharge);
			imageViewNearMe = (ImageView) view
					.findViewById(R.id.imageViewNearMe);
			imageViewBalance = (ImageView) view
					.findViewById(R.id.imageViewBalance);
			imageViewCoins = (ImageView) view.findViewById(R.id.imageViewCoins);

			buttonAddMoney = (Button) view.findViewById(R.id.buttonAddMoney);
			buttonPocketAGift = (Button) view
					.findViewById(R.id.buttonPocketAGift);
			buttonRecharge = (Button) view.findViewById(R.id.buttonRecharge);
			buttonnearMe = (Button) view.findViewById(R.id.buttonnearMe);

			layout_recharge = (LinearLayout) view
					.findViewById(R.id.layout_recharge);
			layout_add_money = (LinearLayout) view
					.findViewById(R.id.layout_add_money);
			layout_gift = (LinearLayout) view.findViewById(R.id.layout_gift);
			layout_near_me = (LinearLayout) view
					.findViewById(R.id.layout_near_me);

			buttonAddMoney.setTypeface(Medium);
			buttonPocketAGift.setTypeface(Medium);
			buttonRecharge.setTypeface(Medium);
			buttonnearMe.setTypeface(Medium);

			DecimalFormat decimalFormat = new DecimalFormat("0.00");
			// Constants.SHAREDPREFERENCE_LOYALTY_POINTS
			String bal = Utility.getBalance(getActivity(),
					Constants.SHAREDPREFERENCE_BALANCE);
			bal = bal == null ? "0" : bal;
			textViewBalance.setText(decimalFormat.format(Double
					.parseDouble(bal)));
			textViewBalance.setTypeface(Medium);
			textViewGiftCoins.setTypeface(Medium);

			textViewGiftCoins.setCompoundDrawablePadding(3);

			SVG svgCoins = SVGParser.getSVGFromResource(getActivity()
					.getResources(), R.raw.ic_home_coin);
			Drawable svgCoinsDrawable = svgCoins.createPictureDrawable();

			SVG svgbalance = SVGParser.getSVGFromResource(getActivity()
					.getResources(), R.raw.ic_home_rupee);
			Drawable svgbalanceDrawable = svgbalance.createPictureDrawable();

			SVG svgRecharge = SVGParser.getSVGFromResource(getActivity()
					.getResources(), R.raw.ic_home_mobile);
			Drawable rechargeDrawable = svgRecharge.createPictureDrawable();

			SVG svgNearMe = SVGParser.getSVGFromResource(getActivity()
					.getResources(), R.raw.ic_home_nearme);
			Drawable nearMeDrawable = svgNearMe.createPictureDrawable();

			imageViewNearMe.setImageDrawable(nearMeDrawable);
			imageViewRecharge.setImageDrawable(rechargeDrawable);
			imageViewBalance.setImageDrawable(svgbalanceDrawable);
			imageViewCoins.setImageDrawable(svgCoinsDrawable);

			String loyalityPoints = Utility.getBalance(getActivity(),
					Constants.SHAREDPREFERENCE_LOYALTY_POINTS);
			textViewGiftCoins.setText(loyalityPoints);

			showFreeBies();

			buttonAddMoney.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Fragment fragment = WalletTopupFragement.newInstance();

					FragmentManager fragmentManager = getActivity()
							.getSupportFragmentManager();
					FragmentTransaction fragmentTransaction = fragmentManager
							.beginTransaction();
					fragmentTransaction.replace(R.id.frame_container, fragment);
					fragmentTransaction.addToBackStack(null);
					fragmentTransaction.commit();

				}
			});

			buttonRecharge.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Fragment fragment=null;
					
					
					try {
						quickPayDataSource.open();

						List<QuickPay> quickpays = quickPayDataSource
								.getQuickPayByLimit(2);
						int size = quickpays.size();
						if (size >= 2) {
							fragment = RechargeMainActivity.newInstance(0);
						} else {
							fragment = RechargeMainActivity.newInstance(1);
						}
					} catch (SQLException e) {
						// TODO: handle exception
					} catch (Exception e) {
						// TODO: handle exception
					} finally {
						quickPayDataSource.close();
					}
					

					FragmentManager fragmentManager = getActivity()
							.getSupportFragmentManager();
					FragmentTransaction fragmentTransaction = fragmentManager
							.beginTransaction();
					fragmentTransaction.replace(R.id.frame_container, fragment);
					fragmentTransaction.addToBackStack(null);
					fragmentTransaction.commit();
				}
			});

			buttonnearMe.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Fragment fragment = GiftMainActivity.newInstance(1);

					FragmentManager fragmentManager = getActivity()
							.getSupportFragmentManager();
					FragmentTransaction fragmentTransaction = fragmentManager
							.beginTransaction();
					fragmentTransaction.replace(R.id.frame_container, fragment);
					fragmentTransaction.addToBackStack(null);
					fragmentTransaction.commit();
				}
			});

			buttonPocketAGift.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Fragment fragment = GiftMainActivity.newInstance(0);

					FragmentManager fragmentManager = getActivity()
							.getSupportFragmentManager();
					FragmentTransaction fragmentTransaction = fragmentManager
							.beginTransaction();
					fragmentTransaction.replace(R.id.frame_container, fragment);
					fragmentTransaction.addToBackStack(null);
					fragmentTransaction.commit();
				}
			});
			
			
			layout_add_money.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Fragment fragment = WalletTopupFragement.newInstance();

					FragmentManager fragmentManager = getActivity()
							.getSupportFragmentManager();
					FragmentTransaction fragmentTransaction = fragmentManager
							.beginTransaction();
					fragmentTransaction.replace(R.id.frame_container, fragment);
					fragmentTransaction.addToBackStack(null);
					
					fragmentTransaction.commit();

				}
			});

			layout_recharge.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Fragment fragment=null;
					
					
					try {
						quickPayDataSource.open();

						List<QuickPay> quickpays = quickPayDataSource
								.getQuickPayByLimit(2);
						int size = quickpays.size();
						if (size >= 2) {
							fragment = RechargeMainActivity.newInstance(0);
						} else {
							fragment = RechargeMainActivity.newInstance(1);
						}
					} catch (SQLException e) {
						// TODO: handle exception
					} catch (Exception e) {
						// TODO: handle exception
					} finally {
						quickPayDataSource.close();
					}
					

					FragmentManager fragmentManager = getActivity()
							.getSupportFragmentManager();
					FragmentTransaction fragmentTransaction = fragmentManager
							.beginTransaction();
					fragmentTransaction.replace(R.id.frame_container, fragment);
					fragmentTransaction.addToBackStack(null);
					fragmentTransaction.commit();
				}
			});

			layout_near_me.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Fragment fragment = GiftMainActivity.newInstance(1);

					FragmentManager fragmentManager = getActivity()
							.getSupportFragmentManager();
					FragmentTransaction fragmentTransaction = fragmentManager
							.beginTransaction();
					fragmentTransaction.replace(R.id.frame_container, fragment);
					fragmentTransaction.addToBackStack(null);
					fragmentTransaction.commit();
				}
			});

			layout_gift.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Fragment fragment = GiftMainActivity.newInstance(0);

					FragmentManager fragmentManager = getActivity()
							.getSupportFragmentManager();
					FragmentTransaction fragmentTransaction = fragmentManager
							.beginTransaction();
					fragmentTransaction.replace(R.id.frame_container, fragment);
					fragmentTransaction.addToBackStack(null);
					fragmentTransaction.commit();
				}
			});
			
			
			
			
			

		} catch (Exception e) {

		}
		return view;

	}

	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView().build());
		// EasyTracker.getInstance(this).activityStart(this);
		easyTracker.send(MapBuilder.createAppView().build());
	}

	@Override
	public void onStop() {
		super.onStop();
		// EasyTracker.getInstance(this).activityStop(this);
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		// outState.putString(KEY_CONTENT, mContent);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.Fragment#onDestroy()
	 */
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		// Intent intent = new Intent(getActivity(), MainActivity.class);
		// intent.putExtra("FROM_QUICKPAY", false);
		// intent.putExtra("IS_GIFT", false);
		// intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
		// | Intent.FLAG_ACTIVITY_NEW_TASK);
		// getActivity().startActivity(intent);
		// getActivity().finish();
	}

	private void showFreeBies() {
		String offerJson = Utility.getSharedPreferences(getActivity(),
				"Deals_offer");

		try {
			JSONObject jsonObjectDescription = new JSONObject(offerJson);

			final JSONArray jsonArrayOffer = jsonObjectDescription
					.getJSONArray("offer_details");

			for (int i = 0; i < jsonArrayOffer.length(); i++) {
				final String catName = jsonArrayOffer.getJSONObject(i)
						.getString("name");
				final String catID = jsonArrayOffer.getJSONObject(i).getString(
						"id");

				final String layoutType = jsonArrayOffer.getJSONObject(i)
						.getString("featured");

				if (layoutType.equalsIgnoreCase("1")) {
					RelativeLayout relativeLayout = new RelativeLayout(
							getActivity());
					relativeLayout.setPadding(15, 15, 15, 5);
					{
						RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
								LayoutParams.WRAP_CONTENT,
								LayoutParams.WRAP_CONTENT);
						// Align bottom-right, and add
						// bottom-margin
						params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);

						TextView textView_Catagory = new TextView(getActivity());
						textView_Catagory.setText(catName);
						textView_Catagory.setLayoutParams(params);
						relativeLayout.addView(textView_Catagory);
					}
					{
						RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
								LayoutParams.WRAP_CONTENT,
								LayoutParams.WRAP_CONTENT);
						// Align bottom-right, and add
						// bottom-margin
						params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
						TextView textView = new TextView(getActivity());
						textView.setText("See all ");
						textView.setCompoundDrawablesWithIntrinsicBounds(0, 0,
								R.drawable.ic_next, 0);
						textView.setTextSize(12);
						textView.setTypeface(Medium);
						textView.setLayoutParams(params);

						relativeLayout.addView(textView);
					}
					relativeLayout
							.setOnClickListener(new View.OnClickListener() {

								@Override
								public void onClick(View v) {
									// TODO Auto-generated
									// method stub
									Intent intent = new Intent(getActivity(),
											GiftGridActivity.class);
									intent.putExtra("NAME", catName);
									intent.putExtra("TYPE", catID);
									startActivity(intent);
								}
							});
					linearLayout_Main.addView(relativeLayout);

					HorizontalScrollView hs = new HorizontalScrollView(
							getActivity());
					GiftImageSpecialOffersLayout giftCell = new GiftImageSpecialOffersLayout(
							getActivity());
					hs.addView(giftCell);
					linearLayout_Main.addView(hs);

					View v = new View(getActivity());
					LayoutParams layoutParams = new LayoutParams(
							LayoutParams.FILL_PARENT, 1);
					v.setLayoutParams(layoutParams);
					v.setBackgroundColor(getResources().getColor(
							R.color.LightGrey));
					v.setPadding(15, 0, 15, 0);
					linearLayout_Main.addView(v);

					// myHorizontalLayout = (MyHorizontalLayout)
					// view
					// .findViewById(R.id.mygallery);

					try {
						freeBiesDataSource.open();

						String str = jsonArrayOffer.getJSONObject(i).getString(
								"details");
						List<FreeBies> list = freeBiesDataSource
								.getAllFreeBies(str);
						int len = list.size();
						if (len != 0) {
							for (FreeBies freebie : list) {
								giftCell.add(freebie);
							}
							Log.d("Aftre for", "Aftre for");

						} else {
							// new FreeBiesTask()
							// .execute(
							// Utility.getCurrentLatitude(
							// getActivity(),
							// Constants.SHAREDPREFERENCE_CURRENT_LATITUDE),
							// Utility.getCurrentLongitude(
							// getActivity(),
							// Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE));
							// pager.setVisibility(View.GONE);

						}
					} catch (SQLException exception) {
						// TODO: handle exception
						// // // Log.e("Er ",
						// exception.getMessage());
					} catch (Exception e) {
						// TODO: handle exception
						// Log.e("Er ", e.getMessage());
					} finally {
						freeBiesDataSource.close();
					}
				} else {
					RelativeLayout relativeLayout = new RelativeLayout(
							getActivity());
					relativeLayout.setPadding(15, 5, 15, 5);
					{
						RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
								LayoutParams.WRAP_CONTENT,
								LayoutParams.WRAP_CONTENT);
						// Align bottom-right, and add
						// bottom-margin
						params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
						params.setMargins(0, 15, 0, 0);
						TextView textView_Catagory = new TextView(getActivity());
						textView_Catagory.setText(catName);
						textView_Catagory.setTypeface(Medium);
						textView_Catagory.setLayoutParams(params);
						relativeLayout.addView(textView_Catagory);
					}
					{
						RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
								LayoutParams.WRAP_CONTENT,
								LayoutParams.WRAP_CONTENT);
						// Align bottom-right, and add
						// bottom-margin
						params.setMargins(0, 15, 0, 0);
						params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
						TextView textView = new TextView(getActivity());
						textView.setText("See all ");
						textView.setCompoundDrawablesWithIntrinsicBounds(0, 0,
								R.drawable.ic_next, 0);
						textView.setTextSize(12);
						textView.setTypeface(Medium);
						textView.setLayoutParams(params);

						relativeLayout.addView(textView);
					}
					relativeLayout
							.setOnClickListener(new View.OnClickListener() {

								@Override
								public void onClick(View v) {
									// TODO Auto-generated
									// method stub
									Intent intent = new Intent(getActivity(),
											GiftGridActivity.class);
									intent.putExtra("NAME", catName);
									intent.putExtra("TYPE", catID);
									startActivity(intent);
								}
							});
					linearLayout_Main.addView(relativeLayout);

					HorizontalScrollView hs = new HorizontalScrollView(
							getActivity());
					GiftCellLayout giftCell = new GiftCellLayout(getActivity());
					hs.addView(giftCell);
					linearLayout_Main.addView(hs);

					View v = new View(getActivity());
					LayoutParams layoutParams = new LayoutParams(
							LayoutParams.FILL_PARENT, 1);
					v.setLayoutParams(layoutParams);
					v.setBackgroundColor(getResources().getColor(
							R.color.LightGrey));
					v.setPadding(15, 0, 15, 0);
					linearLayout_Main.addView(v);

					// myHorizontalLayout = (MyHorizontalLayout)
					// view
					// .findViewById(R.id.mygallery);

					try {
						freeBiesDataSource.open();

						String str = jsonArrayOffer.getJSONObject(i).getString(
								"details");
						List<FreeBies> list = freeBiesDataSource
								.getAllFreeBies(str);
						int len = list.size();
						if (len != 0) {
							for (FreeBies freebie : list) {
								giftCell.add(freebie);
							}
							Log.d("Aftre for", "Aftre for");

						} else {
							// new FreeBiesTask()
							// .execute(
							// Utility.getCurrentLatitude(
							// getActivity(),
							// Constants.SHAREDPREFERENCE_CURRENT_LATITUDE),
							// Utility.getCurrentLongitude(
							// getActivity(),
							// Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE));
							// pager.setVisibility(View.GONE);

						}
					} catch (SQLException exception) {
						// TODO: handle exception
						// // // Log.e("Er ",
						// exception.getMessage());
					} catch (Exception e) {
						// TODO: handle exception
						// Log.e("Er ", e.getMessage());
					} finally {
						freeBiesDataSource.close();
					}
				}

				/*
				 * RelativeLayout relativeLayout = new RelativeLayout(
				 * getActivity()); relativeLayout.setPadding(15, 5, 15, 5); {
				 * RelativeLayout.LayoutParams params = new
				 * RelativeLayout.LayoutParams( LayoutParams.WRAP_CONTENT,
				 * LayoutParams.WRAP_CONTENT); // Align bottom-right, and add //
				 * bottom-margin
				 * params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
				 * 
				 * TextView textView_Catagory = new TextView(getActivity());
				 * textView_Catagory.setText(catName);
				 * textView_Catagory.setLayoutParams(params);
				 * relativeLayout.addView(textView_Catagory); } {
				 * RelativeLayout.LayoutParams params = new
				 * RelativeLayout.LayoutParams( LayoutParams.WRAP_CONTENT,
				 * LayoutParams.WRAP_CONTENT); // Align bottom-right, and add //
				 * bottom-margin
				 * params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT); TextView
				 * textView = new TextView(getActivity());
				 * textView.setText("See all >");
				 * textView.setLayoutParams(params);
				 * 
				 * relativeLayout.addView(textView); }
				 * relativeLayout.setOnClickListener(new View.OnClickListener()
				 * {
				 * 
				 * @Override public void onClick(View v) { // TODO
				 * Auto-generated // method stub Intent intent = new
				 * Intent(getActivity(), GiftGridActivity.class);
				 * intent.putExtra("NAME", catName); intent.putExtra("TYPE",
				 * catID); startActivity(intent); } });
				 * linearLayout_Main.addView(relativeLayout);
				 * 
				 * HorizontalScrollView hs = new HorizontalScrollView(
				 * getActivity()); GiftCellLayout giftCell = new
				 * GiftCellLayout(getActivity()); hs.addView(giftCell);
				 * linearLayout_Main.addView(hs);
				 * 
				 * View v = new View(getActivity()); LayoutParams layoutParams =
				 * new LayoutParams( LayoutParams.FILL_PARENT, 1);
				 * v.setLayoutParams(layoutParams);
				 * v.setBackgroundColor(getResources
				 * ().getColor(R.color.LightGrey)); v.setPadding(15, 0, 15, 0);
				 * linearLayout_Main.addView(v);
				 * 
				 * // myHorizontalLayout = (MyHorizontalLayout) // view //
				 * .findViewById(R.id.mygallery);
				 * 
				 * try { freeBiesDataSource.open();
				 * 
				 * String str = jsonArrayOffer.getJSONObject(i).getString(
				 * "details"); List<FreeBies> list = freeBiesDataSource
				 * .getAllFreeBies(str); int len = list.size(); if (len != 0) {
				 * for (FreeBies freebie : list) { giftCell.add(freebie); }
				 * Log.d("Aftre for", "Aftre for");
				 * 
				 * } else { // new FreeBiesTask() // .execute( //
				 * Utility.getCurrentLatitude( // getActivity(), //
				 * Constants.SHAREDPREFERENCE_CURRENT_LATITUDE), //
				 * Utility.getCurrentLongitude( // getActivity(), //
				 * Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE)); //
				 * pager.setVisibility(View.GONE);
				 * 
				 * } } catch (SQLException exception) { // TODO: handle
				 * exception // // // Log.e("Er ", // exception.getMessage()); }
				 * catch (Exception e) { // TODO: handle exception //
				 * Log.e("Er ", e.getMessage()); } finally {
				 * freeBiesDataSource.close(); }
				 */
			}
		} catch (SQLException se) {

		} catch (Exception e) {
			// TODO: handle exception
		}
	}
}
