﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pay1B2C.Model
    {
    class LocationTable: INotifyPropertyChanged
        {
        public event PropertyChangedEventHandler PropertyChanged;


        private int getLocationID;
        private int getLocationDealID;
        private double getLocationLat;
        private double getLocationLng;
        private String getLocationAddress;
        private String getLocationArea;
        private String getLocationCity;
        private String getLocationState;
        private int getLocationPin;
        private double getLocationDistance;
        private long getLocationUpdatedTime;

        private void NotifyPropertyChanged(String info)
            {
            if(PropertyChanged != null)
                {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
                }
            }

        [SQLite.PrimaryKey, SQLite.AutoIncrement]
        public int locationID
            {
            get;
            set;
            }

        public long locationUpdatedTime
            {
            get { return this.getLocationUpdatedTime; }

            set
                {
                if(value != this.getLocationUpdatedTime)
                    {
                    this.getLocationUpdatedTime = value;
                    NotifyPropertyChanged("locationUpdatedTime");
                    }
                }
            }

        public String locationArea
            {
            get { return this.getLocationArea; }

            set
                {
                if(value != this.getLocationArea)
                    {
                    this.getLocationArea = value;
                    NotifyPropertyChanged("locationArea");
                    }
                }
            }

        public String locationCity
            {
            get { return this.getLocationCity; }

            set
                {
                if(value != this.getLocationCity)
                    {
                    this.getLocationCity = value;
                    NotifyPropertyChanged("locationCity");
                    }
                }
            }

        public String locationState
            {
            get { return this.getLocationState; }

            set
                {
                if(value != this.getLocationState)
                    {
                    this.getLocationState = value;
                    NotifyPropertyChanged("locationState");
                    }
                }
            }



        public double locationDistance
            {
            get { return this.getLocationDistance; }

            set
                {
                if(value != this.getLocationDistance)
                    {
                    this.getLocationDistance = value;
                    NotifyPropertyChanged("locationDistance");
                    }
                }
            }




        public int locationPin
            {
            get { return this.getLocationPin; }

            set
                {
                if(value != this.getLocationPin)
                    {
                    this.getLocationPin = value;
                    NotifyPropertyChanged("locationPin");
                    }
                }
            }





        public double locationLat
            {
            get { return this.getLocationLat; }

            set
                {
                if(value != this.getLocationLat)
                    {
                    this.getLocationLat = value;
                    NotifyPropertyChanged("locationLat");
                    }
                }
            }



        public double locationLng
            {
            get { return this.getLocationLng; }

            set
                {
                if(value != this.getLocationLng)
                    {
                    this.getLocationLng = value;
                    NotifyPropertyChanged("locationLat");
                    }
                }
            }

        public int locationDealID
            {
            get { return this.getLocationDealID; }

            set
                {
                if(value != this.getLocationDealID)
                    {
                    this.getLocationDealID = value;
                    NotifyPropertyChanged("locationDealID");
                    }
                }
            }


        public String locationAddress
            {
            get { return this.getLocationAddress; }

            set
                {
                if(value != this.getLocationAddress)
                    {
                    this.getLocationAddress = value;
                    NotifyPropertyChanged("locationAddress");
                    }
                }
            }


        }
    }
