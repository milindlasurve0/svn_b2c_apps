<html>
    <head>
        <link rel="shortcut icon" href="../img/pay1_favic.png">
        <title>Locate Pay1 Store</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="bootstrap/css/bootstrap-theme.min.css">
        <script src="jquery.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <style>
          .centerMarker{
              position:absolute;
              /*url of the marker*/
              /*background:url(http://maps.gstatic.com/mapfiles/markers2/marker.png) no-repeat;*/
              background:url(image/map_center_pin.png) no-repeat;
              /*center the marker*/
              top:50%;left:50%;

              z-index:1;
              /*fix offset when needed*/
              margin-left:-10px;
              margin-top:-34px;
              /*size of the image*/
              height:40px;
              width:40px;

              cursor:pointer;
            }
        </style>  
        
        
    </head>
    
    <body>    
<!--     <input id="lat" name="lat" placeholder="lat coordinate" type="text" class="form-control"/>
     <input id="lng" name="lng" placeholder="long coordinate" type="text" class="form-control"/>    -->
        
     <div class="container">
        <div class="row vcenter">
          <div class="col-lg-8 col-lg-offset-5 col-md-12 col-sm-12 ">
            <img src="image/pay1_logo.png"/>
          </div>  
        </div>
        <div class="row vcenter">
          <div class="col-lg-8 col-lg-offset-2 col-md-12 col-sm-12 ">
            <div class="row" style="margin-bottom:16px">
              <div class="col-lg-8">
                <div class="col-lg-10 col-md-12 col-sm-12">
                  <div class="input-group">
                    <span class="input-group-addon" id="sizing-addon2"><img src='image/map_pin_grey_xs.png'/></span>      
                      <input type="text" id="autocomplete" name="autocomplete" onFocus="geolocate()" class="form-control" placeholder="Your Location">
                  </div>
                </div>  
                <div class="col-lg-2 col-md-12 col-sm-12">
                   <button id="AddressMap"  type="submit" class="btn btn-danger" onclick="AddressMap()"><span class="glyphicon glyphicon-search "></span> Search</button>
                </div>
              </div>    
            </div>
            <div class="row">
              <div class="col-lg-8">
                <div id="map_canvas" style="height:80%; width:100%;" ></div>
                <div class="centerMarker"></div>
              </div>
              <div class="col-lg-4" style="height: 500px; overflow:auto;">
                STORE NEAR YOU 
               <div class="loader" style="display: none;">
                 <img src="image/img_loader.gif" style="padding-left: 50px">
               </div>    
               <p></p>
              </div>
            </div>
           </div>
        </div>    
     </div>
    
    <!---********************************************START Model Exceed limitation on Locate Pay1 Stores****************************************** -->
    <div class="modal fade" role="dialog" id="locatepay1_maplimit_exceed" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" style="top: 200px;">
              <div class="modal-header">
                <button class="close" data-dismiss="modal">&times; </button>
                <div class="text-center"><h4>This activity doesn't look genuine. </h4></div>
              </div>
                
              <div class="modal-body clearfix text-center">
                   <img src="image/ic_ghost.jpg" alt="">  
                <div class="text-center">
                    <h4 id='grab_error'><strong>Are you a ghost ?</strong></h4>
                </div>
                <div class="text-center">
                    <h4 id='grab_error'>Please reload the page again.</h4>
                </div>          
                <div class="margin-top-50"></div>
                <div class="col-md-4"></div>
                <div class="col-md-4" style="text-align:center"><button class="btn btn-default btn-block" data-dismiss="modal" onClick="window.location.reload()">RELOAD</button></div>
                <div class="col-md-4"></div>
              </div>
            </div>
        </div>
    </div>
    <!---********************************************END Model Exceed limitation on Locate Pay1 Stores****************************************** -->    

     <?php
        $tokenvalue = md5(uniqid(mt_rand(), true)); 
        $url = 'https://cashpg.pay1.in/apis/setTokenValue/'.$tokenvalue;
        $handle = curl_init($url);
        $res = curl_exec($handle);
       echo $body = "<input type='hidden' name ='token' id ='token' value='$tokenvalue'>";
       if($res == 1){ ?> 
        <script src="https://maps.google.com/maps/api/js?libraries=places,drawing,geometry"></script>
     <?php 
        require 'jsmin-php/jsmin.php';
        echo "<script>". JSMin::minify(file_get_contents('miniLocat.js'))."</script>";
         } ?>
    </body>
</html>
