//
//  SidebarViewController.h
//  SidebarDemo
//
//  Created by Simon on 29/6/13.
//  Copyright (c) 2013 Appcoda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import "MBProgressHUD.h"

@class SignOutService;
@class SignOutResponse;

@interface SidebarViewController : UITableViewController<FBLoginViewDelegate>{
    UIView *walletView;
    UIView *coinView;
    UIView *heartView;
    
    BOOL isFBCalled;
    
    FBLoginView *loginview;
    
    
    BOOL isfoundFBData;
    MBProgressHUD *HUD;

    
}



@end
