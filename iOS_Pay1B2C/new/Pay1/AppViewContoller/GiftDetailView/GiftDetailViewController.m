//
//  GiftDetailViewController.m
//  Pay1
//
//  Created by Annapurna on 19/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "GiftDetailViewController.h"
#import "GiftDetailService.h"
#import "GiftDetailResponse.h"
#import "UIImage+Utility.h"
#import "GiftLocationList.h"
#import "GiftDetailMapViewController.h"
#import "DBManager.h"
#import "GiftDetailCriteria.h"
#import "CurrentLocation.h"
#import "DetailImageView.h"
#import "MyLikeService.h"
#import "MyLikeResponse.h"
#import "GiftdetailCommonView.h"
#import "PurchaseDealService.h"
#import "PurchaseDealCriteria.h"
#import "PurchaseDealResponse.h"
#import "RateView.h"
#import "SaveUserReviewService.h"
#import "SaveUserResponse.h"
#import "GiftOfferDetailList.h"
#import "RedeemResponse.h"
#import "GetMyDealList.h"
#import "RechargeTransactionViewController.h"
#import "GiftSingleViewController.h"
#import "RechargeResponse.h"
@interface GiftDetailViewController ()

@end

@implementation GiftDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [rupeeImageView removeFromSuperview];
    [offerPriceLabel removeFromSuperview];
    [plusImageView removeFromSuperview];
    
    
    isPurchasedGift = FALSE;
    isLocationNotFound = FALSE;
    isForRedeem = FALSE;
    isGiftFound = FALSE;
    
    isFromWallet = TRUE;
    
    isOfferPriceFound = FALSE;
  // isMoveToRechargeView = FALSE;
    
    [self showHUDOnView];
    
    [firstGiftLabel removeFromSuperview];
    [secondGiftLabel removeFromSuperview];
    
    likeArray = [[NSMutableArray alloc]init];
    
    [self hideNavBackButton];
    
    firstSepView.backgroundColor = [UIColor colorWithRed:236/255.0 green:236/255.0 blue:236/255.0 alpha:1];
    secondSepView.backgroundColor = [UIColor colorWithRed:236/255.0 green:236/255.0 blue:236/255.0 alpha:1];
   // thirdSepView.backgroundColor = [UIColor colorWithRed:236/255.0 green:236/255.0 blue:236/255.0 alpha:1];
    
    NSString *name = [_shopName stringByReplacingOccurrencesOfString:@"%27" withString:@"'"];
    if(name.length > 20){
    if([UIScreen mainScreen].bounds.size.width == 320)
    [self setNaviationButtonWithText:[NSString stringWithFormat:@"%@",name] selector:@selector(backClicked:) isLeft:TRUE isReduceSize:FALSE isMyFont:15.0];
    else
    [self setNaviationButtonWithText:[NSString stringWithFormat:@"%@",name] selector:@selector(backClicked:) isLeft:TRUE isReduceSize:FALSE isMyFont:15.0];
    }
    else
    [self setNaviationButtonWithText:[NSString stringWithFormat:@"%@",name] selector:@selector(backClicked:) isLeft:TRUE isReduceSize:FALSE isMyFont:18.0];


    
    [offerDescWebView removeFromSuperview];
    
    
    isMoreClicked = FALSE;
    
    [self setConfirmVIewOfButton:callButton];
    
    getITButton.backgroundColor = [UIColor colorWithRed:251/255.0 green:72/255.0 blue:71/255.0 alpha:1];
    getITButton.layer.cornerRadius = 5;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    offerDetailArray = [[NSMutableArray alloc]init];
    locationDetailArrayList = [[NSMutableArray alloc]init];
    
    NSLog(@"#TheGiftIDIS :%@",_giftID);
    
    scrollView.contentSize = CGSizeMake(self.view.frame.size.width, 800);
    
    [self setFrameForGiftDetailContainerView];
    

    
    if([UIScreen mainScreen].bounds.size.width == 320){
    callButton.frame = CGRectMake(5, 10, 150, 40);
    getITButton.frame = CGRectMake(callButton.frame.origin.x + callButton.frame.size.width +10, 10, 150, 40);
    }
    
    
    
    if(_giftType == kSelectedTypeMyGift){
    isAlert = TRUE;
    isGiftFound = TRUE;
        
    [self showViewForMyGiftsView:FALSE];
    }
    
    
    else{
    isAlert = FALSE;

    isGiftFound = FALSE;
    [giftFirst1SepView removeFromSuperview];
    [giftFirst2SepView removeFromSuperview];
    [giftSepView removeFromSuperview];

    [myGiftDetailContainerView removeFromSuperview];
        
    giftDescContainerView.frame = CGRectMake(0, 0, self.view.frame.size.width,250);
        
    detailInfoSep1View.frame = CGRectMake(0, giftDescContainerView.frame.origin.y + giftDescContainerView.frame.size.height + 40, self.view.frame.size.width, 1);
    firstSepView.frame = CGRectMake(0, detailInfoSep1View.frame.origin.y + detailInfoSep1View.frame.size.height, self.view.frame.size.width, 12);
    detailInfoSep2View.frame = CGRectMake(0, firstSepView.frame.origin.y + firstSepView.frame.size.height, self.view.frame.size.width, 1);
        
    offerTxtLabel.frame = CGRectMake(100, detailInfoSep2View.frame.origin.y + detailInfoSep2View.frame.size.height + 15, self.view.frame.size.width, 21);
    offerRedView.frame = CGRectMake(0, offerTxtLabel.frame.origin.y + offerTxtLabel.frame.size.height+ 10, self.view.frame.size.width, 1);
        
    detailInfoView.frame = CGRectMake(0, offerRedView.frame.origin.y + offerRedView.frame.size.height+ 20, self.view.frame.size.width, 10);

    }
    
    
    [self fetchGiftDetailDataFromDB];
    
    
    
    UITapGestureRecognizer *gesRecognzier = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(showInfo:)];
    gesRecognzier.numberOfTapsRequired = 1;
    [moreView addGestureRecognizer:gesRecognzier];
}

-(void)showInfo:(UITapGestureRecognizer *)recognizer{
    [self moreClicked:self];
}



-(void)fetchGiftDetailDataFromDB{
    NSString *query;
    NSLog(@"#GiftID :%@",_giftOfferID);
    DBManager *dbManager = [[DBManager alloc]initWithDatabaseFilename:@"dbgiftdata.sql"];
    query = [NSString stringWithFormat:@"select * from GIFTTABLE WHERE FREEBIEDEALID=\"%@\"",_giftID];
    
    NSString * likeQuery = [NSString stringWithFormat:@"SELECT FREEBIEOFFERID FROM GIFTTABLE WHERE FREEBIELIKE=\"%d\"",1];
    NSLog(@"MYLIKES :%@",[dbManager loadDataFromDB:likeQuery]);
    
    for(NSArray *arr in [dbManager loadDataFromDB:likeQuery]){
    NSLog(@"array :%@",[arr objectAtIndex:0]);
    [likeArray addObject:[arr objectAtIndex:0]];

    }
    
    
    NSArray *myLikeArray = [[Session shared].objSignInResponse.offerLikes componentsSeparatedByString:@","];
    
    if(myLikeArray.count > 0){
    for(int i=0;i<myLikeArray.count;i++){
    [likeArray addObject:[myLikeArray objectAtIndex:i]];
    }
    }
    
    
    
    for(int i=0;i<likeArray.count;i++){
    if([_giftOfferID isEqualToString:[likeArray objectAtIndex:i]])
    [heartButton setBackgroundImage:[UIImage imageNamed:@"red_heart.png"] forState:UIControlStateNormal];
    }
    
    NSArray *theArray = [[NSArray alloc] initWithArray:[dbManager loadDataFromDB:query]];
    
   // NSLog(@"#TheArray :%@",theArray);
    
    NSInteger indexOfOfferURL = [dbManager.arrColumnNames indexOfObject:@"FREEBIEURL"];
    for(int i=0;i<theArray.count;i++){
    NSLog(@">>>>>%@", [[theArray objectAtIndex:i] objectAtIndex:indexOfOfferURL]);
        
    [giftImageView setImageWithURL:[NSURL URLWithString:[[theArray objectAtIndex:i] objectAtIndex:indexOfOfferURL]] placeholderImage:[UIImage imageNamed:@"white.png"]completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
    UIImage *newImage = [UIImage imageWithImage:image  scaledToSize:CGSizeMake(320,212)];
    giftImageView.image = newImage;
    }usingActivityIndicatorStyle:(UIActivityIndicatorViewStyle)UIActivityIndicatorViewStyleGray];
    }
    
    
    NSInteger indexOfThumbNailURL = [dbManager.arrColumnNames indexOfObject:@"FREEBIELOGOURL"];
    for(int i=0;i<theArray.count;i++){
    NSLog(@">>>>>%@", [[theArray objectAtIndex:i] objectAtIndex:indexOfThumbNailURL]);
        
    [thumbImageView setImageWithURL:[NSURL URLWithString:[[theArray objectAtIndex:i] objectAtIndex:indexOfThumbNailURL]] placeholderImage:[UIImage imageNamed:@"deal_icon.png"]completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
            UIImage *newImage = [UIImage imageWithImage:image  scaledToSize:CGSizeMake(320,212)];
    thumbImageView.image = newImage;
    }usingActivityIndicatorStyle:(UIActivityIndicatorViewStyle)UIActivityIndicatorViewStyleGray];
    }
    
    NSInteger indexOfOfferDetailURL = [dbManager.arrColumnNames indexOfObject:@"FREEBIESHORTDESC"];
    for(int i=0;i<theArray.count;i++){
    NSLog(@">>>>>%@", [[theArray objectAtIndex:i] objectAtIndex:indexOfOfferDetailURL]);
    giftTitleLabel.text = [[theArray objectAtIndex:i] objectAtIndex:indexOfOfferDetailURL];
        
    }
    
    NSInteger indexOfLatURL = [dbManager.arrColumnNames indexOfObject:@"FREEBIELAT"];
    for(int i=0;i<theArray.count;i++){
    NSLog(@"ADDRESS>>>>>%@", [[theArray objectAtIndex:i] objectAtIndex:indexOfLatURL]);
    shopLat   =  [[theArray objectAtIndex:i] objectAtIndex:indexOfLatURL];
    }
    
    NSInteger indexOfLongURL = [dbManager.arrColumnNames indexOfObject:@"FREEBIELNG"];
    for(int i=0;i<theArray.count;i++){
    NSLog(@"ADDRESS>>>>>%@", [[theArray objectAtIndex:i] objectAtIndex:indexOfLongURL]);
    shopLong   =  [[theArray objectAtIndex:i] objectAtIndex:indexOfLongURL];
    }
    
    
   
    
    NSInteger indexOfPriceURL = [dbManager.arrColumnNames indexOfObject:@"FREEBIEMINAMOUNT"];
    for(int i=0;i<theArray.count;i++){
    NSLog(@">>>>>%@", [[theArray objectAtIndex:i] objectAtIndex:indexOfPriceURL]);
    giftPriceLabel.text   = [[theArray objectAtIndex:i] objectAtIndex:indexOfPriceURL];
    }
    
    NSInteger indexOfShopNameURL = [dbManager.arrColumnNames indexOfObject:@"FREEBIEDEALNAME"];
    for(int i=0;i<theArray.count;i++){
    NSLog(@">>>>>%@", [[theArray objectAtIndex:i] objectAtIndex:indexOfShopNameURL]);
   // shopNameStr   = [[theArray objectAtIndex:i] objectAtIndex:indexOfShopNameURL];
        
    shopNameStr  =  [[[theArray objectAtIndex:i] objectAtIndex:indexOfShopNameURL] stringByReplacingOccurrencesOfString:@"%27" withString:@"'"];
    }
    
   // [self setFrameForGiftDetailContainerView];

    
    [self callGiftDetailAPIService];
}

-(void)callGiftDetailAPIService{
    GiftDetailService *service = [[GiftDetailService alloc]init];
    [service callGiftDetailService:self.criteria callback:^(NSError *error, GiftDetailResponse *giftDetailResponse) {
    if(error)return ;
    else{
    NSLog(@"#GiftDetailResponse");
    giftDetailResponseObj = giftDetailResponse;
        
    giftPocketStr = giftDetailResponseObj.pocketGift;
        
    if(_giftType == kSelectedTypeMyGift){
   [self showViewForMyGiftsView:FALSE];
    }
        
    [self showGiftDetailInfoOnView:giftDetailResponse];
        
    }
    }];
}

-(GiftDetailCriteria *)criteria{
    GiftDetailCriteria *criteria = [[GiftDetailCriteria alloc]init];
    criteria.giftDealID = _giftID;
    CLLocation *location = [CurrentLocation sharedLocation].currentLocation;
    criteria.userLat = [NSString stringWithFormat:@"%f",location.coordinate.latitude];
    criteria.userLong = [NSString stringWithFormat:@"%f",location.coordinate.longitude];
    
    return criteria;
    
}

#pragma mark - View For My GIfts

-(void)showGiftDetailInfoOnView:(GiftDetailResponse *)objGiftDetailResponse{
    
 //   giftPocketStr  = objGiftDetailResponse.pocketGift ;
    
    offerWebView.frame = CGRectMake(-15, 0, self.view.frame.size.width - 8, 80);

    [offerWebView loadHTMLString:objGiftDetailResponse.offerInfoHTML baseURL:nil];
    NSLog(@"String length :%f",offerWebView.scrollView.contentSize.height);
    
    for(int i=0;i<objGiftDetailResponse.offerDetailArray.count;i++){
    GiftOfferDetailList *objOfferDetailList = [objGiftDetailResponse.offerDetailArray objectAtIndex:i];
    actualPriceStr = objOfferDetailList.actualPrice;
    fromVouchers = objOfferDetailList.byVouchers;
    giftStatusStr = objOfferDetailList.status;

     
    if([fromVouchers intValue] == 1){
    
        NSLog(@"objOfferDetailList.offerPrice :%@ ",objOfferDetailList.offerPrice);
        
    isOfferPriceFound = TRUE;
    offerPriceLabel.text = [NSString stringWithFormat:@"%@",objOfferDetailList.offerPrice];
        
    NSLog(@"offerPriceLabel :%@ ",offerPriceLabel.text);
        
        
    if(offerPriceLabel.text.length > 3){
    rupeeImageView.frame = CGRectMake(plusImageView.frame.origin.x - 90, giftImageView.frame.origin.y + giftImageView.frame.size.height + 83, 20, 20);
    offerPriceLabel.frame = CGRectMake(plusImageView.frame.origin.x - 70 , giftImageView.frame.origin.y + giftImageView.frame.size.height + 83, 55, 21);
    plusImageView.frame = CGRectMake(offerPriceLabel.frame.origin.x + offerPriceLabel.frame.size.width - 12 , giftImageView.frame.origin.y + giftImageView.frame.size.height + 92, 8, 8);
       // offerPriceLabel.backgroundColor = [UIColor redColor];
    }
    else if(offerPriceLabel.text.length == 3){
    rupeeImageView.frame = CGRectMake(plusImageView.frame.origin.x - 85, giftImageView.frame.origin.y + giftImageView.frame.size.height + 83, 20, 20);
    offerPriceLabel.frame = CGRectMake(plusImageView.frame.origin.x - 65 , giftImageView.frame.origin.y + giftImageView.frame.size.height + 83, 50, 21);
    plusImageView.frame = CGRectMake(offerPriceLabel.frame.origin.x + offerPriceLabel.frame.size.width - 15 , giftImageView.frame.origin.y + giftImageView.frame.size.height + 92, 8, 8);
    }
        
    else{
    rupeeImageView.frame = CGRectMake(plusImageView.frame.origin.x - 75, giftImageView.frame.origin.y + giftImageView.frame.size.height + 83, 20, 20);
    offerPriceLabel.frame = CGRectMake(plusImageView.frame.origin.x - 55 , giftImageView.frame.origin.y + giftImageView.frame.size.height + 83, 30, 21);
    plusImageView.frame = CGRectMake(offerPriceLabel.frame.origin.x + offerPriceLabel.frame.size.width - 5 , giftImageView.frame.origin.y + giftImageView.frame.size.height + 92, 8, 8);
        

    }
    
    [giftDescContainerView addSubview:rupeeImageView];
    [giftDescContainerView addSubview:offerPriceLabel];
    [giftDescContainerView addSubview:plusImageView];

    }
    else{
    

    }
        
        
    if([objOfferDetailList.review isEqualToString:@"1"]){
    [rateButton setBackgroundImage:[UIImage imageNamed:@"ic_loveit_select.png"] forState:UIControlStateNormal];
    }
    if([objOfferDetailList.review isEqualToString:@"2"]){
    [rateButton setBackgroundImage:[UIImage imageNamed:@"ic_likeit_select.png"] forState:UIControlStateNormal];
    }
        
    if([objOfferDetailList.review isEqualToString:@"3"]){
    [rateButton setBackgroundImage:[UIImage imageNamed:@"ic_justok_select.png"] forState:UIControlStateNormal];
    }
        
    if([objOfferDetailList.review isEqualToString:@"4"]){
    [rateButton setBackgroundImage:[UIImage imageNamed:@"ic_poor_select.png"] forState:UIControlStateNormal];
    }
        
    if([objOfferDetailList.review isEqualToString:@"5"]){
    [rateButton setBackgroundImage:[UIImage imageNamed:@"ic_itsbad_select.png"] forState:UIControlStateNormal];
    }
    }
    
   // NSLog(@"#Location ARray count :%d",objGiftDetailResponse.locationDetailArray.count);
    
    
    if(objGiftDetailResponse.locationDetailArray.count == 0){
    callButton.hidden = TRUE;
    giftAddressLabel.hidden = TRUE;
    mapButton.hidden = TRUE;
    }
    
    
    else{
    callButton.hidden = FALSE;
    giftAddressLabel.hidden = FALSE;
    mapButton.hidden = FALSE;
        
        
        if(![CLLocationManager locationServicesEnabled]){
            
            NSLog(@"Location Services Enabled");
            
            isLocationNotFound = TRUE;

            
            if([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied){
             UIAlertView *   alert = [[UIAlertView alloc] initWithTitle:@"App Permission Denied"
                                                   message:@"To re-enable, please go to Settings and turn on Location Service for this app."
                                                  delegate:nil
                                         cancelButtonTitle:@"OK"
                                         otherButtonTitles:nil];
                
                alert.delegate = self;
                [alert show];
                
            }
        }
        
        else{
            isLocationNotFound = FALSE;
    NSMutableArray *distanceArray = [[NSMutableArray alloc]init];
    CLLocation *location = [CurrentLocation sharedLocation].currentLocation;
    for(int i=0;i<giftDetailResponseObj.locationDetailArray.count;i++){
    GiftLocationList *objLocList = [giftDetailResponseObj.locationDetailArray objectAtIndex:i];
            
        
    NSString *netDistance = [Constant calculateDistanceBetweenTwoPoints:objLocList.locationLat firstLong:objLocList.locationLong secondLat:[NSString stringWithFormat:@"%f",location.coordinate.latitude] secondLong:[NSString stringWithFormat:@"%f",location.coordinate.longitude]];
            
//    NSLog(@"#TheNetDistance :%@ %@",netDistance,objLocList.area);
    
    float shopDistance = [netDistance floatValue];
        
   
   NSMutableDictionary *  distanceDic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:objLocList.area,@"distanceArea", [NSNumber numberWithFloat:shopDistance],@"distance",nil];
      
    [ distanceArray addObject:distanceDic];
        
    }
    NSArray *array = [NSArray arrayWithArray:[distanceArray mutableCopy]];

        
    NSSortDescriptor *sortDescriptor;
        
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"distance" ascending:YES];
        
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        
    array = [array sortedArrayUsingDescriptors:sortDescriptors];
        
  //  NSLog(@"#TheDistanceArrayis :%@",array);
        
        

        
    for(NSMutableDictionary *dic in array){
        
  //  NSLog(@"#Dic :%@",dic);
  
  //  NSLog(@"#Values :%@ %@",[dic objectForKey:@"distanceArea"],[dic objectForKey:@"distance"]);
   
    if([[dic objectForKey:@"distance"] floatValue] > 1)
    giftAddressLabel.text = [NSString stringWithFormat:@"%@ : %@ km",[dic objectForKey:@"distanceArea"],[dic objectForKey:@"distance"]];
    else
    giftAddressLabel.text = [NSString stringWithFormat:@"%@ : %@ km",[dic objectForKey:@"distanceArea"],@"< 1"];
        
    break;
    

    }


    }
    }
    
    
    
    if([giftPocketStr intValue] == 1){
    isForRedeem = TRUE;
    getITButton.backgroundColor = [UIColor colorWithRed:184/255.0 green:185/255.0 blue:184/255.0 alpha:1];
    [getITButton setTitle:@"REDEEM NOW" forState:UIControlStateNormal];
    }
    
   else if([giftPocketStr intValue] == 2){
    getITButton.backgroundColor = [UIColor colorWithRed:184/255.0 green:185/255.0 blue:184/255.0 alpha:1];
    [getITButton setTitle:@"REDEEMED" forState:UIControlStateNormal];
    }
    
   else if([giftPocketStr intValue] == 3){
    getITButton.backgroundColor = [UIColor colorWithRed:184/255.0 green:185/255.0 blue:184/255.0 alpha:1];
    [getITButton setTitle:@"EXPIRED" forState:UIControlStateNormal];
    }
    else if(_giftType != kSelectedTypeMyGift){
    [getITButton setTitle:@"GET IT NOW" forState:UIControlStateNormal];
    getITButton.backgroundColor = [UIColor redColor];
    }
    else{
    [getITButton setTitle:@"In your pocket" forState:UIControlStateNormal];
    getITButton.backgroundColor = [UIColor colorWithRed:184/255.0 green:185/255.0 blue:184/255.0 alpha:1];
    }
    
    [self showGiftExtraViewOnSuperView:giftPocketStr];
    
}


-(void)showPurchasedGiftDetailInfoOnView:(GiftDetailResponse *)objGiftDetailResponse{
    
    //   giftPocketStr  = objGiftDetailResponse.pocketGift ;
    
    offerWebView.frame = CGRectMake(-15, 0, self.view.frame.size.width - 8, 80);
    
    [offerWebView loadHTMLString:objGiftDetailResponse.offerInfoHTML baseURL:nil];
    NSLog(@"String length :%f",offerWebView.scrollView.contentSize.height);
    
   /* for(int i=0;i<objGiftDetailResponse.offerDetailArray.count;i++){
        GiftOfferDetailList *objOfferDetailList = [objGiftDetailResponse.offerDetailArray objectAtIndex:i];
        actualPriceStr = objOfferDetailList.actualPrice;
        fromVouchers = objOfferDetailList.byVouchers;
        giftStatusStr = objOfferDetailList.status;
        
        
        if([fromVouchers intValue] == 1){
            
            
            isOfferPriceFound = TRUE;
            offerPriceLabel.text = [NSString stringWithFormat:@"%@",objOfferDetailList.offerPrice];
            
            [giftDescContainerView addSubview:rupeeImageView];
            [giftDescContainerView addSubview:offerPriceLabel];
            [giftDescContainerView addSubview:plusImageView];
            
        }
        else{
            
            
        }
        
        
        if([objOfferDetailList.review isEqualToString:@"1"]){
            [rateButton setBackgroundImage:[UIImage imageNamed:@"ic_loveit_select.png"] forState:UIControlStateNormal];
        }
        if([objOfferDetailList.review isEqualToString:@"2"]){
            [rateButton setBackgroundImage:[UIImage imageNamed:@"ic_likeit_select.png"] forState:UIControlStateNormal];
        }
        
        if([objOfferDetailList.review isEqualToString:@"3"]){
            [rateButton setBackgroundImage:[UIImage imageNamed:@"ic_justok_select.png"] forState:UIControlStateNormal];
        }
        
        if([objOfferDetailList.review isEqualToString:@"4"]){
            [rateButton setBackgroundImage:[UIImage imageNamed:@"ic_poor_select.png"] forState:UIControlStateNormal];
        }
        
        if([objOfferDetailList.review isEqualToString:@"5"]){
            [rateButton setBackgroundImage:[UIImage imageNamed:@"ic_itsbad_select.png"] forState:UIControlStateNormal];
        }
    }
    
    // NSLog(@"#Location ARray count :%d",objGiftDetailResponse.locationDetailArray.count);
    
    
    if(objGiftDetailResponse.locationDetailArray.count == 0){
        callButton.hidden = TRUE;
        giftAddressLabel.hidden = TRUE;
        mapButton.hidden = TRUE;
    }
    
    
    else{
        callButton.hidden = FALSE;
        giftAddressLabel.hidden = FALSE;
        mapButton.hidden = FALSE;
        
        
        if(![CLLocationManager locationServicesEnabled]){
            
            NSLog(@"Location Services Enabled");
            
            if([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied){
                UIAlertView *   alert = [[UIAlertView alloc] initWithTitle:@"App Permission Denied"
                                                                   message:@"To re-enable, please go to Settings and turn on Location Service for this app."
                                                                  delegate:nil
                                                         cancelButtonTitle:@"OK"
                                                         otherButtonTitles:nil];
                
                alert.delegate = self;
                [alert show];
                
                isLocationNotFound = TRUE;
            }
        }
        
        else{
            isLocationNotFound = FALSE;
            NSMutableArray *distanceArray = [[NSMutableArray alloc]init];
            CLLocation *location = [CurrentLocation sharedLocation].currentLocation;
            for(int i=0;i<giftDetailResponseObj.locationDetailArray.count;i++){
                GiftLocationList *objLocList = [giftDetailResponseObj.locationDetailArray objectAtIndex:i];
                
                
                NSString *netDistance = [Constant calculateDistanceBetweenTwoPoints:objLocList.locationLat firstLong:objLocList.locationLong secondLat:[NSString stringWithFormat:@"%f",location.coordinate.latitude] secondLong:[NSString stringWithFormat:@"%f",location.coordinate.longitude]];
                
                //    NSLog(@"#TheNetDistance :%@ %@",netDistance,objLocList.area);
                
                float shopDistance = [netDistance floatValue];
                
                
                NSMutableDictionary *  distanceDic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:objLocList.area,@"distanceArea", [NSNumber numberWithFloat:shopDistance],@"distance",nil];
                
                [ distanceArray addObject:distanceDic];
                
            }
            NSArray *array = [NSArray arrayWithArray:[distanceArray mutableCopy]];
            
            
            NSSortDescriptor *sortDescriptor;
            
            sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"distance" ascending:YES];
            
            NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
            
            array = [array sortedArrayUsingDescriptors:sortDescriptors];
            
            //  NSLog(@"#TheDistanceArrayis :%@",array);
            
            
            
            
            for(NSMutableDictionary *dic in array){
                
                //  NSLog(@"#Dic :%@",dic);
                
                //  NSLog(@"#Values :%@ %@",[dic objectForKey:@"distanceArea"],[dic objectForKey:@"distance"]);
                
                if([[dic objectForKey:@"distance"] floatValue] > 1)
                    giftAddressLabel.text = [NSString stringWithFormat:@"%@ : %@ km",[dic objectForKey:@"distanceArea"],[dic objectForKey:@"distance"]];
                else
                    giftAddressLabel.text = [NSString stringWithFormat:@"%@ : %@ km",[dic objectForKey:@"distanceArea"],@"< 1"];
                
                break;
                
                
            }
            
            
        }
    }*/
    
    
    
   /* if([giftPocketStr intValue] == 1){
        isForRedeem = TRUE;
        getITButton.backgroundColor = [UIColor colorWithRed:184/255.0 green:185/255.0 blue:184/255.0 alpha:1];
        [getITButton setTitle:@"REDEEM NOW" forState:UIControlStateNormal];
    }
    
    else if([giftPocketStr intValue] == 2){
        getITButton.backgroundColor = [UIColor colorWithRed:184/255.0 green:185/255.0 blue:184/255.0 alpha:1];
        [getITButton setTitle:@"REDEEMED" forState:UIControlStateNormal];
    }
    
    else if([giftPocketStr intValue] == 3){
        getITButton.backgroundColor = [UIColor colorWithRed:184/255.0 green:185/255.0 blue:184/255.0 alpha:1];
        [getITButton setTitle:@"EXPIRED" forState:UIControlStateNormal];
    }
    else if(_giftType != kSelectedTypeMyGift){
        [getITButton setTitle:@"GET IT NOW" forState:UIControlStateNormal];
        getITButton.backgroundColor = [UIColor redColor];
    }
    else{
        [getITButton setTitle:@"In your pocket" forState:UIControlStateNormal];
        getITButton.backgroundColor = [UIColor colorWithRed:184/255.0 green:185/255.0 blue:184/255.0 alpha:1];
    }
    
    [self showGiftExtraViewOnSuperView:giftPocketStr];*/
    
}




-(IBAction)moreClicked:(id)sender{

    if(isMoreClicked == FALSE){
    [offerDescWebView loadHTMLString:[self addStyleInNotesText:giftDetailResponseObj.contentText] baseURL:nil];
        
    offerDescWebView.frame = CGRectMake(-15, offerWebView.frame.origin.y + offerWebView.frame.size.height - 20, self.view.frame.size.width - 8, 10);

    
    NSLog(@"Length :%lu----------%lu",(unsigned long)[self addStyleInNotesText:giftDetailResponseObj.contentText].length,(unsigned long)giftDetailResponseObj.contentText.length);
        
      

    [detailInfoView addSubview:offerDescWebView];
    [UIView beginAnimations:@"animationOff" context:NULL];
    [UIView setAnimationDuration:0.1f];
    [UIView commitAnimations];
    
    [self removeViewFromSuperView];

   }
    
    
    
    else{
    [offerDescWebView removeFromSuperview];
    [UIView beginAnimations:@"animationOff" context:NULL];
    [UIView setAnimationDuration:0.1f];
    [UIView commitAnimations];
        
        
    if(isGiftFound == TRUE || _giftType == kSelectedTypeMyGift){
    detailInfoView.frame = CGRectMake(0, offerRedView.frame.origin.y + offerRedView.frame.size.height+ 20, self.view.frame.size.width, 10);
    }
        
    detailInfoView.frame = CGRectMake(0, offerRedView.frame.origin.y + offerRedView.frame.size.height+ 20, self.view.frame.size.width, offerWebView.frame.origin.y + offerWebView.frame.size.height + 50);
        
        
    if(giftDetailResponseObj.companyURL.length > 0){
    urlSepView1.frame = CGRectMake(0, detailInfoView.frame.origin.y + detailInfoView.frame.size.height, self.view.frame.size.width, 1);
    urlSepView.frame = CGRectMake(0, urlSepView1.frame.origin.y + urlSepView1.frame.size.height, self.view.frame.size.width, 12);
    urlSepView2.frame = CGRectMake(0, urlSepView.frame.origin.y + urlSepView.frame.size.height, self.view.frame.size.width, 1);
        
    urlDataView.frame = CGRectMake(0,  urlSepView2.frame.origin.y + urlSepView2.frame.size.height, self.view.frame.size.width, 70);
    urlDataView.backgroundColor = [UIColor whiteColor];
        
    mapSep1View.frame = CGRectMake(0, urlDataView.frame.origin.y + urlDataView.frame.size.height, self.view.frame.size.width, 1);
    secondSepView.frame = CGRectMake(0, mapSep1View.frame.origin.y + mapSep1View.frame.size.height, self.view.frame.size.width, 12);
    mapSep2View.frame = CGRectMake(0, secondSepView.frame.origin.y + secondSepView.frame.size.height, self.view.frame.size.width, 1);
        
    }
        
        
    if(giftDetailResponseObj.brandDescription.length > 0){
            
    if(giftDetailResponseObj.companyURL.length > 0)
    brandDescSepView1.frame  = CGRectMake(0, urlDataView.frame.origin.y + urlDataView.frame.size.height, self.view.frame.size.width, 1);
            
    else
    brandDescSepView1.frame  = CGRectMake(0, detailInfoView.frame.origin.y + detailInfoView.frame.size.height, self.view.frame.size.width, 1);
            
            
    brandDescSepView1.backgroundColor = [UIColor darkGrayColor];
    [scrollView addSubview:brandDescSepView1];
            
    brandDescSepView.frame = CGRectMake(0, brandDescSepView1.frame.origin.y + brandDescSepView1.frame.size.height, self.view.frame.size.width, 12);
    brandDescSepView.backgroundColor = [UIColor colorWithRed:236/255.0 green:236/255.0 blue:236/255.0 alpha:1];
    [scrollView addSubview:brandDescSepView];
            
            
    brandDescSepView2.frame = CGRectMake(0,  brandDescSepView.frame.origin.y + brandDescSepView.frame.size.height, self.view.frame.size.width, 1);
    brandDescSepView2.backgroundColor = [UIColor darkGrayColor];
    [scrollView addSubview:brandDescSepView2];
        
   // brandDescDataView = [[UIView alloc]initWithFrame:CGRectMake(0, brandDescSepView2.frame.origin.y + brandDescSepView2.frame.size.height, self.view.frame.size.width,giftDetailResponseObj.brandDescription.length)];
        
        
        if(giftDetailResponseObj.brandDescription.length > 200)
        brandDescDataView.frame = CGRectMake(0, brandDescSepView2.frame.origin.y + brandDescSepView2.frame.size.height, self.view.frame.size.width, giftDetailResponseObj.brandDescription.length - 90);
        else
        brandDescDataView.frame = CGRectMake(0, brandDescSepView2.frame.origin.y + brandDescSepView2.frame.size.height, self.view.frame.size.width, giftDetailResponseObj.brandDescription.length);
        

    UILabel *companyTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(5, 20, 250, 21)];
    companyTitleLabel.text = @"ABOUT THE COMPANY";
        [self setTextOnLabel:companyTitleLabel fontSize:18.0];

    [brandDescDataView addSubview:companyTitleLabel];
            
    UILabel *descLabel = [[UILabel alloc]initWithFrame:CGRectMake(5, companyTitleLabel.frame.origin.y + companyTitleLabel.frame.size.height , self.view.frame.size.width - 8, giftDetailResponseObj.brandDescription.length)];
        descLabel.text = giftDetailResponseObj.brandDescription;
        
        [self setTextOnLabel:descLabel fontSize:14.0];
            
    [brandDescDataView addSubview:descLabel];
            
            
            
    [scrollView addSubview:brandDescDataView];
            
            
            
            
    mapSep1View.frame = CGRectMake(0, brandDescDataView.frame.origin.y + brandDescDataView.frame.size.height, self.view.frame.size.width, 1);
    secondSepView.frame = CGRectMake(0, mapSep1View.frame.origin.y + mapSep1View.frame.size.height, self.view.frame.size.width, 12);
    mapSep2View.frame = CGRectMake(0, secondSepView.frame.origin.y + secondSepView.frame.size.height, self.view.frame.size.width, 1);
            
    }
        
        
    if(giftDetailResponseObj.companyURL.length == 0 && giftDetailResponseObj.brandDescription.length == 0){
    mapSep1View.frame = CGRectMake(0, detailInfoView.frame.origin.y + detailInfoView.frame.size.height, self.view.frame.size.width, 1);
    secondSepView.frame = CGRectMake(0, mapSep1View.frame.origin.y + mapSep1View.frame.size.height, self.view.frame.size.width, 12);
    mapSep2View.frame = CGRectMake(0, secondSepView.frame.origin.y + secondSepView.frame.size.height, self.view.frame.size.width, 1);
    }
        
        
    moreView.frame = CGRectMake(0,offerWebView.frame.origin.y + offerWebView.frame.size.height  , self.view.frame.size.width, 30);
        
    if([UIScreen mainScreen].bounds.size.width == 320){
    moreButton.frame = CGRectMake(245, -1, 45, 30);
    dropButton.frame = CGRectMake(moreButton.frame.origin.x + moreButton.frame.size.width , 12, 10, 10);
    }
        
        
    [moreButton setTitle:@"MORE" forState:UIControlStateNormal];
    [dropButton setBackgroundImage:[UIImage imageNamed:@"drop_more.png"] forState:UIControlStateNormal];


    if(giftDetailResponseObj.locationDetailArray.count > 0)
    [self showMapViewOnViewWhenMoreIsNotClicked];
    else{
    scrollView.contentSize = CGSizeMake(self.view.frame.size.width, mapSep2View.frame.origin.y + mapSep2View.frame.size.height + 100);
    }
   
    
        
    }
    
    isMoreClicked = !isMoreClicked;
}

#pragma mark - MapView on View

-(void)showMapViewOnViewWhenMoreIsNotClicked{

    
    shopNameLabel = [[UILabel alloc]initWithFrame:CGRectMake(10,  5,  self.view.frame.size.width - 100, 21)];

    shopNameLabel.text =  shopNameStr;//@"ZASH RESTRO";
    shopNameLabel.font = [UIFont fontWithName:@"OpenSans-Regular" size:17.0];
    shopNameLabel.textColor = [UIColor darkTextColor];
    [mapContainerView addSubview:shopNameLabel];
    
    UIButton *shopAddressIconBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    shopAddressIconBtn.frame = CGRectMake(5, shopNameLabel.frame.origin.y + shopNameLabel.frame.size.height +5, 12, 15);
    [shopAddressIconBtn setBackgroundImage:[UIImage imageNamed:@"gift_locator.png"] forState:UIControlStateNormal];
    [mapContainerView addSubview:shopAddressIconBtn];
    
    GiftLocationList *objLocList = [giftDetailResponseObj.locationDetailArray objectAtIndex:0];
    shopAddressLabel = [[UILabel alloc]initWithFrame:CGRectMake(shopAddressIconBtn.frame.origin.x + shopAddressIconBtn.frame.size.width + 5, shopNameLabel.frame.origin.y + shopNameLabel.frame.size.height +5 , self.view.frame.size.width - 50, 50)];
    shopAddressLabel.text = objLocList.fullAddress; // @"shop no-7, seva sadan 27, next to laxmi dairy, d.j road vile parle west,mumbai";
    shopAddressLabel.font = [UIFont fontWithName:@"Open Sans" size:12.0];
    shopAddressLabel.textColor = [UIColor darkGrayColor];

    shopAddressLabel.numberOfLines = 0;
    [shopAddressLabel sizeToFit];
    [mapContainerView addSubview:shopAddressLabel];
   
    
    NSLog(@"NumberOfLine :%ld",(long)shopAddressLabel.text.length);


   for(int i=0;i<giftDetailResponseObj.locationDetailArray.count;i++){
    GiftLocationList *objLocList = [giftDetailResponseObj.locationDetailArray objectAtIndex:0];

    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[objLocList.locationLat floatValue] longitude:[objLocList.locationLong floatValue] zoom:13];
    mapView = [GMSMapView mapWithFrame:CGRectMake(0, shopAddressLabel.frame.origin.y + shopAddressLabel.frame.size.height +10, self.view.frame.size.width,180) camera:camera];
    mapView.myLocationEnabled = YES;
    mapView.delegate = self;
    mapView.settings.compassButton = NO;
    mapView.settings.scrollGestures = NO;
    double latitude1 = [objLocList.locationLat doubleValue];
    double longitude1 = [objLocList.locationLong doubleValue];
       
   
       
       
       
    CLLocationCoordinate2D position = CLLocationCoordinate2DMake(latitude1,longitude1);
    GMSMarker *marker = [[GMSMarker alloc]init];
    marker.position = position;
    marker.flat = NO;
    marker.map = mapView;
   }
    [mapContainerView addSubview:mapView];
    
    if(giftDetailResponseObj.locationDetailArray.count >1){
        
    UIButton *moreLocationButton = [UIButton buttonWithType:UIButtonTypeCustom];
    moreLocationButton = [[UIButton alloc]initWithFrame:CGRectMake(5, mapView.frame.origin.y + mapView.frame.size.height + 3, 150, 21)];
    NSString *btnText = [NSString stringWithFormat:@"%lu %@",giftDetailResponseObj.locationDetailArray.count - 1,@"MORE LOCATIONS"];
   // NSString *btnText = [NSString stringWithFormat:@"%d %@",3,@"MORE LOCATIONS"];

    [moreLocationButton setTitle:btnText forState:UIControlStateNormal];
    [moreLocationButton setTitleColor:[UIColor darkGrayColor]forState:UIControlStateNormal];
    moreLocationButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    moreLocationButton.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);

    moreLocationButton.titleLabel.font =  [UIFont fontWithName:@"Open Sans" size:13.0];
        
    [moreLocationButton addTarget:self action:@selector(moreLocationCLicked:) forControlEvents:UIControlEventTouchUpInside];


    [mapContainerView addSubview:moreLocationButton];
    mapContainerView.frame = CGRectMake(0, mapSep2View.frame.origin.y + mapSep2View.frame.size.height, self.view.frame.size.width, 290);

    }
    else{
    if(shopAddressLabel.text.length >80){
    mapContainerView.frame = CGRectMake(0, mapSep2View.frame.origin.y + mapSep2View.frame.size.height, self.view.frame.size.width, 280);
        
    }
    else
    mapContainerView.frame = CGRectMake(0, mapSep2View.frame.origin.y + mapSep2View.frame.size.height, self.view.frame.size.width, 260);
    }
    
    last1SepView.frame = CGRectMake(0, mapContainerView.frame.origin.y + mapContainerView.frame.size.height +3, self.view.frame.size.width, 1);
    last1SepView.backgroundColor = [UIColor lightGrayColor];
    thirdSepView.frame = CGRectMake(0, last1SepView.frame.origin.y + last1SepView.frame.size.height, self.view.frame.size.width, 12);
    thirdSepView.backgroundColor = [UIColor colorWithRed:236/255.0 green:236/255.0 blue:236/255.0 alpha:1];

    last2SepView.frame = CGRectMake(0, thirdSepView.frame.origin.y + thirdSepView.frame.size.height, self.view.frame.size.width, 1);
    last2SepView.backgroundColor = [UIColor lightGrayColor];

    scrollView.contentSize = CGSizeMake(self.view.frame.size.width, last2SepView.frame.origin.y + last2SepView.frame.size.height + 100);
    
    
    if(giftDetailResponseObj.imageListArray.count > 0){
       
    photoTextLabel = [[UILabel alloc]initWithFrame:CGRectMake(100, last2SepView.frame.origin.y + last2SepView.frame.size.height + 15, 100, 21)];
    photoTextLabel.text = @"PHOTOS";
    photoTextLabel.textAlignment = NSTextAlignmentCenter;
    photoTextLabel.font = [UIFont fontWithName:@"Open Sans" size:20.0];

    [scrollView addSubview:photoTextLabel];
        
    photoSepView = [[UIView alloc]initWithFrame:CGRectMake(0, photoTextLabel.frame.origin.y + photoTextLabel.frame.size.height + 20, self.view.frame.size.width, 1)];
    photoSepView.backgroundColor = [UIColor redColor];
    [scrollView addSubview:photoSepView];
        
    photoContainerView = [[UIView alloc]initWithFrame:CGRectMake(0, photoSepView.frame.origin.y + photoSepView.frame.size.height , self.view.frame.size.width, 90)];
    float x = 10;

    for(int i=0;i<giftDetailResponseObj.imageListArray.count;i++){
    photoImageView = [[UIImageView alloc]initWithFrame:CGRectMake(x, 10, 70, 70)];
    photoImageView.userInteractionEnabled = TRUE;
    [photoImageView setImageWithURL:[NSURL URLWithString:[giftDetailResponseObj.imageListArray objectAtIndex:i]] placeholderImage:[UIImage imageNamed:@"deal_default.png"]completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
    UIImage *newImage = [UIImage imageWithImage:image  scaledToSize:CGSizeMake(320,212)];
    photoImageView.image = newImage;
    }usingActivityIndicatorStyle:(UIActivityIndicatorViewStyle)UIActivityIndicatorViewStyleGray];
     
        
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(imageTapped:)];
    tapGesture.delegate = self;
        photoImageView.tag = i ;//[[giftDetailResponseObj.imageListArray objectAtIndex:i]intValue];
    [photoImageView addGestureRecognizer:tapGesture];
            
    photoImageView.backgroundColor = [UIColor blackColor];
    [photoContainerView addSubview:photoImageView];
        
    
        
    x+= 100;
    }
    [scrollView addSubview:photoContainerView];
        
        
    photoLastSepView1 = [[UIView alloc]initWithFrame:CGRectMake(0,  photoContainerView.frame.origin.y + photoContainerView.frame.size.height, self.view.frame.size.width, 1)];
    photoLastSepView1.backgroundColor = [UIColor darkGrayColor];
    [scrollView addSubview:photoLastSepView1];
        
        
    photoLastSepView = [[UIView alloc]initWithFrame:CGRectMake(0, photoLastSepView1.frame.origin.y + photoLastSepView1.frame.size.height, self.view.frame.size.width, 15)];
    photoLastSepView.backgroundColor = [UIColor colorWithRed:236/255.0 green:236/255.0 blue:236/255.0 alpha:1];
    [scrollView addSubview:photoLastSepView];
        
    photoLastSepView2 = [[UIView alloc]initWithFrame:CGRectMake(0,  photoLastSepView.frame.origin.y + photoLastSepView.frame.size.height, self.view.frame.size.width, 1)];
    photoLastSepView2.backgroundColor = [UIColor darkGrayColor];
    [scrollView addSubview:photoLastSepView2];
        
    scrollView.contentSize = CGSizeMake(self.view.frame.size.width, photoLastSepView2.frame.origin.y + photoLastSepView2.frame.size.height + 60);
    }
    
    //giftBottomView.frame = CGRectMake(0, scrollView.frame.origin.y + scrollView.frame.size.height, self.view.frame.size.width, 55);
}


-(void)removeViewFromSuperView{
    if(isMoreClicked == FALSE){
    [shopAddressLabel removeFromSuperview];
    [shopNameLabel removeFromSuperview];
    [photoTextLabel removeFromSuperview];
    [photoSepView removeFromSuperview];
    [photoContainerView removeFromSuperview];
    [photoLastSepView removeFromSuperview];
    [photoLastSepView1 removeFromSuperview];
    [photoLastSepView2 removeFromSuperview];

    }
}


-(void)moreLocationCLicked:(id)sender{
    GiftDetailMapViewController *giftMapVC = [self.storyboard instantiateViewControllerWithIdentifier:@"giftDetailMapC"];
    giftMapVC.giftDetailArray = giftDetailResponseObj.locationDetailArray; //locationDetailArrayList;
    giftMapVC.objGiftType = _giftType;
    giftMapVC.thumbImage = thumbImageView.image;
    giftMapVC.shopName = shopNameStr;
    [self.navigationController pushViewController:giftMapVC animated:YES];
 
}


#pragma mark - Google Maps View Gesture Method



- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate{
    NSLog(@"#HeyTheMapHasTapped %@ %@",thumbImageView.imageURL,thumbImageView.image);
    
        
    if(giftDetailResponseObj.locationDetailArray.count >1){
    GiftDetailMapViewController *giftMapVC = [self.storyboard instantiateViewControllerWithIdentifier:@"giftDetailMapC"];
    giftMapVC.giftDetailArray = giftDetailResponseObj.locationDetailArray; //locationDetailArrayList;
    giftMapVC.objGiftType = _giftType;
    giftMapVC.thumbImage = thumbImageView.image;
    giftMapVC.shopName = shopNameStr;
    [self.navigationController pushViewController:giftMapVC animated:YES];
    }
    
    else{
    GiftSingleViewController *giftSingleMapVC = [self.storyboard instantiateViewControllerWithIdentifier:@"GiftSingleMapVC"];
    GiftLocationList *objGiftLocList = [giftDetailResponseObj.locationDetailArray objectAtIndex:0];
        
    giftSingleMapVC.locationLat = objGiftLocList.locationLat;
    giftSingleMapVC.locationLong = objGiftLocList.locationLong;
    giftSingleMapVC.locationAddress = objGiftLocList.address;
        
    NSLog(@"Gift Detail Info :%@ %@ %@ %@ %@ %@",giftSingleMapVC.locationLat,giftSingleMapVC.locationLong,giftSingleMapVC.locationAddress,objGiftLocList.locationLat,objGiftLocList.locationLong,objGiftLocList.address);
        
    giftSingleMapVC.objGiftType = _giftType;
    giftSingleMapVC.thumbImage = thumbImageView.image;
    giftSingleMapVC.shopName = shopNameStr;
    [self.navigationController pushViewController:giftSingleMapVC animated:YES];
    }
    
    
}

-(NSMutableString *)addStyleInNotesText:(NSString *)notesText{
    
    NSLog(@"#ServerNoteText :%@",notesText);
    
    notesText = [notesText stringByReplacingOccurrencesOfString:@"<p><span style=\"font-size:12px\">"  withString:@"<p style=\"padding-right:5px;padding-left:25px;\"><span style=\"font-size:14px\">"];
    
    notesText = [notesText stringByReplacingOccurrencesOfString:@"<h2>Note:" withString:@"<h2 style=\"padding-right:25px;padding-left:25px;\">Note:"];
    
    NSLog(@"notesText :%@",notesText);

    
    NSMutableString *notesTextStr = [NSMutableString stringWithString:notesText];

    
    return notesTextStr;
}

#pragma mark - Image Tapped

-(void)imageTapped:(UIGestureRecognizer *)gesRecognizer{
    NSLog(@"ImageURL : %ld%@",(long)gesRecognizer.view.tag,[giftDetailResponseObj.imageListArray objectAtIndex:gesRecognizer.view.tag]);
    [self.navigationController setNavigationBarHidden:TRUE];
    
    objDetailImageView = [[DetailImageView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    [self.view addSubview:objDetailImageView];
    
    [objDetailImageView setImageOnDetailView:[giftDetailResponseObj imageListArray] tagValue:gesRecognizer.view.tag];

    
    [objDetailImageView setOnButtonClick:^{
    [objDetailImageView removeFromSuperview];
    [self.navigationController setNavigationBarHidden:FALSE];

    }];
    
}

#pragma mark - WebView Methods
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    if(webView == offerWebView){
    CGRect frame = webView.frame;
    
    frame.size.height = 5.0f;
    
    webView.frame = frame;
    }
    
    if(webView == offerDescWebView){
    CGRect frame = offerDescWebView.frame;
    frame.size.height = 10.0f;
    offerDescWebView.frame = frame;
    }
    
   
}


- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    if(webView == offerWebView){
    CGSize mWebViewTextSize = [webView sizeThatFits:CGSizeMake(1.0f, 1.0f)];  // Pass about any size
    
    CGRect mWebViewFrame = webView.frame;
    
    
    mWebViewFrame.size.height = mWebViewTextSize.height;
    
    webView.frame = mWebViewFrame;
    
    
    //Disable bouncing in webview
    for (id subview in webView.subviews)
    {
        if ([[subview class] isSubclassOfClass: [UIScrollView class]])
        {
            [subview setBounces:NO];
        }
    }
        detailInfoView.frame = CGRectMake(0, offerRedView.frame.origin.y + offerRedView.frame.size.height + 20, self.view.frame.size.width, offerWebView.frame.origin.y + offerWebView.frame.size.height + 50);
        moreView.frame = CGRectMake(0,offerWebView.frame.origin.y + offerWebView.frame.size.height  , self.view.frame.size.width -10, 30);
        
        
        if([UIScreen mainScreen].bounds.size.width == 320){
        moreButton.frame = CGRectMake(245, -1, 45, 30);
        dropButton.frame = CGRectMake(moreButton.frame.origin.x + moreButton.frame.size.width , 12, 10, 10);
        }
        
        if(giftDetailResponseObj.companyURL.length > 0){
        urlSepView1  = [[UIView alloc]initWithFrame:CGRectMake(0, detailInfoView.frame.origin.y + detailInfoView.frame.size.height, self.view.frame.size.width, 1)];
        
            
        urlSepView1.backgroundColor = [UIColor darkGrayColor];
        [scrollView addSubview:urlSepView1];
            

        urlSepView = [[UIView alloc]initWithFrame:CGRectMake(0, urlSepView1.frame.origin.y + urlSepView1.frame.size.height, self.view.frame.size.width, 12)];
        urlSepView.backgroundColor = [UIColor colorWithRed:236/255.0 green:236/255.0 blue:236/255.0 alpha:1];
        [scrollView addSubview:urlSepView];
            
        urlSepView2 = [[UIView alloc]initWithFrame:CGRectMake(0,  urlSepView.frame.origin.y + urlSepView.frame.size.height, self.view.frame.size.width, 1)];
        urlSepView2.backgroundColor = [UIColor darkGrayColor];
        [scrollView addSubview:urlSepView2];
            
            
        urlDataView = [[UIView alloc]initWithFrame:CGRectMake(0,  urlSepView2.frame.origin.y + urlSepView2.frame.size.height, self.view.frame.size.width, 70)];
        urlDataView.backgroundColor = [UIColor whiteColor];
            
        UILabel *urlLabel = [[UILabel alloc]initWithFrame:CGRectMake(5, 20, 180, 40)];
        urlLabel.text = _shopName;
        urlLabel.font = [UIFont fontWithName:@"Open Sans" size:20.0];
            
        [urlDataView addSubview:urlLabel];
            
            
        UIButton *urlButton = [UIButton buttonWithType:UIButtonTypeCustom];
        urlButton.frame = CGRectMake(self.view.frame.size.width - 120, 25, 100, 30);
        [urlButton setTitle:@"VISIT WEBSITE" forState:UIControlStateNormal];
        urlButton.titleLabel.font = [UIFont fontWithName:@"Open Sans" size:14.0];
        [urlButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [urlButton addTarget:self action:@selector(urlButtonCLick:) forControlEvents:UIControlEventTouchUpInside];
        [self setViewOfButton:urlButton];
        [urlDataView addSubview:urlButton];

            
        [scrollView addSubview:urlDataView];
            
            NSLog(@"CompDesc :%@",giftDetailResponseObj.brandDescription);
            
            
            
        mapSep1View.frame = CGRectMake(0, urlDataView.frame.origin.y + urlDataView.frame.size.height, self.view.frame.size.width, 1);
        secondSepView.frame = CGRectMake(0, mapSep1View.frame.origin.y + mapSep1View.frame.size.height, self.view.frame.size.width, 12);
        mapSep2View.frame = CGRectMake(0, secondSepView.frame.origin.y + secondSepView.frame.size.height, self.view.frame.size.width, 1);
            
        }
        
        if(giftDetailResponseObj.brandDescription.length > 0){
            
        if(giftDetailResponseObj.companyURL.length > 0)
        brandDescSepView1  = [[UIView alloc]initWithFrame:CGRectMake(0, urlDataView.frame.origin.y + urlDataView.frame.size.height, self.view.frame.size.width, 1)];
            
        else
        brandDescSepView1  = [[UIView alloc]initWithFrame:CGRectMake(0, detailInfoView.frame.origin.y + detailInfoView.frame.size.height, self.view.frame.size.width, 1)];
            
            
        brandDescSepView1.backgroundColor = [UIColor darkGrayColor];
        [scrollView addSubview:brandDescSepView1];
            
        brandDescSepView = [[UIView alloc]initWithFrame:CGRectMake(0, brandDescSepView1.frame.origin.y + brandDescSepView1.frame.size.height, self.view.frame.size.width, 12)];
        brandDescSepView.backgroundColor = [UIColor colorWithRed:236/255.0 green:236/255.0 blue:236/255.0 alpha:1];
        [scrollView addSubview:brandDescSepView];
            
            
        brandDescSepView2 = [[UIView alloc]initWithFrame:CGRectMake(0,  brandDescSepView.frame.origin.y + brandDescSepView.frame.size.height, self.view.frame.size.width, 1)];
        brandDescSepView2.backgroundColor = [UIColor darkGrayColor];
        [scrollView addSubview:brandDescSepView2];
            
            NSLog(@"Text height :%lu",(unsigned long)giftDetailResponseObj.brandDescription.length);
            
         
            if(giftDetailResponseObj.brandDescription.length > 200)
                brandDescDataView = [[UIView alloc]initWithFrame:CGRectMake(0, brandDescSepView2.frame.origin.y + brandDescSepView2.frame.size.height, self.view.frame.size.width,giftDetailResponseObj.brandDescription.length - 90)];
            else
                brandDescDataView = [[UIView alloc]initWithFrame:CGRectMake(0, brandDescSepView2.frame.origin.y + brandDescSepView2.frame.size.height, self.view.frame.size.width,giftDetailResponseObj.brandDescription.length)];
            
           
            //  brandDescDataView = [[UIView alloc]initWithFrame:CGRectMake(0, brandDescSepView2.frame.origin.y + brandDescSepView2.frame.size.height, self.view.frame.size.width,giftDetailResponseObj.brandDescription.length)];
            
            
            
            
            
        UILabel *companyTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(5, 20, 250, 21)];
        companyTitleLabel.text = @"ABOUT THE COMPANY";
        [self setTextOnLabel:companyTitleLabel fontSize:18.0];
        [brandDescDataView addSubview:companyTitleLabel];
            
        UILabel *descLabel = [[UILabel alloc]initWithFrame:CGRectMake(5, companyTitleLabel.frame.origin.y + companyTitleLabel.frame.size.height , self.view.frame.size.width - 8, giftDetailResponseObj.brandDescription.length)];
        descLabel.text = giftDetailResponseObj.brandDescription;
            [self setTextOnLabel:descLabel fontSize:14.0];

            
        [brandDescDataView addSubview:descLabel];
        

            
        [scrollView addSubview:brandDescDataView];
   
        

       
        mapSep1View.frame = CGRectMake(0, brandDescDataView.frame.origin.y + brandDescDataView.frame.size.height, self.view.frame.size.width, 1);
        secondSepView.frame = CGRectMake(0, mapSep1View.frame.origin.y + mapSep1View.frame.size.height, self.view.frame.size.width, 12);
        mapSep2View.frame = CGRectMake(0, secondSepView.frame.origin.y + secondSepView.frame.size.height, self.view.frame.size.width, 1);
            
        }
        
        
        
        
        
        
       
        if(giftDetailResponseObj.companyURL.length == 0 && giftDetailResponseObj.brandDescription.length == 0){
        mapSep1View.frame = CGRectMake(0, detailInfoView.frame.origin.y + detailInfoView.frame.size.height, self.view.frame.size.width, 1);
        secondSepView.frame = CGRectMake(0, mapSep1View.frame.origin.y + mapSep1View.frame.size.height, self.view.frame.size.width, 12);
        mapSep2View.frame = CGRectMake(0, secondSepView.frame.origin.y + secondSepView.frame.size.height, self.view.frame.size.width, 1);
        }

        if(giftDetailResponseObj.locationDetailArray.count > 0){
        [self showMapViewOnViewWhenMoreIsNotClicked];
        }
        
        else{
            
            scrollView.contentSize = CGSizeMake(self.view.frame.size.width, mapSep2View.frame.origin.y + mapSep2View.frame.size.height + 150);


        }
        
        
        [self hideHUDOnView];

    }
    
    if(webView == offerDescWebView){
        CGSize mWebViewTextSize = [offerDescWebView sizeThatFits:CGSizeMake(1.0f, 1.0f)];  // Pass about any size
        
        CGRect mWebViewFrame = offerDescWebView.frame;
        
        
        mWebViewFrame.size.height = mWebViewTextSize.height;
        
        offerDescWebView.frame = mWebViewFrame;
        
        
        //Disable bouncing in webview
        for (id subview in offerDescWebView.subviews)
        {
            if ([[subview class] isSubclassOfClass: [UIScrollView class]])
            {
                [subview setBounces:NO];
            }
        }
        detailInfoView.frame = CGRectMake(0, offerRedView.frame.origin.y + offerRedView.frame.size.height + 20, self.view.frame.size.width, offerDescWebView.frame.origin.y + offerDescWebView.frame.size.height + 50);
        

        moreView.frame = CGRectMake(0,offerDescWebView.frame.origin.y + offerDescWebView.frame.size.height  , self.view.frame.size.width, 30);
        dropButton.frame = CGRectMake(moreView.frame.origin.x + moreView.frame.size.width + 4, 113, 10, 10);

        if([UIScreen mainScreen].bounds.size.width == 320){
        moreButton.frame = CGRectMake(245, -1, 45, 30);
        dropButton.frame = CGRectMake(moreButton.frame.origin.x + moreButton.frame.size.width , 12, 10, 10);
        }
        
        
        [moreButton setTitle:@"LESS" forState:UIControlStateNormal];
        [dropButton setBackgroundImage:[UIImage imageNamed:@"drop_less.png"] forState:UIControlStateNormal];
        
        
        if(giftDetailResponseObj.companyURL.length > 0){
            
        urlSepView1.frame  = CGRectMake(0, detailInfoView.frame.origin.y + detailInfoView.frame.size.height, self.view.frame.size.width, 1);
        urlSepView1.backgroundColor = [UIColor darkGrayColor];
        [scrollView addSubview:urlSepView1];
            
            
        urlSepView.frame = CGRectMake(0, urlSepView1.frame.origin.y + urlSepView1.frame.size.height, self.view.frame.size.width, 12);
        urlSepView.backgroundColor = [UIColor colorWithRed:236/255.0 green:236/255.0 blue:236/255.0 alpha:1];
        [scrollView addSubview:urlSepView];
            
        urlSepView2.frame = CGRectMake(0,  urlSepView.frame.origin.y + urlSepView.frame.size.height, self.view.frame.size.width, 1);
        urlSepView2.backgroundColor = [UIColor darkGrayColor];
        [scrollView addSubview:urlSepView2];
            
            
        urlDataView.frame = CGRectMake(0,  urlSepView2.frame.origin.y + urlSepView2.frame.size.height, self.view.frame.size.width, 70);
        urlDataView.backgroundColor = [UIColor whiteColor];
            
        mapSep1View.frame = CGRectMake(0, urlDataView.frame.origin.y + urlDataView.frame.size.height, self.view.frame.size.width, 1);
        secondSepView.frame = CGRectMake(0, mapSep1View.frame.origin.y + mapSep1View.frame.size.height, self.view.frame.size.width, 12);
        mapSep2View.frame = CGRectMake(0, secondSepView.frame.origin.y + secondSepView.frame.size.height, self.view.frame.size.width, 1);
            
            
        }
        
        if(giftDetailResponseObj.brandDescription.length > 0){
            
        if(giftDetailResponseObj.companyURL.length > 0)
        brandDescSepView1.frame  = CGRectMake(0, urlDataView.frame.origin.y + urlDataView.frame.size.height, self.view.frame.size.width, 1);
            
        else
        brandDescSepView1.frame  = CGRectMake(0, detailInfoView.frame.origin.y + detailInfoView.frame.size.height, self.view.frame.size.width, 1);
            
            
        brandDescSepView1.backgroundColor = [UIColor darkGrayColor];
        [scrollView addSubview:brandDescSepView1];
            
        brandDescSepView.frame = CGRectMake(0, brandDescSepView1.frame.origin.y + brandDescSepView1.frame.size.height, self.view.frame.size.width, 12);
        brandDescSepView.backgroundColor = [UIColor colorWithRed:236/255.0 green:236/255.0 blue:236/255.0 alpha:1];
        [scrollView addSubview:brandDescSepView];
            
            
        brandDescSepView2.frame = CGRectMake(0,  brandDescSepView.frame.origin.y + brandDescSepView.frame.size.height, self.view.frame.size.width, 1);
        brandDescSepView2.backgroundColor = [UIColor darkGrayColor];
        [scrollView addSubview:brandDescSepView2];
            
            NSLog(@"Text height :%lu",(unsigned long)giftDetailResponseObj.brandDescription.length);

         
               // brandDescDataView = [[UIView alloc]initWithFrame:CGRectMake(0, brandDescSepView2.frame.origin.y + brandDescSepView2.frame.size.height, self.view.frame.size.width,giftDetailResponseObj.brandDescription.length)];
            
     // brandDescDataView.frame = CGRectMake(0, brandDescSepView2.frame.origin.y + brandDescSepView2.frame.size.height, self.view.frame.size.width, giftDetailResponseObj.brandDescription.length);
            
            if(giftDetailResponseObj.brandDescription.length > 200)
                brandDescDataView.frame = CGRectMake(0, brandDescSepView2.frame.origin.y + brandDescSepView2.frame.size.height, self.view.frame.size.width, giftDetailResponseObj.brandDescription.length - 90);
            else
                brandDescDataView.frame = CGRectMake(0, brandDescSepView2.frame.origin.y + brandDescSepView2.frame.size.height, self.view.frame.size.width, giftDetailResponseObj.brandDescription.length);
            
            
        UILabel *companyTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(5, 20, 250, 21)];
        companyTitleLabel.text = @"ABOUT THE COMPANY";
            
            [self setTextOnLabel:companyTitleLabel fontSize:18.0];
        [brandDescDataView addSubview:companyTitleLabel];
            
        UILabel *descLabel = [[UILabel alloc]initWithFrame:CGRectMake(5, companyTitleLabel.frame.origin.y + companyTitleLabel.frame.size.height , self.view.frame.size.width - 8, giftDetailResponseObj.brandDescription.length)];
        descLabel.text = giftDetailResponseObj.brandDescription;
            [self setTextOnLabel:descLabel fontSize:14.0];

            
            
        [brandDescDataView addSubview:descLabel];
            
            
            
        [scrollView addSubview:brandDescDataView];
            
            
            
            
        mapSep1View.frame = CGRectMake(0, brandDescDataView.frame.origin.y + brandDescDataView.frame.size.height, self.view.frame.size.width, 1);
        secondSepView.frame = CGRectMake(0, mapSep1View.frame.origin.y + mapSep1View.frame.size.height, self.view.frame.size.width, 12);
        mapSep2View.frame = CGRectMake(0, secondSepView.frame.origin.y + secondSepView.frame.size.height, self.view.frame.size.width, 1);
            
        }
        
        
        
        
        
        if(giftDetailResponseObj.companyURL.length == 0 && giftDetailResponseObj.brandDescription.length == 0){
        mapSep1View.frame = CGRectMake(0, detailInfoView.frame.origin.y + detailInfoView.frame.size.height, self.view.frame.size.width, 1);
        secondSepView.frame = CGRectMake(0, mapSep1View.frame.origin.y + mapSep1View.frame.size.height, self.view.frame.size.width, 12);
        mapSep2View.frame = CGRectMake(0, secondSepView.frame.origin.y + secondSepView.frame.size.height, self.view.frame.size.width, 1);
        }
        
        if(giftDetailResponseObj.locationDetailArray.count > 0)
        [self showMapViewOnViewWhenMoreIsNotClicked];
        else{
        scrollView.contentSize = CGSizeMake(self.view.frame.size.width, mapSep2View.frame.origin.y + mapSep2View.frame.size.height + 150);

        }
    }

}

#pragma mark - Click Event

-(IBAction)heartBtnClick:(id)sender{
    NSLog(@"#HeartButtonCLick");
    isHeartClick = TRUE;
    [self callMyLikeService];
}

-(IBAction)rateBtnClick:(id)sender{
    NSLog(@"#RateButtonCLick");
    isHeartClick = FALSE;
    
    
    coverView =  [self onAddDimViewOnSuperView:self.view];
    
    UITapGestureRecognizer *gesRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(viewTapped:)];
    gesRecognizer.numberOfTapsRequired = 1;
    [coverView addGestureRecognizer:gesRecognizer];
    
    objRateView = [[RateView alloc]initWithFrame:CGRectMake(10, self.view.frame.origin.y + 150, self.view.frame.size.width , 220)];
    [self.view addSubview:objRateView];
    
    
    GiftDetailViewController *vc = self;
    
    [objRateView setOnItsBadClick:^{
    [vc callRateService:@"5" rateOfferID:_giftOfferID];
    }];
    
    [objRateView setOnJustOKClick:^{
    [vc callRateService:@"3" rateOfferID:_giftOfferID];
    }];
    
    [objRateView setOnLikeITClick:^{
    [vc callRateService:@"2" rateOfferID:_giftOfferID];
    }];
    
    
    [objRateView setOnLoveITClick:^{
    [vc callRateService:@"1" rateOfferID:_giftOfferID];
    }];
    
    [objRateView setOnPoorClick:^{
    [vc callRateService:@"1" rateOfferID:_giftOfferID];
    }];
    
    [objRateView setOnCrossClick:^{
    [self removeRateView];
    }];
}

-(void)removeRateView{
    [objRateView removeFromSuperview];
    [coverView removeFromSuperview];
}

-(void)viewTapped:(UITapGestureRecognizer *)recognzier{
    if(isHeartClick == TRUE){
    [commonView removeFromSuperview];
    }
    if(isHeartClick == FALSE){
    [objRateView removeFromSuperview];
    }
    
    
    [coverView removeFromSuperview];
}

-(void)setFrameForGiftDetailContainerView{
    
    
    giftImageView.frame = CGRectMake(0, 0, self.view.frame.size.width, 170);

    
  
    if([_giftID isEqualToString:_objMyGiftDealList.dealID]){
    isGiftFound = TRUE;
    NSLog(@"#Found %@ %@",_objMyGiftDealList.code,_objMyGiftDealList.expiry);
        
    voucherCodeStr = _objMyGiftDealList.code;
    expiryDateStr = _objMyGiftDealList.expiry;
    pinCodeStr = _objMyGiftDealList.pinCode;
        
  //  [self showGiftExtraViewOnSuperView];
    }
   
    
    
   
    thumbImageView.frame = CGRectMake(10, giftImageView.frame.origin.y + giftImageView.frame.size.height + 5, 50, 50);
    [self setRoundedCornerForImageView:thumbImageView];

    
     giftTitleLabel.frame = CGRectMake(thumbImageView.frame.origin.x + thumbImageView.frame.size.width + 5, giftImageView.frame.origin.y + giftImageView.frame.size.height +5, self.view.frame.size.width - 85, 80);
    

    CGSize yourLabelSize = [giftTitleLabel.text sizeWithAttributes:@{NSFontAttributeName : [UIFont fontWithName:@"Open Sans" size:18.0]}];
    
    NSLog(@"#GiftTitle :%f",yourLabelSize.height);

    
    CGRect rect = giftTitleLabel.frame;
    rect.size.height = yourLabelSize.height +25;
    giftTitleLabel.frame = rect;
    
    giftTitleLabel.text = giftTitleLabel.text;
    
    mapButton.frame = CGRectMake(thumbImageView.frame.origin.x + thumbImageView.frame.size.width + 5, giftTitleLabel.frame.origin.y + giftTitleLabel.frame.size.height , 15, 20);
    
    NSLog(@"#OfferPriceLabel Text :%@",offerPriceLabel.text);
    
    
  /*  if(offerPriceLabel.text.length > 2){
    rupeeImageView.frame = CGRectMake(giftButton.frame.origin.x - 70, giftImageView.frame.origin.y + giftImageView.frame.size.height + 83, 20, 20);
    
    
    offerPriceLabel.frame = CGRectMake(giftButton.frame.origin.x - 50 , giftImageView.frame.origin.y + giftImageView.frame.size.height + 83, 50, 21);
    }
    
   
    
    else{
        rupeeImageView.frame = CGRectMake(giftButton.frame.origin.x - 27, giftImageView.frame.origin.y + giftImageView.frame.size.height + 83, 20, 20);
        
        
        offerPriceLabel.frame = CGRectMake(giftButton.frame.origin.x - 10 , giftImageView.frame.origin.y + giftImageView.frame.size.height + 83, 30, 21);
    }*/
    
    
    
    giftAddressLabel.frame = CGRectMake(mapButton.frame.origin.x + mapButton.frame.size.width + 5, giftTitleLabel.frame.origin.y + giftTitleLabel.frame.size.height -5 , 200, 30);

    giftButton.frame = CGRectMake(self.view.frame.size.width - 80, giftAddressLabel.frame.origin.y + giftAddressLabel.frame.size.height + 2, 25, 25);

    giftPriceLabel.frame = CGRectMake(self.view.frame.size.width - 50, giftAddressLabel.frame.origin.y + giftAddressLabel.frame.size.height + 5
                                        , 50, 21);

}

-(void)callMyLikeService{
    MyLikeService *service = [[MyLikeService alloc]init];
    [service callMyLikeService:_giftOfferID callback:^(NSError *error, MyLikeResponse *likeResponse) {
    if(error)return ;
    else{
    if(likeResponse.likeStatus == TRUE){
    NSLog(@"Check It's Like");
    [self updateLikeContentInDB:@"1"];
    [heartButton setBackgroundImage:[UIImage imageNamed:@"red_heart.png"] forState:UIControlStateNormal];

    }
    if(likeResponse.likeStatus == FALSE){
    NSLog(@"Check It's UnLike");
    [heartButton setBackgroundImage:[UIImage imageNamed:@"ic_unlike.png"] forState:UIControlStateNormal];
    [self updateLikeContentInDB:@"0"];
    }
    else
    NSLog(@"Error!");
    }
    }];
}

-(void)updateLikeContentInDB:(NSString *)likeValue{
    //  NSString * query = [NSString stringWithFormat:@"select * from GIFTSTABLE WHERE FREEBIEOFFERID=\"%@\"",offserValue];
    
    DBManager *dbManager = [[DBManager alloc]initWithDatabaseFilename:@"dbgiftdata.sql"];
    
    
    NSString *query = [NSString stringWithFormat:@"UPDATE GIFTTABLE SET FREEBIELIKE=\"%d\" WHERE FREEBIEOFFERID=\"%@\"",[likeValue intValue],_giftOfferID];
    NSLog(@"#QueryLikeUnLike :%@",query);
    [dbManager executeQuery:query];
    
    if (dbManager.affectedRows != 0) {
        NSLog(@"Query was executed successfully. Affected rows = %d", dbManager.affectedRows);
        //[delegate editingInfoWasFinished];
    }
    else{
        NSLog(@"Could not execute the query.");
    }
    
}


-(IBAction)getITButtonClicked:(id)sender{
    
    if([giftPocketStr intValue] == 2 || [giftPocketStr intValue] == 3 ){
    return;
    }
    
    else if([giftPocketStr intValue] == 1 || isForRedeem){
    isForRedeem = TRUE;
        
    if([giftPocketStr intValue] == 1 || isForRedeem){
    isForRedeem = TRUE;
    isLocationNotFound = FALSE;
    isGiftFound = FALSE;
    isMoveToRechargeView = FALSE;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"REDEEM COUPON"  message:@"" delegate:self cancelButtonTitle:@"DONE" otherButtonTitles:@"CANCEL", nil];
    alert.alertViewStyle = UIAlertViewStyleSecureTextInput;
    [alert show];
    }
    }
    

        
    else if(_giftType != kSelectedTypeMyGift){
    isForRedeem = FALSE;
    isLocationNotFound = FALSE;
        
        
    if([giftPriceLabel.text intValue] > [[Constant getUserCoinBalance] intValue]){
    isMoveToRechargeView = TRUE;


    int requiredPrice = [giftPriceLabel.text intValue] - [[Constant getUserCoinBalance] intValue];
        
    NSString *msgTitle = [NSString stringWithFormat:@"Oops you need more %d Gift Coins.",requiredPrice];
    NSString *alertMessage = [NSString stringWithFormat:@"You have %d Gift Coins. Recharge now to earn more gift coins and get this gift.",requiredPrice];
        
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:msgTitle  message:alertMessage delegate:self cancelButtonTitle:@"RECHARGE" otherButtonTitles:@"LATER", nil];
    [alert show];
        
    return;
    }
     
    else{
    isMoveToRechargeView = FALSE;
    isPurchasedGift = TRUE;
        
    if(isOfferPriceFound){
        
    coverView =  [self onAddDimViewOnSuperView:self.view];
        
    [coverView addSubview:[self  addOfferPriceConfirmationView]];
    }
        
    else{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"CONFIRMATION"  message:@"Do you want to pocket this gift?" delegate:self cancelButtonTitle:@"CONFIRM" otherButtonTitles:@"LATER", nil];
    [alert show];
    }
    }
    }
    
}


-(void)callPurchaseDealServiceBLock{
    [self showHUDOnView];
    PurchaseDealService *service = [[PurchaseDealService alloc]init];
    [service callPurchaseDealService:self.purchaseCriteria callback:^(NSError *error, PurchaseDealResponse *purchaseDealResponse) {
    if(error)return ;
        
    else{
    [self hideHUDOnView];
    if([purchaseDealResponse.status isEqualToString:@"success"]){
    isLocationNotFound = FALSE;
    isPurchasedGift = FALSE;
        
    if(isOfferPriceFound){
    if(((isFromWallet == TRUE && [[Constant getUserWalletBalance] intValue] < [offerPriceLabel.text intValue]) || !isFromWallet)){
    [coverView removeFromSuperview];
    [self navigateToRechargeTransactionView:purchaseDealResponse.formContent];
    }
        
    else if(isFromWallet == TRUE && [[Constant getUserWalletBalance] intValue] > [offerPriceLabel.text intValue]){
    if([purchaseDealResponse.closingBalance isEqual:[NSNull null]]){
    [Constant saveUserWalletBalance:@"0"];
    }
        
    else{
    isForRedeem = TRUE;
    NSString *wallBalance =  [NSString stringWithFormat:@"%@",purchaseDealResponse.closingBalance];
    [Constant saveUserWalletBalance:wallBalance];
        
    [Constant saveUserCoinBalance:purchaseDealResponse.loyaltyBalance];
        
    voucherCodeStr = purchaseDealResponse.dealCouponCode;
    expiryDateStr = purchaseDealResponse.expiry;
    pinCodeStr = purchaseDealResponse.pinCode;
    [Constant saveUserCoinBalance:purchaseDealResponse.loyaltyBalance];
        
    [self showViewForMyGiftsView:TRUE];
    }
   
    }
    }
        
    else{
    isForRedeem = TRUE;
    [Constant saveUserCoinBalance:purchaseDealResponse.loyaltyBalance];
        
    voucherCodeStr = purchaseDealResponse.dealCouponCode;
    expiryDateStr = purchaseDealResponse.expiry;
    pinCodeStr = purchaseDealResponse.pinCode;
    [Constant saveUserCoinBalance:purchaseDealResponse.loyaltyBalance];
        
    [self showViewForMyGiftsView:TRUE];
    }
   
    }
        
        
    else{
    isPurchasedGift = FALSE;
    isForRedeem = FALSE;
    isLocationNotFound = FALSE;
    isGiftFound = FALSE;
        
    [coverView removeFromSuperview];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"GIFT"  message:purchaseDealResponse.messageDesc delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    [alert show];
    }
    
    }
    }];
}



-(PurchaseDealCriteria *)purchaseCriteria{
    
    NSLog(@"#IsFromWallet :%d",isFromWallet);
    
    PurchaseDealCriteria *objCriteria = [[PurchaseDealCriteria alloc]init];
    objCriteria.offerID = _giftOfferID;
    objCriteria.dealID = _giftID;
    objCriteria.couponID = @"";
    objCriteria.amount = giftPriceLabel.text; //@"";
    objCriteria.quantity = @"1";
    objCriteria.freebie = @"true";
    objCriteria.transCategory = @"deal";
    
    if(isOfferPriceFound == TRUE){
    objCriteria.isFromEVoucher = TRUE;
    if(isFromWallet == TRUE){
    objCriteria.partial = @"1";
    objCriteria.paymentOption = @"wallet";

    }
    else{
    objCriteria.partial = @"";
    objCriteria.paymentOption = @"online";

    }
    objCriteria.offerPrice = offerPriceLabel.text;
    }
    
    else{
    objCriteria.isFromEVoucher = FALSE;
    objCriteria.offerPrice = @"0";
    objCriteria.paymentOption = @"wallet";
   
    }
    objCriteria.refID = @"0";
    objCriteria.api = @"true";

    
    
    return objCriteria;
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSLog(@"%@", [alertView textFieldAtIndex:0].text);
    voucherCodeForRedeem = [alertView textFieldAtIndex:0].text;
    if (buttonIndex == 0)
    {
    NSLog(@"OKKKK");
        
   // if(isLocationNotFound == TRUE && !isForRedeem && !isMoveToRechargeView && !isPurchasedGift){
    if(isLocationNotFound == TRUE ){
    [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
    [[UIApplication sharedApplication] openURL: [NSURL URLWithString: UIApplicationOpenSettingsURLString]];
    return;
    }
    
   // if(isForRedeem && !isLocationNotFound && !isMoveToRechargeView && !isPurchasedGift){
    if(isForRedeem  && !isMoveToRechargeView && !isPurchasedGift){

    [self callRedeemServiceBlock];
    }
    if(!isForRedeem && !isLocationNotFound && isPurchasedGift &&!isMoveToRechargeView){
    [self callPurchaseDealServiceBLock];
    }
    if(!isForRedeem && !isLocationNotFound && !isPurchasedGift  && isMoveToRechargeView){
    AppDelegate *appDelegate =  (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [appDelegate.tabBarController setSelectedIndex:1];
    }
        
        
    }
    else
    {
        NSLog(@"NO");
    }

}
-(IBAction)callCLicked:(id)sender{
    
    coverView =  [self onAddDimViewOnSuperView:self.view];
    
    UITapGestureRecognizer *gesRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(viewTapped:)];
    gesRecognizer.numberOfTapsRequired = 1;
    [coverView addGestureRecognizer:gesRecognizer];
    
    
    commonView = [[GiftDetailCommonView alloc]initWithFrame:CGRectMake(10, self.view.frame.origin.y + 150, self.view.frame.size.width - 10 , 250)];
    [self.view addSubview:commonView];
    
    [commonView showCommonViewForGiftDetails:giftDetailResponseObj.locationDetailArray fromView:TRUE rowID:0 shopName:shopNameLabel.text];
    
   // [commonView showCommonViewForGiftDetails:giftDetailResponseObj.locationDetailArray fromView:TRUE];
    
    [commonView setOnCrossClick:^{
    [commonView removeFromSuperview];
    [coverView removeFromSuperview];
    }];
    
    

}



-(void)showGiftExtraViewOnSuperView:(NSString *)forStatus{
  
    if([forStatus intValue] == 1){
    [giftImageView addSubview:firstGiftLabel];
    [giftImageView addSubview:secondGiftLabel];
        
    firstGiftLabel.frame = CGRectMake(50, 10,250 , 80);
    secondGiftLabel.frame = CGRectMake(60, firstGiftLabel.frame.origin.y + firstGiftLabel.frame.size.height - 5 , 220, 40);
    }
    
    if([forStatus intValue] == 2){
    [giftImageView addSubview:firstGiftLabel];
        
    firstGiftLabel.frame = CGRectMake(30, 30,250 , 80);
    firstGiftLabel.text = @"REEDEMED";
    }
    
    if([forStatus intValue] == 3){
    [giftImageView addSubview:firstGiftLabel];
        
    firstGiftLabel.frame = CGRectMake(30, 30,250 , 80);
    firstGiftLabel.text = @"EXPIRED";

    }
  
}

-(void)showViewForMyGiftsView:(BOOL)isShow{
    
    if(isShow){
    isForRedeem = TRUE;
    getITButton.titleLabel.textColor = [UIColor whiteColor];
    getITButton.backgroundColor = [UIColor colorWithRed:184/255.0 green:185/255.0 blue:184/255.0 alpha:1];
        
    [giftImageView addSubview:firstGiftLabel];
    [giftImageView addSubview:secondGiftLabel];
        
    firstGiftLabel.frame = CGRectMake(50, 10,250 , 80);
    secondGiftLabel.frame = CGRectMake(60, firstGiftLabel.frame.origin.y + firstGiftLabel.frame.size.height - 5 , 220, 40);
    
    [getITButton setTitle:@"REDEEM NOW" forState:UIControlStateNormal];
    }
    
    [heartButton removeFromSuperview];
    
    giftDescContainerView.frame = CGRectMake(0, 0, self.view.frame.size.width,250);

    
    
    

    
    
    
    giftFirst1SepView.frame = CGRectMake(0, giftDescContainerView.frame.origin.y + giftDescContainerView.frame.size.height + 40, self.view.frame.size.width, 1);
    giftSepView.frame = CGRectMake(0, giftFirst1SepView.frame.origin.y + giftFirst1SepView.frame.size.height, self.view.frame.size.width, 12);
    giftFirst2SepView.frame = CGRectMake(0, giftSepView.frame.origin.y + giftSepView.frame.size.height, self.view.frame.size.width, 1);
    
    
    [scrollView addSubview:giftFirst1SepView];
    [scrollView addSubview: giftFirst2SepView];
    [scrollView addSubview: giftSepView];

    [scrollView addSubview:myGiftDetailContainerView];

    
    if(pinCodeStr.length > 0){
        pinDataLabel.hidden = FALSE;
        pinTxtLabel.hidden = FALSE;
    myGiftDetailContainerView.frame = CGRectMake(0, giftFirst2SepView.frame.origin.y + giftFirst2SepView.frame.size.height, self.view.frame.size.width, 130);
    }
    else{
        pinDataLabel.hidden = TRUE;
        pinTxtLabel.hidden = TRUE;
    myGiftDetailContainerView.frame = CGRectMake(0, giftFirst2SepView.frame.origin.y + giftFirst2SepView.frame.size.height, self.view.frame.size.width, 100);
    }
    
    
    mobileTxtLabel.frame = CGRectMake(10, 15,143 , 21);
    mobileNumLabel.frame = CGRectMake(mobileTxtLabel.frame.origin.x + mobileTxtLabel.frame.size.width -10, 15,143 , 21);

    voucherTxtLabel.frame = CGRectMake(10, mobileTxtLabel.frame.origin.y + mobileTxtLabel.frame.size.height + 10,132 , 21);
    voucherCodeLabel.frame = CGRectMake(voucherTxtLabel.frame.origin.x + voucherTxtLabel.frame.size.width - 10 , mobileTxtLabel.frame.origin.y + mobileTxtLabel.frame.size.height +10,[UIScreen mainScreen].bounds.size.width - 15 , 21);

    expiryTxtLabel.frame = CGRectMake(10, voucherTxtLabel.frame.origin.y + voucherTxtLabel.frame.size.height + 10,96 , 21);
    expiryDateLabel.frame = CGRectMake(expiryTxtLabel.frame.origin.x + expiryTxtLabel.frame.size.width +5, voucherTxtLabel.frame.origin.y + voucherTxtLabel.frame.size.height +10,143 , 21);

    
    mobileNumLabel.text = [NSString stringWithFormat:@"XXXXXX%@",[[Constant getUserPhoneNumber] substringFromIndex:6]];
    

    
    voucherCodeLabel.text = voucherCodeStr;
    expiryDateLabel.text = expiryDateStr;
    
    
    pinTxtLabel.frame = CGRectMake(10, expiryDateLabel.frame.origin.y + expiryDateLabel.frame.size.height + 10,50 , 21);
    pinDataLabel.frame = CGRectMake(pinTxtLabel.frame.origin.x + pinTxtLabel.frame.size.width -10, expiryDateLabel.frame.origin.y + expiryDateLabel.frame.size.height + 10,143 , 21);
    pinDataLabel.text = pinCodeStr;
    
    
    
    detailInfoSep1View.frame = CGRectMake(0, myGiftDetailContainerView.frame.origin.y + myGiftDetailContainerView.frame.size.height, self.view.frame.size.width, 1);


    firstSepView.frame = CGRectMake(0, detailInfoSep1View.frame.origin.y + detailInfoSep1View.frame.size.height, self.view.frame.size.width, 12);
    detailInfoSep2View.frame = CGRectMake(0, firstSepView.frame.origin.y + firstSepView.frame.size.height, self.view.frame.size.width, 1);
    
    offerTxtLabel.frame = CGRectMake(100, detailInfoSep2View.frame.origin.y + detailInfoSep2View.frame.size.height + 15, self.view.frame.size.width, 21);
    offerRedView.frame = CGRectMake(0, offerTxtLabel.frame.origin.y + offerTxtLabel.frame.size.height+ 15, self.view.frame.size.width, 1);
    
    detailInfoView.frame = CGRectMake(0, offerRedView.frame.origin.y + offerRedView.frame.size.height+ 20, self.view.frame.size.width, 10);
    
   
    
   // if (!isFromWallet && !isOfferPriceFound)
   // [self showGiftDetailInfoOnView:giftDetailResponseObj];
    
    [self showPurchasedGiftDetailInfoOnView:giftDetailResponseObj];
}


#pragma mark - Save Rate Info

-(void)callRateService:(NSString *)ratePoint rateOfferID:(NSString *)rateOfferID{
    SaveUserReviewService *objReviewService = [[SaveUserReviewService alloc]init];
    [objReviewService callSaveUserReview:rateOfferID reviewPoint:ratePoint callback:^(NSError *error, SaveUserResponse *objSaveUserResponse) {
    if(error)return ;
    else{
    NSLog(@"Response Saved");
    [self removeRateView];
    }
    }];
}

#pragma mark - Redeem Block

-(void)callRedeemServiceBlock{
    [self showHUDOnView];
    PurchaseDealService *objService = [[PurchaseDealService alloc]init];
    [objService callPurchaseRedeemService:voucherCodeForRedeem callback:^(NSError *error, RedeemResponse *objRedeemResponse) {
    if(error)return;
    else{
    [self hideHUDOnView];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DEAL"  message:objRedeemResponse.desc delegate:self cancelButtonTitle:nil otherButtonTitles:@"DONE", nil];
    [alert show];
    isGiftFound = FALSE;
    }
        
    }];
}

-(void)backClicked:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)urlButtonCLick:(id)sender{
    NSLog(@"#Company URL :%@",giftDetailResponseObj.companyURL);
 [[UIApplication sharedApplication] openURL:[NSURL URLWithString:giftDetailResponseObj.companyURL]];

}

-(UIView *)addOfferPriceConfirmationView{
    
    UIView * confirmationView;
    
  //  if([[Session shared].objSignInResponse.userWalletBalance intValue] > 0){
    if([[Constant getUserWalletBalance] intValue] > 0){

    confirmationView = [[UIView alloc]initWithFrame:CGRectMake(20, 64+50, [UIScreen mainScreen].bounds.size.width - 40, 280)];
    }
    else{
    confirmationView =  [[UIView alloc]initWithFrame:CGRectMake(20, 64+50, [UIScreen mainScreen].bounds.size.width - 40, 335)];
    }
    
    confirmationView.backgroundColor = [UIColor whiteColor];
        
    
    UILabel *txtLabel = [[UILabel alloc]initWithFrame:CGRectMake(confirmationView.frame.origin.x + 60, 10, 130, 21)];
    txtLabel.text = @"CONFIRMATION";
    [self setTextOnLabel:txtLabel fontSize:18.0];
    [confirmationView addSubview:txtLabel];
    
    
    UILabel *messageLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, txtLabel.frame.origin.y + txtLabel.frame.size.height + 20, confirmationView.frame.size.width - 10, 42)];
    messageLabel.text = @"Are you sure you want to grab this \nvoucher?";
    [self setTextOnLabel:messageLabel fontSize:16.0];
    messageLabel.textAlignment = NSTextAlignmentCenter;
   
    [confirmationView addSubview:messageLabel];
    
    UIView *detailInfoConfirmView = [[UIView alloc]initWithFrame:CGRectMake(10, messageLabel.frame.origin.y + messageLabel.frame.size.height + 10, confirmationView.frame.size.width - 20, 60)];
    detailInfoConfirmView.layer.borderColor = [UIColor grayColor].CGColor;
    detailInfoConfirmView.layer.borderWidth = 1;
    
    UIImageView *logoImageView = [[UIImageView alloc]initWithFrame:CGRectMake(20, 5, 50, 50)];
    logoImageView.image = thumbImageView.image;
    [detailInfoConfirmView addSubview:logoImageView];
    [self setRoundedCornerForImageView:logoImageView];
        
    
    
    UIImageView *rupeeLogoImageView = [[UIImageView alloc]initWithFrame:CGRectMake(logoImageView.frame.origin.x + logoImageView.frame.size.width , 20, 20, 20)];
    rupeeLogoImageView.image = [UIImage imageNamed:@"rupee.png"];
    [detailInfoConfirmView addSubview:rupeeLogoImageView];
    
    
    UILabel *rupeeLabel ;
    

    if(offerPriceLabel.text.length > 3)
    rupeeLabel = [[UILabel alloc]initWithFrame:CGRectMake(rupeeLogoImageView.frame.origin.x + rupeeLogoImageView.frame.size.width, 20, 60, 20)];
    else
    rupeeLabel = [[UILabel alloc]initWithFrame:CGRectMake(rupeeLogoImageView.frame.origin.x + rupeeLogoImageView.frame.size.width, 20, 50, 20)];
   
        
    rupeeLabel.text = offerPriceLabel.text;
    [detailInfoConfirmView addSubview:rupeeLabel];
    
    [confirmationView addSubview:detailInfoConfirmView];

    
    
    UIImageView *plusImageViewBox = [[UIImageView alloc]initWithFrame:CGRectMake(rupeeLabel.frame.origin.x + rupeeLabel.frame.size.width - 15 , 25, 10, 10)];
    plusImageViewBox.image = [UIImage imageNamed:@"plus.png"];
    [detailInfoConfirmView addSubview:plusImageViewBox];
    

    
    
    UIImageView *giftIcon = [[UIImageView alloc]initWithFrame:CGRectMake(plusImageViewBox.frame.origin.x + plusImageViewBox.frame.size.width +5, 15, 30, 30)];
    giftIcon.image = [UIImage imageNamed:@"gift.png"];
    [detailInfoConfirmView addSubview:giftIcon];
    

    
    
    UILabel *priceLabel = [[UILabel alloc]initWithFrame:CGRectMake(giftIcon.frame.origin.x + giftIcon.frame.size.width , 20, 60, 20)];
    priceLabel.text = giftPriceLabel.text;
    [detailInfoConfirmView addSubview:priceLabel];
    
    [confirmationView addSubview:detailInfoConfirmView];

    UILabel *infoLabel;
    UIView *bottomSepView;

    
  //  if([[Session shared].objSignInResponse.userWalletBalance intValue] > 0){
    if([[Constant getUserWalletBalance] intValue] > 0){

    isFromWallet = TRUE;
    checkButton = [UIButton buttonWithType:UIButtonTypeCustom];
    checkButton.frame = CGRectMake(detailInfoConfirmView.frame.origin.x + 40, detailInfoConfirmView.frame.origin.y + detailInfoConfirmView.frame.size.height + 20, 20, 20);
    [checkButton setBackgroundImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateNormal];
        
    [checkButton addTarget:self action:@selector(checkButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        
    infoLabel = [[UILabel alloc]initWithFrame:CGRectMake(checkButton.frame.origin.x + checkButton.frame.size.width + 10,  detailInfoConfirmView.frame.origin.y + detailInfoConfirmView.frame.size.height + 20, 200, 21)];
    infoLabel.text = @"USE WALLET BALANCE";
    infoLabel.font = [UIFont fontWithName:@"Open Sans" size:16.0];
    
    [confirmationView addSubview:checkButton];
    [confirmationView addSubview:infoLabel];
        
    bottomSepView = [[UIView alloc]initWithFrame:CGRectMake(0, infoLabel.frame.origin.y + infoLabel.frame.size.height + 20, confirmationView.frame.size.width, 1)];

    }
    
    else{
    isFromWallet = FALSE;
    infoLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, detailInfoConfirmView.frame.origin.y + detailInfoConfirmView.frame.size.height+5, confirmationView.frame.size.width - 10, 100)];
    infoLabel.text = [NSString stringWithFormat:@"You have %@ gifts Coins. Recharge to earn %@ Gifts coins \nOR \nContinue to pay full Rs. %@",[Session shared].objSignInResponse.userLoyaltyPoints,giftPriceLabel.text,actualPriceStr];
    infoLabel.font = [UIFont fontWithName:@"Open Sans" size:15];
    infoLabel.lineBreakMode = NSLineBreakByWordWrapping;
    infoLabel.numberOfLines = 0;
    [infoLabel sizeToFit];
    [confirmationView addSubview:infoLabel];
        
    bottomSepView = [[UIView alloc]initWithFrame:CGRectMake(0, infoLabel.frame.origin.y + infoLabel.frame.size.height + 30, confirmationView.frame.size.width, 1)];
    }
    
    
    bottomSepView.backgroundColor = [UIColor lightGrayColor];
    [confirmationView addSubview:bottomSepView];
    
    
    
    
    UIButton *confirmButton = [UIButton buttonWithType:UIButtonTypeCustom];
    confirmButton.frame = CGRectMake(0, bottomSepView.frame.origin.y + bottomSepView.frame.size.height  , confirmationView.frame.size.width/2 -1 , confirmationView.frame.size.height - (bottomSepView.frame.origin.y + bottomSepView.frame.size.height));
    [confirmButton setTitle:@"CONFIRM" forState:UIControlStateNormal];
    [confirmButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [confirmButton addTarget:self action:@selector(confirmCLicked:) forControlEvents:UIControlEventTouchUpInside];
    [confirmationView addSubview:confirmButton];
    
    UIView *horizontalSepView = [[UIView alloc]initWithFrame:CGRectMake(confirmButton.frame.origin.x + confirmButton.frame.size.width, bottomSepView.frame.origin.y + bottomSepView.frame.size.height, 1, confirmationView.frame.size.height - (bottomSepView.frame.origin.y + bottomSepView.frame.size.height))];
    horizontalSepView.backgroundColor = [UIColor lightGrayColor];
    [confirmationView addSubview:horizontalSepView];
    
    
    UIButton *cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelButton.frame = CGRectMake(confirmButton.frame.origin.x + confirmButton.frame.size.width + 2, bottomSepView.frame.origin.y + bottomSepView.frame.size.height  , confirmationView.frame.size.width -  confirmationView.frame.size.width/2  , confirmationView.frame.size.height - (bottomSepView.frame.origin.y + bottomSepView.frame.size.height));
    [cancelButton setTitle:@"CANCEL" forState:UIControlStateNormal];
    [cancelButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(cancelCLicked:) forControlEvents:UIControlEventTouchUpInside];

    [confirmationView addSubview:cancelButton];
        
    return confirmationView;
    

}

-(void)confirmCLicked:(id)sender{
    NSLog(@"#ConfirrmClicke");
    [coverView removeFromSuperview];
    [self callPurchaseDealServiceBLock];
}

-(void)navigateToRechargeTransactionView:(NSString *)formHTMLContent{
    
    RechargeTransactionViewController *rechargeTransactionVC = [self.storyboard instantiateViewControllerWithIdentifier:@"rechargeTransactionView"];
    rechargeTransactionVC.objRechargeResponse = [[RechargeResponse alloc]init];
    rechargeTransactionVC.objRechargeResponse.formContent = formHTMLContent;
    rechargeTransactionVC.typeStr = @"E-Voucher Purchase";
    rechargeTransactionVC.operatorName = @"E-VOucher";
    rechargeTransactionVC.isComingFrom = FALSE;
    rechargeTransactionVC.isComingFromGift = TRUE;
    [self.navigationController pushViewController:rechargeTransactionVC animated:YES];
}

-(void)cancelCLicked:(id)sender{
    NSLog(@"#CancelClicke");
    [coverView removeFromSuperview];
}

-(void)checkButtonTapped:(id)sender{
    NSLog(@"#CheckTapped");
    
    if(isFromWallet == TRUE){
    [checkButton setBackgroundImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
    }
    
    if(isFromWallet == FALSE){
    [checkButton setBackgroundImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateNormal];
    }
    
    isFromWallet = !isFromWallet;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
