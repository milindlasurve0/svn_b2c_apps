﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Windows.Input;
using Microsoft.Phone.Tasks;
using System.Device.Location;
using Microsoft.Phone.Maps.Toolkit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Device.Location;
using Microsoft.Phone.Maps.Services;
using Windows.Devices.Geolocation;
using Microsoft.Phone.Maps.Controls;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Microsoft.Phone.Maps.Toolkit;
using System.Windows.Media;
using System.Collections.ObjectModel;


using System.Windows;

using Microsoft.Phone.Controls;


using System.Windows.Shapes;
using System.Windows.Media.Imaging;

using Pay1B2C;
using System.Windows.Threading;
using Microsoft.Phone.Controls.Maps.Platform;

namespace Pay1B2C.Views
    {
    public partial class ShopInfo: PhoneApplicationPage
        {

        List<GeoCoordinate> MyCoordinates = new List<GeoCoordinate>();
        RouteQuery MyQuery = null;
        
        GeocodeQuery Mygeocodequery = null;


        Location location = new Location();
        string shopName, shopNumber, shopLattitude, shopLongitude, currentLattitude, currentLongitude;
        double doubleShopLattitude, doubleShopLongitude, doubleCurrentLattitude, doubleCurrentLongitude;
        string area_name, shopAddress;
        ProgressBarManager ProgressBarManager;
        GeoCoordinateWatcher watcher;

        public ShopInfo()
            {
            InitializeComponent();

            GoogleAnalytics.EasyTracker.GetTracker().SendView("ShopInfo");
            string data = PhoneApplicationService.Current.State[Constant.SHOP_DATA].ToString();
            ProgressIndicator prog = new ProgressIndicator();
            ProgressBarManager = new ProgressBarManager();
            SystemTray.SetProgressIndicator(this, prog);
            ProgressBarManager.Hook(prog);

            var dict = (JObject)JsonConvert.DeserializeObject(data);
            dict["shopname"].ToString();
            double dis = Convert.ToDouble(dict["D"].ToString());
            if(dis < 1)
                {
                dis = dis * 1000;
                textShopDistance.Text = dis.ToString("#.##") + " M";
                }
            else
                {
                textShopDistance.Text = dis.ToString("#.##") + " KM";
                }



            currentLattitude = Constant.LoadPersistent<string>(Constant.SETTINGS_USER_LATTITUDE);
            currentLongitude = Constant.LoadPersistent<string>(Constant.SETTINGS_USER_LONGITUDE);
            shopLattitude = dict["latitude"].ToString();
            shopLongitude = dict["longitude"].ToString();



            doubleShopLattitude = Convert.ToDouble(shopLattitude);
            doubleShopLongitude = Convert.ToDouble(shopLongitude);
            doubleCurrentLattitude = Convert.ToDouble(currentLattitude);
            doubleCurrentLongitude = Convert.ToDouble(currentLongitude);


            string pin = dict["pin"].ToString();
            area_name = dict["area_name"].ToString();
            shopAddress = dict["address"].ToString();
            string city_name = dict["city_name"].ToString();
            string state_name = dict["state_name"].ToString();
            string address = area_name + "\n" + city_name + "\n" + state_name + " - " + pin;
            shopName = dict["shopname"].ToString().ToUpper();
            textShopName.Text = shopName;
            Uri shopImgUri = new Uri(dict["imagepath"].ToString(), UriKind.RelativeOrAbsolute);
            BitmapImage shopImgSource = new BitmapImage(shopImgUri);
            imgShop.Source=shopImgSource;
            textShopName1.Text = "Shop Name:  " + shopName;
            shopNumber = dict["mobile"].ToString();
            textShopArea.Text = dict["area_name"].ToString().ToUpperInvariant();
            textShopAddress.Text = "Address: " + shopAddress + "\n" + address + "\n" + "Shop number " + shopNumber;
            callImage.Tap += callImage_Tapped;
            locateImage.Tap += locateImage_Tapped;

            loadAllShopsOnMap(shopLattitude, shopLongitude, address);


            WebClient client = new WebClient();

            client.DownloadStringCompleted += client_DownloadStringCompleted;

            string Url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=" + shopLattitude + "," + shopLongitude + "&sensor=true";
            client.DownloadStringAsync(new Uri(Url, UriKind.RelativeOrAbsolute));






            }




        private void client_DownloadStringCompleted(object sender, DownloadStringCompletedEventArgs e)
            {

            var getResult = e.Result;
            JObject parseJson = JObject.Parse(getResult);
            var getJsonres = parseJson["results"][0];
            var getJson = getJsonres["address_components"][1];
            var getJson2 = getJsonres["address_components"][2];
            var getJson3 = getJsonres["address_components"][3];
            var sub_area = getJson["long_name"];
            var area = getJson["long_name"];
            var address = getJson["long_name"];
            //Address = sub_area.ToString() + ", " + area.ToString() + ", " + address.ToString();
            shopAddress = getJsonres["formatted_address"].ToString();

            }

        private void callImage_Tapped(object sender, System.Windows.Input.GestureEventArgs e)
            {
            PhoneCallTask phoneCallTask = new PhoneCallTask();

            phoneCallTask.PhoneNumber = shopNumber;
            phoneCallTask.DisplayName = shopName;

            phoneCallTask.Show();
            }

        MapLayer _routeLayer;

       
        private void locateImage_Tapped(object sender, System.Windows.Input.GestureEventArgs e)
            {

            this.GetCoordinates(shopAddress);




            DispatcherTimer DelayedTimer = new DispatcherTimer()
            {
                Interval = TimeSpan.FromSeconds(400000)
            };
            DelayedTimer.Tick += (s, eee) =>
            {

                watcher = new GeoCoordinateWatcher(GeoPositionAccuracy.High); // Use high accuracy. 
                watcher.MovementThreshold = 20; // Use MovementThreshold to ignore noise in the signal. 
                watcher.StatusChanged += new EventHandler<GeoPositionStatusChangedEventArgs>(watcher_StatusChanged);
                if(watcher.Permission == GeoPositionPermission.Denied)
                    {
                    //Location services is disable on the phone. Show a message to the user.
                    MessageBox.Show("Your location is not enabled.Please turn it on.");

                    }
                else
                    {
                    watcher.Start();
                    }

            };
            DelayedTimer.Start();


            }


        private void watcher_StatusChanged(object sender, GeoPositionStatusChangedEventArgs e)
            {
            if(e.Status == GeoPositionStatus.Ready)
                {
                // Use the Position property of the GeoCoordinateWatcher object to get the current location. 
                GeoCoordinate co = watcher.Position.Location;
                WebClient client = new WebClient();
                string latti = co.Latitude.ToString("0.000");
                string longi = co.Longitude.ToString("0.000");
                client.DownloadStringCompleted += client_DownloadStringCompleted1;

                string Url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=" + latti + "," + longi + "&sensor=true";
                client.DownloadStringAsync(new Uri(Url, UriKind.RelativeOrAbsolute));





                watcher.Stop();
                }
            }

        private void client_DownloadStringCompleted1(object sender, DownloadStringCompletedEventArgs e)
            {
            var getResult = e.Result;
            JObject parseJson = JObject.Parse(getResult);
            var getJsonres = parseJson["results"][0];
            var getJson = getJsonres["address_components"][1];
            var getJson2 = getJsonres["address_components"][2];
            var getJson3 = getJsonres["address_components"][3];
            var sub_area = getJson["long_name"];
            var area = getJson["long_name"];
            var address = getJson["long_name"];
            //Address = sub_area.ToString() + ", " + area.ToString() + ", " + address.ToString();
            string shopAddress1 = getJsonres["formatted_address"].ToString();
          
            }


        bool isTappedFirst;
        private void Rectangle_Tapped(object sender, MouseEventArgs e)
            {
            if(!isTappedFirst)
                {
                isTappedFirst = true;
                myStoryboard.Begin();

                }
            else
                {
                isTappedFirst = false;
                myStoryboard.Stop();
                }

            }

        private void buttonImage_Click_Back(object sender, RoutedEventArgs e)
            {
           
            NavigationService.GoBack();
            }

        MapLayer PinLayer1;
        private void loadAllShopsOnMap(string latiitude, string longitude, string address)
            {
            Image img=new Image { Height=55, Width=55, Source=new BitmapImage(new Uri("/Images/ic_home_locate.png", UriKind.RelativeOrAbsolute)) };

            Microsoft.Phone.Maps.Controls.MapOverlay overlay = new MapOverlay()
            {
                PositionOrigin = new Point(0.5, 0.5),
                GeoCoordinate = new System.Device.Location.GeoCoordinate(Convert.ToDouble(latiitude), Convert.ToDouble(longitude)),

                Content =img
            };

            MapLayer ml = new MapLayer { overlay };

            MyMap.Layers.Add(ml);

            }

        void pushPin_Tap(object sender, System.Windows.Input.GestureEventArgs e)
            {
            var push = sender as Pushpin;

            Border brodr = (Border)push.Content;
            if(brodr.Visibility == Visibility.Visible)
                {
                brodr.Visibility = System.Windows.Visibility.Collapsed;
                }
            else
                {
                brodr.Visibility = System.Windows.Visibility.Visible;
                }

            //stop the event from going to the parent map control
            e.Handled = true;
            }



        private void myMapControl_Loaded(object sender, RoutedEventArgs e)
            {
            Microsoft.Phone.Maps.MapsSettings.ApplicationContext.ApplicationId = "730762d9-a6df-4bcf-8458-c9bc8873dd56";
            Microsoft.Phone.Maps.MapsSettings.ApplicationContext.AuthenticationToken = "v6G8RFopHc2NbpS0p6rWIQ";
            }

        private async void GetCoordinates(String address)
            {
            Geolocator MyGeolocator = new Geolocator();
            MyGeolocator.DesiredAccuracyInMeters = 5;
            Geoposition MyGeoPosition = null;
            try
                {
                MyGeoPosition = await MyGeolocator.GetGeopositionAsync(TimeSpan.FromMinutes(1), TimeSpan.FromSeconds(10));
                MyCoordinates.Add(new GeoCoordinate(Constant.LoadPersistent<double>(Constant.SETTINGS_USER_LATTITUDE_DOUBLE), Constant.LoadPersistent<double>(Constant.SETTINGS_USER_LONGITUDE_DOUBLE)));

                Mygeocodequery = new GeocodeQuery();
                Mygeocodequery.SearchTerm = address;
                Mygeocodequery.GeoCoordinate = new GeoCoordinate(MyGeoPosition.Coordinate.Latitude, MyGeoPosition.Coordinate.Longitude);

                Mygeocodequery.QueryCompleted += Mygeocodequery_QueryCompleted;
                Mygeocodequery.QueryAsync();

                }
            catch(UnauthorizedAccessException)
                {
                MessageBox.Show("Location is disabled in phone settings or capabilities are not checked.");
                }
            catch(Exception ex)
                {
                // Something else happened while acquiring the location.
                MessageBox.Show(ex.Message);
                }
            }

        void Mygeocodequery_QueryCompleted(object sender, QueryCompletedEventArgs<IList<MapLocation>> e)
            {
            try
                {
                if(e.Error == null)
                    {
                    Deployment.Current.Dispatcher.BeginInvoke(() =>
                    {
                        MyQuery = new RouteQuery();
                        MyCoordinates.Add(e.Result[0].GeoCoordinate);
                        MyQuery.Waypoints = MyCoordinates;
                        MyQuery.QueryCompleted += MyQuery_QueryCompleted;
                        MyQuery.QueryAsync();

                        Mygeocodequery.Dispose();
                    });
                    }
                }
            catch(Exception exc)
                {
                ProgressBarManager.Hide(55);
                }
            }


        void MyQuery_QueryCompleted(object sender, QueryCompletedEventArgs<Route> e)
            {
            //  if(e.Error == null)
            try
                {
                Route MyRoute = e.Result;
                MapRoute MyMapRoute = new MapRoute(MyRoute);
                MyMap.AddRoute(MyMapRoute);
                MyMap.ZoomLevel=15;
                List<string> RouteList = new List<string>();
                foreach(RouteLeg leg in MyRoute.Legs)
                    {
                    foreach(RouteManeuver maneuver in leg.Maneuvers)
                        {
                        RouteList.Add(maneuver.InstructionText);
                        }
                    }

                //RouteLLS.ItemsSource = RouteList;
                addPushPin(MyCoordinates.First().Latitude, MyCoordinates.First().Longitude, "/Images/ic_home_locate.png");
                addPushPin(MyCoordinates.Last().Latitude, MyCoordinates.Last().Longitude, "/Images/ic_gift_pin.png");
                ProgressBarManager.Hide(55);
                MyMap.Center=new GeoCoordinate(Constant.LoadPersistent<double>(Constant.SETTINGS_USER_LATTITUDE_DOUBLE), Constant.LoadPersistent<double>(Constant.SETTINGS_USER_LONGITUDE_DOUBLE));
                MyQuery.Dispose();
                }
            catch(Exception ee)
                {
                }
            }

        public void addPushPin(double lattitude, double longitude, String uri)
            {

            

            Image img=new Image { Height=55, Width=55, Source=new BitmapImage(new Uri(uri, UriKind.RelativeOrAbsolute)) };

            MapOverlay overlay = new MapOverlay()
            {
                PositionOrigin = new Point(0.5, 0.5),
                GeoCoordinate = new GeoCoordinate(lattitude, longitude),

                Content =img
            };

            MapLayer ml = new MapLayer { overlay };

            MyMap.Layers.Add(ml);

            }




        }
    }