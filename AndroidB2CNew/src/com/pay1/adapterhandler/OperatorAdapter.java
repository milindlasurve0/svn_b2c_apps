package com.pay1.adapterhandler;

import java.util.ArrayList;
import java.util.WeakHashMap;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.pay1.OperatorActivity;
import com.pay1.R;
import com.pay1.constants.OperatorConstant;

public class OperatorAdapter extends BaseAdapter {

	Context context;
	ArrayList<WeakHashMap<String, String>> data;
	private Typeface Reguler;

	public OperatorAdapter(Context context,
			ArrayList<WeakHashMap<String, String>> data) {
		this.context = context;
		this.data = data;
		Reguler = Typeface.createFromAsset(context.getAssets(),
				"Roboto-Regular.ttf");
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder viewHolder;
		try {
			if (convertView == null) {
				LayoutInflater inflater = (LayoutInflater) context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(R.layout.operator_adapter, null);
				viewHolder = new ViewHolder();
				viewHolder.textView_OperatorName = (TextView) convertView
						.findViewById(R.id.textView_OperatorName);
				viewHolder.textView_OperatorName.setTypeface(Reguler);
				viewHolder.textView_OperatorID = (TextView) convertView
						.findViewById(R.id.textView_OperatorID);
				viewHolder.textView_OperatorID.setTypeface(Reguler);
				viewHolder.imageView_logo = (ImageView) convertView
						.findViewById(R.id.imageView_logo);
				convertView.setTag(viewHolder);

			} else {
				viewHolder = (ViewHolder) convertView.getTag();
			}

			WeakHashMap<String, String> map = new WeakHashMap<String, String>();
			map = data.get(position);
			viewHolder.textView_OperatorName.setText(map
					.get(OperatorActivity.OPERATOR_NAME).toString().trim());
			viewHolder.textView_OperatorID.setText(map
					.get(OperatorActivity.OPERATOR_ID).toString().trim());
			viewHolder.imageView_logo.setImageResource(OperatorConstant
					.getOperatorResource(Integer.parseInt(map
							.get(OperatorActivity.OPERATOR_PRODUCT_ID))));
		} catch (Exception e) {
		}
		return convertView;
	}

	private class ViewHolder {
		TextView textView_OperatorName, textView_OperatorID;
		ImageView imageView_logo;
	}

}
