//
//  MyLikeService.m
//  Pay1
//
//  Created by Annapurna on 17/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "MyLikeService.h"
#import "MyLikeResponse.h"
@implementation MyLikeService
-(void)callMyLikeService:(NSString *)likeOfferID callback:(myLikeResponse)callback{
    NSMutableDictionary *postData = [[NSMutableDictionary alloc]init];
    [postData setObject:likeOfferID forKey:@"id"];
    
    NSLog(@"PostData for MyLike:%@",postData);

    RequestData *requestData = [[RequestData alloc]init];
    requestData.baseURL = kBaseURL;
    requestData.webServiceURL = kMyLikeURL;
    requestData.postData = postData;
    requestData.showLoader = FALSE;
    
    requestData.requestSuccessBlock = ^(RequestData *request) {
        
    [self printResponse:request.responseData forRequest:@"MyLike Response"];
    if ([self handleNullResponse:request])
    return;
        
    callback(nil, [self parseMyLikeResponse:request]);
    };
    
    
    requestData.requestFailureBlock = ^(RequestData *request) {
    [self handleNetworkError:request.responseData];
    callback(request.responseData, nil);
    };
    
    [[ServiceManager sharedInstance] makeRequest:requestData];

}

-(MyLikeResponse *)parseMyLikeResponse:(RequestData *)request{
    MyLikeResponse *objResponse = [[MyLikeResponse alloc]init];
    objResponse.status = [request.responseData objectForKey:@"status"];
    objResponse.errorCode = [request.responseData objectForKey:@"errCode"];
    NSMutableDictionary *descDic = [request.responseData objectForKey:@"description"];
    objResponse.message = [descDic objectForKey:@"msg"];
    objResponse.likeStatus = [[descDic objectForKey:@"status"]integerValue] == 1 ? TRUE : FALSE;;
    
    return objResponse;

}

@end
