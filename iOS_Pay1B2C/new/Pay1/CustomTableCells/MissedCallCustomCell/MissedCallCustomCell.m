//
//  MissedCallCustomCell.m
//  Pay1
//
//  Created by Annapurna on 14/07/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "MissedCallCustomCell.h"

@implementation MissedCallCustomCell

- (void)awakeFromNib {
    // Initialization code
    
    if([UIScreen mainScreen].bounds.size.width == 320){
    _priceLabel.frame = CGRectMake(320 - 60, 15, 70, 21);
    _cellSwitch.frame = CGRectMake(320 - 60, 41,30, 31);
        
    _missedNumLabel.font = [UIFont fontWithName:@"Open Sans" size:12.0];
        
        
    }
    
    
    _logoImageView.layer.cornerRadius = _logoImageView.frame.size.height/2;
    _logoImageView.clipsToBounds = YES;
    _logoImageView.layer.masksToBounds = YES;
    
    _logoImageView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _logoImageView.layer.borderWidth = 1;
    
   // [_priceLabel sizeToFit];

    
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
    NSArray *tblView=[[NSBundle mainBundle]loadNibNamed:@"MissedCallCustomCell" owner:self options:nil];
    self=[tblView objectAtIndex:0];
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
