package com.pay1;

import com.larvalabs.svgandroid.SVGParser;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainPagerFragment extends Fragment {

	String title = "???", subtitle = "???";
	private static final String TITLE = "title";
	private static final String SUB_TITLE = "subtitle";

	public static MainPagerFragment newInstance(String title, String subtitle) {
		MainPagerFragment fragment = new MainPagerFragment();
		fragment.title = title;
		fragment.subtitle = subtitle;
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if ((savedInstanceState != null)
				&& savedInstanceState.containsKey(TITLE)
				&& savedInstanceState.containsKey(SUB_TITLE)) {
			title = savedInstanceState.getString(TITLE);
			subtitle = savedInstanceState.getString(SUB_TITLE);
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View viewLayout = null;
		// if (title.equalsIgnoreCase("1")) {

		Typeface Medium = Typeface.createFromAsset(getActivity().getAssets(),
				"EncodeSansNarrow-500-Medium.ttf");
		viewLayout = inflater.inflate(R.layout.tour_at_registration, container,
				false);
		LinearLayout parent_layout = (LinearLayout) viewLayout
				.findViewById(R.id.parent_layout);
		ImageView imageView_tour = (ImageView) viewLayout
				.findViewById(R.id.imageView_tour);

		TextView textMessage = (TextView) viewLayout
				.findViewById(R.id.textMessage);
		textMessage.setTypeface(Medium);
		textMessage.setText(subtitle);

		if (title.equalsIgnoreCase("1")) {
			textMessage.setVisibility(View.GONE);
			/*imageView_tour.setBackgroundDrawable(SVGParser.getSVGFromResource(
					getResources(), R.raw.login_first_screen)*/
			
					//.createPictureDrawable());
			
			imageView_tour.setBackgroundDrawable(getResources().getDrawable(R.drawable.login_first_screen));
			textMessage.setVisibility(View.GONE);
		} else if (title.equalsIgnoreCase("2")) {
			imageView_tour.setBackgroundDrawable(SVGParser.getSVGFromResource(
					getResources(), R.raw.map_tour).createPictureDrawable());
		} else if (title.equalsIgnoreCase("3")) {
			imageView_tour.setBackgroundDrawable(SVGParser.getSVGFromResource(
					getResources(), R.raw.recharge_tour)
					.createPictureDrawable());
		} else if (title.equalsIgnoreCase("4")) {
			imageView_tour.setBackgroundDrawable(SVGParser.getSVGFromResource(
					getResources(), R.raw.money_bag_tour)
					.createPictureDrawable());
		} else if (title.equalsIgnoreCase("5")) {
			imageView_tour.setBackgroundDrawable(SVGParser.getSVGFromResource(
					getResources(), R.raw.gift_tour).createPictureDrawable());
		}

		/*
		 * } else {
		 * 
		 * viewLayout = inflater.inflate(R.layout.registration_pager_adapter,
		 * container, false);
		 * 
		 * ImageView imageView_Logo1 = (ImageView) viewLayout
		 * .findViewById(R.id.imageView_Logo1); if (title.equalsIgnoreCase("2"))
		 * { imageView_Logo1.setImageDrawable(getActivity().getResources()
		 * .getDrawable(R.drawable.ic_map)); } else if
		 * (title.equalsIgnoreCase("3")) {
		 * imageView_Logo1.setImageDrawable(getActivity().getResources()
		 * .getDrawable(R.drawable.ic_recharge)); } else if
		 * (title.equalsIgnoreCase("4")) {
		 * imageView_Logo1.setImageDrawable(getActivity().getResources()
		 * .getDrawable(R.drawable.ic_earn_coins)); } else if
		 * (title.equalsIgnoreCase("5")) {
		 * imageView_Logo1.setImageDrawable(getActivity().getResources()
		 * .getDrawable(R.drawable.ic_gift)); }
		 * 
		 * Typeface Reguler = Typeface.createFromAsset(getActivity()
		 * .getAssets(), "EncodeSansNormal-400-Regular.ttf");
		 * 
		 * TextView textView_Title = (TextView) viewLayout
		 * .findViewById(R.id.textView_Title);
		 * textView_Title.setTypeface(Reguler);
		 * 
		 * TextView textView_SubTitle = (TextView) viewLayout
		 * .findViewById(R.id.textView_SubTitle);
		 * textView_SubTitle.setTypeface(Reguler);
		 * 
		 * try { textView_Title.setText(title);
		 * textView_SubTitle.setText(subtitle);
		 * 
		 * } catch (Exception e) { } }
		 */
		return viewLayout;
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putString(TITLE, title);
		outState.putString(SUB_TITLE, subtitle);
	}
}
