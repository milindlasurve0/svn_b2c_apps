package com.pay1.adapterhandler;

import java.util.ArrayList;
import java.util.WeakHashMap;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.larvalabs.svgandroid.SVGParser;
import com.pay1.R;

public class CallListAdapterLocation extends BaseAdapter {
	private Context context;
	String[] data;
	LayoutInflater inflater;
	Typeface Reguler, Medium;

	public CallListAdapterLocation(Context context, String[] data) {
		this.context = context;
		this.data = data;
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		Reguler = Typeface.createFromAsset(context.getAssets(),
				"EncodeSansNormal-400-Regular.ttf");
		Medium = Typeface.createFromAsset(context.getAssets(),
				"EncodeSansNarrow-500-Medium.ttf");
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.length;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.length;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		// if (convertView == null) {
		try {
			convertView = inflater
					.inflate(R.layout.call_adapter_location, null);
			final ViewHolder viewHolder = new ViewHolder();

			viewHolder.text_area = (TextView) convertView
					.findViewById(R.id.text_area);
			viewHolder.imageCall = (ImageView) convertView
					.findViewById(R.id.imageCall);
			viewHolder.imageCall.setImageDrawable(SVGParser.getSVGFromResource(
					context.getResources(), R.raw.contact_list_icon)
					.createPictureDrawable());

			viewHolder.text_area.setText(data[position]);
			viewHolder.text_area.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					String firstNumber = viewHolder.text_area.getText()
							.toString();
					if (!firstNumber.isEmpty()) {
						Intent callIntent = new Intent(Intent.ACTION_CALL);
						callIntent.setData(Uri.parse("tel:" + firstNumber));
						context.startActivity(callIntent);
					} else {
						Toast.makeText(context, "No number available",
								Toast.LENGTH_LONG).show();
					}
				}
			});
		} catch (Exception exc) {
			Log.d("msg", exc.getMessage());
		}
		// }
		return convertView;
	}

	private class ViewHolder {
		ImageView imageCall;
		TextView text_area, text_adderess;
	}
}
