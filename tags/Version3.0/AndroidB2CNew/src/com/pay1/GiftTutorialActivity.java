package com.pay1;

import com.larvalabs.svgandroid.SVGParser;
import com.pay1.utilities.Utility;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;

public class GiftTutorialActivity extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		/*
		 * getWindow().setBackgroundDrawable( new
		 * ColorDrawable(Color.TRANSPARENT));
		 */
		getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
		setContentView(R.layout.sample_tutorial_layout);

		RelativeLayout rl = (RelativeLayout) findViewById(R.id.my_relative_layout);

		ImageView img_thumb_icon = new ImageView(this);
		ImageView img_top_arrow = new ImageView(this);
		img_thumb_icon.setImageDrawable(getResources().getDrawable(
				R.drawable.ic_launcher));
		img_top_arrow.setImageDrawable(getResources().getDrawable(
				R.drawable.ic_launcher));
		img_top_arrow.setScaleType(ScaleType.FIT_CENTER);
		img_thumb_icon.setScaleType(ScaleType.FIT_CENTER);

		/*
		 * img_top_arrow.setImageDrawable(SVGParser .getSVGFromResource(
		 * GiftTutorialActivity.this .getResources(), R.raw.arrow_notification)
		 * .createPictureDrawable());
		 * 
		 * img_thumb_icon.setImageDrawable(SVGParser .getSVGFromResource(
		 * GiftTutorialActivity.this .getResources(), R.raw.hand_notification)
		 * .createPictureDrawable());
		 */
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
				200, 200);
		params.leftMargin = MyFreeBiesDetailsActivity.thunbleft - 100;
		params.topMargin = MyFreeBiesDetailsActivity.thunbtop;
		rl.addView(img_thumb_icon, params);

		RelativeLayout.LayoutParams params2 = new RelativeLayout.LayoutParams(
				200, 200);
		params2.leftMargin = MyFreeBiesDetailsActivity.arrowleft - 50;
		params2.topMargin = MyFreeBiesDetailsActivity.arrowtop;
		rl.addView(img_top_arrow, params2);

		// setContentView(R.layout.gift_tutorial_activity);
		/*
		 * Button parent_layout=(Button)findViewById(R.id.parent_layout);
		 * parent_layout.setOnClickListener(new OnClickListener() {
		 * 
		 * @Override public void onClick(View v) { // TODO Auto-generated method
		 * stub finish(); } });
		 */

		ImageView mOverLayImage = (ImageView) findViewById(R.id.over_lay_image);
		mOverLayImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Utility.saveshowGiftTutorial(GiftTutorialActivity.this, true);
				finish();
			}
		});
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		Utility.saveshowGiftTutorial(GiftTutorialActivity.this, true);
		finish();
	}
}
