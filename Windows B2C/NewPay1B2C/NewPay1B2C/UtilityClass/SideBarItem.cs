﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NewPay1B2C.Model
{
    class SideBarItem
    {

        private string _Name;
        private string _imagePath;
        private int index;

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public string ImagePath
        {
            get { return _imagePath; }
            set { _imagePath = value; }
        }

        public int Index
        {
            get { return index; }
            set { index = value; }
        }



        public SideBarItem(string Name, int index, String imagePath)
        {
            this.Name = Name;
            this.Index = index;
            this.ImagePath=imagePath;
        }


    }
}
