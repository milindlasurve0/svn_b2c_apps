package com.pay1;

import java.math.BigInteger;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.SQLException;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.larvalabs.svgandroid.SVGParser;
import com.pay1.constants.Constants;
import com.pay1.customviews.MyProgressDialog;
import com.pay1.databasehandler.NumbberWithCircleDataSource;
import com.pay1.databasehandler.NumberWithCircle;
import com.pay1.databasehandler.Operator;
import com.pay1.databasehandler.OperatorDataSource;
import com.pay1.requesthandler.RequestClass;
import com.pay1.utilities.EditTextValidator;
import com.pay1.utilities.Utility;

public class VerificationActivity extends Activity {

	private static final String SCREEN_LABEL = "OTP Screen";
	private EasyTracker easyTracker = null;
	private static final int HASH_EQUEST_CODE = 111;
	private final String TAG = "Sign Up";
	private TextView textView_Mobile, textView_Timer, textView_MSG,
			textView_mob, textView_CountryCode;
	private EditText editText_OTP, editText_Pin;
	private Button button_Submit, button_Resend;
	private ImageView imageView_Edit;
	String hash;
	boolean is_new;
	private OperatorDataSource operatorDataSource;
	private NumbberWithCircleDataSource circleDataSource;

	@SuppressLint("NewApi")
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.verification_activity);
		try {
			easyTracker = EasyTracker.getInstance(VerificationActivity.this);
			easyTracker.set(Fields.SCREEN_NAME, SCREEN_LABEL);
			operatorDataSource = new OperatorDataSource(
					VerificationActivity.this);
			circleDataSource = new NumbberWithCircleDataSource(
					VerificationActivity.this);
			try {
				operatorDataSource.open();

				Operator operator = operatorDataSource.getTopOperator();

				if (operator != null) {
					int days = (int) ((System.currentTimeMillis() - operator
							.getOperatorUptateTime()) / (1000 * 60 * 60 * 24));
					if (days >= 2) {
						new GetOperatorTask().execute();
					}
				} else {
					new GetOperatorTask().execute();
				}

			} catch (SQLException exception) {
				// TODO: handle exception
				// // // Log.e("Er ", exception.getMessage());
			} catch (Exception e) {
				// TODO: handle exception
				// // // Log.e("Er ", e.getMessage());
			} finally {
				operatorDataSource.close();
			}

			Typeface Reguler = Typeface.createFromAsset(getAssets(),
					"Roboto-Regular.ttf");
			// Typeface Medium = Typeface.createFromAsset(getAssets(),
			// "Roboto-Regular.ttf");
			ImageView splash_image = (ImageView) findViewById(R.id.splash_image);
			splash_image.setImageDrawable(SVGParser.getSVGFromResource(
					VerificationActivity.this.getResources(),
					R.raw.pay1_new_logo).createPictureDrawable());
			textView_Mobile = (TextView) findViewById(R.id.textView_Mobile);
			textView_Mobile.setTypeface(Reguler);
			textView_CountryCode = (TextView) findViewById(R.id.textView_CountryCode);
			textView_CountryCode.setTypeface(Reguler);
			textView_mob = (TextView) findViewById(R.id.textView_mob);
			textView_mob.setVisibility(View.GONE);
			try {
				textView_Mobile.setText("+91 "
						+ getIntent().getStringExtra("MOBILE"));
				textView_mob.setText(getIntent().getStringExtra("MOBILE"));
				hash = getIntent().getStringExtra("OTP");
				is_new = getIntent().getBooleanExtra("IS_NEW", false);
			} catch (Exception e) {
				textView_Mobile.setText("");
			}

			button_Submit = (Button) findViewById(R.id.button_Submit);
			button_Submit.setTypeface(Reguler);
			button_Submit.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					submitClick(v);
				}
			});

			button_Resend = (Button) findViewById(R.id.button_Resend);
			button_Resend.setTypeface(Reguler);
			button_Resend.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (is_new) {
						new RegistrationTask().execute(textView_mob.getText()
								.toString().trim());
						new CountDownTimer(180000, 1000) {

							public void onTick(long millisUntilFinished) {
								long millis = millisUntilFinished;
								String hms = String
										.format("%02d:%02d",
												TimeUnit.MILLISECONDS
														.toMinutes(millis)
														- TimeUnit.HOURS
																.toMinutes(TimeUnit.MILLISECONDS
																		.toHours(millis)),
												TimeUnit.MILLISECONDS
														.toSeconds(millis)
														- TimeUnit.MINUTES
																.toSeconds(TimeUnit.MILLISECONDS
																		.toMinutes(millis)));
								textView_Timer.setText(hms);
							}

							public void onFinish() {
								textView_Timer.setText("00:00");
								textView_Timer.setVisibility(View.GONE);
								button_Resend.setVisibility(View.VISIBLE);
								textView_MSG
										.setText("Looks like there is some problem in sending SMS, try re-sending it.");
							}
						}.start();
					} else {
						// Intent intent = new Intent(VerificationActivity.this,
						// ForgotPasswordActivity.class);
						// intent.putExtra("IS_FLAG", is_new);
						// startActivity(intent);
						new ForgotPasswordTask().execute(textView_mob.getText()
								.toString().trim());
					}
				}
			});

			if (!is_new)
				button_Resend.setText("Forgot Password");
			else
				button_Resend.setVisibility(View.GONE);
			imageView_Edit = (ImageView) findViewById(R.id.imageView_Edit);

			imageView_Edit.setImageDrawable(SVGParser.getSVGFromResource(
					getResources(), R.raw.ic_edit).createPictureDrawable());

			if (Build.VERSION.SDK_INT >= 11)
				imageView_Edit.setLayerType(View.LAYER_TYPE_SOFTWARE, null);

			imageView_Edit.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent intent = new Intent(VerificationActivity.this,
							RegistrationActivity.class);
					startActivity(intent);
					finish();
				}
			});

			textView_MSG = (TextView) findViewById(R.id.textView_MSG);
			textView_MSG.setTypeface(Reguler);
			textView_Timer = (TextView) findViewById(R.id.textView_Timer);
			textView_Timer.setTypeface(Reguler);

			editText_Pin = (EditText) findViewById(R.id.editText_Pin);
			editText_Pin.setTypeface(Reguler);
			editText_Pin.setVisibility(View.GONE);
			editText_Pin.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					// TODO Auto-generated method stub

				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					// TODO Auto-generated method stub

				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

				}
			});

			editText_OTP = (EditText) findViewById(R.id.editText_OTP);
			editText_OTP.setTypeface(Reguler);
			editText_OTP.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					// TODO Auto-generated method stub

				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					// TODO Auto-generated method stub

				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

				}
			});

			editText_OTP
					.setOnEditorActionListener(new TextView.OnEditorActionListener() {

						@Override
						public boolean onEditorAction(TextView v, int actionId,
								KeyEvent event) {
							// TODO Auto-generated method stub
							if (actionId == EditorInfo.IME_ACTION_GO) {
								submitClick(v);
								return true;
							}
							return false;
						}
					});

			editText_Pin
					.setOnEditorActionListener(new TextView.OnEditorActionListener() {

						@Override
						public boolean onEditorAction(TextView v, int actionId,
								KeyEvent event) {
							// TODO Auto-generated method stub
							if (actionId == EditorInfo.IME_ACTION_GO) {
								submitClick(v);
								return true;
							}
							return false;
						}
					});

			if (is_new) {
				textView_MSG
						.setText("You will receive one time password (OTP) via SMS. Kindly enter below");
				editText_Pin.setVisibility(View.GONE);
				editText_OTP.setVisibility(View.VISIBLE);
				textView_Timer.setVisibility(View.VISIBLE);
				button_Submit.setText("Verify");
				new CountDownTimer(180000, 1000) {

					public void onTick(long millisUntilFinished) {
						long millis = millisUntilFinished;
						String hms = String
								.format("%02d:%02d",
										TimeUnit.MILLISECONDS.toMinutes(millis)
												- TimeUnit.HOURS
														.toMinutes(TimeUnit.MILLISECONDS
																.toHours(millis)),
										TimeUnit.MILLISECONDS.toSeconds(millis)
												- TimeUnit.MINUTES
														.toSeconds(TimeUnit.MILLISECONDS
																.toMinutes(millis)));
						textView_Timer.setText(hms);
					}

					public void onFinish() {
						textView_Timer.setText("00:00");
						button_Resend.setVisibility(View.VISIBLE);
						textView_MSG
								.setText("Looks like there is some problem in sending SMS, try re-sending it.");
					}
				}.start();
			} else {
				textView_MSG.setText("");
				editText_Pin.setVisibility(View.VISIBLE);
				editText_OTP.setVisibility(View.GONE);
				textView_Timer.setVisibility(View.GONE);
				button_Submit.setText("Login");
			}
		} catch (Exception e) {
			// Log.e("sl", e.getMessage());
		}
	}

	@Override
	protected void onNewIntent(Intent intent) {
		// TODO Auto-generated method stub
		super.onNewIntent(intent);
		String opt = null;
		Bundle bundle = intent.getExtras();
		if (bundle != null)
			opt = bundle.getString("OTP").trim();

		editText_OTP.setText(opt);

		// String str = getMD5EncryptedString(textView_mob.getText().toString()
		// .trim())
		// + getMD5EncryptedString(editText_OTP.getText().toString()
		// .trim());
		// if (str.equalsIgnoreCase(hash)) {
		// Utility.setMobileNumber(VerificationActivity.this,
		// Constants.SHAREDPREFERENCE_MOBILE, textView_mob.getText()
		// .toString().trim());
		// Utility.setPin(VerificationActivity.this,
		// Constants.SHAREDPREFERENCE_PIN, editText_OTP.getText()
		// .toString().trim());
		// Utility.setLoginFlag(VerificationActivity.this,
		// Constants.SHAREDPREFERENCE_IS_LOGIN, true);
		// try {
		// startService(new Intent(VerificationActivity.this,
		// SignUpService.class));
		// } catch (Exception e) {
		// }
		// Intent intent1 = new Intent(VerificationActivity.this,
		// MainActivity.class);
		// intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
		// | Intent.FLAG_ACTIVITY_NEW_TASK);
		// intent.putExtra("IS_FLAG", is_new);
		// startActivity(intent1);
		// finish();
		//
		// }
	}

	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView().build());
		// EasyTracker.getInstance(this).activityStart(this);
		easyTracker.send(MapBuilder.createAppView().build());
	}

	@Override
	public void onStop() {
		super.onStop();
		// EasyTracker.getInstance(this).activityStop(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onActivityResult(int, int,
	 * android.content.Intent)
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		try {
			if (requestCode == HASH_EQUEST_CODE) {
				if (resultCode == Activity.RESULT_OK) {
					hash = data.getStringExtra("OTP");
				}
			}
		} catch (Exception e) {
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		Intent intent = new Intent(VerificationActivity.this,
				RegistrationActivity.class);
		startActivity(intent);
		finish();
	}

	private void submitClick(View v) {
		try {
			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
			if (is_new
					&& !EditTextValidator
							.hasText(VerificationActivity.this, editText_OTP,
									Constants.ERROR_LOGIN_PIN_BLANK_FIELD)) {
				return;
			} else if (!is_new
					&& !EditTextValidator
							.hasText(VerificationActivity.this, editText_Pin,
									Constants.ERROR_LOGIN_PIN_BLANK_FIELD)) {
				return;
			}
			String str;
			if (is_new)
				str = getMD5EncryptedString(textView_mob.getText().toString()
						.trim())
						+ getMD5EncryptedString(editText_OTP.getText()
								.toString().trim());
			else
				str = getMD5EncryptedString(textView_mob.getText().toString()
						.trim())
						+ getMD5EncryptedString(editText_Pin.getText()
								.toString().trim());
			if (str.equalsIgnoreCase(hash)) {

				Utility.setMobileNumber(VerificationActivity.this,
						Constants.SHAREDPREFERENCE_MOBILE, textView_mob
								.getText().toString().trim());
				Utility.setLoginFlag(VerificationActivity.this,
						Constants.SHAREDPREFERENCE_IS_LOGIN, true);
				if (is_new) {
					Utility.setPin(VerificationActivity.this,
							Constants.SHAREDPREFERENCE_PIN, editText_OTP
									.getText().toString().trim());
					// try {
					// startService(new Intent(
					// VerificationActivity.this,
					// SignUpService.class));
					// } catch (Exception e) {
					// }
				} else {
					Utility.setPin(VerificationActivity.this,
							Constants.SHAREDPREFERENCE_PIN, editText_Pin
									.getText().toString().trim());
					// try {
					// startService(new Intent(
					// VerificationActivity.this,
					// VerificationActivity.class));
					// } catch (Exception e) {
					// }
				}
				try {
					/*
					 * startService(new Intent(VerificationActivity.this,
					 * LoginService.class));
					 */

					new LoginTask()
							.execute(
									Utility.getMobileNumber(
											VerificationActivity.this,
											Constants.SHAREDPREFERENCE_MOBILE),
									Utility.getPin(VerificationActivity.this,
											Constants.SHAREDPREFERENCE_PIN),
									Utility.getUUID(VerificationActivity.this,
											Constants.SHAREDPREFERENCE_UUID),
									Utility.getCurrentLatitude(
											VerificationActivity.this,
											Constants.SHAREDPREFERENCE_CURRENT_LATITUDE),
									Utility.getCurrentLongitude(
											VerificationActivity.this,
											Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE),
									Utility.getOSManufacturer(
											VerificationActivity.this,
											Constants.SHAREDPREFERENCE_OS_MANUFACTURER),
									Utility.getOSVersion(
											VerificationActivity.this,
											Constants.SHAREDPREFERENCE_OS_VERSION),
									Utility.getGCMID(VerificationActivity.this,
											Constants.SHAREDPREFERENCE_GCMID));

				} catch (Exception e) {
				}
				// if(Utility.getIsNewTour(VerificationActivity.this)){
				// Intent intent = new Intent(VerificationActivity.this,
				// TourFirstActivity.class);
				// intent.putExtra("IS_FLAG", is_new);
				// intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
				// | Intent.FLAG_ACTIVITY_NEW_TASK);
				// startActivity(intent);
				// finish();
				// }else{
				/*
				 * Intent intent = new Intent(VerificationActivity.this,
				 * MainActivity.class); intent.putExtra("IS_FLAG", is_new);
				 * intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
				 * Intent.FLAG_ACTIVITY_NEW_TASK); startActivity(intent);
				 * finish();
				 */

				/*
				 * Intent intent = new Intent(VerificationActivity.this,
				 * TourFirstActivity.class); intent.putExtra("IS_FLAG", is_new);
				 * intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
				 * Intent.FLAG_ACTIVITY_NEW_TASK); startActivity(intent);
				 * finish();
				 */
				// }
			} else {
				if (is_new)
					Constants.showOneButtonDialog(VerificationActivity.this,
							"OTP", "Invalid OTP", Constants.DIALOG_CLOSE);
				else
					Constants.showOneButtonDialog(VerificationActivity.this,
							"Password", "Invalid Password",
							Constants.DIALOG_CLOSE);
			}
		} catch (Exception e) {
		}
	}

	public static String getMD5EncryptedString(String encTarget) {
		MessageDigest mdEnc = null;
		try {
			mdEnc = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			// System.out.println("Exception while encrypting to md5");
			// e.printStackTrace();
		} // Encryption algorithm
		mdEnc.update(encTarget.getBytes(), 0, encTarget.length());
		String md5 = new BigInteger(1, mdEnc.digest()).toString(16);
		while (md5.length() < 32) {
			md5 = "0" + md5;
		}
		return md5;
	}

	public class RegistrationTask extends AsyncTask<String, String, String>
			implements OnDismissListener {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			try {
				String response = RequestClass.getInstance()
						.readPay1B2CRequest(
								VerificationActivity.this,
								Constants.B2C_URL
										+ "create_user/?mobile_number="
										+ params[0]);

				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			try {

				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");
					String bal, otp;
					JSONObject jsonObject = new JSONObject(replaced);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {

						JSONObject jsonObjectDesc = jsonObject
								.getJSONObject("description");

						bal = jsonObjectDesc.getString("balance");
						hash = jsonObjectDesc.getString("otp");
						Utility.setBalance(VerificationActivity.this,
								Constants.SHAREDPREFERENCE_BALANCE,
								jsonObjectDesc.getString("balance"));

					} else {

						JSONObject jsonObjectDesc = jsonObject
								.getJSONObject("description");

						bal = jsonObjectDesc.getString("balance");
						hash = jsonObjectDesc.getString("otp");

						// System.out.println("Bal " + bal);
						Utility.setBalance(VerificationActivity.this,
								Constants.SHAREDPREFERENCE_BALANCE,
								jsonObjectDesc.getString("balance"));

					}
				} else {
					Intent intent = new Intent(VerificationActivity.this,
							ConnectivityErrorActivity.class);
					startActivity(intent);
				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonDialog(VerificationActivity.this, TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonDialog(VerificationActivity.this, TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(VerificationActivity.this);
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							RegistrationTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			RegistrationTask.this.cancel(true);
			dialog.cancel();
		}

	}

	public class ForgotPasswordTask extends AsyncTask<String, String, String>
			implements OnDismissListener {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			try {

				String response = RequestClass.getInstance()
						.readPay1B2CRequest(
								VerificationActivity.this,
								Constants.B2C_URL + "forgotpwd/?user_name="
										+ URLEncoder.encode(params[0], "utf-8")
										+ "&vtype=1");

				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			try {

				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONObject jsonObject = new JSONObject(replaced);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {

						Intent returnIntent = new Intent(
								VerificationActivity.this,
								ChangePinActivity.class);
						returnIntent.putExtra("OTP",
								jsonObject.getString("otp"));
						returnIntent.putExtra("IS_FLAG", is_new);
						startActivity(returnIntent);
						// finish();

					} else {
						Constants.showOneButtonDialog(
								VerificationActivity.this, TAG,
								Constants.checkCode(replaced),
								Constants.DIALOG_CLOSE_FORGOTPIN);
					}

				} else {
					Intent intent = new Intent(VerificationActivity.this,
							ConnectivityErrorActivity.class);
					startActivity(intent);

				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonDialog(VerificationActivity.this, TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonDialog(VerificationActivity.this, TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(VerificationActivity.this);
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							ForgotPasswordTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			ForgotPasswordTask.this.cancel(true);
			dialog.cancel();
		}

	}

	public class GetOperatorTask extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... params) {
			try {
				// String response = RequestClass.getInstance()
				// .readPay1B2CRequest(VerificationActivity.this,
				// Constants.B2C_URL + "get_all_operators/?");
				// return Constants.loadJSONFromAsset(VerificationActivity.this,
				// "operator.json");
				try {

					long timestamp = System.currentTimeMillis();
					operatorDataSource.open();
					String result;
					// if (!result.startsWith("Error")) {
					// replaced = result.replace("(", "").replace(")", "")
					// .replace(";", "");
					// } else {
					result = Constants.loadJSONFromAsset(
							VerificationActivity.this, "operator.json");
					// }
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");
					JSONObject jsonObject = new JSONObject(replaced);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {
						JSONObject description = jsonObject
								.getJSONObject("description");
						JSONArray mobile = description.getJSONArray("mobile");
						JSONArray data = description.getJSONArray("data");
						JSONArray postpaid = description
								.getJSONArray("postpaid");
						JSONArray dth = description.getJSONArray("dth");
						operatorDataSource.deleteOperator();
						operatorDataSource.createOperator(mobile,
								Constants.RECHARGE_MOBILE, timestamp);
						operatorDataSource.createOperator(data,
								Constants.RECHARGE_DATA, timestamp);
						operatorDataSource.createOperator(postpaid,
								Constants.BILL_PAYMENT, timestamp);
						operatorDataSource.createOperator(dth,
								Constants.RECHARGE_DTH, timestamp);

					}

				} catch (JSONException e) {
				} catch (SQLException exception) {
				} finally {
					operatorDataSource.close();
				}
				return "";
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			try {
				circleDataSource.open();

				NumberWithCircle operator_code = circleDataSource
						.getTopNumberDetails();

				if (operator_code != null) {
					int days = (int) ((System.currentTimeMillis() - operator_code
							.getLastUpdateTime()) / (1000 * 60 * 60 * 24));
					if (days >= 10) {
						// Intent circleServiceIntent = new Intent(
						// MainActivity.this, CircleIntentService.class);
						// startService(circleServiceIntent);
						new GetOperatorCodeTask().execute();
					}
				} else {
					// Intent circleServiceIntent = new
					// Intent(MainActivity.this,
					// CircleIntentService.class);
					// startService(circleServiceIntent);
					new GetOperatorCodeTask().execute();
				}

			} catch (SQLException exception) {
				// TODO: handle exception
				// // // Log.e("Er ", exception.getMessage());
			} catch (Exception e) {
				// TODO: handle exception
				// // // Log.e("Er ", e.getMessage());
			} finally {
				circleDataSource.close();
			}

		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}
	}

	public class GetOperatorCodeTask extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... params) {
			try {
				// String time = Utility.getSharedPreferences(MainActivity.this,
				// Constants.LAST_CIRCLE_UPDATED) == null ? "" : Utility
				// .getSharedPreferences(MainActivity.this,
				// Constants.LAST_CIRCLE_UPDATED);
				// String response = RequestClass
				// .getInstance()
				// .readPay1B2CRequest(
				// MainActivity.this,
				// Constants.B2B_URL
				// + "method=getMobileDetails&mobile=all&timestamp="
				// + time);

				String response = Constants.loadJSONFromAsset(
						VerificationActivity.this, "operatorcode.json");
				// parseResponse(response);
				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}
	}

	private void parseResponse(String response) {
		// TODO Auto-generated method stub
		try {
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date date = new Date();
			String timeStamp = dateFormat.format(date);
			// Log.d("Plan", "Plan Start Circle Start Thread Time loop"
			// + timeStamp);

			circleDataSource.open();

			if (!response.startsWith("Error")) {
				String replaced = response.replace("(", "").replace(")", "")
						.replace(";", "");
				// // Log.d("Circle  ", "Plan  "+response);
				replaced = replaced.substring(1, replaced.length() - 1);
				/* JSONArray array = new JSONArray(replaced); */
				// JSONObject jsonObject = new JSONObject(replaced);
				// String status = jsonObject.getString("status");
				// if (status.equalsIgnoreCase("success")) {
				circleDataSource.deleteCircle();
				circleDataSource.createNumberAndCircleList(replaced, timeStamp,
						System.currentTimeMillis());
				Utility.setSharedPreferences(VerificationActivity.this,
						Constants.LAST_CODE_UPDATED,
						String.valueOf(System.currentTimeMillis()));

				// }
			}
		} catch (SQLException exception) {
			// Log.d("NAC", "row inserted   " + exception.getMessage());
			// TODO: handle exception
			// // // Log.e("Er ", exception.getMessage());
		} catch (Exception e) {
			// Log.d("NAC", "row inserted   " + e.getMessage());
			// TODO: handle exception
			// // // Log.e("Er ", e.getMessage());
		} finally {
			circleDataSource.close();
		}
	}

	public class LoginTask extends AsyncTask<String, String, String> {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			try {

				String response = RequestClass.getInstance()
						.readPay1B2CRequest(
								VerificationActivity.this,
								Constants.B2C_URL + "signin/?username="
										+ URLEncoder.encode(params[0], "utf-8")
										+ "&password="
										+ URLEncoder.encode(params[1], "utf-8")
										+ "&uuid=" + params[2] + "&latitude="
										+ params[3] + "&longitude=" + params[4]
										+ "&manufacturer=" + params[5]
										+ "&os_info=" + params[6]
										+ "&gcm_reg_id=" + params[7]
										+ "&device_type=android&api_version="
										+ Constants.API_VERSION);

				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			super.onPostExecute(result);
			// Log.e("Login", result);
			try {
				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONObject jsonObject = new JSONObject(replaced);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {
						Utility.setLoginFlag(VerificationActivity.this,
								Constants.SHAREDPREFERENCE_IS_LOGIN, true);
						JSONObject jsonObject2 = jsonObject
								.getJSONObject("description");

						Utility.setUserName(VerificationActivity.this,
								Constants.SHAREDPREFERENCE_NAME, jsonObject2
										.getString("name").trim());
						Utility.setMobileNumber(VerificationActivity.this,
								Constants.SHAREDPREFERENCE_MOBILE, jsonObject2
										.getString("mobile").trim());
						Utility.setEmail(VerificationActivity.this,
								Constants.SHAREDPREFERENCE_EMAIL, jsonObject2
										.getString("email").trim());
						Utility.setGender(VerificationActivity.this,
								Constants.SHAREDPREFERENCE_GENDER, jsonObject2
										.getString("gender").trim());
						Utility.setUserID(VerificationActivity.this,
								Constants.SHAREDPREFERENCE_USER_ID, jsonObject2
										.getString("user_id").trim());
						Utility.setBalance(VerificationActivity.this,
								Constants.SHAREDPREFERENCE_BALANCE, jsonObject2
										.getString("wallet_balance").trim());
						Utility.setDOB(VerificationActivity.this,
								Constants.SHAREDPREFERENCE_DOB, jsonObject2
										.getString("date_of_birth").trim());
						Utility.setLatitude(VerificationActivity.this,
								Constants.SHAREDPREFERENCE_LATITUDE,
								jsonObject2.getString("latitude").trim());
						Utility.setLongitude(VerificationActivity.this,
								Constants.SHAREDPREFERENCE_LONGITUDE,
								jsonObject2.getString("longitude").trim());
						Utility.setSupportNumber(VerificationActivity.this,
								Constants.SHAREDPREFERENCE_SUPPORT_NUMBER,
								jsonObject2.getString("support_number").trim());
						Utility.setCookieName(VerificationActivity.this,
								Constants.SHAREDPREFERENCE_COOKIE_NAME,
								jsonObject2.getString("user_val_name"));
						Utility.setCookieValue(VerificationActivity.this,
								Constants.SHAREDPREFERENCE_COOKIE_VALUE,
								jsonObject2.getString("user_val"));
						Utility.setMyTotalGifts(VerificationActivity.this,
								jsonObject2.getString("total_gifts"));

						Utility.setSharedPreferences(VerificationActivity.this,
								"Deals_offer",
								jsonObject2.getString("Deals_offer"));
						Utility.setBalance(VerificationActivity.this,
								Constants.SHAREDPREFERENCE_LOYALTY_POINTS,
								jsonObject2.getString("loyalty_points"));
						Utility.setMyLikes(VerificationActivity.this,
								jsonObject2.getString("total_mylikes"));

						Utility.setMyTotalRedeem(VerificationActivity.this,
								jsonObject2.getString("total_redeem"));

						Utility.setMyReviews(VerificationActivity.this,
								jsonObject2.getString("total_myreviews"));
						JSONObject jsonObjectTemplate = jsonObject2
								.getJSONObject("ErrorMsg");
						Utility.setTemplate(
								VerificationActivity.this,
								Constants.SHAREDPREFERENCE_SMS_BILLPAYMENT_FAILURE,
								jsonObjectTemplate
										.getString("BILLPAYMENT_FAILURE"));
						Utility.setTemplate(
								VerificationActivity.this,
								Constants.SHAREDPREFERENCE_SMS_RECHARGE_FAILURE,
								jsonObjectTemplate
										.getString("RECHARGE_FAILURE"));
						Utility.setTemplate(
								VerificationActivity.this,
								Constants.SHAREDPREFERENCE_SMS_WALLET_REFILLED_RETAILER,
								jsonObjectTemplate
										.getString("WALLET_REFILLED_RETAILER"));

						Utility.setSharedPreferences(
								VerificationActivity.this,
								Constants.SHAREDPREFERENCE_SERVICE_CHARGE_PERCENT,
								jsonObject2.getString("service_charge_percent")
										.trim());

						Utility.setSharedPreferences(VerificationActivity.this,
								Constants.SHAREDPREFERENCE_SERVICE_TAX_PERCENT,
								jsonObject2.getString("service_tax_percent")
										.trim());
						if(jsonObject2.getString("profile_image").contains("images/")){
							Utility.setFbImageId(VerificationActivity.this,"http://b2c.pay1.in/"+
									jsonObject2.getString("profile_image"));
						}else{
							Utility.setFbImageId(VerificationActivity.this,
									jsonObject2.getString("profile_image"));
						}
						
						Utility.setProfileDataSource(VerificationActivity.this,
								jsonObject2.getString("fb_data"));

						/*
						 * if(!jsonObject2.getString("fb_data").isEmpty()||
						 * jsonObject2.getString("fb_data").length()!=0){
						 * JSONObject fbJsonObject=new
						 * JSONObject(jsonObject2.getString("fb_data"));
						 * 
						 * Utility.setUserName(VerificationActivity.this,
						 * Constants.SHAREDPREFERENCE_NAME, fbJsonObject
						 * .getString("name").trim());
						 * Utility.setMobileNumber(VerificationActivity.this,
						 * Constants.SHAREDPREFERENCE_MOBILE, jsonObject2
						 * .getString("mobile").trim());
						 * Utility.setEmail(VerificationActivity.this,
						 * Constants.SHAREDPREFERENCE_EMAIL, fbJsonObject
						 * .getString("email").trim());
						 * Utility.setGender(VerificationActivity.this,
						 * Constants.SHAREDPREFERENCE_GENDER, fbJsonObject
						 * .getString("gender").trim());
						 * 
						 * String imageId="https://graph.facebook.com/" +
						 * fbJsonObject.getString("id")+ "/picture?type=large";
						 * Utility
						 * .setFbImageId(VerificationActivity.this,imageId ); }
						 */
					/*	Intent intent = new Intent(VerificationActivity.this,
								TourFirstActivity.class);
						intent.putExtra("IS_FLAG", is_new);
						intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
								| Intent.FLAG_ACTIVITY_NEW_TASK);
						startActivity(intent);
						finish();*/
						
						Intent intent = new Intent(VerificationActivity.this,
								MainActivity.class);
						intent.putExtra("IS_FLAG",
								getIntent().getExtras().getBoolean("IS_FLAG"));
						intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
								| Intent.FLAG_ACTIVITY_NEW_TASK);
						startActivity(intent);
						finish();

					} else {

					}

				} else {
				}
			} catch (JSONException e) {
				// e.printStackTrace();

			} catch (Exception e) {
				// TODO: handle exception

			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(VerificationActivity.this);
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							LoginTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}
	}

}