//
//  GiftDetailService.h
//  Pay1
//
//  Created by Annapurna on 18/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "Service.h"
@class GiftDetailResponse;
@class GiftDetailCriteria;
typedef void (^giftDetailResponse)(NSError *error, GiftDetailResponse *giftDetailResponse);
@interface GiftDetailService : Service
-(void)callGiftDetailService:(GiftDetailCriteria *)giftDealCriteria callback:(giftDetailResponse)callback;
@end
