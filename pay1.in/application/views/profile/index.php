<?php $this->load->view('common/doctype_html');  
    $current_page_url =  "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    $meteTagData = getMetaTagDetails();
    foreach ($meteTagData as $row){
      
      if($row->page_url == $current_page_url){
        $new_page_tittle =  $row->page_tittle;
        $new_page_meta_description =  $row->page_meta_description;  
        $new_page_meta_tag =  $row->page_meta_tag;  
      }
      
    }

?>
<meta name="description" content="<?php if($new_page_meta_description){ echo $new_page_meta_description; }else{ ?>Pay1 is a convenient cash top up app for Mobile, DTH recharge & bill payment which offers Gift coins on every recharge to Graba Gift.  <?php } ?>">
<meta name="keywords" content="<?php if($new_page_meta_tag){ echo $new_page_meta_tag; }else{ ?>Payments , Pay1  <?php } ?>" />
<title><?php if($new_page_tittle){ echo $new_page_tittle; }else{ ?>About Me | Pay1- Recharge, Earn Gift Coins & Grab a Gift   <?php } ?></title> 
<?php $this->load->view('common/header');
$userData = getUserDetails();

$total_likes = isset($userData['mylikes']) ? $userData['mylikes'] : 0;
$total_gifts = isset($userData['totalgifts']) ? $userData['totalgifts'] : 0 ;
?>
    <link rel="stylesheet" href="/assets/css/profile.css">
    <div class="clearfix" style="clear:both;"></div>
    <div class="profile_top">
        <div class="container ">
            <div class='text-center'>
                <img src="/assets/images/profile/user_img.jpg" class="img-circle profile_img" alt=""><br>
                <div class="top_name"><?php echo isset($userData["name"]) ? $userData["name"] : ""; ?></div>
            </div>
        </div>
    <div class="profile_nav">
        <div class="container">
            <ul class="top_menu">
                    <?php if($total_likes !=0) { ?>
                <a href="/my-likes"><li><i class="glyphicon glyphicon-heart-empty"></i> <?php  echo $total_likes; ?><br>MY LIKES</li></a>
                    <?php } else { ?>
                    <li><i class="glyphicon glyphicon-heart-empty"></i><br>MY LIKES</li>
                    <?php } ?>
                <li><img src="/assets/images/profile/icons.png" alt=""> <?php echo isset($userData['loyaltypoints']) ? $userData['loyaltypoints'] : 0  ?>
                    <br>MY COINS</li>
                <li><img src="/assets/images/profile/cons2.png" alt=""> <?php echo isset($userData['walletbal']) ? $userData['walletbal'] : 0  ?>
                    <br>MY MONEY</li>
                <?php if($total_gifts !=0) { ?>
                <a href="/my-gifts"><li class="last"><img src="/assets/images/profile/profile_cion.png" alt=""> <?php  echo $total_gifts; ?><br>MY GIFTS</li></a>
                    <?php } else { ?>
                    <li class="last"><img src="/assets/images/profile/profile_cion.png" alt=""><br>MY GIFTS</li>
                    <?php } ?>
                    
            </ul>
        </div>
    </div>
    </div>

    <div class="clearfix"></div>

    <div class="content">
        <div class="margin-top-40 clearfix"></div>
        <div class="container">
          <div class="row">
            <div class="col-md-3 profile_tab">
                <ul class="nav nav-pills nav-stacked profile_ul">
                  <li class="active"><a href="#pinfo" data-toggle="pill" id="pinfoli">Personal Info<i class="glyphicon glyphicon-chevron-right glyicon "></i></a></li>
                  <li><a href="#thistory" data-toggle="pill" onclick="getTransactionHistory('0');" >Transaction History <i class="glyphicon glyphicon-chevron-right glyicon "></i></a></li>
                  <li><a href="#whistory" data-toggle="pill" onclick="getWalletHistory('0');" >Wallet History<i class="glyphicon glyphicon-chevron-right glyicon "></i></a></li>
                  <li><a href="#chistory" data-toggle="pill"  onclick="getGiftcoinHistory();" >Gift Coin History<i class="glyphicon glyphicon-chevron-right glyicon "></i></a></li>
                </ul>
            </div>
            <div class="col-md-9">
              <div class="tab-content col-md-10">
                <div class="tab-pane active" id="pinfo">
                  <div class="profile">
                      <span class="text-center col-md-6 col-md-offset-2 profile_name"><label id="name_label"><?php echo isset($userData["name"]) ? $userData["name"] : "" ; ?></label></span><a href="" class=" col-md-4 text-right edit_profile" id="edit_pr"> EDIT &nbsp; <i class="glyphicon glyphicon-edit glyicon"></i></a>
                    <br><br>
                    <table class="table table-responsive border-none">
                       <tr><td>Email id:</td><td><span id="email_label"><?php echo isset($userData["email"]) ? $userData["email"] : ""; ?></span></td></tr>
                       <tr><td>Mobile No.:</td><td><span id="mobile_label"><?php echo isset($userData["mobile"]) ? $userData["mobile"] : ""; ?></span></td></tr>
                       <tr><td>Gender:</td><td><span  id="gender_label"><?php if (isset($userData["gender"]) && $userData["gender"] == "male") echo 'Male'; else echo 'female'; ?></span></td></tr>
                       <tr><td>Date Of Birth:</td><td><span  id="dob_label"><?php echo isset($userData["dob"]) ? $userData["dob"] : ""; ?></span></td></tr>
                       <tr><td>Password:</td><td>***********</td>
                           <!--<tr><td>Password:</td><td>***********<a class=" text-right edit_profile" id="edit_pswrd" onclick="send_OTP()"> <i class="glyphicon glyphicon-edit glyicon" style="cursor: pointer"></i></a></td>-->
                       </tr>                                              
                    </table>
                  </div>

                <div class="eprofile" id="edit_profile">
                  <div class="padding-10">
                    <p class="text-center"><b>Edit Personal Information</b></p><br>
<!--                    <form action="" method="post" id="updateprofile">-->
                    <table class="table table-responsive border-none">
                        <tr><td>Name:</td><td><input type="text" id="name" name="name" class="form-control" value="<?php echo isset($userData["name"]) ? $userData["name"] : ""; ?>"></td></tr>
                        <tr><td>Email id:</td><td><input type="email" id="email" name="email" class="form-control" value="<?php echo isset($userData["email"]) ? $userData["email"] : ""; ?>"></td></tr>
                        <tr><td>Mobile No.::</td><td><?php echo isset($userData["mobile"]) ? $userData["mobile"] : "" ; ?></td></tr>
                        <tr><td>Gender:</td><td><input type="radio" name="gender" value="male" <?php echo (isset($userData["gender"]) && $userData["gender"]=='male')?'checked':'' ?> > Male <input type="radio" name="gender" value="female" <?php // echo ($userData["gender"]=='female')?'checked':'' ?> > Female</td></tr>
                        <tr><td>Date of Birth:</td><td><input type="text" id="dob" name="dob" class="form-control" value="<?php echo isset($userData["dob"]) ? $userData["dob"] : ""; ?>"></td></tr>
                        <!--<tr><td>Password:</td><td><input type="password" id="password" name="password" maxlength="16" class="form-control" value="<?php // echo isset($userData["password"]) ? $userData["password"] : "" ; ?>"></td></tr>-->
                        <tr><td>Password:</td><td>***********<a class=" text-right edit_profile" id="edit_pswrd" onclick="send_OTP(<?php echo $userData["mobile"]; ?>)"> <i class="glyphicon glyphicon-edit glyicon" style="cursor: pointer"></i></a></td>

                                    </table>
                                    <input type="button" class="btn btn-clear pull-right" value="Cancel" style="border-radius:5px">
                                    <input type="button" class="btn btn-save pull-right save" value="Save Changes" style="border-radius:5px"></tr>
                                    <div class="clearfix"></div>
<!--                    </form>-->
                  </div>
                </div>
                </div>
                  
                <div class="loader" style="display:none;">
                    <center>
                        <img class="loading-image" src="/images/bx_loader.gif" alt="loading..">
                    </center>
                </div>   

                <div class="tab-pane" id="thistory" style="overflow-y: scroll; height:370px;">
                    <table class="table table-responsive t_history border-none " id="purchasehistory">    
                        <thead>
                        </thead>
                    </table>
                </div>

                <div class="tab-pane" id="whistory" style="overflow-y: scroll; height:370px;">
                    <table class="table table-responsive w_history border-none1 " id="wallethistory">
                        <thead> 
                        </thead>
                    </table>
                </div>
                  <div class="tab-pane" id="chistory" style="overflow-y: scroll; height:370px;">
                    <table class="table table-responsive c_history border-none1 " id="gifthistory" >
                      <thead>   
                      </thead>
                     </table>
                </div>
            <div class="tab-pane" id="iconnect">
                  <div class="padding-10">
               <h4> Share it on your social network:</h4>
              <p><b>Spread the word. Benefit your friends by inviting them to Pay1.</b></p>
               <br><br>
               <table class="table table-responsive border-none text-center" style="width:100%">

                   
                  <tr ><td ><a 
                              href="http://www.facebook.com/sharer.php?s=100&p[summary]=SUMMARY&p[url]=http://web.pay1.loc/profile&p[title]=Hello"><img src="/assets/images/profile/facebook.png" alt="" class="img-circle img-responsive1"></a><br>Facebook</td>
                          <td ><a href="#"><img src="/assets/images/profile/twitter.png" alt="" class="img-circle img-responsive1"></a><br>Twitter</td>
                          <td ><a href="#">
                                  <img src="/assets/images/profile/google.png" class="img-circle img-responsive1"></a><br>Google Plus</td>
                          <td ><a href="#"><img src="/assets/images/profile/whatsapp.png" alt="" class="img-circle img-responsive1"></a><br>WhatsApp</td>
                          <td ><a href="#"><img src="/assets/images/profile/email.png" alt="" class="img-circle img-responsive1"></a><br>Mail</td>

                  </tr>
               </table>
                  </div>
          </div>
          <div class="tab-pane" id="notification">
                  <div class="padding-10">
                          <ul class="profile_list">
                                  <li>You Visited Malad<br>
                                          See all gift Shops.
                                          Near Chincholi Bunder Location
                                          <p class="blue_date">Yesterday 10:11 AM</p>
                                  </li>
                                  <li>You Visited Malad<br>
                                          See all gift Shops.
                                          Near Chincholi Bunder Location
                                          <p class="blue_date">Yesterday 10:11 AM</p>
                                  </li>
                                  <li>You Visited Malad<br>
                                          See all gift Shops.
                                          Near Chincholi Bunder Location
                                          <p class="blue_date">Yesterday 10:11 AM</p>
                                  </li>

                          </ul>
                  </div>
          </div>
<!--          <div class="tab-pane" id="support">
               <div class="padding-10">
                  <p class="profile_name text-center">Ask Your Query Here</p>
                    <form action="contact/ContactData" id="profile_support" method="POST" >
                        <select name="issue" class="form-control contact_input validate[required]">
                            <option>RECHARGE ISSUE</option>
                            <option>GIFT ISSUE</option>
                            <option>OTHERS</option>
                        </select><br>
                        <textarea rows="6" class="form-control" name="support_msg"></textarea>
                        <input type="submit" class="btn btn-clear pull-right" value="Clear">
                        <input type="submit" class="btn btn-send pull-right" value="Send">

                    </form>

                          <div class="clearfix"></div>
                          <br>
                          <table class="table table-responsive border-none">

                  <tr ><td ><img src="/assets/images/icons/each_rupee_ofrecharge.png" alt="" class="img-circle img-responsive"></td><td>Write us on listen@pay1.in</td></tr>

                  <tr ><td ><img src="/assets/images/icons/each_rupee_ofrecharge.png" alt="" class="img-circle img-responsive"></td><td>Give us a Missed Call to Raise the Complaint of your last Transation, on 023232323</td></tr>

               </table>
                  </div>
          </div>-->
                            <div class="tab-pane">
                                  <div class="padding-10">
                                     <p class="profile_name text-center">Ask Your Query Here</p>
                                             <form action="" method="">
                                                     <textarea rows="6" class="form-control" name="message"></textarea>
                                                     <input type="submit" class="btn btn-clear pull-right" value="Clear">
                                                     <input type="submit" class="btn btn-send pull-right" value="Send">

                                             </form>
                                             <table class="table table-responsive border-none">

                                     <tr ><td><img src="/assets/images/icons/each_rupee_ofrecharge.png" alt="" class="img-circle img-responsive"></td><td>Write us on listen@pay1.in</td></tr>

                                     <tr ><td><img src="/assets/images/icons/each_rupee_ofrecharge.png"  alt="" class="img-circle img-responsive"></td><td>Give us a Missed Call to Raise the Complaint of your last Transation, on 023232323</td></tr>

                                  </table>
                                     </div>
                             </div>
                     </div><!-- tab content -->
     </div>
                        </div>
                </div>
                <div class="margin-top-40 clearfix"></div>
                </div>


        <div class="clearfix"></div>
        
        <!---********************************************model more gift coins****************************************** -->
        <div class="modal fade" role="dialog" id="profile_editsuccess" aria-hidden="true">
           <div class="modal-dialog">
              <div class="modal-content">
                 <div class="modal-header1">
                    <button class="close" data-dismiss="modal">&times; </button>
                    <div class="text-center"><h4>Congratulations </h4></div>
                 </div>
                 <div class="modal-body clearfix text-center">
                    <div class="text-center"><h4 id='grab_error'>Your profile has been updated successfully</h4></div>
                    
                        <div class="margin-top-50"></div>
                        <div class="col-md-4"></div>
                        <div class="col-md-4" style="text-align:center"><button class="btn btn-default btn-block" data-dismiss="modal">DONE</button></div>
                        <div class="col-md-4"></div>

                 </div>


              </div>
           </div>
        </div>
       <!---********************************************model end ****************************************** -->
       
        <!---********************************************model more gift coins****************************************** -->
        <div class="modal fade" role="dialog" id="profile_editerro" aria-hidden="true">
           <div class="modal-dialog">
              <div class="modal-content">
                 <div class="modal-header1">
                    <button class="close" data-dismiss="modal">&times; </button>
                    <div class="text-center"><h4>Oops something went wrong </h4></div>
                 </div>
                 <div class="modal-body clearfix text-center">
                    <div class="text-center"><h4 id='grab_error'>You're profile not updated, please try after some tome</h4></div>

                 </div>


              </div>
           </div>
        </div>
       <!---********************************************model end ****************************************** -->
    
    <!---********************************************model more gift coins****************************************** -->
        <div class="modal fade" role="dialog" id="profile_password_update_success" aria-hidden="true">
           <div class="modal-dialog">
              <div class="modal-content">
                 <div class="modal-header1">
                    <button class="close" data-dismiss="modal">&times; </button>
                    <div class="text-center"><h4>Congratulation </h4></div>
                 </div>
                 <div class="modal-body clearfix text-center">
                    <div class="text-center"><h4 id='grab_error'>You're password updated successfully</h4></div>
                    
                        <div class="margin-top-50"></div>
                        <div class="col-md-4"></div>
                        <div class="col-md-4" style="text-align:center"><button class="btn btn-default btn-block" data-dismiss="modal">DONE</button></div>
                        <div class="col-md-4"></div>

                 </div>


              </div>
           </div>
        </div>
    <!---********************************************model end ****************************************** -->
       
   <!---******************************************** model edit password ****************************************** -->
        <div class="modal fade" role="dialog" id="edit_password_modal" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                   <div id='edit_password_div'  class="modal-body clearfix">
                    <button class="model_disbtn" data-dismiss="modal">× </button>
                    <h4 class="modal-title" id="loginmsg" align="center" style="color:blue;"></h4>
                    
                    <div class="col-sm-12">
                      <h4 class="text-center">RESET PASSWORD</h4>
                    </div>
                    
                    <div class="col-sm-6 border" style="border-right:1px solid gray;">
                      <div class="col-sm-12">
                       <form method="post" id="edit_password_form" action = '' onsubmit = "return checkProfile_OTP_Password(event,'<?php echo $userData["mobile"]; ?>')" >        
                            <h5 >You will receive an OTP</h5>
                            <h6 >Fill in the required information below</h6>
                            <span id ="edit_sent_otp" style="color:blue;"></span>
                            <div class="form-group">
                             <input  id ="edit_forget_otp"  name="edit_forget_otp" type="text" placeholder="OTP" autocomplete="off" class="form-control" aria-describedby="basic-addon1" onfocus="clearsentOTP()" required>
                            </div>
                            <div class="form-group">
                                <input name="edit_forget_password" id="edit_forget_password" maxlength="16"  placeholder="New Password" type="password" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <input name="edit_confirm_password" id="edit_confirm_password" maxlength="16" placeholder="Confirm Password" type="password" class="form-control" onblur="profile_password_match()" required>
                            </div>
                            <span id ="edit_wrong_password" style="color:red;"></span> 
                            <span id ="edit_wrong_otp" style="color:red;"></span> 
                            <span id ="edit_invalid_otp" style="color:red;"></span>
                            <div class="form-group">
                              <input type="submit" Value="Confirm" class="btn btn-danger ">
                              <input id="edit_resend_otp" type="button" Value="Resend OTP" class="btn btn-danger" onclick="profile_send_OTP('<?php echo $userData["mobile"]; ?>')">
                            </div>
                            <span id ="edit_correct_otp" style="color:blue;"></span> 
                            <span id ="edit_correct_password" style="color:blue;"></span> 
                       </form>
                      </div>
                    </div>
                    
                    <div class="otp-message-wrapper col-sm-6 ">
                      <div class="margin-top-40">
                        <i class="otp-message-txt">We will send One Time Password (OTP) on your mobile to reset your password.</i>
                      </div>
                    </div>
                    
                   </div>
                </div>
            </div>
        </div>
       <!---******************************************** model edit password end ****************************************** -->   
<!--     <script src="/assets/js/bootstrap.min.js"></script>
     <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>-->
     <script type="text/javascript" src="<?php echo base_url('js/profile.js'); ?>"></script>

<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '1636095206674498',
      xfbml      : true,
      version    : 'v2.4'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
   
   FB.ui(
   {
   method: 'share',
   href: 'https://developers.facebook.com/docs/'
   }, function(response){});
   
   
</script>        
 
<script src="/assets/js/jquery-scrollToTop.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('body').scrollToTop({skin: 'cycle'});
    
$("#edit_pr").click(function(e){
        e.preventDefault();
  $(".eprofile").show();
  $('.profile').hide();
});
});

     $('#edit_pswrd').click(function () {
         
                $('#edit_password_modal').modal({
                    show: true
                });
     });


$(".save").click(function(){
    b2c.core.edit_user_details();
    
});
$(".btn-clear").click(function(){
    $(".eprofile").hide();
    $('.profile').show();
});
</script>

	
    
    
 <?php $this->load->view('common/footer'); ?>