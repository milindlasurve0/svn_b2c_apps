package com.pay1.adapterhandler;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.pay1.R;

public class DealQuantityAdapter extends BaseAdapter {

	private Context context;
	private String[] quantity = { "1", "2", "3", "4", "5", "6", "7", "8", "9",
			"10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20" };
	private Typeface Reguler, Narrow, Narrow_thin;

	public DealQuantityAdapter(Context context) {
		this.context = context;
		Reguler = Typeface.createFromAsset(context.getAssets(),
				"EncodeSansNormal-400-Regular.ttf");

		Narrow = Typeface.createFromAsset(context.getAssets(),
				"EncodeSansNarrow-500-Medium.ttf");

		Narrow_thin = Typeface.createFromAsset(context.getAssets(),
				"EncodeSansNarrow-100-Thin.ttf");
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return quantity.length;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return quantity[position];
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder viewHolder;
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.spinner_row, null);
			viewHolder = new ViewHolder();

			viewHolder.textOperator = (TextView) convertView
					.findViewById(R.id.textOperator);
			viewHolder.textOperator.setTypeface(Reguler);

			convertView.setTag(viewHolder);

		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		viewHolder.textOperator.setText(quantity[position]);

		return convertView;
	}

	private class ViewHolder {
		TextView textOperator;
	}
}
