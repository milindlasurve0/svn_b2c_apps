package com.pay1.servicehandler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.UnknownHostException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.IntentService;
import android.content.Intent;
import android.database.SQLException;

import com.pay1.constants.Constants;
import com.pay1.databasehandler.Plan;
import com.pay1.databasehandler.PlanDataSource;
import com.pay1.utilities.Utility;

public class PlanIntentService extends IntentService {
	PlanDataSource planDataSource;

	// DateFormat dateFormat;
	// String values = "";

	public PlanIntentService() {
		super("PlanIntentService");
		// TODO Auto-generated constructor stub
		planDataSource = new PlanDataSource(PlanIntentService.this);
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		callPlanApi();

	}

	String jString;

	public void callPlanApi() {
		new Thread(new Runnable() {
			public void run() {

				// HttpClient httpclient = new DefaultHttpClient();
				// // Log.d(TAG,HomeView.webserviceUrl+"applogout");
				// String time = Utility.getSharedPreferences(
				// PlanIntentService.this, Constants.LAST_CIRCLE_UPDATED);
				/*
				 * HttpPost httppost = new HttpPost(Constants.API_URL +
				 * "method=getMobileDetails&mobile=all&timestamp" + time);
				 */
				// String LastUpdateTime = Utility.getSharedPreferences(
				// PlanIntentService.this, Constants.LAST_PLANS_UPDATED);
				// if (LastUpdateTime != null) {
				try {
					planDataSource.open();

					Plan plans = planDataSource.getTopPlan();
					if (plans != null) {
						int days = (int) ((System.currentTimeMillis() - Long
								.parseLong(plans.getPlanUptateTime())) / (1000 * 60 * 60 * 24));
						if (days >= 2) {
							BufferedReader reader = null;
							try {

								// jString = RequestClass
								// .getInstance()
								// .readPay1B2BRequest(
								// PlanIntentService.this,
								// Constants.B2B_URL
								// +
								// "method=getPlanDetails&operator=all&circle=all");

								HttpPost httpPost = new HttpPost(
										Constants.B2B_URL
												+ "method=getPlanDetails&operator=all&circle=all");
								// httpPost.setEntity(new
								// UrlEncodedFormEntity(nameValuePairs));
								HttpClient httpClient = new DefaultHttpClient();
								HttpResponse httpResponse = httpClient
										.execute(httpPost);
								StatusLine statusLine = httpResponse
										.getStatusLine();
								int statusCode = statusLine.getStatusCode();

								if (statusCode == 200) {
									HttpEntity entity = httpResponse
											.getEntity();
									InputStream content = entity.getContent();
									StringBuilder builder = new StringBuilder();
									reader = new BufferedReader(
											new InputStreamReader(content));
									String line;
									while ((line = reader.readLine()) != null) {
										builder.append(line);
									}
									jString = (builder.toString().trim() != null || builder
											.toString().trim() != "") ? builder
											.toString().trim()
											: Constants.ERROR_SERVER;
								} else {
									// // //
									// Log.e(RequestClass.class.toString(),
									// "Failed to connect");
									jString = Constants.ERROR_SERVER;
								}

							} catch (UnknownHostException e) {
								// e.printStackTrace();
								// // // Log.i("fail ", e.getMessage());
								jString = Constants.ERROR_SERVER;
							} catch (ClientProtocolException e) {
								// e.printStackTrace();
								// // // Log.i("fail ", e.getMessage());
								jString = Constants.ERROR_SERVER;
							} catch (IOException ex) {
								// e.printStackTrace();
								// // // Log.i("fail ", e.getMessage());
								jString = Constants.ERROR_SERVER;
							} catch (Exception e) {
								// e.printStackTrace();
								// // // Log.i("fail ", e.getMessage());
								jString = Constants.ERROR_SERVER;
							} finally {
								if (reader != null) {
									try {
										reader.close();
									} catch (final IOException e) {
										// e.printStackTrace();
										jString = Constants.ERROR_SERVER;
									}
								}

							}
						}
					} else {
						try {
							jString = Constants.loadJSONFromAsset(
									PlanIntentService.this, "plan.json");
						} catch (Exception e) {
							jString = "Error";
						}
					}
				} catch (Exception e) {
				} finally {
					planDataSource.close();
				}
				// Date date2 = new Date();

				// String timeStamp2 = dateFormat.format(date2);
				// Log.d("Plan", "Plan Start Time  end response "
				// + timeStamp2);
				try {
					if (jString.startsWith("Error")) {
						try {
							jString = Constants.loadJSONFromAsset(
									PlanIntentService.this, "plan.json");
						} catch (Exception e) {
							jString = "Error";
						}
					}
					if (!jString.startsWith("Error")) {
						String replaced = jString.replace("(", "")
								.replace(")", "").replace(";", "");

						replaced = replaced.substring(1, replaced.length() - 1);
						// replaced = replaced.substring(0,
						// replaced.length() -
						// 1);
						// Log.d("Plan", "Plan  " + replaced);
						savePlansTodataBase(replaced);
						/*
						 * JSONArray array = new JSONArray(replaced); //int
						 * count = array.length(); for (int i = 0; i <
						 * array.length(); i++) { JSONObject jsonObject =
						 * array.getJSONObject(i);
						 * 
						 * savePlansTodataBase(jsonObject.toString()); }
						 */
					}

				} catch (Exception e) {
					// e.printStackTrace();
				}
			}

		}).start();

	}

	/*
	 * public class PlanIntentServiceTask extends AsyncTask<String, String,
	 * String> {
	 * 
	 * @Override protected String doInBackground(String... params) {
	 * planDataSource.open(); String time =
	 * Utility.getSharedPreferences(PlanIntentService.this,
	 * Constants.LAST_PLANS_UPDATED);
	 * 
	 * Date date = new Date();
	 * 
	 * String timeStamp = dateFormat.format(date); // Log.d("Start Time",
	 * "Plan Start Time  response " + timeStamp);
	 * 
	 * ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();
	 * 
	 * listValuePair .add(new BasicNameValuePair("method", "getPlanDetails"));
	 * listValuePair.add(new BasicNameValuePair("operator", "all"));
	 * listValuePair.add(new BasicNameValuePair("circle", "all")); String
	 * response = RequestClass.getInstance().readPay1RequestForPlan(
	 * PlanIntentService.this, Constants.API_URL, listValuePair);
	 * 
	 * 
	 * String response = Constants.getPlanData(PlanIntentService.this,
	 * Constants.API_URL + "method=getPlanDetails&operator=all&circle=all");
	 * 
	 * Date date2 = new Date();
	 * 
	 * String timeStamp2 = dateFormat.format(date2); // Log.d("Start Time",
	 * "Plan Start Time  end response " + timeStamp2); try {
	 * 
	 * if (!response.startsWith("Error")) { String replaced =
	 * response.replace("(", "") .replace(")", "").replace(";", "");
	 * 
	 * replaced = replaced.substring(1, replaced.length() - 1); //replaced =
	 * replaced.substring(0, replaced.length() - 1); //// Log.d("Plan  ",
	 * "Plan  " + response); savePlansTodataBase(replaced); JSONArray array =
	 * new JSONArray(replaced); //int count = array.length(); for (int i = 0; i
	 * < array.length(); i++) { JSONObject jsonObject = array.getJSONObject(i);
	 * 
	 * savePlansTodataBase(jsonObject.toString()); } } } catch (Exception ee) {
	 * 
	 * } return response; }
	 * 
	 * @Override protected void onPostExecute(String result) { // TODO
	 * Auto-generated method stub super.onPostExecute(result);
	 * 
	 * try { stopSelf(); } catch (Exception exception) { // Log.e("Error:",
	 * "Error    "); }
	 * 
	 * }
	 * 
	 * }
	 */

	private void savePlansTodataBase(String response) {
		try {
			planDataSource.open();
			planDataSource.deletePlan();

			long timestamp = System.currentTimeMillis();
			planDataSource.createPlan(response, timestamp);
			Utility.setSharedPreferences(PlanIntentService.this,
					Constants.LAST_PLANS_UPDATED, String.valueOf(timestamp));

		} catch (SQLException exception) {
			// TODO: handle exception
			// Log.e("Er ", exception.getMessage());
		} catch (Exception e) {
			// TODO: handle exception
			// System.out.println("Plan " + e.getMessage());
		} finally {
			planDataSource.close();

		}
	}
}
