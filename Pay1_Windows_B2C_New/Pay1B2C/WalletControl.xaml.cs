﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Pay1B2C.UserControls;
using Pay1B2C.Model;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Pay1B2C.ViewModel;
using System.Windows.Shapes;
using System.Windows.Markup;

namespace Pay1B2C
    {
    public partial class WalletControl: UserControl
        {
        public WalletControl()
            {
            InitializeComponent();

            //var path = new Path();
            //path.Data = Geometry. Parse("M 100,200 C 100,25 400,350 400,175 H 280");


            System.Windows.Shapes.Path newPath = (System.Windows.Shapes.Path)System.Windows.Markup.XamlReader.Load("<Path xmlns='http://schemas.microsoft.com/winfx/2006/xaml/presentation'  Width='30' Height='30'  Fill='#FFFFFFFF' Data='M12,2.267c-5.366,0-9.731,4.366-9.731,9.733c0,5.366,4.365,9.732,9.731,9.732s9.731-4.366,9.731-9.732 C21.731,6.633,17.366,2.267,12,2.267z M12,20.599c-4.741,0-8.599-3.857-8.599-8.599S7.259,3.401,12,3.401S20.599,7.259,20.599,12 S16.741,20.599,12,20.599z M12,0C5.384,0,0,5.383,0,12s5.384,12,12,12s12-5.383,12-12S18.616,0,12,0z M12,22.865 C6.009,22.865,1.133,17.991,1.133,12S6.009,1.134,12,1.134S22.867,6.009,22.867,12S17.991,22.865,12,22.865z M8.403,16.605 c0,0.473,0.385,0.855,0.855,0.855h2.255v-3.179h-3.11V16.605z M8.403,11.125v2.184h3.11v-3.04H9.258 C8.788,10.269,8.403,10.652,8.403,11.125z M14.979,6.741c-1.094-1.09-2.976,2.576-2.976,2.576s-1.914-3.479-2.975-2.576 c-1.248,1.068,1.333,3.01,2.975,3.01C13.646,9.751,16.139,7.903,14.979,6.741z M12.488,17.461h2.252 c0.473,0,0.857-0.383,0.857-0.855v-2.323h-3.11V17.461z M15.596,11.125c0-0.472-0.383-0.855-0.855-0.855h-2.254v3.04h3.11V11.125z' HorizontalAlignment='Center' VerticalAlignment='Center' Margin='0,4,0,0'/>");
            Path path = XamlReader.Load("<Path xmlns='http://schemas.microsoft.com/winfx/2006/xaml/presentation' Stroke='Blue' Data='M 0 0 Q 10 10 20 0'/>") as Path;

            coinPanel.Children.Add(newPath);




            textWalletBalance.Text="Rs. "+Constant.getbalance();
            textCoinsBalance.Text=Constant.getLoyalityPoints();
            List<SideBarItem> sideItem = new List<SideBarItem>();
            sideItem.Add(new SideBarItem("HOME", Constant.SIDE_BAR_HOME, "/Pay1Images/ic_home.png"));
            sideItem.Add(new SideBarItem("RECHARGE & PAY BILLS", Constant.SIDE_BAR_RECHARGE_N_BILLS, "/Pay1Images/                                  recharges_bills.png"));
           // sideItem.Add(new SideBarItem("Free Gifts", Constant.SIDE_BAR_MY_GIFTS, "/Pay1Images/freegifts.png"));
            sideItem.Add(new SideBarItem("GIFTS", Constant.SIDE_BAR_MISSED_CALL, "/Pay1Images/mygifts.png"));
            //sideItem.Add(new SideBarItem("My Transactions", Constant.SIDE_BAR_MY_TRANS, "/Pay1Images/mytransaction.png"));
            sideItem.Add(new SideBarItem("HISTORY", Constant.SIDE_BAR_MISSED_CALL, "/Pay1Images/ic_wallethistory.png"));
            sideItem.Add(new SideBarItem("MISSED CALL RECHARGE", Constant.SIDE_BAR_MISSED_CALL, "/Pay1Images/missedcall.png"));
            sideItem.Add(new SideBarItem("ACCOUNT & SUPPORT", Constant.SIDE_BAR_MISSED_CALL, "/Pay1Images/myprofile.png"));
            sideItem.Add(new SideBarItem("LOGOUT", Constant.SIDE_BAR_MISSED_CALL, "/Pay1Images/logout.png"));
            textUserName.Text=Constant.getUserName();

            textUserName.Text=textUserName.Text.ToUpper();

            string imgPath=Constant.getUserimage();
            if(string.IsNullOrEmpty(Constant.getUserimage()))
                {
                ImageBrush ib = new ImageBrush();

                ib.ImageSource = new BitmapImage(new Uri("/Images/ic_pay1_launcher.png", UriKind.Relative));
                profileEllipse.Fill = ib;

               // userImage.Source=new BitmapImage(new Uri("/Images/ic_pay1_launcher.png", UriKind.RelativeOrAbsolute));
                }
            else
                {
                ImageBrush ib = new ImageBrush();

                ib.ImageSource = new BitmapImage(new Uri(Constant.getUserimage(), UriKind.RelativeOrAbsolute));
                profileEllipse.Fill = ib;
                //userImage.Source=new BitmapImage(new Uri(Constant.getUserimage(), UriKind.RelativeOrAbsolute));
                }


            coinSidePanel.MouseEnter+=(a, b) =>
            {
            (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri("/Views/CoinHistory.xaml?", UriKind.RelativeOrAbsolute));
            };


            walletSidePanel.MouseEnter+=(a, b) =>
            {
            (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri("/Views/TopupWalletLayout.xaml?", UriKind.RelativeOrAbsolute));
            };
            
           
            foreach(SideBarItem SideBarItem1 in sideItem)
                {
                SideBarUserControl SIdeBarUserControl = new SideBarUserControl();
                //SIdeBarUserControl.icon.Source = new BitmapImage(new Uri(SideBarItem1.ImagePath, UriKind.RelativeOrAbsolute));
                SIdeBarUserControl.iconName.Text = SideBarItem1.Name;
                if(SideBarItem1.Name.Equals("LOGOUT"))
                    SIdeBarUserControl.iconName.Opacity=0.4;


                listSide.Items.Add(SIdeBarUserControl);
                }

            }

        /*
         * 
         * 
         * 
         * 
                case 5: (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri("/WalletHistory.xaml?", UriKind.RelativeOrAbsolute));
                    break;
         * 
         */

        private void sideBarList_SelectionChanged(object sender, SelectionChangedEventArgs e)
            {
            if(listSide.SelectedIndex == -1) return;
            Constant.SavePersistent("index", listSide.SelectedIndex);
            Constant.sideBarListHandle(listSide.SelectedIndex);
            switch(listSide.SelectedIndex)
                {

                case 0: (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri("/HomePage.xaml?", UriKind.RelativeOrAbsolute));
                    break;
                case 1:
                    (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri("/Views/RechargeMain.xaml?", UriKind.RelativeOrAbsolute));
                    break;
                case 2:
                    (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri("/GiftMainPivot.xaml?", UriKind.RelativeOrAbsolute));
                    break;
                case 3: (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri("/Views/History.xaml?", UriKind.RelativeOrAbsolute));
                    break;
              
                case 4:
                    (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri("/Views/MisscallRecharge.xaml?", UriKind.RelativeOrAbsolute));
                    break;
                case 5: (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri("/Views/AccountAndSupport.xaml?", UriKind.RelativeOrAbsolute));
                    break;
                case 6:
                    showLogoutDialog();
                    
                    
                    /*if(MessageBox.Show("Are you sure you want to logout.", "Pay1", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
                        {
                        LogOutTask();
                        }
                    else
                        {
                        }*/
                    break;


                }

            }


        public void showLogoutDialog()
            {

            CustomMessageBox messageBox = new CustomMessageBox()
            {
                //set the properties
                Caption = "Pay1",
                Message = "Are you sure you want to logout.",
                LeftButtonContent = "yes",
                RightButtonContent = "no"
            };

            //Add the dismissed event handler
            messageBox.Dismissed += (s1, e1) =>
            {
                switch(e1.Result)
                    {
                    case CustomMessageBoxResult.LeftButton:
                        //add the task you wish to perform when user clicks on yes button here
                        LogOutTask();
                        break;
                    case CustomMessageBoxResult.RightButton:
                        //add the task you wish to perform when user clicks on no button here

                        break;
                    case CustomMessageBoxResult.None:
                        // Do something.
                        break;
                    default:
                        break;
                    }
            };

            //add the show method
            messageBox.Show();

            }

        private async void LogOutTask()
            {
                
                //try
                //    {

                //    string strDel = "DELETE FROM NewFreeBieAllData";
                //    (Application.Current as App).dbh.Delete<NewFreeBieAllData>(strDel);
                //    }
                //catch(SQLiteException see)
                //    {
                //    }

                //try
                //    {

                //    string strDel = "DELETE FROM QuickPayAllDataTable";
                //    (Application.Current as App).dbh.Delete<QuickPayAllDataTable>(strDel);
                //    }
                //catch(SQLiteException see)
                //    {
                //    }


                //ProgressBarManager.Show (18, "Please wait...", false, 10);
                var values = new List<KeyValuePair<string, string>>();
                values.Add(new KeyValuePair<string, string>("actiontype", "signout"));

                values.Add(new KeyValuePair<string, string>("api", "true"));
                FormUrlEncodedContent values1 = new FormUrlEncodedContent(values);
                Task<string> response = Constant.postParamsAndgetResponse(Constant.B2C_URL, values1);

                string result = await response;
                if(string.IsNullOrEmpty(result))
                    {
                    (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri("/Views/NoConnection.xaml?", UriKind.Relative));
                    }
                else
                    {
                    var dict = (JObject)JsonConvert.DeserializeObject(result);
                    string status = dict["status"].ToString();
                    var description = dict["description"];
                    if(status.Equals("success"))
                        {

                        Constant.SavePersistent(Constant.SETTINGS_IS_LOGIN, false);
                        Constant.SavePersistent(Constant.SETTINGS_USER_WALLET_BALANCE, "0.00");

                        Constant.SavePersistent("cookieName", "111");
                        Constant.SavePersistent("cookieValue", "111");
                        (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri("/RegistrationLayout.xaml?", UriKind.Relative));
                        while((Application.Current.RootVisual as PhoneApplicationFrame).BackStack.Count() > 0)
                            {
                            (Application.Current.RootVisual as PhoneApplicationFrame).RemoveBackEntry();
                            }
                        MisscallDataHelper misscallDataHelper=new MisscallDataHelper();
                     //   misscallDataHelper.DeleteAllMissCall();
                        
                        }
                    else if(status.Equals("failure"))
                        {
                        Constant.checkForErrorCode(result); //MessageBox.Show (description.ToString ());
                        }
                    }
                //	ProgressBarManager.Hide (18);
                
            }
        }
    }
