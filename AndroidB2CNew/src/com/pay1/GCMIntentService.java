package com.pay1;

import static com.pay1.constants.Constants.SENDER_ID;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;

import com.google.android.gcm.GCMBaseIntentService;
import com.pay1.constants.Constants;
import com.pay1.utilities.Utility;

public class GCMIntentService extends GCMBaseIntentService {
	Context mContext;
	int i = 1234;
	public static int count = 0;
	//FileCache fileCache;
	String bal = "0";

	public GCMIntentService() {
		super(SENDER_ID);
	}

	// private static final String TAG = "===GCMIntentService===";

	@Override
	protected void onRegistered(Context arg0, String registrationId) {
		// Log.i(TAG, "Device registered: regId = " + registrationId);
		Utility.setGCMID(this, Constants.SHAREDPREFERENCE_GCMID, registrationId);
	}

	@Override
	protected void onUnregistered(Context arg0, String arg1) {
		// // Log.i(TAG, "unregistered = " + arg1);

	}

	@Override
	protected void onMessage(Context aContext, Intent arg1) {
		try {
			String msg = arg1.getExtras().getString("data");
			String description = "", title = "", type = "", img = "", offer_id = "";
			//fileCache = new FileCache(aContext);

			JSONObject jsonObject = new JSONObject(msg);
			// JSONObject jsonObject2 = jsonObject.getJSONObject("msg");
			description = jsonObject.getString("msg");
			title = jsonObject.getString("title");
			type = jsonObject.getString("type");
			bal = jsonObject.getString("bal");
			img = jsonObject.getString("img");
			offer_id = jsonObject.getString("offer_id");
			Utility.setSelectedGift(GCMIntentService.this,
					Constants.SHAREDPREFERENCE_SELECTED_GIFT,
					jsonObject.getString("offer_id"));

			Utility.setRefreshRequired(aContext,
					Constants.SHAREDPREFERENCE_REFESH_QUICKPAY, true);
			Utility.setRefreshRequired(aContext,
					Constants.SHAREDPREFERENCE_REFESH_MY_GIFT, true);
			Utility.setRefreshRequired(aContext,
					Constants.SHAREDPREFERENCE_REFESH_TRANSATION, true);
			Utility.setRefreshRequired(aContext,
					Constants.SHAREDPREFERENCE_REFESH_WALLET, true);

			// if (Utility.getLoginFlag(aContext,
			// Constants.SHAREDPREFERENCE_IS_LOGIN)) {
			if (jsonObject.getString("save").equalsIgnoreCase("3")) {
				Utility.setDailyFreeBieTime(aContext,
						Constants.SHAREDPREFERENCE_FREEBIES_TIME, 0);
			} else {
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
					if (img.length() == 0 || img.equalsIgnoreCase("null")
							|| img.equalsIgnoreCase(null) || img.isEmpty()
							|| img == null) {
						showOnlyTextNotification(aContext, title, description,
								img);
					} else {
						showImageNotification(aContext, title, description,
								img, offer_id);
					}
				} else {
					showOnlyTextNotification(aContext, title, description, img);
				}
			}
			// }
		} catch (JSONException je) {

		} catch (Exception e) {
		}

	}

	private void showOnlyTextNotification(final Context aContext, String title,
			final String msg, String img) {

		try {
			ActivityManager activityManager = (ActivityManager) this
					.getSystemService(ACTIVITY_SERVICE);
			String str = activityManager.getRunningTasks(1).get(0).topActivity
					.getPackageName();

			if (str.equalsIgnoreCase("com.pay1")) {

				// Toast.makeText(aContext, msg, Toast.LENGTH_SHORT).show();
				Intent intent = new Intent();
				intent.setAction("check.balance");
				intent.putExtra("value", msg);
				sendBroadcast(intent);
			} else {
				try {
					Bitmap icon = BitmapFactory.decodeResource(
							aContext.getResources(), R.drawable.ic_launcher);
					int notificationId;
					NotificationCompat.Builder builder = new NotificationCompat.Builder(
							this).setSmallIcon(R.drawable.ic_launcher)
							.setLargeIcon(icon).setContentTitle(title)
							.setContentText(msg).setAutoCancel(true);
					Intent notificationIntent;
					if (img.length() == 0 || img.equalsIgnoreCase("null")
							|| img.equalsIgnoreCase(null) || img.isEmpty()
							|| img == null) {
						notificationIntent = new Intent(this,
								MainActivity.class);
						notificationIntent.putExtra("bal", bal);
						notificationIntent.putExtra("desc", msg);
						notificationId = Constants.BALANCE_NOTIFICATION;
					} else {
						notificationIntent = new Intent(this,
								MainActivity.class);
						notificationIntent.putExtra("bal", bal);
						notificationIntent.putExtra("desc", msg);
						notificationId = Constants.DEAL_NOTIFICATION;
						notificationIntent.putExtra(Constants.RECHARGE_FOR,
								Constants.SUBSCRIBE_DEAL);
					}
					notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
							| Intent.FLAG_ACTIVITY_NEW_TASK);
					PendingIntent contentIntent = PendingIntent.getActivity(
							this, 0, notificationIntent,
							PendingIntent.FLAG_UPDATE_CURRENT);
					builder.setContentIntent(contentIntent);

					try {
						Uri notification1 = RingtoneManager
								.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
						Ringtone r = RingtoneManager.getRingtone(
								getApplicationContext(), notification1);
						r.play();

						Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

						// Vibrate for 1000 milliseconds
						v.vibrate(1000);
					} catch (Exception e) {
					}
					// Add as notification
					NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
					manager.notify(notificationId, builder.build());
				} catch (Exception e) {
				}
			}

		} catch (Exception e) {
		}

	}

	private void showBanner(Context aContext, String type, String description,
			String msg) {
		// TODO Auto-generated method stub
		// NotificationDataSource notificationDataSource = new
		// NotificationDataSource(
		// aContext);
		// try {
		// notificationDataSource.open();
		// notificationDataSource.createNotification(description, 1, ""
		// + System.currentTimeMillis());
		// } catch (SQLException exception) {
		// // TODO: handle exception
		// // // // Log.e("Er ", exception.getMessage());
		// } catch (Exception e) {
		// // TODO: handle exception
		// // // // Log.e("Er ", e.getMessage());
		// } finally {
		// notificationDataSource.close();
		// }
		// Intent notifyIntent = new Intent(this, NotificationActivity.class);
		// notifyIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
		// | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		// //
		// notifyIntent.putExtra("pushBundle", msg);
		//
		// aContext.startActivity(notifyIntent);
	}

	private void callNotification(Context aContext, String type,
			String description, String msg) {

		// NotificationDataSource notificationDataSource = new
		// NotificationDataSource(
		// aContext);
		// try {
		// notificationDataSource.open();
		// notificationDataSource.createNotification(description, 1, ""
		// + System.currentTimeMillis());
		// } catch (SQLException exception) {
		// // TODO: handle exception
		// // // // Log.e("Er ", exception.getMessage());
		// } catch (Exception e) {
		// // TODO: handle exception
		// // // // Log.e("Er ", e.getMessage());
		// } finally {
		// notificationDataSource.close();
		// }
		/*
		 * Intent notifyIntent = new Intent(this, NotificationActivity.class);
		 * notifyIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
		 * Intent.FLAG_ACTIVITY_SINGLE_TOP); //
		 * notifyIntent.putExtra("pushBundle", msg);
		 * 
		 * aContext.startActivity(notifyIntent);
		 */

	}

	@Override
	protected void onError(Context arg0, String errorId) {
		// // Log.i(TAG, "Received error: " + errorId);
	}

	@Override
	protected boolean onRecoverableError(Context context, String errorId) {
		return super.onRecoverableError(context, errorId);
	}

	@SuppressLint("NewApi")
	private void showImageNotification(Context mContext, String title,
			String msg, String url, String offer_id) {
		try {
			
			
			
			Bitmap icon = null;
			Intent notificationIntent;// = new Intent(this, DealActivity.class);
			if (url.length() == 0 || url.equalsIgnoreCase("null")
					|| url.equalsIgnoreCase(null) || url.isEmpty()
					|| url == null) {
				notificationIntent = new Intent(this, MainActivity.class);
			} else {
				icon = Utility.getBitmap(url);
				notificationIntent = new Intent(this, MainActivity.class);
				notificationIntent.putExtra(Constants.RECHARGE_FOR,
						Constants.SUBSCRIBE_DEAL);
			}
			notificationIntent.putExtra("offer_id", offer_id);
			notificationIntent.putExtra("desc", msg);
			notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
					| Intent.FLAG_ACTIVITY_NEW_TASK);
			Notification notif = new Notification.Builder(mContext)
					.setContentTitle(title)
					.setContentText(msg)
					.setAutoCancel(true)
					.setSmallIcon(R.drawable.status_notification)
					 .setLargeIcon(icon)
					 .addAction(
					 R.drawable.ic_launcher,
					 "View Deals",
					 PendingIntent.getActivity(getApplicationContext(),
			 0, notificationIntent, 0, null))
					.setStyle(
							new Notification.BigPictureStyle()
									.bigPicture(Utility.getBitmap(url)))
					.setAutoCancel(true)
					.setContentIntent(
							PendingIntent.getActivity(getApplicationContext(),
									0, notificationIntent, 0, null)).build();

			try {
				Uri notification1 = RingtoneManager
						.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
				Ringtone r = RingtoneManager.getRingtone(
						getApplicationContext(), notification1);
				r.play();

				Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

				// Vibrate for 1000 milliseconds
				v.vibrate(1000);
			} catch (Exception e) {
			}
			NotificationManager nm = (NotificationManager) mContext
					.getSystemService(Context.NOTIFICATION_SERVICE);
			nm.notify(Constants.DEAL_NOTIFICATION, notif);
		} catch (Exception e) {
		}
	}

	private void Notify(Context mContext, String notificationTitle,
			String notificationMessage) {
		try {
			NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
					this).setAutoCancel(true)
					.setContentTitle("Pay1 Notifications")
					.setSmallIcon(R.drawable.status_notification)
					.setContentText("Testing Messages.");

			NotificationCompat.BigPictureStyle bigPicStyle = new NotificationCompat.BigPictureStyle();
			bigPicStyle.bigPicture(Utility
					.getBitmap("http://b2c.pay1.in/images/53bbfe961d542.png"));
			bigPicStyle.setBigContentTitle("Pay1 Notification");
			mBuilder.setStyle(bigPicStyle);

			/*
			 * Notification notification = new Notification(
			 * R.drawable.status_notification, "Pay1 notification",
			 * System.currentTimeMillis()); //
			 * Constants.notificationList.add(notificationMessage);
			 * notification.number = Constants.COUNT_NOTIFY++;
			 */
			// Utility.setNotificationCount(mContext, "NotiCount",
			// Constants.COUNT_NOTIFY);
			Intent notificationIntent = new Intent(this, MainActivity.class);
			notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
					| Intent.FLAG_ACTIVITY_NEW_TASK);
			PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
					notificationIntent, 0);
			mBuilder.setContentIntent(pendingIntent);
			try {
				Uri notification1 = RingtoneManager
						.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
				Ringtone r = RingtoneManager.getRingtone(
						getApplicationContext(), notification1);
				r.play();

				Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

				// Vibrate for 1000 milliseconds
				v.vibrate(1000);
			} catch (Exception e) {
			}
			// notification.number=count++;

			NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
			/*
			 * notification.setLatestEventInfo(mContext, notificationTitle,
			 * notificationMessage, pendingIntent); notification.flags |=
			 * Notification.FLAG_AUTO_CANCEL;
			 */
			notificationManager.notify(100, mBuilder.build());
		} catch (Exception e) {
		}
	}

}
