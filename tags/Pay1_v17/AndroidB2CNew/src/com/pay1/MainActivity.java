package com.pay1;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.SQLException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.Settings.Secure;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.RotateAnimation;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.AsyncFacebookRunner.RequestListener;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.Facebook.DialogListener;
import com.facebook.android.FacebookError;
import com.google.android.gcm.GCMRegistrar;
import com.larvalabs.svgandroid.SVG;
import com.larvalabs.svgandroid.SVGParser;
import com.pay1.adapterhandler.NavDrawerListAdapter;
import com.pay1.constants.Constants;
import com.pay1.constants.SVGConstant;
import com.pay1.customviews.ArcMenu;
import com.pay1.customviews.MyProgressDialog;
import com.pay1.databasehandler.FreeBies;
import com.pay1.databasehandler.FreeBiesDataSource;
import com.pay1.databasehandler.FreeBiesLocationDataSource;
import com.pay1.databasehandler.NumbberWithCircleDataSource;
import com.pay1.databasehandler.NumberWithCircle;
import com.pay1.databasehandler.Operator;
import com.pay1.databasehandler.OperatorDataSource;
import com.pay1.databasehandler.Plan;
import com.pay1.databasehandler.PlanDataSource;
import com.pay1.databasehandler.QuickPay;
import com.pay1.databasehandler.QuickPayDataSource;
import com.pay1.databasehandler.ReClaimGift;
import com.pay1.databasehandler.ReClaimGiftDataSource;
import com.pay1.model.NavDrawerItem;
import com.pay1.requesthandler.RequestClass;
import com.pay1.servicehandler.CircleIntentService;
import com.pay1.servicehandler.FreeBiesUpdateService;
import com.pay1.servicehandler.PlanIntentService;
import com.pay1.utilities.MemoryManager;
import com.pay1.utilities.Utility;

public class MainActivity extends ActionBarActivity implements
		LocationListener, OnClickListener {

	private OperatorDataSource operatorDataSource;
	private QuickPayDataSource quickPayDataSource;
	private NumbberWithCircleDataSource circleDataSource;
	private FreeBiesDataSource freeBiesDataSource;
	private FreeBiesLocationDataSource freeBiesLocationDataSource;
	private ReClaimGiftDataSource reClaimGiftDataSource;

	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;

	// nav drawer title
	private CharSequence mDrawerTitle;

	// used to store app title
	private CharSequence mTitle;

	// slide menu items
	private String[] navMenuTitles;
	// private TypedArray navMenuIcons;

	private ArrayList<NavDrawerItem> navDrawerItems;
	private NavDrawerListAdapter adapter;

	private static String APP_ID = "784598344960547"; // Replace with your App
														// ID

	// Instance of Facebook Class
	private Facebook facebook = new Facebook(APP_ID);
	private AsyncFacebookRunner mAsyncRunner;
	String FILENAME = "AndroidSSO_data";
	private SharedPreferences mPrefs;

	private ImageView imageView_Menu;

	// private boolean doubleBackToExitPressedOnce = false;

	private static Animation createHintSwitchAnimation(final boolean expanded) {
		Animation animation = new RotateAnimation(expanded ? 45 : 0,
				expanded ? 0 : 45, Animation.RELATIVE_TO_SELF, 0.5f,
				Animation.RELATIVE_TO_SELF, 0.5f);
		animation.setStartOffset(0);
		animation.setDuration(100);
		animation.setInterpolator(new DecelerateInterpolator());
		animation.setFillAfter(true);

		return animation;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_activity);

		if (android.os.Build.VERSION.SDK_INT > 9) {
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
					.permitAll().build();
			StrictMode.setThreadPolicy(policy);
		}

		mPrefs = getPreferences(MODE_PRIVATE);
		try {

			mAsyncRunner = new AsyncFacebookRunner(facebook);

			operatorDataSource = new OperatorDataSource(MainActivity.this);
			quickPayDataSource = new QuickPayDataSource(MainActivity.this);
			freeBiesDataSource = new FreeBiesDataSource(MainActivity.this);
			freeBiesLocationDataSource = new FreeBiesLocationDataSource(
					MainActivity.this);
			circleDataSource = new NumbberWithCircleDataSource(
					MainActivity.this);
			reClaimGiftDataSource = new ReClaimGiftDataSource(MainActivity.this);

			Utility.setOneTimeRegistration(MainActivity.this,
					Constants.SHAREDPREFERENCE_REGISTRATION, true);

			Typeface Reguler = Typeface.createFromAsset(getAssets(),
					"EncodeSansNormal-400-Regular.ttf");

			// MemoryManager.clearCache(MainActivity.this);
			//
			// Debug.MemoryInfo memoryInfo = new Debug.MemoryInfo();
			// Debug.getMemoryInfo(memoryInfo);
			//
			// String memMessage = String.format(
			// "Memory: Pss=%.2f MB, Private=%.2f MB, Shared=%.2f MB",
			// memoryInfo.getTotalPss() / 1024.0,
			// memoryInfo.getTotalPrivateDirty() / 1024.0,
			// memoryInfo.getTotalSharedDirty() / 1024.0);
			// System.out.println("Mem " + memMessage);
			//
			// System.out.println("Mem "
			// + MemoryManager.isMemoryLow(MainActivity.this));

			// ArcMenu arcMenu = (ArcMenu) findViewById(R.id.button_New);
			// initArcMenu(arcMenu);

			imageView_Menu = (ImageView) findViewById(R.id.imageView_Menu);
			imageView_Menu.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					imageView_Menu
							.startAnimation(createHintSwitchAnimation(true));
					showMenuDialog();
				}
			});

			try {
				operatorDataSource.open();

				Operator operator = operatorDataSource.getTopOperator();

				if (operator != null) {
					int days = (int) ((System.currentTimeMillis() - operator
							.getOperatorUptateTime()) / (1000 * 60 * 60 * 24));
					if (days >= 2) {
						new GetOperatorTask().execute();
					}
				} else {
					new GetOperatorTask().execute();
				}

			} catch (SQLException exception) {
				// TODO: handle exception
				// // // Log.e("Er ", exception.getMessage());
			} catch (Exception e) {
				// TODO: handle exception
				// // // Log.e("Er ", e.getMessage());
			} finally {
				operatorDataSource.close();
			}

			try {
				circleDataSource.open();

				NumberWithCircle operator_code = circleDataSource
						.getTopNumberDetails();

				if (operator_code != null) {
					int days = (int) ((System.currentTimeMillis() - operator_code
							.getLastUpdateTime()) / (1000 * 60 * 60 * 24));
					if (days >= 10) {
						Intent circleServiceIntent = new Intent(
								MainActivity.this, CircleIntentService.class);
						startService(circleServiceIntent);
						// new GetOperatorCodeTask().execute();
					}
				} else {
					// Intent circleServiceIntent = new
					// Intent(MainActivity.this,
					// CircleIntentService.class);
					// startService(circleServiceIntent);
					new GetOperatorCodeTask().execute();
				}

			} catch (SQLException exception) {
				// TODO: handle exception
				// // // Log.e("Er ", exception.getMessage());
			} catch (Exception e) {
				// TODO: handle exception
				// // // Log.e("Er ", e.getMessage());
			} finally {
				circleDataSource.close();
			}

			try {
				freeBiesDataSource.open();

				FreeBies freebies = freeBiesDataSource.getTopFreeBie();

				if (freebies != null) {
					int days = (int) ((System.currentTimeMillis() - freebies
							.getFreebieUptateTime()) / (1000 * 60 * 60 * 24));
					if (days >= 10) {
						startService(new Intent(MainActivity.this,
								FreeBiesUpdateService.class));
					}
				} else {
					// startService(new Intent(MainActivity.this,
					// FreeBiesService.class));
					new FreeBiesTask().execute();
				}

			} catch (SQLException exception) {
				// TODO: handle exception
				// // // Log.e("Er ", exception.getMessage());
			} catch (Exception e) {
				// TODO: handle exception
				// // // Log.e("Er ", e.getMessage());
			} finally {
				freeBiesDataSource.close();
			}

			mTitle = mDrawerTitle = getTitle();

			// load slide menu items
			navMenuTitles = getResources().getStringArray(
					R.array.nav_drawer_items);

			// nav drawer icons from resources
			// navMenuIcons = getResources().obtainTypedArray(
			// R.array.nav_drawer_icons);

			mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
			mDrawerList = (ListView) findViewById(R.id.list_slidermenu);

			navDrawerItems = new ArrayList<NavDrawerItem>();

			// adding nav drawer items to array
			// Home
			for (int i = 0; i < navMenuTitles.length; i++) {
				// if (i == 6) {
				// navDrawerItems.add(new NavDrawerItem(navMenuTitles[i], 0,
				// true, ""));
				// } else {
				navDrawerItems.add(new NavDrawerItem(navMenuTitles[i], 0));
				// }
			}
			// Find People
			// navDrawerItems.add(new NavDrawerItem(navMenuTitles[1],
			// navMenuIcons
			// .getResourceId(1, -1)));
			// // Photos
			// navDrawerItems.add(new NavDrawerItem(navMenuTitles[2],
			// navMenuIcons
			// .getResourceId(2, -1)));
			// // Communities, Will add a counter here
			// navDrawerItems.add(new NavDrawerItem(navMenuTitles[3],
			// navMenuIcons
			// .getResourceId(3, -1), true, "22"));
			// // Pages
			// navDrawerItems.add(new NavDrawerItem(navMenuTitles[4],
			// navMenuIcons
			// .getResourceId(4, -1)));
			// // What's hot, We will add a counter here
			// navDrawerItems.add(new NavDrawerItem(navMenuTitles[5],
			// navMenuIcons
			// .getResourceId(5, -1), true, "50+"));

			// Recycle the typed array
			// navMenuIcons.recycle();

			mDrawerList.setOnItemClickListener(new SlideMenuClickListener());

			// setting the nav drawer list adapter
			adapter = new NavDrawerListAdapter(getApplicationContext(),
					navDrawerItems, this);
			mDrawerList.setAdapter(adapter);

			// enabling action bar app icon and behaving it as toggle button
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);
			getSupportActionBar().setIcon(R.drawable.ic_profile);
			getSupportActionBar().setHomeButtonEnabled(true);

			mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
					R.drawable.ic_drawer, R.string.empty_string,
					R.string.empty_string) {
				public void onDrawerClosed(View view) {

					imageView_Menu.setVisibility(View.VISIBLE);
					// getSupportActionBar().setTitle(mTitle);
					// calling onPrepareOptionsMenu() to show action bar icons
					supportInvalidateOptionsMenu();
				}

				public void onDrawerOpened(View drawerView) {

					imageView_Menu.setVisibility(View.GONE);
					adapter.notifyDataSetChanged();
					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(drawerView.getWindowToken(), 0);
					getSupportActionBar().setTitle(mDrawerTitle);
					// calling onPrepareOptionsMenu() to hide action bar icons
					supportInvalidateOptionsMenu();

				}
			};
			mDrawerLayout.setDrawerListener(mDrawerToggle);

			if (savedInstanceState == null) {
				// on first time display view for first nav item

				try {
					quickPayDataSource.open();

					QuickPay list = quickPayDataSource.getTopQuickPay();

					// boolean flag_New = getIntent().getBooleanExtra("IS_FLAG",
					// false);
					// boolean flag_MyGift = getIntent().getBooleanExtra(
					// "IS_MYGIFT", false);
					boolean flag_Quickpay = getIntent().getBooleanExtra(
							"FROM_QUICKPAY", false);
					// if (flag_MyGift)
					// displayView(4);
					// else
					if (flag_Quickpay) {
						boolean flag_gift = getIntent().getBooleanExtra(
								"IS_GIFT", false);
						if (flag_gift) {
							displayView(3);
						} else {
							displayView(2);
						}
					} else if (list == null
							|| !Utility.getLoginFlag(MainActivity.this,
									Constants.SHAREDPREFERENCE_IS_LOGIN))
						displayView(2);
					else {
						displayView(1);
						new CheckUpdateTask().execute();
						new CheckBalanceTask().execute();
						try {
							reClaimGiftDataSource.open();

							List<ReClaimGift> reClaimGifts = reClaimGiftDataSource
									.getAllReClaimGift(1);
							int size = reClaimGifts.size();
							if (size != 0) {
								for (ReClaimGift reClaimGift : reClaimGifts) {
									if (reClaimGift.getReClaimCounter() != 0) {
										Constants
												.Notify(MainActivity.this,
														"Pay1",
														"Grab your free gift.",
														reClaimGift
																.getReClaimID(),
														reClaimGift
																.getReClaimTransactionID(),
														reClaimGift
																.getReClaimRechargeAmount());
									}
								}
							}
						} catch (SQLException exception) {
							// TODO: handle exception
							Log.e("Er ", exception.getMessage());
						} catch (Exception e) {
							// TODO: handle exception
							// // // Log.e("Er ", e.getMessage());
						} finally {
							reClaimGiftDataSource.close();
						}
					}

				} catch (Exception e) {
					// TODO: handle exception
					// // // Log.e("Er ", e.getMessage());
					displayView(1);
					new CheckUpdateTask().execute();
					new CheckBalanceTask().execute();
					try {
						reClaimGiftDataSource.open();

						List<ReClaimGift> reClaimGifts = reClaimGiftDataSource
								.getAllReClaimGift(1);
						int size = reClaimGifts.size();
						if (size != 0) {
							for (ReClaimGift reClaimGift : reClaimGifts) {
								if (reClaimGift.getReClaimCounter() != 0) {
									Constants
											.Notify(MainActivity.this,
													"Pay1",
													"Grab your free gift.",
													reClaimGift.getReClaimID(),
													reClaimGift
															.getReClaimTransactionID(),
													reClaimGift
															.getReClaimRechargeAmount());
								}
							}
						}
					} catch (Exception ex) {
						// TODO: handle exception
						// // // Log.e("Er ", e.getMessage());
					} finally {
						reClaimGiftDataSource.close();
					}
				} finally {
					quickPayDataSource.close();
				}
			}

			GCMRegistrar.checkDevice(this);
			GCMRegistrar.checkManifest(this);
			checkNotNull(Constants.SENDER_ID, "SENDER_ID");

			try {
				String uuid;
				TelephonyManager tManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
				uuid = tManager.getDeviceId();
				if (uuid == null) {
					uuid = Secure.getString(
							MainActivity.this.getContentResolver(),
							Secure.ANDROID_ID);
				}
				if (Utility.getUUID(MainActivity.this,
						Constants.SHAREDPREFERENCE_UUID) == null)
					Utility.setUUID(MainActivity.this,
							Constants.SHAREDPREFERENCE_UUID, uuid);
			} catch (Exception exception) {
			}

			final String regId = GCMRegistrar.getRegistrationId(this);
			// // // Log.i(TAG, "registration id =====  " + regId);
			Utility.setGCMID(MainActivity.this,
					Constants.SHAREDPREFERENCE_GCMID, regId);
			if (regId.equals("")) {
				GCMRegistrar.register(this, Constants.SENDER_ID);
			} else {
				// // // Log.v(TAG, "Already registered");
			}

			// try {
			// LocationResult locationResult = new LocationResult() {
			// @Override
			// public void gotLocation(Location location) {
			// // Got the location!
			// try {
			// if (location != null) {
			// Utility.setCurrentLatitude(
			// MainActivity.this,
			// Constants.SHAREDPREFERENCE_CURRENT_LATITUDE,
			// String.valueOf(location.getLatitude()));
			// Utility.setCurrentLongitude(
			// MainActivity.this,
			// Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE,
			// String.valueOf(location.getLongitude()));
			// }
			// } catch (Exception exception) {
			// }
			// }
			// };
			// MyLocation myLocation = new MyLocation();
			// myLocation.getLocation(this, locationResult);
			// } catch (Exception exception) {
			// }

			if (getIntent().getExtras().containsKey("bal")) {
				if (getIntent().getExtras().getString("bal")
						.equalsIgnoreCase("1")) {
					Constants.showFullOneButtonDialog(MainActivity.this,
							"Notification",
							getIntent().getExtras().getString("desc").trim(),
							Constants.DIALOG_CLOSE);
					new CheckBalanceTask().execute();
				}
			}
			if (getIntent().getExtras().containsKey("offer_id")) {
				displayView(3);
			}
			if (!MemoryManager.isMemoryLow(MainActivity.this)) {
				PlanDataSource planDataSource = new PlanDataSource(
						MainActivity.this);
				try {

					planDataSource.open();

					Plan plans = planDataSource.getTopPlan();
					if (plans != null) {
						int days = (int) ((System.currentTimeMillis() - Long
								.parseLong(plans.getPlanUptateTime())) / (1000 * 60 * 60 * 24));
						if (days >= 2) {
							startService(new Intent(MainActivity.this,
									PlanIntentService.class));
						}
					} else {
						startService(new Intent(MainActivity.this,
								PlanIntentService.class));
					}
				} catch (Exception e) {
					startService(new Intent(MainActivity.this,
							PlanIntentService.class));
				} finally {
					planDataSource.close();
				}
			}
			try {
				LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

				// Creating a criteria object to retrieve provider
				Criteria criteria = new Criteria();

				// Getting the name of the best provider
				String provider = locationManager.getBestProvider(criteria,
						true);

				// Getting Current Location
				Location location = locationManager
						.getLastKnownLocation(provider);

				if (location != null) {
					onLocationChanged(location);
				}
				locationManager
						.requestLocationUpdates(provider, 20000, 0, this);
			} catch (Exception e) {
			}
		} catch (Exception e) {
		}

		/*
		 * LoginButton authButton = (LoginButton) findViewById(R.id.buttonFB);
		 * authButton.setSessionStatusCallback(new Session.StatusCallback() {
		 * public void call(Session session, SessionState state,Exception
		 * exception) {
		 * 
		 * 
		 * // TODO Auto-generated method stub if (session.isOpened()) {
		 * Log.i("MAIN ACTIVITY", "Access Token" + session.getAccessToken());
		 * Request.executeMeRequestAsync(session,new Request.GraphUserCallback()
		 * {
		 * 
		 * public void onCompleted(GraphUser user,Response response) { // TODO
		 * Auto-generated method stub if (user != null) { Log.i("MAIN ACTIVITY",
		 * "User ID " + user.getId()); Log.i("MAIN ACTIVITY","Email "+
		 * user.asMap().get("email")); } }}); } else { //This else condition
		 * will be executed when logout will be clicked. } } });
		 */
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		// adapter.notifyDataSetChanged();
	}

	/**
	 * Slide menu item click listener
	 * */
	private class SlideMenuClickListener implements
			ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			// display view for selected nav drawer item
			mDrawerList.smoothScrollToPosition(0);
			if (position == 0) {
				ImageView tv = (ImageView) view
						.findViewById(R.id.imageSettings);// mDrawerList.getChildAt(position);
				tv.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						// Toast.makeText(MainActivity.this, "Clicked",
						// Toast.LENGTH_SHORT).show();
						displayView(0);
					}
				});

				final ImageView fbConnectButton = (ImageView) view
						.findViewById(R.id.fbConnectButton);
				fbConnectButton.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						/*
						 * Intent intent=new
						 * Intent(MainActivity.this,AndroidFacebookConnectActivity
						 * .class); startActivity(intent);
						 */

						String access_token = mPrefs.getString("access_token",
								null);
						if (access_token != null) {

						} else {
							// loginToFacebook(fbConnectButton);
							// ShowToastWithImage("Coming Soon!!!");
						}

						/*
						 * Session.openActiveSession(MainActivity.this, true,
						 * new Session.StatusCallback() {
						 * 
						 * // callback when session changes state public void
						 * call(Session session, SessionState state, Exception
						 * exception) { if (session.isOpened()) { // make
						 * request to;2 the /me API
						 * Request.executeMeRequestAsync(session, new Request.
						 * GraphUserCallback() {
						 * 
						 * // callback after Graph API response with user object
						 * 
						 * @Override public void onCompleted(GraphUser user,
						 * Response response) { if (user != null) { TextView
						 * welcome = (TextView) findViewById(R.id.welcome);
						 * welcome.setText("Hello " + user.getName() + "!");
						 * String name= user.getName(); Log.d("user",
						 * "UserName  "+name); } }
						 * 
						 * 
						 * 
						 * 
						 * }); } } });
						 */
					}
				});

			} else {
				displayView(position);
			}
		}
	}

	@SuppressLint("NewApi")
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.clear();
		getMenuInflater().inflate(R.menu.main, menu);

		// if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
		//
		// SearchManager searchManager = (SearchManager)
		// getSystemService(Context.SEARCH_SERVICE);
		// SearchView searchView = (SearchView) menu
		// .findItem(R.id.menu_search).getActionView();
		// searchView.setSearchableInfo(searchManager
		// .getSearchableInfo(getComponentName()));
		//
		// // searchView.setOnQueryTextListener(new OnQueryTextListener() {
		// //
		// // @Override
		// // public boolean onQueryTextChange(String query) {
		// //
		// // return true;
		// //
		// // }
		// //
		// // @Override
		// // public boolean onQueryTextSubmit(String query) {
		// // // TODO Auto-generated method stub
		// // Intent intent = new Intent(MainActivity.this,
		// // GiftSearchActivity.class);
		// // intent.putExtra("QUERY", query);
		// // startActivity(intent);
		// // return false;
		// // }
		// //
		// // });
		//
		// }
		try {
			MenuItem item = menu.findItem(R.id.menu_wallet);
			MenuItemCompat.setActionView(item, R.layout.bal_wall);
			View view = MenuItemCompat.getActionView(item);

			TextView tv = (TextView) view.findViewById(R.id.textView_Count);

			DecimalFormat decimalFormat = new DecimalFormat("0.00");
			String bal = Utility.getBalance(MainActivity.this,
					Constants.SHAREDPREFERENCE_BALANCE);
			bal = bal == null ? "0" : bal;

			tv.setText("Rs." + decimalFormat.format(Double.parseDouble(bal)));
			tv.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					showWalletRefreshDialog(1);
				}
			});
			view.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					showWalletRefreshDialog(1);
				}
			});
			ImageView imageView_Icon = (ImageView) view
					.findViewById(R.id.imageView_Icon);
			imageView_Icon.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					showWalletRefreshDialog(1);
				}
			});
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		try {
			MenuItem item = menu.findItem(R.id.menu_points);
			MenuItemCompat.setActionView(item, R.layout.bal_coin);
			View view = MenuItemCompat.getActionView(item);

			TextView tv = (TextView) view.findViewById(R.id.textView_Count);

			DecimalFormat decimalFormat = new DecimalFormat("0");
			String bal = Utility.getBalance(MainActivity.this,
					Constants.SHAREDPREFERENCE_LOYALTY_POINTS);
			bal = bal == null ? "0" : bal;

			tv.setText("" + decimalFormat.format(Double.parseDouble(bal)));
			tv.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					showWalletRefreshDialog(2);
				}
			});
			view.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					showWalletRefreshDialog(2);
				}
			});
			ImageView imageView_Icon = (ImageView) view
					.findViewById(R.id.imageView_Icon);
			imageView_Icon.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					showWalletRefreshDialog(2);
				}
			});
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return super.onCreateOptionsMenu(menu);
	}

	public void loginToFacebook(final ImageView fbConnectButton) {

		/*
		 * Session currentSession = Session.getActiveSession(); if
		 * (currentSession == null || currentSession.getState().isClosed()) {
		 * Session session = new Session.Builder(MainActivity.this).build();
		 * Session.setActiveSession(session); currentSession = session; }
		 * 
		 * if (currentSession.isOpened()) { // Do whatever u want. User has
		 * logged in
		 * 
		 * } else if (!currentSession.isOpened()) { // Ask for username and
		 * password OpenRequest op = new Session.OpenRequest(MainActivity.this);
		 * 
		 * op.setLoginBehavior(SessionLoginBehavior.SUPPRESS_SSO);
		 * op.setCallback(null);
		 * 
		 * List<String> permissions = new ArrayList<String>();
		 * permissions.add("publish_stream"); permissions.add("user_likes");
		 * permissions.add("email"); permissions.add("user_birthday");
		 * op.setPermissions(permissions);
		 * 
		 * Session session = new Builder(MainActivity.this).build();
		 * Session.setActiveSession(session); session.openForPublish(op); }
		 */

		mPrefs = getPreferences(MODE_PRIVATE);
		String access_token = mPrefs.getString("access_token", null);
		long expires = mPrefs.getLong("access_expires", 0);

		if (access_token != null) {
			facebook.setAccessToken(access_token);

			Log.d("FB Sessions", "" + facebook.isSessionValid());
		}

		if (expires != 0) {
			facebook.setAccessExpires(expires);
		}

		if (!facebook.isSessionValid()) {
			facebook.authorize(this,
					new String[] { "email", "publish_stream" },
					new DialogListener() {

						@Override
						public void onCancel() {
							// Function to handle cancel event
						}

						@Override
						public void onComplete(Bundle values) {
							// Function to handle complete event
							// Edit Preferences and update facebook acess_token
							SharedPreferences.Editor editor = mPrefs.edit();
							editor.putString("access_token",
									facebook.getAccessToken());
							editor.putLong("access_expires",
									facebook.getAccessExpires());
							editor.commit();

							getProfileInformation(fbConnectButton);
							// fbUserProfile();
						}

						@Override
						public void onError(DialogError error) {
							// Function to handle error
							Log.d("test", "Test");
						}

						@Override
						public void onFacebookError(FacebookError fberror) {
							// Function to handle Facebook errors
							Log.d("test", "Test");
						}

					});
		}
	}

	public void getProfileInformation(final ImageView fbConnectButton) {

		mAsyncRunner.request("me", new RequestListener() {
			@Override
			public void onComplete(String response, Object state) {
				Log.d("Profile", response);
				String json = response;
				// final JSONObject profile=null;
				try {
					// Facebook Profile JSON data
					JSONObject profile = new JSONObject(json);
					Log.d("DARA", json);
					// getting name of the user
					final String name = profile.getString("name");

					// getting email of the user
					final String email = profile.getString("email");
					final String fbId = profile.getString("id");

					Utility.setUserName(MainActivity.this,
							Constants.SHAREDPREFERENCE_NAME,
							profile.getString("name").trim());
					/*
					 * Utility.setMobileNumber(MainActivity.this,
					 * Constants.SHAREDPREFERENCE_MOBILE, profile
					 * .getString("mobile").trim());
					 */
					Utility.setEmail(MainActivity.this,
							Constants.SHAREDPREFERENCE_EMAIL, profile
									.getString("email").trim());
					Utility.setGender(MainActivity.this,
							Constants.SHAREDPREFERENCE_GENDER, profile
									.getString("gender").trim());

					Utility.setFbImage(MainActivity.this, fbId);
					new UpdateProfileTask().execute(profile.getString("name"),
							profile.getString("email").trim(), profile
									.getString("gender").trim());
					runOnUiThread(new Runnable() {

						@Override
						public void run() {
							// Toast.makeText(getApplicationContext(), "Name: "
							// + name + "\nEmail: " + email,
							// Toast.LENGTH_LONG).show();
							// fbConnectButton.setImageBitmap(Utility.getBitmap("http://graph.facebook.com/"+fbId+"/picture?type=large"));
							adapter.notifyDataSetChanged();
							// new
							// UpdateProfileTask().execute(profile.getString("name"),
							// profile.getString("email").trim(), profile
							// .getString("gender").trim());
							new LoadImageTask().execute(fbId);
						}

					});

				} catch (JSONException e) {
					e.printStackTrace();
				}

			}

			@Override
			public void onIOException(IOException e, Object state) {
				Log.d("test", "Test");
			}

			@Override
			public void onFileNotFoundException(FileNotFoundException e,
					Object state) {
				Log.d("test", "Test");
			}

			@Override
			public void onMalformedURLException(MalformedURLException e,
					Object state) {
				Log.d("test", "Test");
			}

			@Override
			public void onFacebookError(FacebookError e, Object state) {
				Log.d("test", "Test");
			}
		});
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// toggle nav drawer on selecting action bar app icon/title
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		// Handle action bar actions click
		switch (item.getItemId()) {
		case R.id.menu_wallet: {

			showWalletRefreshDialog(1);
			// if (!isOpened) {
			// final Animation animation = AnimationUtils.loadAnimation(
			// MainActivity.this, R.anim.translate_down);
			//
			// animation.setAnimationListener(new AnimationListener() {
			//
			// public void onAnimationStart(Animation animation) {
			// }
			//
			// public void onAnimationRepeat(Animation animation) {
			// }
			//
			// public void onAnimationEnd(Animation animation) {
			//
			// LayoutParams params = (LayoutParams) mDrawerLayout
			// .getLayoutParams();
			// params.topMargin = 200;
			// mDrawerLayout.setLayoutParams(params);
			// mDrawerLayout.clearAnimation();
			// isOpened = true;
			// }
			// });
			// mDrawerLayout.startAnimation(animation);
			// } else {
			//
			// LayoutParams params = new LayoutParams(
			// LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
			// params.topMargin = 0;
			// mDrawerLayout.setLayoutParams(params);
			//
			// final Animation animation = AnimationUtils.loadAnimation(
			// MainActivity.this, R.anim.translate_up);
			//
			// animation.setAnimationListener(new AnimationListener() {
			//
			// public void onAnimationStart(Animation animation) {
			// }
			//
			// public void onAnimationRepeat(Animation animation) {
			// }
			//
			// public void onAnimationEnd(Animation animation) {
			//
			// mDrawerLayout.clearAnimation();
			// isOpened = false;
			// }
			// });
			// mDrawerLayout.startAnimation(animation);
			// }
			return true;

		}
		case R.id.menu_points: {
			showWalletRefreshDialog(2);
			return true;
		}
		case R.id.menu_search: {
			showSearchDialog();
			return true;
		}
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private void showWalletRefreshDialog(final int type) {
		final Dialog dialog = new Dialog(this, android.R.style.Theme_Dialog);

		// Setting dialogview
		Window window = dialog.getWindow();
		window.requestFeature(Window.FEATURE_NO_TITLE);
		// window.setGravity(Gravity.TOP);

		// window.setLayout(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);

		dialog.getWindow().setBackgroundDrawableResource(
				android.R.color.transparent);
		WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
		params.y = 50;
		params.gravity = Gravity.TOP;
		dialog.getWindow().setAttributes(params);
		dialog.setTitle(null);
		dialog.setContentView(R.layout.check_activity);
		dialog.setCancelable(true);

		TextView textView_TopTitle, textView_TopBalance;
		Button button_TopRefresh, button_AddMoney;

		Typeface Reguler = Typeface.createFromAsset(getAssets(),
				"EncodeSansNormal-400-Regular.ttf");

		textView_TopTitle = (TextView) dialog
				.findViewById(R.id.textView_TopTitle);
		textView_TopTitle.setTypeface(Reguler);
		textView_TopBalance = (TextView) dialog
				.findViewById(R.id.textView_TopBalance);
		textView_TopBalance.setTypeface(Reguler);
		button_TopRefresh = (Button) dialog
				.findViewById(R.id.button_TopRefresh);
		button_TopRefresh.setTypeface(Reguler);
		button_AddMoney = (Button) dialog.findViewById(R.id.button_AddMoney);
		button_AddMoney.setTypeface(Reguler);

		if (type == 1) {
			DecimalFormat decimalFormat = new DecimalFormat("0.00");
			String bal = Utility.getBalance(MainActivity.this,
					Constants.SHAREDPREFERENCE_BALANCE);
			bal = bal == null ? "0" : bal;

			textView_TopTitle.setText("Pay1 Money");
			textView_TopBalance.setText("Balance Rs "
					+ decimalFormat.format(Double.parseDouble(bal)));
		} else {
			button_AddMoney.setVisibility(View.GONE);
			DecimalFormat decimalFormat = new DecimalFormat("0");
			String bal = Utility.getBalance(MainActivity.this,
					Constants.SHAREDPREFERENCE_LOYALTY_POINTS);
			bal = bal == null ? "0" : bal;
			textView_TopTitle.setText("Gift Coins");
			textView_TopBalance.setText("Balance "
					+ decimalFormat.format(Double.parseDouble(bal)));
		}
		button_TopRefresh.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				new CheckBalanceLoaderTask().execute(String.valueOf(type));
			}
		});
		button_AddMoney.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				displayView(11);
			}
		});

		dialog.show();
	}

	private void showSearchDialog() {
		final Dialog dialog = new Dialog(this, android.R.style.Theme_Dialog);

		// Setting dialogview
		Window window = dialog.getWindow();
		window.requestFeature(Window.FEATURE_NO_TITLE);
		window.setGravity(Gravity.TOP);
		dialog.getWindow().setBackgroundDrawableResource(
				android.R.color.transparent);
		// window.setLayout(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
		// WindowManager.LayoutParams params =
		// dialog.getWindow().getAttributes();
		// params.y = 50;
		// params.gravity = Gravity.TOP;
		// dialog.getWindow().setAttributes(params);
		dialog.setTitle(null);
		dialog.setContentView(R.layout.search_activity);
		dialog.setCancelable(true);

		final EditText editText_Search;

		Typeface Reguler = Typeface.createFromAsset(getAssets(),
				"EncodeSansNormal-400-Regular.ttf");

		editText_Search = (EditText) dialog.findViewById(R.id.editText_Search);
		editText_Search.setTypeface(Reguler);
		// InputMethodManager imm = (InputMethodManager)
		// getSystemService(Context.INPUT_METHOD_SERVICE);
		// imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
		editText_Search.postDelayed(new Runnable() {
			@Override
			public void run() {
				InputMethodManager keyboard = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				keyboard.showSoftInput(editText_Search, 0);
			}
		}, 200);
		editText_Search
				.setOnEditorActionListener(new TextView.OnEditorActionListener() {

					@Override
					public boolean onEditorAction(TextView v, int actionId,
							KeyEvent event) {
						// TODO Auto-generated method stub
						if (actionId == EditorInfo.IME_ACTION_SEARCH) {
							InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
							imm.hideSoftInputFromWindow(
									editText_Search.getWindowToken(),
									InputMethodManager.RESULT_UNCHANGED_SHOWN);
							Intent intent = new Intent(MainActivity.this,
									GiftSearchActivity.class);
							intent.putExtra("QUERY", editText_Search.getText()
									.toString().trim());
							startActivity(intent);
							return true;
						}
						return false;
					}
				});

		dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {

			@Override
			public void onDismiss(DialogInterface dialog) {
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
			}
		});
		dialog.show();
	}

	@SuppressLint("NewApi")
	private void showMenuDialog() {
		final Dialog dialog = new Dialog(this, android.R.style.Theme_Dialog);

		// Setting dialogview
		Window window = dialog.getWindow();
		window.requestFeature(Window.FEATURE_NO_TITLE);
		window.setGravity(Gravity.BOTTOM | Gravity.RIGHT);
		dialog.getWindow().setBackgroundDrawableResource(
				android.R.color.transparent);
		// window.setLayout(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
		// WindowManager.LayoutParams params =
		// dialog.getWindow().getAttributes();
		// params.y = 50;
		// params.gravity = Gravity.TOP;
		// dialog.getWindow().setAttributes(params);
		dialog.setTitle(null);
		dialog.setContentView(R.layout.menu_activity);
		dialog.setCancelable(true);

		final TextView textView_Home, textView_Recharge, textView_Gift, textView_Money;
		final ImageView imageView_Home, imageView_Recharge, imageView_Gift, imageView_Money;

		Typeface Reguler = Typeface.createFromAsset(getAssets(),
				"EncodeSansNormal-400-Regular.ttf");

		textView_Home = (TextView) dialog.findViewById(R.id.textView_Home);
		textView_Home.setTypeface(Reguler, Typeface.BOLD);
		textView_Recharge = (TextView) dialog
				.findViewById(R.id.textView_Recharge);
		textView_Recharge.setTypeface(Reguler, Typeface.BOLD);
		textView_Gift = (TextView) dialog.findViewById(R.id.textView_Gift);
		textView_Gift.setTypeface(Reguler, Typeface.BOLD);
		textView_Money = (TextView) dialog.findViewById(R.id.textView_Money);
		textView_Money.setTypeface(Reguler, Typeface.BOLD);

		imageView_Home = (ImageView) dialog.findViewById(R.id.imageView_Home);
		imageView_Recharge = (ImageView) dialog
				.findViewById(R.id.imageView_Recharge);
		imageView_Gift = (ImageView) dialog.findViewById(R.id.imageView_Gift);
		imageView_Money = (ImageView) dialog.findViewById(R.id.imageView_Money);

		imageView_Home.setImageDrawable(SVGParser.getSVGFromResource(
				getResources(), SVGConstant.getSVGResource(8))
				.createPictureDrawable());
		imageView_Home.setPadding(5, 5, 5, 5);

		imageView_Recharge.setImageDrawable(SVGParser.getSVGFromResource(
				getResources(), SVGConstant.getSVGResource(9))
				.createPictureDrawable());
		imageView_Recharge.setPadding(5, 5, 5, 5);

		imageView_Gift.setImageDrawable(SVGParser.getSVGFromResource(
				getResources(), SVGConstant.getSVGResource(10))
				.createPictureDrawable());
		imageView_Gift.setPadding(5, 5, 5, 5);

		imageView_Money.setImageDrawable(SVGParser.getSVGFromResource(
				getResources(), SVGConstant.getSVGResource(11))
				.createPictureDrawable());
		imageView_Money.setPadding(5, 5, 5, 5);

		if (Build.VERSION.SDK_INT >= 11) {
			imageView_Home.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
			imageView_Recharge.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
			imageView_Gift.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
			imageView_Money.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
		}

		RelativeLayout relativeLayout_Home = (RelativeLayout) dialog
				.findViewById(R.id.relativeLayout_Home);
		relativeLayout_Home.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				displayView(8);
				supportInvalidateOptionsMenu();
			}
		});

		RelativeLayout relativeLayout_Recharge = (RelativeLayout) dialog
				.findViewById(R.id.relativeLayout_Recharge);
		relativeLayout_Recharge.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				displayView(9);
				supportInvalidateOptionsMenu();
			}
		});

		RelativeLayout relativeLayout_Gift = (RelativeLayout) dialog
				.findViewById(R.id.relativeLayout_Gift);
		relativeLayout_Gift.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				displayView(10);
				supportInvalidateOptionsMenu();
			}
		});

		RelativeLayout relativeLayout_Money = (RelativeLayout) dialog
				.findViewById(R.id.relativeLayout_Money);
		relativeLayout_Money.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				displayView(11);
				supportInvalidateOptionsMenu();
			}
		});
		dialog.show();
	}

	/* *
	 * Called when invalidateOptionsMenu() is triggered
	 */
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// if nav drawer is opened, hide the action items
		boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
		MenuItem wallet = menu.findItem(R.id.menu_wallet);
		MenuItem points = menu.findItem(R.id.menu_points);
		MenuItem search = menu.findItem(R.id.menu_search);

		if (mDrawerList.getCheckedItemPosition() == 3
				|| mDrawerList.getCheckedItemPosition() == 10) {
			wallet.setVisible(true);
			points.setVisible(true);
			search.setVisible(true);
		} else if (mDrawerList.getCheckedItemPosition() > 3
				&& mDrawerList.getCheckedItemPosition() != 8
				&& mDrawerList.getCheckedItemPosition() != 9
				&& mDrawerList.getCheckedItemPosition() != 11) {
			wallet.setVisible(false);
			points.setVisible(false);
			search.setVisible(false);
		} else {
			wallet.setVisible(true);
			points.setVisible(true);
			search.setVisible(false);
		}
		if (drawerOpen) {
			wallet.setVisible(false);
			points.setVisible(false);
			search.setVisible(false);
		}
		return super.onPrepareOptionsMenu(menu);
	}

	/**
	 * Diplaying fragment view for selected nav drawer list item
	 * */
	private void displayView(int position) {
		// update the main content by replacing fragments
		Fragment fragment = null;

		switch (position) {
		case 0:
			Intent profileIntent = new Intent(MainActivity.this,
					ProfileActivity.class);
			startActivity(profileIntent);
			break;
		case 1:
			if (Utility.getLoginFlag(MainActivity.this,
					Constants.SHAREDPREFERENCE_IS_LOGIN)) {
							//fragment = QuickPayFragment.newInstance();
				fragment=QuickPayFragmentNew.newInstance();
} else {
				Intent intent = new Intent(MainActivity.this,
						LoginActivity.class);
				startActivity(intent);
			}
			break;
		case 2:
			try {
				quickPayDataSource.open();

				List<QuickPay> quickpays = quickPayDataSource
						.getQuickPayByLimit(2);
				int size = quickpays.size();
				if (size >= 2) {
					fragment = RechargeMainActivity.newInstance(0);
				} else {
					fragment = RechargeMainActivity.newInstance(1);
				}
			} catch (SQLException e) {
				// TODO: handle exception
			} catch (Exception e) {
				// TODO: handle exception
			} finally {
				quickPayDataSource.close();
			}
			break;
		case 3:
			Utility.setBackRequired(MainActivity.this,
					Constants.SHAREDPREFERENCE_BACK_LOCATION, true);
			fragment = GiftMainActivity.newInstance(0);
			break;
		/*
		 * case 4: Utility.setBackRequired(MainActivity.this,
		 * Constants.SHAREDPREFERENCE_BACK_LOCATION, false); fragment =
		 * MyFreeBiesFragment.newInstance(); break;
		 */
		case 4:
			fragment = HistoryFragment.newInstance();
			break;
		case 5:
			fragment = QuickPayMiscallFregment.newInstance();
			break;
		case 6:
			fragment = MyAccountFragment.newInstance();
			break;
		case 7:
			try {
				if (Utility.getLoginFlag(MainActivity.this,
						Constants.SHAREDPREFERENCE_IS_LOGIN)) {

					final Dialog dialog = new Dialog(MainActivity.this);
					dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
					dialog.getWindow().setBackgroundDrawableResource(
							android.R.color.transparent);
					dialog.setContentView(R.layout.dialog_two_button);
					dialog.setCancelable(false);
					dialog.setCanceledOnTouchOutside(false);
					// dialog.setTitle(null);

					Typeface Reguler = Typeface.createFromAsset(getAssets(),
							"EncodeSansNormal-400-Regular.ttf");

					Typeface Medium = Typeface.createFromAsset(getAssets(),
							"EncodeSansNarrow-500-Medium.ttf");

					// set the custom dialog components
					// - text, image and button
					TextView textView_Title = (TextView) dialog
							.findViewById(R.id.textView_Title);
					textView_Title.setText("Logout");
					textView_Title.setTypeface(Reguler);

					TextView textView_Message = (TextView) dialog
							.findViewById(R.id.textView_Message);
					textView_Message
							.setText("Are you sure you want to logout?");
					textView_Message.setTypeface(Medium);

					Button button_Ok = (Button) dialog
							.findViewById(R.id.button_Ok);
					button_Ok.setTypeface(Reguler);
					button_Ok.setText("Yes");
					// if button is clicked, close the
					// custom dialog
					button_Ok.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							dialog.dismiss();
							new LogoutTask().execute();

						}
					});

					Button button_Cancel = (Button) dialog
							.findViewById(R.id.button_Cancel);
					button_Cancel.setText("No");
					button_Cancel.setTypeface(Reguler);
					// if button is clicked, close the
					// custom dialog
					button_Cancel
							.setOnClickListener(new View.OnClickListener() {
								@Override
								public void onClick(View v) {
									dialog.dismiss();
								}
							});

					dialog.show();

				} else {
					Intent intent = new Intent(MainActivity.this,
							LoginActivity.class);
					startActivity(intent);
				}
			} catch (Exception exception) {
			}
			break;
		case 8:
			if (Utility.getLoginFlag(MainActivity.this,
					Constants.SHAREDPREFERENCE_IS_LOGIN)) {
				fragment = QuickPayFragmentNew.newInstance();
			} else {
				Intent intent = new Intent(MainActivity.this,
						LoginActivity.class);
				startActivity(intent);
			}
			break;
		case 9:
			//fragment = RechargeMainActivity.newInstance(0);
			try {
				quickPayDataSource.open();

				List<QuickPay> quickpays = quickPayDataSource
						.getQuickPayByLimit(2);
				int size = quickpays.size();
				if (size >= 2) {
					fragment = RechargeMainActivity.newInstance(0);
				} else {
					fragment = RechargeMainActivity.newInstance(1);
				}
			} catch (SQLException e) {
				// TODO: handle exception
			} catch (Exception e) {
				// TODO: handle exception
			} finally {
				quickPayDataSource.close();
			}
			break;
		case 10:
			Utility.setBackRequired(MainActivity.this,
					Constants.SHAREDPREFERENCE_BACK_LOCATION, true);
			fragment = GiftMainActivity.newInstance(0);
			break;
		case 11:
			fragment = WalletTopupFragement.newInstance();
			break;
		default:
			break;
		}

		if (fragment != null) {
			FragmentManager fragmentManager = getSupportFragmentManager();
			FragmentTransaction fragmentTransaction = fragmentManager
					.beginTransaction();
			fragmentTransaction.replace(R.id.frame_container, fragment);
			fragmentTransaction.addToBackStack(null);
			fragmentTransaction.commit();

			// update selected item and title, then close the drawer

			mDrawerList.setItemChecked(position, true);
			// mDrawerList.setSelection(position);
			mDrawerList.setSelected(true);

			// setTitle(navMenuTitles[position]);
			mDrawerLayout.closeDrawer(mDrawerList);
		} else {
			// error in creating fragment
			// Log.e("MainActivity", "Error in creating fragment");
		}
	}

	@Override
	public void setTitle(CharSequence title) {
		mTitle = title;
		// getSupportActionBar().setTitle(title);
	}

	/**
	 * When using the ActionBarDrawerToggle, you must call it during
	 * onPostCreate() and onConfigurationChanged()...
	 */

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggls
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v7.app.ActionBarActivity#onPostResume()
	 */
	@Override
	protected void onPostResume() {
		// TODO Auto-generated method stub
		super.onPostResume();
		if (mDrawerList.getCheckedItemPosition() == 4) {
			if (Utility.getBackRequired(MainActivity.this,
					Constants.SHAREDPREFERENCE_BACK_LOCATION))
				displayView(4);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v7.app.ActionBarActivity#onBackPressed()
	 */
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (mDrawerLayout.isDrawerOpen(mDrawerList)) {
			mDrawerLayout.closeDrawers();
		} else if (mDrawerList.getCheckedItemPosition() != 1) {
			supportInvalidateOptionsMenu();
			if (mDrawerList.getCheckedItemPosition() == 3) {
				if (FreeBiesPagerFragment.flag == 1) {
					FreeBiesPagerFragment.flag = 0;
					FreeBiesListFragment.adapter.notifyDataSetChanged();
				} else {
					displayView(1);
				}
			} else if (Utility.getLoginFlag(MainActivity.this,
					Constants.SHAREDPREFERENCE_IS_LOGIN)) {
				displayView(1);
			} else if (mDrawerList.getCheckedItemPosition() != 2) {
				displayView(2);
			} else {
				showTwoButtonDialog(MainActivity.this, "Pay1",
						"Are you sure you want to exit?");
			}
		} else {
			// if (doubleBackToExitPressedOnce) {
			// super.onBackPressed();
			// finish();
			// return;
			// }
			// if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
			// getSupportFragmentManager().popBackStack();
			// }
			showTwoButtonDialog(MainActivity.this, "Pay1",
					"Are you sure you want to exit?");
			// this.doubleBackToExitPressedOnce = true;
			// Toast.makeText(this, "Press again to exit.", Toast.LENGTH_SHORT)
			// .show();
			//
			// new Handler().postDelayed(new Runnable() {
			//
			// @Override
			// public void run() {
			// doubleBackToExitPressedOnce = false;
			// }
			// }, 10000);

		}
	}

	@SuppressLint("NewApi")
	private void initArcMenu(ArcMenu menu) {
		for (int i = 8; i < 12; i++) {

			/*
			 * View child = getLayoutInflater().inflate(R.layout.menu_with_font,
			 * null); ImageView
			 * imageView1=(ImageView)child.findViewById(R.id.imageView1);
			 * TextView textView1=(TextView)child.findViewById(R.id.textView1);
			 * 
			 * 
			 * SVG svg = SVGParser.getSVGFromResource(getResources(),
			 * SVGConstant.getSVGResource(i));
			 * imageView1.setImageDrawable(svg.createPictureDrawable()); //
			 * item.setBackgroundResource(R.drawable.circle_profile_background);
			 * //imageView1.setPadding(5, 5, 5, 5);
			 * 
			 * if (Build.VERSION.SDK_INT >= 11) {
			 * imageView1.setLayerType(View.LAYER_TYPE_SOFTWARE, null); }
			 * 
			 * textView1.setText("umesh"+i);
			 * 
			 * final int position = i; menu.addItem(child, new OnClickListener()
			 * {
			 * 
			 * @Override public void onClick(View v) {
			 * 
			 * displayView(position);
			 * 
			 * // if (position == 3) { // Intent intent = new
			 * Intent(MainActivity.this, // GiftMainActivity.class); //
			 * startActivity(intent); // } } });
			 */
			ImageView item = new ImageView(this);
			// item.setImageResource(itemDrawables[i]);
			LinearLayout.LayoutParams parms = new LinearLayout.LayoutParams(80,
					80);
			item.setLayoutParams(parms);
			SVG svg = SVGParser.getSVGFromResource(getResources(),
					SVGConstant.getSVGResource(i));
			item.setImageDrawable(svg.createPictureDrawable());
			// item.setBackgroundResource(R.drawable.circle_profile_background);
			item.setPadding(5, 5, 5, 5);

			if (Build.VERSION.SDK_INT >= 11) {
				item.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
			}

			final int position = i;
			menu.addItem(item, new OnClickListener() {

				@Override
				public void onClick(View v) {

					displayView(position);
					supportInvalidateOptionsMenu();
					// if (position == 3) {
					// Intent intent = new Intent(MainActivity.this,
					// GiftMainActivity.class);
					// startActivity(intent);
					// }
				}
			});
		}
	}

	public void showTwoButtonDialog(final Context context, String Title,
			String message) {
		try {

			final Dialog dialog = new Dialog(context);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.getWindow().setBackgroundDrawableResource(
					android.R.color.transparent);
			dialog.setContentView(R.layout.dialog_two_button);
			dialog.setCancelable(false);
			dialog.setCanceledOnTouchOutside(false);
			// dialog.setTitle(null);

			Typeface Reguler = Typeface.createFromAsset(context.getAssets(),
					"EncodeSansNormal-400-Regular.ttf");

			Typeface Medium = Typeface.createFromAsset(context.getAssets(),
					"EncodeSansNarrow-500-Medium.ttf");

			// set the custom dialog components - text, image and button
			TextView textView_Title = (TextView) dialog
					.findViewById(R.id.textView_Title);
			textView_Title.setText(Title);
			textView_Title.setTypeface(Reguler);

			TextView textView_Message = (TextView) dialog
					.findViewById(R.id.textView_Message);
			textView_Message.setText(message);
			textView_Message.setTypeface(Medium);

			Button button_Ok = (Button) dialog.findViewById(R.id.button_Ok);
			button_Ok.setTypeface(Reguler);
			// if button is clicked, close the custom dialog
			button_Ok.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
					Utility.setRefreshRequired(MainActivity.this,
							Constants.SHAREDPREFERENCE_REFESH_QUICKPAY, true);
					Utility.setRefreshRequired(MainActivity.this,
							Constants.SHAREDPREFERENCE_REFESH_MY_GIFT, true);
					Utility.setRefreshRequired(MainActivity.this,
							Constants.SHAREDPREFERENCE_REFESH_TRANSATION, true);
					Utility.setRefreshRequired(MainActivity.this,
							Constants.SHAREDPREFERENCE_REFESH_WALLET, true);
					MainActivity.this.finish();
				}
			});

			Button button_Cancel = (Button) dialog
					.findViewById(R.id.button_Cancel);
			button_Cancel.setTypeface(Reguler);
			// if button is clicked, close the custom dialog
			button_Cancel.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
				}
			});

			dialog.show();

		} catch (Exception exception) {
		}
	}

	private void checkNotNull(Object reference, String name) {
		if (reference == null) {
			throw new NullPointerException("Errror");
		}
	}

	public class GetOperatorTask extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... params) {
			try {
				// String response = RequestClass.getInstance()
				// .readPay1B2CRequest(MainActivity.this,
				// Constants.B2C_URL + "get_all_operators/?");
				// return Constants.loadJSONFromAsset(MainActivity.this,
				// "operator.json");
				try {

					long timestamp = System.currentTimeMillis();
					operatorDataSource.open();
					String result;
					// if (!result.startsWith("Error")) {
					// replaced = result.replace("(", "").replace(")", "")
					// .replace(";", "");
					// } else {
					result = Constants.loadJSONFromAsset(MainActivity.this,
							"operator.json");
					// }
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");
					JSONObject jsonObject = new JSONObject(replaced);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {
						JSONObject description = jsonObject
								.getJSONObject("description");
						JSONArray mobile = description.getJSONArray("mobile");
						JSONArray data = description.getJSONArray("data");
						JSONArray postpaid = description
								.getJSONArray("postpaid");
						JSONArray dth = description.getJSONArray("dth");
						operatorDataSource.deleteOperator();
						operatorDataSource.createOperator(mobile,
								Constants.RECHARGE_MOBILE, timestamp);
						operatorDataSource.createOperator(data,
								Constants.RECHARGE_DATA, timestamp);
						operatorDataSource.createOperator(postpaid,
								Constants.BILL_PAYMENT, timestamp);
						operatorDataSource.createOperator(dth,
								Constants.RECHARGE_DTH, timestamp);

					}

				} catch (JSONException e) {
				} catch (SQLException exception) {
				} finally {
					operatorDataSource.close();
				}
				return "";
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}
	}

	public class GetOperatorCodeTask extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... params) {
			try {
				// String time = Utility.getSharedPreferences(MainActivity.this,
				// Constants.LAST_CIRCLE_UPDATED) == null ? "" : Utility
				// .getSharedPreferences(MainActivity.this,
				// Constants.LAST_CIRCLE_UPDATED);
				// String response = RequestClass
				// .getInstance()
				// .readPay1B2CRequest(
				// MainActivity.this,
				// Constants.B2B_URL
				// + "method=getMobileDetails&mobile=all&timestamp="
				// + time);

				String response = Constants.loadJSONFromAsset(
						MainActivity.this, "operatorcode.json");
				parseResponse(response);
				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}
	}

	private void parseResponse(String response) {
		// TODO Auto-generated method stub
		try {
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date date = new Date();
			String timeStamp = dateFormat.format(date);
			// Log.d("Plan", "Plan Start Circle Start Thread Time loop"
			// + timeStamp);

			circleDataSource.open();

			if (!response.startsWith("Error")) {
				String replaced = response.replace("(", "").replace(")", "")
						.replace(";", "");
				// // Log.d("Circle  ", "Plan  "+response);
				replaced = replaced.substring(1, replaced.length() - 1);
				/* JSONArray array = new JSONArray(replaced); */
				// JSONObject jsonObject = new JSONObject(replaced);
				// String status = jsonObject.getString("status");
				// if (status.equalsIgnoreCase("success")) {
				circleDataSource.deleteCircle();
				circleDataSource.createNumberAndCircleList(replaced, timeStamp,
						System.currentTimeMillis());
				Utility.setSharedPreferences(MainActivity.this,
						Constants.LAST_CODE_UPDATED,
						String.valueOf(System.currentTimeMillis()));
				// }
			}
		} catch (SQLException exception) {
			// Log.d("NAC", "row inserted   " + exception.getMessage());
			// TODO: handle exception
			// // // Log.e("Er ", exception.getMessage());
		} catch (Exception e) {
			// Log.d("NAC", "row inserted   " + e.getMessage());
			// TODO: handle exception
			// // // Log.e("Er ", e.getMessage());
		} finally {
			circleDataSource.close();
		}
	}

	public class CheckUpdateTask extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... params) {

			String response = RequestClass
					.getInstance()
					.readPay1B2CAppUpdateRequest(MainActivity.this,
							"https://androidquery.appspot.com/api/market?app=com.pay1");
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			try {
				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONObject jsonObject = new JSONObject(replaced);
					String live_version = jsonObject.getString("version");
					// int curVersion = getPackageManager().getPackageInfo(
					// "com.mindsarray.pay1", 0).versionCode;
					String current_version = getPackageManager()
							.getPackageInfo("com.pay1", 0).versionName;

					// // Log.w("SDFSD ", curVersion + "  " + current_version);

					if (!live_version.equalsIgnoreCase("null")
							&& !current_version.equalsIgnoreCase(live_version)) {

						try {
							final Dialog dialog = new Dialog(MainActivity.this);
							dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
							dialog.getWindow().setBackgroundDrawableResource(
									android.R.color.transparent);
							dialog.setContentView(R.layout.dialog_two_button);
							dialog.setCancelable(false);
							dialog.setCanceledOnTouchOutside(false);
							// dialog.setTitle(null);

							Typeface Reguler = Typeface.createFromAsset(
									getAssets(),
									"EncodeSansNormal-400-Regular.ttf");

							Typeface Medium = Typeface.createFromAsset(
									getAssets(),
									"EncodeSansNarrow-500-Medium.ttf");

							TextView textView_Title = (TextView) dialog
									.findViewById(R.id.textView_Title);
							textView_Title.setText("Pay1 Update Available");
							textView_Title.setTypeface(Reguler);
							// set the custom dialog components - text, image
							// and button
							TextView textView_Message = (TextView) dialog
									.findViewById(R.id.textView_Message);
							textView_Message
									.setText("There is new version of Pay1 application available, click OK to upgrade now?");
							textView_Message.setTypeface(Medium);

							Button button_Ok = (Button) dialog
									.findViewById(R.id.button_Ok);
							button_Ok.setTypeface(Reguler);
							// if button is clicked, close the custom dialog
							button_Ok
									.setOnClickListener(new View.OnClickListener() {
										@Override
										public void onClick(View v) {
											dialog.dismiss();
											Intent intent = new Intent(
													Intent.ACTION_VIEW,
													Uri.parse("market://details?id=com.pay1"));
											startActivity(intent);
										}
									});

							Button button_Cancel = (Button) dialog
									.findViewById(R.id.button_Cancel);
							button_Cancel.setTypeface(Reguler);
							// if button is clicked, close the custom dialog
							button_Cancel
									.setOnClickListener(new View.OnClickListener() {
										@Override
										public void onClick(View v) {
											dialog.dismiss();
										}
									});

							dialog.show();
						} catch (Exception exception) {
						}
					}
				} else {

				}
			} catch (JSONException e) {
				// e.printStackTrace();

			} catch (Exception e) {
				// TODO: handle exception

			}
		}

		@Override
		protected void onPreExecute() {

			super.onPreExecute();
		}

	}

	public class CheckBalanceTask extends AsyncTask<String, String, String> {
		// MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {

			String response = RequestClass.getInstance().readPay1B2CRequest(
					MainActivity.this, Constants.B2C_URL + "check_bal");
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			// if (this.dialog.isShowing()) {
			// this.dialog.dismiss();
			// }
			super.onPostExecute(result);
			try {
				// JSONArray array = new JSONArray(result);
				JSONObject jsonObject = new JSONObject(result);
				String status = jsonObject.getString("status");
				if (status.equalsIgnoreCase("SUCCESS")) {
					JSONObject description = jsonObject
							.getJSONObject("description");
					String account_balance = description
							.getString("account_balance");
					Utility.setBalance(MainActivity.this,
							Constants.SHAREDPREFERENCE_BALANCE,
							account_balance.trim());
					Utility.setBalance(MainActivity.this,
							Constants.SHAREDPREFERENCE_LOYALTY_POINTS,
							description.getString("loyalty_points"));
				}
			} catch (JSONException e) {
				// e.printStackTrace();
				// Constants.showCustomToast(MainActivity.this,
				// result);
			} catch (Exception e) {
				// TODO: handle exception
				// Constants.showCustomToast(MainActivity.this,
				// result);
			}
			adapter.notifyDataSetChanged();
		}

		protected void onPreExecute() {
			// dialog = new MyProgressDialog(MainActivity.this);
			// // this.dialog.setMessage("Please wait.....");
			// this.dialog.setCancelable(true);
			// this.dialog
			// .setOnCancelListener(new DialogInterface.OnCancelListener() {
			//
			// @Override
			// public void onCancel(DialogInterface dialog) {
			// // TODO Auto-generated method stub
			// dialog.dismiss();
			// CheckBalanceTask.this.cancel(true);
			// }
			// });
			// this.dialog.show();
			super.onPreExecute();
		}
	}

	public class CheckBalanceLoaderTask extends
			AsyncTask<String, String, String> implements OnDismissListener {
		MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {

			String response = RequestClass.getInstance().readPay1B2CRequest(
					MainActivity.this, Constants.B2C_URL + "check_bal");
			return params[0] + "#" + response;
		}

		@Override
		protected void onPostExecute(String result) {
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			super.onPostExecute(result);
			try {
				// JSONArray array = new JSONArray(result);
				String[] sp = result.trim().split("#");
				JSONObject jsonObject = new JSONObject(sp[1]);
				String status = jsonObject.getString("status");
				if (status.equalsIgnoreCase("SUCCESS")) {
					JSONObject description = jsonObject
							.getJSONObject("description");
					String account_balance = description
							.getString("account_balance");
					Utility.setBalance(MainActivity.this,
							Constants.SHAREDPREFERENCE_BALANCE,
							account_balance.trim());
					Utility.setBalance(MainActivity.this,
							Constants.SHAREDPREFERENCE_LOYALTY_POINTS,
							description.getString("loyalty_points"));

					showWalletRefreshDialog(Integer.parseInt(sp[0]));
				}
			} catch (JSONException e) {
				// e.printStackTrace();
				// Constants.showCustomToast(MainActivity.this,
				// result);
			} catch (Exception e) {
				// TODO: handle exception
				// Constants.showCustomToast(MainActivity.this,
				// result);
			}
			adapter.notifyDataSetChanged();
		}

		protected void onPreExecute() {
			dialog = new MyProgressDialog(MainActivity.this);
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							dialog.dismiss();
							CheckBalanceLoaderTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			dialog.dismiss();
			CheckBalanceLoaderTask.this.cancel(true);
		}

	}

	public class FreeBiesTask extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... params) {
			try {
				// String response = RequestClass
				// .getInstance()
				// .readPay1B2CRequest(
				// MainActivity.this,
				// Constants.B2C_URL
				// + "Get_all_deal_data/?latitude="
				// + params[0]
				// + "&longitude="
				// + params[1]
				// + "&mobile="
				// + Utility
				// .getMobileNumber(
				// MainActivity.this,
				// Constants.SHAREDPREFERENCE_MOBILE));

				String result = Constants.loadJSONFromAsset(MainActivity.this,
						"gift.json");
				if (!result.startsWith("Error")) {

					try {

						freeBiesDataSource.open();
						freeBiesLocationDataSource.open();

						freeBiesDataSource.deleteFreeBies();
						freeBiesLocationDataSource.deleteFreeBiesLocation();

						long t = System.currentTimeMillis();

						freeBiesDataSource.createFreeBies(result, t);

						// freeBiesLocationDataSource.createFreeBiesLocation(
						// result, t);

						// Utility.setSharedPreferences(MainActivity.this,
						// Constants.LAST_GIFT_UPDATED,
						// String.valueOf(System.currentTimeMillis()));
					} catch (SQLException exception) {
						// TODO: handle exception
						Log.e("Er ", exception.getMessage());
					} catch (Exception e) {
						// TODO: handle exception
						Log.e("Er ", e.getMessage());
					} finally {
						freeBiesDataSource.close();
						freeBiesLocationDataSource.close();
					}
				}
			} catch (Exception e) {

			}
			return "Error";
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

		}

		protected void onPreExecute() {
			super.onPreExecute();
		}

	}

	public class LogoutTask extends AsyncTask<String, String, String> implements
			OnDismissListener {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			try {
				String response = RequestClass.getInstance()
						.readPay1B2CRequest(MainActivity.this,
								Constants.B2C_URL + "signout/");

				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			try {

				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONObject jsonObject = new JSONObject(replaced);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {

						SharedPreferences.Editor editor = mPrefs.edit();
						editor.putString("access_token", null);
						Utility.setFbImage64(MainActivity.this, "");
						editor.commit();

						Utility.setLoginFlag(MainActivity.this,
								Constants.SHAREDPREFERENCE_IS_LOGIN, false);

						Utility.setRefreshRequired(MainActivity.this,
								Constants.SHAREDPREFERENCE_REFESH_QUICKPAY,
								true);
						Utility.setRefreshRequired(MainActivity.this,
								Constants.SHAREDPREFERENCE_REFESH_MY_GIFT, true);
						Utility.setRefreshRequired(MainActivity.this,
								Constants.SHAREDPREFERENCE_REFESH_TRANSATION,
								true);
						Utility.setRefreshRequired(MainActivity.this,
								Constants.SHAREDPREFERENCE_REFESH_WALLET, true);

						Utility.setUserName(MainActivity.this,
								Constants.SHAREDPREFERENCE_NAME, null);
						// Utility.setMobileNumber(MainActivity.this,
						// Constants.SHAREDPREFERENCE_MOBILE, null);
						Utility.setDOB(MainActivity.this,
								Constants.SHAREDPREFERENCE_DOB, null);
						// Utility.setEmail(MainActivity.this,
						// Constants.SHAREDPREFERENCE_EMAIL, null);
						Utility.setGender(MainActivity.this,
								Constants.SHAREDPREFERENCE_GENDER, null);
						Utility.setUserID(MainActivity.this,
								Constants.SHAREDPREFERENCE_USER_ID, null);
						Utility.setBalance(MainActivity.this,
								Constants.SHAREDPREFERENCE_BALANCE, null);
						Utility.setSharedPreferences(MainActivity.this,
								Constants.SHAREDPREFERENCE_PURCHASE_DATA, null);
						Utility.setSharedPreferences(MainActivity.this,
								Constants.SHAREDPREFERENCE_WALLET_DATA, null);
						Utility.setSharedPreferences(MainActivity.this,
								Constants.SHAREDPREFERENCE_MY_GIFTS_DATA, null);

						try {
							quickPayDataSource.open();
							// freeBiesDataSource.open();
							reClaimGiftDataSource.open();

							quickPayDataSource.deleteQuickPay();
							// freeBiesDataSource.deleteFreeBies();
							reClaimGiftDataSource.deleteReClaimGift();

						} catch (SQLException exception) {
							// TODO: handle exception
							// // // Log.e("Er ", exception.getMessage());
						} catch (Exception e) {
							// TODO: handle exception
							// // // Log.e("Er ", e.getMessage());
						} finally {
							quickPayDataSource.close();
							// freeBiesDataSource.close();
							reClaimGiftDataSource.close();
						}

						// Constants.showDialog(MainActivity.this, TAG,
						// Constants.checkCode(replaced),
						// Constants.DIALOG_CLOSE);
						// Intent intent = new Intent(MainActivity.this,
						// LoginActivity.class);
						// // Closing all the Activities
						// intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						// // Add new Flag to start new Activity
						// intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK );
						// startActivity(intent);
						// finish();
						// adapter.notifyDataSetChanged();

						// displayView(1);
						try {
							NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
							notificationManager.cancelAll();
						} catch (Exception e) {
						}

						Utility.setOneTimeRegistration(MainActivity.this,
								Constants.SHAREDPREFERENCE_REGISTRATION, false);
						Utility.setOneTimeRegistration(MainActivity.this,
								Constants.SHAREDPREFERENCE_PREFILLED, false);
						Intent intent = new Intent(MainActivity.this,
								RegistrationActivity.class);
						startActivity(intent);
						finish();
					} else {
						Constants.showOneButtonDialog(MainActivity.this,
								"Logout", Constants.checkCode(replaced),
								Constants.DIALOG_CLOSE);
					}

				} else {
					Intent intent = new Intent(MainActivity.this,
							ConnectivityErrorActivity.class);
					startActivity(intent);

				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonDialog(MainActivity.this, "Logout",
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonDialog(MainActivity.this, "Logout",
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			}

		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(MainActivity.this);
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							LogoutTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			LogoutTask.this.cancel(true);
			dialog.cancel();
		}

	}

	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		if (location != null) {

			double latitude = location.getLatitude();

			// Getting longitude of the current location
			double longitude = location.getLongitude();

			// Creating a LatLng object for the current location
			// LatLng latLng = new LatLng(latitude, longitude);

			// Showing the current location in Google Map
			// googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

			// Zoom in the Google Map
			// googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));

			Utility.setCurrentLatitude(MainActivity.this,
					Constants.SHAREDPREFERENCE_CURRENT_LATITUDE,
					String.valueOf(latitude));
			Utility.setCurrentLongitude(MainActivity.this,
					Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE,
					String.valueOf(longitude));

		}
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}

	public class LoadImageTask extends AsyncTask<String, String, Bitmap> {

		@Override
		protected Bitmap doInBackground(String... params) {

			Bitmap bitmap = null;
			URL imageURL = null;
			try {
				imageURL = new URL("https://graph.facebook.com/" + params[0]
						+ "/picture?type=large");
				// imageURL = new URL(Utility.getFbImage(context));

				bitmap = BitmapFactory.decodeStream(imageURL.openConnection()
						.getInputStream());
				Log.d("dd", "fff");
				return bitmap;
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Log.d("dd", "fff");

			return bitmap;
		}

		@Override
		protected void onPostExecute(Bitmap result) {

			super.onPostExecute(result);
			try {
				if (result != null) {

					String imgeInBase64 = Constants.encodeTobase64(result);
					Utility.setFbImage64(MainActivity.this, imgeInBase64);
					// fbConnectButton.setImageBitmap(Constants.decodeBase64(Utility.getFbImage64(context)));
					adapter.notifyDataSetChanged();
					// fbConnectButton.setImageBitmap(result);
				}
			} catch (Exception e) {
				// TODO: handle exception
				// Constants.showCustomToast(MainActivity.this,
				// result);
			}
			// notifyDataSetChanged();

		}

		protected void onPreExecute() {

			super.onPreExecute();
		}
	}

	public class UpdateProfileTask extends AsyncTask<String, String, String>
			implements OnDismissListener {
		// private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			try {
				String response = RequestClass
						.getInstance()
						.readPay1B2CRequest(
								MainActivity.this,
								Constants.B2C_URL + "update_profile/?name="
										+ URLEncoder.encode(params[0], "utf-8")
										+ "&email="
										+ URLEncoder.encode(params[1], "utf-8")
										+ "&gender="
										+ URLEncoder.encode(params[2], "utf-8"));

				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			/*
			 * if (this.dialog.isShowing()) { this.dialog.dismiss(); }
			 */
			try {

				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONObject jsonObject = new JSONObject(replaced);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {
						JSONObject profileData = jsonObject
								.getJSONObject("profileData");
						Utility.setUserName(MainActivity.this,
								Constants.SHAREDPREFERENCE_NAME,
								profileData.getString("name"));
						Utility.setMobileNumber(MainActivity.this,
								Constants.SHAREDPREFERENCE_MOBILE,
								profileData.getString("mobile"));
						Utility.setEmail(MainActivity.this,
								Constants.SHAREDPREFERENCE_EMAIL,
								profileData.getString("email"));
						Utility.setGender(
								MainActivity.this,
								Constants.SHAREDPREFERENCE_GENDER,
								profileData.getString("gender").startsWith("M") ? "m"
										: "f");

						Utility.setDOB(MainActivity.this,
								Constants.SHAREDPREFERENCE_DOB,
								profileData.getString("date_of_birth"));

					} else {

					}

				} else {

				}
			} catch (JSONException e) {

			} catch (Exception e) {

			}
		}

		@Override
		protected void onPreExecute() {
			/*
			 * dialog = new MyProgressDialog(MainActivity.this); //
			 * this.dialog.setMessage("Please wait.....");
			 * this.dialog.setCancelable(false); this.dialog
			 * .setOnCancelListener(new DialogInterface.OnCancelListener() {
			 * 
			 * @Override public void onCancel(DialogInterface dialog) { // TODO
			 * Auto-generated method stub UpdateProfileTask.this.cancel(true); }
			 * }); this.dialog.show(); super.onPreExecute();
			 */
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			UpdateProfileTask.this.cancel(true);
			dialog.cancel();
		}

	}

	protected void ShowToastWithImage(String msg) {
		// TODO Auto-generated method stub
		View toastView = getLayoutInflater().inflate(R.layout.toast,
				(ViewGroup) findViewById(R.id.toastLayout));

		TextView textView = (TextView) toastView.findViewById(R.id.text);

		textView.setText(msg);
		Toast toast = new Toast(MainActivity.this);

		toast.setGravity(Gravity.CENTER_VERTICAL, 0, -100);
		toast.setDuration(200);
		toast.setView(toastView);

		toast.show();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		// Toast.makeText(MainActivity.this, "HI", Toast.LENGTH_SHORT).show();
		Fragment fragment = GiftMainActivity.newInstance(1);
		if (fragment != null) {
			FragmentManager fragmentManager = getSupportFragmentManager();
			FragmentTransaction fragmentTransaction = fragmentManager
					.beginTransaction();
			fragmentTransaction.replace(R.id.frame_container, fragment);
			fragmentTransaction.addToBackStack(null);
			fragmentTransaction.commit();

			// update selected item and title, then close the drawer

			// mDrawerList.setItemChecked(position, true);
			// mDrawerList.setSelection(position);
			// mDrawerList.setSelected(true);

			// setTitle(navMenuTitles[position]);
			mDrawerLayout.closeDrawer(mDrawerList);
		} else {
			// error in creating fragment
			// Log.e("MainActivity", "Error in creating fragment");
		}
	}

}
