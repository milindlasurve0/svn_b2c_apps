package com.pay1.customviews;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;

import com.pay1.R;

public class MyProgressDialog extends Dialog {

	// private GifMovieView imageView_Icon;
	private Dialog dialog;
	Context context;

	public MyProgressDialog(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		this.context = context;
		try {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(context.LAYOUT_INFLATER_SERVICE);
			View layout = inflater.inflate(
					R.layout.custom_progressdialog_activity, null);
			// imageView_Icon = (GifMovieView) layout
			// .findViewById(R.id.imageView_Icon);
			// imageView_Icon.setMovieResource(R.drawable.loader);

			dialog = new Dialog(context);

			this.dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setCanceledOnTouchOutside(false);
			this.dialog.getWindow().setBackgroundDrawableResource(
					android.R.color.transparent);
			this.dialog.setContentView(layout);
			

		} catch (Exception e) {
		}

	}

	public void show() {
		this.dialog.show();
	}

	public boolean isShowing() {
		return this.dialog.isShowing();
	}

	public void dismiss() {
		this.dialog.dismiss();
	}

	public void setCancelable(boolean cancelable) {
		this.dialog.setCancelable(cancelable);
	}
}
