﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;
using NewPay1B2C.Model;
using NewPay1B2C.UserControls;
using NewPay1B2C.UtilityClass;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SQLiteClient;

namespace NewPay1B2C
	{
	public partial class RechargeAndBills : PhoneApplicationPage
		{
		string flag = "1";
		RechargeControl rechargeControl;
		DthRechargeControl dthControl;
		string mobileNo, stv = "0";
		string area, area_name;
		SQLiteConnection db = null;
		string op_code, op_id, pay1_pro_id;
		int min, max;
		String charges_slab = null;
		string prefix, length;
		double service_charge_amount = 0, service_charge_percent = 0, service_tax_percent = 0, dblMin = 0, dblMax = 0, dblAmount = 0;
		string totallRechargeAmount;
		ProgressBarManager ProgressBarManager;
		string userRechargeNumber, rechargeAmount;
		List<OperatorDB> queryResult = null;
		bool rechargeViaWallet = true;
		int mainBalance;
		int mobileNumberLength;
		OperatorDB selectedRow;
		public RechargeAndBills ()
			{
			InitializeComponent ();


			Constant.laodNavigationBar (TitlePanel, LayoutRoot, canvas, "Recharge And Bills");
			GoogleAnalytics.EasyTracker.GetTracker ().SendView ("Recharge And Bills");
			rechargeControl = new RechargeControl ();
			dthControl = new DthRechargeControl ();
			mainBalance = Constant.getbalanceInInt ();
			ProgressIndicator prog = new ProgressIndicator ();

			ProgressBarManager = new ProgressBarManager ();
			SystemTray.SetProgressIndicator (this, prog);
			ProgressBarManager.Hook (prog);


			if (db == null)
				{
				db = new SQLiteConnection (Constant.DATABASE_NAME);
				db.Open ();
				}


			}


		protected override void OnBackKeyPress (System.ComponentModel.CancelEventArgs e)
			{
			//Do your work here
			base.OnBackKeyPress (e);
			NavigationService.Navigate (new Uri ("/HomePage.xaml?", UriKind.RelativeOrAbsolute));
			while (this.NavigationService.BackStack.Any ())
				{
				this.NavigationService.RemoveBackEntry ();
				}
			}

		private void Pivot_SelectionChanged_1 (object sender, SelectionChangedEventArgs e)
			{
			switch (((Pivot)sender).SelectedIndex)
				{
				case 0:
					rechargeControl = mobileRechargeControl;
					rechargeControl.prepaidRadioButton.Checked += new RoutedEventHandler (RadioButton_CheckedPrePaid);
					rechargeControl.postpaidRadioButton.Checked += new RoutedEventHandler (RadioButton_CheckedPostPaid);
					rechargeControl.AdsToggleSwitch.Checked += new EventHandler<RoutedEventArgs> (AdsToggleSwitch_Checked);
					rechargeControl.AdsToggleSwitch.Unchecked += new EventHandler<RoutedEventArgs> (AdsToggleSwitch_Unchecked);
					flag = "1";

					break;

				case 1:
					flag = "2";
					dthControl = dthRechargeControl;
					break;
				case 2:
					rechargeControl = datacardRechargeControl;
					rechargeControl.radioPanel.Visibility = Visibility.Collapsed;

					flag = "3";

					break;

				}

			PhoneApplicationService.Current.State[Constant.OPERATOR_RECHARGE_FLAG] = flag;
			rechargeControl.textAmount.TextChanged += new TextChangedEventHandler (textAmount_TextChanged);

			rechargeControl.mobileNumber.TextChanged += new TextChangedEventHandler (textMobileChange);
			rechargeControl.textCCDC.Tap += textCCDC_Tap;
			rechargeControl.imageContact.Tap += ActionIcon_Tapped;
			rechargeControl.textPlansEvent.Tap += textPlansEvent_Tap;

			rechargeControl.searchOperator.Tap += (z, x) => { NavigationService.Navigate (new Uri ("/OperatorList.xaml?", UriKind.RelativeOrAbsolute)); PhoneApplicationService.Current.State["mobNumber"] = rechargeControl.mobileNumber.Text; }; ;
			rechargeControl.btnPayment.Click += new RoutedEventHandler (buttonImage_Click);


			dthControl.textAmount.TextChanged += new TextChangedEventHandler (textAmount_TextChanged);

			dthControl.mobileNumber.TextChanged += new TextChangedEventHandler (textMobileChange);
			dthControl.textCCDC.Tap += textCCDC_Tap;
			dthControl.imageContact.Tap += ActionIcon_Tapped;
			dthControl.textPlansEvent.Tap += textDthPlansEvent_Tap;

			dthControl.stackOperator.Tap += (z, x) => { NavigationService.Navigate (new Uri ("/OperatorList.xaml?", UriKind.RelativeOrAbsolute)); PhoneApplicationService.Current.State["mobNumber"] = rechargeControl.mobileNumber.Text; }; ;
			dthControl.btnPayment.Click += new RoutedEventHandler (buttonImage_Click);



			}

		private void textDthPlansEvent_Tap (object sender, System.Windows.Input.GestureEventArgs e)
			{
			if (pay1_pro_id != null)
				{
				NavigationService.Navigate (new Uri ("/DthPlans.xaml?op_id=" + pay1_pro_id, UriKind.Relative));
				//StartTransition ();
				}
			else
				{
				MessageBox.Show ("Please select an operator");
				}
			}

		private void textPlansEvent_Tap (object sender, System.Windows.Input.GestureEventArgs e)
			{
			if (pay1_pro_id != null)
				{
				NavigationService.Navigate (new Uri ("/PlansActivity.xaml?code=" + area + "&op_id=" + pay1_pro_id + "&cir_name=" + area_name + "&op_name=AAA", UriKind.RelativeOrAbsolute));
				//StartTransition ();
				}
			else
				{
				MessageBox.Show ("Please select an operator");
				}
			}

		private void AdsToggleSwitch_Checked (object sender, RoutedEventArgs e)
			{
			ToggleSwitch AdsToggleSwitch = sender as ToggleSwitch;
			AdsToggleSwitch.Content = "Special";
			stv = "1";
			}

		private void AdsToggleSwitch_Unchecked (object sender, RoutedEventArgs e)
			{
			ToggleSwitch AdsToggleSwitch = sender as ToggleSwitch;
			AdsToggleSwitch.Content = "Normal";
			stv = "0";

			}

		private void buttonImage_Click (object sender, RoutedEventArgs e)
			{

			//rechargeAmount = rechargeControl.textAmount.Text;

			if (string.IsNullOrEmpty (userRechargeNumber))
				{
				if (flag.Equals ("2"))
					MessageBox.Show ("Please enter customer Id.");
				else
					MessageBox.Show ("Please enter Mobile number.");
				return;
				}
			if (string.IsNullOrEmpty (rechargeAmount))
				{
				MessageBox.Show ("Please enter amount.");
				return;
				}
			if (string.IsNullOrEmpty (op_id))
				{
				MessageBox.Show ("Please select operator.");
				return;
				}



			if (userRechargeNumber.Length != 0)
				{
				if (userRechargeNumber.Length == mobileNumberLength)
					{
					if (rechargeAmount.Length != 0)
						{
						int recAmount = Convert.ToInt32 (rechargeAmount);
						if (recAmount >= min)
							{
							if (recAmount <= max)
								{

								if (checkPrefix (userRechargeNumber))
									{
									ShowPopUp (rechargeViaWallet, flag, userRechargeNumber, recAmount, op_id, pay1_pro_id);
									}
								else
									{
									}


								}
							else
								{
								MessageBox.Show ("maximum amount should be " + max);
								}
							}
						else
							{
							MessageBox.Show ("minimum amount should be " + min);
							}
						}
					else
						{
						MessageBox.Show ("Please enter amount");
						}
					}

				else
					{
					if (!flag.Equals ("2"))
						{

						MessageBox.Show ("Please enter a valid number");
						}
					else
						{
						MessageBox.Show (DthOperatorDetails ());
						}
					}
				}
			else
				{
				MessageBox.Show ("Please enter number");
				}
			}

		private bool checkPrefix (string userNumber)
			{
			bool checkNumber = false;

			if (flag.Equals ("2"))
				{

				string[] pipe_seperator = prefix.Split (',');
				foreach (String s1 in pipe_seperator)
					{
					if (userNumber.StartsWith (s1))
						{
						checkNumber = true;
						break;

						}
					else
						{
						checkNumber = false;
						MessageBox.Show (DthOperatorDetails ());
						}

					}

				}
			else
				{
				checkNumber = true;
				//break;
				}



			return checkNumber;
			}


		string DthOperatorDetails ()
			{
			int pay1Id = Convert.ToInt16 (pay1_pro_id);
			string operatorMsg = "";
			switch (pay1Id)
				{
				case 16:
						{
						operatorMsg = "Customer ID " + "starts with " + prefix + " and is " + length + " digits long.";
						}
					break;
				case 17:
						{
						operatorMsg = "Smart card number " + "starts with " + prefix + " and is " + length + " digits long.";
						}
					break;
				case 18:
						{
						operatorMsg = "Your viewing card (VC) number " + "starts with 0 and is " + length + " digits long.";
						}
					break;
				case 19:
						{
						operatorMsg = "Smart card number " + "starts with " + prefix + " and is " + length + " digits long.";
						}
					break;
				case 20:
						{

						operatorMsg = "Subscriber ID " + "starts with " + prefix + " and is " + length + " digits long.";
						}
					break;
				case 21:
						{
						operatorMsg = "SMS ID to 921-201-2299 from your registered mobile to retrieve your Customer ID.";
						}
					break;

				default:
					break;
				}

			return operatorMsg;
			}


		private void ShowPopUp (bool viaWallet, string recFlag, string popupNumber, int amount, string oper_id, string pay1_pro_id1)
			{
			try
				{
				ContentOverlay.Visibility = Visibility.Visible;
				Popup popup = new Popup ();
				popup.Height = 300;
				popup.Width = 400;
				popup.VerticalOffset = 200;
				RechargePopup control = new RechargePopup ();
				popup.Child = control;
				control.imageOperator.Source = OperatorConstant.getOperatorResource (Convert.ToInt16 (pay1_pro_id1));
				control.textPopupNumber.Text = popupNumber;
				control.textPopupAmount.Text = "Rs. " + amount;
				popup.IsOpen = true;
				control.buttonImageOk.Click += (s, args) =>
				{
					rechargeTask (viaWallet, recFlag, Convert.ToString (amount), popupNumber, oper_id);
					ContentOverlay.Visibility = Visibility.Collapsed;
					popup.IsOpen = false;

				};
				control.buttonImageCancel.Click += (s, args) =>
				{
					ContentOverlay.Visibility = Visibility.Collapsed;
					popup.IsOpen = false;
				};
				}
			catch (Exception e)
				{
				}
			}


		private void RadioButton_CheckedPrePaid (object sender, RoutedEventArgs e)
			{

			flag = "1";
			setOperator (flag, op_code);
			}

		private void RadioButton_CheckedPostPaid (object sender, RoutedEventArgs e)
			{


			flag = "4";
			setOperator (flag, op_code);
			}


		protected override void OnNavigatedTo (System.Windows.Navigation.NavigationEventArgs e)
			{

			if (string.IsNullOrEmpty (PhoneApplicationService.Current.State["data"].ToString ()))
				{
				}
			else
				{
				if (PhoneApplicationService.Current.State["data"].ToString ().Equals ("1"))
					{
					selectedRow = (OperatorDB)PhoneApplicationService.Current.State["operatorData"];
					setOperator (selectedRow.flag, selectedRow.code);
					}

				}

			if (NavigationContext.QueryString.ContainsKey ("validity"))
				{

				string validity = NavigationContext.QueryString["validity"];
				string amt = NavigationContext.QueryString["amt"];
				string desc = NavigationContext.QueryString["desc"];
				string op_id = NavigationContext.QueryString["op_id"];

				if (flag.Equals ("2"))
					{
					dthControl.textAmount.Text = amt;
					}
				else
					{
					rechargeControl.textAmount.Text = amt;
					}

				//area = PhoneApplicationService.Current.State["area"].ToString ();
				}
			}
		private void setOperator (string flag, string op_code)
			{
			if (string.IsNullOrEmpty (op_code))
				{
				}
			else
				{
				try
					{
					SQLiteCommand cmd1 = db.CreateCommand ("SELECT DISTINCT(p_id),* FROM OperatorTable WHERE flag='" + flag + "' AND code='" + op_code + "'");
					var lst = cmd1.ExecuteQuery<OperatorDB> ();
					queryResult = lst.ToList ();
					//PhoneApplicationService.Current.State["operatorData"] = queryResult.First();
					selectedRow = queryResult.First ();
					string isStv = selectedRow.stv;
					var min1 = selectedRow.min;
					var max1 = selectedRow.max;
					min = Convert.ToInt32 (min1);
					max = Convert.ToInt32 (max1);
					op_id = selectedRow.id;
					pay1_pro_id = selectedRow.p_id;
					charges_slab = selectedRow.charges_slab;
					service_charge_amount = Convert.ToDouble (selectedRow.service_charge_amount);
					service_charge_percent = Convert.ToDouble (selectedRow.service_charge_percent);
					service_tax_percent = Convert.ToDouble (selectedRow.service_tax_percent);
					prefix = selectedRow.prefix;
					length = selectedRow.length;
					dblMin = Convert.ToDouble (selectedRow.min);
					dblMax = Convert.ToDouble (selectedRow.max);
					if (flag.Equals ("2"))
						{
						dthControl.textOperator.Text = selectedRow.name;
						mobileNumberLength = Convert.ToInt16 (length);
						dthControl.checkPlan.Visibility = Visibility.Visible;
						dthControl.mobileNumber.Hint = setHint (pay1_pro_id);
						}
					else
						{
						mobileNumberLength = 10;
						rechargeControl.textOperator.Text = selectedRow.name;
						rechargeControl.checkPlan.Visibility = Visibility.Visible;

						}

					if (isStv.Equals ("1"))
						{
						rechargeControl.AdsToggleSwitch.Visibility = Visibility.Visible;
						}
					else
						{
						rechargeControl.AdsToggleSwitch.Visibility = Visibility.Collapsed;
						}
					}
				catch (SQLiteException se)
					{
					}
				catch (Exception e)
					{
					}

				}
			}

		private string setHint (string pay1_pro_id)
			{
			string hint = "";
			switch (Convert.ToInt16 (pay1_pro_id))
				{
				case 16:
						{
						hint = "Customer ID";
						break;
						}
				case 17:
						{
						hint = "Smart Card Number";
						break;
						}
				case 18:
						{
						hint = "Viewing Card Number";
						break;
						}
				case 19:
						{
						hint = "Smart Card Number";
						break;
						}
				case 20:
						{
						hint = "Subscriber ID";
						break;
						}
				case 21:
						{
						hint = "Customer ID";
						break;
						}
				}
			return hint;
			}




		private void textMobileChange (object sender, TextChangedEventArgs e)
			{
			PhoneTextBox mobileNumber = sender as PhoneTextBox;
			userRechargeNumber = mobileNumber.Text;
			if (!flag.Equals ("2"))
				{
				if (mobileNumber.Text.Length >= 4)
					{

					if ((mobileNumber.Text.StartsWith ("7") || mobileNumber.Text.StartsWith ("8") || mobileNumber.Text.StartsWith ("9")))
						{
						SQLiteCommand cmd = db.CreateCommand ("");
						cmd.CommandText = "SELECT * FROM NumberAndCircle WHERE number LIKE '" + mobileNumber.Text + "%'";
						var rec = cmd.ExecuteQuery<NumberAndCircle> ();
						List<NumberAndCircle> NAC = rec.ToList<NumberAndCircle> ();
						if (NAC.Count >= 1)
							{
							area = NAC.First ().area;
							area_name = NAC.First ().area_name;
							//textOperator.Text = NAC.First ().opr_name;
							op_code = NAC.First ().numberOperator;
							PhoneApplicationService.Current.State["area"] = area;
							setOperator (flag, op_code);
							}
						else
							{
							//Task<string> circle = webServiceForCircle ();
							}
						}
					else
						{
						MessageBox.Show ("Please enter valid mobile number.");
						}

					}
				}
			if (userRechargeNumber.Length == 10)
				{
				if (!flag.Equals ("2"))
					{
					rechargeControl.textAmount.Focus ();
					}
				}
			}


		private void ActionIcon_Tapped (object sender, EventArgs e)
			{
			PhoneNumberChooserTask phoneNumberChooserTask = new PhoneNumberChooserTask ();
			phoneNumberChooserTask.Completed += new EventHandler<PhoneNumberResult> (phoneNumberChooserTask_Completed);
			phoneNumberChooserTask.Show ();
			}

		void phoneNumberChooserTask_Completed (object sender, PhoneNumberResult e)
			{
			if (e.TaskResult == TaskResult.OK)
				{
				try
					{
					mobileNo = Constant.RemoveSpecialCharacters (e.PhoneNumber);
					mobileNo = Constant.GetLast (mobileNo, 10);
					string contactName = Constant.RemoveSpecialCharacters (e.DisplayName);

					if (mobileNo.Length >= 10)
						{
						rechargeControl.mobileNumber.Text = mobileNo;

						}

					else
						{
						MessageBox.Show ("Please select correct mobile number.");
						}
					}
				catch (Exception er)
					{
					}
				}
			}




		private void textAmount_TextChanged (object sender, TextChangedEventArgs e)
			{
			PhoneTextBox textAmount = sender as PhoneTextBox;
			rechargeAmount = textAmount.Text;
			if (textAmount.Text.Length != 0)
				{

				int recAmount = Convert.ToInt32 (textAmount.Text);
				if (recAmount > mainBalance || mainBalance == 0)
					{
					rechargeViaWallet = false;
					if (!flag.Equals ("2"))
						{
						rechargeControl.btnPayment.Content = "Pay via card or Netbanking";
						rechargeControl.textCCDC.Text = "Cash Topup? Locate shop";
						}
					else
						{
						dthControl.btnPayment.Content = "Pay via card or Netbanking";
						dthControl.textCCDC.Text = "Cash Topup? Locate shop";
						}

					}
				else
					{
					rechargeViaWallet = true;
					if (!flag.Equals ("2"))
						{
						rechargeControl.btnPayment.Content = "Continue to payment";
						rechargeControl.textCCDC.Text = "Pay via  Credit/Debit Card";
						}
					else
						{
						dthControl.btnPayment.Content = "Continue to payment";
						dthControl.textCCDC.Text = "Pay via  Credit/Debit Card";
						}
					}

				if (flag.Equals ("4"))
					{
					totallRechargeAmount = CalculateTotalAmount (rechargeControl.textService, textAmount.Text, op_code);
					}
				}
			else
				{
				rechargeControl.textService.Text = "";
				}
			}




		private string CalculateTotalAmount (TextBlock txtBlk, string amount, string opId)
			{
			int slab_amount = 0;
			try
				{
				dblAmount = Convert.ToDouble (amount);


				charges_slab = selectedRow.charges_slab;
				service_charge_amount = Convert.ToDouble (selectedRow.service_charge_amount);
				service_charge_percent = Convert.ToDouble (selectedRow.service_charge_percent);
				service_tax_percent = Convert.ToDouble (selectedRow.service_tax_percent);
				dblMin = Convert.ToDouble (selectedRow.min);
				dblMax = Convert.ToDouble (selectedRow.max);




				if (string.IsNullOrEmpty (charges_slab) == true)
					{
					dblAmount = (dblAmount + (dblAmount * service_charge_percent));
					dblAmount = (dblAmount + (dblAmount * service_tax_percent));
					}
				else
					{
					String[] comma_seperator = charges_slab.Split (',');

					foreach (String s in comma_seperator)
						{
						String[] colon_sperator = s.Split (':');
						if (dblAmount >= Convert.ToDouble (colon_sperator[0]))
							{
							slab_amount = Convert.ToInt32 (colon_sperator[1]);
							}
						}
					}
				double tot = dblAmount + slab_amount;
				txtBlk.Text = "( " + tot + " = " + amount + " + " + slab_amount + " service charge inclusive of all taxes.)";
				if (amount.Length == 0)
					{
					txtBlk.Text = "";
					}

				}
			catch (SQLiteException se)
				{
				string see = se.Message;
				}
			return Convert.ToString (dblAmount + slab_amount);




			}


		private async void rechargeTask (bool rechargeViaWallet, string rechargeFlag, string amount, string number, string op_id1)
			{
			ProgressBarManager.Show (1, "Please Wait", false, 10);
			var values = new List<KeyValuePair<string, string>> ();


			if (rechargeViaWallet)
				{
				values.Add (new KeyValuePair<string, string> ("paymentopt", "wallet"));
				}
			else
				{
				values.Add (new KeyValuePair<string, string> ("paymentopt", "online"));
				}
			values.Add (new KeyValuePair<string, string> ("payment", "1"));
			values.Add (new KeyValuePair<string, string> ("api", "true"));
			values.Add (new KeyValuePair<string, string> ("name", ""));
			values.Add (new KeyValuePair<string, string> ("operator", op_id1));
			values.Add (new KeyValuePair<string, string> ("flag", rechargeFlag));


			if (rechargeFlag.Equals ("1") || rechargeFlag.Equals ("3"))
				{
				values.Add (new KeyValuePair<string, string> ("actiontype", "recharge"));
				values.Add (new KeyValuePair<string, string> ("amount", amount));
				values.Add (new KeyValuePair<string, string> ("mobile_number", number));
				values.Add (new KeyValuePair<string, string> ("stv", stv));
				values.Add (new KeyValuePair<string, string> ("recharge", "1"));
				}
			else if (rechargeFlag.Equals ("2"))
				{
				values.Add (new KeyValuePair<string, string> ("actiontype", "recharge"));
				values.Add (new KeyValuePair<string, string> ("amount", amount));
				values.Add (new KeyValuePair<string, string> ("subscriber_id", number));
				values.Add (new KeyValuePair<string, string> ("recharge", "1"));
				}
			else if (rechargeFlag.Equals ("4"))
				{

				values.Add (new KeyValuePair<string, string> ("actiontype", "billpayment"));
				values.Add (new KeyValuePair<string, string> ("amount", totallRechargeAmount));
				values.Add (new KeyValuePair<string, string> ("payment_flag", "1"));
				values.Add (new KeyValuePair<string, string> ("billpayment", "1"));
				values.Add (new KeyValuePair<string, string> ("base_amount", amount));
				values.Add (new KeyValuePair<string, string> ("service_charge", "" + charges_slab));
				values.Add (new KeyValuePair<string, string> ("service_tax", "" + service_tax_percent));
				values.Add (new KeyValuePair<string, string> ("mobile_number", number));
				}
			try
				{
				FormUrlEncodedContent values1 = new FormUrlEncodedContent (values);
				Task<string> response = Constant.postParamsAndgetResponse (Constant.B2C_URL, values1);

				string result = await response;
				if (string.IsNullOrEmpty (result))
					{
					NavigationService.Navigate (new Uri ("/NoConnection.xaml?", UriKind.Relative));
					}
				else
					{
					var dict = (JObject)JsonConvert.DeserializeObject (result);
					string status = dict["status"].ToString ();

					//PhoneApplicationService.Current.State[dealData] = result;

					if (rechargeViaWallet)
						{
						if (status.Equals ("success"))
							{
							var description = dict["description"];
							Constant.setbalance (description["closing_balance"].ToString ());

							Constant.ShowSuccessPopUp (ContentOverlay, rechargeFlag, number, amount, op_id, pay1_pro_id);
							}
						else
							{
							/*string errCode = dict["errCode"].ToString ();
							if (errCode.Equals ("201"))
								{
								(Application.Current.RootVisual as PhoneApplicationFrame).Navigate (new Uri ("/Login.xaml?no-cache=" + Guid.NewGuid (), UriKind.Relative));

								(Application.Current.RootVisual as PhoneApplicationFrame).RemoveBackEntry ();

								//return responseString.ToString();	  
								}
							else
								{

								}	  */
							Constant.checkForErrorCode (result);
							}
						}
					else
						{
						if (status.Equals ("success"))
							{
							var description = dict["description"];
							string form_content = description["form_content"].ToString ();
							PhoneApplicationService.Current.State[Constant.PAYMENT_GATEWAY_HTML] = form_content;
							NavigationService.Navigate (new Uri ("/PaymentGatewayLayout.xaml?page=HomePage", UriKind.Relative));
							NavigationService.RemoveBackEntry ();
							}
						else
							{
							/*string errCode = dict["errCode"].ToString ();
							if (errCode.Equals ("201"))
								{
								(Application.Current.RootVisual as PhoneApplicationFrame).Navigate (new Uri ("/Login.xaml?no-cache=" + Guid.NewGuid (), UriKind.Relative));

								(Application.Current.RootVisual as PhoneApplicationFrame).RemoveBackEntry ();

								//return responseString.ToString();
								}
							else
								{

								}	*/
							Constant.checkForErrorCode (result);
							}
						}

					}
				ProgressBarManager.Hide (1);
				}
			catch (WebException we)
				{
				}
			catch (Exception e)
				{
				}
			//Constant.SetProgressIndicator(false);
			}
		private void textCCDC_Tap (object sender, System.Windows.Input.GestureEventArgs e)
			{

			if (rechargeAmount.Length > 0)
				{
				int recAmount = Convert.ToInt16 (rechargeAmount);
				if (recAmount > mainBalance || mainBalance == 0)
					{
					(Application.Current.RootVisual as PhoneApplicationFrame).Navigate (new Uri ("/ShopLocator.xaml?no-cache=" + Guid.NewGuid (), UriKind.Relative));

					}
				else
					{



					if (userRechargeNumber.Length != 0)
						{
						if (userRechargeNumber.Length == 10)
							{
							if (rechargeAmount.Length != 0)
								{

								if (recAmount >= min)
									{
									if (recAmount <= max)
										{
										if (flag.Equals ("1") || flag.Equals ("3") || flag.Equals ("4"))
											{


											ShowPopUp (false, flag, userRechargeNumber, recAmount, op_id, pay1_pro_id);


											}
										else if (flag.Equals ("2"))
											{

											ShowPopUp (false, flag, userRechargeNumber, recAmount, op_id, pay1_pro_id);

											}
										}
									else
										{
										MessageBox.Show ("maximum amount should be " + max);
										}
									}
								else
									{
									MessageBox.Show ("minimum amount should be " + min);
									}
								}
							else
								{
								MessageBox.Show ("Please enter amount");
								}
							}

						else
							{
							MessageBox.Show ("Please enter a valid number");
							}
						}
					else
						{
						MessageBox.Show ("Please enter number");
						}

					}
				}
			}
		/*private void textCCDC_Tap (object sender, System.Windows.Input.GestureEventArgs e)
			{
				//rechargeAmount = rechargeControl.textAmount.Text;

			if (string.IsNullOrEmpty (userRechargeNumber))
				{
				if(flag.Equals("2"))
				MessageBox.Show ("Please enter customer Id.");
				else
					MessageBox.Show ("Please enter Mobile number.");
				return;
				}
			if (string.IsNullOrEmpty (rechargeAmount))
				{
				MessageBox.Show ("Please enter amount.");
				return;
				}
			if (string.IsNullOrEmpty (op_id))
				{
				MessageBox.Show ("Please select operator.");
				return;
				}



			if (userRechargeNumber.Length != 0)
				{
				if (userRechargeNumber.Length == mobileNumberLength)
					{
					if (rechargeAmount.Length != 0)
						{
						int recAmount = Convert.ToInt32 (rechargeAmount);
						if (recAmount >= min)
							{
							if (recAmount <= max)
								{

								if (checkPrefix (userRechargeNumber))
									{
									ShowPopUp (rechargeViaWallet, flag, userRechargeNumber, recAmount, op_id, pay1_pro_id);
									}
								else
									{
									}


								}
							else
								{
								MessageBox.Show ("maximum amount should be " + max);
								}
							}
						else
							{
							MessageBox.Show ("minimum amount should be " + min);
							}
						}
					else
						{
						MessageBox.Show ("Please enter amount");
						}
					}

				else
					{
					if (!flag.Equals ("2"))
						{

						MessageBox.Show ("Please enter a valid number");
						}
					else
						{
						MessageBox.Show (DthOperatorDetails ());
						}
					}
				}
			else
				{
				MessageBox.Show ("Please enter number");
				}
	}			*/


		}
	}