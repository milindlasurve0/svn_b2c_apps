<?php $isLoggedIn = $this->session->userdata('isLoggedIn'); 
 $userData = getUserDetails();

// $user_name = isset($userData['name']) ? (($userData['name'] != null)?$userData['name']:$userData['mobile']):'Welcome';
 
 $user_name =  (isset($userData['name']) && ($userData['name'] != null))?  $userData['name']:(isset($userData['mobile']) ? $userData['mobile'] : "");

?>
<div class="header-section">
         <div class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
               <div class="navbar-header">
                  <button aria-expanded="false" data-target="#navigation" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  </button>
                  <a href="/" class="navbar-brand"><img alt="" src="/assets/images/icons/logo_pay1.png" ></a>
                  <div class="visible-sm visible-xs home">home</div>
               </div>

               <div id="navigation" class="collapse navbar-collapse pull-right">
                  <ul class="nav navbar-nav">
                      <li><a href="http://pay1.in/partners?ref=cGF5MS5pbg==" target="_blank" class="link-partner" style="cursor:pointer;border-radius:5px">Partner with us</a><li>    
                    <li style="margin-right:5px"><a  id="recharge_paybill" style="cursor:pointer;border-radius:5px" data-toggle="collapse" data-target="#rechargemenu">Recharge/Pay bills</a></li>
                    <li style="margin-right:5px"><a id="gift_store_button" href="/gift-store" style="margin-top: 6px; color: gray;border-radius:5px;display:<?php if($isLoggedIn){ echo "block"; }else{ echo "none"; } ?>">Gift Store</a></li>
                    <li><form method="post" id="redeem_giftcoin" action="/gift-store" onsubmit="return checkLogin(event)">
                            <input type="submit" Value="Redeem Gift Coins" 
                                style="margin-top: 6px; border-top-width: 5px;border-radius:5px;display:<?php if(!$isLoggedIn){ echo "block"; }else{ echo "none"; } ?>"   class="btn btn-save center">
                        </form></li>
                    <li class="dropdown glyp">
                     <a href="#" class="disabled dropdown-toggle orderinfo" style="color: gray;border-radius:5px; display:<?php if(!$isLoggedIn){ echo "block"; }else{ echo "none"; } ?>" data-toggle="dropdown" role="button" aria-haspopup="true" id="signin" aria-expanded="false">Sign up/Sign in</a>     
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="color: gray;border-radius:5px; padding-bottom: 0px; padding-top: 0px; display:<?php if(($isLoggedIn)){ echo "block"; }else{ echo "none"; } ?> " id="welcome" role="button" aria-expanded="false">
                         <img src="/assets/images/profile/user_img.jpg" style="height:35px;width:35px;z-index: 2;border-radius: 30px;"> <span id='wlecomeUsername'><?php echo isset($user_name)? $user_name : "" ?></span><span class="glyphicon glyphicon-chevron-down visible-md visible-lg " style="padding-top: 12px;"></span><span class="glyphicon glyphicon-chevron-right visible-sm visible-xs"></span></a>                    
                     <ul class="dropdown-menu">
                       <li><a id="profile" >View Profile</a></li>
                       <li><a id="logout">Logout</a></li>
                     </ul> 
                    </li>
                  </ul>
               </div>
            </div>
         </div>
      </div>  

    <script>
        
        function checkLogin(event){
            <?php if($isLoggedIn != 1 ){ ?>
//                  event.preventDefault();
//                  $('#basicModal').modal({
//                    show: true
//                });
                localStorage.setItem("redeem", "1");
            <?php } ?> 
        }
        
        $(document).ready(function(){
            $("#profile").click(function(){
                var url = b2cUrl+"/index.php/api_new/action/api/true/actiontype/get_userinfo/?";
                var html = "";
                $.ajax({
                    url: url,
                    type: "GET",
                    timeout: 50000,
                    dataType: "jsonp",
                    data:{
                            res_format : "jsonp"
                        },
                    
                    success: function(data) {
                        if (data.status == "success"){
                             b2c.core.setUserData(data);
                              window.location = "/about-me"
                        }
                        else if (data.status == "failure") {
                            window.location = "/home/logout/"
                        }
                    },
                    beforeSend: function() {
                        $('.loader').show();
                    },
                    complete: function() {
                        $('.loader').hide();
                    },
                    error: function(xhr, error) {
                    },
                });
            });
             
        });
        
    </script>