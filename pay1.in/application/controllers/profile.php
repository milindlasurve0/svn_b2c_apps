<?php

class Profile extends CI_Controller
{
	function __construct() {
        parent::__construct();
        $isLoggedIn = $this->session->userdata('isLoggedIn');
		if($isLoggedIn != 1 ){
		redirect('/home/');
		}
    }
	
    public function index()
    {
        $this->load->view('profile/index');
    }
	
        public function wallet(){
        $this->load->view('profile/wallet');
    }
    
    public function user_logout(){
        $this->session->sess_destroy();
    }
}
