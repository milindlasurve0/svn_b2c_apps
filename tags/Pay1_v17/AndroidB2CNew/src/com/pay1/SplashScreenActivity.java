package com.pay1;

import java.net.URLEncoder;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.pay1.constants.Constants;
import com.pay1.requesthandler.RequestClass;
import com.pay1.servicehandler.LoginService;
import com.pay1.utilities.Utility;

public class SplashScreenActivity extends Activity {

	//private ImageView imageView_Logo, imageView_Logo1;
	//private TextView textView_Slogen;
	private String code = "";

	@SuppressLint("NewApi")
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splashscreen_activity);
		try {
			// try {
			// startService(new Intent(SplashScreenActivity.this,
			// LoginService.class));
			// } catch (Exception e) {
			// }

			Typeface Reguler = Typeface.createFromAsset(getAssets(),
					"EncodeSansNormal-400-Regular.ttf");
		/*	textView_Slogen = (TextView) findViewById(R.id.textView_Slogen);
			textView_Slogen.setTypeface(Reguler);
*/
			/*imageView_Logo = (ImageView) findViewById(R.id.imageView_Logo);
			imageView_Logo.setVisibility(View.GONE);*/

			//imageView_Logo1 = (ImageView) findViewById(R.id.imageView_Logo1);
			// SVG svg = SVGParser.getSVGFromResource(getResources(),
			// R.raw.ic_pay1);
			// // Picture picture = svg.getPicture();
			// Drawable drawable = svg.createPictureDrawable();
			//
			// imageView_Logo1.setImageDrawable(drawable);
			// if (Build.VERSION.SDK_INT >= 11)
			// imageView_Logo1.setLayerType(View.LAYER_TYPE_SOFTWARE, null);

			// Intent intent = new Intent(SplashScreenActivity.this,
			// MainActivity.class);
			// startActivity(intent);
			// finish();
			if (Utility.getLoginFlag(SplashScreenActivity.this,
					Constants.SHAREDPREFERENCE_IS_LOGIN)) {

				// version = android.os.Build.VERSION.RELEASE;
				// manufacturer = android.os.Build.MANUFACTURER;

				new AutoLoginTask().execute(Utility.getMobileNumber(
						SplashScreenActivity.this,
						Constants.SHAREDPREFERENCE_MOBILE), Utility.getPin(
						SplashScreenActivity.this,
						Constants.SHAREDPREFERENCE_PIN), Utility.getGCMID(
						SplashScreenActivity.this,
						Constants.SHAREDPREFERENCE_GCMID), Utility.getUUID(
						SplashScreenActivity.this,
						Constants.SHAREDPREFERENCE_UUID), Utility
						.getCurrentLatitude(SplashScreenActivity.this,
								Constants.SHAREDPREFERENCE_CURRENT_LATITUDE),
						Utility.getCurrentLongitude(SplashScreenActivity.this,
								Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE));

			} else {
				Animation animation = AnimationUtils.loadAnimation(
						SplashScreenActivity.this, R.anim.translate);
				animation
						.setAnimationListener(new Animation.AnimationListener() {

							@Override
							public void onAnimationStart(Animation animation) {
								// TODO Auto-generated method stub

							}

							@Override
							public void onAnimationRepeat(Animation animation) {
								// TODO Auto-generated method stub

							}

							@Override
							public void onAnimationEnd(Animation animation) {
								// TODO Auto-generated method stub
								Intent intent = new Intent(
										SplashScreenActivity.this,
										MainActivity.class);
								startActivity(intent);
								finish();
							}
						});

				//imageView_Logo.startAnimation(animation);
			}

		} catch (Exception e) {
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// super.onBackPressed();
	}

	public class AutoLoginTask extends AsyncTask<String, String, String> {
		// private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			try {

				String response = RequestClass.getInstance()
						.readPay1B2CRequest(
								SplashScreenActivity.this,
								Constants.B2C_URL + "signin/?username="
										+ URLEncoder.encode(params[0], "utf-8")
										+ "&password="
										+ URLEncoder.encode(params[1], "utf-8")
										+ "&gcm_reg_id="
										+ URLEncoder.encode(params[2], "utf-8")
										+ "&uuid="
										+ URLEncoder.encode(params[3], "utf-8")
										+ "&latitude=" + params[4]
										+ "&longitude=" + params[5]);
				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			// if (this.dialog.isShowing()) {
			// this.dialog.dismiss();
			// }
			try {

				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONObject jsonObject = new JSONObject(replaced);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {
						Utility.setLoginFlag(SplashScreenActivity.this,
								Constants.SHAREDPREFERENCE_IS_LOGIN, true);
						JSONObject jsonObject2 = jsonObject
								.getJSONObject("description");

						Utility.setUserName(SplashScreenActivity.this,
								Constants.SHAREDPREFERENCE_NAME, jsonObject2
										.getString("name").trim());
						Utility.setMobileNumber(SplashScreenActivity.this,
								Constants.SHAREDPREFERENCE_MOBILE, jsonObject2
										.getString("mobile").trim());
						Utility.setEmail(SplashScreenActivity.this,
								Constants.SHAREDPREFERENCE_EMAIL, jsonObject2
										.getString("email").trim());
						Utility.setGender(SplashScreenActivity.this,
								Constants.SHAREDPREFERENCE_GENDER, jsonObject2
										.getString("gender").trim());
						Utility.setUserID(SplashScreenActivity.this,
								Constants.SHAREDPREFERENCE_USER_ID, jsonObject2
										.getString("user_id").trim());
						Utility.setBalance(SplashScreenActivity.this,
								Constants.SHAREDPREFERENCE_BALANCE, jsonObject2
										.getString("wallet_balance").trim());
						Utility.setPin(SplashScreenActivity.this,
								Constants.SHAREDPREFERENCE_PIN, Utility.getPin(
										SplashScreenActivity.this,
										Constants.SHAREDPREFERENCE_PIN));
						Utility.setDOB(SplashScreenActivity.this,
								Constants.SHAREDPREFERENCE_DOB, jsonObject2
										.getString("date_of_birth").trim());
						Utility.setLatitude(SplashScreenActivity.this,
								Constants.SHAREDPREFERENCE_LATITUDE,
								jsonObject2.getString("latitude").trim());
						Utility.setLongitude(SplashScreenActivity.this,
								Constants.SHAREDPREFERENCE_LONGITUDE,
								jsonObject2.getString("longitude").trim());
						Utility.setSupportNumber(SplashScreenActivity.this,
								Constants.SHAREDPREFERENCE_SUPPORT_NUMBER,
								jsonObject2.getString("support_number").trim());
						Utility.setCookieName(SplashScreenActivity.this,
								Constants.SHAREDPREFERENCE_COOKIE_NAME,
								jsonObject2.getString("session_name"));
						Utility.setCookieValue(SplashScreenActivity.this,
								Constants.SHAREDPREFERENCE_COOKIE_VALUE,
								jsonObject2.getString("session_id"));
						
						Utility.setSharedPreferences(SplashScreenActivity.this,
								"Deals_offer",
								jsonObject2.getString("Deals_offer"));
						
						
						Utility.setMyTotalGifts(SplashScreenActivity.this,
								jsonObject2.getString("total_gifts"));

						Utility.setMyLikes(SplashScreenActivity.this,
								jsonObject2.getString("total_mylikes"));

						Utility.setMyTotalRedeem(SplashScreenActivity.this,
								jsonObject2.getString("total_redeem"));

						Utility.setMyReviews(SplashScreenActivity.this,
								jsonObject2.getString("total_myreviews"));
						JSONObject jsonObjectTemplate = jsonObject2
								.getJSONObject("ErrorMsg");
						Utility.setTemplate(
								SplashScreenActivity.this,
								Constants.SHAREDPREFERENCE_SMS_BILLPAYMENT_FAILURE,
								jsonObjectTemplate
										.getString("BILLPAYMENT_FAILURE"));
						Utility.setTemplate(
								SplashScreenActivity.this,
								Constants.SHAREDPREFERENCE_SMS_RECHARGE_FAILURE,
								jsonObjectTemplate
										.getString("RECHARGE_FAILURE"));
						Utility.setTemplate(
								SplashScreenActivity.this,
								Constants.SHAREDPREFERENCE_SMS_WALLET_REFILLED_RETAILER,
								jsonObjectTemplate
										.getString("WALLET_REFILLED_RETAILER"));

						Utility.setSharedPreferences(
								SplashScreenActivity.this,
								Constants.SHAREDPREFERENCE_SERVICE_CHARGE_PERCENT,
								jsonObject2.getString("service_charge_percent")
										.trim());

						Utility.setSharedPreferences(SplashScreenActivity.this,
								Constants.SHAREDPREFERENCE_SERVICE_TAX_PERCENT,
								jsonObject2.getString("service_tax_percent")
										.trim());

					} else {
						code = jsonObject.getString("errCode");
					}

				} else {
					// Constants.showDialog(SplashScreenActivity.this, TAG,
					// Constants.ERROR_INTERNET, Constants.DIALOG_CLOSE);
				}
			} catch (JSONException e) {
				// e.printStackTrace();
				// Constants.showDialog(SplashScreenActivity.this, TAG,
				// Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
				// Constants.DIALOG_CLOSE);
			} catch (Exception e) {
				// TODO: handle exception
				// Constants.showDialog(SplashScreenActivity.this, TAG,
				// Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
				// Constants.DIALOG_CLOSE);
			}

			// if (Utility.isFirstTime(SplashScreenActivity.this)) {
			// Intent i = new Intent(SplashScreenActivity.this,
			// TourActivity.class);
			//
			// startActivity(i);
			// } else {

			// }

			if (code.equalsIgnoreCase("205")) {
				Utility.setLoginFlag(SplashScreenActivity.this,
						Constants.SHAREDPREFERENCE_IS_LOGIN, false);
				Intent intent = new Intent(SplashScreenActivity.this,
						LoginActivity.class);
				intent.putExtra("AUTO", true);
				startActivity(intent);
				finish();
			} else {
				Intent i = new Intent(SplashScreenActivity.this,
						MainActivity.class);
				i.putExtra(Constants.RECHARGE_FOR, Constants.RECHARGE_MOBILE);
				startActivity(i);
				finish();
			}
		}

		@Override
		protected void onPreExecute() {
			// dialog = new MyProgressDialog(SplashScreenActivity.this);
			// // this.dialog.setMessage("Please wait.....");
			// this.dialog.setCancelable(true);
			// this.dialog
			// .setOnCancelListener(new DialogInterface.OnCancelListener() {
			//
			// @Override
			// public void onCancel(DialogInterface dialog) {
			// // TODO Auto-generated method stub
			// AutoLoginTask.this.cancel(true);
			// }
			// });
			// this.dialog.show();
			super.onPreExecute();
		}

	}

}
