package com.pay1;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.SQLException;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.pay1.constants.Constants;
import com.pay1.customviews.MyProgressDialog;
import com.pay1.databasehandler.Operator;
import com.pay1.databasehandler.OperatorDataSource;
import com.pay1.databasehandler.Plan;
import com.pay1.databasehandler.PlanDataSource;
import com.pay1.requesthandler.RequestClass;
import com.pay1.utilities.EditTextValidator;
import com.pay1.utilities.Utility;

public class RechargeDthActivity extends AppBaseActivity implements
		OnClickListener {
	private static final String SCREEN_LABEL = "DTH Screen";
	int flag = 1;
	Button btnPlans;// , mBtnRchCancel, mBtnRchNow;
	// ImageView btnContacts;
	ImageView imageView_Ok, imageView_Cancel, imageView_dropdown;
	LinearLayout radioLayout;
	ToggleButton toggleSTV;
	EditText editRechargeSubID, editRechargeAmount;
	/*
	 * RadioGroup radioRchTypeGroup; RadioButton radioTopUp, radioSTV;
	 */
	static int PICK_CONTACT = 100;
	static int PICK_OPERATOR = 111;
	TextView textView_Details, textView_Title, textView_Operator, textViewMob,
			textViewSpinner, textViewAmount;

	String circle_code = null, operatorCode = null, operatorID = null,
			operatorName = null, operatorProductID = null;
	ProgressBar progressBar1;

	Bundle bundle;
	String isStv = "0";
	// PlanAdapterMobile plansAdapter;
	// ListView listPlan;

	private OperatorDataSource operatorDataSource;
	private PlanDataSource planDataSource;
	private boolean doubleBackToExitPressedOnce = false;

	private EasyTracker easyTracker = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.recharge_dth_layout);
		try {
			// Initialize GA
			// Pay1GA pay1GA = new Pay1GA(RechargeDthActivity.this);
			// Pay1GA.getGaTracker().set(Fields.SCREEN_NAME, SCREEN_LABEL);

			easyTracker = EasyTracker.getInstance(RechargeDthActivity.this);
			easyTracker.set(Fields.SCREEN_NAME, SCREEN_LABEL);

			findViewById();

			registerBaseActivityReceiver();

			operatorDataSource = new OperatorDataSource(
					RechargeDthActivity.this);

			try {
				operatorDataSource.open();

				List<Operator> operator = operatorDataSource.getAllOperator();
				if (operator.size() == 0)
					new GetOperatorTask().execute();

			} catch (SQLException exception) {
				// TODO: handle exception
				// // Log.e("Er ", exception.getMessage());
			} catch (Exception e) {
				// TODO: handle exception
				// // Log.e("Er ", e.getMessage());
			} finally {
				operatorDataSource.close();
			}

			bundle = getIntent().getExtras();
			flag = 1;
			imageView_Ok.setOnClickListener(this);
			imageView_Cancel.setOnClickListener(this);
			btnPlans.setOnClickListener(this);
			textView_Operator.setOnClickListener(this);
			imageView_dropdown.setOnClickListener(this);

			planDataSource = new PlanDataSource(RechargeDthActivity.this);
			// new GetPlanstask().execute();

			Typeface Normal = Typeface.createFromAsset(getAssets(),
					"EncodeSansNormal-100-Thin.ttf");
			Typeface Reguler = Typeface.createFromAsset(getAssets(),
					"EncodeSansNormal-400-Regular.ttf");
			Typeface Medium = Typeface.createFromAsset(getAssets(),
					"EncodeSansNarrow-500-Medium.ttf");

			textView_Details.setTypeface(Normal);
			textView_Title.setTypeface(Reguler);
			textViewAmount.setTypeface(Medium);
			textViewMob.setTypeface(Medium);
			textViewSpinner.setTypeface(Medium);
			btnPlans.setTypeface(Medium);
			editRechargeAmount.setTypeface(Normal);
			editRechargeSubID.setTypeface(Normal);
			textView_Operator.setTypeface(Normal);

			editRechargeSubID.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					// TODO Auto-generated method stub
					if (editRechargeSubID.getText().toString().trim().length() > 0)
						EditTextValidator.hasText(RechargeDthActivity.this,
								editRechargeSubID,
								Constants.ERROR_SUB_ID_BLANK_FIELD);
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					// TODO Auto-generated method stub

				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

				}
			});

			editRechargeSubID
					.setOnEditorActionListener(new TextView.OnEditorActionListener() {

						@Override
						public boolean onEditorAction(TextView v, int actionId,
								KeyEvent event) {
							// TODO Auto-generated method stub
							if (actionId == EditorInfo.IME_ACTION_NEXT) {
								Intent intent = new Intent(
										RechargeDthActivity.this,
										OperatorActivity.class);
								intent.putExtra(Constants.RECHARGE_FOR,
										Constants.RECHARGE_DTH);
								startActivityForResult(intent, PICK_OPERATOR);

								return true;
							}
							return false;
						}
					});

			textView_Operator.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					// TODO Auto-generated method stub
					if (textView_Operator.getText().toString().trim().length() > 0)
						if (EditTextValidator.hasText(RechargeDthActivity.this,
								textView_Operator,
								Constants.ERROR_OPERATOR_BLANK_FIELD))
							imageView_dropdown.setVisibility(View.VISIBLE);
						else
							imageView_dropdown.setVisibility(View.GONE);
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					// TODO Auto-generated method stub

				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

				}
			});

			editRechargeAmount.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					// TODO Auto-generated method stub
					if (editRechargeAmount.getText().toString().trim().length() > 0)
						if (EditTextValidator.hasText(RechargeDthActivity.this,
								editRechargeAmount,
								Constants.ERROR_AMOUNT_BLANK_FIELD))
							btnPlans.setVisibility(View.VISIBLE);
						else
							btnPlans.setVisibility(View.GONE);
					if (editRechargeAmount.getText().toString().trim().length() > 2) {
						// if (circle_code != null) {
						progressBar1.setVisibility(View.VISIBLE);
						// Toast.makeText(RechargeMobileActivity.this, pay1OpId,
						// Toast.LENGTH_SHORT).show();
						getPlan(String.valueOf(operatorProductID),
								circle_code,
								Integer.parseInt(editRechargeAmount.getText()
										.toString().trim()));
						// }
					}

				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					// TODO Auto-generated method stub

				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

				}
			});
		} catch (Exception e) {
		}

		overridePendingTransition(R.anim.push_up_in, R.anim.fadeout);
	}

	private void findViewById() {
		// TODO Auto-generated method stub
		btnPlans = (Button) findViewById(R.id.btnPlans);
		btnPlans.setVisibility(View.GONE);
		textView_Operator = (TextView) findViewById(R.id.textView_Operator);
		textView_Details = (TextView) findViewById(R.id.textView_Details);
		textViewMob = (TextView) findViewById(R.id.textViewMob);
		textViewSpinner = (TextView) findViewById(R.id.textViewSpinner);
		textViewAmount = (TextView) findViewById(R.id.textViewAmount);
		textView_Title = (TextView) findViewById(R.id.textView_Title);
		editRechargeSubID = (EditText) findViewById(R.id.editDthRechargeSubId);
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(editRechargeSubID.getWindowToken(), 0);
		editRechargeAmount = (EditText) findViewById(R.id.editDthRechargeAmount);
		imageView_Ok = (ImageView) findViewById(R.id.imageView_Ok);

		imageView_Cancel = (ImageView) findViewById(R.id.imageView_Cancel);
		imageView_dropdown = (ImageView) findViewById(R.id.imageView_dropdown);
		// listPlan = (ListView) findViewById(R.id.listPlan);
		progressBar1 = (ProgressBar) findViewById(R.id.progressBar1);
		progressBar1.setVisibility(View.GONE);
	}

	private boolean checkValidation(Context context) {
		boolean ret = true;

		if (!EditTextValidator.hasText(context, editRechargeSubID,
				Constants.ERROR_SUB_ID_BLANK_FIELD)) {
			ret = false;
			editRechargeSubID.requestFocus();
			return ret;
		} else if (!EditTextValidator.hasText(context, textView_Operator,
				Constants.ERROR_OPERATOR_BLANK_FIELD)) {
			ret = false;
			textView_Operator.requestFocus();
			imageView_dropdown.setVisibility(View.GONE);
			return ret;
		} else if (!EditTextValidator.hasText(context, editRechargeAmount,
				Constants.ERROR_AMOUNT_BLANK_FIELD)) {
			ret = false;
			editRechargeAmount.requestFocus();
			btnPlans.setVisibility(View.GONE);
			return ret;
		} else if (!EditTextValidator.isValidAmount(context,
				editRechargeAmount, Constants.ERROR_AMOUNT_VALID_FIELD)) {
			ret = false;
			editRechargeAmount.requestFocus();
			btnPlans.setVisibility(View.GONE);
			return ret;
		} else
			return ret;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		try {
			if (resultCode == Activity.RESULT_OK) {
				if (requestCode == Constants.PLAN_REQUEST) {
					editRechargeAmount.setText(data.getExtras().getString(
							"result"));
				}
				if (requestCode == PICK_OPERATOR) {
					textView_Operator.setText(data.getExtras().getString(
							"OperatorName"));
					btnPlans.setVisibility(View.VISIBLE);
					operatorCode = data.getExtras().getString("OperatorCode");
					operatorID = data.getExtras().getString("OperatorID");
					operatorProductID = data.getExtras().getString(
							"OperatorProductID");
				}
				if (requestCode == PICK_CONTACT) {

				}
			}
		} catch (Exception e) {
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.textView_Operator: {
			Intent intent = new Intent(RechargeDthActivity.this,
					OperatorActivity.class);
			intent.putExtra(Constants.RECHARGE_FOR, Constants.RECHARGE_DTH);
			startActivityForResult(intent, PICK_OPERATOR);
		}
			break;
		case R.id.imageView_dropdown: {
			Intent intent = new Intent(RechargeDthActivity.this,
					OperatorActivity.class);
			intent.putExtra(Constants.RECHARGE_FOR, Constants.RECHARGE_DTH);
			startActivityForResult(intent, PICK_OPERATOR);
		}
			break;
		case R.id.btnContacts: {
			Intent intent = new Intent(Intent.ACTION_PICK,
					ContactsContract.Contacts.CONTENT_URI);
			startActivityForResult(intent, PICK_CONTACT);
		}
			break;
		case R.id.imageView_Cancel:
			editRechargeAmount.setText("");
			editRechargeSubID.setText("");
			editRechargeSubID.requestFocus();
			textView_Operator.setText("");
			btnPlans.setVisibility(View.GONE);
			textView_Details.setVisibility(View.GONE);
			progressBar1.setVisibility(View.GONE);
			break;
		case R.id.imageView_Ok: {
			if (Utility.getLoginFlag(RechargeDthActivity.this,
					Constants.SHAREDPREFERENCE_IS_LOGIN)) {

				if (!checkValidation(this)) {

				} else {

					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

					String charges_slab = null;
					double service_charge_amount = 0, service_charge_percent = 0, service_tax_percent = 0, min = 0, max = 0;
					try {
						operatorDataSource.open();

						List<Operator> operators = operatorDataSource
								.getAllOperator(Constants.RECHARGE_DTH,
										operatorCode);

						if (operators.size() != 0) {
							charges_slab = operators.get(0)
									.getOperatorChargeSlab();
							service_charge_amount = operators.get(0)
									.getOperatorServiceChargeAmount();
							service_charge_percent = operators.get(0)
									.getOperatorServiceChargePercent();
							service_tax_percent = operators.get(0)
									.getOperatorServiceTaxPercent();
							min = operators.get(0).getOperatorMinAmount();
							max = operators.get(0).getOperatorMaxAmount();
						}

					} catch (SQLException exception) {
						// TODO: handle exception
						// // Log.e("Er ", exception.getMessage());
					} catch (Exception e) {
						// TODO: handle exception
						// // Log.e("Er ", e.getMessage());
					} finally {
						operatorDataSource.close();
					}

					double rechargeAmount = 0, balance = 0;
					try {
						rechargeAmount = Double.parseDouble(editRechargeAmount
								.getText().toString().trim());
						balance = Double.parseDouble(Utility.getBalance(
								RechargeDthActivity.this,
								Constants.SHAREDPREFERENCE_BALANCE));
					} catch (Exception e) {
						rechargeAmount = 0;
						balance = 0;
					}
					if (rechargeAmount < min || rechargeAmount > max) {
						EditTextValidator.isValidAmount(
								RechargeDthActivity.this, editRechargeAmount,
								Constants.ERROR_AMOUNT_RANG_FIELD + min
										+ " to " + max, min, max);
						editRechargeAmount.requestFocus();
						return;
					}
					int slab_amount = 0;
					try {
						if (charges_slab.equalsIgnoreCase("null")
								|| charges_slab.equalsIgnoreCase("")) {
							rechargeAmount = (rechargeAmount + (rechargeAmount * service_charge_percent));
							rechargeAmount = (rechargeAmount + (rechargeAmount * service_tax_percent));
						} else {
							String[] comma_seperator = charges_slab.split(",");
							for (String s : comma_seperator) {
								String[] colon_sperator = s.split(":");
								if (rechargeAmount > Double
										.parseDouble(colon_sperator[0])) {
									slab_amount = Integer
											.parseInt(colon_sperator[1]);
								}
							}
						}
					} catch (Exception e) {
					}
					if ((rechargeAmount + slab_amount) > balance) {
						Intent orderIntent = new Intent(
								RechargeDthActivity.this,
								OrderSummaryForLowBalActivity.class);
						orderIntent.putExtra(Constants.OPERATOR_NAME,
								textView_Operator.getText().toString());
						orderIntent.putExtra(Constants.MOBILE_NUMBER,
								editRechargeSubID.getText().toString().trim());
						orderIntent.putExtra(Constants.CONTACT_NAME, "");
						orderIntent.putExtra(Constants.RECHARGE_AMOUNT,
								editRechargeAmount.getText().toString().trim());
						orderIntent
								.putExtra(Constants.CHARGE_SLAB, slab_amount);
						orderIntent.putExtra(Constants.OPERATOR_ID, operatorID);
						orderIntent.putExtra(Constants.SUBSCRIBER_ID,
								editRechargeSubID.getText().toString().trim());
						orderIntent.putExtra(Constants.RECHARGE_FOR,
								bundle.getInt(Constants.RECHARGE_FOR));
						startActivity(orderIntent);

					} else {
						Intent orderIntent = new Intent(
								RechargeDthActivity.this,
								OrderSummaryActivity.class);
						orderIntent.putExtra(Constants.OPERATOR_NAME,
								textView_Operator.getText().toString());
						orderIntent.putExtra(Constants.MOBILE_NUMBER,
								editRechargeSubID.getText().toString().trim());
						orderIntent.putExtra(Constants.CONTACT_NAME, "");
						orderIntent.putExtra(Constants.RECHARGE_AMOUNT,
								editRechargeAmount.getText().toString().trim());
						orderIntent
								.putExtra(Constants.CHARGE_SLAB, slab_amount);
						orderIntent.putExtra(Constants.OPERATOR_ID, operatorID);
						orderIntent.putExtra(Constants.SUBSCRIBER_ID,
								editRechargeSubID.getText().toString().trim());
						orderIntent.putExtra(Constants.RECHARGE_FOR,
								bundle.getInt(Constants.RECHARGE_FOR));
						startActivity(orderIntent);
					}
				}
			} else {
				// Constants.showOneButtonDialog(RechargeMobileActivity.this,
				// "Recharge ", "Please Login First.",
				// Constants.DIALOG_CLOSE_LOGIN);
				Intent intent = new Intent(RechargeDthActivity.this,
						LoginForRechargeActivity.class);
				startActivity(intent);
			}
		}
			break;
		case R.id.btnPlans:

			Intent plansIntent = new Intent(RechargeDthActivity.this,
					PlanFragmentActivity.class);

			plansIntent
					.putExtra(Constants.RECHARGE_FOR, Constants.RECHARGE_DTH);
			plansIntent.putExtra(Constants.PLAN_JSON, operatorProductID);
			plansIntent.putExtra("CIRCLE_CODE", "all");
			plansIntent.putExtra("DATA_CARD_PLAN", 0);

			startActivityForResult(plansIntent, Constants.PLAN_REQUEST);

			break;
		default:
			break;
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView().build());
		// EasyTracker.getInstance(this).activityStart(this);
		easyTracker.send(MapBuilder.createAppView().build());
	}

	@Override
	public void onStop() {
		super.onStop();
		// EasyTracker.getInstance(this).activityStop(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (Constants.isExpanded) {
			Constants.collapsView(this);
		} else {
			Constants.SELECTED_MENU_INDEX = Constants.RECHARGE_MENU_INDEX;
			Constants.showNavigationBar(RechargeDthActivity.this);
			// overridePendingTransition(R.anim.translate_activity_down,
			// R.anim.translate_activity_up);
			flag = 1;
		}
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unRegisterBaseActivityReceiver();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// if (Constants.isExpanded) {
		// Constants.collapsView(this);
		// } else {
		// super.onBackPressed();
		// overridePendingTransition(R.anim.fadeout, R.anim.push_down_out);
		// }
		if (Constants.isExpanded) {
			Constants.collapsView(this);
		} else {
			if (doubleBackToExitPressedOnce) {
				super.onBackPressed();
				overridePendingTransition(R.anim.fadeout, R.anim.push_down_out);
				closeAllActivitiesInApp();
				return;
			}

			this.doubleBackToExitPressedOnce = true;
			Toast.makeText(this, "Press again to exit.", Toast.LENGTH_SHORT)
					.show();

			new Handler().postDelayed(new Runnable() {

				@Override
				public void run() {
					doubleBackToExitPressedOnce = false;
				}
			}, 10000);
		}
		// if (Constants.isExpanded) {
		// if (doubleBackToExitPressedOnce) {
		// super.onBackPressed();
		// overridePendingTransition(R.anim.fadeout, R.anim.push_down_out);
		// } else {
		// Constants.collapsView(this);
		// }
		// } else if (!Constants.isExpanded) {
		// if (doubleBackToExitPressedOnce) {
		// super.onBackPressed();
		// overridePendingTransition(R.anim.fadeout, R.anim.push_down_out);
		// } else {
		// DisplayMetrics metrics = new DisplayMetrics();
		// getWindowManager().getDefaultDisplay().getMetrics(metrics);
		// int panelWidth = (int) ((metrics.widthPixels) * 0.80);
		// int panelHeight = (int) ((metrics.heightPixels) * 0.80);
		// Constants.expandView(RechargeDthActivity.this, panelWidth,
		// panelHeight);
		// }
		// }
		// if (!doubleBackToExitPressedOnce) {
		// this.doubleBackToExitPressedOnce = true;
		// Toast.makeText(this, "Press again to exit.",
		// Toast.LENGTH_SHORT).show();
		// }
		//
		// new Handler().postDelayed(new Runnable() {
		//
		// @Override
		// public void run() {
		// doubleBackToExitPressedOnce = false;
		// }
		// }, 2000);
		// return;
	}

	protected void getPlan(String operator, String circle, int amount) {

		try {
			planDataSource.open();

			final List<Plan> plans = planDataSource.getPlanDetails(operator,
					"all", amount, 0);
			List<Plan> data = planDataSource.getAllPlan(operator, circle);
			// plansAdapter = new PlanAdapterMobile(RechargeDthActivity.this, 1,
			// plans);
			// listPlan.setAdapter(plansAdapter);
			// listPlan.setOnItemClickListener(new OnItemClickListener() {
			//
			// @Override
			// public void onItemClick(AdapterView<?> arg0, View arg1,
			// int position, long arg3) {
			// // TODO Auto-generated method stub
			// int amount = plans.get(position).getPlanAmount();
			// editRechargeAmount.setText(amount + "");
			// textView_Details.setVisibility(View.VISIBLE);
			// textView_Details.setText(plans.get(position)
			// .getPlanDescription());
			// }
			// });
			if (plans.size() == 0 && data.size() == 0) {
				new GetPlanstask().execute();
			} else {
				if (plans.size() != 0) {
					String LastUpdateTime = plans.get(0).getPlanUptateTime();
					int days = (int) ((System.currentTimeMillis() - Long
							.parseLong(LastUpdateTime)) / (1000 * 60 * 60 * 24));

					if (days >= 1) {
						planDataSource.deletePlan();
						new GetPlanstask().execute();
					} else {
						textView_Details.setText(plans.get(0)
								.getPlanDescription());
						textView_Details.setVisibility(View.VISIBLE);
						progressBar1.setVisibility(View.GONE);
						textView_Details.setTextColor(getResources().getColor(
								R.color.Black));
					}
				} else {
					textView_Details
							.setText("No such plan exist in our system. Kindly check with operator.");
					textView_Details.setVisibility(View.VISIBLE);
					progressBar1.setVisibility(View.GONE);
					textView_Details.setTextColor(getResources().getColor(
							R.color.Red));
				}
			}

		} catch (SQLException exception) {

			// TODO: handle exception
			// // Log.e("Er ", exception.getMessage());
		} catch (Exception e) {
			// TODO: handle exception
			// // Log.e("Er ", e.getMessage());
		} finally {
			planDataSource.close();
			// progressBar1.setVisibility(View.GONE);
		}

	}

	public class GetPlanstask extends AsyncTask<String, String, String> {
		// MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

			listValuePair
					.add(new BasicNameValuePair("method", "getPlanDetails"));
			listValuePair.add(new BasicNameValuePair("operator", operatorCode));
			listValuePair.add(new BasicNameValuePair("circle", circle_code));

			String response = RequestClass.getInstance().readPay1B2BRequest(
					RechargeDthActivity.this, Constants.B2B_URL, listValuePair);
			String replaced = response.replace("(", "").replace(")", "")
					.replace(";", "");
			savePlansTodataBase(replaced);

			return replaced;

		}

		@Override
		protected void onPostExecute(String result) {
			// if (this.dialog.isShowing()) {
			// this.dialog.dismiss();
			// }

			super.onPostExecute(result);
			try {
				if (result != null && !result.startsWith("Error") && flag != 0) {
					flag--;
					getPlan(operatorCode,
							circle_code,
							Integer.parseInt(editRechargeAmount.getText()
									.toString().trim()));
				} else {
					textView_Details
							.setText("No such plan exist in our system. Kindly check with operator.");
					textView_Details.setVisibility(View.VISIBLE);
					textView_Details.setTextColor(getResources().getColor(
							R.color.Red));
				}
			} catch (Exception e) {
				// TODO: handle exception
				// Constants.showCustomToast(RechargeMobileActivity.this,
				// result);
			}

			progressBar1.setVisibility(View.GONE);
		}

		@Override
		protected void onPreExecute() {
			// dialog = new MyProgressDialog(RechargeActivity.this);
			// // this.dialog.setMessage("Please wait.....");
			// this.dialog.show();
			progressBar1.setVisibility(View.VISIBLE);
			super.onPreExecute();

		}

	}

	private void savePlansTodataBase(String planJson) {
		try {
			planDataSource.open();
			// ContentValues contentValues = new ContentValues();
			JSONArray array = new JSONArray(planJson);
			JSONObject planJsonObject = array.getJSONObject(0);

			Iterator<String> operatorKey = planJsonObject.keys();

			if (operatorKey.hasNext()) {

				long time = System.currentTimeMillis();

				JSONObject jsonObject2 = planJsonObject
						.getJSONObject(operatorKey.next());

				String opr_name = jsonObject2.getString("opr_name");
				String prod_code_pay1 = jsonObject2.getString("prod_code_pay1");
				// contentValues.put("operator_id", prod_code_pay1);
				// contentValues.put("operator_name", opr_name);
				// contentValues.put("prod_code_pay1", prod_code_pay1);
				JSONObject circleJsonObject = jsonObject2
						.getJSONObject("circles");
				Iterator<String> circleKey = circleJsonObject.keys();
				if (circleKey.hasNext()) {
					JSONObject circleJsonObject2 = circleJsonObject
							.getJSONObject(circleKey.next().toString());
					String circleName = circleJsonObject2
							.getString("circle_name");

					// contentValues.put("circle_name", circleName);

					JSONObject plansJsonObject = circleJsonObject2
							.getJSONObject("plans");
					String circle_id = circleJsonObject2.getString("circle_id");
					Iterator<String> planKey = plansJsonObject.keys();
					Iterator<String> planKeyDB = plansJsonObject.keys();
					// contentValues.put("circle_id", circle_id);

					while (planKey.hasNext()) {
						String planType = planKey.next().toString();
						JSONArray planArray = plansJsonObject
								.getJSONArray(planType);
						// contentValues.put("planType", planType);

						for (int i = 0; i < planArray.length(); i++) {
							JSONObject jsonObject = planArray.getJSONObject(i);
							String plan_desc = jsonObject
									.getString("plan_desc");
							// contentValues.put("plan_desc", plan_desc);
							String plan_amt = jsonObject.getString("plan_amt");
							// contentValues.put("plan_amt", plan_amt);
							String plan_validity = jsonObject
									.getString("plan_validity");
							// contentValues.put("plan_validity",
							// plan_validity);
							// // Log.e("cORRECT", "cORRECT   " + i + "  "
							// + jsonObject.toString());
							// controller.insertPlans(contentValues);
							planDataSource.createPlan(prod_code_pay1, opr_name,
									circle_id, circleName, planType, plan_amt,
									plan_validity, plan_desc, "" + time);
						}

					}

				}

			}
		} catch (JSONException exception) {

		} catch (SQLException exception) {
			// TODO: handle exception
			// // Log.e("Er ", exception.getMessage());
		} catch (Exception e) {
			// TODO: handle exception
			// // Log.e("Er ", e.getMessage());
		} finally {
			planDataSource.close();
		}
	}

	public class GetOperatorTask extends AsyncTask<String, String, String> {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			try {
				String response = RequestClass.getInstance()
						.readPay1B2CRequest(RechargeDthActivity.this,
								Constants.B2C_URL + "get_all_operators/?");
				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}

			try {

				long timestamp = System.currentTimeMillis();
				operatorDataSource.open();

				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");
					JSONObject jsonObject = new JSONObject(replaced);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {
						JSONObject description = jsonObject
								.getJSONObject("description");
						JSONArray mobile = description.getJSONArray("mobile");
						JSONArray data = description.getJSONArray("data");
						JSONArray postpaid = description
								.getJSONArray("postpaid");
						JSONArray dth = description.getJSONArray("dth");

						for (int i = 0; i < mobile.length(); i++) {
							JSONObject object = mobile.getJSONObject(i);

							operatorDataSource
									.createOperator(
											Integer.parseInt(object.getString(
													"id").trim()),
											object.getString("opr_code").trim(),
											Integer.parseInt(object.getString(
													"product_id").trim()),
											object.getString("name").trim(),
											Integer.parseInt(object.getString(
													"service_id").trim()),
											Integer.parseInt(object.getString(
													"flag").trim()),
											Integer.parseInt(object.getString(
													"delegate").trim()),
											Integer.parseInt(object.getString(
													"stv").trim()),
											Constants.RECHARGE_MOBILE,
											timestamp,
											Integer.parseInt(object
													.getString("max")),
											Integer.parseInt(object
													.getString("min")),
											Integer.parseInt(object
													.getString("service_charge_amount")),
											Integer.parseInt(object
													.getString("service_tax_percent")),
											Integer.parseInt(object
													.getString("service_charge_percent")),
											object.getString("charges_slab")
													.toString());

						}
						for (int i = 0; i < data.length(); i++) {
							JSONObject object = data.getJSONObject(i);

							operatorDataSource
									.createOperator(
											Integer.parseInt(object.getString(
													"id").trim()),
											object.getString("opr_code").trim(),
											Integer.parseInt(object.getString(
													"product_id").trim()),
											object.getString("name").trim(),
											Integer.parseInt(object.getString(
													"service_id").trim()),
											Integer.parseInt(object.getString(
													"flag").trim()),
											Integer.parseInt(object.getString(
													"delegate").trim()),
											Integer.parseInt(object.getString(
													"stv").trim()),
											Constants.RECHARGE_DATA,
											timestamp,
											Integer.parseInt(object
													.getString("max")),
											Integer.parseInt(object
													.getString("min")),
											Integer.parseInt(object
													.getString("service_charge_amount")),
											Integer.parseInt(object
													.getString("service_tax_percent")),
											Integer.parseInt(object
													.getString("service_charge_percent")),
											object.getString("charges_slab")
													.toString());
						}
						for (int i = 0; i < postpaid.length(); i++) {
							JSONObject object = postpaid.getJSONObject(i);

							operatorDataSource
									.createOperator(
											Integer.parseInt(object.getString(
													"id").trim()),
											object.getString("opr_code").trim(),
											Integer.parseInt(object.getString(
													"product_id").trim()),
											object.getString("name").trim(),
											Integer.parseInt(object.getString(
													"service_id").trim()),
											Integer.parseInt(object.getString(
													"flag").trim()),
											Integer.parseInt(object.getString(
													"delegate").trim()),
											Integer.parseInt(object.getString(
													"stv").trim()),
											Constants.BILL_PAYMENT,
											timestamp,
											Integer.parseInt(object
													.getString("max")),
											Integer.parseInt(object
													.getString("min")),
											Integer.parseInt(object
													.getString("service_charge_amount")),
											Integer.parseInt(object
													.getString("service_tax_percent")),
											Integer.parseInt(object
													.getString("service_charge_percent")),
											object.getString("charges_slab")
													.toString());
						}
						for (int i = 0; i < dth.length(); i++) {

							JSONObject object = dth.getJSONObject(i);

							operatorDataSource
									.createOperator(
											Integer.parseInt(object.getString(
													"id").trim()),
											object.getString("opr_code").trim(),
											Integer.parseInt(object.getString(
													"product_id").trim()),
											object.getString("name").trim(),
											Integer.parseInt(object.getString(
													"service_id").trim()),
											Integer.parseInt(object.getString(
													"flag").trim()),
											Integer.parseInt(object.getString(
													"delegate").trim()),
											Integer.parseInt(object.getString(
													"stv").trim()),
											Constants.RECHARGE_DTH,
											timestamp,
											Integer.parseInt(object
													.getString("max")),
											Integer.parseInt(object
													.getString("min")),
											Integer.parseInt(object
													.getString("service_charge_amount")),
											Integer.parseInt(object
													.getString("service_tax_percent")),
											Integer.parseInt(object
													.getString("service_charge_percent")),
											object.getString("charges_slab")
													.toString());
						}

					}
				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonDialog(RechargeDthActivity.this,
						"Operator", Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			} catch (SQLException exception) {
				// TODO: handle exception
				// // Log.e("Er ", exception.getMessage());
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonDialog(RechargeDthActivity.this,
						"Operator", Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			} finally {
				operatorDataSource.close();
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(RechargeDthActivity.this);
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(false);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							GetOperatorTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}
	}
}
