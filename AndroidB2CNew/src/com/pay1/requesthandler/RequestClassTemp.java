package com.pay1.requesthandler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.UnknownHostException;
import java.security.KeyStore;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

import android.content.Context;

import com.pay1.constants.Constants;
import com.pay1.utilities.Utility;

public class RequestClassTemp {

	// DefaultHttpClient
	HttpClient httpClient;
	HttpPost httpPost;
	HttpResponse httpResponse;
	StatusLine statusLine;
	HttpEntity entity;
	InputStream content;
	String result = null;
	List<Cookie> cookies = null;
	CookieStore cookieStore;

	private static RequestClassTemp instance = null;

	public RequestClassTemp() {
		try {
			// HttpParams httpParameters = new BasicHttpParams();
			// // Set the timeout in milliseconds until a connection is
			// // established.
			// int timeoutConnection = 30000;
			// HttpConnectionParams.setConnectionTimeout(httpParameters,
			// timeoutConnection);
			// // Set the default socket timeout (SO_TIMEOUT)
			// // in milliseconds which is the timeout for waiting for data.
			// int timeoutSocket = 30000;
			// HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
			//
			// httpClient = new DefaultHttpClient(httpParameters);

			HttpParams httpParameters = new BasicHttpParams();
			// Set the timeout in milliseconds until a connection is
			// established.
			int timeoutConnection = 60000;
			HttpConnectionParams.setConnectionTimeout(httpParameters,
					timeoutConnection);
			// Set the default socket timeout (SO_TIMEOUT)
			// in milliseconds which is the timeout for waiting for data.
			int timeoutSocket = 60000;
			HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
			KeyStore trustStore = KeyStore.getInstance(KeyStore
					.getDefaultType());
			trustStore.load(null, null);

			SSLSocketFactory sf = new MySSLSocketFactory(trustStore);
			sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

			SchemeRegistry registry = new SchemeRegistry();
			registry.register(new Scheme("http", PlainSocketFactory
					.getSocketFactory(), 80));
			registry.register(new Scheme("https", sf, 443));

			DefaultHttpClient client = new DefaultHttpClient(httpParameters);
			// ClientConnectionManager mgr = client.getConnectionManager();
			HttpParams params = client.getParams();
			httpClient = new DefaultHttpClient(new ThreadSafeClientConnManager(
					params, registry), params);

		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		
		
	}

	public static RequestClassTemp getInstance() {
		if (instance == null) {
			instance = new RequestClassTemp();
		}
		return instance;
	}

	public synchronized String readPay1B2CRequest(Context context, String url,
			List<NameValuePair> nameValuePairs) {

		if (Constants.isOnline(context)) {

			BufferedReader reader = null;

			try {
				if (Utility.getCookieValue(context,
						Constants.SHAREDPREFERENCE_COOKIE_VALUE) == null) {
					cookieStore = new BasicCookieStore();

				} else {
					cookieStore = new BasicCookieStore();

					BasicClientCookie cookie = new BasicClientCookie(
							Utility.getCookieName(context,
									Constants.SHAREDPREFERENCE_COOKIE_NAME),
							Utility.getCookieValue(context,
									Constants.SHAREDPREFERENCE_COOKIE_VALUE));
					cookie.setVersion(Utility.getCookieVersion(context,
							Constants.SHAREDPREFERENCE_COOKIE_VERSION));
					cookie.setDomain(Utility.getCookieDomain(context,
							Constants.SHAREDPREFERENCE_COOKIE_DOMAIN));
					cookie.setPath(Utility.getCookiePath(context,
							Constants.SHAREDPREFERENCE_COOKIE_PATH));
					// Date expiryDate = new
					// Date(Utility.getCookieExpiry(context,
					// Constants.SHAREDPREFERENCE_COOKIE_EXPIRY));
					// cookie.setExpiryDate(expiryDate);

					cookieStore.addCookie(cookie);
				}
				HttpContext ctx = new BasicHttpContext();
				ctx.setAttribute(ClientContext.COOKIE_STORE, cookieStore);
				StringBuilder builder = new StringBuilder();
				// // // Log.v("URL", URL + part);
				httpPost = new HttpPost(url);
				httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				httpResponse = httpClient.execute(httpPost, ctx);
				statusLine = httpResponse.getStatusLine();
				int statusCode = statusLine.getStatusCode();
				if (statusCode == 200) {
					// if (Utility.getCookieValue(context,
					// Constants.SHAREDPREFERENCE_COOKIE_VALUE) == null) {
					cookies = cookieStore.getCookies();
					for (Cookie a : cookies) {
						// cookieStore.addCookie(a);
						// Utility.setCookieVersion(context,
						// Constants.SHAREDPREFERENCE_COOKIE_VERSION,
						// a.getVersion());
						// Utility.setCookieName(context,
						// Constants.SHAREDPREFERENCE_COOKIE_NAME,
						// a.getName());
						// Utility.setCookieValue(context,
						// Constants.SHAREDPREFERENCE_COOKIE_VALUE,
						// a.getValue());
						// Utility.setCookieDomain(context,
						// Constants.SHAREDPREFERENCE_COOKIE_DOMAIN,
						// a.getDomain());
						// Utility.setCookiePath(context,
						// Constants.SHAREDPREFERENCE_COOKIE_PATH,
						// a.getPath());
						// break;

						if (a.getName().trim().equalsIgnoreCase("PHPSESSID")) {
							cookieStore.addCookie(a);
							Utility.setCookieVersion(context,
									Constants.SHAREDPREFERENCE_COOKIE_VERSION,
									a.getVersion());
							Utility.setCookieName(context,
									Constants.SHAREDPREFERENCE_COOKIE_NAME, a
											.getName());
							Utility.setCookieValue(context,
									Constants.SHAREDPREFERENCE_COOKIE_VALUE, a
											.getValue());
							Utility.setCookieDomain(context,
									Constants.SHAREDPREFERENCE_COOKIE_DOMAIN, a
											.getDomain());
							Utility.setCookiePath(context,
									Constants.SHAREDPREFERENCE_COOKIE_PATH, a
											.getPath());
							break;
						}
						// Utility.setCookieExpiry(context,
						// Constants.SHAREDPREFERENCE_COOKIE_EXPIRY,
						// String.valueOf(a.getExpiryDate()));
						// // System.out.println("- " +
						// a.getValue().toString());
					}
					// }
					entity = httpResponse.getEntity();
					content = entity.getContent();
					reader = new BufferedReader(new InputStreamReader(content));
					String line;
					while ((line = reader.readLine()) != null) {
						builder.append(line);
					}
					result = (builder.toString().trim() != null || builder
							.toString().trim() != "") ? builder.toString()
							.trim() : Constants.ERROR_SERVER;
				} else {
					// // // Log.e(RequestClass.class.toString(),
					// "Failed to connect");
					entity = httpResponse.getEntity();
					content = entity.getContent();
					reader = new BufferedReader(new InputStreamReader(content));
					String line;
					while ((line = reader.readLine()) != null) {
						builder.append(line);
					}
					result = (builder.toString().trim() != null || builder
							.toString().trim() != "") ? builder.toString()
							.trim() : Constants.ERROR_SERVER;
					// result = Constants.ERROR_SERVER;
				}
			} catch (UnknownHostException e) {
				// e.printStackTrace();
				// // // Log.i("fail ", e.getMessage());
				result = Constants.ERROR_SERVER;
			} catch (ClientProtocolException e) {
				// e.printStackTrace();
				// // // Log.i("fail ", e.getMessage());
				result = Constants.ERROR_SERVER;
			} catch (IOException e) {
				// e.printStackTrace();
				// // // Log.i("fail ", e.getMessage());
				result = Constants.ERROR_SERVER;
			} catch (Exception e) {
				// e.printStackTrace();
				// // // Log.i("fail ", e.getMessage());
				result = Constants.ERROR_SERVER;
			} finally {
				if (reader != null) {
					try {
						reader.close();
					} catch (final IOException e) {
						// e.printStackTrace();
						result = Constants.ERROR_SERVER;
					}
				}

			}
		} else {
			result = Constants.ERROR_INTERNET;
		}
		// // Log.w("Result ", result);
		return result;
	}

	public synchronized String readPay1B2CRequest(Context context, String url) {

		if (Constants.isOnline(context)) {

			BufferedReader reader = null;

			try {
				if (Utility.getCookieValue(context,
						Constants.SHAREDPREFERENCE_COOKIE_VALUE) == null) {
					cookieStore = new BasicCookieStore();

				} else {
					cookieStore = new BasicCookieStore();

					BasicClientCookie cookie = new BasicClientCookie(
							Utility.getCookieName(context,
									Constants.SHAREDPREFERENCE_COOKIE_NAME),
							Utility.getCookieValue(context,
									Constants.SHAREDPREFERENCE_COOKIE_VALUE));
					cookie.setVersion(Utility.getCookieVersion(context,
							Constants.SHAREDPREFERENCE_COOKIE_VERSION));
					cookie.setDomain(Utility.getCookieDomain(context,
							Constants.SHAREDPREFERENCE_COOKIE_DOMAIN));
					cookie.setPath(Utility.getCookiePath(context,
							Constants.SHAREDPREFERENCE_COOKIE_PATH));
					// Date expiryDate = new
					// Date(Utility.getCookieExpiry(context,
					// Constants.SHAREDPREFERENCE_COOKIE_EXPIRY));
					// cookie.setExpiryDate(expiryDate);

					cookieStore.addCookie(cookie);
				}

				HttpContext ctx = new BasicHttpContext();
				ctx.setAttribute(ClientContext.COOKIE_STORE, cookieStore);
				StringBuilder builder = new StringBuilder();
				// // Log.w("URL", url);
				httpPost = new HttpPost(url);
				//httpClient.setCookieStore(cookieStore);
				httpResponse = httpClient.execute(httpPost, ctx);
				statusLine = httpResponse.getStatusLine();
				int statusCode = statusLine.getStatusCode();

				// HttpContext ctx = new BasicHttpContext();
				// ctx.setAttribute(ClientContext.COOKIE_STORE, cookieStore);
				// StringBuilder builder = new StringBuilder();
				// // // Log.w("URL", url);
				// httpPost = new HttpPost(url);
				// // httpPost.setEntity(new
				// UrlEncodedFormEntity(nameValuePairs));
				// httpResponse = httpClient.execute(httpPost, ctx);
				// statusLine = httpResponse.getStatusLine();
				// int statusCode = statusLine.getStatusCode();
				if (statusCode == 200) {
					// if (Utility.getCookieValue(context,
					// Constants.SHAREDPREFERENCE_COOKIE_VALUE) == null) {
					// cookies = cookieStore.getCookies();
					//Header[] headers=httpResponse.getHeaders("Set-Cookie");
				/*	cookies = httpClient.getCookieStore().getCookies();
					for (Cookie a : cookies) {
						if (a.getName().trim().equalsIgnoreCase("PHPSESSID")) {
							cookieStore.addCookie(a);
							Utility.setCookieVersion(context,
									Constants.SHAREDPREFERENCE_COOKIE_VERSION,
									a.getVersion());
							Utility.setCookieName(context,
									Constants.SHAREDPREFERENCE_COOKIE_NAME, a
											.getName());
							Utility.setCookieValue(context,
									Constants.SHAREDPREFERENCE_COOKIE_VALUE, a
											.getValue());
							Utility.setCookieDomain(context,
									Constants.SHAREDPREFERENCE_COOKIE_DOMAIN, a
											.getDomain());
							Utility.setCookiePath(context,
									Constants.SHAREDPREFERENCE_COOKIE_PATH, a
											.getPath());
							break;
						}*/
						// Utility.setCookieExpiry(context,
						// Constants.SHAREDPREFERENCE_COOKIE_EXPIRY, a
						// .getExpiryDate().toString());
						// // System.out.println("- " +
						// a.getValue().toString());

				//	}
					// }
					entity = httpResponse.getEntity();
					content = entity.getContent();
					reader = new BufferedReader(new InputStreamReader(content));
					String line;
					while ((line = reader.readLine()) != null) {
						builder.append(line);
					}
					result = (builder.toString().trim() != null || builder
							.toString().trim() != "") ? builder.toString()
							.trim() : Constants.ERROR_SERVER;
				} else {
					// // // Log.e(RequestClass.class.toString(),
					// "Failed to connect");
					result = Constants.ERROR_SERVER;
				}
			} catch (UnknownHostException e) {
				// e.printStackTrace();
				// // // Log.i("fail ", e.getMessage());
				result = Constants.ERROR_SERVER;
			} catch (ClientProtocolException e) {
				// e.printStackTrace();
				// // // Log.i("fail ", e.getMessage());
				result = Constants.ERROR_SERVER;
			} catch (IOException e) {
				// e.printStackTrace();
				// // // Log.i("fail ", e.getMessage());
				result = Constants.ERROR_SERVER;
			} catch (Exception e) {
				// e.printStackTrace();
				// // // Log.i("fail ", e.getMessage());
				result = Constants.ERROR_SERVER;
			} finally {
				if (reader != null) {
					try {
						reader.close();
					} catch (final IOException e) {
						// e.printStackTrace();
						result = Constants.ERROR_SERVER;
					}
				}

			}
		} else {
			result = Constants.ERROR_INTERNET;
		}
		return result;
	}

	public String readPay1B2BRequest(Context context, String url,
			List<NameValuePair> nameValuePairs) {

		if (Constants.isOnline(context)) {

			BufferedReader reader = null;

			try {
				if (Utility.getCookieValue(context,
						Constants.SHAREDPREFERENCE_COOKIE_VALUE_PANEL) == null) {
					cookieStore = new BasicCookieStore();

				} else {
					cookieStore = new BasicCookieStore();

					BasicClientCookie cookie = new BasicClientCookie(
							Utility.getCookieName(
									context,
									Constants.SHAREDPREFERENCE_COOKIE_NAME_PANEL),
							Utility.getCookieValue(
									context,
									Constants.SHAREDPREFERENCE_COOKIE_VALUE_PANEL));
					cookie.setVersion(Utility.getCookieVersion(context,
							Constants.SHAREDPREFERENCE_COOKIE_VERSION_PANEL));
					cookie.setDomain(Utility.getCookieDomain(context,
							Constants.SHAREDPREFERENCE_COOKIE_DOMAIN_PANEL));
					cookie.setPath(Utility.getCookiePath(context,
							Constants.SHAREDPREFERENCE_COOKIE_PATH_PANEL));
					// Date expiryDate = new
					// Date(Utility.getCookieExpiry(context,
					// Constants.SHAREDPREFERENCE_COOKIE_EXPIRY));
					// cookie.setExpiryDate(expiryDate);

					cookieStore.addCookie(cookie);
				}
				HttpContext ctx = new BasicHttpContext();
				ctx.setAttribute(ClientContext.COOKIE_STORE, cookieStore);
				StringBuilder builder = new StringBuilder();
				// // // Log.v("URL", URL + part);
				httpPost = new HttpPost(url);
				httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				httpResponse = httpClient.execute(httpPost, ctx);
				statusLine = httpResponse.getStatusLine();
				int statusCode = statusLine.getStatusCode();
				if (statusCode == 200) {
					// if (Utility.getCookieValue(context,
					// Constants.SHAREDPREFERENCE_COOKIE_VALUE) == null) {
					cookies = cookieStore.getCookies();
					for (Cookie a : cookies) {
						cookieStore.addCookie(a);
						Utility.setCookieVersion(
								context,
								Constants.SHAREDPREFERENCE_COOKIE_VERSION_PANEL,
								a.getVersion());
						Utility.setCookieName(context,
								Constants.SHAREDPREFERENCE_COOKIE_NAME_PANEL,
								a.getName());
						Utility.setCookieValue(context,
								Constants.SHAREDPREFERENCE_COOKIE_VALUE_PANEL,
								a.getValue());
						Utility.setCookieDomain(context,
								Constants.SHAREDPREFERENCE_COOKIE_DOMAIN_PANEL,
								a.getDomain());
						Utility.setCookiePath(context,
								Constants.SHAREDPREFERENCE_COOKIE_PATH_PANEL,
								a.getPath());
						break;
						// Utility.setCookieExpiry(context,
						// Constants.SHAREDPREFERENCE_COOKIE_EXPIRY,
						// String.valueOf(a.getExpiryDate()));
						// // System.out.println("- " +
						// a.getValue().toString());
					}
					// }
					entity = httpResponse.getEntity();
					content = entity.getContent();
					reader = new BufferedReader(new InputStreamReader(content));
					String line;
					while ((line = reader.readLine()) != null) {
						builder.append(line);
					}
					result = (builder.toString().trim() != null || builder
							.toString().trim() != "") ? builder.toString()
							.trim() : Constants.ERROR_SERVER;
				} else {
					// // // Log.e(RequestClass.class.toString(),
					// "Failed to connect");
					entity = httpResponse.getEntity();
					content = entity.getContent();
					reader = new BufferedReader(new InputStreamReader(content));
					String line;
					while ((line = reader.readLine()) != null) {
						builder.append(line);
					}
					result = (builder.toString().trim() != null || builder
							.toString().trim() != "") ? builder.toString()
							.trim() : Constants.ERROR_SERVER;
					// result = Constants.ERROR_SERVER;
				}
			} catch (UnknownHostException e) {
				// e.printStackTrace();
				// // // Log.i("fail ", e.getMessage());
				result = Constants.ERROR_SERVER;
			} catch (ClientProtocolException e) {
				// e.printStackTrace();
				// // // Log.i("fail ", e.getMessage());
				result = Constants.ERROR_SERVER;
			} catch (IOException e) {
				// e.printStackTrace();
				// // // Log.i("fail ", e.getMessage());
				result = Constants.ERROR_SERVER;
			} catch (Exception e) {
				// e.printStackTrace();
				// // // Log.i("fail ", e.getMessage());
				result = Constants.ERROR_SERVER;
			} finally {
				if (reader != null) {
					try {
						reader.close();
					} catch (final IOException e) {
						// e.printStackTrace();
						result = Constants.ERROR_SERVER;
					}
				}

			}
		} else {
			result = Constants.ERROR_INTERNET;
		}
		// // Log.w("Result ", result);
		return result;
	}

	public String readPay1B2BRequest(Context context, String url) {

		if (Constants.isOnline(context)) {

			BufferedReader reader = null;

			try {
				if (Utility.getCookieValue(context,
						Constants.SHAREDPREFERENCE_COOKIE_VALUE_PANEL) == null) {
					cookieStore = new BasicCookieStore();

				} else {
					cookieStore = new BasicCookieStore();

					BasicClientCookie cookie = new BasicClientCookie(
							Utility.getCookieName(
									context,
									Constants.SHAREDPREFERENCE_COOKIE_NAME_PANEL),
							Utility.getCookieValue(
									context,
									Constants.SHAREDPREFERENCE_COOKIE_VALUE_PANEL));
					cookie.setVersion(Utility.getCookieVersion(context,
							Constants.SHAREDPREFERENCE_COOKIE_VERSION_PANEL));
					cookie.setDomain(Utility.getCookieDomain(context,
							Constants.SHAREDPREFERENCE_COOKIE_DOMAIN_PANEL));
					cookie.setPath(Utility.getCookiePath(context,
							Constants.SHAREDPREFERENCE_COOKIE_PATH_PANEL));
					// Date expiryDate = new
					// Date(Utility.getCookieExpiry(context,
					// Constants.SHAREDPREFERENCE_COOKIE_EXPIRY));
					// cookie.setExpiryDate(expiryDate);

					cookieStore.addCookie(cookie);
				}
				HttpContext ctx = new BasicHttpContext();
				ctx.setAttribute(ClientContext.COOKIE_STORE, cookieStore);
				StringBuilder builder = new StringBuilder();
				// // Log.w("URL", url);
				httpPost = new HttpPost(url);
				// httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				httpResponse = httpClient.execute(httpPost, ctx);
				statusLine = httpResponse.getStatusLine();
				int statusCode = statusLine.getStatusCode();
				if (statusCode == 200) {
					// if (Utility.getCookieValue(context,
					// Constants.SHAREDPREFERENCE_COOKIE_VALUE) == null) {
					cookies = cookieStore.getCookies();
					for (Cookie a : cookies) {
						cookieStore.addCookie(a);
						Utility.setCookieVersion(
								context,
								Constants.SHAREDPREFERENCE_COOKIE_VERSION_PANEL,
								a.getVersion());
						Utility.setCookieName(context,
								Constants.SHAREDPREFERENCE_COOKIE_NAME_PANEL,
								a.getName());
						Utility.setCookieValue(context,
								Constants.SHAREDPREFERENCE_COOKIE_VALUE_PANEL,
								a.getValue());
						Utility.setCookieDomain(context,
								Constants.SHAREDPREFERENCE_COOKIE_DOMAIN_PANEL,
								a.getDomain());
						Utility.setCookiePath(context,
								Constants.SHAREDPREFERENCE_COOKIE_PATH_PANEL,
								a.getPath());
						break;
						// Utility.setCookieExpiry(context,
						// Constants.SHAREDPREFERENCE_COOKIE_EXPIRY, a
						// .getExpiryDate().toString());
						// // System.out.println("- " +
						// a.getValue().toString());

					}
					// }
					entity = httpResponse.getEntity();
					content = entity.getContent();
					reader = new BufferedReader(new InputStreamReader(content));
					String line;
					while ((line = reader.readLine()) != null) {
						builder.append(line);
					}
					result = (builder.toString().trim() != null || builder
							.toString().trim() != "") ? builder.toString()
							.trim() : Constants.ERROR_SERVER;
				} else {
					// // // Log.e(RequestClass.class.toString(),
					// "Failed to connect");
					result = Constants.ERROR_SERVER;
				}
			} catch (UnknownHostException e) {
				// e.printStackTrace();
				// // // Log.i("fail ", e.getMessage());
				result = Constants.ERROR_SERVER;
			} catch (ClientProtocolException e) {
				// e.printStackTrace();
				// // // Log.i("fail ", e.getMessage());
				result = Constants.ERROR_SERVER;
			} catch (IOException e) {
				// e.printStackTrace();
				// // // Log.i("fail ", e.getMessage());
				result = Constants.ERROR_SERVER;
			} catch (Exception e) {
				// e.printStackTrace();
				// // // Log.i("fail ", e.getMessage());
				result = Constants.ERROR_SERVER;
			} finally {
				if (reader != null) {
					try {
						reader.close();
					} catch (final IOException e) {
						// e.printStackTrace();
						result = Constants.ERROR_SERVER;
					}
				}

			}
		} else {
			result = Constants.ERROR_INTERNET;
		}
		return result;
	}

	public String readPay1B2CAppUpdateRequest(Context context, String url) {

		if (Constants.isOnline(context)) {

			BufferedReader reader = null;

			try {

				StringBuilder builder = new StringBuilder();
				// // Log.w("URL", url);
				httpPost = new HttpPost(url);
				// httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				httpResponse = httpClient.execute(httpPost);
				statusLine = httpResponse.getStatusLine();
				int statusCode = statusLine.getStatusCode();
				if (statusCode == 200) {
					entity = httpResponse.getEntity();
					content = entity.getContent();
					reader = new BufferedReader(new InputStreamReader(content));
					String line;
					while ((line = reader.readLine()) != null) {
						builder.append(line);
					}
					result = (builder.toString().trim() != null || builder
							.toString().trim() != "") ? builder.toString()
							.trim() : Constants.ERROR_SERVER;
				} else {
					// // // Log.e(RequestClass.class.toString(),
					// "Failed to connect");
					result = Constants.ERROR_SERVER;
				}
			} catch (UnknownHostException e) {
				// e.printStackTrace();
				// // // Log.i("fail ", e.getMessage());
				result = Constants.ERROR_SERVER;
			} catch (ClientProtocolException e) {
				// e.printStackTrace();
				// // // Log.i("fail ", e.getMessage());
				result = Constants.ERROR_SERVER;
			} catch (IOException e) {
				// e.printStackTrace();
				// // // Log.i("fail ", e.getMessage());
				result = Constants.ERROR_SERVER;
			} catch (Exception e) {
				// e.printStackTrace();
				// // // Log.i("fail ", e.getMessage());
				result = Constants.ERROR_SERVER;
			} finally {
				if (reader != null) {
					try {
						reader.close();
					} catch (final IOException e) {
						// e.printStackTrace();
						result = Constants.ERROR_SERVER;
					}
				}

			}
		} else {
			result = Constants.ERROR_INTERNET;
		}
		return result;
	}
}