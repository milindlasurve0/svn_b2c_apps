package com.pay1;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.pay1.customviews.DatePickerDailog;
import com.pay1.customviews.MyProgressDialog;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pay1.constants.Constants;
import com.pay1.requesthandler.RequestClass;
import com.pay1.utilities.EditTextValidator;
import com.pay1.utilities.Utility;

public class ProfileActivity extends AppBaseActivity {

	private static final String SCREEN_LABEL = "Profile Screen";
	private EasyTracker easyTracker = null;
	private final String TAG = "Profile";
	private TextView textView_TitleName, textView_TitleDOB,
			textView_TitleGender, textView_TitleEmail, textView_TitleNumber,
			textView_TitlePin;
	private TextView textView_Title, textView_Name, textView_DOB,
			textView_Gender, textView_Email, textView_Mobile, textView_Pin;
	private EditText editText_Name, editText_Email, editText_Mobile;
	private ImageView imageView_EditName, imageView_EditDOB,
			imageView_EditGender, imageView_EditEmail, imageView_EditMobile,
			imageView_EditPin, imageView_Back;
	private RelativeLayout relativeLayout_Name, relativeLayout_Gender,
			relativeLayout_Email;
	private ImageView imageView_NameOk, imageView_NameCancel, imageView_DOBOk,
			imageView_DOBCancel, imageView_GenderOk, imageView_GenderCancel,
			imageView_EmailOk, imageView_EmailCancel;
	private Button button_Male, button_Female;
	private LinearLayout dobAction;

	int selected_id = -1;
	private int mYear;
	private int mMonth;
	private int mDay;
	StringBuilder builder;
	static final int DATE_DIALOG_ID = 0;
	static final int UPDATE_EMAIL = 1;
	static final int CHANGE_PIN = 2;
	String months[] = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug",
			"Sep", "Oct", "Nov", "Dec" };

	private Calendar dateandtime;

	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.profile_activity);

		try {
			easyTracker = EasyTracker.getInstance(ProfileActivity.this);
			easyTracker.set(Fields.SCREEN_NAME, SCREEN_LABEL);
			registerBaseActivityReceiver();

			dateandtime = Calendar.getInstance();
			Typeface Normal = Typeface.createFromAsset(getAssets(),
					"EncodeSansNormal-100-Thin.ttf");
			Typeface Reguler = Typeface.createFromAsset(getAssets(),
					"EncodeSansNormal-400-Regular.ttf");

			textView_Title = (TextView) findViewById(R.id.textView_Title);
			textView_Title.setTypeface(Reguler);

			imageView_Back = (ImageView) findViewById(R.id.imageView_Back);
			imageView_Back.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					// overridePendingTransition(R.anim.slide_in_left,
					// R.anim.slide_out_right);
					// finish();
					onBackPressed();
				}
			});

			String d = Utility
					.getDOB(ProfileActivity.this,
							Constants.SHAREDPREFERENCE_DOB).toString().trim()
					.replaceAll("-", "/");

			Date dt = null;
			try {
				dt = new Date(d);
			} catch (Exception e) {
				dt = new Date();
			}
			final Calendar c = Calendar.getInstance();
			c.setTime(dt);

			dateandtime.setTime(dt);
			mYear = c.get(Calendar.YEAR);
			mMonth = c.get(Calendar.MONTH);
			mDay = c.get(Calendar.DAY_OF_MONTH);

			textView_TitleName = (TextView) findViewById(R.id.textView_TitleName);
			textView_TitleName.setTypeface(Reguler);
			textView_TitleDOB = (TextView) findViewById(R.id.textView_TitleDOB);
			textView_TitleDOB.setTypeface(Reguler);
			textView_TitleGender = (TextView) findViewById(R.id.textView_TitleGender);
			textView_TitleGender.setTypeface(Reguler);
			textView_TitleEmail = (TextView) findViewById(R.id.textView_TitleEmail);
			textView_TitleEmail.setTypeface(Reguler);
			textView_TitleNumber = (TextView) findViewById(R.id.textView_TitleNumber);
			textView_TitleNumber.setTypeface(Reguler);
			textView_TitlePin = (TextView) findViewById(R.id.textView_TitlePin);
			textView_TitlePin.setTypeface(Reguler);

			textView_Name = (TextView) findViewById(R.id.textView_Name);
			textView_Name.setTypeface(Normal);
			textView_DOB = (TextView) findViewById(R.id.textView_DOB);
			textView_DOB.setTypeface(Normal);
			builder = new StringBuilder(
					new SimpleDateFormat("yyyy-MM-dd").format(c.getTime()));
			textView_DOB.setText(new SimpleDateFormat("dd MMM yyyy")
					.format(dateandtime.getTime()));
			textView_Gender = (TextView) findViewById(R.id.textView_Gender);
			textView_Gender.setTypeface(Normal);
			textView_Email = (TextView) findViewById(R.id.textView_Email);
			textView_Email.setTypeface(Normal);
			textView_Email.setText(Utility.getEmail(ProfileActivity.this,
					Constants.SHAREDPREFERENCE_EMAIL));
			textView_Mobile = (TextView) findViewById(R.id.textView_Mobile);
			textView_Mobile.setTypeface(Normal);
			textView_Mobile.setText(Utility.getMobileNumber(
					ProfileActivity.this, Constants.SHAREDPREFERENCE_MOBILE));
			textView_Pin = (TextView) findViewById(R.id.textView_Pin);
			textView_Pin.setTypeface(Normal);
			textView_Pin.setText(Utility.getPin(ProfileActivity.this,
					Constants.SHAREDPREFERENCE_PIN));

			relativeLayout_Name = (RelativeLayout) findViewById(R.id.relativeLayout_Name);
			relativeLayout_Name.setVisibility(View.GONE);
			imageView_NameOk = (ImageView) findViewById(R.id.imageView_NameOk);
			imageView_NameCancel = (ImageView) findViewById(R.id.imageView_NameCancel);

			dobAction = (LinearLayout) findViewById(R.id.dobAction);
			dobAction.setVisibility(View.GONE);
			imageView_DOBOk = (ImageView) findViewById(R.id.imageView_DOBOk);
			imageView_DOBCancel = (ImageView) findViewById(R.id.imageView_DOBCancel);

			relativeLayout_Gender = (RelativeLayout) findViewById(R.id.relativeLayout_Gender);
			relativeLayout_Gender.setVisibility(View.GONE);
			imageView_GenderOk = (ImageView) findViewById(R.id.imageView_GenderOk);
			imageView_GenderCancel = (ImageView) findViewById(R.id.imageView_GenderCancel);
			button_Male = (Button) findViewById(R.id.button_Male);
			button_Male.setTypeface(Normal);
			button_Female = (Button) findViewById(R.id.button_Female);
			button_Female.setTypeface(Normal);

			relativeLayout_Email = (RelativeLayout) findViewById(R.id.relativeLayout_Email);
			relativeLayout_Email.setVisibility(View.GONE);
			imageView_EmailOk = (ImageView) findViewById(R.id.imageView_EmailOk);
			imageView_EmailCancel = (ImageView) findViewById(R.id.imageView_EmailCancel);

			editText_Name = (EditText) findViewById(R.id.editText_Name);
			editText_Name.setTypeface(Normal);
			editText_Name.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					// TODO Auto-generated method stub
					if (editText_Name.getText().toString().trim().length() > 0)
						EditTextValidator
								.hasText(ProfileActivity.this, editText_Name,
										Constants.ERROR_NAME_BLANK_FIELD);
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					// TODO Auto-generated method stub

				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

				}
			});
			editText_Email = (EditText) findViewById(R.id.editText_Email);
			editText_Email.setTypeface(Normal);
			editText_Mobile = (EditText) findViewById(R.id.editText_Mobile);
			editText_Mobile.setTypeface(Normal);
			editText_Mobile.setVisibility(View.GONE);

			imageView_EditName = (ImageView) findViewById(R.id.imageView_EditName);
			imageView_EditDOB = (ImageView) findViewById(R.id.imageView_EditDOB);
			imageView_EditGender = (ImageView) findViewById(R.id.imageView_EditGender);
			imageView_EditEmail = (ImageView) findViewById(R.id.imageView_EditEmail);
			imageView_EditMobile = (ImageView) findViewById(R.id.imageView_EditMobile);
			imageView_EditMobile.setVisibility(View.GONE);
			imageView_EditPin = (ImageView) findViewById(R.id.imageView_EditPin);

			imageView_EditPin.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					relativeLayout_Name.setVisibility(View.GONE);
					textView_Name.setVisibility(View.VISIBLE);
					imageView_EditName.setVisibility(View.VISIBLE);

					dobAction.setVisibility(View.GONE);
					imageView_EditDOB.setVisibility(View.VISIBLE);

					relativeLayout_Gender.setVisibility(View.GONE);
					textView_Gender.setVisibility(View.VISIBLE);
					imageView_EditGender.setVisibility(View.VISIBLE);

					relativeLayout_Email.setVisibility(View.GONE);
					textView_Email.setVisibility(View.VISIBLE);
					imageView_EditEmail.setVisibility(View.VISIBLE);

					Intent intent = new Intent(ProfileActivity.this,
							ChangePinActivity.class);
					startActivityForResult(intent, CHANGE_PIN);
				}
			});

			updateName();
			updateDOB();
			updateGender();
			// updateEmail();

			imageView_EditEmail.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					relativeLayout_Name.setVisibility(View.GONE);
					textView_Name.setVisibility(View.VISIBLE);
					imageView_EditName.setVisibility(View.VISIBLE);

					dobAction.setVisibility(View.GONE);
					imageView_EditDOB.setVisibility(View.VISIBLE);

					relativeLayout_Gender.setVisibility(View.GONE);
					textView_Gender.setVisibility(View.VISIBLE);
					imageView_EditGender.setVisibility(View.VISIBLE);

					relativeLayout_Email.setVisibility(View.GONE);
					textView_Email.setVisibility(View.VISIBLE);
					imageView_EditEmail.setVisibility(View.VISIBLE);

					Intent intent = new Intent(ProfileActivity.this,
							ChangeEmailActivity.class);
					intent.putExtra("EMAIL", textView_Email.getText()
							.toString().trim());
					startActivityForResult(intent, UPDATE_EMAIL);
				}
			});
		} catch (Exception e) {
		}
		overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}

	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView().build());
		// EasyTracker.getInstance(this).activityStart(this);
		easyTracker.send(MapBuilder.createAppView().build());
	}

	@Override
	public void onStop() {
		super.onStop();
		// EasyTracker.getInstance(this).activityStop(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (Constants.isExpanded) {
			Constants.collapsView(this);
		} else {
			Constants.SELECTED_MENU_INDEX = Constants.PROFILE_MENU_INDEX;
			Constants.showNavigationBar(ProfileActivity.this);
			// overridePendingTransition(R.anim.push_down_in,
			// R.anim.push_down_out);
		}
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
		unRegisterBaseActivityReceiver();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (Constants.isExpanded) {
			Constants.collapsView(this);
		} else {
			super.onBackPressed();
			overridePendingTransition(R.anim.slide_in_left,
					R.anim.slide_out_right);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		try {
			if (resultCode == Activity.RESULT_OK) {
				if (requestCode == UPDATE_EMAIL) {
					textView_Email.setText(data.getExtras().getString("EMAIL"));
				}
				if (requestCode == CHANGE_PIN) {
					textView_Pin.setText(data.getExtras().getString("PIN"));
				}
			}

		} catch (Exception e) {
		}
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DATE_DIALOG_ID:
			DatePickerDialog dialog = new DatePickerDialog(this,
					mDateSetListener, mYear, mMonth, mDay);

			return dialog;

		}
		return null;
	}

	private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {

		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			mYear = year;
			mMonth = monthOfYear;
			mDay = dayOfMonth;
			updateDisplay();
		}
	};

	private void updateDisplay() {

		String d = mDay < 10 ? "0" + mDay : "" + mDay;
		String m = (mMonth + 1) < 10 ? "0" + (mMonth + 1) : "" + (mMonth + 1);
		builder = new StringBuilder().append(mYear).append("-").append(m)
				.append("-").append(d);
		textView_DOB.setText(d + "-" + months[mMonth] + "-" + mYear);

	}

	private void updateName() {
		textView_Name.setText(Utility.getUserName(ProfileActivity.this,
				Constants.SHAREDPREFERENCE_NAME));
		imageView_NameOk.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (EditTextValidator.hasText(ProfileActivity.this,
						editText_Name, Constants.ERROR_NAME_BLANK_FIELD)) {

					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

					textView_Name.setText(editText_Name.getText().toString()
							.trim());
					String str = textView_Gender.getText().toString().trim()
							.equalsIgnoreCase("Male") ? "m" : "f";
					new UpdateProfileTask().execute(editText_Name.getText()
							.toString().trim(), builder.toString().trim(), str);
				} else {
					editText_Name.requestFocus();
				}
			}
		});
		imageView_NameCancel.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

				relativeLayout_Name.setVisibility(View.GONE);
				textView_Name.setVisibility(View.VISIBLE);
				imageView_EditName.setVisibility(View.VISIBLE);
			}
		});
		imageView_EditName.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				editText_Name
						.setText(textView_Name.getText().toString().trim());
				relativeLayout_Name.setVisibility(View.VISIBLE);
				textView_Name.setVisibility(View.GONE);
				imageView_EditName.setVisibility(View.GONE);

				dobAction.setVisibility(View.GONE);
				imageView_EditDOB.setVisibility(View.VISIBLE);

				relativeLayout_Gender.setVisibility(View.GONE);
				textView_Gender.setVisibility(View.VISIBLE);
				imageView_EditGender.setVisibility(View.VISIBLE);

				relativeLayout_Email.setVisibility(View.GONE);
				textView_Email.setVisibility(View.VISIBLE);
				imageView_EditEmail.setVisibility(View.VISIBLE);
			}
		});
	}

	private void updateDOB() {
		// updateDisplay();
		imageView_DOBOk.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				String str = textView_Gender.getText().toString().trim()
						.equalsIgnoreCase("Male") ? "m" : "f";
				new UpdateProfileTask().execute(editText_Name.getText()
						.toString().trim(), builder.toString().trim(), str);
			}
		});
		imageView_DOBCancel.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dobAction.setVisibility(View.GONE);
				imageView_EditDOB.setVisibility(View.VISIBLE);
			}
		});
		imageView_EditDOB.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// showDialog(DATE_DIALOG_ID);

				DatePickerDailog dp = new DatePickerDailog(
						ProfileActivity.this, dateandtime,
						new DatePickerDailog.DatePickerListner() {

							@Override
							public void OnDoneButton(Dialog datedialog,
									Calendar c) {
								datedialog.dismiss();
								dateandtime.set(Calendar.YEAR,
										c.get(Calendar.YEAR));
								dateandtime.set(Calendar.MONTH,
										c.get(Calendar.MONTH));
								dateandtime.set(Calendar.DAY_OF_MONTH,
										c.get(Calendar.DAY_OF_MONTH));
								// ((Button) arg0)
								// .setText(new SimpleDateFormat(
								// "MMMM dd yyyy").format(c
								// .getTime()));

								// String d = mDay < 10 ? "0" + mDay : ""
								// + mDay;
								// String m = (mMonth + 1) < 10 ? "0"
								// + (mMonth + 1) : "" + (mMonth + 1);
								// builder = new
								// StringBuilder().append(mYear)
								// .append("-").append(m).append("-")
								// .append(d);
								builder = new StringBuilder(
										new SimpleDateFormat("yyyy-MM-dd")
												.format(dateandtime.getTime()));
								textView_DOB.setText(new SimpleDateFormat(
										"dd MMM yyyy").format(dateandtime
										.getTime()));
							}

							@Override
							public void OnCancelButton(Dialog datedialog) {
								// TODO Auto-generated method stub
								datedialog.dismiss();
							}
						});
				dp.show();
				dobAction.setVisibility(View.VISIBLE);
				imageView_EditDOB.setVisibility(View.GONE);

				relativeLayout_Name.setVisibility(View.GONE);
				textView_Name.setVisibility(View.VISIBLE);
				imageView_EditName.setVisibility(View.VISIBLE);

				relativeLayout_Gender.setVisibility(View.GONE);
				textView_Gender.setVisibility(View.VISIBLE);
				imageView_EditGender.setVisibility(View.VISIBLE);

				relativeLayout_Email.setVisibility(View.GONE);
				textView_Email.setVisibility(View.VISIBLE);
				imageView_EditEmail.setVisibility(View.VISIBLE);
			}
		});

	}

	private void updateGender() {
		String s = Utility.getGender(ProfileActivity.this,
				Constants.SHAREDPREFERENCE_GENDER);
		textView_Gender.setText(s.startsWith("m") ? "Male" : "Female");
		imageView_GenderOk.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (selected_id == -1) {
					Constants.showOneButtonDialog(ProfileActivity.this, TAG,
							"Please select gender.", Constants.DIALOG_CLOSE);
				} else {
					String str = selected_id == 0 ? "m" : "f";
					textView_Gender.setText(selected_id == 0 ? "Male"
							: "Female");
					new UpdateProfileTask().execute(textView_Name.getText()
							.toString().trim(), builder.toString().trim(), str);
				}
			}
		});
		imageView_GenderCancel.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				relativeLayout_Gender.setVisibility(View.GONE);
				textView_Gender.setVisibility(View.VISIBLE);
				imageView_EditGender.setVisibility(View.VISIBLE);
			}
		});
		imageView_EditGender.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				relativeLayout_Gender.setVisibility(View.VISIBLE);
				textView_Gender.setVisibility(View.GONE);
				imageView_EditGender.setVisibility(View.GONE);

				relativeLayout_Name.setVisibility(View.GONE);
				textView_Name.setVisibility(View.VISIBLE);
				imageView_EditName.setVisibility(View.VISIBLE);

				dobAction.setVisibility(View.GONE);
				imageView_EditDOB.setVisibility(View.VISIBLE);

				relativeLayout_Email.setVisibility(View.GONE);
				textView_Email.setVisibility(View.VISIBLE);
				imageView_EditEmail.setVisibility(View.VISIBLE);
			}
		});
		button_Male = (Button) findViewById(R.id.button_Male);
		button_Male.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				selected_id = 0;
				button_Male
						.setBackgroundResource(R.drawable.button_login_selected);
				button_Male
						.setTextColor(getResources().getColor(R.color.White));
				button_Female
						.setBackgroundResource(R.drawable.button_signup_selected);
				button_Female.setTextColor(getResources().getColor(
						R.color.app_blue_color));
			}
		});

		button_Female = (Button) findViewById(R.id.button_Female);
		button_Female.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				selected_id = 1;
				button_Female
						.setBackgroundResource(R.drawable.button_login_selected);
				button_Female.setTextColor(getResources().getColor(
						R.color.White));
				button_Male
						.setBackgroundResource(R.drawable.button_signup_selected);
				button_Male.setTextColor(getResources().getColor(
						R.color.app_blue_color));
			}
		});

		if (s.startsWith("m")) {
			selected_id = 0;
			button_Male.setBackgroundResource(R.drawable.button_login_selected);
			button_Male.setTextColor(getResources().getColor(R.color.White));
			button_Female
					.setBackgroundResource(R.drawable.button_signup_selected);
			button_Female.setTextColor(getResources().getColor(
					R.color.app_blue_color));
		} else {
			selected_id = 1;
			button_Female
					.setBackgroundResource(R.drawable.button_login_selected);
			button_Female.setTextColor(getResources().getColor(R.color.White));
			button_Male
					.setBackgroundResource(R.drawable.button_signup_selected);
			button_Male.setTextColor(getResources().getColor(
					R.color.app_blue_color));
		}
	}

	private void updateEmail() {
		textView_Email.setText(Utility.getEmail(ProfileActivity.this,
				Constants.SHAREDPREFERENCE_EMAIL));
		imageView_EmailOk.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (EditTextValidator.hasText(ProfileActivity.this,
						editText_Email, Constants.ERROR_EMAIL_BLANK_FIELD)
						&& EditTextValidator.isEmailAddress(
								ProfileActivity.this, editText_Email,
								Constants.ERROR_EMAIL_VALID_FIELD)) {

					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

					textView_Email.setText(editText_Email.getText().toString()
							.trim());
					String str = textView_Gender.getText().toString().trim()
							.equalsIgnoreCase("Male") ? "m" : "f";
					new UpdateProfileTask().execute(textView_Name.getText()
							.toString().trim(), builder.toString().trim(), str,
							editText_Email.getText().toString().trim());
				} else {
					editText_Email.requestFocus();
				}
			}
		});
		imageView_EmailCancel.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

				relativeLayout_Email.setVisibility(View.GONE);
				textView_Email.setVisibility(View.VISIBLE);
				imageView_EditEmail.setVisibility(View.VISIBLE);
			}
		});
		imageView_EditEmail.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				editText_Email.setText(textView_Email.getText().toString()
						.trim());
				relativeLayout_Email.setVisibility(View.VISIBLE);
				textView_Email.setVisibility(View.GONE);
				imageView_EditEmail.setVisibility(View.GONE);

				relativeLayout_Name.setVisibility(View.GONE);
				textView_Name.setVisibility(View.VISIBLE);
				imageView_EditName.setVisibility(View.VISIBLE);

				dobAction.setVisibility(View.GONE);
				imageView_EditDOB.setVisibility(View.VISIBLE);

				relativeLayout_Gender.setVisibility(View.GONE);
				textView_Gender.setVisibility(View.VISIBLE);
				imageView_EditGender.setVisibility(View.VISIBLE);
			}
		});
	}

	public class UpdateProfileTask extends AsyncTask<String, String, String>
			implements OnDismissListener {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			try {
				String response = RequestClass
						.getInstance()
						.readPay1B2CRequest(
								ProfileActivity.this,
								Constants.B2C_URL + "update_profile/?name="
										+ URLEncoder.encode(params[0], "utf-8")
										+ "&date_of_birth="
										+ URLEncoder.encode(params[1], "utf-8")
										+ "&gender="
										+ URLEncoder.encode(params[2], "utf-8"));

				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			try {

				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONObject jsonObject = new JSONObject(replaced);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {

						// JSONObject jsonObject2 = jsonObject
						// .getJSONObject("description");
						relativeLayout_Name.setVisibility(View.GONE);
						textView_Name.setVisibility(View.VISIBLE);
						imageView_EditName.setVisibility(View.VISIBLE);

						dobAction.setVisibility(View.GONE);
						imageView_EditDOB.setVisibility(View.VISIBLE);

						relativeLayout_Gender.setVisibility(View.GONE);
						textView_Gender.setVisibility(View.VISIBLE);
						imageView_EditGender.setVisibility(View.VISIBLE);

						relativeLayout_Email.setVisibility(View.GONE);
						textView_Email.setVisibility(View.VISIBLE);
						imageView_EditEmail.setVisibility(View.VISIBLE);

						Utility.setUserName(ProfileActivity.this,
								Constants.SHAREDPREFERENCE_NAME, textView_Name
										.getText().toString().trim());
						Utility.setMobileNumber(ProfileActivity.this,
								Constants.SHAREDPREFERENCE_MOBILE,
								textView_Mobile.getText().toString().trim());
						Utility.setEmail(ProfileActivity.this,
								Constants.SHAREDPREFERENCE_EMAIL,
								textView_Email.getText().toString().trim());
						Utility.setGender(ProfileActivity.this,
								Constants.SHAREDPREFERENCE_GENDER,
								textView_Gender.getText().toString().trim()
										.startsWith("M") ? "m" : "f");
						Utility.setPin(ProfileActivity.this,
								Constants.SHAREDPREFERENCE_PIN, textView_Pin
										.getText().toString().trim());
						Utility.setDOB(ProfileActivity.this,
								Constants.SHAREDPREFERENCE_DOB, builder
										.toString().trim());
						Constants.showOneButtonDialog(ProfileActivity.this,
								TAG, "Profile updated successfully.",
								Constants.DIALOG_CLOSE);

					} else {
						Constants.showOneButtonDialog(ProfileActivity.this,
								TAG, Constants.checkCode(replaced),
								Constants.DIALOG_CLOSE);
					}

				} else {
					// Constants.showOneButtonDialog(ProfileActivity.this, TAG,
					// Constants.ERROR_INTERNET, Constants.DIALOG_CLOSE);
					Intent intent = new Intent(ProfileActivity.this,
							ConnectivityErrorActivity.class);
					intent.putExtra(Constants.RECHARGE_FOR,
							Constants.WALLET_PAYMENT);
					startActivity(intent);

				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonDialog(ProfileActivity.this, TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonDialog(ProfileActivity.this, TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(ProfileActivity.this);
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(false);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							UpdateProfileTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			UpdateProfileTask.this.cancel(true);
			dialog.cancel();
		}

	}

}
