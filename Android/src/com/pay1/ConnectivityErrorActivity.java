package com.pay1;

import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.pay1.constants.Constants;

public class ConnectivityErrorActivity extends AppBaseActivity {

	private static final String SCREEN_LABEL = "Connectivity Error Screen";
	private EasyTracker easyTracker = null;

	private TextView textView_Title, textView_Msg, textView_Retry;
	private int rechargeType;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		try {
			setContentView(R.layout.connectivity_error_activity);
			easyTracker = EasyTracker
					.getInstance(ConnectivityErrorActivity.this);
			easyTracker.set(Fields.SCREEN_NAME, SCREEN_LABEL);
			findViewById();
			registerBaseActivityReceiver();

			Bundle orderBundle = getIntent().getExtras();
			rechargeType = orderBundle.getInt(Constants.RECHARGE_FOR);

			Typeface Normal = Typeface.createFromAsset(getAssets(),
					"EncodeSansNormal-100-Thin.ttf");
			Typeface Reguler = Typeface.createFromAsset(getAssets(),
					"EncodeSansNormal-400-Regular.ttf");
			Typeface Medium = Typeface.createFromAsset(getAssets(),
					"EncodeSansNarrow-500-Medium.ttf");
			textView_Title.setTypeface(Reguler);
			textView_Msg.setTypeface(Medium);
			textView_Retry.setTypeface(Normal);

			textView_Retry.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onBackPressed();
				}
			});

		} catch (Exception e) {
		}
		overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}

	private void findViewById() {
		// TODO Auto-generated method stub
		textView_Title = (TextView) findViewById(R.id.textView_Title);
		textView_Msg = (TextView) findViewById(R.id.textView_Msg);
		textView_Retry = (TextView) findViewById(R.id.textView_Retry);
	}

	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView().build());
		// EasyTracker.getInstance(this).activityStart(this);
		easyTracker.send(MapBuilder.createAppView().build());
	}

	@Override
	public void onStop() {
		super.onStop();
		// EasyTracker.getInstance(this).activityStop(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (Constants.isExpanded) {
			Constants.collapsView(this);
		} else {
			if (rechargeType == Constants.RECHARGE_MOBILE
					|| rechargeType == Constants.RECHARGE_DTH
					|| rechargeType == Constants.RECHARGE_DATA) {
				Constants.SELECTED_MENU_INDEX = Constants.RECHARGE_MENU_INDEX;
			} else if (rechargeType == Constants.SUBSCRIBE_DEAL) {
				Constants.SELECTED_MENU_INDEX = Constants.DEAL_MENU_INDEX;
			} else if (rechargeType == Constants.BILL_PAYMENT) {
				Constants.SELECTED_MENU_INDEX = Constants.BILL_MENU_INDEX;
			} else if (rechargeType == Constants.WALLET_PAYMENT) {
				Constants.SELECTED_MENU_INDEX = Constants.PROFILE_MENU_INDEX;
			}
			Constants.showNavigationBar(ConnectivityErrorActivity.this);
		}
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unRegisterBaseActivityReceiver();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (Constants.isExpanded) {
			Constants.collapsView(this);
		} else {
			super.onBackPressed();
			overridePendingTransition(R.anim.slide_in_left,
					R.anim.slide_out_right);
		}
	}

}