//
//  CurrentLocation.m
//  CurrentLocation
//
//  Created by Dharmender Yadav on 21/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CurrentLocation.h"

@implementation CurrentLocation

static CurrentLocation *_sharedLocation=nil;

+(CurrentLocation*)sharedLocation {
	@synchronized([CurrentLocation class]) {
		if(!_sharedLocation)
        [[self alloc] init];
		return _sharedLocation;
	}
}


+(id)alloc {
    @synchronized([CurrentLocation class]) {
			NSAssert(_sharedLocation==nil,@"Attempt to allocate second object of singleton class");
        _sharedLocation = [super alloc];
			return _sharedLocation;
    }
	return nil;
}

-(id)init {
	self = [super init];
	if(self!=nil) {
		CLLocationManager *locMgr = [[CLLocationManager alloc] init];
		self.locationMgr = locMgr;
        
     //   [self.locationMgr requestWhenInUseAuthorization];
     //   [self.locationMgr requestAlwaysAuthorization];
        
		self.locationMgr.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
		geocoder = [[CLGeocoder alloc] init];
        
        if([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
        [self.locationMgr requestWhenInUseAuthorization];
        [self.locationMgr requestAlwaysAuthorization];

        }
        
        if([CLLocationManager locationServicesEnabled] &&[CLLocationManager authorizationStatus] != kCLAuthorizationStatusDenied)
        {
            NSLog(@"Location Permision not granted");
        }
        
        self.locationMgr.delegate = self;

		self.isGeoCodingForCurrentLocation = FALSE;
		[self startUpdatingLocation];
	}
	return self;
}




-(void)stopUpdatingLocation {
	[_locationMgr stopUpdatingLocation];
}



-(void)startUpdatingLocation {
	NSString *model = [[UIDevice currentDevice] model];
	//NSLog(@"model:%@",model);
	if([model isEqualToString:@"iPhone Simulator"] || [model isEqualToString:@"iPad Simulator"]) {
		[self updateDefaultLocation];
	}
	else {
		[_locationMgr startUpdatingLocation];
	}
}

- (void)updateDefaultLocation {
	CLLocationCoordinate2D tempCoord;
	tempCoord.latitude = 28.563042;//37.33233141;//
	tempCoord.longitude = 77.366036;//-122.0312186;//
	self.currentLocation = [[CLLocation alloc] initWithLatitude:tempCoord.latitude longitude:tempCoord.longitude];

}


#pragma mark CLLocationManager Delegate

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
	NSString *model= [[UIDevice currentDevice] model];
	if([model isEqualToString:@"iPhone Simulator"] || [model isEqualToString:@"iPad Simulator"]) {
		[self updateDefaultLocation];
	}
	else {
		self.currentLocation = newLocation;
	}
  
	if(self.isGeoCodingForCurrentLocation) {
	[self addressFromLocation:self.currentLocation callback:^(NSError *error, CLPlacemark *placemark){
    if(error!=nil) {
    self.placemark = placemark;
    }
	}];
	}
	
//	if([_delegate respondsToSelector:self.selectorLocationUpdate])
   // [_delegate performSelector:self.selectorLocationUpdate withObject:self.currentLocation];

	//NSLog(@"lat:%f",self.currentLocation.coordinate.latitude);
    //NSLog(@"long:%f",self.currentLocation.coordinate.longitude);
}
/*
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
	//if([_delegate respondsToSelector:self.selectorLocationError])
   // [_delegate performSelector:self.selectorLocationError withObject:error];
    
    UIAlertView *   alert = [[UIAlertView alloc] initWithTitle:@"App Permission Denied"
                                                       message:@"To re-enable, please go to Settings and turn on Location Service for this app."
                                                      delegate:nil
                                             cancelButtonTitle:@"OK"
                                             otherButtonTitles:nil];
    [alert show];
    
    NSLog(@"Location Service Disabled");
    
    
}*/




#pragma mark Distance Calcutaion

- (double)getDistanceFromCurrentLocation :(double )desLat longitudeAddress:(double)desLong {
	double distance = [self distance:self.currentLocation.coordinate.latitude  lon1:self.currentLocation.coordinate.longitude lat2:desLat lon2:desLong];
	return distance;
}

-(double) distance:(double)lati1  lon1:(double)long1 lat2:(double)lat2 lon2:(double)lon2 {
	//double pi=22.0/7.0;
	double theta = long1 - lon2;
	double dist = sin([self toRadian:lati1]) * sin([self toRadian:lat2]) + cos([self toRadian:lati1]) * cos([self toRadian:lat2]) * cos([self toRadian:theta]);
	dist = acos(dist);
	dist = [self toDegree:dist];
	double distance = dist * 60 * 1.1515;
	distance = distance * 1.609344*1000;
	distance=round((distance*100))/100;
	return distance;
}

-(float)toDegree:(float)rad{
	return rad*(180/(3.14159265));
}

-(float)toRadian:(float)deg{
	return deg*((3.14159265)/180);
}


#pragma mark ReverseGeo

-(void) addressFromLocation:(CLLocation *) location callback:(geocoderCallback)callback {
	[geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
		NSLog(@"Found placemarks: %@, error: %@", placemarks, error);
		if (error == nil && [placemarks count] > 0) {
			callback(nil, [placemarks lastObject]);
		} else {
			callback(error, nil);
		}
	} ];
}

-(NSString *) getCurrentLocationFormattedAddress {
	if(self.isGeoCodingForCurrentLocation)
		return [self formatAddressWithPlacemark:self.placemark];
	return nil;
}

-(NSString *) formatAddressWithPlacemark:(CLPlacemark *) placemark {
	NSString *add = @"";
	if(placemark.name)
		add = [NSString stringWithFormat:@"%@ %@,",add, placemark.name];
 
	//if(placemark.thoroughfare)
	//	add = [NSString stringWithFormat:@"%@ %@",add, placemark.thoroughfare];
	
	//	if(placemark.subThoroughfare)
		//	add = [NSString stringWithFormat:@"%@ %@",add, placemark.subThoroughfare];

	
	if(placemark.locality)
		add = [NSString stringWithFormat:@"%@ %@,",add, placemark.locality];
	
	if(placemark.subLocality){
		add = [NSString stringWithFormat:@"%@ %@,",add, placemark.subLocality];
    NSLog(@"area :%@",add);

  }
	
	if(placemark.administrativeArea)
		add = [NSString stringWithFormat:@"%@ %@,",add, placemark.administrativeArea];
     
	//if(placemark.postalCode)
		//add = [NSString stringWithFormat:@"%@ %@,",add, placemark.postalCode];

	if(placemark.country)
		add = [NSString stringWithFormat:@"%@ %@",add, placemark.country];
	
	return add;
}

#pragma mark ReverseGeo


-(void)addressFromCity:(NSString *)cityName callback:(citygeocoderCallback)callback {
  NSLog(@"%@",cityName);
  CLGeocoder *geocoder1 = [[CLGeocoder alloc]init];
  [geocoder1 geocodeAddressString:cityName completionHandler:^(NSArray *placemarks, NSError *error) {
    if (error == nil && [placemarks count] > 0) {
      NSLog(@"fdgdgd %@",[placemarks lastObject]);
			callback(nil, [placemarks lastObject]);
		}else {
			callback(error, nil);
		}
  }];
}

@end
