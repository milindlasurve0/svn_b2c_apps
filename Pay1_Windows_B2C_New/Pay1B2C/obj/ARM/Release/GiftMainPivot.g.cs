﻿#pragma checksum "F:\Pay1WP8\Pay1B2C\Pay1B2C\GiftMainPivot.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "4D7ED1E1CF3429972610F413ED74DF91"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34014
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Microsoft.Phone.Controls;
using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace Pay1B2C {
    
    
    public partial class GiftMainPivot : Microsoft.Phone.Controls.PhoneApplicationPage {
        
        internal System.Windows.Controls.Canvas canvas;
        
        internal System.Windows.Media.Animation.Storyboard moveAnimation;
        
        internal System.Windows.Controls.StackPanel TitlePanel;
        
        internal System.Windows.Controls.Canvas LayoutRoot;
        
        internal System.Windows.Controls.Grid grdCommands;
        
        internal System.Windows.Controls.Grid ContentPanel;
        
        internal Microsoft.Phone.Controls.Pivot mainGiftPivot;
        
        internal System.Windows.Controls.StackPanel offerPanelMain;
        
        internal Microsoft.Phone.Controls.Pivot infoPivot;
        
        internal System.Windows.Controls.StackPanel offerPanel;
        
        internal System.Windows.Controls.ListBox nearYouListBox;
        
        internal System.Windows.Controls.StackPanel offerPanelCategory;
        
        internal System.Windows.Controls.ListBox myGiftsListBox;
        
        internal System.Windows.Controls.StackPanel emptyViewPanel;
        
        internal System.Windows.Controls.ListBox myLikedGiftsListBox;
        
        internal System.Windows.Controls.StackPanel emptyLikeViewPanel;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/Pay1B2C;component/GiftMainPivot.xaml", System.UriKind.Relative));
            this.canvas = ((System.Windows.Controls.Canvas)(this.FindName("canvas")));
            this.moveAnimation = ((System.Windows.Media.Animation.Storyboard)(this.FindName("moveAnimation")));
            this.TitlePanel = ((System.Windows.Controls.StackPanel)(this.FindName("TitlePanel")));
            this.LayoutRoot = ((System.Windows.Controls.Canvas)(this.FindName("LayoutRoot")));
            this.grdCommands = ((System.Windows.Controls.Grid)(this.FindName("grdCommands")));
            this.ContentPanel = ((System.Windows.Controls.Grid)(this.FindName("ContentPanel")));
            this.mainGiftPivot = ((Microsoft.Phone.Controls.Pivot)(this.FindName("mainGiftPivot")));
            this.offerPanelMain = ((System.Windows.Controls.StackPanel)(this.FindName("offerPanelMain")));
            this.infoPivot = ((Microsoft.Phone.Controls.Pivot)(this.FindName("infoPivot")));
            this.offerPanel = ((System.Windows.Controls.StackPanel)(this.FindName("offerPanel")));
            this.nearYouListBox = ((System.Windows.Controls.ListBox)(this.FindName("nearYouListBox")));
            this.offerPanelCategory = ((System.Windows.Controls.StackPanel)(this.FindName("offerPanelCategory")));
            this.myGiftsListBox = ((System.Windows.Controls.ListBox)(this.FindName("myGiftsListBox")));
            this.emptyViewPanel = ((System.Windows.Controls.StackPanel)(this.FindName("emptyViewPanel")));
            this.myLikedGiftsListBox = ((System.Windows.Controls.ListBox)(this.FindName("myLikedGiftsListBox")));
            this.emptyLikeViewPanel = ((System.Windows.Controls.StackPanel)(this.FindName("emptyLikeViewPanel")));
        }
    }
}

