//
//  ViewController.m
//  Pay1
//
//  Created by Annapurna on 02/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "ViewController.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "PasswordViewController.h"
#import "MainViewController.h"
#import "PrivacyPolicyViewController.h"
#import "TermsAndConditionViewController.h"
#import "CreateUserService.h"
#import "CreateUserResponse.h"
#import "NoNetworkView.h"
#import "Constant.h"
#import <QuartzCore/QuartzCore.h>
#import "DBManager.h"
#import "GetOperatorService.h"
#import "QuickPayResponse.h"
#import "QucickPayList.h"
#import "Session.h"

#define MAX_LENGTH 10


@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    HUD = [[MBProgressHUD alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [self.view addSubview:HUD];
    
    /*if ([Constant getUserPhoneNumber].length > 0 && [Constant getUserPass].length >0 && [Session shared].isFromAutoLogin){
        HUD.labelText = @"Loading...";
        HUD.dimBackground = YES;
        [self showHUDOnView];
    }*/
    
    // [phNumTxtField becomeFirstResponder];
    //phNumTxtField.text  =  @"9975629244"; //@"8285458853"; //@"9975629244"; //@"7053251414";
    
    //PasswordViewController *passwordVCObj = [self.storyboard instantiateViewControllerWithIdentifier:@"passwordViewC"];
   // [self.navigationController pushViewController:passwordVCObj animated:YES];
    
    _scrollView.scrollEnabled = FALSE;
    
    
    UIView *spacerTextView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    [incorrectTextField setLeftViewMode:UITextFieldViewModeAlways];
    [incorrectTextField setLeftView:spacerTextView];
    
   
   
    
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    numberToolbar.barStyle = UIBarStyleDefault;
    numberToolbar.items = [NSArray arrayWithObjects:[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelNumberPad)],[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],[[UIBarButtonItem alloc]initWithTitle:@"GO" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)],nil];
    [numberToolbar sizeToFit];
    phNumTxtField.inputAccessoryView = numberToolbar;
    
    
    [self setDefaultUIProperty];
    NSLog(@"StatusBarHeight :%f",[UIApplication sharedApplication].statusBarFrame.size.height);
    bgImgView.frame = CGRectMake(0, -20, self.view.frame.size.width, self.view.frame.size.height);
    self.scrollView.contentSize = CGSizeMake(bgImgView.frame.size.width, bgImgView.frame.size.height + 20);

    [self removeAllErrorViewFromSuperView];
}

-(void)viewWillAppear:(BOOL)animated{
    
  [self.navigationController setNavigationBarHidden:TRUE];
}


#pragma mark - ViewController's Method

-(void)loginClicked:(id)sender{
    
    [phNumTxtField resignFirstResponder];
    
    
   

    
    if ([phNumTxtField.text length] == 0 || phNumTxtField.text.length < 10 || phNumTxtField.text.length > 10) {
    [self showValidationAlertView:otpErrorImgView];
    [self showValidationAlertView:incorrectTextField];
    return;
    }
    
    
    
    NSString * firstLetter = [phNumTxtField.text substringToIndex:1];

    
    NSLog(@"The Numbe  : %@",firstLetter);
    
    
    if([firstLetter isEqualToString:@"0"] || [firstLetter isEqualToString:@"1"] || [firstLetter isEqualToString:@"2"] || [firstLetter isEqualToString:@"3"] || [firstLetter isEqualToString:@"4"] || [firstLetter isEqualToString:@"5"] || [firstLetter isEqualToString:@"6"] ){
        [self showValidationAlertView:otpErrorImgView];
        [self showValidationAlertView:incorrectTextField];
        
        incorrectTextField.placeholder = @"Please enter correct Mobile number";
        
        return;
    }
    
    
    
    
    else{
        
    [incorrectTextField removeFromSuperview];
    [otpErrorImgView removeFromSuperview];
        
        if(![Constant checkNetworkConnection]){
            
            NSLog(@"#NoNetworkFound");
            
            bgImgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
            bgImgView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.4];
            
            [self.view addSubview:bgImgView];
            
            
            objNoNetworkView = [[NoNetworkView alloc]initWithFrame:CGRectMake(20, self.view.frame.origin.y + self.view.frame.size.height / 2 - 100 , [UIScreen mainScreen].bounds.size.width - 50, 150)];
            [bgImgView addSubview:objNoNetworkView];
            
            phNumTxtField.userInteractionEnabled = FALSE;
            startButton.userInteractionEnabled = FALSE;
            termsButton.userInteractionEnabled = FALSE;
            privacyButton.userInteractionEnabled = FALSE;
            
            UITapGestureRecognizer *tapGes = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(viewTapped:)];
            tapGes.delegate = self;
            tapGes.numberOfTapsRequired = 1;
            [bgImgView addGestureRecognizer:tapGes];
            
            
            [objNoNetworkView setOnRetryClick:^(){
                [bgImgView removeFromSuperview];
                
                phNumTxtField.userInteractionEnabled = TRUE;
                startButton.userInteractionEnabled = TRUE;
                termsButton.userInteractionEnabled = TRUE;
                privacyButton.userInteractionEnabled = TRUE;
                
                [objNoNetworkView removeFromSuperview];
            }];
            
            return;
        }
     
        
        
        
        
        
        
        else{
    HUD.labelText = @"Loading...";
    HUD.dimBackground = YES;
        
    [self showHUDOnView];
        
        
    CreateUserService *objCreateUserService = [[CreateUserService alloc]init];
    [objCreateUserService callCreateUserService:phNumTxtField.text callback:^(NSError *error, CreateUserResponse *objCreateUserResponse) {
    if(error)return ;
    else{
        
    [Constant saveUserPhoneNumber:phNumTxtField.text];

        
    createUserResponseObj = objCreateUserResponse;
    [self callQuickPayService];
    }
    }];
        }
    }
    
    
    /**/
}

-(void)callQuickPayService{
    GetOperatorService *objService = [[GetOperatorService alloc]init];
    [objService callQuickPayService:^(NSError *error, QuickPayResponse *objQuickResponse) {
    if(error)return ;
    else{
    //[self saveQuickPayDataInDB:objQuickResponse];
    [self navigateToPasswordView:createUserResponseObj];

    }
    }];
}

-(void)saveQuickPayDataInDB:(QuickPayResponse *)quickResponse{
    DBManager* objMobDBManager = [[DBManager alloc]initWithDatabaseFilename:@"quickPayDB.sql"];
    NSString *query;
    for(int i=0;i<quickResponse.descriptionArray.count;i++){
    QucickPayList *objQuickList = [quickResponse.descriptionArray objectAtIndex:i];
    NSLog(@"#InsertValues :%@,%d,%@,%d,%d,%d,%d,%@,%d,%@,%@,%@,%@,%@,%d",objQuickList.quickID,[objQuickList.qucikFlag intValue],objQuickList.quickName,[objQuickList.amount intValue],[objQuickList.stv intValue],[objQuickList.operatorID intValue],[objQuickList.product intValue],objQuickList.phNumber,[objQuickList.delegateFlag intValue],objQuickList.missedNumber,objQuickList.operatorName ,objQuickList.operatorCode,[Constant currentSystemDateTime],objQuickList.dateTime,[objQuickList.transactionFlag intValue]);
        
        
    query = [NSString stringWithFormat:@"insert into QuickTable values(null,'%@','%d','%@','%d','%d','%d','%d','%@','%d','%@','%@','%@','%@','%@','%d')",objQuickList.quickID,[objQuickList.qucikFlag intValue],objQuickList.quickName,[objQuickList.amount intValue],[objQuickList.stv intValue],[objQuickList.operatorID intValue],[objQuickList.product intValue],objQuickList.phNumber,[objQuickList.delegateFlag intValue],objQuickList.missedNumber,objQuickList.operatorName ,objQuickList.operatorCode,[Constant currentSystemDateTime],objQuickList.dateTime,[objQuickList.transactionFlag intValue]];
        
        NSLog(@"#InsertQuery :%@",query);
        
        [objMobDBManager executeQuery:query];
        
    }
    
                                                                                                                                                                                          
    if (objMobDBManager.affectedRows != 0) {
        NSLog(@"Query was executed successfully.");
    }
    else{
        NSLog(@"Could not execute the query.");
    }
    
    [self navigateToPasswordView:createUserResponseObj];
}

-(void)navigateToPasswordView:(CreateUserResponse *)userResponse{
    [self hideHUDOnView];
    PasswordViewController *passwordVCObj = [self.storyboard instantiateViewControllerWithIdentifier:@"passwordViewC"];
    passwordVCObj.createUserResponseObj = userResponse;
    passwordVCObj.userPhoneNumber = phNumTxtField.text;
    passwordVCObj.userOTP = userResponse.otp;
    phNumTxtField.text = @"";
    [self.navigationController pushViewController:passwordVCObj animated:YES];
}

-(void)setDefaultUIProperty{
    
    [startButton setBackgroundColor:[UIColor colorWithRed:36/255.0 green:182/255.0 blue:82/255.0 alpha:0.8]];
    startButton.layer.cornerRadius = 5;
    
    
    if([UIScreen mainScreen].bounds.size.height == 480){
    logoImageView.frame = CGRectMake(70, 60, 180, 128);
    phNumTxtField.frame = CGRectMake(60, logoImageView.frame.origin.y + logoImageView.frame.size.height + 80, self.view.frame.size.width - 120, 35);
    thinImageView.frame = CGRectMake(30, phNumTxtField.frame.origin.y + phNumTxtField.frame.size.height + 5, self.view.frame.size.width-70, 1);
    indiaCodeLabel.frame = CGRectMake(26, logoImageView.frame.origin.y + logoImageView.frame.size.height + 85, 32, 21);
    
    termsView.frame = CGRectMake(20, thinImageView.frame.origin.y + thinImageView.frame.size.height + 30, self.view.frame.size.width - 30, 70);
    nextButton.frame = CGRectMake(0, 400, 320, 60);
    }
    
    else if([UIScreen mainScreen].bounds.size.height == 568){
    logoImageView.frame = CGRectMake(70, 120, 180, 128);
    phNumTxtField.frame = CGRectMake(60, logoImageView.frame.origin.y + logoImageView.frame.size.height + 80, self.view.frame.size.width - 120, 35);
    thinImageView.frame = CGRectMake(30, phNumTxtField.frame.origin.y + phNumTxtField.frame.size.height + 5, self.view.frame.size.width-70, 1);
    indiaCodeLabel.frame = CGRectMake(26, logoImageView.frame.origin.y + logoImageView.frame.size.height + 85, 32, 21);
        
        
    termsView.frame = CGRectMake(20, thinImageView.frame.origin.y + thinImageView.frame.size.height + 30, self.view.frame.size.width - 30, 70);
    nextButton.frame = CGRectMake(0, 490, 320, 60);
    }
    else{
        
      //  _scrollView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
     //   nextButton.frame = CGRectMake(0, 600, self.view.frame.size.width, 60);

    }
    
    }

-(void)startClicked:(UIButton *)sender{
    NSLog(@">>>>>>");
}


-(void)viewTapped:(UITapGestureRecognizer *)sender{
    [bgImgView removeFromSuperview];
}

-(IBAction)termsClicked:(id)sender{
    TermsAndConditionViewController *termsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"TermsAndConditionViewController"];
    [self.navigationController pushViewController:termsVC animated:YES];
}

-(IBAction)privacyClicked:(id)sender{
    PrivacyPolicyViewController *privacyVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PrivacyPolicyViewController"];
    [self.navigationController pushViewController:privacyVC animated:YES];
}

-(IBAction)nextClick:(id)sender{
    [self loginClicked:self];
}


-(void)cancelNumberPad{
    _scrollView.scrollEnabled = FALSE;
    [phNumTxtField resignFirstResponder];
    [incorrectTextField removeFromSuperview];
    [otpErrorImgView removeFromSuperview];
    self.scrollView.frame = CGRectMake(0, 20, self.view.frame.size.width, self.view.frame.size.height);


}

-(void)doneWithNumberPad{
    [self loginClicked:self];
    self.scrollView.frame = CGRectMake(0, 20, self.view.frame.size.width, self.view.frame.size.height);
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return TRUE;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    _scrollView.scrollEnabled = TRUE;
    return true;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    
    NSString *editedText = [textField.text stringByReplacingCharactersInRange:range withString:string];
   // NSLog(@"#TextLength :%d %d %d %d",textField.text.length , range.length,editedText.length,MAX_LENGTH);

    if(editedText.length == 0){
    textField.text = @"";
    }
    
    if(incorrectTextField.hidden == FALSE && editedText.length > 0){
    [incorrectTextField removeFromSuperview];
    [otpErrorImgView removeFromSuperview];
    }
    
   else if (editedText.length >= MAX_LENGTH)
    {
        return NO; // return NO to not change text
    }
    
    NSUInteger newLength = [editedText length] + [string length] - range.length;
    return newLength <= MAX_LENGTH + 1;
    
    
  /* else
   {
   return TRUE;
       
   }*/
}

-(void)showHUDOnView{
    [HUD show:YES];
}
-(void)hideHUDOnView{
    [HUD hide:YES];
}

-(void)viewWillDisappear:(BOOL)animated{
    [pageTimer invalidate];
    pageTimer = nil;
}


-(void)removeAllErrorViewFromSuperView{
    
   /* if([UIScreen mainScreen].bounds.size.height == 480){
    otpErrorImgView.frame = CGRectMake(260,thinImageView.frame.origin.y - 25, 20, 20);
    incorrectTextField.frame = CGRectMake(50, thinImageView.frame.origin.y+ thinImageView.frame.size.
                                          height + 2, self.view.frame.size.width - 100, 35);

    }
    
    if([UIScreen mainScreen].bounds.size.height == 568){
   

    }*/
    
    otpErrorImgView.frame = CGRectMake(260, thinImageView.frame.origin.y - 25 , 20, 20);
    incorrectTextField.frame = CGRectMake(50, thinImageView.frame.origin.y+ thinImageView.frame.size.
                                          height + 5, self.view.frame.size.width - 100, 35);
    
    [self setUIForErrorViews:incorrectTextField];
    [otpErrorImgView removeFromSuperview];
    [incorrectTextField removeFromSuperview];
}


-(void)showValidationAlertView:(UIView *)errorAlertView{
    errorAlertView.transform = CGAffineTransformMakeScale(0.01, 0.01);
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
    errorAlertView.transform = CGAffineTransformMakeScale(1.2, 1.2);
    [errorAlertView setAlpha:1.0];
        
    [self.scrollView addSubview:errorAlertView];
    }completion:^(BOOL finished){
        
    }];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if ([UIScreen mainScreen].bounds.size.height == 480) {
    [_scrollView setContentOffset: CGPointMake(0,_scrollView.contentOffset.y)];
    }
    
    if ([UIScreen mainScreen].bounds.size.height  == 568){
    [_scrollView setContentOffset: CGPointMake(0,_scrollView.contentOffset.y)];
    }
}


-(void)setUIForErrorViews:(UIView *)errorView{
    errorView.layer.borderColor = [UIColor redColor].CGColor;
    errorView.layer.borderWidth = 2;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
