package com.pay1.databasehandler;

public class Plan {

	private int planID;
	private String planOpertorID;
	private String planOperatorName;
	private String planCircleID;
	private String planCircleName;
	private String planType;
	private int planAmount;
	private String planValidity;
	private String planDescription;
	private String planUptateTime;
	private int planShowFlag;

	/**
	 * @return the planID
	 */
	public final int getPlanID() {
		return planID;
	}

	/**
	 * @param planID
	 *            the planID to set
	 */
	public final void setPlanID(int planID) {
		this.planID = planID;
	}

	/**
	 * @return the planOpertorID
	 */
	public final String getPlanOpertorID() {
		return planOpertorID;
	}

	/**
	 * @param planOpertorID
	 *            the planOpertorID to set
	 */
	public final void setPlanOpertorID(String planOpertorID) {
		this.planOpertorID = planOpertorID;
	}

	/**
	 * @return the planOperatorName
	 */
	public final String getPlanOperatorName() {
		return planOperatorName;
	}

	/**
	 * @param planOperatorName
	 *            the planOperatorName to set
	 */
	public final void setPlanOperatorName(String planOperatorName) {
		this.planOperatorName = planOperatorName;
	}

	/**
	 * @return the planCircleID
	 */
	public final String getPlanCircleID() {
		return planCircleID;
	}

	/**
	 * @param planCircleID
	 *            the planCircleID to set
	 */
	public final void setPlanCircleID(String planCircleID) {
		this.planCircleID = planCircleID;
	}

	/**
	 * @return the planCircleName
	 */
	public final String getPlanCircleName() {
		return planCircleName;
	}

	/**
	 * @param planCircleName
	 *            the planCircleName to set
	 */
	public final void setPlanCircleName(String planCircleName) {
		this.planCircleName = planCircleName;
	}

	/**
	 * @return the planType
	 */
	public final String getPlanType() {
		return planType;
	}

	/**
	 * @param planType
	 *            the planType to set
	 */
	public final void setPlanType(String planType) {
		this.planType = planType;
	}

	/**
	 * @return the planAmount
	 */
	public final int getPlanAmount() {
		return planAmount;
	}

	/**
	 * @param planAmount
	 *            the planAmount to set
	 */
	public final void setPlanAmount(int planAmount) {
		this.planAmount = planAmount;
	}

	/**
	 * @return the planValidity
	 */
	public final String getPlanValidity() {
		return planValidity;
	}

	/**
	 * @param planValidity
	 *            the planValidity to set
	 */
	public final void setPlanValidity(String planValidity) {
		this.planValidity = planValidity;
	}

	/**
	 * @return the planDescription
	 */
	public final String getPlanDescription() {
		return planDescription;
	}

	/**
	 * @param planDescription
	 *            the planDescription to set
	 */
	public final void setPlanDescription(String planDescription) {
		this.planDescription = planDescription;
	}

	/**
	 * @return the planUptateTime
	 */
	public final String getPlanUptateTime() {
		return planUptateTime;
	}

	/**
	 * @param planUptateTime
	 *            the planUptateTime to set
	 */
	public final void setPlanUptateTime(String planUptateTime) {
		this.planUptateTime = planUptateTime;
	}

	/**
	 * @return the planShowFlag
	 */
	public final int getPlanShowFlag() {
		return planShowFlag;
	}

	/**
	 * @param planShowFlag
	 *            the planShowFlag to set
	 */
	public final void setPlanShowFlag(int planShowFlag) {
		this.planShowFlag = planShowFlag;
	}

}