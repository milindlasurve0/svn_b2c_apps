package com.pay1.utilities;

import java.io.BufferedInputStream;
import java.io.FilterInputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.pay1.constants.Constants;

public class Utility {

	private static String PREFERENCE;

	// private static int MAX_IMAGE_DIMENSION = 720;

	// Preferences for Entertainment

	public static boolean isFirstTime(Context context) {
		SharedPreferences preferences = context.getApplicationContext()
				.getSharedPreferences(" SHARED_PREFERENCES_NAME ",
						android.content.Context.MODE_PRIVATE);
		return preferences.getBoolean(Constants.IS_FIRST_TIME, true);
	}

	public static void saveFirstTime(Context context, boolean value) {
		SharedPreferences preferences = context.getApplicationContext()
				.getSharedPreferences(" SHARED_PREFERENCES_NAME ",
						android.content.Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putBoolean(Constants.IS_FIRST_TIME, value);
		editor.commit();
	}

	public static void setSharedPreference(Context context, String name,
			String value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		// editor.clear();
		editor.putString(name, value);
		editor.commit();
	}

	// Drawable
	public static void setDrawableSharedPreference(Context context,
			String name, int value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		// editor.clear();
		editor.putInt(name, value);
		editor.commit();
	}

	public static String getSharedPreferences(Context context, String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getString(name, null);
	}

	public static long getLongSharedPreferences(Context context, String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getLong(name, 0);
	}

	// for notification count
	public static void setNotificationCount(Context context, String name,
			int value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		// editor.clear();
		editor.putInt(name, value);
		editor.commit();
	}

	public static int getNotificationCount(Context context, String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getInt(name, 0);
	}

	// for chat count
	public static void setChatCount(Context context, String name, int value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		// editor.clear();
		editor.putInt(name, value);
		editor.commit();
	}

	public static int getChatCount(Context context, String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getInt(name, 0);
	}

	public static void setLoginFlag(Context context, String name, boolean value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		// editor.clear();
		editor.putBoolean(name, value);
		editor.commit();
	}

	public static boolean getLoginFlag(Context context, String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getBoolean(name, false);
	}

	public static void setCookieVersion(Context context, String name, int value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putInt(name, value);
		editor.commit();
	}

	public static int getCookieVersion(Context context, String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);

		return settings.getInt(name, 0);
	}

	public static void setCookieName(Context context, String name, String value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(name, value);
		editor.commit();
	}

	public static String getCookieName(Context context, String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);

		return settings.getString(name, null);
	}

	public static void setCookieValue(Context context, String name, String value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(name, value);
		editor.commit();
	}

	public static String getCookieValue(Context context, String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);

		return settings.getString(name, null);
	}

	public static void setCookieDomain(Context context, String name,
			String value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(name, value);
		editor.commit();
	}

	public static String getCookieDomain(Context context, String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);

		return settings.getString(name, null);
	}

	public static void setCookiePath(Context context, String name, String value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(name, value);
		editor.commit();
	}

	public static String getCookiePath(Context context, String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);

		return settings.getString(name, null);
	}

	public static void setCookieExpiry(Context context, String name,
			String value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(name, value);
		editor.commit();
	}

	public static String getCookieExpiry(Context context, String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);

		return settings.getString(name, null);
	}

	public static void setChatFlag(Context context, String name, boolean value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		// editor.clear();
		editor.putBoolean(name, value);
		editor.commit();
	}

	public static boolean getChatFlag(Context context, String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getBoolean(name, false);
	}

	public static void setChatTime(Context context, String name, long value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		// editor.clear();
		editor.putLong(name, value);
		editor.commit();
	}

	public static long getChatTime(Context context, String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getLong(name, 0);
	}

	public static void setCityLastUpdateTime(Context context, String name,
			long value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		// editor.clear();
		editor.putLong(name, value);
		editor.commit();
	}

	public static long getCityLastUpdateTime(Context context, String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getLong(name, 0);
	}

	public static void setUserID(Context context, String name, String value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		// editor.clear();
		editor.putString(name, value);
		editor.commit();
	}

	public static String getUserID(Context context, String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getString(name, null);
	}

	public static void setBalance(Context context, String name, String value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		// editor.clear();
		editor.putString(name, value);
		editor.commit();
	}

	public static String getBalance(Context context, String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getString(name, null);
	}

	public static void setUserName(Context context, String name, String value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		// editor.clear();
		editor.putString(name, value);
		editor.commit();
	}

	public static String getUserName(Context context, String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getString(name, null);
	}

	public static void setMobileNumber(Context context, String name,
			String value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		// editor.clear();
		editor.putString(name, value);
		editor.commit();
	}

	public static String getMobileNumber(Context context, String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getString(name, null);
	}

	public static void setEmail(Context context, String name, String value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		// editor.clear();
		editor.putString(name, value);
		editor.commit();
	}

	public static String getEmail(Context context, String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getString(name, null);
	}

	public static void setGender(Context context, String name, String value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		// editor.clear();
		editor.putString(name, value);
		editor.commit();
	}

	public static String getGender(Context context, String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getString(name, null);
	}

	public static void setDOB(Context context, String name, String value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		// editor.clear();
		editor.putString(name, value);
		editor.commit();
	}

	public static String getDOB(Context context, String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getString(name, null);
	}

	public static void setLatitude(Context context, String name, String value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		// editor.clear();
		editor.putString(name, value);
		editor.commit();
	}

	public static String getLatitude(Context context, String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getString(name, null);
	}

	public static void setLongitude(Context context, String name, String value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		// editor.clear();
		editor.putString(name, value);
		editor.commit();
	}

	public static String getLongitude(Context context, String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getString(name, null);
	}

	public static void setPin(Context context, String name, String value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		// editor.clear();
		editor.putString(name, value);
		editor.commit();
	}

	public static String getPin(Context context, String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getString(name, null);
	}

	public static void setIsNew(Context context, String name, boolean value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		// editor.clear();
		editor.putBoolean(name, value);
		editor.commit();
	}

	public static boolean getIsNew(Context context, String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getBoolean(name, true);
	}

	public static void setCurrentLatitude(Context context, String name,
			String value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		// editor.clear();
		editor.putString(name, value);
		editor.commit();
	}

	public static String getCurrentLatitude(Context context, String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getString(name, null);
	}

	public static void setCurrentLongitude(Context context, String name,
			String value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		// editor.clear();
		editor.putString(name, value);
		editor.commit();
	}

	public static String getCurrentLongitude(Context context, String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getString(name, null);
	}

	public static void setGCMID(Context context, String name, String value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		// editor.clear();
		editor.putString(name, value);
		editor.commit();
	}

	public static String getGCMID(Context context, String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getString(name, null);
	}

	public static void setUUID(Context context, String name, String value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		// editor.clear();
		editor.putString(name, value);
		editor.commit();
	}

	public static String getUUID(Context context, String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getString(name, null);
	}

	public static void setOneTime(Context context, String name, boolean value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		// editor.clear();
		editor.putBoolean(name, value);
		editor.commit();
	}

	public static boolean getOneTime(Context context, String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getBoolean(name, true);
	}

	public static void setSupportNumber(Context context, String name,
			String value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		// editor.clear();
		editor.putString(name, value);
		editor.commit();
	}

	public static String getSupportNumber(Context context, String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getString(name, null);
	}

	public static Bitmap getBitmap(String url) {
		Bitmap imageBitmap = null;
		try {
			URL aURL = new URL(url);
			URLConnection conn = aURL.openConnection();
			conn.connect();
			InputStream is = conn.getInputStream();
			BufferedInputStream bis = new BufferedInputStream(is);
			try {
				imageBitmap = BitmapFactory
						.decodeStream(new FlushedInputStream(is));
			} catch (OutOfMemoryError error) {
			}

			bis.close();
			is.close();
			final int IMAGE_MAX_SIZE = 50;
			// Decode image size
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			int scale = 1;
			while ((o.outWidth * o.outHeight) * (1 / Math.pow(scale, 2)) > IMAGE_MAX_SIZE) {
				scale++;
			}
			if (scale > 1) {
				scale--;
				// scale to max possible inSampleSize that still yields an image
				// larger than target
				o = new BitmapFactory.Options();
				o.inSampleSize = scale;
				// b = BitmapFactory.decodeStream(in, null, o);

				// resize to desired dimensions
				int height = imageBitmap.getHeight();
				int width = imageBitmap.getWidth();

				double y = Math.sqrt(IMAGE_MAX_SIZE
						/ (((double) width) / height));
				double x = (y / height) * width;

				Bitmap scaledBitmap = Bitmap.createScaledBitmap(imageBitmap,
						(int) x, (int) y, true);
				imageBitmap.recycle();
				imageBitmap = scaledBitmap;

				System.gc();
			} else {
				// b = BitmapFactory.decodeStream(in);
			}

		} catch (OutOfMemoryError error) {
		} catch (Exception e) {
		}
		return imageBitmap;
	}

	static class FlushedInputStream extends FilterInputStream {
		public FlushedInputStream(InputStream inputStream) {
			super(inputStream);
		}
	}

	public static void setSharedPreference(Context mContext,
			String contactUpdateTime, long currentTimeMillis) {

		SharedPreferences settings = mContext.getSharedPreferences(PREFERENCE,
				0);
		SharedPreferences.Editor editor = settings.edit();
		// editor.clear();
		editor.putLong(contactUpdateTime, currentTimeMillis);
		editor.commit();
	}

}// final class ends here

