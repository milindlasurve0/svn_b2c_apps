//
//  CustomAlertView.h
//  Pay1
//
//  Created by Annapurna on 13/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomAlertView : UIView{
    IBOutlet UIView *topView;
    IBOutlet UILabel *messageLabel;
    IBOutlet UIButton *doneButton;
    IBOutlet UILabel *topViewLabel;

    
}

@property (nonatomic, copy) void (^onButtonClick)();

-(void)setTextOnCustomView:(NSString *)topViewLabelText messageText:(NSString *)messageLabelText;



@end
