//
//  RechargeTransactionViewController.h
//  Pay1
//
//  Created by Annapurna on 10/07/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "BaseViewController.h"
@class RechargeResponse;
@interface RechargeTransactionViewController : BaseViewController<UIWebViewDelegate,UIAlertViewDelegate>{
    IBOutlet UIWebView *theWebView;
    
    NSString *successString;
    NSString *failureString;
    NSString *cancelString;
    NSString *timeOutString;
    
    NSMutableArray *urlArray;
    
    NSString *flagString;
    NSString *operatorNameString;
    NSString *operatorIDString;
    NSString *rechargeNumberString;
    NSString *operatorCodeString;
    NSString *rechargeAmountString;
    NSString *stvString;
    
    UIView *coverView;
    
    BOOL isMoveBack;
    
    BOOL isTransactionCompleted;
        
}


@property (nonatomic, strong) RechargeResponse *objRechargeResponse;
@property (nonatomic, strong) NSString  *amountToRecharge;
@property (nonatomic, strong) NSString  *numberToRecharge;
@property (nonatomic, strong) NSString  *operatorName;


@property (nonatomic, strong) NSString *typeStr;

@property (nonatomic, assign) BOOL isComingFrom;

@property (nonatomic, assign) BOOL isComingFromGift;

@property (nonatomic, strong) NSString  *postPaidLabelText;








/*
@property (nonatomic, strong) NSString  *operatorSTV;
@property (nonatomic, strong) NSString  *operatorID;
@property (nonatomic, strong) NSString  *operatorFlag;
@property (nonatomic, strong) NSString  *operatorCode;*/




@end
