package com.pay1;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.WeakHashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.SQLException;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.larvalabs.svgandroid.SVGParser;
import com.pay1.adapterhandler.CallListAdapter;
import com.pay1.constants.Constants;
import com.pay1.customviews.GiftImageGalleryLayout;
import com.pay1.customviews.MyProgressDialog;
import com.pay1.databasehandler.FreeBies;
import com.pay1.databasehandler.FreeBiesDataSource;
import com.pay1.requesthandler.RequestClass;
import com.pay1.utilities.MyLocation;
import com.pay1.utilities.MyLocation.LocationResult;
import com.pay1.utilities.RoundedTransformation;
import com.pay1.utilities.Utility;
import com.squareup.picasso.Picasso;

public class MyFreeBiesDetailsActivity extends Activity {

	public static final String DEAL_ID = "id";
	public static int thunbleft;
	public static int thunbtop;

	public static int arrowleft;
	public static int arrowtop;

	private static final String SCREEN_LABEL = "Freebie Details Screen";
	private EasyTracker easyTracker = null;
	ArrayList<HashMap<String, String>> numberWithLocationList;
	private final String TAG = "Deal";
	JSONArray imageArray;
	// private ImageLoader imageLoader;
	private ImageView imageView_Back, imageView_Image, imageView_Locate,
			imageView_Expired, imageView_Photo, imageView_Like,
			imageView_Points, imageView_Rating, imageView_Love,
			imageView_LikeIt, imageView_JustOk, imageView_Poor,
			imageView_ItsBad, img_right_arrow;
	private TextView textView_Title, textView_OfferName,
			textView_OfferDescription, textView_Mobile, textView_Voucher,
			textView_ExpiryDate, textView_DealName, textView_Address,
			textView_Distance, textView_Unit, textView_Points,
			textView_ShopGallery, textView_Brand, textView_BrandDescription,
			textView_Rating, textView_Offer, textView_OfferDetails,
			textView_More, textView_Rate, textView_AddressMore;
	private WebView webView_Details;
	private RelativeLayout relativeLayout_Locate, relativeLayout_Gallery;
	private LinearLayout linearLayout_Main, linearLayout_More;
	private Button button_Call, button_Purchase;

	private View view_ShopGallery, view_sep;

	ImageView img_redeem_now;

	// private ProgressBar progressBar1;
	private ArrayList<WeakHashMap<String, String>> data_location;

	public static final String CONTENT_TYPE = "type";
	public static final String TITLE = "title";
	public static final String CONTENT = "content";
	public static final String DEAL_NAME = "dealname";
	public static final String OFFER_PRICE = "offer_price";
	public static final String CATAGORY = "category";
	public static final String CATAGORY_ID = "category_id";
	public static final String DISTANCE = "distance";
	public static final String ACTUAL_PRICE = "actual_price";
	public static final String DISCOUNT = "discount";
	public static final String TOTAL_STOCK = "total_stock";
	public static final String URL = "img_url";
	public static final String STOCK_SOLD = "stock_sold";
	public static final String LATITUDE = "latitude";
	public static final String LONGITUDE = "longitude";
	public static final String ADDRESS = "address";
	public static final String CITY = "city";
	public static final String STATE = "state";
	public static final String GALARY_URL = "gal_image";
	public static final String GALARY_INFO = "image_info";
	public static final String VOUCHER_CODE = "code";
	public static final String NUMBER = "number";
	Typeface fontAwesome;
	private String deal_ID, mobile = "";
	int offer_ID, amount;

	@SuppressLint("NewApi")
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.gift_deatil_activity);
		try {

			if (!Utility.showGiftTutorial(MyFreeBiesDetailsActivity.this)) {
				// showDailogForFreeBie();

				Intent intent = new Intent(MyFreeBiesDetailsActivity.this,
						GiftTutorialActivity.class);
				startActivity(intent);

			}
			fontAwesome = Typeface.createFromAsset(getAssets(),
					"fontawesome-webfont.ttf");
			// ShowcaseImage();
			img_redeem_now = (ImageView) findViewById(R.id.img_redeem_now);
			img_redeem_now.setImageDrawable(SVGParser.getSVGFromResource(
					getResources(), R.raw.icon_redemption)
					.createPictureDrawable());

			easyTracker = EasyTracker
					.getInstance(MyFreeBiesDetailsActivity.this);
			easyTracker.set(Fields.SCREEN_NAME, SCREEN_LABEL);
			numberWithLocationList = new ArrayList<HashMap<String, String>>();
			Typeface Reguler = Typeface.createFromAsset(getAssets(),
					"EncodeSansNormal-400-Regular.ttf");
			Typeface Medium = Typeface.createFromAsset(getAssets(),
					"EncodeSansNarrow-500-Medium.ttf");
			RelativeLayout back_layout = (RelativeLayout) findViewById(R.id.back_layout);
			back_layout.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onBackPressed();
				}
			});
			imageView_Back = (ImageView) findViewById(R.id.imageView_Back);
			imageView_Back.setImageDrawable(SVGParser.getSVGFromResource(
					getResources(), R.raw.ic_back_old).createPictureDrawable());
			if (Build.VERSION.SDK_INT >= 11)
				imageView_Back.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
			imageView_Back.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onBackPressed();
				}
			});

			webView_Details = (WebView) findViewById(R.id.webView_Details);
			webView_Details.setVisibility(View.GONE);
			webView_Details.setLongClickable(false);

			webView_Details.setBackgroundColor(0x00000000);
			if (Build.VERSION.SDK_INT >= 11)
				webView_Details.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);

			imageView_Image = (ImageView) findViewById(R.id.imageView_Image);
			imageView_Photo = (ImageView) findViewById(R.id.imageView_Photo);
			imageView_Like = (ImageView) findViewById(R.id.imageView_Like);
			imageView_Points = (ImageView) findViewById(R.id.imageView_Points);
			final RelativeLayout mainLayout = (RelativeLayout) findViewById(R.id.parent);
			mainLayout.getViewTreeObserver().addOnGlobalLayoutListener(
					new ViewTreeObserver.OnGlobalLayoutListener() {
						public void onGlobalLayout() {
							// Remove the listener before proceeding
							if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
								mainLayout.getViewTreeObserver()
										.removeOnGlobalLayoutListener(this);
							} else {
								mainLayout.getViewTreeObserver()
										.removeGlobalOnLayoutListener(this);
							}
							thunbleft = getRelativeLeft(imageView_Like);
							thunbtop = getRelativeTop(imageView_Like);
							arrowleft = getRelativeLeft(imageView_Points);
							arrowtop = getRelativeTop(imageView_Points);
							Rect rectf = new Rect();
							imageView_Like.getLocalVisibleRect(rectf);

							Log.d("WIDTH        :",
									String.valueOf(rectf.width()));
							Log.d("HEIGHT       :",
									String.valueOf(rectf.height()));
							Log.d("left         :", String.valueOf(rectf.left));
							Log.d("right        :", String.valueOf(rectf.right));
							Log.d("top          :", String.valueOf(rectf.top));
							Log.d("bottom       :",
									String.valueOf(rectf.bottom));
						}
					});

			imageView_Locate = (ImageView) findViewById(R.id.imageView_Locate);
			imageView_Expired = (ImageView) findViewById(R.id.imageView_Expired);
			imageView_Expired.setVisibility(View.GONE);
			imageView_Rating = (ImageView) findViewById(R.id.imageView_Rating);
			imageView_Rating.setVisibility(View.GONE);
			imageView_Love = (ImageView) findViewById(R.id.imageView_Love);
			imageView_Love.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					try {
						imageView_LikeIt.setTag("0");
						imageView_LikeIt.setImageDrawable(getResources()
								.getDrawable(R.drawable.ic_likeit_unsel));
						imageView_JustOk.setTag("0");
						imageView_JustOk.setImageDrawable(getResources()
								.getDrawable(R.drawable.ic_justok_unsel));
						imageView_Poor.setTag("0");
						imageView_Poor.setImageDrawable(getResources()
								.getDrawable(R.drawable.ic_poor_unsel));
						imageView_ItsBad.setTag("0");
						imageView_ItsBad.setImageDrawable(getResources()
								.getDrawable(R.drawable.ic_itsbad_unsel));
						if (imageView_Love.getTag().equals("1")) {
							imageView_Love.setTag("0");
							imageView_Love.setImageDrawable(getResources()
									.getDrawable(R.drawable.ic_loveit_unsel));
						} else {
							/*
							 * Toast t = Toast.makeText(
							 * MyFreeBiesDetailsActivity.this, "LOVE IT!",
							 * Toast.LENGTH_SHORT); t.setGravity(Gravity.CENTER,
							 * 0, 0); t.show();
							 */

							ShowToastWithImage("LOVE IT!", getResources()
									.getDrawable(R.drawable.ic_loveit_select));

							imageView_Love.setTag("1");
							imageView_Love.setImageDrawable(getResources()
									.getDrawable(R.drawable.ic_loveit_select));
						}
						new RatingTask().execute(String.valueOf(offer_ID), "5");
					} catch (Exception e) {
					}
				}
			});
			imageView_Love
					.setOnLongClickListener(new View.OnLongClickListener() {

						@Override
						public boolean onLongClick(View v) {
							// TODO Auto-generated method stub
							try {
								Toast t = Toast.makeText(
										MyFreeBiesDetailsActivity.this,
										"LOVE IT!", Toast.LENGTH_SHORT);
								t.setGravity(Gravity.CENTER, 0, 0);
								t.show();
							} catch (Exception e) {
							}
							return false;
						}
					});
			imageView_LikeIt = (ImageView) findViewById(R.id.imageView_LikeIt);
			imageView_LikeIt.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					try {
						imageView_Love.setTag("0");
						imageView_Love.setImageDrawable(getResources()
								.getDrawable(R.drawable.ic_loveit_unsel));
						imageView_JustOk.setTag("0");
						imageView_JustOk.setImageDrawable(getResources()
								.getDrawable(R.drawable.ic_justok_unsel));
						imageView_Poor.setTag("0");
						imageView_Poor.setImageDrawable(getResources()
								.getDrawable(R.drawable.ic_poor_unsel));
						imageView_ItsBad.setTag("0");
						imageView_ItsBad.setImageDrawable(getResources()
								.getDrawable(R.drawable.ic_itsbad_unsel));
						if (imageView_LikeIt.getTag().equals("1")) {
							imageView_LikeIt.setTag("0");
							imageView_LikeIt.setImageDrawable(getResources()
									.getDrawable(R.drawable.ic_likeit_unsel));
						} else {
							/*
							 * Toast t = Toast.makeText(
							 * MyFreeBiesDetailsActivity.this, "LIKE IT!",
							 * Toast.LENGTH_SHORT); t.setGravity(Gravity.CENTER,
							 * 0, 0); t.show();
							 */

							ShowToastWithImage("LIKE IT!", getResources()
									.getDrawable(R.drawable.ic_likeit_select));

							imageView_LikeIt.setTag("1");
							imageView_LikeIt.setImageDrawable(getResources()
									.getDrawable(R.drawable.ic_likeit_select));
						}
						new RatingTask().execute(String.valueOf(offer_ID), "4");
					} catch (Exception e) {
					}
				}
			});
			imageView_LikeIt
					.setOnLongClickListener(new View.OnLongClickListener() {

						@Override
						public boolean onLongClick(View v) {
							// TODO Auto-generated method stub
							try {
								Toast t = Toast.makeText(
										MyFreeBiesDetailsActivity.this,
										"LIKE IT!", Toast.LENGTH_SHORT);
								t.setGravity(Gravity.CENTER, 0, 0);
								t.show();
							} catch (Exception e) {
							}
							return false;
						}
					});
			imageView_JustOk = (ImageView) findViewById(R.id.imageView_JustOk);
			imageView_JustOk.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					try {
						imageView_Love.setTag("0");
						imageView_Love.setImageDrawable(getResources()
								.getDrawable(R.drawable.ic_loveit_unsel));
						imageView_LikeIt.setTag("0");
						imageView_LikeIt.setImageDrawable(getResources()
								.getDrawable(R.drawable.ic_likeit_unsel));
						imageView_Poor.setTag("0");
						imageView_Poor.setImageDrawable(getResources()
								.getDrawable(R.drawable.ic_poor_unsel));
						imageView_ItsBad.setTag("0");
						imageView_ItsBad.setImageDrawable(getResources()
								.getDrawable(R.drawable.ic_itsbad_unsel));
						if (imageView_JustOk.getTag().equals("1")) {
							imageView_JustOk.setTag("0");
							imageView_JustOk.setImageDrawable(getResources()
									.getDrawable(R.drawable.ic_justok_unsel));
						} else {
							/*
							 * Toast t = Toast.makeText(
							 * MyFreeBiesDetailsActivity.this, "JUST OK!",
							 * Toast.LENGTH_SHORT); t.setGravity(Gravity.CENTER,
							 * 0, 0); t.show();
							 */

							ShowToastWithImage("JUST OK!", getResources()
									.getDrawable(R.drawable.ic_justok_select));

							imageView_JustOk.setTag("1");
							imageView_JustOk.setImageDrawable(getResources()
									.getDrawable(R.drawable.ic_justok_select));
						}
						new RatingTask().execute(String.valueOf(offer_ID), "3");
					} catch (Exception e) {
					}
				}
			});
			imageView_JustOk
					.setOnLongClickListener(new View.OnLongClickListener() {

						@Override
						public boolean onLongClick(View v) {
							// TODO Auto-generated method stub
							try {
								Toast t = Toast.makeText(
										MyFreeBiesDetailsActivity.this,
										"JUST OK!", Toast.LENGTH_SHORT);
								t.setGravity(Gravity.CENTER, 0, 0);
								t.show();
							} catch (Exception e) {
							}
							return false;
						}
					});
			imageView_Poor = (ImageView) findViewById(R.id.imageView_Poor);
			imageView_Poor.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					try {
						imageView_Love.setTag("0");
						imageView_Love.setImageDrawable(getResources()
								.getDrawable(R.drawable.ic_loveit_unsel));
						imageView_LikeIt.setTag("0");
						imageView_LikeIt.setImageDrawable(getResources()
								.getDrawable(R.drawable.ic_likeit_unsel));
						imageView_JustOk.setTag("0");
						imageView_JustOk.setImageDrawable(getResources()
								.getDrawable(R.drawable.ic_justok_unsel));
						imageView_ItsBad.setTag("0");
						imageView_ItsBad.setImageDrawable(getResources()
								.getDrawable(R.drawable.ic_itsbad_unsel));
						if (imageView_Poor.getTag().equals("1")) {
							imageView_Poor.setTag("0");
							imageView_Poor.setImageDrawable(getResources()
									.getDrawable(R.drawable.ic_poor_unsel));
						} else {
							/*
							 * Toast t = Toast.makeText(
							 * MyFreeBiesDetailsActivity.this, "POOR!",
							 * Toast.LENGTH_SHORT); t.setGravity(Gravity.CENTER,
							 * 0, 0); t.show();
							 */

							ShowToastWithImage("POOR!", getResources()
									.getDrawable(R.drawable.ic_poor_select));
							imageView_Poor.setTag("1");
							imageView_Poor.setImageDrawable(getResources()
									.getDrawable(R.drawable.ic_poor_select));
						}
						new RatingTask().execute(String.valueOf(offer_ID), "2");
					} catch (Exception e) {
					}
				}
			});
			imageView_Poor
					.setOnLongClickListener(new View.OnLongClickListener() {

						@Override
						public boolean onLongClick(View v) {
							// TODO Auto-generated method stub
							try {
								Toast t = Toast.makeText(
										MyFreeBiesDetailsActivity.this,
										"POOR!", Toast.LENGTH_SHORT);
								t.setGravity(Gravity.CENTER, 0, 0);
								t.show();
							} catch (Exception e) {
							}
							return false;
						}
					});
			imageView_ItsBad = (ImageView) findViewById(R.id.imageView_ItsBad);
			imageView_ItsBad.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					try {
						imageView_Love.setTag("0");
						imageView_Love.setImageDrawable(getResources()
								.getDrawable(R.drawable.ic_loveit_unsel));
						imageView_LikeIt.setTag("0");
						imageView_LikeIt.setImageDrawable(getResources()
								.getDrawable(R.drawable.ic_likeit_unsel));
						imageView_JustOk.setTag("0");
						imageView_JustOk.setImageDrawable(getResources()
								.getDrawable(R.drawable.ic_justok_unsel));
						imageView_Poor.setTag("0");
						imageView_Poor.setImageDrawable(getResources()
								.getDrawable(R.drawable.ic_poor_unsel));
						if (imageView_ItsBad.getTag().equals("1")) {
							imageView_ItsBad.setTag("0");
							imageView_ItsBad.setImageDrawable(getResources()
									.getDrawable(R.drawable.ic_itsbad_unsel));
						} else {
							/*
							 * Toast t = Toast.makeText(
							 * MyFreeBiesDetailsActivity.this, "ITS BAD!",
							 * Toast.LENGTH_SHORT); t.setGravity(Gravity.CENTER,
							 * 0, 0); t.show();
							 */

							ShowToastWithImage("ITS BAD!", getResources()
									.getDrawable(R.drawable.ic_itsbad_select));
							imageView_ItsBad.setTag("1");
							imageView_ItsBad.setImageDrawable(getResources()
									.getDrawable(R.drawable.ic_itsbad_select));
						}
						new RatingTask().execute(String.valueOf(offer_ID), "1");
					} catch (Exception e) {
					}
				}
			});
			imageView_ItsBad
					.setOnLongClickListener(new View.OnLongClickListener() {

						@Override
						public boolean onLongClick(View v) {
							// TODO Auto-generated method stub
							try {
								Toast t = Toast.makeText(
										MyFreeBiesDetailsActivity.this,
										"ITS BAD!", Toast.LENGTH_SHORT);
								t.setGravity(Gravity.CENTER, 0, 0);
								t.show();
							} catch (Exception e) {
							}
							return false;
						}
					});
			// imageLoader = new ImageLoader(MyFreeBiesDetailsActivity.this);

			button_Call = (Button) findViewById(R.id.button_Call);
			button_Call.setTypeface(Reguler);

			button_Call.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					// if (!mobile.equalsIgnoreCase("")) {
					/*
					 * Intent callIntent = new Intent(Intent.ACTION_CALL);
					 * callIntent.setData(Uri.parse("tel:" + mobile));
					 * startActivity(callIntent);
					 */
					if (numberWithLocationList.size() >= 1) {
						// Log.d("Area", "Area");
						showDialogForCallList(numberWithLocationList);
					}
					// }
				}
			});

			button_Purchase = (Button) findViewById(R.id.button_Purchase);
			button_Purchase.setTypeface(Reguler);
			button_Purchase.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (button_Purchase.getText().toString().trim()
							.equalsIgnoreCase("Pocket now")) {
						String loyalityPoints = Utility.getBalance(
								MyFreeBiesDetailsActivity.this,
								Constants.SHAREDPREFERENCE_LOYALTY_POINTS);
						String giftPoints = textView_Points.getText()
								.toString();
						int loyalityPointsInt = Integer
								.parseInt(loyalityPoints);
						int giftPointsInt = Integer.parseInt(giftPoints);
						if (giftPointsInt > loyalityPointsInt) {
							showDailogForCoinsBalance(loyalityPoints,
									giftPoints);
						} else {
							showTwoButtonDialog(MyFreeBiesDetailsActivity.this,
									"Confirmation",
									"Do you want to pocket this gift ?", "");
						}
					} else if (button_Purchase.getText().toString().trim()
							.equalsIgnoreCase("Redeem now")) {
						showTwoButtonDialog(MyFreeBiesDetailsActivity.this,
								"Redeem Coupon", "Enter password");
					}
				}
			});

			img_redeem_now.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					showTwoButtonDialog(MyFreeBiesDetailsActivity.this,
							"Redeem Coupon", "Enter password");
				}
			});

			linearLayout_More = (LinearLayout) findViewById(R.id.linearLayout_More);
			linearLayout_More.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (webView_Details.getVisibility() == View.VISIBLE) {
						webView_Details.setVisibility(View.GONE);
						img_right_arrow.setImageDrawable(SVGParser
								.getSVGFromResource(
										MyFreeBiesDetailsActivity.this
												.getResources(),
										R.raw.ic_dwnarrow)
								.createPictureDrawable());
					} else {
						img_right_arrow.setImageDrawable(SVGParser
								.getSVGFromResource(
										MyFreeBiesDetailsActivity.this
												.getResources(),
										R.raw.ic_uparrow)
								.createPictureDrawable());
						webView_Details.setVisibility(View.VISIBLE);
					}

				}
			});
			img_right_arrow = (ImageView) findViewById(R.id.img_right_arrow);
			img_right_arrow.setImageDrawable(SVGParser.getSVGFromResource(
					this.getResources(), R.raw.ic_dwnarrow)
					.createPictureDrawable());

			view_sep = (View) findViewById(R.id.view_sep);

			textView_Rate = (TextView) findViewById(R.id.textView_Rate);
			textView_Rate.setTypeface(Reguler);
			textView_AddressMore = (TextView) findViewById(R.id.textView_AddressMore);
			textView_AddressMore.setTypeface(Reguler);

			textView_More = (TextView) findViewById(R.id.textView_More);
			textView_More.setTypeface(Reguler);

			textView_DealName = (TextView) findViewById(R.id.textView_DealName);
			textView_DealName.setTypeface(Reguler);

			textView_OfferName = (TextView) findViewById(R.id.textView_OfferName);
			textView_OfferName.setTypeface(Reguler);

			textView_OfferDescription = (TextView) findViewById(R.id.textView_OfferDescription);
			textView_OfferDescription.setTypeface(Reguler);

			textView_Distance = (TextView) findViewById(R.id.textView_Distance);
			textView_Distance.setTypeface(Reguler);

			textView_Unit = (TextView) findViewById(R.id.textView_Unit);
			textView_Unit.setTypeface(Reguler);

			textView_Address = (TextView) findViewById(R.id.textView_Address);
			textView_Address.setTypeface(Reguler);

			textView_Mobile = (TextView) findViewById(R.id.textView_Mobile);
			textView_Mobile.setTypeface(Reguler);

			textView_Voucher = (TextView) findViewById(R.id.textView_Voucher);
			textView_Voucher.setTypeface(Reguler);

			textView_ExpiryDate = (TextView) findViewById(R.id.textView_ExpiryDate);
			textView_ExpiryDate.setTypeface(Reguler);

			textView_Points = (TextView) findViewById(R.id.textView_Points);
			textView_Points.setTypeface(Reguler);

			textView_ShopGallery = (TextView) findViewById(R.id.textView_ShopGallery);
			textView_ShopGallery.setTypeface(Reguler);
			view_ShopGallery = (View) findViewById(R.id.view_ShopGallery);

			linearLayout_Main = (LinearLayout) findViewById(R.id.linearLayout_Main);

			relativeLayout_Gallery = (RelativeLayout) findViewById(R.id.relativeLayout_Gallery);

			textView_Brand = (TextView) findViewById(R.id.textView_Brand);
			textView_Brand.setTypeface(Reguler);

			textView_BrandDescription = (TextView) findViewById(R.id.textView_BrandDescription);
			textView_BrandDescription.setTypeface(Reguler);

			textView_Rating = (TextView) findViewById(R.id.textView_Rating);

			textView_Rating.setTypeface(Reguler);
			textView_Rating.setVisibility(View.GONE);

			textView_Offer = (TextView) findViewById(R.id.textView_Offer);
			textView_Offer.setTypeface(Reguler);

			textView_OfferDetails = (TextView) findViewById(R.id.textView_OfferDetails);
			textView_OfferDetails.setTypeface(Reguler);

			relativeLayout_Locate = (RelativeLayout) findViewById(R.id.relativeLayout_Locate);
			relativeLayout_Locate.setVisibility(View.GONE);
			relativeLayout_Locate
					.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub

							boolean gps_enabled = false;
							boolean network_enabled = false;
							LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
							try {
								gps_enabled = lm
										.isProviderEnabled(LocationManager.GPS_PROVIDER);
							} catch (Exception ex) {
							}
							try {
								network_enabled = lm
										.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
							} catch (Exception ex) {
							}

							// don't start listeners if no provider is
							// enabled
							boolean hasGPS = getPackageManager()
									.hasSystemFeature(
											PackageManager.FEATURE_LOCATION_GPS);
							WifiManager wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
							if (!(network_enabled && wifi.isWifiEnabled())
									&& (hasGPS && !gps_enabled)) {

								showTwoButtonDialog(
										MyFreeBiesDetailsActivity.this,
										"Shop Locator",
										"Turn on your location service on the phone to locate "
												+ textView_DealName.getText()
														.toString().trim()
												+ ".",
										Constants.DIALOG_CLOSE_LOCATION);
							} else if (gps_enabled || network_enabled) {
								boolean show_route = false;

								if (wifi.isWifiEnabled())
									show_route = true;
								else if (network_enabled && gps_enabled)
									show_route = true;
								else if (network_enabled)
									show_route = true;
								try {

									ArrayList<WeakHashMap<String, String>> temp = new ArrayList<WeakHashMap<String, String>>();

									for (int j = 0; j < data_location.size(); j++) {
										WeakHashMap<String, String> map1 = new WeakHashMap<String, String>();
										WeakHashMap<String, String> map = new WeakHashMap<String, String>(
												data_location.get(j));
										if (Integer.parseInt(map.get(DEAL_ID)) == Integer
												.parseInt(deal_ID)) {
											map1.put(LATITUDE,
													map.get(LATITUDE));
											map1.put(LONGITUDE,
													map.get(LONGITUDE));
											map1.put(ADDRESS, map.get(ADDRESS));
											map1.put(CITY, map.get(CITY));
											map1.put(STATE, map.get(STATE));

											temp.add(map1);
										}
									}
									if (temp.size() > 1) {
										Intent intent = new Intent(
												MyFreeBiesDetailsActivity.this,
												FreeBiesLocationActivity.class);
										intent.putExtra("ROUTE", show_route);
										intent.putExtra("DEAL",
												textView_DealName.getText()
														.toString().trim());
										intent.putExtra("ID", deal_ID);
										intent.putExtra("FROM_DETAILS", true);
										intent.putExtra("LOCATIONS",
												data_location);
										startActivity(intent);
									} else {
										Intent intent = new Intent(
												MyFreeBiesDetailsActivity.this,
												FreeBiesLocatorActivity.class);
										WeakHashMap<String, String> map = new WeakHashMap<String, String>(
												temp.get(0));
										intent.putExtra("ROUTE", show_route);
										intent.putExtra("DEAL",
												textView_DealName.getText()
														.toString().trim());
										intent.putExtra("LATITUDE", map
												.get(FreeBiesActivity.LATITUDE));
										intent.putExtra(
												"LONGITUDE",
												map.get(FreeBiesActivity.LONGITUDE));
										intent.putExtra("ADDRESS", map
												.get(FreeBiesActivity.ADDRESS));
										intent.putExtra("CITY",
												map.get(FreeBiesActivity.CITY));
										intent.putExtra("STATE",
												map.get(FreeBiesActivity.STATE));
										startActivity(intent);
									}
								} catch (Exception e) {
									Intent intent = new Intent(
											MyFreeBiesDetailsActivity.this,
											FreeBiesLocationActivity.class);
									intent.putExtra("ROUTE", show_route);
									intent.putExtra("DEAL", textView_DealName
											.getText().toString().trim());
									intent.putExtra("ID", deal_ID);
									intent.putExtra("FROM_DETAILS", true);
									intent.putExtra("LOCATIONS", data_location);
									startActivity(intent);
								}
							} else {
								showTwoButtonDialog(
										MyFreeBiesDetailsActivity.this,
										"Shop Locator",
										"Turn on your location service on the phone to locate "
												+ textView_DealName.getText()
														.toString().trim()
												+ ".",
										Constants.DIALOG_CLOSE_LOCATION);
							}

						}
					});

			data_location = new ArrayList<WeakHashMap<String, String>>();

			textView_Title = (TextView) findViewById(R.id.textView_Title);
			textView_Title.setTypeface(Medium);
			// progressBar1 = (ProgressBar) findViewById(R.id.progressBar1);

			Bundle bundle = getIntent().getExtras();
			if (bundle != null) {
				final FreeBies freeBie = (FreeBies) getIntent()
						.getSerializableExtra("GIFT");
				offer_ID = freeBie.getFreebieOfferID();
				amount = freeBie.getFreebieMinAmount();
				mobile = freeBie.getFreebieDealerMobile();

				if (mobile.equalsIgnoreCase("")) {
					button_Call.setVisibility(View.GONE);
					button_Call.setBackgroundColor(Color.GRAY);
				}
				// imageLoader.getDealImage(bundle.getString("URL"),
				// imageView_Image, progressBar1);
				try {
					Picasso.with(MyFreeBiesDetailsActivity.this)
							.load(freeBie.getFreebieURL())
							.placeholder(R.drawable.deal_default).fit()
							.into(imageView_Image);
				} catch (Exception e) {
				}
				try {
					Picasso.with(MyFreeBiesDetailsActivity.this)
							.load(freeBie.getFreebieLogoURL())
							.transform(new RoundedTransformation(35, 0))
							.placeholder(R.drawable.deal_icon)
							.into(imageView_Photo);
				} catch (Exception e) {
				}

				try {
					Picasso.with(MyFreeBiesDetailsActivity.this)
							.load("https://maps.googleapis.com/maps/api/staticmap?center="
									+ freeBie.getFreebieLat()
									+ ","
									+ freeBie.getFreebieLng()
									+ "&zoom=13&size=200x150&maptype=roadmap&markers=color:red%7Clabel:A%7C"
									+ freeBie.getFreebieLat()
									+ ","
									+ freeBie.getFreebieLng() + "&format=gif")
							.fit().into(imageView_Locate);
				} catch (Exception e) {
				}
				if (freeBie.getFreebieLike() == 0)
					imageView_Like.setImageResource(R.drawable.ic_unlike);
				else
					imageView_Like.setImageResource(R.drawable.ic_like);
				imageView_Like.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if (freeBie.getFreebieLike() == 0) {
							imageView_Like.setImageResource(R.drawable.ic_like);
							freeBie.setFreebieLike(1);
						} else {
							imageView_Like
									.setImageResource(R.drawable.ic_unlike);
							freeBie.setFreebieLike(0);
						}
						new LikeTask().execute(offer_ID + "");
					}
				});
				textView_Title.setText(freeBie.getFreebieDealName());
				textView_OfferName.setText(freeBie.getFreebieOfferName());
				textView_OfferDescription
						.setText(freeBie.getFreebieShortDesc());
				textView_Points.setText(String.valueOf(freeBie
						.getFreebieMinAmount()));

				if (bundle.getBoolean("MY")) {
					textView_Mobile.setVisibility(View.VISIBLE);
					textView_Voucher.setVisibility(View.VISIBLE);
					textView_ExpiryDate.setVisibility(View.VISIBLE);
					view_sep.setVisibility(View.VISIBLE);
					imageView_Like.setVisibility(View.GONE);

					String str = Utility.getMobileNumber(
							MyFreeBiesDetailsActivity.this,
							Constants.SHAREDPREFERENCE_MOBILE);
					try {
						String temp = str.substring(0, 6);
						textView_Mobile.setText("Mobile number: "
								+ str.replaceAll(temp, "XXXXXX"));
					} catch (Exception e) {
						textView_Mobile.setText("Mobile number: " + str);
					}
					textView_Voucher.setText("Voucher code: "
							+ freeBie.getFreebieCode());

					textView_ExpiryDate.setText("Expiry date: "
							+ freeBie.getFreebieExpiry());
					button_Purchase.setText("REDEEM NOW");

					button_Purchase.setText("In your pocket");
					img_redeem_now.setVisibility(View.VISIBLE);

					button_Purchase.setBackground(getResources().getDrawable(
							R.drawable.gray_pocket_selector));
					button_Purchase.setEnabled(false);
					imageView_Expired.setVisibility(View.VISIBLE);
					imageView_Expired.setImageResource(R.drawable.ic_pocketed);
					if (freeBie.getFreebieCouponStatusID() == 3) {
						imageView_Expired.setVisibility(View.VISIBLE);
						img_redeem_now.setVisibility(View.GONE);

						imageView_Expired
								.setImageResource(R.drawable.ic_expired);
					} else if (freeBie.getFreebieCouponStatusID() == 2) {
						button_Purchase.setVisibility(View.INVISIBLE);
						imageView_Expired.setVisibility(View.VISIBLE);
						img_redeem_now.setVisibility(View.GONE);
						imageView_Expired
								.setImageResource(R.drawable.ic_redeemed);
					}
				} else {
					textView_Mobile.setVisibility(View.GONE);
					textView_Voucher.setVisibility(View.GONE);
					textView_ExpiryDate.setVisibility(View.GONE);
					view_sep.setVisibility(View.GONE);
					imageView_Like.setVisibility(View.VISIBLE);
					button_Purchase.setText("POCKET NOW");
					/*
					 * button_Purchase.setBackgroundColor(getResources().getColor
					 * ( R.color.blue));
					 */

					button_Purchase.setBackground(getResources().getDrawable(
							R.drawable.pocket_button_selector));
				}

				deal_ID = String.valueOf(freeBie.getFreebieDealID());
				new DealDetailsTask().execute(deal_ID, Utility
						.getCurrentLatitude(MyFreeBiesDetailsActivity.this,
								Constants.SHAREDPREFERENCE_CURRENT_LATITUDE),
						Utility.getCurrentLongitude(
								MyFreeBiesDetailsActivity.this,
								Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE));

			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}

	protected void ShowToastWithImage(String msg, Drawable drawable) {
		// TODO Auto-generated method stub
		View toastView = getLayoutInflater().inflate(R.layout.toast,
				(ViewGroup) findViewById(R.id.toastLayout));
		ImageView imageView = (ImageView) toastView.findViewById(R.id.image);

		imageView.setImageDrawable(drawable);
		// imageView.setBackgroundDrawable(bitmapDrawable);

		TextView textView = (TextView) toastView.findViewById(R.id.text);

		textView.setText(msg);
		Toast toast = new Toast(MyFreeBiesDetailsActivity.this);

		toast.setGravity(Gravity.CENTER_VERTICAL, 0, -100);
		toast.setDuration(200);
		toast.setView(toastView);

		toast.show();
	}

	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView().build());
		// EasyTracker.getInstance(this).activityStart(this);
		easyTracker.send(MapBuilder.createAppView().build());
	}

	@Override
	public void onStop() {
		super.onStop();
		// EasyTracker.getInstance(this).activityStop(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		// Constants.showNavigationBar(DealDetailsActivity.this);
		try {
			LocationResult locationResult = new LocationResult() {
				@Override
				public void gotLocation(Location location) {
					// Got the location!
					try {
						// //// // Log.w("Location", location + "");
						if (location != null) {
							location.getLongitude();
							location.getLatitude();

							Utility.setCurrentLatitude(
									MyFreeBiesDetailsActivity.this,
									Constants.SHAREDPREFERENCE_CURRENT_LATITUDE,
									String.valueOf(location.getLatitude()));
							Utility.setCurrentLongitude(
									MyFreeBiesDetailsActivity.this,
									Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE,
									String.valueOf(location.getLongitude()));
						}
					} catch (Exception exception) {
					}
				}
			};
			MyLocation myLocation = new MyLocation();
			myLocation.getLocation(this, locationResult);
		} catch (Exception exception) {

		}
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
	}

	public class DealDetailsTask extends AsyncTask<String, String, String>
			implements OnDismissListener {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			try {
				String response = RequestClass.getInstance()
						.readPay1B2CRequest(
								MyFreeBiesDetailsActivity.this,
								Constants.B2C_URL + "get_deal_details/?id="
										+ params[0] + "&latitude=" + params[1]
										+ "&longitude=" + params[2]
										+ "&api_version=3");

				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			try {

				if (!result.startsWith("Error")) {
					String replaced = result;

					JSONObject jsonObject = new JSONObject(replaced);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {

						JSONObject jsonObject_Desc = jsonObject
								.getJSONObject("description");

						JSONArray jsonArrayDetails = jsonObject_Desc
								.getJSONArray("deal_detail");

						for (int i = 0; i < jsonArrayDetails.length(); i++) {
							JSONObject jsonObjectInner = jsonArrayDetails
									.getJSONObject(i);

							String str = jsonObjectInner.getString(
									"content_txt").trim();
							webView_Details
									.loadDataWithBaseURL(
											null,
											"<html><head><style>@font-face {font-family: Normal;src: url(\"file:///android_asset/EncodeSansNormal-400-Regular.ttf\")}"
													+ "@font-face {font-family: Narrow;src: url(\"file:///android_asset/EncodeSansNarrow-500-Medium.ttf\")}"
													+ "body {margin: 0px 5px;padding:0px;color:#000000;}"
													+ "h1{font-size:14px;font-family:\"Normal\";margin:0;padding:10px 0 5px 0;color:#000000;}"
													+ "h2{font-size:14px;font-family:\"Normal\";margin:0;padding:10px 0 5px 0;color:#000000;}"
													+ "ul{margin:0 0 10px 0;padding-left:20px;}"
													+ "li{font-size:12px;margin:0 0 2px 0;font-family:\"Normal\";}</style></head><body>"
													+ str + "</body></html>",
											"text/html", "UTF-8", null);

						}

						String des = jsonObject_Desc
								.getString("brand_description");
						textView_BrandDescription.setText(des);
						if (des.equalsIgnoreCase("")) {
							textView_Brand.setVisibility(View.GONE);
							textView_BrandDescription.setVisibility(View.GONE);
						}

						data_location.clear();
						numberWithLocationList.clear();
						JSONArray jsonArrayLocation = jsonObject_Desc
								.getJSONArray("location_detail");

						for (int i = 0; i < jsonArrayLocation.length(); i++) {
							JSONObject jsonObjectInner = jsonArrayLocation
									.getJSONObject(i);
							WeakHashMap<String, String> map = new WeakHashMap<String, String>();
							map.put(DEAL_ID,
									jsonObjectInner.getString("deal_id"));
							double latitude = Double
									.parseDouble(jsonObjectInner
											.getString("lat"));
							double longitude = Double
									.parseDouble(jsonObjectInner
											.getString("lng"));
							map.put(LATITUDE, String.valueOf(latitude));
							map.put(LONGITUDE, String.valueOf(longitude));
							map.put(ADDRESS,
									jsonObjectInner.getString("address"));
							map.put(CITY, jsonObjectInner.getString("city"));
							map.put(STATE, jsonObjectInner.getString("state"));
							map.put(NUMBER,
									jsonObjectInner.getString("dealer_contact"));
							HashMap<String, String> numberLocationMap = new HashMap<String, String>();
							numberLocationMap
									.put("number", jsonObjectInner
											.getString("dealer_contact"));
							numberLocationMap.put("area",
									jsonObjectInner.getString("area"));
							numberLocationMap.put("city",
									jsonObjectInner.getString("city"));
							numberLocationMap.put("address",
									jsonObjectInner.getString("address"));

							if (jsonObjectInner.getString("dealer_contact")
									.isEmpty()
									&& jsonObjectInner.getString(
											"dealer_contact").length() == 0) {

							} else {
								numberWithLocationList.add(numberLocationMap);
							}
							if (numberWithLocationList.size() >= 1) {
								button_Call.setVisibility(View.VISIBLE);
								button_Call.setEnabled(true);
							} else {
								button_Call.setVisibility(View.VISIBLE);
								button_Call.setEnabled(false);
							}
							data_location.add(map);

							if (i == 0) {
								textView_DealName.setText(MyFreeBiesDetailsActivity.this.textView_Title.getText().toString());
								textView_Address.setText(jsonObjectInner
										.getString("address")
										+ ", "
										+ jsonObjectInner.getString("area")
										+ ", "
										+ jsonObjectInner.getString("city"));
								double dis = 0;
								DecimalFormat decimalFormat = new DecimalFormat(
										"0.0");
								try {
									String lat = Utility
											.getCurrentLatitude(
													MyFreeBiesDetailsActivity.this,
													Constants.SHAREDPREFERENCE_CURRENT_LATITUDE);
									String lng = Utility
											.getCurrentLongitude(
													MyFreeBiesDetailsActivity.this,
													Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE);
									String dist = "0";
									if (lat != "" && lng != "")
										dist = String
												.valueOf(Constants.distanceFrom(
														Double.parseDouble(lat),
														Double.parseDouble(lng),
														latitude, longitude));

									dis = Double.parseDouble(dist);
									if (dis < 1) {
										dis = dis * 1000;
										textView_Distance.setText(decimalFormat
												.format(dis));
										textView_Unit.setText("(M)");
									} else {
										textView_Distance.setText(decimalFormat
												.format(dis));
										textView_Unit.setText("(KM)");
									}

								} catch (Exception e) {
								}
								if (jsonArrayLocation.length() > 1) {
									textView_AddressMore
											.setVisibility(View.VISIBLE);
									textView_AddressMore
											.setText(jsonArrayLocation.length()
													- 1 + "  more locations");
								} else {
									textView_AddressMore
											.setVisibility(View.GONE);
								}
							}
						}

						relativeLayout_Locate.setVisibility(View.VISIBLE);

						// mobile = jsonObject_Desc.getString("dealer_contact");
						/*
						 * if (mobile.equalsIgnoreCase("")) {
						 * 
						 * button_Call.setBackgroundColor(Color.GRAY); }
						 */
						JSONArray jsonArrayOfferDeatils = jsonObject_Desc
								.getJSONArray("offer_detail");
						for (int i = 0; i < jsonArrayOfferDeatils.length(); i++) {
							int rating = 0;
							try {
								rating = Integer.parseInt(jsonArrayOfferDeatils
										.getJSONObject(i).getString("off_avg"));
								switch (rating) {
								case 1:
									imageView_Rating
											.setImageResource(R.drawable.ic_itsbad_select);
									break;
								case 2:
									imageView_Rating
											.setImageResource(R.drawable.ic_poor_select);
									break;
								case 3:
									imageView_Rating
											.setImageResource(R.drawable.ic_justok_select);
									break;
								case 4:
									imageView_Rating
											.setImageResource(R.drawable.ic_likeit_select);
									break;
								case 5:
									imageView_Rating
											.setImageResource(R.drawable.ic_loveit_select);
									break;
								default:
									imageView_Rating.setVisibility(View.GONE);
									textView_Rating.setVisibility(View.GONE);
									break;
								}
							} catch (Exception e) {
							}

							int review = 0;
							try {
								review = Integer.parseInt(jsonArrayOfferDeatils
										.getJSONObject(i).getString("review"));
								imageView_ItsBad.setTag("0");
								imageView_Poor.setTag("0");
								imageView_JustOk.setTag("0");
								imageView_LikeIt.setTag("0");
								imageView_Love.setTag("0");
								switch (review) {
								case 1:
									imageView_ItsBad.setTag("1");
									imageView_ItsBad
											.setImageResource(R.drawable.ic_itsbad_select);
									break;
								case 2:
									imageView_Poor.setTag("1");
									imageView_Poor
											.setImageResource(R.drawable.ic_poor_select);
									break;
								case 3:
									imageView_JustOk.setTag("1");
									imageView_JustOk
											.setImageResource(R.drawable.ic_justok_select);
									break;
								case 4:
									imageView_LikeIt.setTag("1");
									imageView_LikeIt
											.setImageResource(R.drawable.ic_likeit_select);
									break;
								case 5:
									imageView_Love.setTag("1");
									imageView_Love
											.setImageResource(R.drawable.ic_loveit_select);
									break;
								default:
									imageView_ItsBad.setTag("0");
									imageView_Poor.setTag("0");
									imageView_JustOk.setTag("0");
									imageView_LikeIt.setTag("0");
									imageView_Love.setTag("0");
									break;
								}
							} catch (Exception e) {
							}
							if (rating != 0) {
								textView_Rating.setText(String.valueOf(rating));
								imageView_Rating.setVisibility(View.VISIBLE);
								textView_Rating.setVisibility(View.VISIBLE);
							}
						}
						final JSONArray jsonArrayImage = jsonObject_Desc
								.getJSONArray("image_list");
						imageArray = jsonArrayImage;
						if (jsonArrayImage.length() != 0) {
							textView_ShopGallery.setVisibility(View.VISIBLE);
							linearLayout_Main.setVisibility(View.VISIBLE);
							// linearLayout_Main.setBackgroundColor(getResources().getColor(R.color.RosyBrown));
							view_ShopGallery.setVisibility(View.VISIBLE);

							HorizontalScrollView hs = new HorizontalScrollView(
									MyFreeBiesDetailsActivity.this);
							GiftImageGalleryLayout giftCell = new GiftImageGalleryLayout(
									MyFreeBiesDetailsActivity.this);

							hs.addView(giftCell);
							linearLayout_Main.addView(hs);

							for (int i = 0; i < jsonArrayImage.length(); i++)
								giftCell.add(jsonArrayImage.getString(i));

						} else {
							textView_ShopGallery.setVisibility(View.GONE);
							linearLayout_Main.setVisibility(View.GONE);
							view_ShopGallery.setVisibility(View.GONE);

						}

						JSONObject jsonObjectOfferInfo = jsonObject_Desc
								.getJSONObject("offer_info");

						StringBuffer sb = new StringBuffer();
						String bullet = getResources().getString(
								R.string.bullet);
						String time = "", day = "", appointment = "", exclusive_gender = "";
						if (jsonObjectOfferInfo.getString("time").isEmpty()) {

						} else {
							time = bullet + " "
									+ jsonObjectOfferInfo.getString("time");
						}

						if (jsonObjectOfferInfo.getString("day").isEmpty()) {

						} else {
							day = bullet + " "
									+ jsonObjectOfferInfo.getString("day");
						}

						if (jsonObjectOfferInfo.getString("appointment")
								.isEmpty()) {

						} else {
							appointment = bullet
									+ " "
									+ jsonObjectOfferInfo
											.getString("appointment");
						}

						if (jsonObjectOfferInfo.getString("exclusive_gender")
								.isEmpty()) {

						} else {
							exclusive_gender = bullet
									+ " "
									+ jsonObjectOfferInfo
											.getString("exclusive_gender");
						}

						sb.append(time).append("\n").append(day).append("\n")
								.append(appointment).append("\n")
								.append(exclusive_gender);

						/*
						 * String htmlDesc="<html><body>";
						 * 
						 * htmlDesc+="<ul style='list-style-type: circle'>";
						 * 
						 * htmlDesc+="<ol>";
						 * 
						 * htmlDesc+="hkaHKDHAKSD";
						 * 
						 * htmlDesc+="</ol>";
						 * 
						 * htmlDesc+="</ul>";
						 * 
						 * htmlDesc+="</body></html>";
						 */

						// String.valueOf(sb)
						textView_OfferDetails.setTextSize(14);
						// textView_OfferDetails.setTypeface(fontAwesome);
						textView_OfferDetails.setText(String.valueOf(sb));

						boolean gps_enabled = false;
						boolean network_enabled = false;
						LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
						try {
							gps_enabled = lm
									.isProviderEnabled(LocationManager.GPS_PROVIDER);
						} catch (Exception ex) {
						}

						try {
							network_enabled = lm
									.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
						} catch (Exception ex) {
						}
						// don't start listeners if no provider is
						// enabled
						boolean hasGPS = getPackageManager().hasSystemFeature(
								PackageManager.FEATURE_LOCATION_GPS);
						WifiManager wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);

						if (!(network_enabled && wifi.isWifiEnabled())
								&& (hasGPS && !gps_enabled)) {
							textView_Distance.setText("");
							textView_Unit.setText("");
							textView_Distance.setVisibility(View.INVISIBLE);
							textView_Unit.setVisibility(View.INVISIBLE);
						}
					} else {
						// Constants.showOneButtonDialog(
						// MyFreeBiesDetailsActivity.this, TAG,
						// Constants.checkCode(replaced),
						// Constants.DIALOG_CLOSE);
					}

				} else {
					// Intent intent = new
					// Intent(MyFreeBiesDetailsActivity.this,
					// ConnectivityErrorActivity.class);
					// intent.putExtra(Constants.RECHARGE_FOR,
					// Constants.SUBSCRIBE_DEAL);
					// startActivity(intent);

				}
			} catch (JSONException e) {
				// e.printStackTrace();
				// Constants.showOneButtonDialog(MyFreeBiesDetailsActivity.this,
				// TAG, Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
				// Constants.DIALOG_CLOSE);
			} catch (Exception e) {
				// TODO: handle exception
				// Constants.showOneButtonDialog(MyFreeBiesDetailsActivity.this,
				// TAG, Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
				// Constants.DIALOG_CLOSE);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(MyFreeBiesDetailsActivity.this);
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							dialog.dismiss();
							DealDetailsTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			dialog.dismiss();
			DealDetailsTask.this.cancel(true);
		}

	}

	public void editActions(View v) {
		Toast.makeText(this, "Layout", Toast.LENGTH_SHORT).show();
	}

	public void showTwoButtonDialog(final Context context, String Title,
			String message, String s) {
		try {

			final Dialog dialog = new Dialog(context);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.getWindow().setBackgroundDrawableResource(
					android.R.color.transparent);
			dialog.setContentView(R.layout.dialog_two_button);
			dialog.setCancelable(false);
			dialog.setCanceledOnTouchOutside(false);
			// dialog.setTitle(null);

			Typeface Reguler = Typeface.createFromAsset(context.getAssets(),
					"EncodeSansNormal-400-Regular.ttf");

			Typeface Medium = Typeface.createFromAsset(context.getAssets(),
					"EncodeSansNarrow-500-Medium.ttf");

			// set the custom dialog components - text, image and button
			TextView textView_Title = (TextView) dialog
					.findViewById(R.id.textView_Title);
			textView_Title.setText(Title);
			textView_Title.setTypeface(Reguler);

			TextView textView_Message = (TextView) dialog
					.findViewById(R.id.textView_Message);
			textView_Message.setText(message);
			textView_Message.setTypeface(Medium);

			Button button_Ok = (Button) dialog.findViewById(R.id.button_Ok);
			button_Ok.setTypeface(Reguler);
			button_Ok.setText("CONFIRM");
			// if button is clicked, close the custom dialog
			button_Ok.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
					// if(amount<Utility.get)
					String loyalityPoints = Utility.getBalance(context,
							Constants.SHAREDPREFERENCE_LOYALTY_POINTS);
					String giftPoints = textView_Points.getText().toString();
					int loyalityPointsInt = Integer.parseInt(loyalityPoints);
					int giftPointsInt = Integer.parseInt(giftPoints);

					if (giftPointsInt > loyalityPointsInt) {
						showDailogForCoinsBalance(loyalityPoints, giftPoints);
					} else {
						new PurchaseDealTask().execute(deal_ID,
								String.valueOf(offer_ID),
								String.valueOf(amount));
					}
				}
			});

			Button button_Cancel = (Button) dialog
					.findViewById(R.id.button_Cancel);
			button_Cancel.setTypeface(Reguler);
			button_Cancel.setText("LATER");
			// if button is clicked, close the custom dialog
			button_Cancel.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
				}
			});

			dialog.show();

		} catch (Exception exception) {
		}
	}

	public void showTwoButtonDialog(final Context context, String Title,
			String message, final int Type) {
		try {

			final Dialog dialog = new Dialog(context);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.getWindow().setBackgroundDrawableResource(
					android.R.color.transparent);
			dialog.setContentView(R.layout.dialog_two_button);
			dialog.setCancelable(false);
			dialog.setCanceledOnTouchOutside(false);
			// dialog.setTitle(null);

			Typeface Reguler = Typeface.createFromAsset(context.getAssets(),
					"EncodeSansNormal-400-Regular.ttf");

			Typeface Medium = Typeface.createFromAsset(context.getAssets(),
					"EncodeSansNarrow-500-Medium.ttf");

			// set the custom dialog components - text, image and button
			TextView textView_Title = (TextView) dialog
					.findViewById(R.id.textView_Title);
			textView_Title.setText(Title);
			textView_Title.setTypeface(Reguler);

			TextView textView_Message = (TextView) dialog
					.findViewById(R.id.textView_Message);
			textView_Message.setText(message);
			textView_Message.setTypeface(Medium);

			Button button_Ok = (Button) dialog.findViewById(R.id.button_Ok);
			button_Ok.setTypeface(Reguler);
			// if button is clicked, close the custom dialog
			button_Ok.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();

					Intent viewIntent = new Intent(
							Settings.ACTION_LOCATION_SOURCE_SETTINGS);
					context.startActivity(viewIntent);
				}
			});

			Button button_Cancel = (Button) dialog
					.findViewById(R.id.button_Cancel);
			button_Cancel.setTypeface(Reguler);
			// if button is clicked, close the custom dialog
			button_Cancel.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();

					// don't start listeners if no provider is
					// enabled
					boolean show_route = false;
					boolean gps_enabled = false;
					boolean network_enabled = false;
					LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
					try {
						gps_enabled = lm
								.isProviderEnabled(LocationManager.GPS_PROVIDER);
					} catch (Exception ex) {
					}
					try {
						network_enabled = lm
								.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
					} catch (Exception ex) {
					}
					WifiManager wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
					if (wifi.isWifiEnabled())
						show_route = true;
					else if (network_enabled && gps_enabled)
						show_route = true;
					else if (network_enabled)
						show_route = true;
					try {

						ArrayList<WeakHashMap<String, String>> temp = new ArrayList<WeakHashMap<String, String>>();

						for (int j = 0; j < data_location.size(); j++) {
							WeakHashMap<String, String> map1 = new WeakHashMap<String, String>();
							WeakHashMap<String, String> map = new WeakHashMap<String, String>(
									data_location.get(j));
							if (Integer.parseInt(map.get(DEAL_ID)) == Integer
									.parseInt(deal_ID)) {
								map1.put(LATITUDE, map.get(LATITUDE));
								map1.put(LONGITUDE, map.get(LONGITUDE));
								map1.put(ADDRESS, map.get(ADDRESS));
								map1.put(CITY, map.get(CITY));
								map1.put(STATE, map.get(STATE));

								temp.add(map1);
							}
						}
						if (temp.size() > 1) {
							Intent intent = new Intent(
									MyFreeBiesDetailsActivity.this,
									FreeBiesLocationActivity.class);
							intent.putExtra("ROUTE", show_route);
							intent.putExtra("DEAL", textView_DealName.getText()
									.toString().trim());
							intent.putExtra("ID", deal_ID);
							intent.putExtra("FROM_DETAILS", true);
							intent.putExtra("LOCATIONS", data_location);
							startActivity(intent);
						} else {
							Intent intent = new Intent(
									MyFreeBiesDetailsActivity.this,
									FreeBiesLocatorActivity.class);
							WeakHashMap<String, String> map = new WeakHashMap<String, String>(
									temp.get(0));

							intent.putExtra("ROUTE", show_route);
							intent.putExtra("DEAL", textView_DealName.getText()
									.toString().trim());
							intent.putExtra("LATITUDE",
									map.get(FreeBiesActivity.LATITUDE));
							intent.putExtra("LONGITUDE",
									map.get(FreeBiesActivity.LONGITUDE));
							intent.putExtra("ADDRESS",
									map.get(FreeBiesActivity.ADDRESS));
							intent.putExtra("CITY",
									map.get(FreeBiesActivity.CITY));
							intent.putExtra("STATE",
									map.get(FreeBiesActivity.STATE));
							startActivity(intent);
						}
					} catch (Exception e) {
						Intent intent = new Intent(
								MyFreeBiesDetailsActivity.this,
								FreeBiesLocationActivity.class);
						intent.putExtra("ROUTE", show_route);
						intent.putExtra("DEAL", textView_DealName.getText()
								.toString().trim());
						intent.putExtra("ID", deal_ID);
						intent.putExtra("FROM_DETAILS", true);
						intent.putExtra("LOCATIONS", data_location);
						startActivity(intent);
					}

				}
			});

			button_Ok.setText("Ok");
			button_Cancel.setText("Not now");

			dialog.show();

		} catch (Exception exception) {
		}
	}

	public void showTwoButtonDialog(final Context context, String Title,
			String message) {
		try {

			final Dialog dialog = new Dialog(context);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.getWindow().setBackgroundDrawableResource(
					android.R.color.transparent);
			dialog.setContentView(R.layout.dialog_redeem);
			dialog.setCancelable(false);
			dialog.setCanceledOnTouchOutside(false);
			// dialog.setTitle(null);

			Typeface Reguler = Typeface.createFromAsset(context.getAssets(),
					"EncodeSansNormal-400-Regular.ttf");

			Typeface Medium = Typeface.createFromAsset(context.getAssets(),
					"EncodeSansNarrow-500-Medium.ttf");

			// set the custom dialog components - text, image and button
			TextView textView_Title = (TextView) dialog
					.findViewById(R.id.textView_Title);
			textView_Title.setText(Title);
			textView_Title.setTypeface(Reguler);

			TextView textView_Message = (TextView) dialog
					.findViewById(R.id.textView_Message);
			textView_Message.setText("");
			textView_Message.setTypeface(Medium);

			final EditText editText_Password = (EditText) dialog
					.findViewById(R.id.editText_Password);
			editText_Password.setTypeface(Medium);

			Button button_Ok = (Button) dialog.findViewById(R.id.button_Ok);
			button_Ok.setTypeface(Reguler);
			// if button is clicked, close the custom dialog
			button_Ok.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
					String[] vCode = textView_Voucher.getText().toString()
							.split(":");
					new RedeemGiftTask().execute(vCode[1].trim(), Utility
							.getMobileNumber(MyFreeBiesDetailsActivity.this,
									Constants.SHAREDPREFERENCE_MOBILE),
							deal_ID, editText_Password.getText().toString()
									.trim());
				}
			});

			Button button_Cancel = (Button) dialog
					.findViewById(R.id.button_Cancel);
			button_Cancel.setTypeface(Reguler);
			// if button is clicked, close the custom dialog
			button_Cancel.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
				}
			});

			dialog.show();

		} catch (Exception exception) {
		}
	}

	public class RedeemGiftTask extends AsyncTask<String, String, String>
			implements OnDismissListener {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			try {
				String response = RequestClass.getInstance()
						.readPay1B2CRequest(
								MyFreeBiesDetailsActivity.this,
								Constants.B2C_URL
										+ "Redeem_FreebieByApp/?coupon="
										+ params[0] + "&mobile=" + params[1]
										+ "&dealid=" + params[2] + "&password="
										+ params[3]);

				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				if (this.dialog.isShowing()) {
					this.dialog.dismiss();
				}
			} catch (Exception e) {
			}
			try {

				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONObject jsonObject = new JSONObject(replaced);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {

						// JSONArray jsonArray_Desc = jsonObject
						// .getJSONArray("description");

						Utility.setRefreshRequired(
								MyFreeBiesDetailsActivity.this,
								Constants.SHAREDPREFERENCE_REFESH_MY_GIFT, true);
						Utility.setBackRequired(MyFreeBiesDetailsActivity.this,
								Constants.SHAREDPREFERENCE_BACK_LOCATION, true);
						Constants.showOneButtonDialog(
								MyFreeBiesDetailsActivity.this, TAG,
								Constants.checkCode(replaced),
								Constants.DIALOG_CLOSE);

					} else {
						Constants.showOneButtonDialog(
								MyFreeBiesDetailsActivity.this, TAG,
								Constants.checkCode(replaced),
								Constants.DIALOG_CLOSE);
					}

				} else {
					Intent intent = new Intent(MyFreeBiesDetailsActivity.this,
							ConnectivityErrorActivity.class);
					startActivity(intent);
				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonDialog(MyFreeBiesDetailsActivity.this,
						TAG, Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonDialog(MyFreeBiesDetailsActivity.this,
						TAG, Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(MyFreeBiesDetailsActivity.this);
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							dialog.dismiss();
							RedeemGiftTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			dialog.dismiss();
			RedeemGiftTask.this.cancel(true);
		}
	}

	public class LikeTask extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... params) {
			try {

				String response = RequestClass.getInstance()
						.readPay1B2CRequest(MyFreeBiesDetailsActivity.this,
								Constants.B2C_URL + "MyLikes/?id=" + params[0]);

				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {

				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONObject jsonObject = new JSONObject(replaced);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {

						JSONObject jsonObject2 = jsonObject
								.getJSONObject("description");
						FreeBiesDataSource freeBiesDataSource = new FreeBiesDataSource(
								MyFreeBiesDetailsActivity.this);
						try {

							freeBiesDataSource.open();
							freeBiesDataSource.updateFreeBies(Integer
									.parseInt(jsonObject2.getString("status")),
									String.valueOf(offer_ID));
						} catch (SQLException exception) {
						} catch (Exception e) {

						} finally {
							freeBiesDataSource.close();
						}
					}
				}
			} catch (JSONException e) {
			} catch (Exception e) {
			}
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}
	}

	public class RatingTask extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... params) {
			try {

				String response = RequestClass.getInstance()
						.readPay1B2CRequest(
								MyFreeBiesDetailsActivity.this,
								Constants.B2C_URL + "SetOfferReview/?id="
										+ params[0] + "&review=" + params[1]);

				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {

				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONObject jsonObject = new JSONObject(replaced);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {

						JSONObject jsonObject2 = jsonObject
								.getJSONObject("description");
						FreeBiesDataSource freeBiesDataSource = new FreeBiesDataSource(
								MyFreeBiesDetailsActivity.this);
						try {

							freeBiesDataSource.open();
							freeBiesDataSource.updateFreeBies(Integer
									.parseInt(jsonObject2.getString("status")),
									String.valueOf(offer_ID));
						} catch (SQLException exception) {
						} catch (Exception e) {

						} finally {
							freeBiesDataSource.close();
						}
					}
				}
			} catch (JSONException e) {
			} catch (Exception e) {
			}
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}
	}

	public class PurchaseDealTask extends AsyncTask<String, String, String>
			implements OnDismissListener {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {

			String response = RequestClass
					.getInstance()
					.readPay1B2CRequest(
							MyFreeBiesDetailsActivity.this,
							Constants.B2C_URL
									+ "purchase_deal/?deal_id="
									+ params[0]
									+ "&offer_id="
									+ params[1]
									+ "&coupon_id=&paymentopt=wallet"
									+ "&amount="
									+ params[2]
									+ "&quantity=1&trans_category=deal&freebie=true&ref_id=&api_version=3");

			return response;
		}

		@Override
		protected void onCancelled() {
			// TODO Auto-generated method stub
			super.onCancelled();
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			try {

				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");
					JSONObject jsonObject = new JSONObject(replaced);

					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {
						JSONObject description = jsonObject
								.getJSONObject("description");
						Utility.setBalance(MyFreeBiesDetailsActivity.this,
								Constants.SHAREDPREFERENCE_LOYALTY_POINTS,
								description.getString("loyalty_balance"));
						Utility.setBalance(MyFreeBiesDetailsActivity.this,
								Constants.SHAREDPREFERENCE_BALANCE,
								description.getString("closing_balance"));
						String str = Utility.getMobileNumber(
								MyFreeBiesDetailsActivity.this,
								Constants.SHAREDPREFERENCE_MOBILE);
						try {
							String temp = str.substring(0, 6);
							textView_Mobile.setText("Mobile number: "
									+ str.replaceAll(temp, "XXXXXX"));
						} catch (Exception e) {
							textView_Mobile.setText("Mobile number: " + str);
						}
						textView_Voucher.setText("Voucher code: "
								+ description.getString("deal_coupon_code"));
						try {
							textView_ExpiryDate.setText("Expiry date: "
									+ description.getString("expiry"));
						} catch (Exception e) {
						}

						textView_Mobile.setVisibility(View.VISIBLE);
						textView_Voucher.setVisibility(View.VISIBLE);
						textView_ExpiryDate.setVisibility(View.VISIBLE);
						view_sep.setVisibility(View.VISIBLE);
						button_Purchase.setText("REDEEM NOW");
						button_Purchase.setBackgroundDrawable(getResources()
								.getDrawable(R.drawable.gray_pocket_selector));

						Utility.setRefreshRequired(
								MyFreeBiesDetailsActivity.this,
								Constants.SHAREDPREFERENCE_REFESH_MY_GIFT, true);

						button_Purchase.setText("In your pocket");
						img_redeem_now.setVisibility(View.VISIBLE);
						button_Purchase.setBackgroundDrawable(getResources()
								.getDrawable(R.drawable.gray_pocket_selector));

					} else {
						Constants.showOneButtonDialog(
								MyFreeBiesDetailsActivity.this, "Gift",
								Constants.checkCode(replaced),
								Constants.DIALOG_CLOSE);
					}
				} else {
					Intent intent = new Intent(MyFreeBiesDetailsActivity.this,
							ConnectivityErrorActivity.class);
					startActivity(intent);
				}
			} catch (Exception e) {
				Constants.showOneButtonDialog(MyFreeBiesDetailsActivity.this,
						"Gift", Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(MyFreeBiesDetailsActivity.this);
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(false);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							PurchaseDealTask.this.cancel(true);
							dialog.cancel();
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			PurchaseDealTask.this.cancel(true);
			dialog.cancel();
		}

	}

	public void showDailogForFreeBie() {
		final Dialog dialog = new Dialog(MyFreeBiesDetailsActivity.this);

		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

		dialog.setCancelable(false);
		dialog.setCanceledOnTouchOutside(false);
		dialog.getWindow().setBackgroundDrawableResource(
				android.R.color.transparent);
		dialog.setContentView(R.layout.dialog_freebie_info_layout);

		TextView text = (TextView) dialog.findViewById(R.id.textViewInfo);

		String msg = "1. Please read the offer carefully"
				+ "\n2. Please check the T& C before using the offer"
				+ "\n3. Once you pocket the gift you need to redeem gift"
				+ "\n4. When you redeem the gift ask the merchant to add password";

		text.setText(msg);

		Button dialogButton = (Button) dialog
				.findViewById(R.id.btnDialogContinue);
		// if button is clicked, close the custom dialog
		dialogButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				dialog.dismiss();
			}
		});

		dialog.show();
	}

	public void showDialogForCallList(
			final ArrayList<HashMap<String, String>> numberWithLocationList) {
		try {
			final Dialog dialog = new Dialog(MyFreeBiesDetailsActivity.this);

			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

			dialog.setCancelable(true);
			dialog.setCanceledOnTouchOutside(true);
			dialog.getWindow().setBackgroundDrawableResource(
					android.R.color.transparent);
			dialog.setContentView(R.layout.dialog_call_list);
			ImageView imageView_Close = (ImageView) dialog
					.findViewById(R.id.imageView_Close);
			TextView textView_Title=(TextView)dialog.findViewById(R.id.textView_Title);
			textView_Title.setText(this.textView_Title.getText().toString());
			imageView_Close.setImageDrawable(SVGParser.getSVGFromResource(
					MyFreeBiesDetailsActivity.this.getResources(),
					R.raw.ic_close).createPictureDrawable());
			imageView_Close.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					dialog.dismiss();
				}
			});
			ListView callListView = (ListView) dialog
					.findViewById(R.id.listViewCall);
			CallListAdapter adapter = new CallListAdapter(
					MyFreeBiesDetailsActivity.this, numberWithLocationList);

			callListView.setAdapter(adapter);

			callListView.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {
					// TODO Auto-generated method stub
					dialog.dismiss();
					String number = numberWithLocationList.get(arg2).get(
							"number");
					String[] numberArray = number.split(",");
					String firstNumber = numberArray[0].trim();
					if (!firstNumber.isEmpty()) {
						Intent callIntent = new Intent(Intent.ACTION_CALL);
						callIntent.setData(Uri.parse("tel:" + firstNumber));
						startActivity(callIntent);
					} else {
						Toast.makeText(MyFreeBiesDetailsActivity.this,
								"No number available", Toast.LENGTH_LONG)
								.show();
					}
				}
			});

			dialog.show();
		} catch (Exception exc) {
			Log.d("msg", "Msg");
		}
	}

	public void showDailogForCoinsBalance(String balance, String requiredBalance) {
		try {
			final Dialog dialog = new Dialog(MyFreeBiesDetailsActivity.this);

			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

			dialog.setCancelable(false);
			dialog.setCanceledOnTouchOutside(false);
			dialog.getWindow().setBackgroundDrawableResource(
					android.R.color.transparent);

			Typeface Reguler = Typeface.createFromAsset(getAssets(),
					"EncodeSansNormal-400-Regular.ttf");
			Typeface Medium = Typeface.createFromAsset(getAssets(),
					"EncodeSansNarrow-500-Medium.ttf");

			dialog.setContentView(R.layout.dialog_low_coins);

			TextView textView_Message4 = (TextView) dialog
					.findViewById(R.id.textView_Message4);

			TextView textView_Title = (TextView) dialog
					.findViewById(R.id.textView_Title);

			TextView textView_Message1 = (TextView) dialog
					.findViewById(R.id.textView_Message1);
			TextView textView_Message2 = (TextView) dialog
					.findViewById(R.id.textView_Message2);
			
			ImageView imgNoCoins=(ImageView)dialog.findViewById(R.id.imgNoCoins);
			imgNoCoins.setImageDrawable(SVGParser.getSVGFromResource(
					MyFreeBiesDetailsActivity.this.getResources(),
					R.raw.ic_not_enough_balance).createPictureDrawable());
			
			//textView_Title.set
			textView_Message1.setTypeface(Reguler);
			textView_Message2.setTypeface(Reguler);
			textView_Message4.setTypeface(Reguler);
			textView_Title.setTypeface(Reguler);
			// textViewInfo.setTypeface();
			/*String message1 = "<font color=#1651ff>Redeeming this gift requires </font> <font color=#E63638 >"
					+ requiredBalance
					+ "</font><font color=#1651ff> Gift Coins.</font>";
			String message2 = "<font color=#1651ff>You have </font> <font color=#E63638 >"
					+ balance
					+ "</font><font color=#1651ff> Gift Coins.</font>";*/
			
			String message1="You have "+balance+" Gift Coins.\nRecharge now to add more\ncoins and pocket this gift.";
			textView_Message1.setText(Html.fromHtml(message1));
			//textView_Message2.setText(Html.fromHtml(message2));
			Button btnDialogContinue = (Button) dialog
					.findViewById(R.id.btnDialogContinue);

			btnDialogContinue.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					// Utility.saveshowDialog(MyFreeBiesDetailsActivity.this,
					// true);
					dialog.dismiss();
					/*
					 * Intent i = new Intent(MyFreeBiesDetailsActivity.this,
					 * MainActivity.class); i.putExtra(Constants.RECHARGE_FOR,
					 * Constants.RECHARGE_MOBILE); startActivity(i); finish();
					 */

					Intent intent = new Intent(MyFreeBiesDetailsActivity.this,
							MainActivity.class);
					intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
							| Intent.FLAG_ACTIVITY_NEW_TASK);
					intent.putExtra("FROM_QUICKPAY", true);
					intent.putExtra("IS_GIFT", false);
					MyFreeBiesDetailsActivity.this.startActivity(intent);
					MyFreeBiesDetailsActivity.this.finish();

				}
			});

			Button button_cancel = (Button) dialog
					.findViewById(R.id.button_cancel);

			button_cancel.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					// Utility.saveshowDialog(MyFreeBiesDetailsActivity.this,
					// true);
					dialog.dismiss();
				}
			});

			dialog.show();
		} catch (Exception exc) {
			Log.d("msg", "Msg");
		}
	}

	private ImageView mOverLayImage;

	public void ShowcaseImage() {
		final Dialog overlayInfo = new Dialog(MyFreeBiesDetailsActivity.this);

		overlayInfo.requestWindowFeature(Window.FEATURE_NO_TITLE);
		overlayInfo.getWindow().setBackgroundDrawable(
				new ColorDrawable(Color.TRANSPARENT));
		overlayInfo.getWindow().clearFlags(
				WindowManager.LayoutParams.FLAG_DIM_BEHIND);
		overlayInfo.setContentView(R.layout.overlay_view);
		overlayInfo.show();

		/*
		 * mOverLayImage = (ImageView) overlayInfo
		 * .findViewById(R.id.over_lay_image);
		 * mOverLayImage.setOnClickListener(new OnClickListener() {
		 * 
		 * @Override public void onClick(View v) { overlayInfo.cancel(); } });
		 */
	}

	private int getRelativeLeft(View myView) {
		if (myView.getParent() == myView.getRootView())
			return myView.getLeft();
		else
			return myView.getLeft()
					+ getRelativeLeft((View) myView.getParent());
	}

	private int getRelativeTop(View myView) {
		if (myView.getParent() == myView.getRootView())
			return myView.getTop();
		else
			return myView.getTop() + getRelativeTop((View) myView.getParent());
	}

}
