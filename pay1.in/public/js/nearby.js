    var map;
    
    $(document).ready(function(){
    
                 map = new GMaps({  div: '#map',lat: 0,lng: 0,zoom:13});
                 $('div#nearbyme').html("<img id='loadingimage' src='/images/loading.gif' />");
                
                if(($.cookie('latitude')!="")  && ($.cookie('longitude')!="") &&  ($.cookie('latitude')!=undefined) && ($.cookie('longitude')!=undefined) )
                {
                    
                           map.setCenter($.cookie('latitude'), $.cookie('longitude'));
                           getNearbyDeals($.cookie('latitude'), $.cookie('longitude'));
                           
                }
                else
                {
                
                            GMaps.geolocate({
                                                                success: function (position) {

                                                                    map.setCenter(position.coords.latitude, position.coords.longitude);

                                                                    $.cookie('latitude', position.coords.latitude, {expires: 7,path:'/'});
                                                                    $.cookie('longitude', position.coords.longitude, {expires: 7,path:'/'});

                                                                    $('img#loadingimage').remove();

                                                                    getNearbyDeals(position.coords.latitude, position.coords.longitude);

                                                                },
                                                                error: function (error) {
                                                                    alert('Tracing Location  failed: ' + error.message);
                                                                },
                                                                not_supported: function () {
                                                                    alert("Your browser does not support geolocation");
                                                                },
                                                                always: function () {
                                                                    $('img#loadingimage').remove();
                                                                }
                                        });
                }
              
                                            
                                            
                                            
    });
function getNearbyDeals(latitude,longitude)
{
  
  var mapsJson=[];
  
   var promise2=$.ajax({url:b2cUrl+'/index.php/api_new/action/api/true/actiontype/get_deals/?id=69&latitude=19.184249599999998&longitude=72.831876&api_version=3&distance=5&res_format=jsonp',data:{format:'JSONP'},dataType:'JSONP'});
    
    $.when(promise2).done(function(res){
    
    $.each(res.description,function(k,v){
        
          
                    HTML='<div  class="col-lg-3 col-sm-3  text-center nearbylisting" data-gift-points="'+v.actual_price+'">';
                    HTML+='<div class="thumbnail tileheight">';

                                 HTML+='<div class="addtofavdiv">';
                                 HTML+='<img src="/images/addtofavorities.png" data-swap="/images/addtofavorities_red.png" value="0" style="cursor: pointer">';
                                 HTML+='</div>';

                                  HTML+='<div>'
                                  HTML+='<img src="'+v.img_url+'"  width="150px" height="100px" />';
                                  HTML+='</div>'  

                                  HTML+='<div class="card_infolabel pull-right showpoints">';
                                  HTML+='<img src="/images/gift_ic.png">';
                                  HTML+='<span class="card_infolabel">'+v.actual_price+'</span>';
                                  HTML+='</div>';
                                  
                                  
                                  HTML+='<div class="caption prod-caption top15">';
                                  HTML+='<a href="/gifts/detail/'+v.offer_id+'"><h5>'+v.offer_name+'</h5></a>';
                                  HTML+='<span class="listingspan">'+v.dealname+'</span>';
                                  HTML+='</div>';

                                  

                    HTML+='</div>';
                    HTML+='</div>';
        
          
        $('div#nearbyme').append(HTML);
        
        HTML='';
        
        mapsJson={lat:v.latitude,lng:longitude,title:v.offer_name,click:function(e){console.log("Clicked Me")},infoWindow:{content:'<p><b>'+v.offer_name+'</b></p><p>'+v.category+'</p>'}};
        
        map.addMarker(mapsJson);
    });
    
    
    }).fail(function(){
        console.log("Failed");
    }).always(function(){
         $('img#loadingimage').remove();
        console.log("Finished Everthing");
    });
  

}