﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace Pay1B2C.Views
    {
    public partial class TopupWalletLayout: PhoneApplicationPage
        {
        public TopupWalletLayout()
            {
            InitializeComponent();
            Constant.laodNavigationBar(TitlePanel, LayoutRoot, canvas, "Topup And Coupon");
            GoogleAnalytics.EasyTracker.GetTracker().SendView("Topup And Coupon");
            textBalance.Text="Rs. "+Constant.getbalance();
            }

        private void panelLocateRetailer_Tap(object sender, System.Windows.Input.GestureEventArgs e)
            {
            (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri("/NearByRetailers.xaml?", UriKind.RelativeOrAbsolute));
            }

        private void panelOnlinePayment_Tap(object sender, System.Windows.Input.GestureEventArgs e)
            {
            (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri("/Views/OnlineRefill.xaml?", UriKind.RelativeOrAbsolute));
            }

        private void panelRedeemCoupon_Tap(object sender, System.Windows.Input.GestureEventArgs e)
            {
            (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri("/Views/CouponCode.xaml?", UriKind.RelativeOrAbsolute));
            }
        }
    }