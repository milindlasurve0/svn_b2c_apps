package com.pay1;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.http.SslError;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.JsResult;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.larvalabs.svgandroid.SVGParser;
import com.pay1.constants.Constants;
import com.pay1.customviews.MyProgressDialog;
import com.pay1.requesthandler.RequestClass;
import com.pay1.utilities.Utility;

public class PGActivity extends Activity {

	private static final String SCREEN_LABEL = "PG Screen";
	private EasyTracker easyTracker = null;
	private ImageView imageView_Back;
	private TextView textView_Title;
	private WebView webView_PG;
	private ProgressBar progressBar_PG;
	private String html = "";
	int rechargeType;
	String operatorName, mobileNumber, rechargeAmount, payOpid,
			specialRecharge, tax = "", trans_id = "";

	@SuppressLint("NewApi")
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pg_activity);
		try {
			easyTracker = EasyTracker.getInstance(PGActivity.this);
			easyTracker.set(Fields.SCREEN_NAME, SCREEN_LABEL);

			Bundle bundle = getIntent().getExtras();
			if (bundle != null) {
				html = bundle.getString("CONTENT");
				mobileNumber = bundle.getString("MOBILE_NUMBER");
				rechargeAmount = bundle.getString("RECHARGE_AMOUNT");
				rechargeType = bundle.getInt("RECHARGE_FOR");
				operatorName = bundle.getString("OPERATOR_NAME");
				tax = bundle.getString("TAX");
				trans_id = bundle.getString("TRANS_ID");
			}

			imageView_Back = (ImageView) findViewById(R.id.imageView_Back);
			imageView_Back.setImageDrawable(SVGParser.getSVGFromResource(
					getResources(), R.raw.ic_back_old).createPictureDrawable());
			if (Build.VERSION.SDK_INT >= 11)
				imageView_Back.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
			
			RelativeLayout back_layout=(RelativeLayout)findViewById(R.id.back_layout);
			
			
			back_layout.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onBackPressed();
				}
			});

			
			
			Typeface Reguler = Typeface.createFromAsset(getAssets(),
					"EncodeSansNormal-400-Regular.ttf");

			textView_Title = (TextView) findViewById(R.id.textView_Title);
			textView_Title.setTypeface(Reguler);

			progressBar_PG = (ProgressBar) findViewById(R.id.progressBar_PG);
			progressBar_PG.setVisibility(View.GONE);

			webView_PG = (WebView) findViewById(R.id.webView_PG);
			webView_PG.setOnLongClickListener(new View.OnLongClickListener() {

				@Override
				public boolean onLongClick(View v) {
					// TODO Auto-generated method stub
					return true;
				}
			});
			webView_PG.setLongClickable(false);
			webView_PG.setBackgroundColor(0x00000000);
			if (Build.VERSION.SDK_INT >= 11)
				webView_PG.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);
			webView_PG.getSettings().setJavaScriptEnabled(true);
			webView_PG.getSettings().setDomStorageEnabled(true);
			webView_PG.getSettings().setDatabaseEnabled(true);
			webView_PG.setWebViewClient(new MyWebClient());
			webView_PG.setWebChromeClient(new MyWebChromeClient());
			webView_PG.loadDataWithBaseURL(null, html, "text/html", "UTF-8",
					null);

		} catch (Exception e) {
		}
		overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}

	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView().build());
		// EasyTracker.getInstance(this).activityStart(this);
		easyTracker.send(MapBuilder.createAppView().build());
	}

	@Override
	public void onStop() {
		super.onStop();
		// EasyTracker.getInstance(this).activityStop(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// super.onBackPressed();
		showTwoButtonDialog(PGActivity.this, "Pay1",
				"Are you sure you want to cancel this transaction?");
	}

	public void showTwoButtonDialog(final Context context, String Title,
			String message) {
		try {

			final Dialog dialog = new Dialog(context);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			 dialog.getWindow().setBackgroundDrawableResource(
			 android.R.color.transparent);
			dialog.setContentView(R.layout.dialog_two_button);
			dialog.setCancelable(false);
			dialog.setCanceledOnTouchOutside(false);
			// dialog.setTitle(null);

			Typeface Reguler = Typeface.createFromAsset(context.getAssets(),
					"EncodeSansNormal-400-Regular.ttf");

			Typeface Medium = Typeface.createFromAsset(context.getAssets(),
					"EncodeSansNarrow-500-Medium.ttf");

			// set the custom dialog components - text, image and button
			TextView textView_Title = (TextView) dialog
					.findViewById(R.id.textView_Title);
			textView_Title.setText(Title);
			textView_Title.setTypeface(Reguler);

			TextView textView_Message = (TextView) dialog
					.findViewById(R.id.textView_Message);
			textView_Message.setText(message);
			textView_Message.setTypeface(Medium);

			Button button_Ok = (Button) dialog.findViewById(R.id.button_Ok);
			button_Ok.setTypeface(Reguler);
			// if button is clicked, close the custom dialog
			button_Ok.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
					PGActivity.this.finish();
					overridePendingTransition(R.anim.slide_in_left,
							R.anim.slide_out_right);
				}
			});

			Button button_Cancel = (Button) dialog
					.findViewById(R.id.button_Cancel);
			button_Cancel.setTypeface(Reguler);
			// if button is clicked, close the custom dialog
			button_Cancel.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
				}
			});

			dialog.show();

		} catch (Exception exception) {
		}
	}

	class MyWebClient extends WebViewClient {
		// private MyProgressDialog dialog = new
		// MyProgressDialog(PGActivity.this);

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * android.webkit.WebViewClient#onReceivedSslError(android.webkit.WebView
		 * , android.webkit.SslErrorHandler, android.net.http.SslError)
		 */
		@Override
		public void onReceivedSslError(WebView view, SslErrorHandler handler,
				SslError error) {
			handler.proceed(); // Ignore SSL certificate errors
		}

		public void onReceivedError(WebView view, int errorCode,
				String description, String failingUrl) {
			// Log.e("Load Signup page", description);
			Toast.makeText(
					PGActivity.this,
					"Problem loading. Make sure internet connection is available.",
					Toast.LENGTH_SHORT).show();
		}

		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			// TODO Auto-generated method stub
			super.onPageStarted(view, url, favicon);
			progressBar_PG.setVisibility(View.VISIBLE);
			// if (this.this.dialog.isShowing())
			// this.this.dialog.dismiss();
			// dialog = new MyProgressDialog(PGActivity.this);
			// this.dialog.setMessage("Please wait.....");
			// this.dialog.setCancelable(false);
			// this.dialog
			// .setOnCancelListener(new DialogInterface.OnCancelListener() {
			//
			// @Override
			// public void onCancel(DialogInterface dialog) {
			// // TODO Auto-generated method stub
			// dialog.dismiss();
			// finish();
			// }
			// });
			//
			// this.dialog.show();
		}

		@Override
		public void onPageFinished(WebView view, String url) {
			// String str=url.substring(url.lastIndexOf("/"));
			// try {
			// if (this.dialog.isShowing())
			// this.dialog.dismiss();
			// } catch (Exception e) {
			// this.dialog.dismiss();
			// }
			progressBar_PG.setVisibility(View.GONE);
			// Log.w("PG", url);
			view.loadUrl("javascript:console.log(document.body.getElementsByTagName('pre')[0].innerHTML);");
			if (url.contains("payu_update")) {
				String str = url.substring(url.lastIndexOf("/") + 1);
				// Log.w("OUT", str);

				String reason = null;
				boolean type = false;
				if (str.equalsIgnoreCase("success")) {
					type = true;
					reason = "Transaction completed successfully.";
				} else if (str.equalsIgnoreCase("failure")) {
					type = false;
					reason = "Transaction failed.";
				} else if (str.equalsIgnoreCase("cancel")) {
					type = false;
					reason = "Transaction cancelled.";
				} else if (str.equalsIgnoreCase("timeout")) {
					type = false;
					reason = "Transaction request timeout.";
				}

				if (rechargeType == Constants.WALLET_PAYMENT) {
					Constants.showSuccessFailureDialog(PGActivity.this,
							"Success", rechargeType, Utility.getMobileNumber(
									PGActivity.this,
									Constants.SHAREDPREFERENCE_MOBILE),
							operatorName, rechargeAmount, tax, type, reason,
							trans_id, "");
				} else {
					if (type) {
						new CheckBalanceTask().execute();
					} else {
						Constants.showSuccessFailureDialog(PGActivity.this,
								"Success", rechargeType, mobileNumber,
								operatorName, rechargeAmount, tax, type,
								reason, trans_id, "");
					}
				}

			}

		}

		// @Override
		// public void onDismiss(DialogInterface dialog) {
		// // TODO Auto-generated method stub
		// this.dialog.dismiss();
		// finish();
		// }
	}

	private class MyWebChromeClient extends WebChromeClient {

		// display alert message in Web View
		@Override
		public boolean onJsAlert(WebView view, String url, String message,
				JsResult result) {
			Log.d("Web", message);
			// new AlertDialog.Builder(view.getContext()).setMessage(message)
			// .setCancelable(true).show();
			// result.confirm();
			return true;
		}

	}

	public class CheckBalanceTask extends AsyncTask<String, String, String> {
		 MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {

			String response = RequestClass.getInstance().readPay1B2CRequest(
					PGActivity.this,
					Constants.B2C_URL + "GetTrans_WalletDetail/?txnid="
							+ trans_id);
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			 if (this.dialog.isShowing()) {
			 this.dialog.dismiss();
			 }
			super.onPostExecute(result);
			try {
				// JSONArray array = new JSONArray(result);
				JSONObject jsonObject = new JSONObject(result);
				String status = jsonObject.getString("status");
				if (status.equalsIgnoreCase("SUCCESS")) {
					JSONObject description = jsonObject
							.getJSONObject("description");
					String account_balance = description
							.getString("account_balance");
					Utility.setBalance(PGActivity.this,
							Constants.SHAREDPREFERENCE_BALANCE,
							account_balance.trim());
					String points = description.getString("points");
					Utility.setBalance(PGActivity.this,
							Constants.SHAREDPREFERENCE_LOYALTY_POINTS,
							description.getString("loyalty_points"));

					Constants.showSuccessFailureDialog(PGActivity.this,
							"Success", rechargeType, mobileNumber,
							operatorName, rechargeAmount, tax, true,
							"Transaction completed successfully.", trans_id,
							points);
				}
			} catch (JSONException e) {
				// e.printStackTrace();
				// Constants.showCustomToast(MainActivity.this,
				// result);
			} catch (Exception e) {
				// TODO: handle exception
				// Constants.showCustomToast(MainActivity.this,
				// result);
			}

		}

		protected void onPreExecute() {
			 dialog = new MyProgressDialog(PGActivity.this);
			 // this.dialog.setMessage("Please wait.....");
			 this.dialog.setCancelable(true);
			 this.dialog
			 .setOnCancelListener(new DialogInterface.OnCancelListener() {
			
			 @Override
			 public void onCancel(DialogInterface dialog) {
			 // TODO Auto-generated method stub
			 dialog.dismiss();
			 CheckBalanceTask.this.cancel(true);
			 }
			 });
			 this.dialog.show();
			super.onPreExecute();
		}
	}

}
