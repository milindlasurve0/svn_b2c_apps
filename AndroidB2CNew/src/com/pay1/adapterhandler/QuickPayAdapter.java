package com.pay1.adapterhandler;

import java.util.ArrayList;
import java.util.WeakHashMap;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.pay1.QuickPayFragment;
import com.pay1.R;
import com.pay1.constants.Constants;
import com.pay1.constants.OperatorConstant;

public class QuickPayAdapter extends BaseAdapter {

	Context context;
	ArrayList<WeakHashMap<String, String>> data;
	private Typeface Reguler;

	public QuickPayAdapter(Context context,
			ArrayList<WeakHashMap<String, String>> data) {
		this.context = context;
		this.data = data;
		Reguler = Typeface.createFromAsset(context.getAssets(),
				"Roboto-Regular.ttf");
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@SuppressLint("NewApi")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder viewHolder;
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.quickpay_adapter, null);
			viewHolder = new ViewHolder();
			viewHolder.imageView_Operator = (ImageView) convertView
					.findViewById(R.id.imageView_Operator);
			viewHolder.textView_Symbol = (TextView) convertView
					.findViewById(R.id.textView_Symbol);
			viewHolder.textView_Symbol.setTypeface(Reguler);
			viewHolder.textView_Mobile = (TextView) convertView
					.findViewById(R.id.textView_Mobile);
			viewHolder.textView_Mobile.setTypeface(Reguler);
			viewHolder.textView_Amount = (TextView) convertView
					.findViewById(R.id.textView_Amount);
			viewHolder.textView_Amount.setTypeface(Reguler);
			convertView.setTag(viewHolder);

		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		WeakHashMap<String, String> map = new WeakHashMap<String, String>();
		map = data.get(position);
		viewHolder.imageView_Operator.setImageResource(OperatorConstant
				.getOperatorResource(Integer.parseInt(map
						.get(QuickPayFragment.OPERATOR))));
		viewHolder.textView_Mobile.setText(map.get(QuickPayFragment.MOBILE)
				.toString().trim());
		if (Integer.parseInt(map.get(QuickPayFragment.FLAG)) == Constants.BILL_PAYMENT) {
			viewHolder.textView_Symbol.setVisibility(View.GONE);
			viewHolder.textView_Amount.setText(map.get(QuickPayFragment.AMOUNT)
					.toString().trim());
		} else {
			viewHolder.textView_Symbol.setVisibility(View.GONE);
			viewHolder.textView_Amount.setText(context.getResources().getString(R.string.rs)+" "+ map.get(QuickPayFragment.AMOUNT)
					.toString().trim());
		}
		

		return convertView;
	}

	private class ViewHolder {
		ImageView imageView_Operator;
		TextView textView_Mobile, textView_Amount, textView_Symbol;
	}

}
