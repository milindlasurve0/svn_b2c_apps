﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace Pay1B2C.Utilities
    {
    public class CheckBoxSelector: IValueConverter
        {
        public System.Windows.Media.ImageSource FalseValue;
        public System.Windows.Media.ImageSource TrueValue;


        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
            {
            TrueValue = new BitmapImage(new Uri("/Images/toggle_on.png", UriKind.RelativeOrAbsolute));
            FalseValue = new BitmapImage(new Uri("/Images/toggle_off.png", UriKind.RelativeOrAbsolute));

            if(value == null)
                return FalseValue;
            else
                return (bool)value ? TrueValue : FalseValue;
            }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
            {
            return value != null ? value.Equals(TrueValue) : false;
            }
        }
    }

    
