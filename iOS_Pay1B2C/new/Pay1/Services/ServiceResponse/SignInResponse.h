//
//  SignInResponse.h
//  Pay1
//
//  Created by Annapurna on 12/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SignInResponse : NSObject

@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) NSString *userMobileNum;
@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) NSString *userEmail;
@property (nonatomic, strong) NSString *userGender;
@property (nonatomic, strong) NSString *userDOB;
@property (nonatomic, strong) NSString *userID;
@property (nonatomic, strong) NSString *userLat;
@property (nonatomic, strong) NSString *userLong;
@property (nonatomic, strong) NSString *sessionName;
@property (nonatomic, strong) NSString *userManufacturer;
@property (nonatomic, strong) NSString *userOSInfo;
@property (nonatomic, strong) NSString *userFBData;
@property (nonatomic, strong) NSString *userISGenuine;
@property (nonatomic, strong) NSString *deviceType;
@property (nonatomic, strong) NSString *userType;
@property (nonatomic, strong) NSString *created;
@property (nonatomic, strong) NSString *updated;
@property (nonatomic, strong) NSString *userWalletBalance;
@property (nonatomic, strong) NSString *userLoyaltyPoints;
@property (nonatomic, strong) NSString *userRole;
@property (nonatomic, strong) NSString *serviceChargePercent;
@property (nonatomic, strong) NSString *serviceTaxPercent;
@property (nonatomic, strong) NSString *supportNumber;
@property (nonatomic, strong) NSString *userAPIVersion;
@property (nonatomic, strong) NSString *offerLikes;


@property (nonatomic, strong) NSString *userVal;
@property (nonatomic, strong) NSString *userValName;
@property (nonatomic, strong) NSString *totalGifts;
@property (nonatomic, strong) NSString *totalRedeem;
@property (nonatomic, strong) NSString *totalMyLikes;
@property (nonatomic, strong) NSString *totalMyReview;
@property (nonatomic, strong) NSString *profileImageURL;
@property (nonatomic, strong) NSString *billPaymentFailureInfo;
@property (nonatomic, strong) NSString *rechargeFailureInfo;
@property (nonatomic, strong) NSString *walletReffiledRetailer;
@property (nonatomic, strong) NSString *offerReview1;
@property (nonatomic, strong) NSString *offerReview2;
@property (nonatomic, strong) NSString *offerReview3;
@property (nonatomic, strong) NSString *offerReview4;
@property (nonatomic, strong) NSString *offerReview5;

@property (nonatomic, strong) NSMutableArray *offerDetailArray;
@property (nonatomic, strong) NSMutableArray *recentPay1TransactionArray;

@property (nonatomic, strong) NSString *errorCode;
@property (nonatomic, strong) NSString *descMessage;
@property (nonatomic, strong) NSString *gcmRegID;
@property (nonatomic, strong) NSString *uID;
@property (nonatomic, strong) NSString *locationSRC;
@property (nonatomic, strong) NSString *userDeviceID;

@property (nonatomic, strong) NSString *signinDesc;





@end
