//
//  OptionViewController.m
//  Pay1
//
//  Created by Annapurna on 04/08/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "OptionViewController.h"
#import "SignOutResponse.h"
#import "SignOutService.h"
#import "UIImage+Utility.h"
#import "ViewController.h"
#import "ProfileViewController.h"
#import "CoinViewController.h"
#import "WalletDetailViewController.h"
#import "AccountsAndSupportViewController.h"
#import "MissedCallViewController.h"
#import "HistoryViewController.h"
#import "GiftsViewController.h"
#import "MainViewController.h"
#import "HomeViewController.h"
@implementation OptionViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    

    
}

-(void)viewWillAppear:(BOOL)animated{    
    
    coinDataLabel.text = [Constant getUserCoinBalance]; //[Session shared].objSignInResponse.userLoyaltyPoints;
    walletDataLabel.text = [Constant getUserWalletBalance]; //[Session shared].objSignInResponse.userWalletBalance;
    
    [scrollView setContentSize:CGSizeMake(self.view.frame.size.width, self.view.frame.size.height - 110)];
    
    NSLog(@"#ImageURL :%@ %@",[Session shared].objSignInResponse.profileImageURL,[Constant getUserImage:[Session shared].objSignInResponse.userID]);
    
    
    
    
    loginview.delegate = self;
    
    
    loginview.frame = CGRectMake(100, 10, 100, 100);
    
    for (id loginObject in loginview.subviews)
    {
    if ([loginObject isKindOfClass:[UIButton class]])
    {
    UIButton *btn = loginObject;
    btn.frame = CGRectMake(0, 0, loginview.frame.size.width, loginview.frame.size.height);
            
    btn.layer.cornerRadius = btn.frame.size.height/2;
    btn.clipsToBounds = YES;
    btn.layer.masksToBounds = YES;
    btn.layer.borderWidth = 2;
    btn.layer.borderColor = [UIColor whiteColor].CGColor;
            
            
            
  /*  UILabel *nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(90, btn.frame.origin.y + btn.frame.size.height + 10, self.view.frame.size.width - 50, 21)];
    nameLabel.textColor = [UIColor whiteColor];
    nameLabel.font = [UIFont fontWithName:@"Open Sans" size:17];
    nameLabel.text = [Session shared].objSignInResponse.userName;*/
            // [self.view addSubview:nameLabel];
            
    CGImageRef cgref = [[Constant getUserImage:[Session shared].objSignInResponse.userID] CGImage];
    
    NSLog(@"#CGImageRef :%@ %@",cgref,[Session shared].objSignInResponse.userName);
            
            
    if([Session shared].objSignInResponse.profileImageURL.length == 0 &&  cgref == NULL){
    [btn setBackgroundImage:[UIImage imageNamed:@"facebook_connect.png"] forState:UIControlStateNormal];
   // [nameLabel removeFromSuperview];
    }
    
    else if([Constant getUserImage:[Session shared].objSignInResponse.userID] != nil){
    NSLog(@">>>");
    UIImage * loginImage = [Constant getUserImage:[Session shared].objSignInResponse.userID];
    [btn setBackgroundImage:loginImage forState:UIControlStateNormal];
    }
        
        
    else{
        
    [btn.imageView setImageWithURL:[NSURL URLWithString:[Session shared].objSignInResponse.profileImageURL] placeholderImage:[UIImage imageNamed:@"white.png"]completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
            UIImage *newImage = [UIImage imageWithImage:image  scaledToSize:CGSizeMake(320,212)];
            [btn setBackgroundImage:newImage forState:UIControlStateNormal];
    }usingActivityIndicatorStyle:(UIActivityIndicatorViewStyle)UIActivityIndicatorViewStyleGray];
        
    }
        
    }
   if ([loginObject isKindOfClass:[UILabel class]])
    {
            //[loginObject removeFromSuperview];
            
    [loginObject setHidden:TRUE];
    }
        
    }
    
    
    if([Session shared].objSignInResponse.userName.length > 0){
        
    userNameLabel.text = [Session shared].objSignInResponse.userName;
    }
    else{
    [userNameLabel removeFromSuperview];
    }
    
    UITapGestureRecognizer *walletGes = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(walletClicked:)];
    walletGes.numberOfTapsRequired = 1;
    [walletView addGestureRecognizer:walletGes];
    
    UITapGestureRecognizer *coinGes = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(coinClicked:)];
    coinGes.numberOfTapsRequired = 1;
    [coinView addGestureRecognizer:coinGes];
}

-(IBAction)homeClicked:(id)sender{
    AppDelegate *appDelegate =  (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [appDelegate.tabBarController setSelectedIndex:0];
}

-(IBAction)rechargeClicked:(id)sender{
    AppDelegate *appDelegate =  (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [appDelegate.tabBarController setSelectedIndex:1];
}

-(IBAction)giftClicked:(id)sender{
    AppDelegate *appDelegate =  (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [appDelegate.tabBarController setSelectedIndex:2];
}

-(IBAction)historyClicked:(id)sender{
    HistoryViewController *historyVC = [self.storyboard instantiateViewControllerWithIdentifier:@"HistoryViewController"];
    historyVC.hidesBottomBarWhenPushed = TRUE;
    [self.navigationController pushViewController:historyVC animated:YES];
}

-(IBAction)missedCallClicked:(id)sender{
    MissedCallViewController *missedVC = [self.storyboard instantiateViewControllerWithIdentifier:@"missed"];
    missedVC.hidesBottomBarWhenPushed = TRUE;

    [self.navigationController pushViewController:missedVC animated:YES];
}

-(IBAction)accountClicked:(id)sender{
    AccountsAndSupportViewController *acoountVC = [self.storyboard instantiateViewControllerWithIdentifier:@"AccountAndSupportView"];
    acoountVC.hidesBottomBarWhenPushed = TRUE;

    [self.navigationController pushViewController:acoountVC animated:YES];
}

-(IBAction)logoutClicked:(id)sender{
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Pay1"message: @"Are you sure you want to logout?"delegate: self cancelButtonTitle:@"Yes" otherButtonTitles:@"No",nil];
    
    [alert show];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
   // NSLog(@"#RowIndex :%ld",(long)indexPath.row);
    
    if (indexPath.row == 8) {
        NSLog(@"Logout is clicked");
       
    }
    /*  if (indexPath.row == 7) {
     NSLog(@"Missed Call clicked");
     
     MissedCallViewController *homeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"missed"];
     UINavigationController* navController = (UINavigationController*)self.revealViewController.frontViewController;
     [navController setViewControllers:@[homeVC] animated: YES ];
     [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
     [self.revealViewController setHidesBottomBarWhenPushed:TRUE];
     [self.revealViewController.navigationController pushViewController:navController animated:YES];
     }*/
    
}


-(void)callSignOutService{
    [self showHUDOnView];
    SignOutService *signOutService = [[SignOutService alloc]init];
    [signOutService callSignOutService:^(NSError *error, SignOutResponse *signOutResponse) {
    if(error)return ;
    else{
    if([signOutResponse.status isEqualToString:@"success"]){
    //[Session shared].isLogoutDone = TRUE;
    NSLog(@"we get success from server");
    NSLog(@"The sign out response is%@",signOutResponse.status);
    [Constant saveUserPhoneNumber:@""];
    [Constant saveUserPassword:@""];
    [Constant saveMyGiftsCount:@"0"];
    [Constant saveUserCoinBalance:@""];
    [Constant saveUserWalletBalance:@""];
        
    [self hideHUDOnView];
                
    if([Session shared].isFromAutoLogin == TRUE){
   // [Session shared].isFromAutoLogin = FALSE;
      //  [Session shared].isFromSlide = TRUE;
        
       // [Session shared].isFromAutoLogin = FALSE;
        
        
        
        AppDelegate *app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        [app doLogout];
        
        
    }
    else{
       // [Session shared].isFromAutoLogin = FALSE;
    //[self.navigationController popToRootViewControllerAnimated:YES];
        
        AppDelegate *app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        [app doLogout];
    }
        
    }
    }
    }];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
    NSLog(@"user pressed Button Indexed 0");
    [self callSignOutService];
    }
    else
    {
    NSLog(@"user pressed Button Indexed 1");
    }
    
}

-(void)loginViewShowingLoggedInUser:(FBLoginView *)loginView{
    NSLog(@"You are logged in ");
}

-(void)loginViewFetchedUserInfo:(FBLoginView *)loginView user:(id<FBGraphUser>)user{
    NSLog(@"Info %@", user);
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=normal",[user objectForKey:@"id"]]];
    NSLog(@"User URL is :%@",url);
    
    if([Constant getFacebookCalledRecord] == FALSE){
    [Constant saveFaceBookCalledRecord:TRUE];
        
        
    [Session shared].objSignInResponse.userName = [NSString stringWithFormat:@"%@",[user objectForKey:@"name"]];
    [Session shared].objSignInResponse.userEmail = [NSString stringWithFormat:@"%@",[user objectForKey:@"email"]];
    [Session shared].objSignInResponse.userGender = [NSString stringWithFormat:@"%@",[user objectForKey:@"gender"]];
    [Session shared].objSignInResponse.profileImageURL = [url absoluteString];
        
        
        
        [self navigateToProfileView];
        
    }
}

-(void)loginViewShowingLoggedOutUser:(FBLoginView *)loginView{
    NSLog(@"You are logged out ");
    
}


-(void)loginView:(FBLoginView *)loginView handleError:(NSError *)error{
    NSLog(@"%@", [error localizedDescription]);
}



-(void)navigateToProfileView{
    ProfileViewController *profileVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
    profileVC.isFromFB = TRUE;
   /* UINavigationController* navController = (UINavigationController*)self.revealViewController.frontViewController;
    [navController setViewControllers:@[profileVC] animated: YES ];
    [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
    
    [self.revealViewController.navigationController pushViewController:navController animated:YES];*/
    
     [self.navigationController pushViewController:profileVC animated:YES];
}

-(void)walletClicked:(UITapGestureRecognizer*)sender{
    NSLog(@"#WalletClicked");
    
    
   /* [Session shared].isFromSlide = TRUE;
    WalletDetailViewController *walletDetailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"walletDetailViewC"];
    UINavigationController* navController = (UINavigationController*)self.revealViewController.frontViewController;
    [navController setViewControllers:@[walletDetailVC] animated: YES ];
    [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
    
    [self.revealViewController.navigationController pushViewController:navController animated:YES];*/
    
   // WalletDetailViewController *walletDetailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"walletDetailViewC"];
   // [self.navigationController pushViewController:walletDetailVC animated:YES];
    
    AppDelegate *appDeleagte = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
                [appDeleagte.tabBarController setSelectedIndex:3];
    
    
    /*  NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
     UINavigationController *destViewController = (UINavigationController*)segue.destinationViewController;
     
     
     
     if ( [segue isKindOfClass: [SWRevealViewControllerSegue class]] ) {
     SWRevealViewControllerSegue *swSegue = (SWRevealViewControllerSegue*) segue;
     
     swSegue.performBlock = ^(SWRevealViewControllerSegue* rvc_segue, UIViewController* svc, UIViewController* dvc) {
     
     UINavigationController* navController = (UINavigationController*)self.revealViewController.frontViewController;
     [navController setViewControllers: @[dvc] animated: NO ];
     [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
     };
     
     }*/
}

-(void)coinClicked:(UITapGestureRecognizer *)sender{
    NSLog(@"#CoinClicked");
    
   // [Session shared].isFromSlide = TRUE;
    
   /* [Session shared].isFromSlide = TRUE;
    
    CoinViewController *coinVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CoinView"];
    UINavigationController* navController = (UINavigationController*)self.revealViewController.frontViewController;
    [navController setViewControllers:@[coinVC] animated: YES ];
    [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
    
    [self.revealViewController.navigationController pushViewController:navController animated:YES];*/
    
    CoinViewController *coinVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CoinView"];
    coinVC.hidesBottomBarWhenPushed = TRUE;
    [self.navigationController pushViewController:coinVC animated:YES];
    
}



@end
