package com.pay1;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.content.res.Configuration;
import android.database.SQLException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.Settings.Secure;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.RotateAnimation;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionLoginBehavior;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.google.android.gcm.GCMRegistrar;
import com.larvalabs.svgandroid.SVG;
import com.larvalabs.svgandroid.SVGParser;
import com.pay1.adapterhandler.NavDrawerListAdapter;
import com.pay1.constants.Constants;
import com.pay1.constants.SVGConstant;
import com.pay1.customviews.ArcMenu;
import com.pay1.customviews.MyProgressDialog;
import com.pay1.databasehandler.FreeBiesDataSource;
import com.pay1.databasehandler.FreeBiesLocationDataSource;
import com.pay1.databasehandler.NumbberWithCircleDataSource;
import com.pay1.databasehandler.NumberWithCircle;
import com.pay1.databasehandler.Operator;
import com.pay1.databasehandler.OperatorDataSource;
import com.pay1.databasehandler.Plan;
import com.pay1.databasehandler.PlanDataSource;
import com.pay1.databasehandler.QuickPay;
import com.pay1.databasehandler.QuickPayDataSource;
import com.pay1.databasehandler.ReClaimGift;
import com.pay1.databasehandler.ReClaimGiftDataSource;
import com.pay1.model.NavDrawerItem;
import com.pay1.requesthandler.RequestClass;
import com.pay1.servicehandler.GPSTracker;
import com.pay1.utilities.MemoryManager;
import com.pay1.utilities.Utility;

public class MainActivity extends ActionBarActivity implements
		LocationListener, OnClickListener, OnSharedPreferenceChangeListener {

	private OperatorDataSource operatorDataSource;
	private static QuickPayDataSource quickPayDataSource;
	private NumbberWithCircleDataSource circleDataSource;
	private FreeBiesDataSource freeBiesDataSource;
	private FreeBiesLocationDataSource freeBiesLocationDataSource;
	private ReClaimGiftDataSource reClaimGiftDataSource;

	private int m_counter = 0;
	float m_lastTouchX, m_lastTouchY, m_posX, m_posY, m_prevX, m_prevY,
			m_imgXB, m_imgYB, m_imgXC, m_imgYC, m_dx, m_dy;

	private static DrawerLayout mDrawerLayout;
	private static ListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;

	// nav drawer title
	private CharSequence mDrawerTitle;

	// used to store app title
	private CharSequence mTitle;

	// slide menu items
	private String[] navMenuTitles;
	// private TypedArray navMenuIcons;

	private ArrayList<NavDrawerItem> navDrawerItems;
	private NavDrawerListAdapter adapter;
	Typeface Reguler;
	private static ImageView imageView_Menu;

	// private LoginButton loginBtn;
	private UiLifecycleHelper uiHelper;

	// private boolean doubleBackToExitPressedOnce = false;
	boolean fromImageClick = false;

	private static Animation createHintSwitchAnimation(final boolean expanded) {
		Animation animation = new RotateAnimation(expanded ? 45 : 0,
				expanded ? 0 : 45, Animation.RELATIVE_TO_SELF, 0.5f,
				Animation.RELATIVE_TO_SELF, 0.5f);
		animation.setStartOffset(0);
		animation.setDuration(100);
		animation.setInterpolator(new DecelerateInterpolator());
		// animation.setFillAfter(true);

		return animation;
	}

	private Session.StatusCallback statusCallback = new Session.StatusCallback() {
		@Override
		public void call(Session session, SessionState state,
				Exception exception) {
			if (state.isOpened()) {
				// System.out.println("FB " + "Facebook session opened.");
				// new UpdateProfileTask().execute();
				Request request = Request.newMeRequest(session,
						new Request.GraphUserCallback() {
							@Override
							public void onCompleted(GraphUser user,
									Response response) {
								if (user != null) {

									if (fromImageClick) {

										Intent intent = new Intent(
												MainActivity.this,
												ProfileActivity.class);
										intent.putExtra("fbData", response
												.getGraphObject()
												.getInnerJSONObject()
												.toString());
										intent.putExtra("fromFB", true);
										startActivity(intent);
										fromImageClick = false;
									}

								} else {
									Log.i("TAG", "Logged out...");
								}

							}
						});

				Request.executeBatchAsync(request);

			} else if (state.isClosed()) {
				System.out.println("FB " + "Facebook session closed.");
			}
		}
	};

	@Override
	public void onResume() {
		super.onResume();
		uiHelper.onResume();
		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
	}

	@Override
	public void onPause() {
		super.onPause();
		uiHelper.onPause();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		uiHelper.onDestroy();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		uiHelper.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void onSaveInstanceState(Bundle savedState) {
		super.onSaveInstanceState(savedState);
		uiHelper.onSaveInstanceState(savedState);
	}

	private static final String LOGCAT = null;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		uiHelper = new UiLifecycleHelper(this, statusCallback);

		uiHelper.onCreate(savedInstanceState);

		Session session = Session.getActiveSession();
		if (session == null) {
			if (savedInstanceState != null) {
				session = Session.restoreSession(MainActivity.this, null,
						statusCallback, savedInstanceState);
			}
			if (session == null) {
				session = new Session(MainActivity.this);
			}
			Session.setActiveSession(session);
		}

		fromImageClick = false;
		setContentView(R.layout.main_activity);
		FrameLayout frame_main = (FrameLayout) findViewById(R.id.frame_main);
		
		
		
		/*
		 * frame_main.setOnDragListener(new OnDragListener() {
		 * 
		 * @Override public boolean onDrag(View layoutview, DragEvent dragevent)
		 * { int action = dragevent.getAction(); FrameLayout.LayoutParams params
		 * = (android.widget.FrameLayout.LayoutParams)
		 * layoutview.getLayoutParams(); switch (action) { case
		 * DragEvent.ACTION_DRAG_STARTED: Log.d(LOGCAT, "Drag event started");
		 * 
		 * break; case DragEvent.ACTION_DRAG_ENTERED: Log.d(LOGCAT,
		 * "Drag event entered into " + layoutview.toString()); break; case
		 * DragEvent.ACTION_DRAG_EXITED: Log.d(LOGCAT, "Drag event exited from "
		 * + layoutview.toString()); break; case DragEvent.ACTION_DROP:
		 * Log.d(LOGCAT, "Dropped"); View view = (View)
		 * dragevent.getLocalState(); ViewGroup owner = (ViewGroup)
		 * view.getParent(); owner.removeView(view); FrameLayout container =
		 * (FrameLayout) layoutview; params.topMargin=150;
		 * params.leftMargin=150; container.addView(view,params);
		 * view.setVisibility(View.VISIBLE); break; case
		 * DragEvent.ACTION_DRAG_ENDED: Log.d(LOGCAT, "Drag ended"); break;
		 * default: break; } return true; } });
		 */
		Reguler = Typeface.createFromAsset(MainActivity.this.getAssets(),
				"Roboto-Regular.ttf");

		if (android.os.Build.VERSION.SDK_INT > 9) {
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
					.permitAll().build();
			StrictMode.setThreadPolicy(policy);
		}

		FrameLayout frame_container = (FrameLayout) findViewById(R.id.frame_container);
		Constants.setFont(MainActivity.this, frame_container);
		/*
		 * loginBtn = (LoginButton) findViewById(R.id.fb_login_button);
		 * loginBtn.setReadPermissions(Arrays
		 * .asList("user_about_me, user_friends, email"));
		 * 
		 * loginBtn.setLoginBehavior(SessionLoginBehavior.SUPPRESS_SSO);
		 * loginBtn.setUserInfoChangedCallback(new UserInfoChangedCallback() {
		 * 
		 * @Override public void onUserInfoFetched(GraphUser user) { if (user !=
		 * null) { System.out.println("FB " + " You are currently logged in as "
		 * + user.getName()); } else { System.out.println("FB " +
		 * "You are not logged in."); } } });
		 */

		try {

			operatorDataSource = new OperatorDataSource(MainActivity.this);
			quickPayDataSource = new QuickPayDataSource(MainActivity.this);
			freeBiesDataSource = new FreeBiesDataSource(MainActivity.this);
			freeBiesLocationDataSource = new FreeBiesLocationDataSource(
					MainActivity.this);
			circleDataSource = new NumbberWithCircleDataSource(
					MainActivity.this);
			reClaimGiftDataSource = new ReClaimGiftDataSource(MainActivity.this);

			Utility.setOneTimeRegistration(MainActivity.this,
					Constants.SHAREDPREFERENCE_REGISTRATION, true);

			Typeface Reguler = Typeface.createFromAsset(getAssets(),
					"Roboto-Regular.ttf");

			// MemoryManager.clearCache(MainActivity.this);
			//
			// Debug.MemoryInfo memoryInfo = new Debug.MemoryInfo();
			// Debug.getMemoryInfo(memoryInfo);
			//
			// String memMessage = String.format(
			// "Memory: Pss=%.2f MB, Private=%.2f MB, Shared=%.2f MB",
			// memoryInfo.getTotalPss() / 1024.0,
			// memoryInfo.getTotalPrivateDirty() / 1024.0,
			// memoryInfo.getTotalSharedDirty() / 1024.0);
			// System.out.println("Mem " + memMessage);
			//
			// System.out.println("Mem "
			// + MemoryManager.isMemoryLow(MainActivity.this));

			// ArcMenu arcMenu = (ArcMenu) findViewById(R.id.button_New);
			// initArcMenu(arcMenu);

			imageView_Menu = (ImageView) findViewById(R.id.imageView_Menu);

			/*
			 * imageView_Menu.setOnTouchListener(new OnTouchListener() {
			 * 
			 * public boolean onTouch(View view, MotionEvent motionEvent) { if
			 * (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
			 * DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(
			 * view); view.startDrag(null, shadowBuilder, view, 0);
			 * view.setVisibility(View.INVISIBLE); return true; } else { return
			 * false; } } });
			 */
			imageView_Menu.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					imageView_Menu
							.startAnimation(createHintSwitchAnimation(true));
					showMenuDialog();
				}
			});

			try {
				operatorDataSource.open();

				Operator operator = operatorDataSource.getTopOperator();

				if (operator != null) {
					int days = (int) ((System.currentTimeMillis() - operator
							.getOperatorUptateTime()) / (1000 * 60 * 60 * 24));
					if (days >= 2) {
						new GetOperatorTask().execute();
					}
				} else {
					new GetOperatorTask().execute();
				}

			} catch (SQLException exception) {
				// TODO: handle exception
				// // // Log.e("Er ", exception.getMessage());
			} catch (Exception e) {
				// TODO: handle exception
				// // // Log.e("Er ", e.getMessage());
			} finally {
				operatorDataSource.close();
			}

			try {
				circleDataSource.open();

				NumberWithCircle operator_code = circleDataSource
						.getTopNumberDetails();

				if (operator_code != null) {
					int days = (int) ((System.currentTimeMillis() - operator_code
							.getLastUpdateTime()) / (1000 * 60 * 60 * 24));
					if (days >= 10) {
						/*
						 * Intent circleServiceIntent = new Intent(
						 * MainActivity.this, CircleIntentService.class);
						 * startService(circleServiceIntent);
						 */
						// new GetOperatorCodeTask().execute();
					}
				} else {
					// Intent circleServiceIntent = new
					// Intent(MainActivity.this,
					// CircleIntentService.class);
					// startService(circleServiceIntent);
					new GetOperatorCodeTask().execute();
				}

			} catch (SQLException exception) {
				// TODO: handle exception
				// // // Log.e("Er ", exception.getMessage());
			} catch (Exception e) {
				// TODO: handle exception
				// // // Log.e("Er ", e.getMessage());
			} finally {
				circleDataSource.close();
			}

			/*
			 * try { freeBiesDataSource.open();
			 * 
			 * FreeBies freebies = freeBiesDataSource.getTopFreeBie();
			 * 
			 * if (freebies != null) { int days = (int)
			 * ((System.currentTimeMillis() - freebies .getFreebieUptateTime())
			 * / (1000 * 60 * 60 * 24)); if (days >= 1) { startService(new
			 * Intent(MainActivity.this, FreeBiesUpdateService.class)); } } else
			 * { // startService(new Intent(MainActivity.this, //
			 * FreeBiesService.class)); //new FreeBiesTask().execute(); }
			 * 
			 * } catch (SQLException exception) { // TODO: handle exception //
			 * // // Log.e("Er ", exception.getMessage()); } catch (Exception e)
			 * { // TODO: handle exception // // // Log.e("Er ",
			 * e.getMessage()); } finally { freeBiesDataSource.close(); }
			 */

			mTitle = mDrawerTitle = getTitle();

			// load slide menu items
			navMenuTitles = getResources().getStringArray(
					R.array.nav_drawer_items);

			// nav drawer icons from resources
			// navMenuIcons = getResources().obtainTypedArray(
			// R.array.nav_drawer_icons);

			mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
			mDrawerList = (ListView) findViewById(R.id.list_slidermenu);

			
			final View activityRootView = findViewById(R.id.frame_main);
			frame_main.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
			  @Override
			  public void onGlobalLayout() {
				  
				  if (mDrawerLayout.isDrawerOpen(mDrawerList)) {
					  imageView_Menu.setVisibility(View.GONE);
					}else{
						
						
				  Rect r = new Rect();
				    //r will be populated with the coordinates of your view that area still visible.
				    activityRootView.getWindowVisibleDisplayFrame(r);

				    int heightDiff = activityRootView.getRootView().getHeight() - (r.bottom - r.top);
				    if (heightDiff > 100) { // if more than 100 pixels, its probably a keyboard...
				      imageView_Menu.setVisibility(View.GONE);
				    }else{
									if (mDrawerList.getCheckedItemPosition() == 1
											|| mDrawerList
													.getCheckedItemPosition() == 8) {
										imageView_Menu.setVisibility(View.GONE);
									} else {
										imageView_Menu
												.setVisibility(View.VISIBLE);
									}
				    	  //imageView_Menu.setVisibility(View.VISIBLE);
				    }
					}
			  }
			});
			
			
			
			if (mDrawerList.getCheckedItemPosition() == 8) {
				TextView textFragment = (TextView) findViewById(R.id.textViewBalance);

				DecimalFormat decimalFormat = new DecimalFormat("0.00");
				String bal = Utility.getBalance(MainActivity.this,
						Constants.SHAREDPREFERENCE_BALANCE);
				bal = bal == null ? "0" : bal;

				textFragment.setText("Rs."
						+ decimalFormat.format(Double.parseDouble(bal)));
			}

			navDrawerItems = new ArrayList<NavDrawerItem>();

			// adding nav drawer items to array
			// Home
			for (int i = 0; i < navMenuTitles.length; i++) {
				// if (i == 6) {
				// navDrawerItems.add(new NavDrawerItem(navMenuTitles[i], 0,
				// true, ""));
				// } else {
				navDrawerItems.add(new NavDrawerItem(navMenuTitles[i], 0));
				// }
			}
			// Find People
			// navDrawerItems.add(new NavDrawerItem(navMenuTitles[1],
			// navMenuIcons
			// .getResourceId(1, -1)));
			// // Photos
			// navDrawerItems.add(new NavDrawerItem(navMenuTitles[2],
			// navMenuIcons
			// .getResourceId(2, -1)));
			// // Communities, Will add a counter here
			// navDrawerItems.add(new NavDrawerItem(navMenuTitles[3],
			// navMenuIcons
			// .getResourceId(3, -1), true, "22"));
			// // Pages
			// navDrawerItems.add(new NavDrawerItem(navMenuTitles[4],
			// navMenuIcons
			// .getResourceId(4, -1)));
			// // What's hot, We will add a counter here
			// navDrawerItems.add(new NavDrawerItem(navMenuTitles[5],
			// navMenuIcons
			// .getResourceId(5, -1), true, "50+"));

			// Recycle the typed array
			// navMenuIcons.recycle();

			mDrawerList.setOnItemClickListener(new SlideMenuClickListener());

			// setting the nav drawer list adapter
			adapter = new NavDrawerListAdapter(getApplicationContext(),
					navDrawerItems, this);
			mDrawerList.setAdapter(adapter);

			// enabling action bar app icon and behaving it as toggle button
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);
			getSupportActionBar().setDisplayUseLogoEnabled(true);
			getSupportActionBar().setIcon(
					this.getResources().getDrawable(R.drawable.ic_profile));
			getSupportActionBar().setHomeButtonEnabled(true);
			getSupportActionBar().setTitle("");
			mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
					R.drawable.ic_drawer, R.string.empty_string,
					R.string.empty_string) {
				public void onDrawerClosed(View view) {
					if (mDrawerList.getCheckedItemPosition() == 1
							|| mDrawerList.getCheckedItemPosition() == 8) {
						imageView_Menu.setVisibility(View.GONE);
					} else {
						imageView_Menu.setVisibility(View.VISIBLE);
					}

					// getSupportActionBar().setTitle(mTitle);
					// calling onPrepareOptionsMenu() to show action bar icons
					supportInvalidateOptionsMenu();
				}

				public void onDrawerOpened(View drawerView) {

					
					adapter.notifyDataSetChanged();
					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(drawerView.getWindowToken(), 0);
					// getSupportActionBar().setTitle(mDrawerTitle);
					// calling onPrepareOptionsMenu() to hide action bar icons
					supportInvalidateOptionsMenu();
					imageView_Menu.setVisibility(View.GONE);
				}
			};
			mDrawerLayout.setDrawerListener(mDrawerToggle);

			if (savedInstanceState == null) {
				// on first time display view for first nav item

				try {
					quickPayDataSource.open();

					QuickPay list = quickPayDataSource.getTopQuickPay();

					// boolean flag_New = getIntent().getBooleanExtra("IS_FLAG",
					// false);
					// boolean flag_MyGift = getIntent().getBooleanExtra(
					// "IS_MYGIFT", false);
					boolean flag_Quickpay = getIntent().getBooleanExtra(
							"FROM_QUICKPAY", false);
					// if (flag_MyGift)
					// displayView(4);
					// else
					if (flag_Quickpay) {
						boolean flag_gift = getIntent().getBooleanExtra(
								"IS_GIFT", false);
						if (flag_gift) {
							displayView(3);
						} else {
							int showView = getIntent().getIntExtra("ShowView",
									2);
							displayView(showView);
						}
					} else if (list == null
							|| !Utility.getLoginFlag(MainActivity.this,
									Constants.SHAREDPREFERENCE_IS_LOGIN))
						displayView(2);
					else {
						displayView(1);
						new CheckUpdateTask().execute();
						new CheckBalanceTask().execute();
						try {
							reClaimGiftDataSource.open();

							List<ReClaimGift> reClaimGifts = reClaimGiftDataSource
									.getAllReClaimGift(1);
							int size = reClaimGifts.size();
							if (size != 0) {
								for (ReClaimGift reClaimGift : reClaimGifts) {
									if (reClaimGift.getReClaimCounter() != 0) {
										Constants
												.Notify(MainActivity.this,
														"Pay1",
														"Grab your free gift.",
														reClaimGift
																.getReClaimID(),
														reClaimGift
																.getReClaimTransactionID(),
														reClaimGift
																.getReClaimRechargeAmount());
									}
								}
							}
						} catch (SQLException exception) {
							// TODO: handle exception
							Log.e("Er ", exception.getMessage());
						} catch (Exception e) {
							// TODO: handle exception
							// // // Log.e("Er ", e.getMessage());
						} finally {
							reClaimGiftDataSource.close();
						}
					}

				} catch (Exception e) {
					// TODO: handle exception
					// // // Log.e("Er ", e.getMessage());
					displayView(1);
					new CheckUpdateTask().execute();
					new CheckBalanceTask().execute();
					try {
						reClaimGiftDataSource.open();

						List<ReClaimGift> reClaimGifts = reClaimGiftDataSource
								.getAllReClaimGift(1);
						int size = reClaimGifts.size();
						if (size != 0) {
							for (ReClaimGift reClaimGift : reClaimGifts) {
								if (reClaimGift.getReClaimCounter() != 0) {
									Constants
											.Notify(MainActivity.this,
													"Pay1",
													"Grab your free gift.",
													reClaimGift.getReClaimID(),
													reClaimGift
															.getReClaimTransactionID(),
													reClaimGift
															.getReClaimRechargeAmount());
								}
							}
						}
					} catch (Exception ex) {
						// TODO: handle exception
						// // // Log.e("Er ", e.getMessage());
					} finally {
						reClaimGiftDataSource.close();
					}
				} finally {
					quickPayDataSource.close();
				}
			}

			GCMRegistrar.checkDevice(this);
			GCMRegistrar.checkManifest(this);
			checkNotNull(Constants.SENDER_ID, "SENDER_ID");

			try {
				String uuid;
				TelephonyManager tManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
				uuid = tManager.getDeviceId();
				if (uuid == null) {
					uuid = Secure.getString(
							MainActivity.this.getContentResolver(),
							Secure.ANDROID_ID);
				}
				if (Utility.getUUID(MainActivity.this,
						Constants.SHAREDPREFERENCE_UUID) == null)
					Utility.setUUID(MainActivity.this,
							Constants.SHAREDPREFERENCE_UUID, uuid);
			} catch (Exception exception) {
			}

			final String regId = GCMRegistrar.getRegistrationId(this);
			// // // Log.i(TAG, "registration id =====  " + regId);
			Utility.setGCMID(MainActivity.this,
					Constants.SHAREDPREFERENCE_GCMID, regId);
			if (regId.equals("")) {
				GCMRegistrar.register(this, Constants.SENDER_ID);
			} else {
				// // // Log.v(TAG, "Already registered");
			}

			// try {
			// LocationResult locationResult = new LocationResult() {
			// @Override
			// public void gotLocation(Location location) {
			// // Got the location!
			// try {
			// if (location != null) {
			// Utility.setCurrentLatitude(
			// MainActivity.this,
			// Constants.SHAREDPREFERENCE_CURRENT_LATITUDE,
			// String.valueOf(location.getLatitude()));
			// Utility.setCurrentLongitude(
			// MainActivity.this,
			// Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE,
			// String.valueOf(location.getLongitude()));
			// }
			// } catch (Exception exception) {
			// }
			// }
			// };
			// MyLocation myLocation = new MyLocation();
			// myLocation.getLocation(this, locationResult);
			// } catch (Exception exception) {
			// }

			if (getIntent().getExtras().containsKey("bal")) {
				if (getIntent().getExtras().getString("bal")
						.equalsIgnoreCase("1")) {
					Constants.showFullOneButtonDialog(MainActivity.this,
							"Notification",
							getIntent().getExtras().getString("desc").trim(),
							Constants.DIALOG_CLOSE);
					new CheckBalanceTask().execute();
				}
			}
			if (getIntent().getExtras().containsKey("offer_id")) {
				displayView(3);
			}
			if (!MemoryManager.isMemoryLow(MainActivity.this)) {
				/*
				 * if (getIntent().getExtras().containsKey(
				 * "isFirstTimeAfterUpdate")) { if
				 * (getIntent().getExtras().getBoolean(
				 * "isFirstTimeAfterUpdate")) { startService(new
				 * Intent(MainActivity.this, PlanIntentService.class));
				 * Utility.saveFirstTimeAfterUpdate(MainActivity.this, false); }
				 * }
				 * 
				 * else {
				 */

				PlanDataSource planDataSource = new PlanDataSource(
						MainActivity.this);
				try {

					planDataSource.open();

					Plan plans = planDataSource.getTopPlan();
					if (plans != null) {
						int days = (int) ((System.currentTimeMillis() - Long
								.parseLong(plans.getPlanUptateTime())) / (1000 * 60 * 60 * 24));
						if (days >= 2) {
							/*
							 * startService(new Intent(MainActivity.this,
							 * PlanIntentService.class));
							 */
						}
					} else {
						/*
						 * startService(new Intent(MainActivity.this,
						 * PlanIntentService.class));
						 */
					}
				} catch (Exception e) {
					/*
					 * startService(new Intent(MainActivity.this,
					 * PlanIntentService.class));
					 */
				} finally {
					planDataSource.close();
				}
				// }
			}
			try {
				LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

				// Creating a criteria object to retrieve provider
				Criteria criteria = new Criteria();

				// Getting the name of the best provider
				String provider = locationManager.getBestProvider(criteria,
						true);
				locationManager.requestLocationUpdates(
						LocationManager.GPS_PROVIDER, 60 * 10, 1000f,
						mLocationListener);

				// Getting Current Location
				Location location = locationManager
						.getLastKnownLocation(provider);

				if (location != null) {
					onLocationChanged(location);
				}
				locationManager
						.requestLocationUpdates(provider, 20000, 0, this);
			} catch (Exception e) {
			}
		} catch (Exception e) {
		}

		GPSTracker gps = new GPSTracker(this);
		if (gps.canGetLocation()) { // gps enabled} // return boolean true/false

			gps.getLatitude(); // returns latitude
			gps.getLongitude(); // returns longitude

			Utility.setCurrentLatitude(MainActivity.this,
					Constants.SHAREDPREFERENCE_CURRENT_LATITUDE,
					String.valueOf(gps.getLatitude()));
			Utility.setCurrentLongitude(MainActivity.this,
					Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE,
					String.valueOf(gps.getLongitude()));
		}

	}

	private final LocationListener mLocationListener = new LocationListener() {
		@Override
		public void onLocationChanged(final Location location) {
			// your code here
			if (location != null) {

				double latitude = location.getLatitude();

				// Getting longitude of the current location
				double longitude = location.getLongitude();

				/*
				 * Utility.setCurrentLatitude(MainActivity.this,
				 * Constants.SHAREDPREFERENCE_CURRENT_LATITUDE,
				 * String.valueOf(latitude));
				 * Utility.setCurrentLongitude(MainActivity.this,
				 * Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE,
				 * String.valueOf(longitude));
				 */

			}
		}

		@Override
		public void onProviderDisabled(String arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onProviderEnabled(String provider) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
			// TODO Auto-generated method stub

		}
	};

	/**
	 * Slide menu item click listener
	 * */
	private class SlideMenuClickListener implements
			ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			// display view for selected nav drawer item
			mDrawerList.smoothScrollToPosition(0);
			if (position == 0) {
				ImageView tv = (ImageView) view
						.findViewById(R.id.imageSettings);// mDrawerList.getChildAt(position);
				tv.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						// Toast.makeText(MainActivity.this, "Clicked",
						// Toast.LENGTH_SHORT).show();
						displayView(0);

						/*
						 * Session session = new Session(MainActivity.this);
						 * Session.OpenRequest openRequest = new
						 * Session.OpenRequest( MainActivity.this);
						 * openRequest.setPermissions(Arrays
						 * .asList("user_about_me, user_friends, email"));
						 * openRequest
						 * .setLoginBehavior(SessionLoginBehavior.SUPPRESS_SSO);
						 * openRequest.setCallback(statusCallback);
						 * Session.setActiveSession(session);
						 * session.openForPublish(openRequest);
						 */
						// session.openActiveSession(MainActivity.this, true,
						// statusCallback);

					}
				});

			} else {
				displayView(position);
			}
		}
	}

	@SuppressLint("NewApi")
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.clear();
		getMenuInflater().inflate(R.menu.main, menu);

		// if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
		//
		// SearchManager searchManager = (SearchManager)
		// getSystemService(Context.SEARCH_SERVICE);
		// SearchView searchView = (SearchView) menu
		// .findItem(R.id.menu_search).getActionView();
		// searchView.setSearchableInfo(searchManager
		// .getSearchableInfo(getComponentName()));
		//
		// // searchView.setOnQueryTextListener(new OnQueryTextListener() {
		// //
		// // @Override
		// // public boolean onQueryTextChange(String query) {
		// //
		// // return true;
		// //
		// // }
		// //
		// // @Override
		// // public boolean onQueryTextSubmit(String query) {
		// // // TODO Auto-generated method stub
		// // Intent intent = new Intent(MainActivity.this,
		// // GiftSearchActivity.class);
		// // intent.putExtra("QUERY", query);
		// // startActivity(intent);
		// // return false;
		// // }
		// //
		// // });
		//
		// }
		try {
			MenuItem item = menu.findItem(R.id.menu_wallet);
			MenuItemCompat.setActionView(item, R.layout.bal_wall);
			View view = MenuItemCompat.getActionView(item);

			TextView tv = (TextView) view.findViewById(R.id.textView_Count);
			tv.setTypeface(Reguler);
			ImageView image = (ImageView) view
					.findViewById(R.id.imageView_Icon);
			DecimalFormat decimalFormat = new DecimalFormat("0.00");
			String bal = Utility.getBalance(MainActivity.this,
					Constants.SHAREDPREFERENCE_BALANCE);
			bal = bal == null ? "0" : bal;

			tv.setText("Rs." + decimalFormat.format(Double.parseDouble(bal)));
			image.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					displayView(11);
				}
			});

			tv.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					displayView(11);
				}
			});

			item.setOnMenuItemClickListener(new OnMenuItemClickListener() {

				@Override
				public boolean onMenuItemClick(MenuItem item) {
					// TODO Auto-generated method stub
					displayView(11);
					return false;
				}
			});

			// tv.setOnClickListener(new View.OnClickListener() {
			//
			// @Override
			// public void onClick(View v) {
			// // TODO Auto-generated method stub
			// showWalletRefreshDialog(1);
			// }
			// });
			// view.setOnClickListener(new View.OnClickListener() {
			//
			// @Override
			// public void onClick(View v) {
			// // TODO Auto-generated method stub
			// showWalletRefreshDialog(1);
			// }
			// });
			// ImageView imageView_Icon = (ImageView) view
			// .findViewById(R.id.imageView_Icon);
			// imageView_Icon.setOnClickListener(new View.OnClickListener() {
			//
			// @Override
			// public void onClick(View v) {
			// // TODO Auto-generated method stub
			// showWalletRefreshDialog(1);
			// }
			// });
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		try {
			MenuItem item = menu.findItem(R.id.menu_points);
			MenuItemCompat.setActionView(item, R.layout.bal_coin);
			View view = MenuItemCompat.getActionView(item);

			TextView tv = (TextView) view.findViewById(R.id.textView_Count);
			tv.setTypeface(Reguler);
			ImageView image = (ImageView) view
					.findViewById(R.id.imageView_Icon);
			DecimalFormat decimalFormat = new DecimalFormat("0");
			String bal = Utility.getBalance(MainActivity.this,
					Constants.SHAREDPREFERENCE_LOYALTY_POINTS);
			bal = bal == null ? "0" : bal;

			tv.setText("" + decimalFormat.format(Double.parseDouble(bal)));
			image.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent intent = new Intent(MainActivity.this,
							CoinHistoryActivity.class);
					startActivity(intent);
				}
			});
			tv.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent intent = new Intent(MainActivity.this,
							CoinHistoryActivity.class);
					startActivity(intent);
				}
			});

			item.setOnMenuItemClickListener(new OnMenuItemClickListener() {

				@Override
				public boolean onMenuItemClick(MenuItem item) {
					// TODO Auto-generated method stub
					Intent intent = new Intent(MainActivity.this,
							CoinHistoryActivity.class);
					startActivity(intent);
					return false;
				}
			});
			//
			// @Override
			// public void onClick(View v) {
			// tv.setOnClickListener(new View.OnClickListener() {
			//
			// @Override
			// public void onClick(View v) {
			// // TODO Auto-generated method stub
			// showWalletRefreshDialog(2);
			// }
			// });
			// view.setOnClickListener(new View.OnClickListener() {
			//
			// @Override
			// public void onClick(View v) {
			// // TODO Auto-generated method stub
			// showWalletRefreshDialog(2);
			// }
			// });
			// ImageView imageView_Icon = (ImageView) view
			// .findViewById(R.id.imageView_Icon);
			// imageView_Icon.setOnClickListener(new View.OnClickListener() {
			//
			// @Override
			// public void onClick(View v) {
			// // TODO Auto-generated method stub
			// showWalletRefreshDialog(2);
			// }
			// });
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// toggle nav drawer on selecting action bar app icon/title
		Log.d("ddd", "sg");
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		// Handle action bar actions click
		switch (item.getItemId()) {
		case R.id.menu_wallet: {
			displayView(11);
			// showWalletRefreshDialog(1);
			// if (!isOpened) {
			// final Animation animation = AnimationUtils.loadAnimation(
			// MainActivity.this, R.anim.translate_down);
			//
			// animation.setAnimationListener(new AnimationListener() {
			//
			// public void onAnimationStart(Animation animation) {
			// }
			//
			// public void onAnimationRepeat(Animation animation) {
			// }
			//
			// public void onAnimationEnd(Animation animation) {
			//
			// LayoutParams params = (LayoutParams) mDrawerLayout
			// .getLayoutParams();
			// params.topMargin = 200;
			// mDrawerLayout.setLayoutParams(params);
			// mDrawerLayout.clearAnimation();
			// isOpened = true;
			// }
			// });
			// mDrawerLayout.startAnimation(animation);
			// } else {
			//
			// LayoutParams params = new LayoutParams(
			// LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
			// params.topMargin = 0;
			// mDrawerLayout.setLayoutParams(params);
			//
			// final Animation animation = AnimationUtils.loadAnimation(
			// MainActivity.this, R.anim.translate_up);
			//
			// animation.setAnimationListener(new AnimationListener() {
			//
			// public void onAnimationStart(Animation animation) {
			// }
			//
			// public void onAnimationRepeat(Animation animation) {
			// }
			//
			// public void onAnimationEnd(Animation animation) {
			//
			// mDrawerLayout.clearAnimation();
			// isOpened = false;
			// }
			// });
			// mDrawerLayout.startAnimation(animation);
			// }
			return true;

		}
		case R.id.menu_points: {
			// showWalletRefreshDialog(2);

			return true;
		}
		case R.id.menu_search: {
			showSearchDialog();
			return true;
		}
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	// private void showWalletRefreshDialog(final int type) {
	// final Dialog dialog = new Dialog(this, android.R.style.Theme_Dialog);
	//
	// // Setting dialogview
	// Window window = dialog.getWindow();
	// window.requestFeature(Window.FEATURE_NO_TITLE);
	// // window.setGravity(Gravity.TOP);
	//
	// // window.setLayout(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
	//
	// dialog.getWindow().setBackgroundDrawableResource(
	// android.R.color.transparent);
	// WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
	// params.y = 50;
	// params.gravity = Gravity.TOP;
	// dialog.getWindow().setAttributes(params);
	// dialog.setTitle(null);
	// dialog.setContentView(R.layout.check_activity);
	// dialog.setCancelable(true);
	//
	// TextView textView_TopTitle, textView_TopBalance;
	// Button button_TopRefresh, button_AddMoney;
	//
	// Typeface Reguler = Typeface.createFromAsset(getAssets(),
	// "EncodeSansNormal-400-Regular.ttf");
	//
	// textView_TopTitle = (TextView) dialog
	// .findViewById(R.id.textView_TopTitle);
	// textView_TopTitle.setTypeface(Reguler);
	// textView_TopBalance = (TextView) dialog
	// .findViewById(R.id.textView_TopBalance);
	// textView_TopBalance.setTypeface(Reguler);
	// button_TopRefresh = (Button) dialog
	// .findViewById(R.id.button_TopRefresh);
	// button_TopRefresh.setTypeface(Reguler);
	// button_AddMoney = (Button) dialog.findViewById(R.id.button_AddMoney);
	// button_AddMoney.setTypeface(Reguler);
	//
	// if (type == 1) {
	// DecimalFormat decimalFormat = new DecimalFormat("0.00");
	// String bal = Utility.getBalance(MainActivity.this,
	// Constants.SHAREDPREFERENCE_BALANCE);
	// bal = bal == null ? "0" : bal;
	//
	// textView_TopTitle.setText("Pay1 Money");
	// textView_TopBalance.setText("Balance Rs "
	// + decimalFormat.format(Double.parseDouble(bal)));
	// } else {
	// button_AddMoney.setVisibility(View.GONE);
	// DecimalFormat decimalFormat = new DecimalFormat("0");
	// String bal = Utility.getBalance(MainActivity.this,
	// Constants.SHAREDPREFERENCE_LOYALTY_POINTS);
	// bal = bal == null ? "0" : bal;
	// textView_TopTitle.setText("Gift Coins");
	// textView_TopBalance.setText("Balance "
	// + decimalFormat.format(Double.parseDouble(bal)));
	// }
	// button_TopRefresh.setOnClickListener(new View.OnClickListener() {
	//
	// @Override
	// public void onClick(View v) {
	// // TODO Auto-generated method stub
	// dialog.dismiss();
	// new CheckBalanceLoaderTask().execute(String.valueOf(type));
	// }
	// });
	// button_AddMoney.setOnClickListener(new View.OnClickListener() {
	//
	// @Override
	// public void onClick(View v) {
	// // TODO Auto-generated method stub
	// dialog.dismiss();
	// displayView(11);
	// }
	// });
	//
	// dialog.show();
	// }

	private void showSearchDialog() {
		final Dialog dialog = new Dialog(this, android.R.style.Theme_Dialog);

		// Setting dialogview
		Window window = dialog.getWindow();
		window.requestFeature(Window.FEATURE_NO_TITLE);
		window.setGravity(Gravity.TOP);
		dialog.getWindow().setBackgroundDrawableResource(
				android.R.color.transparent);
		// window.setLayout(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
		// WindowManager.LayoutParams params =
		// dialog.getWindow().getAttributes();
		// params.y = 50;
		// params.gravity = Gravity.TOP;
		// dialog.getWindow().setAttributes(params);
		dialog.setTitle(null);
		dialog.setContentView(R.layout.search_activity);
		dialog.setCancelable(true);

		final EditText editText_Search;

		Typeface Reguler = Typeface.createFromAsset(getAssets(),
				"Roboto-Regular.ttf");

		editText_Search = (EditText) dialog.findViewById(R.id.editText_Search);
		editText_Search.setTypeface(Reguler);
		// InputMethodManager imm = (InputMethodManager)
		// getSystemService(Context.INPUT_METHOD_SERVICE);
		// imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
		editText_Search.postDelayed(new Runnable() {
			@Override
			public void run() {
				InputMethodManager keyboard = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				keyboard.showSoftInput(editText_Search, 0);
			}
		}, 200);
		editText_Search
				.setOnEditorActionListener(new TextView.OnEditorActionListener() {

					@Override
					public boolean onEditorAction(TextView v, int actionId,
							KeyEvent event) {
						// TODO Auto-generated method stub
						if (actionId == EditorInfo.IME_ACTION_SEARCH) {
							InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
							imm.hideSoftInputFromWindow(
									editText_Search.getWindowToken(),
									InputMethodManager.RESULT_UNCHANGED_SHOWN);
							Intent intent = new Intent(MainActivity.this,
									GiftSearchActivity.class);
							intent.putExtra("QUERY", editText_Search.getText()
									.toString().trim());
							startActivity(intent);
							return true;
						}
						return false;
					}
				});

		dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {

			@Override
			public void onDismiss(DialogInterface dialog) {
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
			}
		});
		dialog.show();
	}

	@SuppressLint("NewApi")
	private void showMenuDialog() {
		final Dialog dialog = new Dialog(this, android.R.style.Theme_Dialog);

		// Setting dialogview
		Window window = dialog.getWindow();
		window.requestFeature(Window.FEATURE_NO_TITLE);
		window.setGravity(Gravity.BOTTOM | Gravity.RIGHT);
		dialog.getWindow().setBackgroundDrawableResource(
				android.R.color.transparent);
		// window.setLayout(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
		// WindowManager.LayoutParams params =
		// dialog.getWindow().getAttributes();
		// params.y = 50;
		// params.gravity = Gravity.TOP;
		// dialog.getWindow().setAttributes(params);
		dialog.setTitle(null);
		dialog.setContentView(R.layout.menu_activity);
		dialog.setCancelable(true);
		imageView_Menu.setVisibility(View.GONE);

		final TextView textView_Home, textView_Recharge, textView_Gift, textView_Money;
		final ImageView imageView_Menu, imageView_Home, imageView_Recharge, imageView_Gift, imageView_Money;
		imageView_Menu = (ImageView) dialog.findViewById(R.id.imageView_Menu);
		
		Animation rotation = AnimationUtils.loadAnimation(this,
				R.anim.rotate_animation);

		Typeface Reguler = Typeface.createFromAsset(getAssets(),
				"Roboto-Regular.ttf");

		textView_Home = (TextView) dialog.findViewById(R.id.textView_Home);
		textView_Home.setTypeface(Reguler, Typeface.BOLD);
		textView_Recharge = (TextView) dialog
				.findViewById(R.id.textView_Recharge);
		textView_Recharge.setTypeface(Reguler, Typeface.BOLD);
		textView_Gift = (TextView) dialog.findViewById(R.id.textView_Gift);
		textView_Gift.setTypeface(Reguler, Typeface.BOLD);
		textView_Money = (TextView) dialog.findViewById(R.id.textView_Money);
		textView_Money.setTypeface(Reguler, Typeface.BOLD);

		RelativeLayout relativeLayout_Menu = (RelativeLayout) dialog
				.findViewById(R.id.relativeLayout_Menu);
		relativeLayout_Menu.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				MainActivity.imageView_Menu.setVisibility(View.VISIBLE);
			}
		});
		imageView_Home = (ImageView) dialog.findViewById(R.id.imageView_Home);
		imageView_Recharge = (ImageView) dialog
				.findViewById(R.id.imageView_Recharge);
		imageView_Gift = (ImageView) dialog.findViewById(R.id.imageView_Gift);
		imageView_Money = (ImageView) dialog.findViewById(R.id.imageView_Money);

		imageView_Home.setImageDrawable(SVGParser.getSVGFromResource(
				getResources(), SVGConstant.getSVGResource(8))
				.createPictureDrawable());
		imageView_Home.setPadding(5, 5, 5, 5);

		imageView_Recharge.setImageDrawable(SVGParser.getSVGFromResource(
				getResources(), SVGConstant.getSVGResource(9))
				.createPictureDrawable());
		imageView_Recharge.setPadding(5, 5, 5, 5);

		imageView_Gift.setImageDrawable(SVGParser.getSVGFromResource(
				getResources(), SVGConstant.getSVGResource(10))
				.createPictureDrawable());
		imageView_Gift.setPadding(5, 5, 5, 5);

		imageView_Money.setImageDrawable(SVGParser.getSVGFromResource(
				getResources(), SVGConstant.getSVGResource(11))
				.createPictureDrawable());
		imageView_Money.setPadding(5, 5, 5, 5);

		if (Build.VERSION.SDK_INT >= 11) {
			imageView_Home.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
			imageView_Recharge.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
			imageView_Gift.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
			imageView_Money.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
		}

		RelativeLayout relativeLayout_Home = (RelativeLayout) dialog
				.findViewById(R.id.relativeLayout_Home);
		relativeLayout_Home.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				displayView(8);
				imageView_Menu.setVisibility(View.GONE);
				supportInvalidateOptionsMenu();
				MainActivity.imageView_Menu.setVisibility(View.VISIBLE);
			}
		});

		RelativeLayout relativeLayout_Recharge = (RelativeLayout) dialog
				.findViewById(R.id.relativeLayout_Recharge);
		relativeLayout_Recharge.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				displayView(9);
				supportInvalidateOptionsMenu();
				MainActivity.imageView_Menu.setVisibility(View.VISIBLE);
			}
		});

		RelativeLayout relativeLayout_Gift = (RelativeLayout) dialog
				.findViewById(R.id.relativeLayout_Gift);
		relativeLayout_Gift.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				displayView(10);
				supportInvalidateOptionsMenu();
				MainActivity.imageView_Menu.setVisibility(View.VISIBLE);
			}
		});

		RelativeLayout relativeLayout_Money = (RelativeLayout) dialog
				.findViewById(R.id.relativeLayout_Money);
		relativeLayout_Money.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				displayView(11);
				MainActivity.imageView_Menu.setVisibility(View.VISIBLE);
				supportInvalidateOptionsMenu();
			}
		});
		dialog.show();
		imageView_Menu.startAnimation(rotation);
	}

	/* 
	 * Called when invalidateOptionsMenu() is triggered
	 */
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// if nav drawer is opened, hide the action items
		boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
		MenuItem wallet = menu.findItem(R.id.menu_wallet);
		MenuItem points = menu.findItem(R.id.menu_points);
		MenuItem search = menu.findItem(R.id.menu_search);

		if (mDrawerList.getCheckedItemPosition() == 3
				|| mDrawerList.getCheckedItemPosition() == 10
				|| mDrawerList.getCheckedItemPosition() == 15) {
			wallet.setVisible(true);
			points.setVisible(true);
			search.setVisible(true);
		} else if (mDrawerList.getCheckedItemPosition() > 3
				&& mDrawerList.getCheckedItemPosition() != 8
				&& mDrawerList.getCheckedItemPosition() != 9
				&& mDrawerList.getCheckedItemPosition() != 11) {
			wallet.setVisible(false);
			points.setVisible(false);
			search.setVisible(false);
		} else {
			wallet.setVisible(true);
			points.setVisible(true);
			search.setVisible(false);
		}
		if (drawerOpen) {
			wallet.setVisible(false);
			points.setVisible(false);
			search.setVisible(false);
		}
		return super.onPrepareOptionsMenu(menu);
	}

	/**
	 * Diplaying fragment view for selected nav drawer list item
	 * */
	private void displayView(int position) {
		// update the main content by replacing fragments
		Fragment fragment = null;
		supportInvalidateOptionsMenu();
		switch (position) {
		case 0:
			Intent profileIntent = new Intent(MainActivity.this,
					ProfileActivity.class);
			startActivity(profileIntent);
			break;
		case 1:
			if (Utility.getLoginFlag(MainActivity.this,
					Constants.SHAREDPREFERENCE_IS_LOGIN)) {
				// fragment = QuickPayFragment.newInstance();
				fragment = QuickPayFragmentNew.newInstance();
				imageView_Menu.setVisibility(View.GONE);
			} else {
				Intent intent = new Intent(MainActivity.this,
						LoginActivity.class);
				startActivity(intent);
			}
			break;
		case 2:
			try {
				quickPayDataSource.open();

				List<QuickPay> quickpays = quickPayDataSource
						.getQuickPayByLimit(2);
				int size = quickpays.size();
				if (size >= 2) {
					fragment = RechargeMainActivity.newInstance(0);
				} else {
					fragment = RechargeMainActivity.newInstance(1);
				}
			} catch (SQLException e) {
				// TODO: handle exception
			} catch (Exception e) {
				// TODO: handle exception
			} finally {
				quickPayDataSource.close();
			}
			break;
		case 3:
			Utility.setBackRequired(MainActivity.this,
					Constants.SHAREDPREFERENCE_BACK_LOCATION, true);
			fragment = GiftMainActivity.newInstance(0);
			break;
		/*
		 * case 4: Utility.setBackRequired(MainActivity.this,
		 * Constants.SHAREDPREFERENCE_BACK_LOCATION, false); fragment =
		 * MyFreeBiesFragment.newInstance(); break;
		 */
		case 4:
			fragment = HistoryFragment.newInstance();
			break;
		case 5:
			fragment = QuickPayMiscallFregment.newInstance();
			break;
		case 6:
			fragment = MyAccountFragment.newInstance();
			break;
		case 7:
			try {
				if (Utility.getLoginFlag(MainActivity.this,
						Constants.SHAREDPREFERENCE_IS_LOGIN)) {

					final Dialog dialog = new Dialog(MainActivity.this);
					dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
					dialog.getWindow().setBackgroundDrawableResource(
							android.R.color.transparent);
					dialog.setContentView(R.layout.dialog_two_button);
					dialog.setCancelable(false);
					dialog.setCanceledOnTouchOutside(false);
					// dialog.setTitle(null);

					Typeface Reguler = Typeface.createFromAsset(getAssets(),
							"Roboto-Regular.ttf");

					Typeface Medium = Typeface.createFromAsset(getAssets(),
							"Roboto-Regular.ttf");

					// set the custom dialog components
					// - text, image and button
					TextView textView_Title = (TextView) dialog
							.findViewById(R.id.textView_Title);
					textView_Title.setText("Logout");
					textView_Title.setTypeface(Reguler);

					TextView textView_Message = (TextView) dialog
							.findViewById(R.id.textView_Message);
					textView_Message
							.setText("Are you sure you want to logout?");
					textView_Message.setTypeface(Medium);

					Button button_Ok = (Button) dialog
							.findViewById(R.id.button_Ok);
					button_Ok.setTypeface(Reguler);
					button_Ok.setText("Yes");
					// if button is clicked, close the
					// custom dialog
					button_Ok.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							dialog.dismiss();
							new LogoutTask().execute();

						}
					});

					Button button_Cancel = (Button) dialog
							.findViewById(R.id.button_Cancel);
					button_Cancel.setText("No");
					button_Cancel.setTypeface(Reguler);
					// if button is clicked, close the
					// custom dialog
					button_Cancel
							.setOnClickListener(new View.OnClickListener() {
								@Override
								public void onClick(View v) {
									dialog.dismiss();
								}
							});

					dialog.show();

				} else {
					Intent intent = new Intent(MainActivity.this,
							LoginActivity.class);
					startActivity(intent);
				}
			} catch (Exception exception) {
			}
			break;
		case 8:
			if (Utility.getLoginFlag(MainActivity.this,
					Constants.SHAREDPREFERENCE_IS_LOGIN)) {
				fragment = QuickPayFragmentNew.newInstance();
				imageView_Menu.setVisibility(View.GONE);
			} else {
				Intent intent = new Intent(MainActivity.this,
						LoginActivity.class);
				startActivity(intent);
			}
			break;
		case 9:
			// fragment = RechargeMainActivity.newInstance(0);
			try {
				quickPayDataSource.open();

				List<QuickPay> quickpays = quickPayDataSource
						.getQuickPayByLimit(2);
				int size = quickpays.size();
				if (size >= 2) {
					fragment = RechargeMainActivity.newInstance(0);
				} else {
					fragment = RechargeMainActivity.newInstance(1);
				}
			} catch (SQLException e) {
				// TODO: handle exception
			} catch (Exception e) {
				// TODO: handle exception
			} finally {
				quickPayDataSource.close();
			}
			break;
		case 10:
			Utility.setBackRequired(MainActivity.this,
					Constants.SHAREDPREFERENCE_BACK_LOCATION, true);
			fragment = GiftMainActivity.newInstance(0);
			break;
		case 11:
			fragment = WalletTopupFragement.newInstance();
			break;
		default:
			break;
		}

		if (fragment != null) {
			FragmentManager fragmentManager = getSupportFragmentManager();
			FragmentTransaction fragmentTransaction = fragmentManager
					.beginTransaction();
			/*
			 * if (position == 1) {
			 * fragmentTransaction.replace(R.id.frame_container, fragment,
			 * "sometag"); } else {
			 */
			fragmentTransaction.replace(R.id.frame_container, fragment);
			// }

			fragmentTransaction.addToBackStack(null);
			fragmentTransaction.commit();

			// update selected item and title, then close the drawer

			mDrawerList.setItemChecked(position, true);
			// mDrawerList.setSelection(position);
			mDrawerList.setSelected(true);

			// setTitle(navMenuTitles[position]);
			mDrawerLayout.closeDrawer(mDrawerList);
		} else {
			// error in creating fragment
			// Log.e("MainActivity", "Error in creating fragment");
		}
		/*
		 * if (position == 1||position == 8) { QuickPayFragmentNew mSomeFragment
		 * = (QuickPayFragmentNew) getSupportFragmentManager()
		 * .findFragmentByTag("sometag");
		 * mSomeFragment.updateBalance(MainActivity.this); } else { //
		 * QuickPayFragmentNew mSomeFragment = (QuickPayFragmentNew) //
		 * getSupportFragmentManager().findFragmentByTag("sometag"); }
		 */

	}

	public static void changeView(int index, Context context) {
		Fragment fragment = null;

		switch (index) {
		case 3:
			fragment = GiftMainActivity.newInstance(0);
			break;

		case 15:
			fragment = GiftMainActivity.newInstance(1);
			break;
		case 2:
			try {
				quickPayDataSource.open();

				List<QuickPay> quickpays = quickPayDataSource
						.getQuickPayByLimit(2);
				int size = quickpays.size();
				if (size >= 2) {
					fragment = RechargeMainActivity.newInstance(0);
				} else {
					fragment = RechargeMainActivity.newInstance(1);
				}
			} catch (SQLException e) {
				// TODO: handle exception
			} catch (Exception e) {
				// TODO: handle exception
			} finally {
				quickPayDataSource.close();
			}
			break;

		case 7:
			fragment = WalletTopupFragement.newInstance();
			break;

		}

		if (fragment != null) {
			FragmentManager fragmentManager = ((FragmentActivity) context)
					.getSupportFragmentManager();
			FragmentTransaction fragmentTransaction = fragmentManager
					.beginTransaction();
			fragmentTransaction.replace(R.id.frame_container, fragment);
			fragmentTransaction.addToBackStack(null);
			fragmentTransaction.commit();

			// update selected item and title, then close the drawer

			mDrawerList.setItemChecked(index, true);
			// mDrawerList.setSelection(position);
			mDrawerList.setSelected(true);

			// setTitle(navMenuTitles[position]);
			mDrawerLayout.closeDrawer(mDrawerList);
		} else {
			// error in creating fragment
			// Log.e("MainActivity", "Error in creating fragment");
		}
		imageView_Menu.setVisibility(View.VISIBLE);
		((FragmentActivity) context).supportInvalidateOptionsMenu();

	}

	@Override
	public void setTitle(CharSequence title) {
		mTitle = title;
		// getSupportActionBar().setTitle(title);
	}

	/**
	 * When using the ActionBarDrawerToggle, you must call it during
	 * onPostCreate() and onConfigurationChanged()...
	 */

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggls
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v7.app.ActionBarActivity#onPostResume()
	 */
	@Override
	protected void onPostResume() {
		// TODO Auto-generated method stub
		super.onPostResume();
		if (mDrawerList.getCheckedItemPosition() == 4) {
			if (Utility.getBackRequired(MainActivity.this,
					Constants.SHAREDPREFERENCE_BACK_LOCATION))
				displayView(4);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v7.app.ActionBarActivity#onBackPressed()
	 */
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (mDrawerLayout.isDrawerOpen(mDrawerList)) {
			mDrawerLayout.closeDrawers();
		} else if (mDrawerList.getCheckedItemPosition() != 1) {
			supportInvalidateOptionsMenu();
			if (mDrawerList.getCheckedItemPosition() == 3) {
				if (FreeBiesPagerFragment.flag == 1) {
					FreeBiesPagerFragment.flag = 0;
					FreeBiesListFragment.adapter.notifyDataSetChanged();
				} else {
					displayView(1);
					imageView_Menu.setVisibility(View.GONE);
				}
			} else if (Utility.getLoginFlag(MainActivity.this,
					Constants.SHAREDPREFERENCE_IS_LOGIN)) {
				displayView(1);
				imageView_Menu.setVisibility(View.GONE);
			} else if (mDrawerList.getCheckedItemPosition() != 2) {
				displayView(2);
			} else {
				showTwoButtonDialog(MainActivity.this, "Pay1",
						"Are you sure you want to exit?");
			}

		} else {
			// if (doubleBackToExitPressedOnce) {
			// super.onBackPressed();
			// finish();
			// return;
			// }
			// if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
			// getSupportFragmentManager().popBackStack();
			// }
			showTwoButtonDialog(MainActivity.this, "Pay1",
					"Are you sure you want to exit?");

			/*
			 * if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
			 * showTwoButtonDialog(MainActivity.this, "Pay1",
			 * "Are you sure you want to exit?"); } else {
			 * getSupportFragmentManager().popBackStack(); }
			 */

			// this.doubleBackToExitPressedOnce = true;
			// Toast.makeText(this, "Press again to exit.", Toast.LENGTH_SHORT)
			// .show();
			//
			// new Handler().postDelayed(new Runnable() {
			//
			// @Override
			// public void run() {
			// doubleBackToExitPressedOnce = false;
			// }
			// }, 10000);

		}
	}

	@SuppressLint("NewApi")
	private void initArcMenu(ArcMenu menu) {
		for (int i = 8; i < 12; i++) {

			/*
			 * View child = getLayoutInflater().inflate(R.layout.menu_with_font,
			 * null); ImageView
			 * imageView1=(ImageView)child.findViewById(R.id.imageView1);
			 * TextView textView1=(TextView)child.findViewById(R.id.textView1);
			 * 
			 * 
			 * SVG svg = SVGParser.getSVGFromResource(getResources(),
			 * SVGConstant.getSVGResource(i));
			 * imageView1.setImageDrawable(svg.createPictureDrawable()); //
			 * item.setBackgroundResource(R.drawable.circle_profile_background);
			 * //imageView1.setPadding(5, 5, 5, 5);
			 * 
			 * if (Build.VERSION.SDK_INT >= 11) {
			 * imageView1.setLayerType(View.LAYER_TYPE_SOFTWARE, null); }
			 * 
			 * textView1.setText("umesh"+i);
			 * 
			 * final int position = i; menu.addItem(child, new OnClickListener()
			 * {
			 * 
			 * @Override public void onClick(View v) {
			 * 
			 * displayView(position);
			 * 
			 * // if (position == 3) { // Intent intent = new
			 * Intent(MainActivity.this, // GiftMainActivity.class); //
			 * startActivity(intent); // } } });
			 */
			ImageView item = new ImageView(this);
			// item.setImageResource(itemDrawables[i]);
			LinearLayout.LayoutParams parms = new LinearLayout.LayoutParams(80,
					80);
			item.setLayoutParams(parms);
			SVG svg = SVGParser.getSVGFromResource(getResources(),
					SVGConstant.getSVGResource(i));
			item.setImageDrawable(svg.createPictureDrawable());
			// item.setBackgroundResource(R.drawable.circle_profile_background);
			item.setPadding(5, 5, 5, 5);

			if (Build.VERSION.SDK_INT >= 11) {
				item.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
			}

			final int position = i;
			menu.addItem(item, new OnClickListener() {

				@Override
				public void onClick(View v) {

					displayView(position);
					supportInvalidateOptionsMenu();
					// if (position == 3) {
					// Intent intent = new Intent(MainActivity.this,
					// GiftMainActivity.class);
					// startActivity(intent);
					// }
				}
			});
		}
	}

	public void showTwoButtonDialog(final Context context, String Title,
			String message) {
		try {

			final Dialog dialog = new Dialog(context);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.getWindow().setBackgroundDrawableResource(
					android.R.color.transparent);
			dialog.setContentView(R.layout.dialog_two_button);
			dialog.setCancelable(false);
			dialog.setCanceledOnTouchOutside(false);
			// dialog.setTitle(null);

			Typeface Reguler = Typeface.createFromAsset(context.getAssets(),
					"Roboto-Regular.ttf");

			Typeface Medium = Typeface.createFromAsset(context.getAssets(),
					"Roboto-Regular.ttf");

			// set the custom dialog components - text, image and button
			TextView textView_Title = (TextView) dialog
					.findViewById(R.id.textView_Title);
			textView_Title.setText(Title);
			textView_Title.setTypeface(Reguler);

			TextView textView_Message = (TextView) dialog
					.findViewById(R.id.textView_Message);
			textView_Message.setText(message);
			textView_Message.setTypeface(Medium);

			Button button_Ok = (Button) dialog.findViewById(R.id.button_Ok);
			button_Ok.setTypeface(Reguler);
			// if button is clicked, close the custom dialog
			button_Ok.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
					Utility.setRefreshRequired(MainActivity.this,
							Constants.SHAREDPREFERENCE_REFESH_QUICKPAY, true);
					Utility.setRefreshRequired(MainActivity.this,
							Constants.SHAREDPREFERENCE_REFESH_MY_GIFT, true);
					Utility.setRefreshRequired(MainActivity.this,
							Constants.SHAREDPREFERENCE_REFESH_TRANSATION, true);
					Utility.setRefreshRequired(MainActivity.this,
							Constants.SHAREDPREFERENCE_REFESH_WALLET, true);
					MainActivity.this.finish();
				}
			});

			Button button_Cancel = (Button) dialog
					.findViewById(R.id.button_Cancel);
			button_Cancel.setTypeface(Reguler);
			// if button is clicked, close the custom dialog
			button_Cancel.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
				}
			});

			dialog.show();

		} catch (Exception exception) {
		}
	}

	private void checkNotNull(Object reference, String name) {
		if (reference == null) {
			throw new NullPointerException("Errror");
		}
	}

	public class GetOperatorTask extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... params) {
			try {
				// String response = RequestClass.getInstance()
				// .readPay1B2CRequest(MainActivity.this,
				// Constants.B2C_URL + "get_all_operators/?");
				// return Constants.loadJSONFromAsset(MainActivity.this,
				// "operator.json");
				try {

					long timestamp = System.currentTimeMillis();
					operatorDataSource.open();
					String result;
					// if (!result.startsWith("Error")) {
					// replaced = result.replace("(", "").replace(")", "")
					// .replace(";", "");
					// } else {
					result = Constants.loadJSONFromAsset(MainActivity.this,
							"operator.json");
					// }
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");
					JSONObject jsonObject = new JSONObject(replaced);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {
						JSONObject description = jsonObject
								.getJSONObject("description");
						JSONArray mobile = description.getJSONArray("mobile");
						JSONArray data = description.getJSONArray("data");
						JSONArray postpaid = description
								.getJSONArray("postpaid");
						JSONArray dth = description.getJSONArray("dth");
						operatorDataSource.deleteOperator();
						operatorDataSource.createOperator(mobile,
								Constants.RECHARGE_MOBILE, timestamp);
						operatorDataSource.createOperator(data,
								Constants.RECHARGE_DATA, timestamp);
						operatorDataSource.createOperator(postpaid,
								Constants.BILL_PAYMENT, timestamp);
						operatorDataSource.createOperator(dth,
								Constants.RECHARGE_DTH, timestamp);

					}

				} catch (JSONException e) {
				} catch (SQLException exception) {
				} finally {
					operatorDataSource.close();
				}
				return "";
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}
	}

	public class GetOperatorCodeTask extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... params) {
			try {
				// String time = Utility.getSharedPreferences(MainActivity.this,
				// Constants.LAST_CIRCLE_UPDATED) == null ? "" : Utility
				// .getSharedPreferences(MainActivity.this,
				// Constants.LAST_CIRCLE_UPDATED);
				// String response = RequestClass
				// .getInstance()
				// .readPay1B2CRequest(
				// MainActivity.this,
				// Constants.B2B_URL
				// + "method=getMobileDetails&mobile=all&timestamp="
				// + time);

				String response = Constants.loadJSONFromAsset(
						MainActivity.this, "operatorcode.json");
				// parseResponse(response);
				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}
	}

	private void parseResponse(String response) {
		// TODO Auto-generated method stub
		try {
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date date = new Date();
			String timeStamp = dateFormat.format(date);
			// Log.d("Plan", "Plan Start Circle Start Thread Time loop"
			// + timeStamp);

			circleDataSource.open();

			if (!response.startsWith("Error")) {
				String replaced = response.replace("(", "").replace(")", "")
						.replace(";", "");
				// // Log.d("Circle  ", "Plan  "+response);
				replaced = replaced.substring(1, replaced.length() - 1);
				/* JSONArray array = new JSONArray(replaced); */
				// JSONObject jsonObject = new JSONObject(replaced);
				// String status = jsonObject.getString("status");
				// if (status.equalsIgnoreCase("success")) {
				circleDataSource.deleteCircle();
				circleDataSource.createNumberAndCircleList(replaced, timeStamp,
						System.currentTimeMillis());
				Utility.setSharedPreferences(MainActivity.this,
						Constants.LAST_CODE_UPDATED,
						String.valueOf(System.currentTimeMillis()));
				// }
			}
		} catch (SQLException exception) {
			// Log.d("NAC", "row inserted   " + exception.getMessage());
			// TODO: handle exception
			// // // Log.e("Er ", exception.getMessage());
		} catch (Exception e) {
			// Log.d("NAC", "row inserted   " + e.getMessage());
			// TODO: handle exception
			// // // Log.e("Er ", e.getMessage());
		} finally {
			circleDataSource.close();
		}
	}

	public class CheckUpdateTask extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... params) {

			String response = RequestClass
					.getInstance()
					.readPay1B2CAppUpdateRequest(MainActivity.this,
							"https://androidquery.appspot.com/api/market?app=com.pay1");
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			try {
				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONObject jsonObject = new JSONObject(replaced);
					String live_version = jsonObject.getString("version");
					// int curVersion = getPackageManager().getPackageInfo(
					// "com.mindsarray.pay1", 0).versionCode;
					String current_version = getPackageManager()
							.getPackageInfo("com.pay1", 0).versionName;

					// // Log.w("SDFSD ", curVersion + "  " + current_version);

					if (!live_version.equalsIgnoreCase("null")
							&& !current_version.equalsIgnoreCase(live_version)) {

						try {
							final Dialog dialog = new Dialog(MainActivity.this);
							dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
							dialog.getWindow().setBackgroundDrawableResource(
									android.R.color.transparent);
							dialog.setContentView(R.layout.dialog_two_button);
							dialog.setCancelable(false);
							dialog.setCanceledOnTouchOutside(false);
							// dialog.setTitle(null);

							Typeface Reguler = Typeface.createFromAsset(
									getAssets(), "Roboto-Regular.ttf");

							Typeface Medium = Typeface.createFromAsset(
									getAssets(), "Roboto-Regular.ttf");

							TextView textView_Title = (TextView) dialog
									.findViewById(R.id.textView_Title);
							textView_Title.setText("Pay1 Update Available");
							textView_Title.setTypeface(Reguler);
							// set the custom dialog components - text, image
							// and button
							TextView textView_Message = (TextView) dialog
									.findViewById(R.id.textView_Message);
							textView_Message
									.setText("There is new version of Pay1 application available, click OK to upgrade now?");
							textView_Message.setTypeface(Medium);

							Button button_Ok = (Button) dialog
									.findViewById(R.id.button_Ok);
							button_Ok.setTypeface(Reguler);
							// if button is clicked, close the custom dialog
							button_Ok
									.setOnClickListener(new View.OnClickListener() {
										@Override
										public void onClick(View v) {
											dialog.dismiss();
											Intent intent = new Intent(
													Intent.ACTION_VIEW,
													Uri.parse("market://details?id=com.pay1"));
											startActivity(intent);
										}
									});

							Button button_Cancel = (Button) dialog
									.findViewById(R.id.button_Cancel);
							button_Cancel.setTypeface(Reguler);
							// if button is clicked, close the custom dialog
							button_Cancel
									.setOnClickListener(new View.OnClickListener() {
										@Override
										public void onClick(View v) {
											dialog.dismiss();
										}
									});

							dialog.show();
						} catch (Exception exception) {
						}
					}
				} else {

				}
			} catch (JSONException e) {
				// e.printStackTrace();

			} catch (Exception e) {
				// TODO: handle exception

			}
		}

		@Override
		protected void onPreExecute() {

			super.onPreExecute();
		}

	}

	public class CheckBalanceTask extends AsyncTask<String, String, String> {
		// MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {

			String response = RequestClass.getInstance().readPay1B2CRequest(
					MainActivity.this, Constants.B2C_URL + "check_bal");
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			// if (this.dialog.isShowing()) {
			// this.dialog.dismiss();
			// }
			super.onPostExecute(result);
			try {
				// JSONArray array = new JSONArray(result);
				JSONObject jsonObject = new JSONObject(result);
				String status = jsonObject.getString("status");
				if (status.equalsIgnoreCase("SUCCESS")) {
					JSONObject description = jsonObject
							.getJSONObject("description");
					String account_balance = description
							.getString("account_balance");
					Utility.setBalance(MainActivity.this,
							Constants.SHAREDPREFERENCE_BALANCE,
							account_balance.trim());
					Utility.setBalance(MainActivity.this,
							Constants.SHAREDPREFERENCE_LOYALTY_POINTS,
							description.getString("loyalty_points"));
					supportInvalidateOptionsMenu();
					/*
					 * QuickPayFragmentNew mSomeFragment = (QuickPayFragmentNew)
					 * getSupportFragmentManager()
					 * .findFragmentByTag("sometag");
					 */
					// mSomeFragment.updateBalance(MainActivity.this);
				}
			} catch (JSONException e) {
				// e.printStackTrace();
				// Constants.showCustomToast(MainActivity.this,
				// result);
			} catch (Exception e) {
				// TODO: handle exception
				// Constants.showCustomToast(MainActivity.this,
				// result);
			}
			adapter.notifyDataSetChanged();
		}

		protected void onPreExecute() {
			// dialog = new MyProgressDialog(MainActivity.this);
			// // this.dialog.setMessage("Please wait.....");
			// this.dialog.setCancelable(true);
			// this.dialog
			// .setOnCancelListener(new DialogInterface.OnCancelListener() {
			//
			// @Override
			// public void onCancel(DialogInterface dialog) {
			// // TODO Auto-generated method stub
			// dialog.dismiss();
			// CheckBalanceTask.this.cancel(true);
			// }
			// });
			// this.dialog.show();
			super.onPreExecute();
		}
	}

	public class CheckBalanceLoaderTask extends
			AsyncTask<String, String, String> implements OnDismissListener {
		MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {

			String response = RequestClass.getInstance().readPay1B2CRequest(
					MainActivity.this, Constants.B2C_URL + "check_bal");
			return params[0] + "#" + response;
		}

		@Override
		protected void onPostExecute(String result) {
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			super.onPostExecute(result);
			try {
				// JSONArray array = new JSONArray(result);
				String[] sp = result.trim().split("#");
				JSONObject jsonObject = new JSONObject(sp[1]);
				String status = jsonObject.getString("status");
				if (status.equalsIgnoreCase("SUCCESS")) {
					JSONObject description = jsonObject
							.getJSONObject("description");
					String account_balance = description
							.getString("account_balance");
					Utility.setBalance(MainActivity.this,
							Constants.SHAREDPREFERENCE_BALANCE,
							account_balance.trim());
					Utility.setBalance(MainActivity.this,
							Constants.SHAREDPREFERENCE_LOYALTY_POINTS,
							description.getString("loyalty_points"));

					// showWalletRefreshDialog(Integer.parseInt(sp[0]));
					supportInvalidateOptionsMenu();
				}
			} catch (JSONException e) {
				// e.printStackTrace();
				// Constants.showCustomToast(MainActivity.this,
				// result);
			} catch (Exception e) {
				// TODO: handle exception
				// Constants.showCustomToast(MainActivity.this,
				// result);
			}
			adapter.notifyDataSetChanged();
		}

		protected void onPreExecute() {
			dialog = new MyProgressDialog(MainActivity.this);
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							dialog.dismiss();
							CheckBalanceLoaderTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			dialog.dismiss();
			CheckBalanceLoaderTask.this.cancel(true);
		}

	}

	public class FreeBiesTask extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... params) {
			try {
				// String response = RequestClass
				// .getInstance()
				// .readPay1B2CRequest(
				// MainActivity.this,
				// Constants.B2C_URL
				// + "Get_all_deal_data/?latitude="
				// + params[0]
				// + "&longitude="
				// + params[1]
				// + "&mobile="
				// + Utility
				// .getMobileNumber(
				// MainActivity.this,
				// Constants.SHAREDPREFERENCE_MOBILE));

				String result = Constants.loadJSONFromAsset(MainActivity.this,
						"gift.json");
				if (!result.startsWith("Error")) {

					try {

						freeBiesDataSource.open();
						freeBiesLocationDataSource.open();

						freeBiesDataSource.deleteFreeBies();
						freeBiesLocationDataSource.deleteFreeBiesLocation();

						long t = System.currentTimeMillis();

						freeBiesDataSource.createFreeBies(result, t);

						// freeBiesLocationDataSource.createFreeBiesLocation(
						// result, t);

						// Utility.setSharedPreferences(MainActivity.this,
						// Constants.LAST_GIFT_UPDATED,
						// String.valueOf(System.currentTimeMillis()));
					} catch (SQLException exception) {
						// TODO: handle exception
						Log.e("Er ", exception.getMessage());
					} catch (Exception e) {
						// TODO: handle exception
						Log.e("Er ", e.getMessage());
					} finally {
						freeBiesDataSource.close();
						freeBiesLocationDataSource.close();
					}
				}
			} catch (Exception e) {

			}
			return "Error";
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

		}

		protected void onPreExecute() {
			super.onPreExecute();
		}

	}

	public class LogoutTask extends AsyncTask<String, String, String> implements
			OnDismissListener {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			try {
				String response = RequestClass.getInstance()
						.readPay1B2CRequest(MainActivity.this,
								Constants.B2C_URL + "signout/");

				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			try {

				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONObject jsonObject = new JSONObject(replaced);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {

						Utility.setLoginFlag(MainActivity.this,
								Constants.SHAREDPREFERENCE_IS_LOGIN, false);

						Utility.setRefreshRequired(MainActivity.this,
								Constants.SHAREDPREFERENCE_REFESH_QUICKPAY,
								true);
						Utility.setRefreshRequired(MainActivity.this,
								Constants.SHAREDPREFERENCE_REFESH_MY_GIFT, true);
						Utility.setRefreshRequired(MainActivity.this,
								Constants.SHAREDPREFERENCE_REFESH_TRANSATION,
								true);
						Utility.setRefreshRequired(MainActivity.this,
								Constants.SHAREDPREFERENCE_REFESH_WALLET, true);

						Utility.setUserName(MainActivity.this,
								Constants.SHAREDPREFERENCE_NAME, null);
						// Utility.setMobileNumber(MainActivity.this,
						// Constants.SHAREDPREFERENCE_MOBILE, null);
						Utility.setDOB(MainActivity.this,
								Constants.SHAREDPREFERENCE_DOB, null);
						// Utility.setEmail(MainActivity.this,
						// Constants.SHAREDPREFERENCE_EMAIL, null);
						Utility.setGender(MainActivity.this,
								Constants.SHAREDPREFERENCE_GENDER, null);
						Utility.setUserID(MainActivity.this,
								Constants.SHAREDPREFERENCE_USER_ID, null);
						Utility.setBalance(MainActivity.this,
								Constants.SHAREDPREFERENCE_BALANCE, null);
						Utility.setSharedPreferences(MainActivity.this,
								Constants.SHAREDPREFERENCE_PURCHASE_DATA, null);
						Utility.setSharedPreferences(MainActivity.this,
								Constants.SHAREDPREFERENCE_WALLET_DATA, null);
						Utility.setSharedPreferences(MainActivity.this,
								Constants.SHAREDPREFERENCE_MY_GIFTS_DATA, null);

						Utility.setFbImage64(MainActivity.this, "");
						Utility.setFbImageId(MainActivity.this, "");

						try {
							quickPayDataSource.open();
							// freeBiesDataSource.open();
							reClaimGiftDataSource.open();

							quickPayDataSource.deleteQuickPay();
							// freeBiesDataSource.deleteFreeBies();
							reClaimGiftDataSource.deleteReClaimGift();

						} catch (SQLException exception) {
							// TODO: handle exception
							// // // Log.e("Er ", exception.getMessage());
						} catch (Exception e) {
							// TODO: handle exception
							// // // Log.e("Er ", e.getMessage());
						} finally {
							quickPayDataSource.close();
							// freeBiesDataSource.close();
							reClaimGiftDataSource.close();
						}

						// Constants.showDialog(MainActivity.this, TAG,
						// Constants.checkCode(replaced),
						// Constants.DIALOG_CLOSE);
						// Intent intent = new Intent(MainActivity.this,
						// LoginActivity.class);
						// // Closing all the Activities
						// intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						// // Add new Flag to start new Activity
						// intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK );
						// startActivity(intent);
						// finish();
						// adapter.notifyDataSetChanged();

						// displayView(1);
						try {
							NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
							notificationManager.cancelAll();
						} catch (Exception e) {
						}

						Utility.setOneTimeRegistration(MainActivity.this,
								Constants.SHAREDPREFERENCE_REGISTRATION, false);
						Utility.setOneTimeRegistration(MainActivity.this,
								Constants.SHAREDPREFERENCE_PREFILLED, false);
						Intent intent = new Intent(MainActivity.this,
								RegistrationActivity.class);
						startActivity(intent);
						finish();
					} else {
						Constants.showOneButtonDialog(MainActivity.this,
								"Logout", Constants.checkCode(replaced),
								Constants.DIALOG_CLOSE);
					}

				} else {
					Intent intent = new Intent(MainActivity.this,
							ConnectivityErrorActivity.class);
					startActivity(intent);

				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonDialog(MainActivity.this, "Logout",
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonDialog(MainActivity.this, "Logout",
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			}

		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(MainActivity.this);
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							LogoutTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			LogoutTask.this.cancel(true);
			dialog.cancel();
		}

	}

	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		if (location != null) {

			double latitude = location.getLatitude();

			// Getting longitude of the current location
			double longitude = location.getLongitude();

			// Creating a LatLng object for the current location
			// LatLng latLng = new LatLng(latitude, longitude);

			// Showing the current location in Google Map
			// googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

			// Zoom in the Google Map
			// googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));

			/*
			 * Utility.setCurrentLatitude(MainActivity.this,
			 * Constants.SHAREDPREFERENCE_CURRENT_LATITUDE,
			 * String.valueOf(latitude));
			 * Utility.setCurrentLongitude(MainActivity.this,
			 * Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE,
			 * String.valueOf(longitude));
			 */

		}
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}

	public class LoadImageTask extends AsyncTask<String, String, Bitmap> {

		@Override
		protected Bitmap doInBackground(String... params) {

			Bitmap bitmap = null;
			URL imageURL = null;
			try {
				imageURL = new URL("https://graph.facebook.com/" + params[0]
						+ "/picture?type=large");
				// imageURL = new URL(Utility.getFbImage(context));

				bitmap = BitmapFactory.decodeStream(imageURL.openConnection()
						.getInputStream());
				Log.d("dd", "fff");
				return bitmap;
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Log.d("dd", "fff");

			return bitmap;
		}

		@Override
		protected void onPostExecute(Bitmap result) {

			super.onPostExecute(result);
			try {
				if (result != null) {

					String imgeInBase64 = Constants.encodeTobase64(result);
					Utility.setFbImage64(MainActivity.this, imgeInBase64);
					// fbConnectButton.setImageBitmap(Constants.decodeBase64(Utility.getFbImage64(context)));
					adapter.notifyDataSetChanged();
					// fbConnectButton.setImageBitmap(result);
				}
			} catch (Exception e) {
				// TODO: handle exception
				// Constants.showCustomToast(MainActivity.this,
				// result);
			}
			// notifyDataSetChanged();

		}

		protected void onPreExecute() {

			super.onPreExecute();
		}
	}

	public class UpdateProfileTask extends AsyncTask<String, String, String>
			implements OnDismissListener {
		// private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			try {
				String response = RequestClass
						.getInstance()
						.readPay1B2CRequest(
								MainActivity.this,
								Constants.B2C_URL
										+ "update_profile/?name="
										+ URLEncoder.encode(params[0], "utf-8")
										+ "&email="
										+ URLEncoder.encode(params[1], "utf-8")
										+ "&gender="
										+ URLEncoder.encode(params[2], "utf-8")
										+ "&facebook_data="
										+ URLEncoder.encode(params[3], "utf-8")
										+ "&password="
										+ URLEncoder.encode(
												Utility.getPin(
														MainActivity.this,
														Constants.SHAREDPREFERENCE_PIN),
												"utf-8"));

				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			/*
			 * if (this.dialog.isShowing()) { this.dialog.dismiss(); }
			 */
			try {

				if (!result.startsWith("Error")) {
					/*
					 * String replaced = result.replace("(", "").replace(")",
					 * "") .replace(";", "");
					 */

					JSONObject jsonObject = new JSONObject(result);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {
						JSONObject profileData = jsonObject
								.getJSONObject("profileData");
						Utility.setUserName(MainActivity.this,
								Constants.SHAREDPREFERENCE_NAME,
								profileData.getString("name"));
						Utility.setMobileNumber(MainActivity.this,
								Constants.SHAREDPREFERENCE_MOBILE,
								profileData.getString("mobile"));
						Utility.setEmail(MainActivity.this,
								Constants.SHAREDPREFERENCE_EMAIL,
								profileData.getString("email"));
						Utility.setGender(
								MainActivity.this,
								Constants.SHAREDPREFERENCE_GENDER,
								profileData.getString("gender").startsWith("M") ? "m"
										: "f");

						Utility.setDOB(MainActivity.this,
								Constants.SHAREDPREFERENCE_DOB,
								profileData.getString("date_of_birth"));

						if (Utility.getFbImageId(MainActivity.this).isEmpty()
								|| Utility.getFbImageId(MainActivity.this)
										.length() == 0) {

						} else {
							new LoadImageTask().execute(Utility
									.getFbImageId(MainActivity.this));
						}

					} else {

					}

				} else {

				}
			} catch (JSONException e) {

			} catch (Exception e) {

			}
		}

		@Override
		protected void onPreExecute() {
			/*
			 * dialog = new MyProgressDialog(MainActivity.this); //
			 * this.dialog.setMessage("Please wait.....");
			 * this.dialog.setCancelable(false); this.dialog
			 * .setOnCancelListener(new DialogInterface.OnCancelListener() {
			 * 
			 * @Override public void onCancel(DialogInterface dialog) { // TODO
			 * Auto-generated method stub UpdateProfileTask.this.cancel(true); }
			 * }); this.dialog.show(); super.onPreExecute();
			 */
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			UpdateProfileTask.this.cancel(true);
			dialog.cancel();
		}

	}

	protected void ShowToastWithImage(String msg) {
		// TODO Auto-generated method stub
		View toastView = getLayoutInflater().inflate(R.layout.toast,
				(ViewGroup) findViewById(R.id.toastLayout));

		TextView textView = (TextView) toastView.findViewById(R.id.text);

		textView.setText(msg);
		Toast toast = new Toast(MainActivity.this);

		toast.setGravity(Gravity.CENTER_VERTICAL, 0, -100);
		toast.setDuration(200);
		toast.setView(toastView);

		toast.show();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		// Toast.makeText(MainActivity.this, "HI", Toast.LENGTH_SHORT).show();

		if (v.getTag().toString().equalsIgnoreCase("1")) {

			Fragment fragment = GiftMainActivity.newInstance(4);
			if (fragment != null) {
				FragmentManager fragmentManager = getSupportFragmentManager();
				FragmentTransaction fragmentTransaction = fragmentManager
						.beginTransaction();
				fragmentTransaction.replace(R.id.frame_container, fragment);
				fragmentTransaction.addToBackStack(null);
				fragmentTransaction.commit();

				// update selected item and title, then close the drawer

				// mDrawerList.setItemChecked(position, true);
				// mDrawerList.setSelection(position);
				// mDrawerList.setSelected(true);

				// setTitle(navMenuTitles[position]);
				mDrawerLayout.closeDrawer(mDrawerList);
			} else {
				// error in creating fragment
				// Log.e("MainActivity", "Error in creating fragment");
			}
		} else if (v.getTag().toString().equalsIgnoreCase("2")) {
			mDrawerLayout.closeDrawer(mDrawerList);
			new CheckBalanceTask().execute();
			/*
			 * QuickPayFragmentNew mSomeFragment = (QuickPayFragmentNew)
			 * getSupportFragmentManager() .findFragmentByTag("sometag");
			 */
			// mSomeFragment.updateBalance(MainActivity.this);
		} else if (v.getTag().toString().equalsIgnoreCase("3")) {
			try {
				// mDrawerLayout.closeDrawer(mDrawerList);
				if (!Utility.getProfileDataSource(MainActivity.this)
						.equalsIgnoreCase("1")) {
					fromImageClick = true;
					Session session = new Session(MainActivity.this);
					Session.OpenRequest openRequest = new Session.OpenRequest(
							MainActivity.this);
					openRequest.setPermissions(Arrays.asList("user_about_me",
							"user_friends", "email", "user_birthday"));
					openRequest
							.setLoginBehavior(SessionLoginBehavior.SUPPRESS_SSO);
					openRequest.setCallback(statusCallback);
					Session.setActiveSession(session);
					session.openForPublish(openRequest);
				}

			} catch (Exception e) {

			}
		} else if (v.getTag().toString().equalsIgnoreCase("4")) {

			Intent intent = new Intent(MainActivity.this, ProfileActivity.class);

			startActivity(intent);
			mDrawerLayout.closeDrawer(mDrawerList);

		} else if (v.getTag().toString().equalsIgnoreCase("6")) {

			Intent intent = new Intent(MainActivity.this,
					CoinHistoryActivity.class);

			startActivity(intent);
			mDrawerLayout.closeDrawer(mDrawerList);

		} else if (v.getTag().toString().equalsIgnoreCase("7")) {
			displayView(11);

		}
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
			String key) {
		// TODO Auto-generated method stub
		Log.d("aaa", "ads");
	}

	public void drag(MotionEvent event, View v) {

		FrameLayout.LayoutParams params = (android.widget.FrameLayout.LayoutParams) v
				.getLayoutParams();

		switch (event.getAction()) {
		case MotionEvent.ACTION_MOVE: {
			params.topMargin = (int) event.getRawY() - (v.getHeight());
			params.leftMargin = (int) event.getRawX() - (v.getWidth() / 2);
			v.setLayoutParams(params);
			break;
		}
		case MotionEvent.ACTION_UP: {
			params.topMargin = (int) event.getRawY() - (v.getHeight());
			params.leftMargin = (int) event.getRawX() - (v.getWidth() / 2);
			v.setLayoutParams(params);
			break;
		}
		case MotionEvent.ACTION_DOWN: {
			v.setLayoutParams(params);
			break;
		}
		}
	}
}
