package com.pay1.databasehandler;

public class ReClaimGift {
	private int reClaimID;
	private String reClaimTransactionID;
	private long reClaimRechargeAmount;
	private int reClaimFlag;
	private int reClaimCounter;
	private long reClaimUptateTime;

	/**
	 * @return the reClaimID
	 */
	public int getReClaimID() {
		return reClaimID;
	}

	/**
	 * @param reClaimID
	 *            the reClaimID to set
	 */
	public void setReClaimID(int reClaimID) {
		this.reClaimID = reClaimID;
	}

	/**
	 * @return the reClaimTransactionID
	 */
	public String getReClaimTransactionID() {
		return reClaimTransactionID;
	}

	/**
	 * @param reClaimTransactionID
	 *            the reClaimTransactionID to set
	 */
	public void setReClaimTransactionID(String reClaimTransactionID) {
		this.reClaimTransactionID = reClaimTransactionID;
	}

	/**
	 * @return the reClaimRechargeAmount
	 */
	public long getReClaimRechargeAmount() {
		return reClaimRechargeAmount;
	}

	/**
	 * @param reClaimRechargeAmount
	 *            the reClaimRechargeAmount to set
	 */
	public void setReClaimRechargeAmount(long reClaimRechargeAmount) {
		this.reClaimRechargeAmount = reClaimRechargeAmount;
	}

	/**
	 * @return the reClaimFlag
	 */
	public int getReClaimFlag() {
		return reClaimFlag;
	}

	/**
	 * @param reClaimFlag
	 *            the reClaimFlag to set
	 */
	public void setReClaimFlag(int reClaimFlag) {
		this.reClaimFlag = reClaimFlag;
	}

	/**
	 * @return the reClaimCounter
	 */
	public int getReClaimCounter() {
		return reClaimCounter;
	}

	/**
	 * @param reClaimCounter
	 *            the reClaimCounter to set
	 */
	public void setReClaimCounter(int reClaimCounter) {
		this.reClaimCounter = reClaimCounter;
	}

	/**
	 * @return the reClaimUptateTime
	 */
	public long getReClaimUptateTime() {
		return reClaimUptateTime;
	}

	/**
	 * @param reClaimUptateTime
	 *            the reClaimUptateTime to set
	 */
	public void setReClaimUptateTime(long reClaimUptateTime) {
		this.reClaimUptateTime = reClaimUptateTime;
	}

}