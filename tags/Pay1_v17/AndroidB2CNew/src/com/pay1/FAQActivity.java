package com.pay1;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.SQLException;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.larvalabs.svgandroid.SVGParser;
import com.pay1.constants.Constants;
import com.pay1.customviews.MyProgressDialog;
import com.pay1.requesthandler.RequestClass;

public class FAQActivity extends Activity {

	private static final String SCREEN_LABEL = "FAQ Screen";
	private EasyTracker easyTracker = null;

	private final String TAG = "FAQs";

	private WebView webView_FAQS;

	private ImageView imageView_Back;
	private TextView textView_Title;

	// private FAQDataSource faqDataSource;

	public static final String QUESTION = "question";
	public static final String ANSWER = "answer";

	// private long timestamp = 0;

	@SuppressLint("NewApi")
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.faq_activity);
		try {
			easyTracker = EasyTracker.getInstance(FAQActivity.this);
			easyTracker.set(Fields.SCREEN_NAME, SCREEN_LABEL);

			// timestamp = System.currentTimeMillis();

			// faqDataSource = new FAQDataSource(FAQActivity.this);

			RelativeLayout back_layout=(RelativeLayout)findViewById(R.id.back_layout);
			back_layout.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onBackPressed();
				}
			});
			
			imageView_Back = (ImageView) findViewById(R.id.imageView_Back);
			imageView_Back.setImageDrawable(SVGParser.getSVGFromResource(
					getResources(), R.raw.ic_back_old).createPictureDrawable());
			if (Build.VERSION.SDK_INT >= 11)
				imageView_Back.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
			imageView_Back.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onBackPressed();
				}
			});

			Typeface Reguler = Typeface.createFromAsset(getAssets(),
					"EncodeSansNormal-400-Regular.ttf");
			textView_Title = (TextView) findViewById(R.id.textView_Title);
			textView_Title.setText(TAG);
			textView_Title.setTypeface(Reguler);

			webView_FAQS = (WebView) findViewById(R.id.webView_FAQS);
			webView_FAQS.setLongClickable(false);
			webView_FAQS.setBackgroundColor(0x00000000);
			if (Build.VERSION.SDK_INT >= 11)
				webView_FAQS.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);
			webView_FAQS.getSettings().setJavaScriptEnabled(true);
			webView_FAQS.getSettings().setDomStorageEnabled(true);
			webView_FAQS.getSettings().setDatabaseEnabled(true);
			webView_FAQS.setOnLongClickListener(new View.OnLongClickListener() {

				@Override
				public boolean onLongClick(View v) {
					// TODO Auto-generated method stub
					return true;
				}
			});

			new FAQsTask().execute();

		} catch (Exception e) {
		}
		overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}

	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView().build());
		// EasyTracker.getInstance(this).activityStart(this);
		easyTracker.send(MapBuilder.createAppView().build());
	}

	@Override
	public void onStop() {
		super.onStop();
		// EasyTracker.getInstance(this).activityStop(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
	}

	public class FAQsTask extends AsyncTask<String, String, String> implements
			OnDismissListener {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			try {
				String response = RequestClass.getInstance()
						.readPay1B2CRequest(FAQActivity.this,
								Constants.B2C_URL + "get_faq/?req=faq");

				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			try {

				// faqDataSource.open();

				if (!result.startsWith("Error")) {
					/*String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");
*/
					JSONObject jsonObject = new JSONObject(result);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {
						// data.clear();

						JSONArray jsonArrayDesc = jsonObject
								.getJSONArray("description");

						for (int i = 0; i < jsonArrayDesc.length(); i++) {
							// WeakHashMap<String, String> map = new
							// WeakHashMap<String,
							// String>();
							// map.put(QUESTION, jsonArrayDesc.getJSONObject(i)
							// .getString("question"));
							// map.put(ANSWER, jsonArrayDesc.getJSONObject(i)
							// .getString("answer"));
							//
							// // data.add(map);
							//
							// faqDataSource.createFAQ(
							// jsonArrayDesc.getJSONObject(i).getString(
							// "question"),
							// jsonArrayDesc.getJSONObject(i).getString(
							// "answer"), timestamp);
							webView_FAQS
									.loadDataWithBaseURL(
											null,
											"<html><head><style>@font-face {font-family: Normal;src: url(\"file:///android_asset/EncodeSansNormal-400-Regular.ttf\")}"
													+ "@font-face {font-family: Narrow;src: url(\"file:///android_asset/EncodeSansNarrow-500-Medium.ttf\")}"
													+ "body {margin: 0px;padding:15px;text-align:justify;}"
													+ "h1{font-size:24px;font-family:\"Normal\";margin:0;padding:10px 0 5px 0;color:#000000;}"
													+ "h2{font-size:14px;font-family:\"Narrow\";margin:0;padding:10px 0 5px 0;color:#000000;}"
													+ "h3{font-size:13px;font-family:\"Narrow\";margin:0;padding:10px 0 5px 0;}"
													+ "ol{margin:0 0 10px 0;padding-left:20px;}"
													+ "li{font-size:13px;margin:0 0 2px 0;font-family:\"Normal\";}</style></head><body>"
													+ jsonArrayDesc
															.getJSONObject(i)
															.getString("answer")
													+ "</body></html>",
											"text/html", "UTF-8", null);
						}

						// FAQActivity.this.runOnUiThread(new Runnable() {
						//
						// @Override
						// public void run() {
						// // TODO Auto-generated method stub
						// adapter.notifyDataSetChanged();
						// }
						// });

					} else {
						Constants.showOneButtonDialog(FAQActivity.this, TAG,
								Constants.checkCode(result),
								Constants.DIALOG_CLOSE);
					}

				} else {
					Intent intent = new Intent(FAQActivity.this,
							ConnectivityErrorActivity.class);
					startActivity(intent);
				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonDialog(FAQActivity.this, TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			} catch (SQLException exception) {
				// TODO: handle exception
				// // // Log.e("Er ", exception.getMessage());
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonDialog(FAQActivity.this, TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			} finally {
				// faqDataSource.close();
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(FAQActivity.this);
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							FAQsTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			FAQsTask.this.cancel(true);
			dialog.cancel();
		}

	}
}