package com.pay1;

import java.net.URLEncoder;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.pay1.constants.Constants;
import com.pay1.customviews.MyProgressDialog;
import com.pay1.requesthandler.RequestClass;
import com.pay1.utilities.EditTextValidator;
import com.pay1.utilities.Utility;

public class ChangePinActivity extends AppBaseActivity {

	private static final String SCREEN_LABEL = "Change Pin Screen";
	private EasyTracker easyTracker = null;

	private final String TAG = "Change Pin";
	private ImageView imageView_Back, imageView_Ok, imageView_Cancel;
	private EditText editText_OldPin, editText_NewPin, editText_ConfirmPin;
	private TextView textView_Title, textView_TitleOldPin,
			textView_TitleNewPin, textView_TitleConfirmPin;

	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.change_pin_activity);
		try {
			easyTracker = EasyTracker.getInstance(ChangePinActivity.this);
			easyTracker.set(Fields.SCREEN_NAME, SCREEN_LABEL);

			registerBaseActivityReceiver();

			Typeface Normal = Typeface.createFromAsset(getAssets(),
					"EncodeSansNormal-100-Thin.ttf");

			Typeface Reguler = Typeface.createFromAsset(getAssets(),
					"EncodeSansNormal-400-Regular.ttf");

			textView_Title = (TextView) findViewById(R.id.textView_Title);
			textView_Title.setTypeface(Reguler);
			textView_TitleOldPin = (TextView) findViewById(R.id.textView_TitleOldPin);
			textView_TitleOldPin.setTypeface(Reguler);
			textView_TitleNewPin = (TextView) findViewById(R.id.textView_TitleNewPin);
			textView_TitleNewPin.setTypeface(Reguler);
			textView_TitleConfirmPin = (TextView) findViewById(R.id.textView_TitleConfirmPin);
			textView_TitleConfirmPin.setTypeface(Reguler);

			editText_OldPin = (EditText) findViewById(R.id.editText_OldPin);
			editText_OldPin.setTypeface(Normal);
			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(editText_OldPin.getWindowToken(), 0);
			editText_OldPin.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					// TODO Auto-generated method stub
					if (editText_OldPin.getText().toString().trim().length() > 0)
						EditTextValidator.hasText(ChangePinActivity.this,
								editText_OldPin,
								Constants.ERROR_PIN_BLANK_FIELD);
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					// TODO Auto-generated method stub

				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

				}
			});

			editText_NewPin = (EditText) findViewById(R.id.editText_NewPin);
			editText_NewPin.setTypeface(Normal);
			editText_NewPin.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					// TODO Auto-generated method stub
					if (editText_NewPin.getText().toString().trim().length() > 0)
						EditTextValidator.hasText(ChangePinActivity.this,
								editText_NewPin,
								Constants.ERROR_PIN_BLANK_FIELD);
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					// TODO Auto-generated method stub

				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

				}
			});

			editText_ConfirmPin = (EditText) findViewById(R.id.editText_ConfirmPin);
			editText_ConfirmPin.setTypeface(Normal);
			editText_ConfirmPin.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					// TODO Auto-generated method stub
					if (editText_ConfirmPin.getText().toString().trim()
							.length() > 0)
						EditTextValidator.hasText(ChangePinActivity.this,
								editText_ConfirmPin,
								Constants.ERROR_PIN_BLANK_FIELD);
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					// TODO Auto-generated method stub

				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

				}
			});

			editText_ConfirmPin
					.setOnEditorActionListener(new TextView.OnEditorActionListener() {

						@Override
						public boolean onEditorAction(TextView v, int actionId,
								KeyEvent event) {
							// TODO Auto-generated method stub
							if (actionId == EditorInfo.IME_ACTION_GO) {
								try {
									if (checkValidation(ChangePinActivity.this)) {
										if (editText_OldPin
												.getText()
												.toString()
												.trim()
												.equalsIgnoreCase(
														editText_NewPin
																.getText()
																.toString()
																.trim())) {
											Constants
													.showOneButtonDialog(
															ChangePinActivity.this,
															TAG,
															"Current Pin and Account Pin must be different.",
															Constants.DIALOG_CLOSE);

										} else if (editText_OldPin
												.getText()
												.toString()
												.trim()
												.equalsIgnoreCase(
														editText_NewPin
																.getText()
																.toString()
																.trim())) {
											Constants
													.showOneButtonDialog(
															ChangePinActivity.this,
															TAG,
															"Account Pin and Retype Pin must be same.",
															Constants.DIALOG_CLOSE);

										} else {
											new ChangePinTask().execute(
													Utility.getMobileNumber(
															ChangePinActivity.this,
															Constants.SHAREDPREFERENCE_MOBILE),
													editText_OldPin.getText()
															.toString().trim(),
													editText_NewPin.getText()
															.toString().trim(),
													editText_ConfirmPin
															.getText()
															.toString().trim());
										}
									}
								} catch (Exception e) {
									// e.printStackTrace();
								}
								return true;
							}
							return false;
						}
					});

			imageView_Back = (ImageView) findViewById(R.id.imageView_Back);
			imageView_Back.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onBackPressed();
				}
			});

			imageView_Ok = (ImageView) findViewById(R.id.imageView_Ok);
			imageView_Ok.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (checkValidation(ChangePinActivity.this)) {
						if (editText_OldPin
								.getText()
								.toString()
								.trim()
								.equalsIgnoreCase(
										editText_NewPin.getText().toString()
												.trim())) {
							Constants
									.showOneButtonDialog(
											ChangePinActivity.this,
											TAG,
											"Current Pin and Account Pin must be different.",
											Constants.DIALOG_CLOSE);

						} else if (editText_OldPin
								.getText()
								.toString()
								.trim()
								.equalsIgnoreCase(
										editText_NewPin.getText().toString()
												.trim())) {
							Constants.showOneButtonDialog(
									ChangePinActivity.this, TAG,
									"Account Pin and Retype Pin must be same.",
									Constants.DIALOG_CLOSE);

						} else {
							new ChangePinTask().execute(
									Utility.getMobileNumber(
											ChangePinActivity.this,
											Constants.SHAREDPREFERENCE_MOBILE),
									editText_OldPin.getText().toString().trim(),
									editText_NewPin.getText().toString().trim(),
									editText_ConfirmPin.getText().toString()
											.trim());
						}
					}
				}
			});

			imageView_Cancel = (ImageView) findViewById(R.id.imageView_Cancel);
			imageView_Cancel.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					editText_OldPin.setText("");
					editText_NewPin.setText("");
					editText_ConfirmPin.setText("");
				}
			});

		} catch (Exception e) {
		}

		overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}

	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView().build());
		// EasyTracker.getInstance(this).activityStart(this);
		easyTracker.send(MapBuilder.createAppView().build());
	}

	@Override
	public void onStop() {
		super.onStop();
		// EasyTracker.getInstance(this).activityStop(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unRegisterBaseActivityReceiver();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (Constants.isExpanded) {
			Constants.collapsView(this);
		} else {
			super.onBackPressed();
			overridePendingTransition(R.anim.slide_in_left,
					R.anim.slide_out_right);
		}
	}

	private boolean checkValidation(Context context) {
		boolean ret = true;

		if (!EditTextValidator.hasText(context, editText_OldPin,
				Constants.ERROR_LOGIN_PIN_BLANK_FIELD)) {
			ret = false;
			editText_OldPin.requestFocus();
			return ret;
		} else if (!EditTextValidator.hasText(context, editText_NewPin,
				Constants.ERROR_LOGIN_PIN_BLANK_FIELD)) {
			ret = false;
			editText_NewPin.requestFocus();
			return ret;
		} else if (!EditTextValidator.hasText(context, editText_ConfirmPin,
				Constants.ERROR_LOGIN_PIN_BLANK_FIELD)) {
			ret = false;
			editText_ConfirmPin.requestFocus();
			return ret;
		} else if (!EditTextValidator.isValidPin(context, editText_OldPin,
				Constants.ERROR_LOGIN_PIN_VALID_FIELD)) {
			ret = false;
			editText_OldPin.requestFocus();
			return ret;
		} else if (!EditTextValidator.isValidPin(context, editText_NewPin,
				Constants.ERROR_LOGIN_PIN_VALID_FIELD)) {
			ret = false;
			editText_NewPin.requestFocus();
			return ret;
		} else if (!EditTextValidator.isValidPin(context, editText_ConfirmPin,
				Constants.ERROR_LOGIN_PIN_VALID_FIELD)) {
			ret = false;
			editText_ConfirmPin.requestFocus();
			return ret;
		} else
			return ret;
	}

	public class ChangePinTask extends AsyncTask<String, String, String>
			implements OnDismissListener {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			try {

				String response = RequestClass.getInstance()
						.readPay1B2CRequest(
								ChangePinActivity.this,
								Constants.B2C_URL
										+ "update_password/?mobile_number="
										+ params[0] + "&current_password="
										+ URLEncoder.encode(params[1], "utf-8")
										+ "&password="
										+ URLEncoder.encode(params[2], "utf-8")
										+ "&confirm_password="
										+ URLEncoder.encode(params[3], "utf-8")
										+ "&code=");

				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			try {

				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONObject jsonObject = new JSONObject(replaced);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {

						// JSONObject jsonObject2 = jsonObject
						// .getJSONObject("description");

						// Utility.setLoginFlag(ChangePinActivity.this,
						// Constants.SHAREDPREFERENCE_IS_LOGIN, false);
						//
						// Utility.setUserName(ChangePinActivity.this,
						// Constants.SHAREDPREFERENCE_NAME, null);
						// Utility.setMobileNumber(ChangePinActivity.this,
						// Constants.SHAREDPREFERENCE_MOBILE, null);
						// Utility.setEmail(ChangePinActivity.this,
						// Constants.SHAREDPREFERENCE_EMAIL, null);
						// Utility.setGender(ChangePinActivity.this,
						// Constants.SHAREDPREFERENCE_GENDER, null);
						// Utility.setUserID(ChangePinActivity.this,
						// Constants.SHAREDPREFERENCE_USER_ID, null);
						// Utility.setBalance(ChangePinActivity.this,
						// Constants.SHAREDPREFERENCE_BALANCE, null);

						Utility.setPin(ChangePinActivity.this,
								Constants.SHAREDPREFERENCE_PIN, editText_NewPin
										.getText().toString().trim());

						// Constants.showOneButtonDialog(ChangePinActivity.this,
						// TAG, "Pin updated successfully.",
						// Constants.DIALOG_CLOSE_CONFIRM);

						final Dialog dialog = new Dialog(ChangePinActivity.this);
						dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
						// dialog.getWindow().setBackgroundDrawableResource(
						// android.R.color.transparent);
						dialog.setContentView(R.layout.dialog_success_one_button);
						// dialog.setTitle(null);

						// set the custom dialog components - text, image and
						// button
						Typeface Reguler = Typeface
								.createFromAsset(getAssets(),
										"EncodeSansNormal-400-Regular.ttf");

						Typeface Narrow = Typeface.createFromAsset(getAssets(),
								"EncodeSansNarrow-500-Medium.ttf");

						// set the custom dialog components - text, image and
						// button
						TextView textView_Title = (TextView) dialog
								.findViewById(R.id.textView_Title);
						textView_Title.setText(TAG);
						textView_Title.setTypeface(Reguler);

						TextView textView_Message = (TextView) dialog
								.findViewById(R.id.textView_Message);
						textView_Message.setText("Pin updated successfully.");
						textView_Message.setTypeface(Narrow);

						ImageView imageView_Ok = (ImageView) dialog
								.findViewById(R.id.imageView_Ok);
						// if button is clicked, close the custom dialog
						imageView_Ok
								.setOnClickListener(new View.OnClickListener() {
									@Override
									public void onClick(View v) {
										dialog.dismiss();

										Intent returnIntent = new Intent();
										returnIntent.putExtra("PIN",
												editText_NewPin.getText()
														.toString().trim());
										setResult(RESULT_OK, returnIntent);
										onBackPressed();
									}
								});

						dialog.show();

					} else {
						Constants.showOneButtonDialog(ChangePinActivity.this,
								TAG, Constants.checkCode(replaced),
								Constants.DIALOG_CLOSE);
					}

				} else {
					// Constants.showOneButtonDialog(ChangePinActivity.this,
					// TAG,
					// Constants.ERROR_INTERNET, Constants.DIALOG_CLOSE);
					Intent intent = new Intent(ChangePinActivity.this,
							ConnectivityErrorActivity.class);
					intent.putExtra(Constants.RECHARGE_FOR,
							Constants.WALLET_PAYMENT);
					startActivity(intent);

				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonDialog(ChangePinActivity.this, TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonDialog(ChangePinActivity.this, TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(ChangePinActivity.this);
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							ChangePinTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			ChangePinTask.this.cancel(true);
			dialog.cancel();
		}

	}

}
