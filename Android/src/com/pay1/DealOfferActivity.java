package com.pay1;

import java.util.ArrayList;
import java.util.WeakHashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.pay1.customviews.MyProgressDialog;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnDismissListener;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.pay1.adapterhandler.DealOfferExpandedAdapter;
import com.pay1.constants.Constants;
import com.pay1.requesthandler.RequestClass;

public class DealOfferActivity extends AppBaseActivity {

	private static final String SCREEN_LABEL = "Deal Offer List Screen";
	private EasyTracker easyTracker = null;

	private final String TAG = "Deals";

	private ExpandableListView listView_DealOffers;
	private ImageView imageView_Back;
	private DealOfferExpandedAdapter adapter;
	private TextView textView_Title;
	private ArrayList<WeakHashMap<String, String>> data;

	public static final String OFFER_ID = "id";
	public static final String DEAL_ID = "d_id";
	public static final String DEAL_NAME = "d_name";
	public static final String OFFER_NAME = "name";
	public static final String OFFER_PRICE = "offer_price";
	public static final String ACTUAL_PRICE = "actual_price";
	public static final String DISCOUNT = "discount";
	public static final String TOTAL_STOCK = "total_stock";
	public static final String STOCK_SOLD = "stock_sold";
	public static final String VALIDITY = "validity";
	public static final String VALIDITY_MODE = "validity_mode";
	public static final String MAX_QUANTITY = "max_quantity";
	public static final String STATUS = "status";
	public static final String SHORT_DESC = "short_desc";
	public static final String LONG_DESC = "long_desc";
	public static final String CREATED = "created";
	public static final String UPDATED = "updated";

	private String deal_ID, deal_name;

	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.deal_offers_activity);
		try {
			easyTracker = EasyTracker.getInstance(DealOfferActivity.this);
			easyTracker.set(Fields.SCREEN_NAME, SCREEN_LABEL);
			registerBaseActivityReceiver();

			// Typeface Normal = Typeface.createFromAsset(getAssets(),
			// "EncodeSansNormal-100-Thin.ttf");

			Typeface Reguler = Typeface.createFromAsset(getAssets(),
					"EncodeSansNormal-400-Regular.ttf");

			textView_Title = (TextView) findViewById(R.id.textView_Title);
			textView_Title.setTypeface(Reguler);

			imageView_Back = (ImageView) findViewById(R.id.imageView_Back);
			imageView_Back.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onBackPressed();
				}
			});

			data = new ArrayList<WeakHashMap<String, String>>();
			listView_DealOffers = (ExpandableListView) findViewById(R.id.listView_DealOffers);
			// textView_NoData = (TextView) findViewById(R.id.textView_NoData);
			// listView_DealOffers.setEmptyView(textView_NoData);
			adapter = new DealOfferExpandedAdapter(DealOfferActivity.this, data);
			listView_DealOffers.setAdapter(adapter);
			listView_DealOffers
					.setOnItemClickListener(new AdapterView.OnItemClickListener() {

						@Override
						public void onItemClick(AdapterView<?> parent,
								View view, int position, long id) {
							// TODO Auto-generated method stub
							try {
								// WeakHashMap<String, String> map = new
								// WeakHashMap<String, String>();
								// map = data.get(position);
								// Intent intent = new Intent(
								// DealOfferActivity.this,
								// DealDetailsActivity.class);
								// intent.putExtra("URL", map.get(URL));
								// intent.putExtra("DEAL_ID", map.get(DEAL_ID));
								// intent.putExtra("DEAL_NAME",
								// map.get(DEAL_NAME));
								// intent.putExtra("ACTUAL_PRICE",
								// map.get(ACTUAL_PRICE));
								// intent.putExtra("OFFER_PRICE",
								// map.get(OFFER_PRICE));
								// intent.putExtra("STOCK_SOLD",
								// map.get(STOCK_SOLD));
								// intent.putExtra("DISCOUNT",
								// map.get(DISCOUNT));
								//
								// startActivity(intent);
							} catch (Exception e) {
							}
						}
					});

			Bundle bundle = getIntent().getExtras();
			if (bundle != null) {
				deal_ID = bundle.getString("DEAL_ID");
				deal_name = bundle.getString("DEAL_NAME");
				new DealOfferTask().execute(deal_ID);
			}

		} catch (Exception e) {
		}
		overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

	}

	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView().build());
		// EasyTracker.getInstance(this).activityStart(this);
		easyTracker.send(MapBuilder.createAppView().build());
	}

	@Override
	public void onStop() {
		super.onStop();
		// EasyTracker.getInstance(this).activityStop(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unRegisterBaseActivityReceiver();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (Constants.isExpanded) {
			Constants.collapsView(this);
		} else {
			super.onBackPressed();
			overridePendingTransition(R.anim.slide_in_left,
					R.anim.slide_out_right);
		}
	}

	public class DealOfferTask extends AsyncTask<String, String, String>
			implements OnDismissListener {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			try {
				String response = RequestClass.getInstance()
						.readPay1B2CRequest(
								DealOfferActivity.this,
								Constants.B2C_URL + "get_deal_offers/?id="
										+ params[0]);

				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			try {

				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONObject jsonObject = new JSONObject(replaced);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {
						data.clear();

						JSONArray jsonArrayDesc = jsonObject
								.getJSONArray("description");

						for (int i = 0; i < jsonArrayDesc.length(); i++) {
							WeakHashMap<String, String> map = new WeakHashMap<String, String>();

							map.put(OFFER_ID, jsonArrayDesc.getJSONObject(i)
									.getString("id"));
							map.put(DEAL_ID, jsonArrayDesc.getJSONObject(i)
									.getString("deal_id"));
							map.put(DEAL_NAME, deal_name);
							map.put(OFFER_NAME, jsonArrayDesc.getJSONObject(i)
									.getString("name"));
							map.put(OFFER_PRICE, jsonArrayDesc.getJSONObject(i)
									.getString("offer_price"));
							map.put(ACTUAL_PRICE, jsonArrayDesc
									.getJSONObject(i).getString("actual_price"));
							map.put(DISCOUNT, jsonArrayDesc.getJSONObject(i)
									.getString("discount"));
							map.put(TOTAL_STOCK, jsonArrayDesc.getJSONObject(i)
									.getString("total_stock"));
							map.put(STOCK_SOLD, jsonArrayDesc.getJSONObject(i)
									.getString("stock_sold"));
							map.put(VALIDITY, jsonArrayDesc.getJSONObject(i)
									.getString("validity"));
							map.put(VALIDITY_MODE,
									jsonArrayDesc.getJSONObject(i).getString(
											"validity_mode"));
							map.put(MAX_QUANTITY, jsonArrayDesc
									.getJSONObject(i).getString("max_quantity"));
							map.put(STATUS, jsonArrayDesc.getJSONObject(i)
									.getString("status"));
							map.put(SHORT_DESC, jsonArrayDesc.getJSONObject(i)
									.getString("short_desc"));
							map.put(LONG_DESC, jsonArrayDesc.getJSONObject(i)
									.getString("long_desc"));
							map.put(CREATED, jsonArrayDesc.getJSONObject(i)
									.getString("created"));
							map.put(UPDATED, jsonArrayDesc.getJSONObject(i)
									.getString("updated"));

							data.add(map);
						}

						DealOfferActivity.this.runOnUiThread(new Runnable() {

							@Override
							public void run() {
								// TODO Auto-generated method stub
								adapter.notifyDataSetChanged();
							}
						});

					} else {
						Constants.showOneButtonDialog(DealOfferActivity.this,
								TAG, Constants.checkCode(replaced),
								Constants.DIALOG_CLOSE);
					}

				} else {
					// Constants.showOneButtonDialog(DealOfferActivity.this,
					// TAG,
					// Constants.ERROR_INTERNET, Constants.DIALOG_CLOSE);
					Intent intent = new Intent(DealOfferActivity.this,
							ConnectivityErrorActivity.class);
					intent.putExtra(Constants.RECHARGE_FOR,
							Constants.SUBSCRIBE_DEAL);
					startActivity(intent);

				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonDialog(DealOfferActivity.this, TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonDialog(DealOfferActivity.this, TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(DealOfferActivity.this);
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							DealOfferTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			DealOfferTask.this.cancel(true);
			dialog.cancel();
		}

	}

}
