//
//  ProfileViewController.m
//  Pay1
//
//  Created by webninjaz on 16/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "ProfileViewController.h"
#import "DatePickerView.h"
#import "ProfileUpdateCriteria.h"
#import "ProfileUpdateResponse.h"
#import "ProfileUpdateService.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "UpdatePasswordViewController.h"
#import "UIDevice+IdentifierAddition.h"
#import "NSString+MD5Addition.h"
#import "UpdatePasswordService.h"
#import "UpdatePasswordResponse.h"
@interface ProfileViewController ()

@end

@implementation ProfileViewController

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)



- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    NSLog(@"#NAvControllerArray :%lu",(unsigned long)self.navigationController.viewControllers.count);
    
   // UIViewController * viewController = [self.navigationController.viewControllers objectAtIndex:1];

    
    isSaveData = FALSE;
    isUserLoginData = FALSE;
    
    [self setRoundedCornerForImageView:picImageView];
    
    penImgView.hidden = TRUE;
    
    self.view.backgroundColor = [UIColor colorWithRed:240/255.0 green:240/255.0 blue:240/255.0 alpha:1];
    
    rightButton = [[UIBarButtonItem alloc] initWithTitle:@"EDIT" style:UIBarButtonItemStylePlain target:self action:@selector(editClicked:)];
    rightButton.tintColor = [UIColor whiteColor];
    self.navigationItem.rightBarButtonItem = rightButton;
    
    
    [self setNaviationButtonWithText:@"My Profile" selector:@selector(backClicked:) isLeft:TRUE isReduceSize:TRUE isMyFont:18.0];
    
    
    
    if([UIScreen mainScreen].bounds.size.width == 320){
    _scrollView.contentSize = CGSizeMake(320, self.view.frame.size.height);
    }
    else
    _scrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height);
    
    
    
    
    [datePickerView setOnDatePickerCancelClick:^{
    NSLog(@"We have to navigate on cart view");
    }];
    
    if([UIScreen mainScreen].bounds.size.width == 320){
    _scrollView.scrollEnabled = TRUE;
    }
    else
    _scrollView.scrollEnabled = FALSE;

    
    
    if(_isFromFB == TRUE){
    [self showUserInfoOnView];
   // picImageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[Session shared].objSignInResponse.profileImageURL]]];
    [picImageView setImageWithURL:[NSURL URLWithString:[Session shared].objSignInResponse.profileImageURL] placeholderImage:[UIImage imageNamed:@"ic_launcher.png"]];
    [self editClicked:self];
        
    }
    else{
    if([Session shared].objSignInResponse.userName.length == 0){
    picImageView.image = [UIImage imageNamed:@"ic_launcher.png"];

    maleGenderButton.selected = FALSE;
    femaleGenderButton.selected = TRUE;
    [maleGenderButton setBackgroundImage:[UIImage imageNamed:@"green_uncheck_button.png"] forState:UIControlStateNormal];
    [femaleGenderButton setBackgroundImage:[UIImage imageNamed:@"green_check_button.png"] forState:UIControlStateNormal];
        
    NSDate *todayDate = [NSDate date];
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init] ;
    [dateFormatter setDateFormat:@"dd-MMM-yyyy"];
    NSString *convertedString = [dateFormatter stringFromDate:todayDate];
    dateOfBirthTextField.text = convertedString;
        
    genderTextLabel.text = @"FEMALE";
    mobileNumberTextField.text = [Constant getUserPhoneNumber];
        
    NSLog(@"#GetUserPass :%@",[Constant getUserPass]);
        
    resetPasswordTextField.text = [Constant getUserPass];
        
    }
    
    else{
    maleLabel.hidden = TRUE;
    maleGenderButton.hidden = TRUE;
    femaleLabel.hidden = TRUE;
    femaleGenderButton.hidden = TRUE;
        
        
    datePickerButton.hidden = TRUE;
        dobCoverButton.hidden = TRUE;
        
   // NSLog(@"Check Image :%@",[Constant getUserImage:[Session shared].objSignInResponse.userID]);

   
    if([Constant getUserImage:[Session shared].objSignInResponse.userID] != nil)
    picImageView.image = [Constant getUserImage:[Session shared].objSignInResponse.userID];
    else if([Session shared].objSignInResponse.profileImageURL.length > 0)
  //  picImageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[Session shared].objSignInResponse.profileImageURL]]];
    [picImageView setImageWithURL:[NSURL URLWithString:[Session shared].objSignInResponse.profileImageURL] placeholderImage:[UIImage imageNamed:@"ic_launcher.png"]];

    else
    picImageView.image = [UIImage imageNamed:@"ic_launcher.png"];

    [self showUserInfoOnView];
    }
    
    
    [self setCustomFrameForSmallFrame];
    }

}


-(void)showUserInfoOnView{
    
    [self disableViewForSignInData];
        
    
    isUserLoginData = TRUE;

    nameTextField.text = [Session shared].objSignInResponse.userName;
    dateOfBirthTextField.text = [Constant convertServerBirthDate:[Session shared].objSignInResponse.userDOB];
    if([[Session shared].objSignInResponse.userGender isEqualToString:@"m"] || [[Session shared].objSignInResponse.userGender isEqualToString:@"male"]){
    genderTextLabel.text = @"MALE";
        
        
    maleGenderButton.selected = TRUE;
    femaleGenderButton.selected = FALSE;
    [maleGenderButton setBackgroundImage:[UIImage imageNamed:@"green_check_button.png"] forState:UIControlStateNormal];
    [femaleGenderButton setBackgroundImage:[UIImage imageNamed:@"green_uncheck_button.png"] forState:UIControlStateNormal];

    }
    if([[Session shared].objSignInResponse.userGender isEqualToString:@"f"] || [[Session shared].objSignInResponse.userGender isEqualToString:@"female"]){
    genderTextLabel.text = @"FEMALE";
    maleGenderButton.selected = FALSE;
    femaleGenderButton.selected = TRUE;
    [maleGenderButton setBackgroundImage:[UIImage imageNamed:@"green_uncheck_button.png"] forState:UIControlStateNormal];
    [femaleGenderButton setBackgroundImage:[UIImage imageNamed:@"green_check_button.png"] forState:UIControlStateNormal];

    }
    

    if(![[Session shared].objSignInResponse.userEmail isEqualToString:@"(null)"])
    emailTextField.text = [Session shared].objSignInResponse.userEmail;
    
    NSLog(@"Data :%@ %@",[Session shared].objSignInResponse.userMobileNum,[Session shared].objSignInResponse.userDOB);
    
    
    mobileNumberTextField.text = [Session shared].objSignInResponse.userMobileNum;
    resetPasswordTextField.text = [Constant getUserPass];
  //  resetPasswordTextField.frame
}


-(void)editClicked:(id)sender{
    _scrollView.scrollEnabled = TRUE;

    if(isSaveData == FALSE){
    rightButton.title = @"SAVE";
    penImgView.hidden = FALSE;
    
    [self setCustomFrameForSmallFrameWhenEditCliked];
    
    nameTextField.backgroundColor = [UIColor whiteColor];
    emailTextField.backgroundColor = [UIColor whiteColor];
      
    picImageView.userInteractionEnabled = TRUE;
    UITapGestureRecognizer *imageTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(picButtonClick:)];
    imageTap.delegate = self;
    [picImageView addGestureRecognizer:imageTap];
        
    }
    
    else{
        
    if([UIScreen mainScreen].bounds.size.width > 320)
    _scrollView.scrollEnabled = FALSE;

    picImageView.userInteractionEnabled = FALSE;
    rightButton.title = @"EDIT";
       
        
    [self setCustomFrameForSmallFrame];
        
    [self showHUDOnView];
    [self callProfileUpdateServiceBlock];

    }
    
    isSaveData = !isSaveData;

}


-(void)picButtonClick:(UITapGestureRecognizer *)sender{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    NSLog(@"chosenImage :%@",chosenImage);
    picImageView.image = chosenImage;
    
    UIImage *theImage = info[UIImagePickerControllerOriginalImage];
    
    NSLog(@"#TheImage :%@",theImage);
    
    NSLog(@"[Session shared].objSignInResponse.userID :%@",[Session shared].objSignInResponse.userID);
    
    
    [Constant saveUserImage:chosenImage userKey:[Session shared].objSignInResponse.userID];

    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

-(IBAction)maleRadioClicked:(id)sender{
    femaleGenderButton.selected = FALSE;
    maleGenderButton.selected = TRUE;
    [maleGenderButton setBackgroundImage:[UIImage imageNamed:@"green_check_button.png"] forState:UIControlStateNormal];
    [femaleGenderButton setBackgroundImage:[UIImage imageNamed:@"green_uncheck_button.png"] forState:UIControlStateNormal];
}

-(IBAction)femaleRadioClicked:(id)sender{
    femaleGenderButton.selected = TRUE;
    maleGenderButton.selected = FALSE;
    [femaleGenderButton setBackgroundImage:[UIImage imageNamed:@"green_check_button.png"] forState:UIControlStateNormal];
    [maleGenderButton setBackgroundImage:[UIImage imageNamed:@"green_uncheck_button.png"] forState:UIControlStateNormal];
}


-(IBAction)datePickerButtonClicked:(id)sender{
    
    
    coverView =  [self onAddDimViewOnSuperView:self.view];
    
    UITapGestureRecognizer *gesRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(coverViewClicked:)];
    gesRecognizer.numberOfTapsRequired = 1;
    [coverView addGestureRecognizer:gesRecognizer];
    
    
    if([UIScreen mainScreen].bounds.size.width == 320){
    datePicker = [[DatePickerView alloc]initWithFrame:CGRectMake(10, self.view.frame.origin.y + 150, 320-20, 250)];
    [coverView addSubview:datePicker];
    }
    else{
    datePicker = [[DatePickerView alloc]initWithFrame:CGRectMake(10, self.view.frame.origin.y + 150, 320-20, 250)];
    [coverView addSubview:datePicker];
    }
    
    [datePicker setOnDatePickerCancelClick:^{
    [datePicker removeFromSuperview];
    [coverView removeFromSuperview];
    }];
    
    [datePicker setOnDatePickerSetClick:^(NSString *date) {
    NSLog(@"TheSeelctedDate :%@",date);
        
    dateOfBirthTextField.text = date;
        
    [datePicker removeFromSuperview];
    [coverView removeFromSuperview];
    }];
    
    
    }




- (UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize
{
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

-(void)callProfileUpdateServiceBlock{
    ProfileUpdateService *profileUpdateService = [[ProfileUpdateService alloc]init];
    [profileUpdateService callProfileUpdateService:self.profileUpdateCriteria callback:^(NSError *error, SignInResponse *profileUpdateResponse) {
    if([profileUpdateResponse.status isEqualToString:@"failure"]){
    NSLog(@"The Status get error");
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Pay1" message:profileUpdateResponse.descMessage delegate:self cancelButtonTitle:@"DONE" otherButtonTitles:nil, nil];
    [alert show];
        
    }
    
    else{
    NSLog(@"The status is Success");
        
    [Session shared].objSignInResponse = profileUpdateResponse;;
        
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Pay1" message:profileUpdateResponse.descMessage delegate:self cancelButtonTitle:@"DONE" otherButtonTitles:nil, nil];
    [alert show];
    }
  //  [self setCustomFrameForSmallFrame];
        
    [self showUserInfoOnView];

    }];
    
    
    [self hideHUDOnView];
}


-(ProfileUpdateCriteria *)profileUpdateCriteria{
    ProfileUpdateCriteria *profileUpdateCriteria = [[ProfileUpdateCriteria alloc]init];
    profileUpdateCriteria.profileUpdateName = nameTextField.text;
    profileUpdateCriteria.profileUpdateDateOfBirth = [Constant convertBirthDate:dateOfBirthTextField.text]; //dateOfBirthTextField.text;
    
    
    if(maleGenderButton.selected == TRUE)
    profileUpdateCriteria.profileUpdateGender = @"m";
    if(femaleGenderButton.selected == TRUE)
    profileUpdateCriteria.profileUpdateGender = @"f";

    
    
    if(![[Session shared].objSignInResponse.userEmail isEqualToString:emailTextField.text])
    profileUpdateCriteria.profileUpdateEmail = emailTextField.text;
    
    if([[Session shared].objSignInResponse.userEmail isEqualToString:emailTextField.text]){
    profileUpdateCriteria.isSendPassword = FALSE;
    }
    
    else{
    profileUpdateCriteria.profileUpdatePassword = resetPasswordTextField.text;
    profileUpdateCriteria.isSendPassword = TRUE;
    }
    
    profileUpdateCriteria.profileUpdatePassword = resetPasswordTextField.text;
    NSString *strUDID = [[UIDevice currentDevice] uniqueDeviceIdentifier];
    NSLog(@"value is------>%@",strUDID);
    profileUpdateCriteria.profileUpdateUDID = strUDID;

    
   // profileUpdateCriteria.profileUpdateGemUDID = @"";
    profileUpdateCriteria.profileUpdateDeviceType = @"iOS";
    
    return profileUpdateCriteria;
}

-(void)coverViewClicked:(UITapGestureRecognizer *)recognizer{
    [datePicker removeFromSuperview];
    [coverView removeFromSuperview];
}

-(void)backClicked:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)setCustomFrameForSmallFrameWhenEditCliked
{
    
    if([UIScreen mainScreen].bounds.size.width == 320){
        picImageView.frame = CGRectMake(110, 13, 100, 100);
        
    }
    else
        picImageView.frame = CGRectMake(136, 13, 100, 100);
    
    
    
    nameLabel.frame = CGRectMake(16, firstThinImage.frame.origin.y + firstThinImage.frame.size.height  + 10, 51, 21);
    
    if([UIScreen mainScreen].bounds.size.width == 320){
    nameTextField.frame = CGRectMake(nameLabel.frame.origin.x + nameLabel.frame.size.width, firstThinImage.frame.origin.y + firstThinImage.frame.size.height  + 10, 240, 40);
   
    }
    else
    nameTextField.frame = CGRectMake(nameLabel.frame.origin.x + nameLabel.frame.size.width, firstThinImage.frame.origin.y + firstThinImage.frame.size.height  + 10, self.view.frame.size.width - 75, 40);
    
    
    
    [self addSpaceBetweenTextFieldOriginAndText:nameTextField];
    
    
    dateOfBirthLabel.frame = CGRectMake(11, secondThinImage.frame.origin.y + secondThinImage.frame.size.height  + 20, 125, 21);
    
    
    if([UIScreen mainScreen].bounds.size.width == 320){
    dateOfBirthTextField.frame = CGRectMake(137, secondThinImage.frame.origin.y + secondThinImage.frame.size.height  + 20, 139, 30);
    datePickerButton.frame = CGRectMake(279, secondThinImage.frame.origin.y + secondThinImage.frame.size.height  + 20, 30, 30);
    dobCoverButton.frame = CGRectMake(275, secondThinImage.frame.origin.y + secondThinImage.frame.size.height  + 10, 40, 40);

    }
    else{
    dateOfBirthTextField.frame = CGRectMake(137, secondThinImage.frame.origin.y + secondThinImage.frame.size.height  + 20, 180, 30);
    datePickerButton.frame = CGRectMake(dateOfBirthTextField.frame.origin.x + dateOfBirthTextField.frame.size.width + 10, secondThinImage.frame.origin.y + secondThinImage.frame.size.height  + 20, 30, 30);
    dobCoverButton.frame = CGRectMake(dateOfBirthTextField.frame.origin.x + dateOfBirthTextField.frame.size.width + 5, secondThinImage.frame.origin.y + secondThinImage.frame.size.height  + 10, 40, 40);

    }
    
    
    datePickerButton.hidden = FALSE;
    dobCoverButton.hidden = FALSE;
    

    genderLabel.frame = CGRectMake(11, thirdThinImage.frame.origin.y + thirdThinImage.frame.size.height  + 10, 70, 21);
    genderTextLabel.frame =  CGRectMake(self.view.frame.size.width - 100, thirdThinImage.frame.origin.y + thirdThinImage.frame.size.height  + 20, 80, 30);
    genderTextLabel.hidden = TRUE;

    
    
    maleGenderButton.frame = CGRectMake(genderLabel.frame.origin.x + genderLabel.frame.size.width + 10, thirdThinImage.frame.origin.y + thirdThinImage.frame.size.height  + 15, 30, 30);
    
    
    maleLabel.frame = CGRectMake(maleGenderButton.frame.origin.x + maleGenderButton.frame.size.width + 5, thirdThinImage.frame.origin.y + thirdThinImage.frame.size.height  + 15, 46, 21);
    
    
    femaleGenderButton.frame = CGRectMake(maleLabel.frame.origin.x + maleLabel.frame.size.width + 10, thirdThinImage.frame.origin.y + thirdThinImage.frame.size.height  + 15, 30, 30);
    
    femaleLabel.frame = CGRectMake(femaleGenderButton.frame.origin.x + femaleGenderButton.frame.size.width + 5, thirdThinImage.frame.origin.y + thirdThinImage.frame.size.height  + 15, 90, 21);
    
    maleLabel.hidden = FALSE;
    maleGenderButton.hidden = FALSE;
    femaleLabel.hidden = FALSE;
    femaleGenderButton.hidden = FALSE;
    
    
    
    emailLabel.frame = CGRectMake(11, fourthThinImage.frame.origin.y + fourthThinImage.frame.size.height  + 10, 51, 21);
    
    
    if([UIScreen mainScreen].bounds.size.width == 320){
    emailTextField.frame = CGRectMake(emailLabel.frame.origin.x  + emailLabel.frame.size.width, fourthThinImage.frame.origin.y + fourthThinImage.frame.size.height  +10, 240, 40);
        
    }
    else
    emailTextField.frame = CGRectMake(emailLabel.frame.origin.x  + emailLabel.frame.size.width, fourthThinImage.frame.origin.y + fourthThinImage.frame.size.height  +10, self.view.frame.size.width - 75, 40);
    
    
    
    [self addSpaceBetweenTextFieldOriginAndText:emailTextField];


    
    mobileNumberLabel.frame = CGRectMake(11, fifthThinImage.frame.origin.y + fifthThinImage.frame.size.height  + 20, 143, 21);
    
    if([UIScreen mainScreen].bounds.size.width == 320){
    mobileNumberTextField.frame = CGRectMake(170, fifthThinImage.frame.origin.y + fifthThinImage.frame.size.height  + 20, 131, 30);
        
    }
      resetPasswordLabel.frame = CGRectMake(11, 440, 157, 21);
    
    
    if([UIScreen mainScreen].bounds.size.width == 320){
        chnagePasswordView.frame = CGRectMake(160, sixthThinImage.frame.origin.y + sixthThinImage.frame.size.height  + 20, 150, 40);
        resetPasswordTextField.frame = CGRectMake(18, 8, 110, 21);
        resetPasswordTextField.textAlignment = NSTextAlignmentRight;
        penImgView.frame = CGRectMake(resetPasswordTextField.frame.origin.x + resetPasswordTextField.frame.size.width, 5, 20, 25);
    }
    else{
        chnagePasswordView.frame = CGRectMake(200, sixthThinImage.frame.origin.y + sixthThinImage.frame.size.height  + 20, 150, 40);
        resetPasswordTextField.frame = CGRectMake(18, 8, 110, 21);
        resetPasswordTextField.textAlignment = NSTextAlignmentRight;
        penImgView.frame = CGRectMake(resetPasswordTextField.frame.origin.x + resetPasswordTextField.frame.size.width, 5, 20, 25);
    }
    
    
    
    
    emailTextField.userInteractionEnabled = TRUE;
    nameTextField.userInteractionEnabled = TRUE;
    penImgView.userInteractionEnabled = TRUE;
    
    
    nameTextField.textAlignment = NSTextAlignmentLeft;
    emailTextField.textAlignment = NSTextAlignmentLeft;
    
}


-(void)setCustomFrameForSmallFrame{
    if([UIScreen mainScreen].bounds.size.width == 320){
    picImageView.frame = CGRectMake(110, 13, 100, 100);
   
    }
    else
    picImageView.frame = CGRectMake(136, 13, 100, 100);
    
    
    nameLabel.frame = CGRectMake(16, 147, 51, 21);

    nameTextField.frame = CGRectMake(nameLabel.frame.origin.x + nameLabel.frame.size.width, firstThinImage.frame.origin.y + firstThinImage.frame.size.height  + 10, 204, 40);
    nameTextField.backgroundColor = [UIColor clearColor];
    nameTextField.textAlignment = NSTextAlignmentRight;
    
    if([UIScreen mainScreen].bounds.size.width == 320){
    nameTextField.frame = CGRectMake(nameLabel.frame.origin.x + nameLabel.frame.size.width, firstThinImage.frame.origin.y + firstThinImage.frame.size.height  + 10, 240, 40);
    }
    else
    nameTextField.frame = CGRectMake(nameLabel.frame.origin.x + nameLabel.frame.size.width, firstThinImage.frame.origin.y + firstThinImage.frame.size.height  + 10, self.view.frame.size.width - 75, 40);
    
    dateOfBirthLabel.frame = CGRectMake(11, secondThinImage.frame.origin.y + secondThinImage.frame.size.height  + 20, 125, 21);
    
    if([UIScreen mainScreen].bounds.size.width == 320){
    dateOfBirthTextField.frame = CGRectMake(137, secondThinImage.frame.origin.y + secondThinImage.frame.size.height  + 20, 170, 30);
    }
    else
    dateOfBirthTextField.frame = CGRectMake(137, secondThinImage.frame.origin.y + secondThinImage.frame.size.height  + 20, self.view.frame.size.width - 150, 30);
    
  
    datePickerButton.frame = CGRectMake(279, secondThinImage.frame.origin.y + secondThinImage.frame.size.height  + 20, 30, 30);
    dobCoverButton.frame = CGRectMake(275, secondThinImage.frame.origin.y + secondThinImage.frame.size.height  + 10, 40, 40);
    
    dateOfBirthTextField.textAlignment = NSTextAlignmentRight;

    
    if(isSaveData == TRUE){
    maleGenderButton.hidden = TRUE;
    maleLabel.hidden = TRUE;
    femaleGenderButton.hidden = TRUE;
    femaleLabel.hidden = TRUE;
       
    genderTextLabel.hidden = FALSE;
    genderLabel.frame = CGRectMake(11, thirdThinImage.frame.origin.y + thirdThinImage.frame.size.height  + 20, 70, 21);

    if([UIScreen mainScreen].bounds.size.width == 320)
    genderTextLabel.frame =  CGRectMake(self.view.frame.size.width - 100, thirdThinImage.frame.origin.y + thirdThinImage.frame.size.height  + 20, 80, 30);
        
    else{
    genderTextLabel.frame =  CGRectMake(265, thirdThinImage.frame.origin.y + thirdThinImage.frame.size.height  + 20, 100, 30);

    }
     
    }
    
    else{
    maleGenderButton.hidden = FALSE;
    maleLabel.hidden = FALSE;
    femaleGenderButton.hidden = FALSE;
    femaleLabel.hidden = FALSE;
        
    if(isUserLoginData == FALSE){
    genderTextLabel.hidden = FALSE;
    }
  
        
    if([UIScreen mainScreen].bounds.size.width == 320){
    genderTextLabel.frame =  CGRectMake(228, thirdThinImage.frame.origin.y + thirdThinImage.frame.size.height  + 20, 80, 30);
      
    }
    else
    genderTextLabel.frame =  CGRectMake(265, thirdThinImage.frame.origin.y + thirdThinImage.frame.size.height  + 20, 80, 30);
        
        
    maleGenderButton.frame = CGRectMake(genderLabel.frame.origin.x + genderLabel.frame.size.width + 20, thirdThinImage.frame.origin.y + thirdThinImage.frame.size.height  + 20, 30, 30);
        
        
    maleLabel.frame = CGRectMake(maleGenderButton.frame.origin.x + maleGenderButton.frame.size.width + 5, thirdThinImage.frame.origin.y + thirdThinImage.frame.size.height  + 20, 46, 21);
        
        
    femaleGenderButton.frame = CGRectMake(maleLabel.frame.origin.x + maleLabel.frame.size.width + 10, thirdThinImage.frame.origin.y + thirdThinImage.frame.size.height  + 20, 30, 30);
        
    femaleLabel.frame = CGRectMake(femaleGenderButton.frame.origin.x + femaleGenderButton.frame.size.width  , thirdThinImage.frame.origin.y + thirdThinImage.frame.size.height  + 20, 90, 21);
        
        
    }
    
    emailLabel.frame = CGRectMake(11, 325, 51, 24);
    emailTextField.backgroundColor = [UIColor clearColor];
    emailTextField.textAlignment = NSTextAlignmentRight;
    
    
    if([UIScreen mainScreen].bounds.size.width == 320){
    emailTextField.frame = CGRectMake(emailLabel.frame.origin.x  + emailLabel.frame.size.width, fourthThinImage.frame.origin.y + fourthThinImage.frame.size.height  +10, 240, 40);
        
    }
    else
    emailTextField.frame = CGRectMake(emailLabel.frame.origin.x  + emailLabel.frame.size.width, fourthThinImage.frame.origin.y + fourthThinImage.frame.size.height  +10, self.view.frame.size.width - 75, 40);
    
    
    
    mobileNumberLabel.frame = CGRectMake(11, fifthThinImage.frame.origin.y + fifthThinImage.frame.size.height  + 20, 143, 21);
    
    
    if([UIScreen mainScreen].bounds.size.width == 320){
        mobileNumberTextField.frame = CGRectMake(170, fifthThinImage.frame.origin.y + fifthThinImage.frame.size.height  + 20, 131, 30);
    }
    else{
    mobileNumberTextField.frame = CGRectMake(170, fifthThinImage.frame.origin.y + fifthThinImage.frame.size.height  + 20, self.view.frame.size.width - 180, 30);

    }
    resetPasswordLabel.frame = CGRectMake(11, 440, 157, 21);
    
    
    if(isSaveData == TRUE){
    penImgView.hidden = TRUE;
    
    
    if([UIScreen mainScreen].bounds.size.width == 320){
    chnagePasswordView.frame = CGRectMake(160, sixthThinImage.frame.origin.y + sixthThinImage.frame.size.height  + 20, 150, 40);
    resetPasswordTextField.frame = CGRectMake(18, 8, 120, 21);
    resetPasswordTextField.textAlignment = NSTextAlignmentRight;
    }
        
        
    else{
    chnagePasswordView.frame = CGRectMake(210, sixthThinImage.frame.origin.y + sixthThinImage.frame.size.height  + 20, 150, 40);
  //  resetPasswordTextField.frame = CGRectMake(18, 8, 90, 21);
   // resetPasswordTextField.textAlignment = NSTextAlignmentRight;
        
    resetPasswordTextField.frame = CGRectMake(18, 8, chnagePasswordView.frame.size.width - 10, 21);

    }
    
    }
    
    if(isSaveData == FALSE){
    penImgView.hidden = TRUE;
    penImgView.userInteractionEnabled = FALSE;
        
        
    if([UIScreen mainScreen].bounds.size.width == 320){
    chnagePasswordView.frame = CGRectMake(160, sixthThinImage.frame.origin.y + sixthThinImage.frame.size.height  + 20, 150, 40);
    resetPasswordTextField.frame = CGRectMake(18, 8, 120, 21);
    resetPasswordTextField.textAlignment = NSTextAlignmentRight;
    }
    else
    resetPasswordTextField.frame = CGRectMake(18, 8, chnagePasswordView.frame.size.width - 10, 21);
    }
    
    [self disableViewForSignInData];

}


-(void)disableViewForSignInData{
    maleLabel.hidden = TRUE;
    maleGenderButton.hidden = TRUE;
    femaleGenderButton.hidden = TRUE;
    femaleLabel.hidden = TRUE;
    
    
    datePickerButton.hidden = TRUE;
    dobCoverButton.hidden = TRUE;
    picImageView.userInteractionEnabled = NO;
    picImageView.userInteractionEnabled = NO;
    nameTextField.userInteractionEnabled = NO;
    dateOfBirthTextField.userInteractionEnabled = NO;
    emailTextField.userInteractionEnabled = NO;
    mobileNumberTextField.userInteractionEnabled = NO;
    resetPasswordTextField.userInteractionEnabled = NO;
}

-(IBAction)changePasswordClicked:(id)sender{
    [self showHUDOnView];
    [self callForgotPasswordServiceBlock];
}

-(void)callForgotPasswordServiceBlock{
    UpdatePasswordService *objService = [[UpdatePasswordService alloc]init];
    [objService callForgotPasswordService:[Constant getUserPhoneNumber] vtype:@"1" callback:^(NSError *error, UpdatePasswordResponse *objForgotPswdRespons) {
    if(error)return ;
    else{
    [self hideHUDOnView];
        [self navigateToChangePassword:objForgotPswdRespons.messageOTP];
    }
    }];
}

-(void)navigateToChangePassword:(NSString *)otp{
    UpdatePasswordViewController *updatePswdVC = [self.storyboard instantiateViewControllerWithIdentifier:@"updatePasswordViewC"];
    updatePswdVC.isUserLoggedIn = TRUE;
    updatePswdVC.pswdOTP = otp;

    [self.navigationController pushViewController:updatePswdVC animated:YES];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return TRUE;
}
/*
-(void)passwordViewTapped:(UITapGestureRecognizer *)recognizer{
    UpdatePasswordViewController *updatePswdVC = [self.storyboard instantiateViewControllerWithIdentifier:@"updatePasswordViewC"];
    //Call Update Password Service here.
    [self.navigationController pushViewController:updatePswdVC animated:YES];
}*/

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
