//
//  GetUpdatedDealResponse.m
//  Pay1
//
//  Created by Annapurna on 09/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "GetUpdatedDealResponse.h"

@implementation GetUpdatedDealResponse
-(id)init {
    self = [super init];
    if(self) {
        self.allDealsArray = [[NSMutableArray alloc] init] ;
        self.offerDetailsArray = [[NSMutableArray alloc]init];
    }
    return self;
}
@end
