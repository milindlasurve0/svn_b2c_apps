//
//  SignOutService.h
//  Pay1
//
//  Created by webninjaz on 03/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "Service.h"

@class SignOutResponse;

typedef void (^signOutResponse)(NSError *error,SignOutResponse *signOutResponse);

@interface SignOutService : Service

-(void)callSignOutService:(signOutResponse)callback;

@end
