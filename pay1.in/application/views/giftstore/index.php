<?php $this->load->view('common/doctype_html'); 

    $current_page_url =  "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    $meteTagData = getMetaTagDetails();
    foreach ($meteTagData as $row){
      
      if($row->page_url == $current_page_url){
        $new_giftpage_tittle =  $row->page_tittle;
        $new_giftpage_meta_description =  $row->page_meta_description;  
        $new_giftpage_meta_tag =  $row->page_meta_tag;  
      }
      
    }
 
?>
<meta name="description" content=" <?php if($new_giftpage_meta_description){ echo $new_giftpage_meta_description; }else{ ?>Recharge or pay bills to earn gift coins. And choose a Gift from Gift Store to aspire your dream & make the most out of your recharge. <?php } ?> ">
<meta name="keywords" content="<?php if($new_giftpage_meta_tag){ echo $new_giftpage_meta_tag; }else{ ?>Gift Store - Recharge, Earn Gift Coins & Grab a Gift , Pay1 <?php } ?>" />
<title><?php if($new_giftpage_tittle){ echo $new_giftpage_tittle; }else{ ?>Gift Store - Recharge, Earn Gift Coins & Grab a Gift | Pay1 <?php } ?></title>     
<?php $this->load->view('common/header'); 

    $domain_url = CDEV_URL;
    $imageData = getImageDetails();

    foreach ($imageData as $row){
        if(($row->image_flag == 2) && ($row->gift_type == 0)){
        $new_img_url = $domain_url.$row->image_url;   
        }
    }

?>
<style type="text/css">
.autocomplete-suggestions {border: 1px solid #999; background: #fff; cursor: default; overflow: auto; }
.autocomplete-suggestion { padding: 10px 5px; font-size: 1.0em; white-space: nowrap; overflow: hidden; }
.autocomplete-selected { background: #f0f0f0; }
.autocomplete-suggestions strong { font-weight: normal; color: #3399ff; }
</style>
      <link rel="stylesheet" href="/assets/css/style.css">

      <div class="clearfix" style="clear:both;"></div>
      
      <!-- ======================== Gift Header Image ========================== -->
      <input type="hidden" id="gift_store_img_hdn" value="<?php if(isset($new_img_url)){  echo $new_img_url; }else{ echo '/assets/images/background/Gift-Store.jpg';}  ?>">
      <div class="service_page1">
          <img src="/assets/images/default_img/pay1_loader.jpg" alt="" class="img-responsive" style="" id="gift_store_img">
      </div>
      <div class="bottom_bg" style=""></div>
      <!-- ======================== Header Image end========================== -->
       <div class="clearfix"></div>

      <div class="container-fluid">
         <div class="container border-both">
            <div class="row">
               <div class="col-md-12 text-center margin-top-25">
                  <div class="row">
                 <form action=" " >
                        <div class="col-md-4 col-md-offset-3">
                           <input type="text" id ="search_brand" name="search_brand" class="form-control autocomplete" placeholder="Search by typing the product or brand name...">
                        </div>
                        <div class="col-md-1">
                            <!--<input type="submit" value="SEARCH" class="btn btn-save search-btn">-->
                            <button type="button" class="btn btn-save search-btn" onclick="search_brands()" style="border-radius:5px">SEARCH</button>
                         </div>
                      </form>
               </div>
            </div>
              
               <div class="col-md-12">
                  
                   <p class="heading margin-top-40 mobile_left col-md-12 "><span class="page_title">Featured </span><span class="pull-right v"><a class="see_offers" href="/categories-featured-cid-1">See all <span class=""><i class="glyphicon glyphicon-chevron-right mob"></i></span></a></span>
                  
                  <div class='padding-both'>
                      <div id="products" class="row list-group">
                        <?php $k=1;
                        foreach($featured['description'] as $row): ?> 
                        <?php    $deal_name_arr[] = array("value" => $row['deal'],"data"=> $row['deal']); ?>
                    
                    <div class="item col-md-3 col-xs-6">
                        <div class="thumbnail">
                           <div class="col-md-10 col-xs-10">
                               <p class="p_detail"><?php echo $row['offer_desc']; ?></p>
                           </div>
                           <div class="col-md-2 col-xs-2">
                               <p class="<?php echo $cls = (($row['mylikes']==0)?"p_icon":"p_icon red_active") ; ?>" id="<?php echo $k."_".$row['id']; ?>" onclick="set_mylike('<?php echo $k."_".$row['id']; ?>',<?php echo $row['id'];  ?>)" value="<?php echo $row['id'];  ?>" ><i class="glyphicon glyphicon-heart-empty"></i></p>
                           </div>
<!--                            <a href="/giftstore/giftdetails/<?php // echo $id_area = $row['deal_id'].'_'.trim($row['area']); ?>" style="padding-left: 1px;padding-bottom: 0px">
                                <img class="group list-group-image img-responsive" src="<?php // echo $row['img_url']; ?>"  alt="" style="width:189px;height: 96px" />
                            </a>-->
                            
                            <a onclick="GoToGiftDetails('<?php echo $row['deal_id']; ?>','<?php  echo (isset($row['deal_url']) && !empty($row['deal_url'])) ? $row['deal_url'] : 'gift';  ?>')" style="padding-left: 1px;padding-bottom: 0px">
                                <img class="group list-group-image img-responsive" src="<?php echo $row['img_url']; ?>"  alt="" style="width:189px;height: 96px" />
                            </a>
                            
                            
                            <div class="caption" style="padding-top:0px">

                                <div class="row">
                                    <div class="col-md-4 col-xs-4">
                                        <?php if($row['logo'] == ''){ ?>
                                        <img class="logo" src="/public/assets/images/default_img/ic_logo_entertainment.png" alt="">
                                        <?php }else{ ?>
                                            <img src="<?php echo $row['logo']; ?>" class="logo"> 
                                        <?php } ?>
                                    </div>
                                     <?php if(isset($row['by_voucher']) && !empty($row['by_voucher']) && $row['by_voucher']==1){ ?>
                                    <!-- Showing both amount amount and  Loyaty points-->
                                    <div class="col-md-8 col-xs-8" style="text-align:right">
                                        <i class="fa fa-inr"></i> <?php echo $row['offer_price']; ?> + <img src="/assets/images/gift/iconall.png" alt=""> <span><?php echo $row['min_amount']; ?></span>
                                     </div>
                                        
                                    <?php }else{ ?> 
                                        <!-- Showing Only Loyaty points-->
                                        <div class="col-md-6 col-xs-6">
                                            <img src="/assets/images/gift/iconall.png" alt=""> <span><?php echo $row['min_amount']; ?></span>
                                        </div>
                                        <?php } ?>
                                  <div class="col-md-12 col-xs-12 text-left margin-top-20">  
                                       <p class="group inner list-group-item-text"><?php echo $row['deal']; ?></p>
                                       <?php if(!empty($row['area'])){  ?>
                                       <p><img src="/assets/images/gift/icongift.png" alt=""> <span> <?php echo $row['area']; ?></span></p>
                                       <?php } ?>
                                    </div>
                                </div>
                           </div>
                        </div>
                     </div>
                    <?php $k++; endforeach; ?> 
                  
                
                        <p class="heading margin-top-40 mobile_left col-md-12 "><span class="page_title">Recommended </span><span class="pull-right v"><a class="see_offers" href="/categories-recommended-for-you-cid-4">See all <span class=""><i class="glyphicon glyphicon-chevron-right mob"></i></span></a></span>
                   
                      <?php $k=1; foreach($recommended['description'] as $row): 
                                  $deal_name_arr[] = array("value" => $row['deal'],"data"=> $row['deal']); ?>
                            
                       <div class="item col-md-3 col-xs-6">
                        <div class="thumbnail">
                           <div class="col-md-10 col-xs-10">
                               <p class="p_detail"><?php echo $row['offer_desc']; ?></p>
                           </div>
                           <div class="col-md-2 col-xs-2">
                              <p class="<?php echo $cls = (($row['mylikes']==0)?"p_icon":"p_icon red_active") ; ?>"  id="<?php echo $k."_".$row['id']; ?>" onclick="set_mylike('<?php echo $k."_".$row['id']; ?>',<?php echo $row['id'];  ?>)"  value="<?php echo $row['id'];  ?>" ><i class="glyphicon glyphicon-heart-empty"></i></p>
                           </div>
<!--                            <a href="/giftstore/giftdetails/<?php // echo $id_area = $row['deal_id'].'_'.trim($row['area']); ?>" style="padding-left: 1px;">
                               <img class="group list-group-image img-responsive" src="<?php // echo $row['img_url']; ?>"  alt="" style="width:189px;height: 96px" />
                            </a>-->
                            
                            <a onclick="GoToGiftDetails('<?php echo $row['deal_id']; ?>','<?php  echo (isset($row['deal_url']) && !empty($row['deal_url'])) ? $row['deal_url'] : 'gift';  ?>')" style="padding-left: 1px;padding-bottom: 0px">
                                <img class="group list-group-image img-responsive" src="<?php echo $row['img_url']; ?>"  alt="" style="width:189px;height: 96px" />
                            </a>
                             
                            <div class="caption">

                                <div class="row">
                                    <div class="col-md-6 col-xs-6">
                                     <?php if($row['logo'] == ''){ ?>
                                        <img class="logo" src="/public/assets/images/default_img/ic_logo_online.png" alt="">
                                        <?php }else{ ?>
                                            <img src="<?php echo $row['logo']; ?>" class="logo"> 
                                        <?php } ?>
                                    </div>
                                     <?php if(isset($row['by_voucher']) && !empty($row['by_voucher']) && $row['by_voucher']==1){ ?>
                                    <!-- Showing both amount amount and  Loyaty points-->
                                    <div class="col-md-8 col-xs-8" style="text-align:right">
                                        <i class="fa fa-inr"></i> <?php echo $row['offer_price']; ?> + <img src="/assets/images/gift/iconall.png" alt=""> <span><?php echo $row['min_amount']; ?></span>
                                     </div>
                                        
                                    <?php }else{ ?> 
                                        <!-- Showing Only Loyaty points-->
                                        <div class="col-md-6 col-xs-6">
                                            <img src="/assets/images/gift/iconall.png" alt=""> <span><?php echo $row['min_amount']; ?></span>
                                        </div>
                                        <?php } ?>
                                    <div class="col-md-12 col-xs-12 text-left margin-top-20">  
                                       <p class="group inner list-group-item-text"><?php echo $row['deal']; ?></p>
                                       <?php if(!empty($row['area'])){  ?>
                                       <p><img src="/assets/images/gift/icongift.png" alt=""> <span> <?php echo $row['area']; ?></span></p>
                                       <?php } ?>
                                    </div>
                                </div>
                           </div>
                        </div>
                     </div>
                    <?php $k++; endforeach; ?> 
                    
                      <p id="demo"></p>    

                      <p class="heading margin-top-40 mobile_left col-md-12 "><span class="page_title">Categories </span><span class="pull-right v"><a class="see_offers" href="/categories">See all <span class=""><i class="glyphicon glyphicon-chevron-right mob"></i></span></a></span>
                    
                      <?php $k=1; foreach($category['description'] as $row): ?> 
                     <?php $deal_name_arr[] = array("value" => $row['deal'],"data"=> $row['deal']); ?>      
                          
                       <div class="item col-md-3 col-xs-6">
                        <div class="thumbnail">
                           <div class="col-md-10 col-xs-10">
                               <p class="p_detail"><?php echo $row['offer_desc']; ?></p>
                           </div>
                           <div class="col-md-2 col-xs-2">
                              
                            <p class="<?php echo $cls = (($row['mylikes']==0)?"p_icon":"p_icon red_active") ; ?>"  id="<?php echo $k."_".$row['id']; ?>" onclick="set_mylike('<?php echo $k."_".$row['id']; ?>',<?php echo $row['id'];  ?>)" value="<?php echo $row['id'];  ?>" ><i class="glyphicon glyphicon-heart-empty"></i></p>
                           </div>
<!--                            <a href="/giftstore/giftdetails/<?php // echo $id_area = $row['deal_id'].'_'.trim($row['area']); ?>" style="padding-left: 1px;">
                               <img class="group list-group-image img-responsive" src="<?php // echo $row['img_url']; ?>"  alt="" style="width:189px;height: 96px" />
                            </a>-->
                            
                            <a onclick="GoToGiftDetails('<?php echo $row['deal_id']; ?>','<?php  echo (isset($row['deal_url']) && !empty($row['deal_url'])) ? $row['deal_url'] : 'gift';  ?>')" style="padding-left: 1px;padding-bottom: 0px">
                                <img class="group list-group-image img-responsive" src="<?php echo $row['img_url']; ?>"  alt="" style="width:189px;height: 96px" />
                            </a>
                            
                            <div class="caption">

                                <div class="row">
                                    <?php if(isset($row['by_voucher']) && !empty($row['by_voucher']) && $row['by_voucher']==1){ ?>
                                    <div class="col-md-4 col-xs-4">
                                    <?php }else{ ?>
                                    <div class="col-md-6 col-xs-6">
                                    <?php } ?>
                                    <?php if($row['logo'] == ''){ ?>
                                        <img class="logo" src="/public/assets/images/default_img/ic_logo_evoucher.png" alt="">
                                        <?php }else{ ?>
                                            <img src="<?php echo $row['logo']; ?>" class="logo"> 
                                        <?php } ?>
                                    </div>
                                    <?php if(isset($row['by_voucher']) && !empty($row['by_voucher']) && $row['by_voucher']==1){ ?>
                                    <!-- Showing both amount amount and  Loyaty points-->
                                    <div class="col-md-8 col-xs-8" style="text-align:right">
                                        <i class="fa fa-inr"></i> <?php echo $row['offer_price']; ?> + <img src="/assets/images/gift/iconall.png" alt=""> <span><?php echo $row['min_amount']; ?></span>
                                     </div>
                                        
                                    <?php }else{ ?> 
                                        <!-- Showing Only Loyaty points-->
                                        <div class="col-md-6 col-xs-6">
                                            <img src="/assets/images/gift/iconall.png" alt=""> <span><?php echo $row['min_amount']; ?></span>
                                        </div>
                                    <?php } ?>
                                    
                                    <div class="col-md-12 col-xs-12 text-left margin-top-20">  
                                       <p class="group inner list-group-item-text"><?php echo $row['deal']; ?></p>
                                       <?php if(!empty($row['area'])){  ?>
                                       <p><img src="/assets/images/gift/icongift.png" alt=""> <span> <?php echo $row['area']; ?></span></p>
                                       <?php } ?>
                                    </div>
                                </div>
                           </div>
                        </div>
                     </div>
                    <?php $k++; endforeach; ?> 
                            
                        <div class="clearfix"></div>

                   </div>
                  </div>
                
            <div class="margin-top-30 clearfix"></div>
               </div>


            </div>
         </div>
      </div>
      <div class="clearfix"></div>
      
      <div class="modal fade" role="dialog" id="failure">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="cls" data-dismiss="modal">&times;</button>
                  <h3>Payment Failure</h3>
               </div>
               <div class="modal-body">
                  <p>Sorry! Your previous payment attempt failed.</p>
                  <div class="pull-right">
                     <button class="modal_button" id="try-again">Try Again</button>
                     <button class="modal_button" id="cancel-button" data-dismiss="modal">Cancel</button>
                  </div>
                  <div class="clearfix"></div>
               </div>
            </div>
         </div>
      </div>
        <script type="text/javascript" src="/public/js/jquery.autocomplete.min.js"></script>
        <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
<!--      <script type="text/javascript" src="assets/js/jssor.slider.mini.js"></script>
      <script src="assets/js/slider.js"></script>-->
      <script type="text/javascript">
         $(document).ready(function($) {
             $('body').scrollToTop({skin: 'cycle'}); 
            $("#gift_store_img").attr("src",$("#gift_store_img_hdn").val());
            map_lat  = localStorage.getItem('latitude');
            map_long = localStorage.getItem('longitude');
            if(map_lat==null && map_long == null ){
                getLocation();
            } else {
              localStorage.setItem("latitude", map_lat);
              localStorage.setItem("longitude",map_long);
            }
         });
         
        var data1 = <?php echo json_encode($deal_name_arr); ?>
        
        $('.autocomplete').autocomplete({
         lookup: data1,
         onSelect: function (suggestion) {
          search_brands();
         }
        });
        
         
         
        var xcv;
        var lat_lng;
        
       xcv = document.getElementById("demo");

        function getLocation() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPosition);
            } else { 
                xcv.innerHTML = "Geolocation is not supported by this browser.";
            }
        }

        function showPosition(position) {
//            xcv.innerHTML = "Latitude: " + position.coords.latitude + 
//            "<br>Longitude: " + position.coords.longitude;
            localStorage.setItem("latitude", position.coords.latitude);
            localStorage.setItem("longitude", position.coords.longitude);
//            xcv.innerHTML = localStorage.getItem("latitude");
        }
        
       

        
      </script>
        <script type="text/javascript" src="assets/js/gift_custom.js"> </script>

<?php $this->load->view('common/footer'); ?>