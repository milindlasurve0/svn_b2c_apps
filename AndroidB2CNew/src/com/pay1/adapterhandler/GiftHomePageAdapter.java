package com.pay1.adapterhandler;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.pay1.R;
import com.pay1.databasehandler.FreeBies;
import com.pay1.utilities.RoundedTransformation;
import com.squareup.picasso.Picasso;

public class GiftHomePageAdapter extends BaseAdapter {
	Context context;
	List<FreeBies> data;
	private int o_id = 0;

	Typeface Reguler;

	public GiftHomePageAdapter(Context context, List<FreeBies> data) {
		this.context = context;
		this.data = data;
		Reguler = Typeface.createFromAsset(this.context.getAssets(),
				"Roboto-Regular.ttf");
		
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		try {
			GiftHolder holder = null;

			if (row == null) {
				LayoutInflater inflater = ((Activity) context)
						.getLayoutInflater();
				row = inflater.inflate(R.layout.gift_cell_home_adapter, null);

				holder = new GiftHolder();
				holder.imageView_Photo = (ImageView) row
						.findViewById(R.id.imageView_Photo);
				holder.textView_OfferDesc = (TextView) row
						.findViewById(R.id.textView_OfferDesc);
				holder.textView_OfferDesc.setText(data.get(position).getFreebieShortDesc());
			try {
				Picasso.with(context).load(data.get(position).getFreebieLogoURL())
						.placeholder(R.drawable.deal_default)
						.transform(new RoundedTransformation(0, 0))
						.into(holder.imageView_Photo);
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			}
		} catch (Exception e) {
		}
		return row;

	}

	class GiftHolder {
		TextView textView_OfferDesc;
		ImageView imageView_Photo;
	

	}

	
	
}
