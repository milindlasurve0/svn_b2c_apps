﻿using Pay1B2C.Model;
using SQLite;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pay1B2C.ViewModel
    {
    class OperatorHelper
        {
        SQLiteConnection dbConn;

      
        public async Task<bool> onCreate(string DB_PATH)
            {
            try
                {
                if(!CheckFileExists(App.PAY1_DB_PATH).Result)
                    {
                    using(dbConn = new SQLiteConnection(App.PAY1_DB_PATH))
                        {
                        dbConn.CreateTable<OperatorDB>();
                        }
                    }
                return true;
                }
            catch
                {
                return false;
                }
            }
        private async Task<bool> CheckFileExists(string fileName)
            {
            try
                {
                var store = await Windows.Storage.ApplicationData.Current.LocalFolder.GetFileAsync(fileName);
                return true;
                }
            catch
                {
                return false;
                }
            }


        public OperatorDB ReadOperatorDBByFlag(String flag)
            {
            using(var dbConn = new SQLiteConnection(App.PAY1_DB_PATH))
                {
                var existingconact = dbConn.Query<OperatorDB>("SELECT * from OperatorDB where flag = '"+flag+"'").FirstOrDefault();
                return existingconact;
                }
            }
       
        public ObservableCollection<OperatorDB> ReadOperatorDB(String opFlag)
            {
            using(var dbConn = new SQLiteConnection(App.PAY1_DB_PATH))
                {
                List<OperatorDB> myCollection  = dbConn.Table<OperatorDB>().Where(k=>k.flag==opFlag).ToList<OperatorDB>();
              
              
                ObservableCollection<OperatorDB> OperatorDBList = new ObservableCollection<OperatorDB>(myCollection);
                return OperatorDBList;
                }
            }


        public OperatorDB ReadOperatorDB(String opFlag,string op_code)
            {

            using(var dbConn = new SQLiteConnection(App.PAY1_DB_PATH))
                {
                List<OperatorDB> myCollection  = dbConn.Table<OperatorDB>().Where(k => k.flag==opFlag).Where(j => j.code==op_code).ToList<OperatorDB>();


                OperatorDB OperatorDBList = myCollection.FirstOrDefault();
                return OperatorDBList;
                }
            }

       
        public void UpdateContact(OperatorDB contact)
            {
               using(var dbConn = new SQLiteConnection(App.DB_PATH))
                   {
                   var existingconact = dbConn.Query<OperatorDB>("select * from OperatorDB where Id =" + contact.id).FirstOrDefault();
                   if(existingconact != null)
                       {
                       //existingconact.Name = contact.Name;
                       //existingconact.PhoneNumber = contact.PhoneNumber;
                       //existingconact.CreationDate = contact.CreationDate;
                       dbConn.RunInTransaction(() =>
                       {
                           dbConn.Update(existingconact);
                       });
                       }
                   }
            }
       
        public void Insert(OperatorDB newcontact)
            {
            using(var dbConn = new SQLiteConnection(App.PAY1_DB_PATH))
                {
                dbConn.RunInTransaction(() =>
                {
                    dbConn.Insert(newcontact);
                });
                }
            }

        
        public void DeleteMisscallById(String Id)
            {
            using(var dbConn = new SQLiteConnection(App.PAY1_DB_PATH))
                {
                var existingconact = dbConn.Query<OperatorDB>("select * from OperatorDB where id =" + Id).FirstOrDefault();
                if(existingconact != null)
                    {
                    dbConn.RunInTransaction(() =>
                    {
                        dbConn.Delete(existingconact);
                    });
                    }
                }
            }
       
        public void DeleteAllMissCall()
            {
            using(var dbConn = new SQLiteConnection(App.PAY1_DB_PATH))
                {
               
                dbConn.DropTable<OperatorDB>();
                dbConn.CreateTable<OperatorDB>();
                dbConn.Dispose();
                dbConn.Close();
               
                }
            }
        }

    }
