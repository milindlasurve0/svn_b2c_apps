//
//  GiftCustomCell.h
//  Pay1
//
//  Created by Annapurna on 16/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#import "UIImageView+WebCache.h"
#import "UIImage+Utility.h"

@class GiftResponse;
@class GiftList;
@class GiftDetailList;
@class GiftCategoryList;
@interface GiftCustomCell : UITableViewCell<UIScrollViewDelegate>{
    
    UIImageView *giftImgView;
    UILabel *offerTitleLabel;
    UILabel *productLabel;
    UILabel *shopName;
    UILabel *addressLabel;
    UILabel *pointLabel;
    IBOutlet UILabel *titleLabel;
    
    IBOutlet UIScrollView *giftCellScrollView;
    
    UIButton *pointBtn;
    UIButton *addressBtn;
    
    
  
    
    IBOutlet UIView *seeAllButton;
    
    NSArray *offerIDArray;
    
    NSMutableArray *offerIDArrayList;
    
    NSMutableArray *imageURLArray;
    NSMutableArray *shopNameArray;
    NSMutableArray *areaArray;
    NSMutableArray *offerArray;
    NSMutableArray *priceArray;
    NSMutableArray *offerPriceArray;
    NSMutableArray *offerVoucherArray;

    
    NSMutableArray *dealArray;
    NSMutableArray *offerIDList;
    
    IBOutlet UIView *sepView;

    NSString *offserValue ;
}

@property(nonatomic, copy) void (^onImageViewClick)(NSString * dealID,NSString *offerID,NSString *shopName);
@property(nonatomic, copy) void (^onSeeAllClick)(NSString *offerTitle,NSString *offerID,NSString *offerVoucher,NSInteger seeAllGiftID);

//-(void)loadGiftDataOnTableView:(GiftList *)giftList objGiftDetailList:(GiftDetailList *)giftDetailList;

-(void)loadGiftDataOnTableView:(GiftList *)giftList rowIndex:(NSInteger)rowIndex;

//-(void)loadGiftDataOnTableView:(GiftList *)giftList indexRow:(NSInteger)indexRow;


@end
