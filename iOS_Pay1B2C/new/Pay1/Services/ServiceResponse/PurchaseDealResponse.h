//
//  PurchaseDealResponse.h
//  Pay1
//
//  Created by Annapurna on 23/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PurchaseDealResponse : NSObject

@property (nonatomic, strong) NSString *messageDesc;
@property (nonatomic, strong) NSString *errorCode;
@property (nonatomic, strong) NSString *status;

@property (nonatomic, strong) NSDictionary *purchaseInfoDic;

@property (nonatomic, strong) NSString *closingBalance;
@property (nonatomic, strong) NSString *dealCouponCode;
@property (nonatomic, strong) NSString *expiry;
@property (nonatomic, strong) NSString *loyaltyBalance;
@property (nonatomic, strong) NSString *loyaltyPoints;

@property (nonatomic, strong) NSString *transactionID;

@property (nonatomic, strong) NSString *formContent;
@property (nonatomic, strong) NSString *onlineForm;


@property (nonatomic, strong) NSMutableArray *dealArray;
@property (nonatomic, strong) NSString *pinCode;


@end
