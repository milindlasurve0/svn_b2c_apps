//
//  WalletHistoryService.m
//  Pay1
//
//  Created by webninjaz on 19/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "WalletHistoryService.h"
#import "WalletHistoryCriteria.h"
#import "WalletHistoryResponse.h"
#import "TransactionList.h"
@implementation WalletHistoryService

-(void)callWalletHistoryService:(WalletHistoryCriteria *)criteria callback:(walletHistoryResponse)callback{
   NSMutableDictionary *postData = [[NSMutableDictionary alloc]init];
   [postData setObject:criteria.walletHistoryParamCriteria forKey:@"Param"];
   [postData setObject:criteria.walletHistoryPageCriteria forKey:@"page"];
   [postData setObject:criteria.walletHistoryLimitCriteria forKey:@"limit"];
    
    
    NSLog(@"PostData for Update Password :%@",postData);
    
    RequestData *requestData = [[RequestData alloc]init];
    requestData.baseURL = kBaseURL;
    requestData.webServiceURL = kWalletRequestURL;
    requestData.postData = postData;
    requestData.showLoader = FALSE;
    
    requestData.requestSuccessBlock = ^(RequestData *request) {
    [self printResponse:request.responseData forRequest:@"Update Wallet History Response"];
    if ([self handleNullResponse:request])
    return;
        
    callback(nil, [self parseWalletHistoryResponse:request]);
    };
    
    requestData.requestFailureBlock = ^(RequestData *request) {
        [self handleNetworkError:request.responseData];
        callback(request.responseData, nil);
    };
    
    [[ServiceManager sharedInstance] makeRequest:requestData];
}

-(WalletHistoryResponse *)parseWalletHistoryResponse:(RequestData *)request{
    WalletHistoryResponse *objWalletResponse = [[WalletHistoryResponse alloc]init];
    objWalletResponse.walletStatus = [request.responseData objectForKey:@"status"];
    objWalletResponse.errorCode = [request.responseData objectForKey:@"errCode"];
    NSMutableArray *walletArray = [request.responseData valueForKey:@"description"];
    
    NSLog(@"The discription is %@",walletArray);
    
    for(NSMutableDictionary *dic in walletArray){
    TransactionList *transactionList = [[TransactionList alloc]init];
    transactionList.transactionBaseAmount = [dic objectForKey:@"base_amount"];
    transactionList.transactionClosingBalance = [dic objectForKey:@"closing_bal"];
    transactionList.transactionDealName = [dic objectForKey:@"deal_name"];
    transactionList.transactionAmount = [dic objectForKey:@"transaction_amount"];
    transactionList.transactionIPAddress  = [dic objectForKey:@"ip_address"];
    transactionList.transactionMihPayID = [dic objectForKey:@"mihpayid"];
    transactionList.transactionPhoneNumber = [dic objectForKey:@"number"];
    transactionList.transactionOfferName = [dic objectForKey:@"offer_name"];
    transactionList.transactionOperatorName = [dic objectForKey:@"operator_name"];
    transactionList.transactionPaymentMode = [dic objectForKey:@"payment_mode"];
    transactionList.transactionProductID = [dic objectForKey:@"product_id"];
    transactionList.transactionRechargeFlag = [dic objectForKey:@"recharge_flag"];
    transactionList.transactionResponseID = [dic objectForKey:@"response_id"];
        
    transactionList.transactionServiceID = [dic objectForKey:@"service_id"];
    transactionList.transactionStatus = [dic objectForKey:@"status"];
    transactionList.transactionTransCategory = [dic objectForKey:@"trans_category"];
    transactionList.transactionDateTime = [dic objectForKey:@"trans_datetime"];
    transactionList.transactionTransType = [dic objectForKey:@"trans_type"];
    transactionList.transactionAmount = [dic objectForKey:@"transaction_amount"];
    transactionList.transactionID = [dic objectForKey:@"transaction_id"];
    transactionList.transactionMode = [dic objectForKey:@"transaction_mode"];

    [objWalletResponse.walletDataArray addObject:transactionList];
    }
    return objWalletResponse;
}


@end
