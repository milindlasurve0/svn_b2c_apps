package com.pay1.databasehandler;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

public class OperatorDataSource {

	// Database fields
	private SQLiteDatabase database;
	private Pay1SQLiteHelper dbHelper;

	public OperatorDataSource(Context context) {
		dbHelper = new Pay1SQLiteHelper(context);
	}

	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
		System.out.println("Released " + SQLiteDatabase.releaseMemory());
	}

	public void createOperator(JSONArray response, int type, long timestamp) {
		try {
			open();
			String operator = "INSERT INTO " + Pay1SQLiteHelper.TABLE_OPERATOR
					+ " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";

			SQLiteStatement insert_operator = database
					.compileStatement(operator);
			database.beginTransaction();

			for (int i = 0; i < response.length(); i++) {
				try {
					JSONObject object = response.getJSONObject(i);

					insert_operator.bindLong(2,
							Integer.parseInt(object.getString("id").trim()));
					insert_operator.bindString(3, object.getString("opr_code")
							.trim());
					insert_operator.bindLong(4, Integer.parseInt(object
							.getString("product_id").trim()));
					insert_operator.bindString(5, object.getString("name")
							.trim());
					insert_operator.bindLong(6, Integer.parseInt(object
							.getString("service_id").trim()));
					insert_operator.bindLong(7,
							Integer.parseInt(object.getString("flag").trim()));
					insert_operator.bindLong(8, Integer.parseInt(object
							.getString("delegate").trim()));
					insert_operator.bindLong(9,
							Integer.parseInt(object.getString("stv").trim()));
					insert_operator.bindLong(10, type);
					insert_operator.bindLong(11, timestamp);
					insert_operator.bindLong(12,
							Integer.parseInt(object.getString("max")));
					insert_operator.bindLong(13,
							Integer.parseInt(object.getString("min")));
					insert_operator.bindString(14,
							object.getString("charges_slab"));
					insert_operator.bindLong(15, Integer.parseInt(object
							.getString("service_charge_amount")));
					insert_operator.bindLong(16, Integer.parseInt(object
							.getString("service_charge_percent")));
					insert_operator.bindLong(17, Integer.parseInt(object
							.getString("service_charge_percent")));
					insert_operator.bindLong(18,
							Integer.parseInt(object.getString("length")));
					insert_operator.bindString(19, object.getString("prefix")
							.toString());

					insert_operator.execute();
					insert_operator.clearBindings();
				} catch (Exception e) {
				}
			}
		} catch (SQLException exception) {
			// Log.d("Er ", "row inserted   " + exception.getMessage());
			// TODO: handle exception
			// // // Log.e("Er ", exception.getMessage());
		} catch (Exception e) {
			// Log.d("Er ", "row inserted   " + e.getMessage());
			// TODO: handle exception
			// // // Log.e("Er ", e.getMessage());
		} finally {
			database.setTransactionSuccessful();
			database.endTransaction();
			close();
		}
	}

	public void createOperator(int operatorID, String operatorCode,
			int opratorProductID, String operatorName, int operatorServiceID,
			int opratorFlag, int operatorDelegate, int operatorSTV,
			int operatorType, long operatorUptateTime, int operatorMaxAmount,
			int operatorMinAmount, int operatorServiceChargeAmount,
			int operatorServiceChargePercent, int operatorServiceTaxPercent,
			String operatorChargeSlab, int operatorLength, String operatorPrefix) {

		ContentValues values = new ContentValues();
		values.put(Pay1SQLiteHelper.OPERATOR_ID, operatorID);
		values.put(Pay1SQLiteHelper.OPERATOR_CODE, operatorCode);
		values.put(Pay1SQLiteHelper.OPERATOR_PRODUCT_ID, opratorProductID);
		values.put(Pay1SQLiteHelper.OPERATOR_NAME, operatorName);
		values.put(Pay1SQLiteHelper.OPERATOR_SERVICE_ID, operatorServiceID);
		values.put(Pay1SQLiteHelper.OPERATOR_FLAG, opratorFlag);
		values.put(Pay1SQLiteHelper.OPERATOR_DELEGATE, operatorDelegate);
		values.put(Pay1SQLiteHelper.OPERATOR_STV, operatorSTV);
		values.put(Pay1SQLiteHelper.OPERATOR_TYPE, operatorType);
		values.put(Pay1SQLiteHelper.OPERATOR_TIME, operatorUptateTime);

		values.put(Pay1SQLiteHelper.OPERATOR_MAX, operatorMaxAmount);
		values.put(Pay1SQLiteHelper.OPERATOR_MIN, operatorMinAmount);
		values.put(Pay1SQLiteHelper.OPERATOR_SERVICE_CHARGE_AMOUNT,
				operatorServiceChargeAmount);
		values.put(Pay1SQLiteHelper.OPERATOR_CHARGES_SLAB, operatorChargeSlab);
		values.put(Pay1SQLiteHelper.OPERATOR_SERVICE_CHARGE_PERCENT,
				operatorServiceChargePercent);
		values.put(Pay1SQLiteHelper.OPERATOR_SERVICE_TAX_PERCENT,
				operatorServiceTaxPercent);
		values.put(Pay1SQLiteHelper.OPERATOR_LENGTH, operatorLength);
		values.put(Pay1SQLiteHelper.OPERATOR_PREFIX, operatorPrefix);

		database.insert(Pay1SQLiteHelper.TABLE_OPERATOR, null, values);
		// Cursor cursor = database.query(Pay1SQLiteHelper.TABLE_Operator,
		// null, Pay1SQLiteHelper.Operator_ID + " = " + insertId, null,
		// null, null, null);
		// cursor.moveToFirst();
		// Operator neOperator = cursorToOperator(cursor);
		// cursor.close();
		// return newOperator;
	}

	public void deleteOperator() {
		database.delete(Pay1SQLiteHelper.TABLE_OPERATOR, null, null);
	}

	public List<Operator> getAllOperator() {
		List<Operator> operators = new ArrayList<Operator>();

		Cursor cursor = database.query(Pay1SQLiteHelper.TABLE_OPERATOR, null,
				null, null, null, null, Pay1SQLiteHelper.OPERATOR_NAME);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Operator Operator = cursorToOperator(cursor);
			operators.add(Operator);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return operators;
	}

	public List<Operator> getAllOperator(int operatorType) {
		List<Operator> operators = new ArrayList<Operator>();

		Cursor cursor = database.query(Pay1SQLiteHelper.TABLE_OPERATOR, null,
				Pay1SQLiteHelper.OPERATOR_TYPE + " = " + operatorType, null,
				null, null, Pay1SQLiteHelper.OPERATOR_NAME);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Operator operator = cursorToOperator(cursor);
			operators.add(operator);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return operators;
	}

	public Operator getOperator(int operatorType, String operatorCode) {

		Cursor cursor = database.query(Pay1SQLiteHelper.TABLE_OPERATOR, null,
				Pay1SQLiteHelper.OPERATOR_TYPE + " = " + operatorType + " AND "
						+ Pay1SQLiteHelper.OPERATOR_CODE + " = '"
						+ operatorCode + "'", null, null, null, null);

		if (cursor.getCount() > 0) {
			cursor.moveToFirst();
			Operator operator = cursorToOperator(cursor);
			cursor.moveToNext();
			// make sure to close the cursor
			cursor.close();
			return operator;
		} else {
			return null;
		}
	}

	public Operator getTopOperator() {

		Cursor cursor = database.query(Pay1SQLiteHelper.TABLE_OPERATOR, null,
				null, null, null, null, Pay1SQLiteHelper.OPERATOR_TIME
						+ " desc limit 1");

		if (cursor.getCount() > 0) {
			cursor.moveToFirst();
			Operator operator = cursorToOperator(cursor);
			cursor.moveToNext();
			// make sure to close the cursor
			cursor.close();
			return operator;
		} else {
			return null;
		}
	}

	private Operator cursorToOperator(Cursor cursor) {
		Operator Operator = new Operator();
		Operator.setoID(cursor.getInt(0));
		Operator.setOperatorID(cursor.getInt(1));
		Operator.setOperatorCode(cursor.getString(2));
		Operator.setOperatorProductID(cursor.getInt(3));
		Operator.setOperatorName(cursor.getString(4));
		Operator.setOperatorServiceID(cursor.getInt(5));
		Operator.setOperatorFlag(cursor.getInt(6));
		Operator.setOperatorDelegate(cursor.getInt(7));
		Operator.setOperatorSTV(cursor.getInt(8));
		Operator.setOperatorType(cursor.getInt(9));
		Operator.setOperatorUptateTime(cursor.getLong(10));
		Operator.setOperatorMaxAmount(cursor.getInt(11));
		Operator.setOperatorMinAmount(cursor.getInt(12));
		Operator.setOperatorChargeSlab(cursor.getString(13));
		Operator.setOperatorServiceChargeAmount(cursor.getInt(14));
		Operator.setOperatorServiceTaxPercent(cursor.getInt(15));
		Operator.setOperatorServiceChargePercent(cursor.getInt(16));
		Operator.setOperatorLength(cursor.getInt(17));
		Operator.setOperatorPrefix(cursor.getString(18));
		return Operator;
	}
}
