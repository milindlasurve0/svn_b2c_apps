package com.pay1;

import java.net.URLEncoder;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.google.android.gcm.GCMRegistrar;
import com.pay1.customviews.MyProgressDialog;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.pay1.constants.Constants;
import com.pay1.requesthandler.RequestClass;
import com.pay1.utilities.ArrayHelper;
import com.pay1.utilities.EditTextValidator;
import com.pay1.utilities.MyLocation;
import com.pay1.utilities.MyLocation.LocationResult;
import com.pay1.utilities.Utility;

public class LoginConfirmActivity extends Activity {

	private static final String SCREEN_LABEL = "Login Confirm Screen";
	private EasyTracker easyTracker = null;

	private TextView textView_Title;
	private EditText editText_OTP, editText_NewPin, editText_ConfirmPin;
	private Button button_Done;

	private final String TAG = "Login";
	String str = "";
	private Double longitude, latitude;
	private String uuid, version, manufacturer;

	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_confirm_activity);
		try {
			easyTracker = EasyTracker.getInstance(LoginConfirmActivity.this);
			easyTracker.set(Fields.SCREEN_NAME, SCREEN_LABEL);
			Typeface Normal = Typeface.createFromAsset(getAssets(),
					"EncodeSansNormal-100-Thin.ttf");

			textView_Title = (TextView) findViewById(R.id.textView_Title);
			textView_Title.setTypeface(Normal);

			editText_OTP = (EditText) findViewById(R.id.editText_OTP);
			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(editText_OTP.getWindowToken(), 0);
			editText_OTP.setTypeface(Normal);
			editText_OTP.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					// TODO Auto-generated method stub
					EditTextValidator
							.hasTextLogin(LoginConfirmActivity.this,
									editText_OTP,
									Constants.ERROR_LOGIN_PIN_BLANK_FIELD);
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					// TODO Auto-generated method stub

				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

				}
			});
			editText_NewPin = (EditText) findViewById(R.id.editText_NewPin);
			editText_NewPin.setTypeface(Normal);
			editText_NewPin.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					// TODO Auto-generated method stub
					EditTextValidator.hasTextLogin(LoginConfirmActivity.this,
							editText_NewPin,
							Constants.ERROR_LOGIN_PIN_BLANK_FIELD);
					if (editText_OTP.length() == 10) {
						editText_ConfirmPin.requestFocus();
					}
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					// TODO Auto-generated method stub

				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

				}
			});

			editText_ConfirmPin = (EditText) findViewById(R.id.editText_ConfirmPin);
			editText_ConfirmPin.setTypeface(Normal);
			editText_ConfirmPin.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					// TODO Auto-generated method stub
					EditTextValidator.hasTextLogin(LoginConfirmActivity.this,
							editText_ConfirmPin,
							Constants.ERROR_LOGIN_PIN_BLANK_FIELD);

				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					// TODO Auto-generated method stub

				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

				}
			});
			editText_ConfirmPin
					.setOnEditorActionListener(new TextView.OnEditorActionListener() {

						@Override
						public boolean onEditorAction(TextView v, int actionId,
								KeyEvent event) {
							// TODO Auto-generated method stub
							if (actionId == EditorInfo.IME_ACTION_GO) {
								if (checkValidation(LoginConfirmActivity.this)) {
									InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
									imm.hideSoftInputFromWindow(
											v.getWindowToken(), 0);

									new LoginConfirmTask().execute(
											Utility.getMobileNumber(
													LoginConfirmActivity.this,
													Constants.SHAREDPREFERENCE_MOBILE),
											editText_NewPin.getText()
													.toString().trim(),
											editText_ConfirmPin.getText()
													.toString().trim(),
											editText_OTP.getText().toString()
													.trim());
								}
								return true;
							}
							return false;
						}
					});

			button_Done = (Button) findViewById(R.id.button_Done);
			button_Done.setTypeface(Normal);
			button_Done.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (checkValidation(LoginConfirmActivity.this)) {
						InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
						imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

						new LoginConfirmTask().execute(
								Utility.getMobileNumber(
										LoginConfirmActivity.this,
										Constants.SHAREDPREFERENCE_MOBILE),
								editText_NewPin.getText().toString().trim(),
								editText_ConfirmPin.getText().toString().trim(),
								editText_OTP.getText().toString().trim());
					}
				}
			});

			try {
				TelephonyManager tManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
				uuid = tManager.getDeviceId();
				if (uuid == null) {
					uuid = Secure.getString(
							LoginConfirmActivity.this.getContentResolver(),
							Secure.ANDROID_ID);
				}
				if (Utility.getUUID(LoginConfirmActivity.this,
						Constants.SHAREDPREFERENCE_UUID) == null)
					Utility.setUUID(LoginConfirmActivity.this,
							Constants.SHAREDPREFERENCE_UUID, uuid);
			} catch (Exception exception) {
			}
			version = android.os.Build.VERSION.RELEASE;
			manufacturer = android.os.Build.MANUFACTURER;

			GCMRegistrar.checkDevice(this);
			GCMRegistrar.checkManifest(this);
			checkNotNull(Constants.SENDER_ID, "SENDER_ID");

			final String regId = GCMRegistrar.getRegistrationId(this);
			// // Log.i(TAG, "registration id =====  " + regId);
			Utility.setGCMID(LoginConfirmActivity.this,
					Constants.SHAREDPREFERENCE_GCMID, regId);
			if (regId.equals("")) {
				GCMRegistrar.register(this, Constants.SENDER_ID);
			} else {
				// // Log.v(TAG, "Already registered");
			}

			try {
				LocationResult locationResult = new LocationResult() {
					@Override
					public void gotLocation(Location location) {
						// Got the location!
						try {
							longitude = location.getLongitude();
							latitude = location.getLatitude();

							Utility.setCurrentLatitude(
									LoginConfirmActivity.this,
									Constants.SHAREDPREFERENCE_CURRENT_LATITUDE,
									String.valueOf(location.getLatitude()));
							Utility.setCurrentLongitude(
									LoginConfirmActivity.this,
									Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE,
									String.valueOf(location.getLongitude()));

						} catch (Exception exception) {
						}
					}
				};
				MyLocation myLocation = new MyLocation();
				myLocation.getLocation(this, locationResult);
			} catch (Exception exception) {
			}
		} catch (Exception e) {
		}
	}

	@Override
	protected void onNewIntent(Intent intent) {
		// TODO Auto-generated method stub
		super.onNewIntent(intent);

		Bundle bundle = intent.getExtras();
		if (bundle != null)
			str = bundle.getString("OTP");

		editText_OTP.setText(str);
	}

	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView().build());
		// EasyTracker.getInstance(this).activityStart(this);
		easyTracker.send(MapBuilder.createAppView().build());
	}

	@Override
	public void onStop() {
		super.onStop();
		// EasyTracker.getInstance(this).activityStop(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (getIntent() != null && getIntent().getExtras() != null) {
			Bundle bundle = getIntent().getExtras();
			if (bundle != null)
				str = bundle.getString("OTP");
		}
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
	}

	private void checkNotNull(Object reference, String name) {
		if (reference == null) {
			throw new NullPointerException("Errror");
		}
	}

	private boolean checkValidation(Context context) {
		boolean ret = true;

		if (!EditTextValidator.hasTextLogin(context, editText_OTP,
				Constants.ERROR_OTP_BLANK_FIELD)) {
			ret = false;
			editText_OTP.requestFocus();
			return ret;
		} else if (!EditTextValidator.hasTextLogin(context, editText_NewPin,
				Constants.ERROR_LOGIN_PIN_BLANK_FIELD)) {
			ret = false;
			editText_NewPin.requestFocus();
			return ret;
		} else if (!EditTextValidator.isValidPinLogin(context, editText_NewPin,
				Constants.ERROR_LOGIN_PIN_VALID_FIELD)) {
			ret = false;
			editText_NewPin.requestFocus();
			return ret;
		} else if (!EditTextValidator.hasTextLogin(context,
				editText_ConfirmPin, Constants.ERROR_LOGIN_PIN_BLANK_FIELD)) {
			ret = false;
			editText_ConfirmPin.requestFocus();
			return ret;
		} else if (!EditTextValidator.isValidPinLogin(context,
				editText_ConfirmPin, Constants.ERROR_LOGIN_PIN_VALID_FIELD)) {
			ret = false;
			editText_ConfirmPin.requestFocus();
			return ret;
		} else
			return ret;
	}

	public class LoginConfirmTask extends AsyncTask<String, String, String> {
		// private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			try {
				String response = RequestClass
						.getInstance()
						.readPay1B2CRequest(
								LoginConfirmActivity.this,
								Constants.B2C_URL
										+ "update_password/?mobile_number="
										+ URLEncoder.encode(params[0], "utf-8")
										+ "&current_password=&password="
										+ URLEncoder.encode(params[1], "utf-8")
										+ "&confirm_password="
										+ URLEncoder.encode(params[2], "utf-8")
										+ "&code="
										+ URLEncoder.encode(params[3], "utf-8"));

				return response;

			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			// if (this.dialog.isShowing()) {
			// this.dialog.dismiss();
			// }
			try {

				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONObject jsonObject = new JSONObject(replaced);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {

						// Constants.showOneButtonDialog(
						// LoginConfirmActivity.this, TAG,
						// "User created successfully.",
						// Constants.DIALOG_CLOSE_CHANGE_PIN);

						new LoginTask().execute(Utility.getMobileNumber(
								LoginConfirmActivity.this,
								Constants.SHAREDPREFERENCE_MOBILE),
								editText_NewPin.getText().toString().trim(),
								Utility.getUUID(LoginConfirmActivity.this,
										Constants.SHAREDPREFERENCE_UUID),
								String.valueOf(latitude), String
										.valueOf(longitude), manufacturer,
								version, Utility.getGCMID(
										LoginConfirmActivity.this,
										Constants.SHAREDPREFERENCE_GCMID));

					} else {
						Constants.showOneButtonDialog(
								LoginConfirmActivity.this, TAG,
								Constants.checkCode(replaced),
								Constants.DIALOG_CLOSE);
					}

				} else {
					// Constants.showOneButtonDialog(LoginConfirmActivity.this,
					// TAG,
					// Constants.ERROR_INTERNET, Constants.DIALOG_CLOSE);
					Intent intent = new Intent(LoginConfirmActivity.this,
							ConnectivityErrorActivity.class);
					intent.putExtra(Constants.RECHARGE_FOR,
							Constants.WALLET_PAYMENT);
					startActivity(intent);

				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonDialog(LoginConfirmActivity.this, TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonDialog(LoginConfirmActivity.this, TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			}
		}

		@Override
		protected void onPreExecute() {
			// dialog = new MyProgressDialog(LoginConfirmActivity.this);
			// // this.dialog.setMessage("Please wait.....");
			// this.dialog.setCancelable(false);
			// this.dialog
			// .setOnCancelListener(new DialogInterface.OnCancelListener() {
			//
			// @Override
			// public void onCancel(DialogInterface dialog) {
			// // TODO Auto-generated method stub
			// LoginConfirmTask.this.cancel(true);
			// }
			// });
			// this.dialog.show();
			super.onPreExecute();
		}

		// @Override
		// public void onDismiss(DialogInterface dialog) {
		// // TODO Auto-generated method stub
		// LoginConfirmTask.this.cancel(true);
		// dialog.cancel();
		// }

	}

	public class LoginTask extends AsyncTask<String, String, String> implements
			OnDismissListener {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			try {
				try {
					long LastUpdateTime = Utility.getLongSharedPreferences(
							LoginConfirmActivity.this,
							Constants.CONTACT_UPDATE_TIME);

					int days = (int) ((System.currentTimeMillis() - LastUpdateTime) / (1000 * 60 * 60 * 24));

					if (days >= 2) {
						ArrayHelper arrayHelper = new ArrayHelper(
								LoginConfirmActivity.this);
						arrayHelper.clearArray(Constants.CONATACT_JSON);
						arrayHelper
								.saveArray(
										Constants.CONATACT_JSON,
										Constants
												.getAllContacts(LoginConfirmActivity.this));
					}
				} catch (Exception e) {
				}
				String response = RequestClass.getInstance()
						.readPay1B2CRequest(
								LoginConfirmActivity.this,
								Constants.B2C_URL + "signin/?username="
										+ URLEncoder.encode(params[0], "utf-8")
										+ "&password="
										+ URLEncoder.encode(params[1], "utf-8")
										+ "&uuid=" + params[2] + "&latitude="
										+ params[3] + "&longitude=" + params[4]
										+ "&manufacturer=" + params[5]
										+ "&os_info=" + params[6]
										+ "&gcm_reg_id=" + params[7]
										+ "&device_type=android");

				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			try {

				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONObject jsonObject = new JSONObject(replaced);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {
						Utility.setLoginFlag(LoginConfirmActivity.this,
								Constants.SHAREDPREFERENCE_IS_LOGIN, true);
						JSONObject jsonObject2 = jsonObject
								.getJSONObject("description");

						Utility.setUserName(LoginConfirmActivity.this,
								Constants.SHAREDPREFERENCE_NAME, jsonObject2
										.getString("name").trim());
						Utility.setMobileNumber(LoginConfirmActivity.this,
								Constants.SHAREDPREFERENCE_MOBILE, jsonObject2
										.getString("mobile").trim());
						Utility.setEmail(LoginConfirmActivity.this,
								Constants.SHAREDPREFERENCE_EMAIL, jsonObject2
										.getString("email").trim());
						Utility.setGender(LoginConfirmActivity.this,
								Constants.SHAREDPREFERENCE_GENDER, jsonObject2
										.getString("gender").trim());
						Utility.setUserID(LoginConfirmActivity.this,
								Constants.SHAREDPREFERENCE_USER_ID, jsonObject2
										.getString("user_id").trim());
						Utility.setBalance(LoginConfirmActivity.this,
								Constants.SHAREDPREFERENCE_BALANCE, jsonObject2
										.getString("wallet_balance").trim());
						Utility.setPin(LoginConfirmActivity.this,
								Constants.SHAREDPREFERENCE_PIN, editText_NewPin
										.getText().toString().trim());
						Utility.setDOB(LoginConfirmActivity.this,
								Constants.SHAREDPREFERENCE_DOB, jsonObject2
										.getString("date_of_birth").trim());
						Utility.setLatitude(LoginConfirmActivity.this,
								Constants.SHAREDPREFERENCE_LATITUDE,
								jsonObject2.getString("latitude").trim());
						Utility.setLongitude(LoginConfirmActivity.this,
								Constants.SHAREDPREFERENCE_LONGITUDE,
								jsonObject2.getString("longitude").trim());
						Utility.setSupportNumber(LoginConfirmActivity.this,
								Constants.SHAREDPREFERENCE_SUPPORT_NUMBER,
								jsonObject2.getString("support_number").trim());

						Utility.setSharedPreference(
								LoginConfirmActivity.this,
								Constants.SHAREDPREFERENCE_SERVICE_CHARGE_PERCENT,
								jsonObject2.getString("service_charge_percent")
										.trim());

						Utility.setSharedPreference(LoginConfirmActivity.this,
								Constants.SHAREDPREFERENCE_SERVICE_TAX_PERCENT,
								jsonObject2.getString("service_tax_percent")
										.trim());
						// if (Utility.isFirstTime(LoginConfirmActivity.this)) {
						// Intent i = new Intent(LoginConfirmActivity.this,
						// TourActivity.class);
						//
						// startActivity(i);
						// } else {

						// }

					} else {
						// Constants.showOneButtonDialog(
						// LoginConfirmActivity.this, TAG,
						// Constants.checkCode(replaced),
						// Constants.DIALOG_CLOSE);
					}

				} else {
					// Constants.showOneButtonDialog(LoginConfirmActivity.this,
					// TAG,
					// Constants.ERROR_INTERNET, Constants.DIALOG_CLOSE);
					// Intent intent = new Intent(LoginConfirmActivity.this,
					// ConnectivityErrorActivity.class);
					// intent.putExtra(Constants.RECHARGE_FOR,
					// Constants.WALLET_PAYMENT);
					// startActivity(intent);

				}
			} catch (JSONException e) {
				// e.printStackTrace();
				// Constants.showOneButtonDialog(LoginConfirmActivity.this, TAG,
				// Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
				// Constants.DIALOG_CLOSE);
			} catch (Exception e) {
				// TODO: handle exception
				// Constants.showOneButtonDialog(LoginConfirmActivity.this, TAG,
				// Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
				// Constants.DIALOG_CLOSE);
			}

			Intent i = new Intent(LoginConfirmActivity.this,
					RechargeMobileActivity.class);
			i.putExtra(Constants.RECHARGE_FOR, Constants.RECHARGE_MOBILE);
			startActivity(i);
			finish();
		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(LoginConfirmActivity.this);
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							LoginTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			LoginTask.this.cancel(true);
			dialog.cancel();
		}

	}
}
