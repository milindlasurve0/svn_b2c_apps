//
//  Pay1TransactionList.h
//  Pay1
//
//  Created by Annapurna on 09/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Pay1TransactionList : NSObject


@property (nonatomic, strong) NSString *transID;
@property (nonatomic, strong) NSString *flag;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *amount;

@property (nonatomic, strong) NSString *userID;
@property (nonatomic, strong) NSString *stv;
@property (nonatomic, strong) NSString *operatorID;
@property (nonatomic, strong) NSString *product;

@property (nonatomic, strong) NSString *transNumber;
@property (nonatomic, strong) NSString *delegateFlag;
@property (nonatomic, strong) NSString *missedNumber;
@property (nonatomic, strong) NSString *confirmation;

@property (nonatomic, strong) NSString *operatorName;
@property (nonatomic, strong) NSString *transactionFlag;
@property (nonatomic, strong) NSString *operatorCode;
@property (nonatomic, strong) NSString *dateTime;

@end
