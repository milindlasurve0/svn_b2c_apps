package com.pay1.adapterhandler;

import java.util.ArrayList;
import java.util.WeakHashMap;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.pay1.QuickPayActivity;
import com.pay1.R;

public class QuickPayAdapter extends BaseAdapter {

	private Context context;
	private ArrayList<WeakHashMap<String, String>> data;

	public QuickPayAdapter(Context context,
			ArrayList<WeakHashMap<String, String>> data) {
		this.context = context;
		this.data = data;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder viewHolder;
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.quickpay_adapter, null);
			viewHolder = new ViewHolder();

			Typeface Narrow = Typeface.createFromAsset(context.getAssets(),
					"EncodeSansNarrow-500-Medium.ttf");

			Typeface Narrow_thin = Typeface.createFromAsset(
					context.getAssets(), "EncodeSansNarrow-500-Medium.ttf");

			viewHolder.textView_Name = (TextView) convertView
					.findViewById(R.id.textView_Name);
			viewHolder.textView_Name.setTypeface(Narrow);
			viewHolder.textView_Mobile = (TextView) convertView
					.findViewById(R.id.textView_Mobile);
			viewHolder.textView_Mobile.setTypeface(Narrow_thin);
			viewHolder.textView_Amount = (TextView) convertView
					.findViewById(R.id.textView_Amount);
			viewHolder.textView_Amount.setTypeface(Narrow_thin);
			convertView.setTag(viewHolder);

		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		WeakHashMap<String, String> map = new WeakHashMap<String, String>();
		map = data.get(position);

		viewHolder.textView_Name.setText(map.get(QuickPayActivity.NAME)
				.toString().trim());
		viewHolder.textView_Mobile.setText(map.get(QuickPayActivity.NUMBER)
				.toString().trim());
		viewHolder.textView_Amount.setText(map.get(QuickPayActivity.AMOUNT)
				.toString().trim());

		return convertView;
	}

	private class ViewHolder {
		TextView textView_Name, textView_Mobile, textView_Amount;
	}
}
