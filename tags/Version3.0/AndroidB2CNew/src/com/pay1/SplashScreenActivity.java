package com.pay1;

import java.net.URLEncoder;

import org.json.JSONException;
import org.json.JSONObject;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.SQLException;
import android.database.sqlite.SQLiteException;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.larvalabs.svgandroid.SVGParser;
import com.pay1.RegistrationActivity.SyncTask;
import com.pay1.constants.Constants;
import com.pay1.databasehandler.FreeBiesDataSource;
import com.pay1.databasehandler.FreeBiesLocationDataSource;
import com.pay1.databasehandler.Pay1SyncDataHelper;
import com.pay1.requesthandler.RequestClass;
import com.pay1.utilities.Utility;

public class SplashScreenActivity extends Activity {

	// private ImageView imageView_Logo, imageView_Logo1;
	// private TextView textView_Slogen;
	private String code = "";
	private FreeBiesDataSource freeBiesDataSource;
	private FreeBiesLocationDataSource freeBiesLocationDataSource;
	public static final String AUTHORITY = "com.pay1.provider";
	public static final String ACCOUNT_TYPE = "b2c.pay1.in";
	public static final String ACCOUNT = "Pay1";
	public static final long SECONDS_PER_HOUR = 3600L;
	public static final long SYNC_INTERVAL_IN_HOURS = 4L;
	public static final long SYNC_INTERVAL = SYNC_INTERVAL_IN_HOURS
			* SECONDS_PER_HOUR;
	private final int SPLASH_DISPLAY_LENGTH = 4000;
	Account syncAccount; boolean isFinished=false;
	ContentResolver syncResolver;
	Pay1SyncDataHelper dbHelper;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		/*syncAccount = CreateSyncAccount(this);
		syncResolver = getContentResolver();
		*/
		
		syncAccount = CreateSyncAccount(this);
		syncResolver = getContentResolver();
		new SyncTask().execute();
		ContentResolver.setSyncAutomatically(syncAccount, AUTHORITY, true);
		ContentResolver.addPeriodicSync(syncAccount, AUTHORITY,
				Bundle.EMPTY, SYNC_INTERVAL);
		
	
		
		
		setContentView(R.layout.splashscreen_activity);

		try {
			dbHelper = new Pay1SyncDataHelper(SplashScreenActivity.this);
			dbHelper.getWritableDatabase();

		} catch (SQLiteException se) {
			se.printStackTrace();
		} catch (Exception se) {
			se.printStackTrace();
		} finally {
			dbHelper.close();
		}

		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				if(isFinished){
					
				}else{
				Intent intent = new Intent(SplashScreenActivity.this,
						MainActivity.class);
				isFinished=true;
				intent.putExtra("IS_FLAG",
						getIntent().getExtras().getBoolean("IS_FLAG"));
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
						| Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
				finish();
				}
			}
		}, SPLASH_DISPLAY_LENGTH);
		
		ImageView splash_image = (ImageView) findViewById(R.id.splash_image);
		splash_image.setImageDrawable(SVGParser.getSVGFromResource(
				SplashScreenActivity.this.getResources(), R.raw.pay1_new_logo)
				.createPictureDrawable());

		try {
			// try {
			// startService(new Intent(SplashScreenActivity.this,
			// LoginService.class));
			// } catch (Exception e) {
			// }
			freeBiesDataSource = new FreeBiesDataSource(
					SplashScreenActivity.this);
			freeBiesLocationDataSource = new FreeBiesLocationDataSource(
					SplashScreenActivity.this);
			//setFreeGift();
			//sync(syncAccount);
			Typeface Reguler = Typeface.createFromAsset(getAssets(),
					"Roboto-Regular.ttf");
			/*
			 * textView_Slogen = (TextView) findViewById(R.id.textView_Slogen);
			 * textView_Slogen.setTypeface(Reguler);
			 */
			/*
			 * imageView_Logo = (ImageView) findViewById(R.id.imageView_Logo);
			 * imageView_Logo.setVisibility(View.GONE);
			 */

			// imageView_Logo1 = (ImageView) findViewById(R.id.imageView_Logo1);
			// SVG svg = SVGParser.getSVGFromResource(getResources(),
			// R.raw.ic_pay1);
			// // Picture picture = svg.getPicture();
			// Drawable drawable = svg.createPictureDrawable();
			//
			// imageView_Logo1.setImageDrawable(drawable);
			// if (Build.VERSION.SDK_INT >= 11)
			// imageView_Logo1.setLayerType(View.LAYER_TYPE_SOFTWARE, null);

			// Intent intent = new Intent(SplashScreenActivity.this,
			// MainActivity.class);
			// startActivity(intent);
			// finish();
			/*
			 * if(Utility.isFirstTimeAfterUpdate(SplashScreenActivity.this)){
			 * Intent intent =new
			 * Intent(SplashScreenActivity.this,RegistrationActivity.class);
			 * startActivity(intent); }else{
			 */

			/*ContentResolver.setSyncAutomatically(syncAccount, AUTHORITY, true);
			ContentResolver.addPeriodicSync(syncAccount, AUTHORITY,
					Bundle.EMPTY, SYNC_INTERVAL);*/
			/*
			 * Utility.setCurrentLatitude(SplashScreenActivity.this,
			 * Constants.SHAREDPREFERENCE_CURRENT_LATITUDE,"20.123");
			 */
			if (Utility.getLoginFlag(SplashScreenActivity.this,
					Constants.SHAREDPREFERENCE_IS_LOGIN)) {

				// version = android.os.Build.VERSION.RELEASE;
				// manufacturer = android.os.Build.MANUFACTURER;

				new AutoLoginTask().execute(Utility.getMobileNumber(
						SplashScreenActivity.this,
						Constants.SHAREDPREFERENCE_MOBILE), Utility.getPin(
						SplashScreenActivity.this,
						Constants.SHAREDPREFERENCE_PIN), Utility.getGCMID(
						SplashScreenActivity.this,
						Constants.SHAREDPREFERENCE_GCMID), Utility.getUUID(
						SplashScreenActivity.this,
						Constants.SHAREDPREFERENCE_UUID), Utility
						.getCurrentLatitude(SplashScreenActivity.this,
								Constants.SHAREDPREFERENCE_CURRENT_LATITUDE),
						Utility.getCurrentLongitude(SplashScreenActivity.this,
								Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE));

			} else {
				Animation animation = AnimationUtils.loadAnimation(
						SplashScreenActivity.this, R.anim.translate);
				animation
						.setAnimationListener(new Animation.AnimationListener() {

							@Override
							public void onAnimationStart(Animation animation) {
								// TODO Auto-generated method stub

							}

							@Override
							public void onAnimationRepeat(Animation animation) {
								// TODO Auto-generated method stub

							}

							@Override
							public void onAnimationEnd(Animation animation) {
								// TODO Auto-generated method stub
								if(isFinished){
									
								}else{
								Intent intent = new Intent(SplashScreenActivity.this,
										MainActivity.class);
								isFinished=true;
								intent.putExtra("IS_FLAG",
										getIntent().getExtras().getBoolean("IS_FLAG"));
								intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
										| Intent.FLAG_ACTIVITY_NEW_TASK);
								startActivity(intent);
								finish();
								}
							}
						});

				// imageView_Logo.startAnimation(animation);
				// }
			}
		} catch (Exception e) {
		}
	}
	
	
	public class SyncTask extends AsyncTask<String, String, String>{

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			sync(syncAccount);
			return null;
		}
		
	}

	public static Account CreateSyncAccount(Context context) {
		Account newAccount = new Account(ACCOUNT, ACCOUNT_TYPE);
		try {
			AccountManager accountManager = (AccountManager) context
					.getSystemService(ACCOUNT_SERVICE);

			if (accountManager.addAccountExplicitly(newAccount, null, null)) {

			} else {
				/*
				 * The account exists or some other error occurred. Log this,
				 * report it, or handle it internally.
				 */
			}
		} catch (Exception ae) {
			Log.e("run", "run");
			ae.printStackTrace();
		}
		return newAccount;
	}

	public static void sync(Account sAccount) {
		Bundle settingsBundle = new Bundle();
		settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
		settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);

		ContentResolver.requestSync(sAccount, AUTHORITY, settingsBundle);
	}

	private void setFreeGift() {
		// TODO Auto-generated method stub
		/*try {
			freeBiesDataSource.open();

			FreeBies freebies = freeBiesDataSource.getTopFreeBie();

			if (freebies != null) {
				int days = (int) ((System.currentTimeMillis() - freebies
						.getFreebieUptateTime()) / (1000 * 60 * 60 * 24));
				if (days >= 10) {
					startService(new Intent(SplashScreenActivity.this,
							FreeBiesUpdateService.class));
				}
			} else {
				// startService(new Intent(MainActivity.this,
				// FreeBiesService.class));
				new FreeBiesTask().execute();
			}

		} catch (SQLException exception) {
			// TODO: handle exception
			// // // Log.e("Er ", exception.getMessage());
		} catch (Exception e) {
			// TODO: handle exception
			// // // Log.e("Er ", e.getMessage());
		} finally {
			freeBiesDataSource.close();
		}*/

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// super.onBackPressed();
	}

	public class AutoLoginTask extends AsyncTask<String, String, String> {
		// private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			try {

				String response = RequestClass.getInstance()
						.readPay1B2CRequest(
								SplashScreenActivity.this,
								Constants.B2C_URL + "signin/?username="
										+ URLEncoder.encode(params[0], "utf-8")
										+ "&password="
										+ URLEncoder.encode(params[1], "utf-8")
										+ "&gcm_reg_id="
										+ URLEncoder.encode(params[2], "utf-8")
										+ "&uuid="
										+ URLEncoder.encode(params[3], "utf-8")
										+ "&latitude=" + params[4]
										+ "&longitude=" + params[5]
										+ "&api_version="
										+ Constants.API_VERSION);
				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			// if (this.dialog.isShowing()) {
			// this.dialog.dismiss();
			// }
			try {

				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONObject jsonObject = new JSONObject(replaced);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {
						Utility.setLoginFlag(SplashScreenActivity.this,
								Constants.SHAREDPREFERENCE_IS_LOGIN, true);
						JSONObject jsonObject2 = jsonObject
								.getJSONObject("description");

						Utility.setUserName(SplashScreenActivity.this,
								Constants.SHAREDPREFERENCE_NAME, jsonObject2
										.getString("name").trim());
						Utility.setMobileNumber(SplashScreenActivity.this,
								Constants.SHAREDPREFERENCE_MOBILE, jsonObject2
										.getString("mobile").trim());
						Utility.setEmail(SplashScreenActivity.this,
								Constants.SHAREDPREFERENCE_EMAIL, jsonObject2
										.getString("email").trim());
						Utility.setGender(SplashScreenActivity.this,
								Constants.SHAREDPREFERENCE_GENDER, jsonObject2
										.getString("gender").trim());
						Utility.setUserID(SplashScreenActivity.this,
								Constants.SHAREDPREFERENCE_USER_ID, jsonObject2
										.getString("user_id").trim());
						Utility.setBalance(SplashScreenActivity.this,
								Constants.SHAREDPREFERENCE_BALANCE, jsonObject2
										.getString("wallet_balance").trim());
						Utility.setPin(SplashScreenActivity.this,
								Constants.SHAREDPREFERENCE_PIN, Utility.getPin(
										SplashScreenActivity.this,
										Constants.SHAREDPREFERENCE_PIN));
						Utility.setDOB(SplashScreenActivity.this,
								Constants.SHAREDPREFERENCE_DOB, jsonObject2
										.getString("date_of_birth").trim());
						Utility.setLatitude(SplashScreenActivity.this,
								Constants.SHAREDPREFERENCE_LATITUDE,
								jsonObject2.getString("latitude").trim());
						Utility.setLongitude(SplashScreenActivity.this,
								Constants.SHAREDPREFERENCE_LONGITUDE,
								jsonObject2.getString("longitude").trim());
						Utility.setSupportNumber(SplashScreenActivity.this,
								Constants.SHAREDPREFERENCE_SUPPORT_NUMBER,
								jsonObject2.getString("support_number").trim());
						Utility.setCookieName(SplashScreenActivity.this,
								Constants.SHAREDPREFERENCE_COOKIE_NAME,
								jsonObject2.getString("user_val_name"));
						Utility.setCookieValue(SplashScreenActivity.this,
								Constants.SHAREDPREFERENCE_COOKIE_VALUE,
								jsonObject2.getString("user_val"));

						Utility.setSharedPreferences(SplashScreenActivity.this,
								"Deals_offer",
								jsonObject2.getString("Deals_offer"));

						Utility.setMyTotalGifts(SplashScreenActivity.this,
								jsonObject2.getString("total_gifts"));

						Utility.setMyLikes(SplashScreenActivity.this,
								jsonObject2.getString("total_mylikes"));

						Utility.setMyTotalRedeem(SplashScreenActivity.this,
								jsonObject2.getString("total_redeem"));

						Utility.setMyReviews(SplashScreenActivity.this,
								jsonObject2.getString("total_myreviews"));
						JSONObject jsonObjectTemplate = jsonObject2
								.getJSONObject("ErrorMsg");
						Utility.setTemplate(
								SplashScreenActivity.this,
								Constants.SHAREDPREFERENCE_SMS_BILLPAYMENT_FAILURE,
								jsonObjectTemplate
										.getString("BILLPAYMENT_FAILURE"));
						Utility.setTemplate(
								SplashScreenActivity.this,
								Constants.SHAREDPREFERENCE_SMS_RECHARGE_FAILURE,
								jsonObjectTemplate
										.getString("RECHARGE_FAILURE"));
						Utility.setTemplate(
								SplashScreenActivity.this,
								Constants.SHAREDPREFERENCE_SMS_WALLET_REFILLED_RETAILER,
								jsonObjectTemplate
										.getString("WALLET_REFILLED_RETAILER"));

						Utility.setSharedPreferences(
								SplashScreenActivity.this,
								Constants.SHAREDPREFERENCE_SERVICE_CHARGE_PERCENT,
								jsonObject2.getString("service_charge_percent")
										.trim());

						Utility.setSharedPreferences(SplashScreenActivity.this,
								Constants.SHAREDPREFERENCE_SERVICE_TAX_PERCENT,
								jsonObject2.getString("service_tax_percent")
										.trim());

						if(jsonObject2.getString("profile_image").contains("images/")){
							Utility.setFbImageId(SplashScreenActivity.this,"http://b2c.pay1.in/"+
									jsonObject2.getString("profile_image"));
						}else{
							Utility.setFbImageId(SplashScreenActivity.this,
									jsonObject2.getString("profile_image"));
						}
						Utility.setProfileDataSource(SplashScreenActivity.this,
								jsonObject2.getString("fb_data"));
						/*
						 * if(!jsonObject2.getString("fb_data").isEmpty()||
						 * jsonObject2.getString("fb_data").length()!=0){
						 * 
						 * JSONObject fbJsonObject=new
						 * JSONObject(jsonObject2.getString("fb_data"));
						 * 
						 * Utility.setUserName(SplashScreenActivity.this,
						 * Constants.SHAREDPREFERENCE_NAME, fbJsonObject
						 * .getString("name").trim());
						 * Utility.setMobileNumber(SplashScreenActivity.this,
						 * Constants.SHAREDPREFERENCE_MOBILE, jsonObject2
						 * .getString("mobile").trim());
						 * Utility.setEmail(SplashScreenActivity.this,
						 * Constants.SHAREDPREFERENCE_EMAIL, fbJsonObject
						 * .getString("email").trim());
						 * Utility.setGender(SplashScreenActivity.this,
						 * Constants.SHAREDPREFERENCE_GENDER, fbJsonObject
						 * .getString("gender").trim());
						 * 
						 * String imageId="https://graph.facebook.com/" +
						 * fbJsonObject.getString("id")+ "/picture?type=large";
						 * Utility
						 * .setFbImageId(SplashScreenActivity.this,imageId ); }
						 */

					} else {
						code = jsonObject.getString("errCode");
					}

				} else {
					// Constants.showDialog(SplashScreenActivity.this, TAG,
					// Constants.ERROR_INTERNET, Constants.DIALOG_CLOSE);
				}
			} catch (JSONException e) {
				// e.printStackTrace();
				// Constants.showDialog(SplashScreenActivity.this, TAG,
				// Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
				// Constants.DIALOG_CLOSE);
			} catch (Exception e) {
				// TODO: handle exception
				// Constants.showDialog(SplashScreenActivity.this, TAG,
				// Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
				// Constants.DIALOG_CLOSE);
			}

			// if (Utility.isFirstTime(SplashScreenActivity.this)) {
			// Intent i = new Intent(SplashScreenActivity.this,
			// TourActivity.class);
			//
			// startActivity(i);
			// } else {

			// }

		/*	if (code.equalsIgnoreCase("205")) {
				Utility.setLoginFlag(SplashScreenActivity.this,
						Constants.SHAREDPREFERENCE_IS_LOGIN, false);
				Intent intent = new Intent(SplashScreenActivity.this,
						LoginActivity.class);
				intent.putExtra("AUTO", true);
				startActivity(intent);
				finish();
			} else {*/
			
			if(isFinished){
				
			}else{
			Intent intent = new Intent(SplashScreenActivity.this,
					MainActivity.class);
			isFinished=true;
			
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
					| Intent.FLAG_ACTIVITY_NEW_TASK);
			intent.putExtra("isFirstTimeAfterUpdate", Utility
					.isFirstTimeAfterUpdate(SplashScreenActivity.this));
			intent.putExtra(Constants.RECHARGE_FOR, Constants.RECHARGE_MOBILE);
			startActivity(intent);
			finish();
			}
			
			/*	Intent i = new Intent(SplashScreenActivity.this,
						MainActivity.class);
				i.putExtra("isFirstTimeAfterUpdate", Utility
						.isFirstTimeAfterUpdate(SplashScreenActivity.this));
				i.putExtra(Constants.RECHARGE_FOR, Constants.RECHARGE_MOBILE);
				startActivity(i);
				finish();*/
			//}
		}

		@Override
		protected void onPreExecute() {
			// dialog = new MyProgressDialog(SplashScreenActivity.this);
			// // this.dialog.setMessage("Please wait.....");
			// this.dialog.setCancelable(true);
			// this.dialog
			// .setOnCancelListener(new DialogInterface.OnCancelListener() {
			//
			// @Override
			// public void onCancel(DialogInterface dialog) {
			// // TODO Auto-generated method stub
			// AutoLoginTask.this.cancel(true);
			// }
			// });
			// this.dialog.show();
			super.onPreExecute();
		}

	}

	public class FreeBiesTask extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... params) {
			try {
				// String response = RequestClass
				// .getInstance()
				// .readPay1B2CRequest(
				// MainActivity.this,
				// Constants.B2C_URL
				// + "Get_all_deal_data/?latitude="
				// + params[0]
				// + "&longitude="
				// + params[1]
				// + "&mobile="
				// + Utility
				// .getMobileNumber(
				// MainActivity.this,
				// Constants.SHAREDPREFERENCE_MOBILE));

				String result = Constants.loadJSONFromAsset(
						SplashScreenActivity.this, "gift.json");
				if (!result.startsWith("Error")) {

					try {

						freeBiesDataSource.open();
						freeBiesLocationDataSource.open();

						freeBiesDataSource.deleteFreeBies();
						freeBiesLocationDataSource.deleteFreeBiesLocation();

						long t = System.currentTimeMillis();

						freeBiesDataSource.createFreeBies(result, t);

						// freeBiesLocationDataSource.createFreeBiesLocation(
						// result, t);

						// Utility.setSharedPreferences(MainActivity.this,
						// Constants.LAST_GIFT_UPDATED,
						// String.valueOf(System.currentTimeMillis()));
					} catch (SQLException exception) {
						// TODO: handle exception
						Log.e("Er ", exception.getMessage());
					} catch (Exception e) {
						// TODO: handle exception
						Log.e("Er ", e.getMessage());
					} finally {
						freeBiesDataSource.close();
						freeBiesLocationDataSource.close();
					}
				}
			} catch (Exception e) {

			}
			return "Error";
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

		}

		protected void onPreExecute() {
			super.onPreExecute();
		}

	}

}
