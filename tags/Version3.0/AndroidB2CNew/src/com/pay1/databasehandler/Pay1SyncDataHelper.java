package com.pay1.databasehandler;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import android.content.Context;

public class Pay1SyncDataHelper extends SQLiteAssetHelper{ //SQLiteOpenHelper {
	public static final int DATABASE_VERSION = 4;
	public static final String DATABASE_NAME = "pay1database.db";
	public static final String DATABASE_NAME_GIFT = "syncdatabasegift.db";
	
	public static final String SYNC_DATABASE = "syncdatabase.db";
	public static final String SYNC_DATABASE_GIFT = "syncdatabasegiftnew.db";

	

	public static final String TABLE_PLAN = "PlanTable";

	public static final String PLAN_ID = "p_id";
	public static final String PLAN_OPERATOR_ID = "operator_id";
	public static final String PLAN_OPERATOR_NAME = "operator_name";
	public static final String PLAN_CIRCLE_ID = "circle_id";
	public static final String PLAN_CIRCLE_NAME = "circle_name";
	public static final String PLAN_TYPE = "plan_type";
	public static final String PLAN_AMOUNT = "amount";
	public static final String PLAN_VALIDITY = "validity";
	public static final String PLAN_DESCRIPTION = "description";
	public static final String PLAN_UPDATE_TIME = "time";
	public static final String PLAN_SHOW_FLAG = "show_flag";

	

	public static final String OPERATOR_ID = "op_id";
	public static final String OPERATOR_NAME = "op_name";
	public static final String OPERATOR_CODE = "op_code";
	public static final String RECHARGE_TYPE = "rechargeType";
	public static final String RECHARGE_COUNT = "rechargeCount";

	
	
	public static final String TABLE_NUMBERANDCIRCLE = "NumberAndCircleTable";

	public static final String NAC_AREA_ID = "nac_area_id";
	public static final String NAC_AREA_NAME = "nac_area_name";
	public static final String NAC_AREA_CODE = "nac_area_code";
	public static final String NAC_OPERATOR_CODE = "nac_operator_code";
	public static final String NAC_OPERATOR_NAME = "nac_operator_name";
	public static final String NAC_OPERATOR_ID = "nac_operator_id";
	public static final String NAC_MOBILE_NUMBER = "nac_mobile_number";
	public static final String NAC_UPDATE_TIME = "nac_update_time";
	


	/*private final String CREATE_ENTERTAINMENT = "CREATE TABLE "
			+ TABLE_ENTERTAINMENT + " ( " + ENTERTAINMENT_ID
			+ " integer primary key autoincrement, " + ENTERTAINMENT_PRODUCT_ID
			+ " text, " + ENTERTAINMENT_PRODUCT_CODE + " text, "
			+ ENTERTAINMENT_PRODUCT_NAME + " text, "
			+ ENTERTAINMENT_PRODUCT_PRICE + " text, "
			+ ENTERTAINMENT_PRODUCT_VALIDITY + " text, "
			+ ENTERTAINMENT_PRODUCT_SHORT_DESCRIPTION + " text, "
			+ ENTERTAINMENT_PRODUCT_LONG_DESCRIPTION + " text,"
			+ ENTERTAINMENT_PRODUCT_IMAGE + " text, "
			+ ENTERTAINMENT_PRODUCT_DATA_FIELD + " text, "
			+ ENTERTAINMENT_PRODUCT_DATA_TYPE + " text, "
			+ ENTERTAINMENT_PRODUCT_DATA_LENGTH + " text, "
			+ ENTERTAINMENT_PRODUCT_UPDATE_TIME + " text );";

	private final String CREATE_NOTIFICATION = "CREATE TABLE "
			+ TABLE_NOTIFICATION + " ( " + NOTIFICATION_ID
			+ " integer primary key autoincrement, " + NOTIFICATION_DATA
			+ " text, " + NOTIFICATION_TYPE + " integer, " + NOTIFICATION_TIME
			+ " text );";

	private final String CREATE_PLAN = "CREATE TABLE " + TABLE_PLAN + " ( "
			+ PLAN_ID + " integer primary key autoincrement, "
			+ PLAN_OPERATOR_ID + " text, " + PLAN_OPERATOR_NAME + " text, "
			+ PLAN_CIRCLE_ID + " text, " + PLAN_CIRCLE_NAME + " text, "
			+ PLAN_TYPE + " text, " + PLAN_AMOUNT + " integer, "
			+ PLAN_VALIDITY + " text, " + PLAN_DESCRIPTION + " text, "
			+ PLAN_UPDATE_TIME + " text );";
	
	private final String CREATE_PLAN = "CREATE TABLE " + TABLE_PLAN + " ( "
			+ PLAN_ID + " integer primary key autoincrement, "
			+ PLAN_OPERATOR_ID + " text, " + PLAN_OPERATOR_NAME + " text, "
			+ PLAN_CIRCLE_ID + " text, " + PLAN_CIRCLE_NAME + " text, "
			+ PLAN_TYPE + " text, " + PLAN_AMOUNT + " integer, "
			+ PLAN_VALIDITY + " text, " + PLAN_DESCRIPTION + " text, "
			+ PLAN_UPDATE_TIME + " text,"+PLAN_SHOW_FLAG+" text );";

	private final String CREATE_VMN = "CREATE TABLE " + TABLE_VMN + " ( "
			+ VMN_ID + " integer primary key, " + VMN_NUMBER + " text );";

	private final String MOST_VISITED = "CREATE TABLE "
			+ TABLE_MOST_VISITED_OPERATOR + " ( " + OPERATOR_ID
			+ " integer primary key, " + OPERATOR_NAME + " text, "
			+ OPERATOR_CODE + " text, " + RECHARGE_TYPE + " text, "
			+ RECHARGE_COUNT + " integer );";

	private final String CREATE_ADDRESS = "CREATE TABLE " + TABLE_ADDRESS
			+ " ( " + ADDRESS_ID + " integer primary key autoincrement, "
			+ ADDRESS_LINE + " text, " + ADDRESS_AREA + " text, "
			+ ADDRESS_CITY + " text, " + ADDRESS_STATE + " text, "
			+ ADDRESS_ZIP + " text, " + ADDRESS_LATITUDE + " text, "
			+ ADDRESS_LONGITUDE + " text);";
	
	private final String CREATE_NUMBER_CIRCLE = "CREATE TABLE " + TABLE_NUMBERANDCIRCLE
			+ " ( " + NAC_AREA_ID + " integer primary key autoincrement, "
			+ NAC_AREA_CODE + " text, " + NAC_AREA_NAME + " text, "
			+ NAC_OPERATOR_CODE + " text, " + NAC_OPERATOR_NAME + " text, "
			+ NAC_OPERATOR_ID + " text, " + NAC_MOBILE_NUMBER + " text,"
			+ NAC_UPDATE_TIME + " text "
			+ ");";

	private final String CREATE_CHATHISTORY = "CREATE TABLE "
			+ TABLE_CHATHISTORY + " ( " + CHATHISTORY_ID
			+ " integer primary key autoincrement, " + CHATHISTORY_TEXT
			+ " text, " + CHATHISTORY_TIME + " text, " + CHATHISTORY_POSITION
			+ " text );";*/
	
	private Context dbContext;
//	public static String dbPath = "/mnt/sdcard/Download";
	
	public Pay1SyncDataHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		setForcedUpgrade(DATABASE_VERSION);
		 dbContext = context;
	} 
	
/*	public Pay1SyncDataHelper(Context context, String databaseName) {
		super(context, databaseName, null, DATABASE_VERSION);
		 dbContext = context;
	} */
	
	public boolean cloneDatabase(String inputDBName, String outputDBName) {
		try{
			File dbFile = dbContext.getDatabasePath(inputDBName);//new File(dbPath + "/" + inputDBName);
			InputStream input = new FileInputStream(dbFile);
			
		    OutputStream output = new FileOutputStream(dbFile.getAbsoluteFile().getParentFile().getAbsolutePath() + "/" + outputDBName);
	
		    byte[] buffer = new byte[1024];
		    int length;
		    while ((length = input.read(buffer)) > 0)
		    {
		        output.write(buffer, 0, length);
		    }
		    
		    output.flush();
		    output.close();
		    input.close();
		    return true;
		}
		catch(IOException e){
			e.printStackTrace();
			return false;
		} 
	}

	
//	@Override
//	public void onCreate(SQLiteDatabase db) {
//		// TODO Auto-generated method stub
//		db.execSQL(CREATE_ENTERTAINMENT);
//		db.execSQL(CREATE_NOTIFICATION);
//		db.execSQL(CREATE_PLAN);
//		db.execSQL(CREATE_VMN);
//		db.execSQL(CREATE_ADDRESS);
//		db.execSQL(CREATE_CHATHISTORY);
//		db.execSQL(MOST_VISITED);
//		db.execSQL(CREATE_NUMBER_CIRCLE);
//	}
//
//	@Override
//	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
//		// TODO Auto-generated method stub
//		db.execSQL("DROP TABLE IF EXISTS " + TABLE_ENTERTAINMENT);
//		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NOTIFICATION);
//		db.execSQL("DROP TABLE IF EXISTS " + TABLE_PLAN);
//		db.execSQL("DROP TABLE IF EXISTS " + TABLE_VMN);
//		db.execSQL("DROP TABLE IF EXISTS " + TABLE_ADDRESS);
//		db.execSQL("DROP TABLE IF EXISTS " + TABLE_CHATHISTORY);
//		db.execSQL("DROP TABLE IF EXISTS " + MOST_VISITED);
//		onCreate(db);
//	}

}

