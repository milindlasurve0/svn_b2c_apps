﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Pay1B2C.UserControls;
using System.Windows.Media.Imaging;

namespace Pay1B2C.Views
    {
    public partial class CoinHistory: PhoneApplicationPage
       {
        ProgressBarManager ProgressBarManager;

        List<string> purchaseList;
        public CoinHistory()
            {
            InitializeComponent();
            GoogleAnalytics.EasyTracker.GetTracker().SendView("Coin History");

           // Constant.laodNavigationBar(TitlePanel, LayoutRoot, canvas, "My Transactions");
            purchaseList = new List<string>();
           // Constant.getAllContacts();
            ProgressIndicator prog = new ProgressIndicator();
            ProgressBarManager = new ProgressBarManager();
            SystemTray.SetProgressIndicator(this, prog);
            ProgressBarManager.Hook(prog);
            getPurchaseTask(0);


            }
        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
            {
            //Do your work here
            base.OnBackKeyPress(e);
            /*NavigationService.Navigate(new Uri("/HomePage.xaml?", UriKind.RelativeOrAbsolute));
             while(this.NavigationService.BackStack.Any())
                 {
                 this.NavigationService.RemoveBackEntry();
                 }*/
            }
        private async void getPurchaseTask(int page)
            {
            ProgressBarManager.Show(19, "Please wait...", false, 10);
            if(coinListBox.Items.Count > 0)
                {
                coinListBox.Items.RemoveAt(coinListBox.Items.Count - 1);
                }

            var values = new List<KeyValuePair<string, string>>();
            values.Add(new KeyValuePair<string, string>("actiontype", "LoyaltyPointsHistory"));
            values.Add(new KeyValuePair<string, string>("api_version", Constant.API_VERSION));
            values.Add(new KeyValuePair<string, string>("api", "true"));
            FormUrlEncodedContent values1 = new FormUrlEncodedContent(values);
            Task<string> response = Constant.postParamsAndgetResponse(Constant.B2C_URL, values1);

            string result = await response;
            if(string.IsNullOrEmpty(result))
                {
                NavigationService.Navigate(new Uri("/Views/NoConnection.xaml?", UriKind.Relative));
                }
            else
                {
                var dict = (JObject)JsonConvert.DeserializeObject(result);
                string status = dict["status"].ToString();

                if(status.Equals("success"))
                    {
                    var description = dict["description"];

                    foreach(var item in description)
                        {
                        GiftCoinsControls giftCoinControl=new GiftCoinsControls();
                        giftCoinControl.textTransId.Text="TRANSACTION ID: "+item["trans_id"].ToString();
                         giftCoinControl.textDate.Text=item["trans_datetime"].ToString();



                         if(item["redeem_status"].ToString().Equals("1"))
                             {
                             giftCoinControl.textCoinNumber.Text=item["coins"].ToString()+ " coins redeemed";
                           
                             giftCoinControl.textMobileNumber.Text="Grabbed gift \""+ item["Name"] + "\"";
                           
                             giftCoinControl.imageCoin.Source=new BitmapImage(new Uri("/Images/ic_coin_minus.png", UriKind.Relative)); 
                           
                             }
                         else if(item["redeem_status"].ToString().Equals("2"))
                             {
                              giftCoinControl.textCoinNumber.Text=item["coins"].ToString()+ " coins reversed";
                           
                             giftCoinControl.textMobileNumber.Text="Grabbed gift \""+ item["Name"] + "\"";

                             giftCoinControl.imageCoin.Source=new BitmapImage(new Uri("/Images/ic_coin_minus.png", UriKind.Relative));
                            

                             }
                         else if(item["redeem_status"].ToString().Equals("3"))
                             {


                              giftCoinControl.textCoinNumber.Text=item["coins"].ToString()+ " coins added";
                           
                             giftCoinControl.textMobileNumber.Text=item["rechargeType"].ToString()+" of Rs. "+ item["transaction_amount"] + "\n("+ item["Name"].ToString()+")";
                           
                             giftCoinControl.imageCoin.Source=new BitmapImage(new Uri("/Images/ic_coin_plus.png", UriKind.Relative));
                           

                             }
                         else if(item["redeem_status"].ToString().Equals("4"))
                             {


                             giftCoinControl.textCoinNumber.Text=item["coins"].ToString()+ " coins reversed";
                           
                             giftCoinControl.textMobileNumber.Text=item["msg"].ToString();

                             giftCoinControl.imageCoin.Source=new BitmapImage(new Uri("/Images/ic_coin_minus.png", UriKind.Relative));
                           
                             }
                         else if(item["redeem_status"].ToString().Equals("5"))
                             {

                             giftCoinControl.textCoinNumber.Text=item["coins"].ToString()+ " coins added";
                           
                             giftCoinControl.textMobileNumber.Text=item["msg"].ToString();

                             giftCoinControl.imageCoin.Source=new BitmapImage(new Uri("/Images/ic_coin_plus.png", UriKind.Relative));
                           


                             }
                         else
                             {
                                
                                 giftCoinControl.textCoinNumber.Text="NA";

                                 giftCoinControl.textMobileNumber.Text="NA";

                                 giftCoinControl.imageCoin.Source=new BitmapImage(new Uri("/Images/ic_coin_plus.png", UriKind.Relative));
                           

                                 
                             }

                         coinListBox.Items.Add(giftCoinControl);

                        }

                    if(coinListBox.Items.Count == 0)
                        {
                        emptyViewPanel.Visibility = Visibility.Visible;
                        coinListBox.Visibility = Visibility.Collapsed;
                        }
                    else
                        {
                        emptyViewPanel.Visibility = Visibility.Collapsed;
                        coinListBox.Visibility = Visibility.Visible;
                        }

                   
                    }
                else
                    {
                    Constant.checkForErrorCode(result);
                    }
                }
            ProgressBarManager.Hide(19);
            }

    

        private void Button_Click(object sender, RoutedEventArgs e)
            {
            PhoneApplicationService.Current.State["data"] = "2";
            (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri("/RechargeAndBills.xaml?", UriKind.RelativeOrAbsolute));
            (Application.Current.RootVisual as PhoneApplicationFrame).RemoveBackEntry();
            }

        private void ButtonImage_Click(object sender, RoutedEventArgs e)
            {
            NavigationService.GoBack();
            }

        }
    }