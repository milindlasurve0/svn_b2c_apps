﻿using Microsoft.Phone.Controls;
using Microsoft.Phone.UserData;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Cimbalino.Phone.Toolkit.Extensions;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using ZXing;
using Microsoft.Phone.Shell;
using System.Windows.Controls.Primitives;
using Pay1B2C.UserControls;

using Microsoft.Phone.Tasks;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading;
using System.Windows.Threading;
using Pay1B2C.Utilities;
using System.Device.Location;
using Windows.Devices.Geolocation;
using System.IO;

namespace Pay1B2C
    {
    public static class Constant
        {

        public static readonly string FacebookAppId = "Your Facebook App ID here";
        public const String DATABASE_NAME = "Pay1B2C";
        //public const String B2C_URL = "http://cdev.pay1.in/index.php/api_new/action/";
        public const String B2C_URL = "https://b2c.pay1.in/index.php/api_new/action/";
        public const String B2B_URL = "http://panel.pay1.in/apis/receiveWeb/mindsarray/mindsarray/json?";
        public const String API_VERSION="3";
        public const String OPERATOR_RECHARGE_FLAG = "flag";
        

        public const int SIDE_BAR_MOBILE = 1;
        public const int SIDE_BAR_DATA = 3;
        public const int SIDE_BAR_DTH = 2;
        public const int SIDE_BAR_BILL = 4;
        public const int SIDE_BAR_ELECTRICITY = 5;
        public const int SIDE_BAR_DEALS = 6;
        public const int WALLET_REFILL = 7;
        public const int MOBILE_RECHARGE = 9;
        public const int WALLET_PROFILE = 8;
        public const String IS_PROFILE = "is_profile";
        public const String NOTIFICATION_URL = "notificationUrl";
        public const String OPERATOR_LIST_UPDATE_TIME = "operator_list_update_time";
        public const String FREEBIE_LIST_UPDATE_TIME = "freeBie_list_update_time";
        //public const String FREEBIE_LIST_UPDATE_TIME = "freeBie_list_update_time";
        public const String PLAN_INSERT_TIME = "plan_update_time";
        public const String CIRCLE_INSERT_TIME = "circle_update_time";

        public const int SIDE_BAR_HOME = 1;
        public const int SIDE_BAR_RECHARGE_N_BILLS = 3;
        public const int SIDE_BAR_MY_GIFTS = 2;
        public const int SIDE_BAR_MISSED_CALL = 4;
        public const int SIDE_BAR_MY_TRANS = 5;

        public const String MOBILE_NUMBER = "mobile_number";
        public const String RECHARGE_AMOUNT = "recharge_amount";
        public const String SETTINGS_IS_LOGIN = "islogin";
        public const String SETTINGS_IS_NEW = "is_new";
        public const String SETTINGS_IS_FIRST = "is_first";
        public const String SETTINGS_IS_FIRST_TIME = "is_first_time";
        public const String SETTINGS_USER_NAME = "user_name";
        public const String SETTINGS_USER_PIN = "user_pin";
        public const String SETTINGS_USER_MOBILE = "user_mobile";
        public const String SETTINGS_USER_EMAIL = "user_email";
        public const String SETTINGS_USER_GENDER = "user_gender";
        public const String SETTINGS_USER_ID = "user_id";
        public const String SETTINGS_USER_WALLET_BALANCE = "wallet_balance";
        public const String SETTINGS_USER_DOB = "user_date_of_birth";
        public const String SETTINGS_USER_LATTITUDE = "latitude";
        public const String SETTINGS_USER_LONGITUDE = "longitude";
        public const String SETTINGS_USER_LATTITUDE_DOUBLE = "latitudeDbl";
        public const String SETTINGS_USER_LONGITUDE_DOUBLE = "longitudeDbl";
        
        public const String SETTINGS_USER_LOCATION_SELECTED = "location_selected";
        public const String SETTINGS_USER_SUPPORT_NUMBER = "support_number";
        public const String SHOP_DATA = "shopData";
        public const String RECHARGE_OPERATOR = "rechargeOperator";
        
        public const String SHOP_LOCATION_DATA = "shop_location_Data";
        public const String SHOP_LOCATION_JSON = "shop_location_Json";
        public const String PAYMENT_GATEWAY_HTML = "paymentGatewatHtml";
        public const String RECHARGE_RESULT = "recharge_result";
        public const String RECHARGE_REASON = "recharge_reason";
        public const String OPERATOR_RECHARGE_ID = "operator_id";
        public const String REQUEST_FOR = "requestFor";
        public const String OPERATOR_RECHARGE_FOR = "rechargeFor";
        public const String OPERATOR_RECHARGE_LOGO = "operator_logo";
        public const String OPERATOR_RECHARGE_NAME = "operator_name";
        public const String MYFREEBIE = "myFreeBie";
        public const String RECHARGE_FREEBIE = "recFreeBie";
        public const String ALLFREEBIE = "allFreeBie";
        public const String SETTINGS_USER_SERVICE_CHARGE = "service_charge_percent";
        public const String SETTINGS_USER_SERVICE_TAX = "service_tax_percent";
        public const String FEATURED_DEALS="FEATURED_DEALS";

        // 	this.Perform (() => MyMethod (), 2500);
        public static double curLat, curLong;
        public static async void getCurrentLatLong()
            {
            Geolocator geolocator = new Geolocator();
            geolocator.DesiredAccuracyInMeters = 50;

            try
                {
                //Geoposition geoposition = await geolocator.GetGeopositionAsync(
                //    maximumAge : TimeSpan.FromMinutes(5),
                //    timeout : TimeSpan.FromSeconds(10)
                //    );
                try
                    {
                    Geoposition geoposition = await geolocator.GetGeopositionAsync();
                    curLat = geoposition.Coordinate.Latitude;
                    curLong = geoposition.Coordinate.Longitude;

                    Constant.SavePersistent(Constant.SETTINGS_USER_LATTITUDE_DOUBLE, curLat);
                    Constant.SavePersistent(Constant.SETTINGS_USER_LONGITUDE_DOUBLE, curLong);
                    }
                catch(Exception exc)
                    {
                    }

               
                }
            catch(Exception exc)
                {
                }
            }

        public static void showTwoButtonDialog(String capString, String msgString)
            {

            CustomMessageBox messageBox = new CustomMessageBox()
            {
                //set the properties
                Caption =capString,
                Message = msgString,
                LeftButtonContent = "yes",
                RightButtonContent = "no"
            };

            //Add the dismissed event handler
            messageBox.Dismissed += (s1, e1) =>
            {
                switch(e1.Result)
                    {
                    case CustomMessageBoxResult.LeftButton:
                        //add the task you wish to perform when user clicks on yes button here

                        break;
                    case CustomMessageBoxResult.RightButton:
                        //add the task you wish to perform when user clicks on no button here

                        break;
                    case CustomMessageBoxResult.None:
                        // Do something.
                        break;
                    default:
                        break;
                    }
            };

            //add the show method
            messageBox.Show();


            }


        public static void showOneButtonDialog(String capString, String msgString)
            {

            CustomMessageBox messageBox = new CustomMessageBox()
            {
                //set the properties
                Caption =capString,
                Message = msgString,
                LeftButtonContent = "Done",
               // RightButtonContent = "no"
            };

            //Add the dismissed event handler
            messageBox.Dismissed += (s1, e1) =>
            {
                switch(e1.Result)
                    {
                    case CustomMessageBoxResult.LeftButton:
                        //add the task you wish to perform when user clicks on yes button here

                        break;
                    case CustomMessageBoxResult.RightButton:
                        //add the task you wish to perform when user clicks on no button here

                        break;
                    case CustomMessageBoxResult.None:
                        // Do something.
                        break;
                    default:
                        break;
                    }
            };

            //add the show method
            messageBox.Show();


            }







        public async static Task<String> postParamsAndGetResponse(FormUrlEncodedContent values)
            {

            string responseFromServer = ""; ;

            CookieContainer cookies = new CookieContainer();
            HttpClientHandler handler = new HttpClientHandler();

            Uri uri = new Uri(B2C_URL);

            string cookieName = LoadPersistent<string>("allCookie");
            //string cookie_value = LoadPersistent<string> ("cookieValue");
            if(cookieName != null)
                cookies= getList(B2C_URL);

            handler.CookieContainer = cookies;

            try
                {
                HttpClient httpClient = new HttpClient(handler);

                HttpResponseMessage response = await httpClient.PostAsync(B2C_URL, values);


                var responseCookies = cookies.GetCookies(uri);//.Cast<Cookie> ();
                int ii = 0;
                int count = ii + 1;
                var check = LoadPersistent<string>("cookieName");

                var responseString = await response.Content.ReadAsStringAsync();
                responseFromServer = responseString.ToString();
                //IEnumerable<string> setCookieVal1 = response.Headers.GetValues ("Set-Cookie");
                if((int)PhoneApplicationService.Current.State["loginCall"] == 1)
                    {

                    IEnumerable<string> setCookieVal = response.Headers.GetValues("Set-Cookie");

                    foreach(string cookie in setCookieVal)
                        {
                        SavePersistent("allCookie", cookie);

                        PhoneApplicationService.Current.State["loginCall"] = 2;

                        }
                    }


                }
            catch(Exception sec)
                {
                }
            return responseFromServer;

            }

        //public static bool isLogin()
        //{
        //    return Constant.LoadPersistent<Boolean>(Constant.SETTINGS_IS_LOGIN);
        //}

        public static CookieContainer ReadCookies(this HttpResponseMessage response)
            {
            var pageUri = response.RequestMessage.RequestUri;

            var cookieContainer = new CookieContainer();
            IEnumerable<string> cookies;
            if(response.Headers.TryGetValues("Set-Cookie", out cookies))
                {
                foreach(var cookie in cookies)
                    {
                    SavePersistent("cookieName", cookie);
                    //SavePersistent("cookieValue", cookie.Value);
                    cookieContainer.SetCookies(pageUri, cookie);
                    break;
                    }
                }

            return cookieContainer;
            }
        public static void MyMethod()
            {


            DispatcherTimer DelayedTimer = new DispatcherTimer()
            {
                Interval = TimeSpan.FromSeconds(2)
            };
            DelayedTimer.Tick += (s, e) =>
            {
                ShellToast toast = new ShellToast();
                toast.Title = "Pay1";
                toast.Content = "Grab a gift.";
                toast.NavigationUri = new Uri("/PostRechargeFreeBie.xaml?msg=You forgot to grab your free gift.\nExplore the unlimited excitement,grab a gift now!!!", UriKind.RelativeOrAbsolute);
                toast.Show();
            };
            DelayedTimer.Start();



            }

        public static void SharingOnSocialMedia()
            {
            ShareLinkTask shareLinkTask = new ShareLinkTask();
            shareLinkTask.Title = "DebugMode";
            shareLinkTask.LinkUri = new Uri("http://debugmode.net", UriKind.Absolute);
            shareLinkTask.Message = "Post of DebugMode.";
            shareLinkTask.Show();
            }

        public static void Perform(Action myMethod, int delayInMilliseconds)
            {
            BackgroundWorker worker = new BackgroundWorker();

            worker.DoWork += (s, e) => Thread.Sleep(delayInMilliseconds);

            worker.RunWorkerCompleted += (s, e) => myMethod.Invoke();

            worker.RunWorkerAsync();
            }




        public static string jsonCircle = @"[
{
        'type': 'States',
        'id': 0,
        'name': 'Select Circle',
        'code': 'SC'
    },
    {
        'type': 'States',
        'id': 1,
        'name': 'Andhra Pradesh',
        'code': 'AP'
    },
    {
        'type': 'States',
        'id': 2,
        'name': 'Assam',
        'code': 'AS'
    },
    {
        'type': 'States',
        'id': 3,
        'name': 'Bihar & Jharkhand',
        'code': 'BR'
    },
    {
        'type': 'Metros',
        'id': 5,
        'name': 'Delhi NCR',
        'code': 'DL'
    },
    {
        'type': 'States',
        'id': 6,
        'name': 'Gujarat',
        'code': 'GJ'
    },
    {
        'type': 'States',
        'id': 7,
        'name': 'Haryana',
        'code': 'NE'
    },
    {
        'type': 'States',
        'id': 8,
        'name': 'Himachal Pradesh',
        'code': 'HP'
    },
    {
        'type': 'States',
        'id': 9,
        'name': 'Jammu & Kashmir',
        'code': 'JK'
    },
    {
        'type': 'States',
        'id': 10,
        'name': 'Karnataka',
        'code': 'KA'
    },
    {
        'type': 'States',
        'id': 11,
        'name': 'Kerala',
        'code': 'KL'
    },
    {
        'type': 'Metros',
        'id': 12,
        'name': 'Kolkata',
        'code': 'KO'
    },
    {
        'type': 'States',
        'id': 13,
        'name': 'Maharashtra & Goa',
        'code': 'MH'
    },
    {
        'type': 'States',
        'id': 14,
        'name': 'Madhya Pradesh & Chattisgarh',
        'code': 'MP'
    },
    {
        'type': 'Metros',
        'id': 15,
        'name': 'Mumbai',
        'code': 'MU'
    },
    {
        'type': 'States',
        'id': 16,
        'name': 'North East India',
        'code': 'NE'
    },
    {
        'type': 'States',
        'id': 17,
        'name': 'Orissa',
        'code': 'OR'
    },
    {
        'type': 'States',
        'id': 18,
        'name': 'Punjab',
        'code': 'PB'
    },
    {
        'type': 'States',
        'id': 19,
        'name': 'Rajasthan',
        'code': 'RJ'
    },
    {
        'type': 'States',
        'id': 20,
        'name': 'Tamil Nadu',
        'code': 'TN'
    },
    {
        'type': 'States',
        'id': 21,
        'name': 'Uttar Pradesh (East)',
        'code': 'UE'
    },{
        'type': 'States',
        'id': 22,
        'name': 'Uttar Pradesh (West) & Uttarakhand',
        'code': 'UW'
    }, 
    {
        'type': 'States',
        'id': 23,
        'name': 'West Bengal',
        'code': 'WB'
    }
]";



        public static double distance(double curLat,double cutLong,double shopLat,double shopLong)
            {
            var shopCoord = new GeoCoordinate(shopLat, shopLong);
            var curCoord = new GeoCoordinate(curLat, cutLong);


            double distance = shopCoord.GetDistanceTo(curCoord);
            return distance/1000;
            }

        public static bool ValidateEmail(string email)
            {
            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            Match match = regex.Match(email);
            if(match.Success)
                return true;
            else
                return false;
            }

        private static Version TargetedVersion = new Version(8, 0, 10492);
        public static bool IsTargetedVersion
            {
            get { return Environment.OSVersion.Version >= TargetedVersion; }
            }


        public static string GetLast(this string source, int tail_length)
            {
            if(tail_length >= source.Length)
                return source;
            return source.Substring(source.Length - tail_length);
            }

        //public static List<string> getFreeBieIds ()
        //	{
        //					 List<string> aa= new List<string>();
        //	string str_select = "SELECT deal_id,freeBieUpdateTime  FROM FreeBieAllData ORDER BY  deal_id";

        //	ObservableCollection<NewFreeBieAllData> student_oblist = (Application.Current as App).dbh.SelectObservableCollection<NewFreeBieAllData> (str_select);
        //	   return 	aa;
        //	//
        //	}


        public static Color ConvertStringToColor(String hex)
            {
            //remove the # at the front
            hex = hex.Replace("#", "");

            byte a = 255;
            byte r = 255;
            byte g = 255;
            byte b = 255;

            int start = 0;

            //handle ARGB strings (8 characters long)
            if(hex.Length == 8)
                {
                a = byte.Parse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
                start = 2;
                }

            //convert RGB characters to bytes
            r = byte.Parse(hex.Substring(start, 2), System.Globalization.NumberStyles.HexNumber);
            g = byte.Parse(hex.Substring(start + 2, 2), System.Globalization.NumberStyles.HexNumber);
            b = byte.Parse(hex.Substring(start + 4, 2), System.Globalization.NumberStyles.HexNumber);

            return Color.FromArgb(a, r, g, b);
            }


        public static bool checkInternetConnection()
            {
            return (Microsoft.Phone.Net.NetworkInformation.NetworkInterface.NetworkInterfaceType !=
					Microsoft.Phone.Net.NetworkInformation.NetworkInterfaceType.None);
            }

        public static void checkForErrorCode(string errCode)
            {
            JObject jObj = (JObject)JsonConvert.DeserializeObject(errCode);
            string code = jObj["errCode"].ToString();
            switch(code)
                {
                case "201": (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri("/RegistrationLayout.xaml?", UriKind.RelativeOrAbsolute));
                    break;
                default:
                    MessageBox.Show(jObj["description"].ToString()); ;
                    break;
                };
            }



        public static ImageSource GenerateBarCode(string data)
            {
            //Generate Barcode in Windows Phone 8
            ImageSource barcodeImage = null;
            if(!string.IsNullOrEmpty(data))
                {
                BarcodeWriter BarcodeData = new BarcodeWriter();

                BarcodeData.Format = BarcodeFormat.QR_CODE;

                BarcodeData.Options.Height = 500;

                BarcodeData.Options.Width = 500;

                BarcodeData.Options.Margin = 1;

                barcodeImage = BarcodeData.Write(data);
                }
            else
                {
                barcodeImage = new BitmapImage(new Uri("Images/deal_default.png", UriKind.RelativeOrAbsolute));
                }
            return barcodeImage;
            }

        public static void getAllContacts()
            {
            Contacts objContacts = new Contacts();
            objContacts.SearchCompleted += new EventHandler<ContactsSearchEventArgs>(objContacts_SearchCompleted);
            objContacts.SearchAsync(string.Empty, FilterKind.None, null);
            }

        public static List<Dictionary<string, string>> lst1;
        public static void objContacts_SearchCompleted(object sender, ContactsSearchEventArgs e)
            {
            lst1 = new List<Dictionary<string, string>>();
            foreach(var result in e.Results)
                {
                Dictionary<string, string> d = new Dictionary<string, string>();
                d.Add(result.DisplayName, RemoveSpecialCharacters(result.PhoneNumbers.FirstOrDefault().PhoneNumber));
                lst1.Add(d);
                }

            }

        public static string RemoveSpecialCharacters(string str)
            {
            StringBuilder sb = new StringBuilder();
            foreach(char c in str)
                {
                if((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || c == '.' || c == '_')
                    {
                    sb.Append(c);
                    }
                }
            return sb.ToString();
            }


        public static string getContactName(string contactNumber)
            {
            string conatctName = "Unknown";
            for(int i = 0;i < lst1.Count;i++)
                {
                string ss = lst1[i].Values.First();
                string val = lst1[i].Keys.First();// ToString();
                if(ss.Contains(GetLast(contactNumber, 10)))
                    {
                    conatctName = val;
                    break;
                    }
                }
            return conatctName;
            }


        public static bool ClearPersistent(string key)
            {
            if(null == key)
                return false;

            var store = IsolatedStorageSettings.ApplicationSettings;
            if(store.Contains(key))
                store.Remove(key);
            store.Save();
            return true;
            }

        public static bool SavePersistent(string key, object value)
            {

            var store = IsolatedStorageSettings.ApplicationSettings;
            if(store.Contains(key))
                store[key] = value;
            else
                store.Add(key, value);

            store.Save();
            return true;
            }

        public static T LoadPersistent<T>(string key)
            {
            var store = IsolatedStorageSettings.ApplicationSettings;
            if(!store.Contains(key))
                return default(T);

            return (T)store[key];
            }

        public static string EncodeString(string password)
            {
            //var myString = password;

            //var myStringBytes = myString.GetBytes(); // this will get the UTF8 bytes for the string

            //var md5Hash = myStringBytes.ComputeMD5Hash().ToBase64String();

           String hash = MD5CryptoServiceProvider.GetMd5String(password);
           return hash;
            }

        public static void ShowSuccessPopUp(Grid ContentOverlay, string recFlag, string popupNumber, string amount, string pay1_pro_id1, bool rechargeStatus,String msg,String points)
            {
            bool isDone=true;
            PhoneApplicationService.Current.State["rechargeAmount"] = amount;
            ContentOverlay.Visibility = Visibility.Visible;
            Popup popup = new Popup();
          
            popup.VerticalOffset = 200;
            popup.HorizontalOffset=25;
            SuccessPopup control = new SuccessPopup();
            if(rechargeStatus==false)
                {
                control.buttonImageOk.Content="OK";
                control.buttonImageCancel.Content="CANCEL";
                }



            if(rechargeStatus)
                {
                control.statusHeader.Text = "SUCCESS";
                if(!recFlag.Equals(Constant.WALLET_REFILL))
                    {
                    control.congratsPanel.Visibility=Visibility.Visible;
                    control.textGiftCoins.Text="You have earned "+points+" gifts coins!";
                    control.Height=500;
                    }
                else
                    {
                    control.congratsPanel.Visibility=Visibility.Collapsed;
                    control.Height=350;
                    }
                
                }
            else
                {
                control.statusHeader.Text = "FAILED";
                if(!recFlag.Equals(Constant.WALLET_REFILL))
                    {
                    control.congratsPanel.Visibility=Visibility.Collapsed;
                    control.Height=350;
                    }
                else
                    {
                    control.congratsPanel.Visibility=Visibility.Collapsed;
                    control.Height=350;
                    }
              
                }
            popup.Child = control;
            control.imageOperator.Source = OperatorConstant.getOperatorResource(Convert.ToInt16(pay1_pro_id1));
            control.textPopupNumber.Text = popupNumber;
            control.textPopupAmount.Text = "Rs. " + amount;
            if(popup.IsOpen!=true)
                {
                popup.IsOpen = true;
                if(recFlag.Equals("1"))
                    {
                    control.textMessage.Text = "Prepaid Recharge\n"+msg;
                    }
                else if(recFlag.Equals("2"))
                    {
                    control.textMessage.Text = "DTH Recharge\n"+msg;
                    }
                else if(recFlag.Equals("3"))
                    {
                    control.textMessage.Text = "Datacard Recharge\n"+msg;
                    }
                else if(recFlag.Equals("4"))
                    {
                    control.textMessage.Text = "Postpaid Bill\n"+msg;
                    }
                else if(recFlag.Equals("5"))
                    {
                    if(PhoneApplicationService.Current.State.ContainsKey("Wallet_Topup"))
                        control.textMessage.Text = PhoneApplicationService.Current.State["Wallet_Topup"].ToString();
                    }
                control.textMessage.Text=control.textMessage.Text.ToUpper();
                control.buttonImageOk.Click += (s, args) =>
                {
                    if(rechargeStatus && !recFlag.Equals("5"))
                        {
                        (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri("/GiftMainPivot.xaml?", UriKind.RelativeOrAbsolute));
                        (Application.Current.RootVisual as PhoneApplicationFrame).RemoveBackEntry();
                        PhoneApplicationService.Current.State.Remove("operatorData");
                        ContentOverlay.Visibility = Visibility.Collapsed;
                        popup.IsOpen = false;
                        }
                    else
                        {
                        (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri("/HomePage.xaml?", UriKind.RelativeOrAbsolute));
                        (Application.Current.RootVisual as PhoneApplicationFrame).RemoveBackEntry();
                        PhoneApplicationService.Current.State.Remove("operatorData");
                        ContentOverlay.Visibility = Visibility.Collapsed;
                        popup.IsOpen = false;
                        }

                };


                control.buttonImageCancel.Click += (s, args) =>
                {
                    PhoneApplicationService.Current.State.Remove("operatorData");
                    if(rechargeStatus && !recFlag.Equals("5"))
                        {
                        (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri("/HomePage.xaml?", UriKind.RelativeOrAbsolute));
                        (Application.Current.RootVisual as PhoneApplicationFrame).RemoveBackEntry();
                        ContentOverlay.Visibility = Visibility.Collapsed;
                        popup.IsOpen = false;
                        }
                    else
                        {
                        (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri("/HomePage.xaml?", UriKind.RelativeOrAbsolute));
                        (Application.Current.RootVisual as PhoneApplicationFrame).RemoveBackEntry();
                        ContentOverlay.Visibility = Visibility.Collapsed;
                        popup.IsOpen = false;
                        }

                };

                }
            }


        public static string ConvertImageToBase64(BitmapImage BitmapImageObject)
            {
           

            WriteableBitmap WriteableBitmapObject = new WriteableBitmap(BitmapImageObject.PixelWidth, BitmapImageObject.PixelHeight);

            byte[] bytearrayObject = ConvertToByteArray(WriteableBitmapObject);

            return Convert.ToBase64String(bytearrayObject);

            }

        public static byte[] ConvertToByteArray(WriteableBitmap writeableBitmap)
            {
            using(var ms = new MemoryStream())
                {
                writeableBitmap.SaveJpeg(ms, 100, 100, 0, 50);

                return ms.ToArray();
                }
            }


        public static BitmapImage Base64StringToBitmap(string base64String)
            {
            byte[] byteBuffer = Convert.FromBase64String(base64String);
            MemoryStream memoryStream = new MemoryStream(byteBuffer);
            memoryStream.Position = 0;

            BitmapImage bitmapImage = new BitmapImage();
            bitmapImage.SetSource(memoryStream);

            memoryStream.Close();
            memoryStream = null;
            byteBuffer = null;

            return bitmapImage;
            }


        public static String getEmail()
            {
            return Constant.LoadPersistent<String>("EMAIL");
            }

        public static void setEmail(String email)
            {
            Constant.SavePersistent("EMAIL", email);
            }


        public static String getUserNumber()
            {
            return Constant.LoadPersistent<String>("USER_NUMBER");
            }

        public static void setUserNumber(String number)
            {
            Constant.SavePersistent("USER_NUMBER", number);
            }


        public static String getUserName()
            {
            return Constant.LoadPersistent<String>("USER_NAME");
            }

        public static void setUserName(String number)
            {
            Constant.SavePersistent("USER_NAME", number);
            }


        public static String getUserimage()
            {
            return Constant.LoadPersistent<String>("USER_IMAGE");
            }

        public static void setUserimage(String image)
            {
            Constant.SavePersistent("USER_IMAGE", image);
            }

        



        public static String getbalance()
            {
            return Constant.LoadPersistent<String>("USER_BALANCE");
            }
        public static int getbalanceInInt()
            {
            string bal = Constant.LoadPersistent<String>("USER_BALANCE");
            double balDbl = Convert.ToDouble(bal);
            int intBal = Convert.ToInt16(balDbl);
            return intBal;
            }

        public static void setbalance(String balance)
            {
            Constant.SavePersistent("USER_BALANCE", balance);
            }
        

        public static String getLastTransaction()
            {
            return Constant.LoadPersistent<String>("LastTransaction");
            }

        public static void setLoyalityPoints(String balance)
            {
            Constant.SavePersistent("loyalty_points", balance);
            }
           public static String getLoyalityPoints()
            {
            return Constant.LoadPersistent<String>("loyalty_points");
            }


        public static void setLastTransaction(String trans)
            {
            Constant.SavePersistent("LastTransaction", trans);
            }



        public static String getOtpHash()
            {
            return Constant.LoadPersistent<String>("OTP");
            }

        public static void setOtpHash(String otp)
            {
            Constant.SavePersistent("OTP", otp);
            }

        public static String getOtp()
            {
            return Constant.LoadPersistent<String>(Constant.SETTINGS_USER_PIN);
            }

        public static void setOtp(String otp)
            {
            Constant.SavePersistent(Constant.SETTINGS_USER_PIN, otp);
            }





        public static string getUUID()
            {
            byte[] id = (byte[])Microsoft.Phone.Info.DeviceExtendedProperties.GetValue("DeviceUniqueId");
            string uniqueID = Convert.ToBase64String(id);
            return uniqueID;
            }


        public static async Task<String> postParamsAndgetResponse(string url, FormUrlEncodedContent values)
            {

            string responseFromServer = ""; ;

            CookieContainer cookies = new CookieContainer();
            HttpClientHandler handler = new HttpClientHandler();

            Uri uri = new Uri(url);

            string cookieName = LoadPersistent<string>("allCookie");
            //string cookie_value = LoadPersistent<string> ("cookieValue");
            if(cookieName != null)
                cookies= getList(url);

            handler.CookieContainer = cookies;

            try
                {
                HttpClient httpClient = new HttpClient(handler);

                HttpResponseMessage response = await httpClient.PostAsync(url, values);


                var responseCookies = cookies.GetCookies(uri);//.Cast<Cookie> ();
                int ii = 0;
                int count = ii + 1;
                var check = LoadPersistent<string>("cookieName");

                var responseString = await response.Content.ReadAsStringAsync();
                responseFromServer = responseString.ToString();
                //IEnumerable<string> setCookieVal1 = response.Headers.GetValues ("Set-Cookie");
                if((int)PhoneApplicationService.Current.State["loginCall"] == 1)
                    {

                    IEnumerable<string> setCookieVal = response.Headers.GetValues("Set-Cookie");
                    foreach(string cookie in setCookieVal)
                        {
                        SavePersistent("allCookie", cookie);

                        PhoneApplicationService.Current.State["loginCall"] = 2;

                        }
                    }


                }
            catch(Exception sec)
                {
                }
            return responseFromServer;

            }

       

        public static async Task<String> postParamsAndgetResponse(string url)//, FormUrlEncodedContent values)
            {

            var httpClient = new HttpClient(new HttpClientHandler());
            HttpResponseMessage response = await httpClient.GetAsync(url);// PostAsync(url, values);
            //  response.EnsureSuccessStatusCode ();
            var responseString = await response.Content.ReadAsStringAsync();


            return responseString.ToString();
            }

        public static CookieContainer getList(String url)
            {
            String allCookie = LoadPersistent<string>("allCookie");
            string[] cook1 = allCookie.Replace("; path=/,", String.Empty).Split(';');
            CookieContainer list1 = new CookieContainer();
            Uri uri = new Uri(url);
            for(int j = 0;j < cook1.Length;j++)
                {
                int i = 0;
                while((i = cook1[j].IndexOf("PHPSESSID", i)) != -1)
                    {

                    Console.WriteLine(cook1[j].Substring(i));
                    String str = cook1[j].Substring(i);
                    string[] cooki2 = str.Split('=');
                    string name1 = cooki2[0];
                    string value1 = cooki2[1];
                    list1.Add(uri, new Cookie(name1, value1));
                    i++;
                    }


                }

            for(int j = 0;j < cook1.Length;j++)
                {
                int i = 0;
                while((i = cook1[j].IndexOf("b2c.pay1.in", i)) != -1)
                    {


                    String str = cook1[j].Substring(i);
                    string[] cooki2 = str.Split('=');
                    string name1 = cooki2[0];
                    string value1 = cooki2[1];
                    list1.Add(uri, new Cookie(name1, value1));
                    i++;
                    }


                }
            return list1;
            }

        public static Canvas LayoutRoot;
        public static Canvas canvas;
        // public static StackPanel titlePanel;
        public static void laodNavigationBar(StackPanel TitlePanel, Canvas Container, Canvas canvas1, string title)
            {
            //Perform (() => MyMethod (), 2500);
            PhoneApplicationService.Current.State.Remove("operatorData");
            TitlePanel.Children.Clear();
            NavSideBar nav = new NavSideBar();
            //nav.Height = 90;
            nav.textBalance.Text="Rs. "+Constant.getbalance();
            nav.textCoins.Text=Constant.getLoyalityPoints();
            nav.balancePanel.MouseEnter+=(a, b) =>
            {
            (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri("/Views/TopupWalletLayout.xaml?", UriKind.RelativeOrAbsolute));
            };

            nav.coinsPanel.MouseEnter+=(a, b) =>
            {
            (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri("/Views/CoinHistory.xaml?", UriKind.RelativeOrAbsolute));
            };



            canvas = canvas1;
            if(title.Equals("Home") || title.Equals("Misscall Recharge") || title.Equals("My Gifts") || title.Equals("My Transactions") || title.Equals("Cash Topup") || title.Equals("Online Topup") || title.Equals("Redeem Coupon") || title.Equals("Wallet History") || title.Equals("My Profile") || title.Equals("Support") || title.Equals("Terms & Condition") || title.Equals("FAQs") || title.Equals("Privacy Policy"))
                {
                canvas.ManipulationDelta += canvas_ManipulationDelta;
                canvas.ManipulationCompleted += canvas_ManipulationCompleted;
                canvas.ManipulationStarted += canvas_ManipulationStarted;
                }
            var con = Container.Parent;// GetType();
            // titlePanel = TitlePanel;
            nav.textNavBar.Text = title;
            TitlePanel.Children.Add(nav);
            LayoutRoot = Container;
            //nav.expandBtn.MouseLeave += new MouseEventHandler(panel1_MouseLeave1);
            nav.expandBtn.Tap+=(a, b) =>
            {
                nav.textBalance.Text="Rs. "+Constant.getbalance();
                nav.textCoins.Text=Constant.getLoyalityPoints();
                var left = Canvas.GetLeft(LayoutRoot);
                if(left > -100)
                    {

                    MoveViewWindow(-395);
                    }
                else
                    {

                    MoveViewWindow(0);
                    }
            };
            }

        private static void panel1_MouseLeave1(object sender, MouseEventArgs e)
            {
            var left = Canvas.GetLeft(LayoutRoot);
            if(left > -100)
                {

                MoveViewWindow(-395);
                }
            else
                {

                MoveViewWindow(0);
                }
            }


        static void MoveViewWindow(double left)
            {
            _viewMoved = true;
            //MessageBox.Show("open");
            ((Storyboard)canvas.Resources["moveAnimation"]).SkipToFill();
            ((DoubleAnimation)((Storyboard)canvas.Resources["moveAnimation"]).Children[0]).To = left;
            ((Storyboard)canvas.Resources["moveAnimation"]).Begin();
            }

        private static void canvas_ManipulationDelta(object sender, ManipulationDeltaEventArgs e)
            {
            if(e.DeltaManipulation.Translation.X != 0)
                Canvas.SetLeft(LayoutRoot, Math.Min(Math.Max(-395, Canvas.GetLeft(LayoutRoot) + e.DeltaManipulation.Translation.X), 0));

            }


        static double initialPosition;
        static bool _viewMoved = false;
        private static void canvas_ManipulationStarted(object sender, ManipulationStartedEventArgs e)
            {
            _viewMoved = false;

            initialPosition = Canvas.GetLeft(LayoutRoot);
            }

        private static void canvas_ManipulationCompleted(object sender, ManipulationCompletedEventArgs e)
            {
            var left = Canvas.GetLeft(LayoutRoot);

            if(_viewMoved)
                return;
            if(Math.Abs(initialPosition - left) < 100)
                {
                //bouncing back

                MoveViewWindow(initialPosition);
                return;
                }
            //change of state
            if(initialPosition - left > 0)
                {

                //slide to the left
                if(initialPosition > -395)
                    MoveViewWindow(-395);
                else
                    MoveViewWindow(-395);
                }
            else
                {

                //slide to the right
                if(initialPosition < -395)
                    MoveViewWindow(-395);
                else
                    MoveViewWindow(0);
                }



            }


        internal static void sideBarListHandle(int listSideIndex)
            {

            var left = Canvas.GetLeft(LayoutRoot);
            if(left > -100)
                {

                MoveViewWindow(-395);
                }
            else
                {

                MoveViewWindow(0);
                }


            }

        private static async void getAndUpdateBalance()
            {
            var values = new List<KeyValuePair<string, string>>();
            values.Add(new KeyValuePair<string, string>("actiontype", "Check_bal"));

            values.Add(new KeyValuePair<string, string>("api", "true"));
            FormUrlEncodedContent values1 = new FormUrlEncodedContent(values);
            Task<string> response = Constant.postParamsAndgetResponse(Constant.B2C_URL, values1);

            string result = await response;
            if(string.IsNullOrEmpty(result))
                {
                (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri("/Views/NoConnection.xaml?", UriKind.Relative));
                }
            else
                {
                var dict = (JObject)JsonConvert.DeserializeObject(result);
                string status = dict["status"].ToString();
                var description = dict["description"];
                if(status.Equals("success"))
                    {
                    string account_balance = description["account_balance"].ToString();
                    Constant.setbalance(account_balance);
                    }
                }
            }

        private static async void LogOutTask()
            {
            //ProgressBarManager.Show (18, "Please wait...", false, 10);
            var values = new List<KeyValuePair<string, string>>();
            values.Add(new KeyValuePair<string, string>("actiontype", "signout"));

            values.Add(new KeyValuePair<string, string>("api", "true"));
            FormUrlEncodedContent values1 = new FormUrlEncodedContent(values);
            Task<string> response = Constant.postParamsAndgetResponse(Constant.B2C_URL, values1);

            string result = await response;
            if(string.IsNullOrEmpty(result))
                {
                (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri("/Views/NoConnection.xaml?", UriKind.Relative));
                }
            else
                {
                var dict = (JObject)JsonConvert.DeserializeObject(result);
                string status = dict["status"].ToString();
                var description = dict["description"];
                if(status.Equals("success"))
                    {

                    Constant.SavePersistent(Constant.SETTINGS_IS_LOGIN, false);
                    Constant.SavePersistent(Constant.SETTINGS_USER_WALLET_BALANCE, "0.00");

                    Constant.SavePersistent("cookieName", "111");
                    Constant.SavePersistent("cookieValue", "111");
                    (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri("/RegistrationLayout.xaml?", UriKind.Relative));
                    while((Application.Current.RootVisual as PhoneApplicationFrame).BackStack.Count() > 0)
                        {
                        (Application.Current.RootVisual as PhoneApplicationFrame).RemoveBackEntry();
                        }
                    }
                else if(status.Equals("failure"))
                    {
                    Constant.checkForErrorCode(result);
                    }
                }
            //ProgressBarManager.Hide (18);
            }



        public static BitmapImage imageForRating(string id)
            {
            int rating=Convert.ToInt16(id);
            switch(rating)
                {
                case 1:

                    return new BitmapImage(new Uri("/Images/ic_loveit_select.png", UriKind.RelativeOrAbsolute));
                    
                case 2:

                    return new BitmapImage(new Uri("/Images/ic_likeit_select.png", UriKind.RelativeOrAbsolute));
                  
                case 3:

                    return new BitmapImage(new Uri("/Images/ic_justok_select.png", UriKind.RelativeOrAbsolute));
                    
                case 4:

                    return new BitmapImage(new Uri("/Images/ic_poor_select.png", UriKind.RelativeOrAbsolute));
                   
                case 5:

                    return new BitmapImage(new Uri("/Images/ic_itsbad_select.png", UriKind.RelativeOrAbsolute));
                   
                case 0:
                 return   new BitmapImage(new Uri("/Images/love.png", UriKind.RelativeOrAbsolute));
                   
                default:
                    return new BitmapImage(new Uri("/Images/love.png", UriKind.RelativeOrAbsolute));
                  
                }
            }



        internal static void setTypesGiftsOrder(string offerDetails)
            {
            Constant.SavePersistent("offerDetails", offerDetails);
            }

        internal static string getTypesGiftsOrder()
            {
            return Constant.LoadPersistent<string>("offerDetails");
          //  Constant.SavePersistent("offerDetails", offerDetails);
            }


        internal static void setCategoriesGiftsOrder(string offerDetails)
            {
            Constant.SavePersistent("getCategoriesGiftsOrder", offerDetails);
            }

        internal static string getCategoriesGiftsOrder()
            {
            return Constant.LoadPersistent<string>("getCategoriesGiftsOrder");
            //  Constant.SavePersistent("offerDetails", offerDetails);
            }



        }
    }
