//
//  CoinHistoryService.h
//  Pay1
//
//  Created by webninjaz on 09/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "Service.h"

@class CoinHistoryCriteria;
@class CoinHistoryResponse;

//typedef void (^coinHistoryResponse)(NSError *error, CoinHistoryResponse *coinHistoryResponse);
typedef void (^coinHistoryResponse)(NSError *error,CoinHistoryResponse *objCoinHistoryResponse);

@interface CoinHistoryService : Service

//-(void)callCoinHistoryService:(CoinHistoryCriteria *)criteria callback:(coinHistoryResponse)callback;
-(void)callCoinHistoryService:(coinHistoryResponse)callback;

@end
