package com.pay1;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Dialog;

import com.pay1.customviews.MyProgressDialog;

import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.pay1.adapterhandler.InfoAdapter;
import com.pay1.constants.Constants;
import com.pay1.requesthandler.RequestClass;
import com.pay1.utilities.Utility;

public class ProfileInfoActivity extends AppBaseActivity {

	private final String TAG = "Info";

	private ListView listView_Info;
	private ArrayList<String> list;
	private InfoAdapter adapter;
	static int selected_index = -1;

	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.profile_info_activity);
		try {

			registerBaseActivityReceiver();

			listView_Info = (ListView) findViewById(R.id.listView_Info);
			list = new ArrayList<String>();

			list.add("Profile");
			list.add("Purchase history");
			list.add("Quick pay");
			list.add("Support");
			list.add("FAQs");
			if (Utility.getLoginFlag(ProfileInfoActivity.this,
					Constants.SHAREDPREFERENCE_IS_LOGIN))
				list.add("Logout");
			else
				list.add("Login");

			adapter = new InfoAdapter(ProfileInfoActivity.this, list);
			listView_Info.setAdapter(adapter);
			listView_Info
					.setOnItemClickListener(new AdapterView.OnItemClickListener() {

						@Override
						public void onItemClick(AdapterView<?> parent,
								View view, int position, long id) {
							// TODO Auto-generated method stub
							selected_index = position;
							switch (position) {
							case 0:
								if (Utility.getLoginFlag(
										ProfileInfoActivity.this,
										Constants.SHAREDPREFERENCE_IS_LOGIN)) {
									startActivity(new Intent(
											ProfileInfoActivity.this,
											ProfileActivity.class));
								} else {
									Intent intent = new Intent(
											ProfileInfoActivity.this,
											LoginForRechargeActivity.class);
									startActivity(intent);
								}
								break;

							case 1:
								startActivity(new Intent(
										ProfileInfoActivity.this,
										PurchaseHistoryActivity.class));
								break;
							case 2:
								startActivity(new Intent(
										ProfileInfoActivity.this,
										QuickPayActivity.class));
								/*
								 * startActivity(new Intent(
								 * ProfileInfoActivity.this,
								 * SampleQuick.class));
								 */
								break;
							case 3:
								startActivity(new Intent(
										ProfileInfoActivity.this,
										SupportActivity.class));
								break;
							case 4:
								startActivity(new Intent(
										ProfileInfoActivity.this,
										FAQActivity.class));
								break;
							case 5:
								try {
									if (Utility
											.getLoginFlag(
													ProfileInfoActivity.this,
													Constants.SHAREDPREFERENCE_IS_LOGIN)) {
										// new AlertDialog.Builder(
										// ProfileInfoActivity.this)
										// .setTitle("Logout")
										// .setMessage(
										// "Are you sure you want to logout?")
										// .setPositiveButton(
										// android.R.string.yes,
										// new DialogInterface.OnClickListener()
										// {
										// public void onClick(
										// DialogInterface dialog,
										// int which) {
										// this.dialog.dismiss();
										// new LogoutTask()
										// .execute();
										// }
										// })
										// .setNegativeButton(
										// android.R.string.no,
										// new DialogInterface.OnClickListener()
										// {
										// public void onClick(
										// DialogInterface dialog,
										// int which) {
										// this.dialog.dismiss();
										// }
										// })
										// .setIcon(
										// android.R.drawable.ic_dialog_alert)
										// .show();

										try {

											final Dialog dialog = new Dialog(
													ProfileInfoActivity.this);
											dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
											// dialog.getWindow().setBackgroundDrawableResource(
											// android.R.color.transparent);
											dialog.setContentView(R.layout.dialog_success_two_button);
											// dialog.setTitle(null);

											Typeface Reguler = Typeface
													.createFromAsset(
															getAssets(),
															"EncodeSansNormal-400-Regular.ttf");

											Typeface Narrow = Typeface
													.createFromAsset(
															getAssets(),
															"EncodeSansNarrow-500-Medium.ttf");

											// set the custom dialog components
											// - text, image and button
											TextView textView_Title = (TextView) dialog
													.findViewById(R.id.textView_Title);
											textView_Title.setText("logout");
											textView_Title.setTypeface(Reguler);

											TextView textView_Message = (TextView) dialog
													.findViewById(R.id.textView_Message);
											textView_Message
													.setText("Are you sure you want to logout?");
											textView_Message
													.setTypeface(Narrow);

											ImageView imageView_Ok = (ImageView) dialog
													.findViewById(R.id.imageView_Ok);
											// if button is clicked, close the
											// custom dialog
											imageView_Ok
													.setOnClickListener(new View.OnClickListener() {
														@Override
														public void onClick(
																View v) {
															dialog.dismiss();
															new LogoutTask()
																	.execute();

														}
													});

											ImageView imageView_Cancel = (ImageView) dialog
													.findViewById(R.id.imageView_Cancel);
											// if button is clicked, close the
											// custom dialog
											imageView_Cancel
													.setOnClickListener(new View.OnClickListener() {
														@Override
														public void onClick(
																View v) {
															dialog.dismiss();
														}
													});

											dialog.show();

										} catch (Exception exception) {
										}
									} else {
										Intent intent = new Intent(
												ProfileInfoActivity.this,
												LoginActivity.class);
										startActivity(intent);
									}

								} catch (Exception e) {
								}
								break;
							default:
								break;
							}
						}
					});

		} catch (Exception e) {
		}
		overridePendingTransition(R.anim.push_up_in, R.anim.fadeout);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (Constants.isExpanded) {
			Constants.collapsView(this);
		} else {
			Constants.SELECTED_MENU_INDEX = Constants.PROFILE_MENU_INDEX;
			Constants.showNavigationBar(ProfileInfoActivity.this);
			list.clear();
			list.add("Profile");
			list.add("Purchase history");
			list.add("Quick pay");
			list.add("Support");
			list.add("FAQs");
			if (Utility.getLoginFlag(ProfileInfoActivity.this,
					Constants.SHAREDPREFERENCE_IS_LOGIN))
				list.add("Logout");
			else
				list.add("Login");
			ProfileInfoActivity.this.runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					adapter.notifyDataSetChanged();
				}
			});
		}
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unRegisterBaseActivityReceiver();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (Constants.isExpanded) {
			Constants.collapsView(this);
		} else {
			super.onBackPressed();
			overridePendingTransition(R.anim.fadeout, R.anim.push_down_out);
		}
	}

	public class LogoutTask extends AsyncTask<String, String, String> implements
			OnDismissListener {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			try {
				String response = RequestClass.getInstance()
						.readPay1B2CRequest(ProfileInfoActivity.this,
								Constants.B2C_URL + "signout/");

				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			try {

				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONObject jsonObject = new JSONObject(replaced);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {

						Utility.setLoginFlag(ProfileInfoActivity.this,
								Constants.SHAREDPREFERENCE_IS_LOGIN, false);

						Utility.setUserName(ProfileInfoActivity.this,
								Constants.SHAREDPREFERENCE_NAME, null);
						// Utility.setMobileNumber(ProfileInfoActivity.this,
						// Constants.SHAREDPREFERENCE_MOBILE, null);
						Utility.setDOB(ProfileInfoActivity.this,
								Constants.SHAREDPREFERENCE_DOB, null);
						// Utility.setEmail(ProfileInfoActivity.this,
						// Constants.SHAREDPREFERENCE_EMAIL, null);
						Utility.setGender(ProfileInfoActivity.this,
								Constants.SHAREDPREFERENCE_GENDER, null);
						Utility.setUserID(ProfileInfoActivity.this,
								Constants.SHAREDPREFERENCE_USER_ID, null);
						Utility.setBalance(ProfileInfoActivity.this,
								Constants.SHAREDPREFERENCE_BALANCE, null);

						// Constants.showDialog(ProfileInfoActivity.this, TAG,
						// Constants.checkCode(replaced),
						// Constants.DIALOG_CLOSE);
						// Intent intent = new Intent(ProfileInfoActivity.this,
						// LoginActivity.class);
						// // Closing all the Activities
						// intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						// // Add new Flag to start new Activity
						// intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK );
						// startActivity(intent);
						// finish();
						closeAllActivities();

					} else {
						Constants.showOneButtonDialog(ProfileInfoActivity.this,
								TAG, Constants.checkCode(replaced),
								Constants.DIALOG_CLOSE);
					}

				} else {
					// Constants.showOneButtonDialog(ProfileInfoActivity.this,
					// TAG, Constants.ERROR_INTERNET,
					// Constants.DIALOG_CLOSE);
					Intent intent = new Intent(ProfileInfoActivity.this,
							ConnectivityErrorActivity.class);
					intent.putExtra(Constants.RECHARGE_FOR,
							Constants.WALLET_PAYMENT);
					startActivity(intent);

				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonDialog(ProfileInfoActivity.this, TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonDialog(ProfileInfoActivity.this, TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(ProfileInfoActivity.this);
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							LogoutTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			LogoutTask.this.cancel(true);
			dialog.cancel();
		}

	}

}
