package com.pay1.adapterhandler;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.pay1.BillPaymentActivity;
import com.pay1.OrderSummaryActivity;
import com.pay1.OrderSummaryForLowBalActivity;
import com.pay1.R;
import com.pay1.constants.Constants;
import com.pay1.customviews.MyProgressDialog;
import com.pay1.requesthandler.RequestClass;
import com.pay1.utilities.Utility;

public class ItemAdapter extends ArrayAdapter<JSONObject> {

	public List<JSONObject> data;
	Context context;
	int layoutResID;
	Typeface Normal, Reguler, Medium;
	OnClickListener onClickListener;
	public static int pos;
	static String isMisscall = "0";

	public ItemAdapter(Context context, int layoutResourceId,
			List<JSONObject> data, OnClickListener clickListener) {
		super(context, layoutResourceId, data);

		this.data = data;
		this.context = context;
		this.layoutResID = layoutResourceId;
		Normal = Typeface.createFromAsset(context.getAssets(),
				"EncodeSansNormal-100-Thin.ttf");
		Reguler = Typeface.createFromAsset(context.getAssets(),
				"EncodeSansNormal-400-Regular.ttf");
		Medium = Typeface.createFromAsset(context.getAssets(),
				"EncodeSansNarrow-500-Medium.ttf");
		this.onClickListener = clickListener;

	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		NewsHolder holder = null;
		View row = convertView;
		holder = null;

		if (row == null) {
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			row = inflater.inflate(layoutResID, parent, false);

			holder = new NewsHolder();

			holder.itemNamefront = (TextView) row
					.findViewById(R.id.textView_Name_front);
			holder.itemNumberFront = (TextView) row
					.findViewById(R.id.textView_Mobile_front);
			holder.itemOperatorFront = (TextView) row
					.findViewById(R.id.textView_Operator_front);
			holder.textView_Amount = (TextView) row
					.findViewById(R.id.textView_Amount);
			holder.imageView_Misscall = (ImageView) row
					.findViewById(R.id.imageView_Misscall);
			holder.imageView_Info = (ImageView) row
					.findViewById(R.id.imageView_Info);
			holder.imageQuickPay = (ImageView) row
					.findViewById(R.id.imageQuickPay);
			holder.front = (RelativeLayout) row.findViewById(R.id.front);
			holder.swipe_delete = (Button) row.findViewById(R.id.swipe_delete);
			holder.relativeLayout_Image = (RelativeLayout) row
					.findViewById(R.id.relativeLayout_Image);
			holder.imageView_Image = (ImageView) row
					.findViewById(R.id.imageView_Image);

			holder.itemNamefront.setTypeface(Reguler);
			holder.itemNumberFront.setTypeface(Medium);
			holder.itemOperatorFront.setTypeface(Normal);
			holder.textView_Amount.setTypeface(Medium);
			holder.swipe_delete.setTypeface(Normal);

			// setClickListeners(holder.swipe_delete);
			setClickListeners(holder.swipe_delete);
			/*
			 * holder.itemNameback = (TextView) row
			 * .findViewById(R.id.textView_Name_back_back);
			 * holder.itemNumberBack = (TextView) row
			 * .findViewById(R.id.textView_Mobile_back);
			 */
			// holder.swipe_delete.setTag(position);
			holder.swipe_delete.setTag(position);

			row.setTag(holder);
		} else {
			holder = (NewsHolder) row.getTag();
		}
		try {
			final JSONObject itemdata = data.get(position);
			final String flag = itemdata.getString("flag");
			String delegate_flag = itemdata.getString("delegate_flag");
			String confirmation = itemdata.getString("confirmation");
			// final String id = itemdata.getString("id");
			String name = itemdata.getString("name");
			// if (name.equalsIgnoreCase("")) {
			// holder.itemNamefront.setVisibility(View.GONE);
			// } else {
			holder.itemNamefront.setVisibility(View.VISIBLE);
			holder.itemNamefront.setText(name);
			// }
			holder.itemNumberFront.setText(itemdata
					.getString("number"));
			holder.itemOperatorFront.setText(itemdata
					.getString("operator_name"));
			holder.textView_Amount.setText("Rs: "
					+ itemdata.getString("amount"));
			if (delegate_flag.equalsIgnoreCase("1")) {
				holder.imageView_Misscall
						.setImageResource(R.drawable.misscallcolor);
			} else {
				holder.imageView_Misscall.setImageResource(R.drawable.misscall);
			}

			if (confirmation.equalsIgnoreCase("1")) {
				holder.imageView_Info.setImageResource(R.drawable.alertcolor);
			} else {
				holder.imageView_Info.setImageResource(R.drawable.alert);
			}

			int service_id = 1;
			try {
				service_id = Integer.parseInt(flag);
			} catch (Exception e) {
				service_id = 1;
			}
			switch (service_id) {
			case 1:
				holder.relativeLayout_Image.setBackgroundColor(context
						.getResources().getColor(R.color.app_green_color));
				holder.imageView_Image.setImageResource(R.drawable.wal_mobile);
				holder.imageView_Misscall.setVisibility(View.VISIBLE);
				break;
			case 2:
				holder.relativeLayout_Image.setBackgroundColor(context
						.getResources().getColor(R.color.app_green_color));
				holder.imageView_Image.setImageResource(R.drawable.wal_dth);
				holder.imageView_Misscall.setVisibility(View.INVISIBLE);
				break;
			case 3:
				holder.relativeLayout_Image.setBackgroundColor(context
						.getResources().getColor(R.color.app_green_color));
				holder.imageView_Image
						.setImageResource(R.drawable.wal_datacard);
				holder.imageView_Misscall.setVisibility(View.INVISIBLE);
				break;
			case 4:
				holder.relativeLayout_Image.setBackgroundColor(context
						.getResources().getColor(R.color.app_skyblue_color));
				holder.imageView_Image
						.setImageResource(R.drawable.wal_postpaid);
				holder.imageView_Misscall.setVisibility(View.INVISIBLE);
				holder.textView_Amount.setText("-");
				break;
			}

			holder.imageView_Misscall
					.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							pos = position;
							try {
								// if (itemdata.getString("delegate_flag")
								// .equalsIgnoreCase("1")) {
								showTwoButtonDialogForMisscall(context,
										"Set Missedcall", "", 1, itemdata);
								/*
								 * Toast.makeText( context,
								 * "Misscall number for this number is: " +
								 * itemdata .getString("missed_number"),
								 * Toast.LENGTH_LONG).show();
								 */
								// } else {
								// showTwoButtonDialogForMisscall(context,
								// "Set Missedcall",
								// "You dont have any misscall number.", 1,
								// itemdata);
								// }
							} catch (Exception e) {
							}
						}
					});
			holder.imageQuickPay.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					try {

						double rechargeAmount = 0, balance = 0;
						try {
							rechargeAmount = Double.parseDouble(data
									.get(position).getString("amount").trim());
							balance = Double.parseDouble(Utility
									.getBalance(context,
											Constants.SHAREDPREFERENCE_BALANCE));
						} catch (Exception e) {
							rechargeAmount = 0;
							balance = 0;
						}

						if (flag.equalsIgnoreCase("1")
								|| flag.equalsIgnoreCase("2")
								|| flag.equalsIgnoreCase("3")) {
							if (rechargeAmount > balance) {
								Intent intent = new Intent(context,
										OrderSummaryForLowBalActivity.class);
								intent.putExtra(Constants.RECHARGE_FOR,
										Constants.QUICK_PAY);
								intent.putExtra(Constants.QUICKPAY_DATA, data
										.get(position).toString());
								context.startActivity(intent);
							} else {
								Intent intent = new Intent(context,
										OrderSummaryActivity.class);
								intent.putExtra(Constants.RECHARGE_FOR,
										Constants.QUICK_PAY);
								intent.putExtra(Constants.QUICKPAY_DATA, data
										.get(position).toString());
								context.startActivity(intent);
							}
						} else if (flag.equalsIgnoreCase("4")) {
							Intent intent = new Intent(context,
									BillPaymentActivity.class);
							intent.putExtra(Constants.RECHARGE_FOR,
									Constants.QUICK_PAY);
							intent.putExtra(Constants.QUICKPAY_DATA,
									data.get(position).toString());
							context.startActivity(intent);
						}

					} catch (Exception e) {
					}
				}
			});

			/*
			 * holder.swipe_delete.setOnClickListener(new OnClickListener() {
			 * 
			 * @Override public void onClick(View v) { // TODO Auto-generated
			 * method stub pos = position; new DeleteQuickTask().execute(id); }
			 * });
			 */
			/*
			 * holder.swipe_edit.setOnClickListener(new OnClickListener() {
			 * 
			 * @Override public void onClick(View v) { Intent intent = new
			 * Intent(context, EditQuickPayActivity.class);
			 * intent.putExtra(Constants.EDIT_DATA, data.get(position)
			 * .toString()); context.startActivity(intent); } });
			 */

			// holde

			/*
			 * holder.itemNameback.setText(data.get(position).get("name"));
			 * holder.itemNumberBack.setText(data.get(position).get("number"));
			 */
		} catch (JSONException je) {
		} catch (Exception e) {
		}
		return row;

	}

	public void showTwoButtonDialogForMisscall(final Context context,
			String Title, String message, final int Type,
			final JSONObject itemdata) {
		try {

			final Dialog dialog = new Dialog(context);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			// dialog.getWindow().setBackgroundDrawableResource(
			// android.R.color.transparent);
			dialog.setContentView(R.layout.dialog_success_for_misscall);
			// dialog.setTitle(null);

			// set the custom dialog components - text, image and button
			Typeface Reguler = Typeface.createFromAsset(context.getAssets(),
					"EncodeSansNormal-400-Regular.ttf");

			Typeface Narrow = Typeface.createFromAsset(context.getAssets(),
					"EncodeSansNarrow-500-Medium.ttf");

			// set the custom dialog components - text, image and button
			TextView textView_Title = (TextView) dialog
					.findViewById(R.id.textView_Title);
			textView_Title.setText(Title);
			textView_Title.setTypeface(Reguler);

			TextView textView_Message = (TextView) dialog
					.findViewById(R.id.textView_Message);
			textView_Message.setText(message);
			textView_Message.setTypeface(Narrow);

			TextView textMisscall = (TextView) dialog
					.findViewById(R.id.textMisscall);

			final ToggleButton toggleMissCall = (ToggleButton) dialog
					.findViewById(R.id.toggleMissCall);
			try {

				if (itemdata.getString("delegate_flag").equalsIgnoreCase("1")) {
					toggleMissCall.setChecked(true);
					textMisscall.setTextSize(15f);

					textMisscall.setText("Request "
							+ itemdata.getString("number")
							+ " to give missedcall on "
							+ itemdata.getString("missed_number")
							+ " to recharge.");
				} else {
					toggleMissCall.setChecked(false);
					textMisscall.setTextSize(12f);
					textMisscall
							.setText("Turn on to activate missed call recharge service for "
									+ itemdata.getString("number")
									+ " of Rs. "
									+ itemdata.getString("amount" + "") + ".");
				}
			} catch (JSONException je) {

			}

			ImageView imageView_Ok = (ImageView) dialog
					.findViewById(R.id.imageView_Ok);
			// if button is clicked, close the custom dialog

			toggleMissCall.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (toggleMissCall.isChecked()) {
						isMisscall = "1";
					} else {
						isMisscall = "0";
					}
				}
			});

			imageView_Ok.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {

					dialog.dismiss();
					// new AddQuickPayTask().execute();
					try {
						new AddQuickPayTask().execute(
								itemdata.getString("name"),
								itemdata.getString("number"),
								itemdata.getString("amount"),
								itemdata.getString("operator_id"),
								itemdata.getString("flag"), "0",
								itemdata.getString("id"),
								itemdata.getString("stv"));
					} catch (JSONException e) {
					}
				}
			});

			ImageView imageView_Cancel = (ImageView) dialog
					.findViewById(R.id.imageView_Cancel);
			// if button is clicked, close the custom dialog
			imageView_Cancel.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
				}
			});

			dialog.show();

		} catch (Exception exception) {
		}
	}

	private void setClickListeners(View view) {
		view.setOnClickListener(onClickListener);
	}

	static class NewsHolder {

		TextView itemNamefront, itemNumberFront, itemOperatorFront,
				textView_Amount;// ,
		// itemNameback,
		// itemNumberBack;
		RelativeLayout front, relativeLayout_Image;
		// FrameLayout main_layout;
		ImageView imageView_Misscall, imageView_Info, imageQuickPay,
				imageView_Image;
		LinearLayout backLayout;
		Button swipe_delete;

	}

	public class AddQuickPayTask extends AsyncTask<String, String, String>
			implements OnDismissListener {
		private MyProgressDialog dialog;
		String url;

		@Override
		protected String doInBackground(String... params) {
			ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();
			// listValuePair.add(new BasicNameValuePair("actiontype",
			// "recharge"));

			listValuePair.add(new BasicNameValuePair("name", params[0]));

			listValuePair.add(new BasicNameValuePair("number", params[1]));
			listValuePair.add(new BasicNameValuePair("amount", params[2]));
			listValuePair.add(new BasicNameValuePair("operator_id", params[3]));
			listValuePair.add(new BasicNameValuePair("flag", params[4]));

			listValuePair
					.add(new BasicNameValuePair("confirmation", params[5]));
			listValuePair.add(new BasicNameValuePair("id", params[6]));
			listValuePair.add(new BasicNameValuePair("stv", params[7]));
			listValuePair.add(new BasicNameValuePair("delegate_flag",
					isMisscall));

			url = Constants.B2C_URL + "edit_quickpay";
			String response = RequestClass.getInstance().readPay1B2CRequest(
					context, url, listValuePair);

			return response;
		}

		@Override
		protected void onCancelled() {
			// TODO Auto-generated method stub
			super.onCancelled();
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			try {

				if (!result.startsWith("Error")) {

					JSONObject jsonObject = new JSONObject(result);

					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {
						JSONObject description = jsonObject
								.getJSONObject("description");
						String missed_number = description
								.getString("missed_number");
						String msg = description.getString("msg");
						String msgString = "";
						// ItemAdapter.data.get(pos);

						if (missed_number.equalsIgnoreCase("null")
								|| missed_number.equalsIgnoreCase("")) {
							msgString = msg;// +
											// "\nTo recharge this number give misscall to "+
											// missed_number + ".";
						} else {
							msgString = msg
									+ "\nTo recharge this number give misscall to "
									+ missed_number + ".";
						}
						data.get(pos).put("delegate_flag", isMisscall);
						data.get(pos).put("missed_number", missed_number);
						notifyDataSetChanged();
						Constants.showOneButtonDialog(context, "Add Quick Pay",
								msgString, Constants.DIALOG_CLOSE_QUICK_PAY);

					} else {
						Constants.showOneButtonDialog(context, "Add Quick Pay",
								jsonObject.getString("description"),
								Constants.DIALOG_CLOSE_QUICK_PAY);
					}

				} else {
					Constants.showOneButtonDialog(context, "Add Quick Pay",
							Constants.ERROR_INTERNET,
							Constants.DIALOG_CLOSE_ORDER);

				}
			} catch (JSONException e) {

			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(context);
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(false);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							AddQuickPayTask.this.cancel(true);
							dialog.cancel();
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			AddQuickPayTask.this.cancel(true);
			dialog.cancel();
		}
	}

	public class DeleteQuickTask extends AsyncTask<String, String, String>
			implements OnDismissListener {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String url = Constants.B2C_URL + "del_quickpay/?id=" + params[0];
			String response = RequestClass.getInstance().readPay1B2CRequest(
					context, url);
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			if (!result.startsWith("Error")) {

				String status;
				try {
					JSONObject jsonObject = new JSONObject(result);
					status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {
						// String description = jsonObject
						// .getString("description");
						Constants.showOneButtonDialog(context, "Quick Pay",
								jsonObject.getString("description"),
								Constants.DIALOG_CLOSE);
						data.remove(pos);
						notifyDataSetChanged();
					} else {
						Constants.showOneButtonDialog(context, "Quick Pay",
								jsonObject.getString("description"),
								Constants.DIALOG_CLOSE);
					}
				} catch (JSONException e) {
				}
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(context);
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							DeleteQuickTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			DeleteQuickTask.this.cancel(true);
			dialog.cancel();
		}

	}

}
