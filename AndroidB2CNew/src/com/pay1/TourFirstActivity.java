package com.pay1;

import android.content.Intent;
import android.database.SQLException;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.ImageView;

import com.larvalabs.svgandroid.SVGParser;
import com.pay1.constants.Constants;
import com.pay1.databasehandler.FreeBiesDataSource;
import com.pay1.databasehandler.FreeBiesLocationDataSource;

public class TourFirstActivity extends FragmentActivity {

	//private ViewPager pager;
	//private RegistrationPagerAdapter adapter;
	private final int SPLASH_DISPLAY_LENGTH = 4000;
	private FreeBiesDataSource freeBiesDataSource;
	private FreeBiesLocationDataSource freeBiesLocationDataSource;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tour_first_activity);

		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
		// Button buttonContinue = (Button) findViewById(R.id.buttonContinue);
		Typeface Reguler = Typeface.createFromAsset(getAssets(),
				"Roboto-Regular.ttf");
		// buttonContinue.setTypeface(Reguler);
		ImageView splash_image=(ImageView)findViewById(R.id.splash_image);
		splash_image.setImageDrawable(SVGParser.getSVGFromResource(
				TourFirstActivity.this.getResources(),
				R.raw.pay1_new_logo).createPictureDrawable());
		freeBiesDataSource = new FreeBiesDataSource(TourFirstActivity.this);
		freeBiesLocationDataSource = new FreeBiesLocationDataSource(
				TourFirstActivity.this);
		/*try {
			freeBiesDataSource.open();

			FreeBies freebies = freeBiesDataSource.getTopFreeBie();

			if (freebies != null) {
				int days = (int) ((System.currentTimeMillis() - freebies
						.getFreebieUptateTime()) / (1000 * 60 * 60 * 24));
				if (days >= 10) {
					startService(new Intent(TourFirstActivity.this,
							FreeBiesUpdateService.class));
				}
			} else {
				// startService(new Intent(MainActivity.this,
				// FreeBiesService.class));
				new FreeBiesTask().execute();
			}

		} catch (SQLException exception) {
			// TODO: handle exception
			// // // Log.e("Er ", exception.getMessage());
		} catch (Exception e) {
			// TODO: handle exception
			// // // Log.e("Er ", e.getMessage());
		} finally {
			freeBiesDataSource.close();
		}*/
		
		
		
		/*if(Utility.isFirstTimeAfterUpdate(TourFirstActivity.this)){
			
			startService(new Intent(TourFirstActivity.this,
					PlanIntentService.class));
			Utility.saveFirstTimeAfterUpdate(TourFirstActivity.this, false);
		}*/
		
		
		//pager = (ViewPager) findViewById(R.id.pager);

		//adapter = new RegistrationPagerAdapter(this.getSupportFragmentManager());
		//pager.setAdapter(adapter);
		//CirclePageIndicator indicator = (CirclePageIndicator) findViewById(R.id.indicator);
		//indicator.setViewPager(pager);

		//runnable(5);

		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				Intent intent = new Intent(TourFirstActivity.this,
						MainActivity.class);
				intent.putExtra("IS_FLAG",
						getIntent().getExtras().getBoolean("IS_FLAG"));
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
						| Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
				finish();
			}
		}, SPLASH_DISPLAY_LENGTH);

		/*
		 * buttonContinue.setOnClickListener(new OnClickListener() {
		 * 
		 * @Override public void onClick(View v) { // TODO Auto-generated method
		 * stub Intent intent = new Intent(TourFirstActivity.this,
		 * MainActivity.class); intent.putExtra("IS_FLAG",
		 * getIntent().getExtras().getBoolean("IS_FLAG"));
		 * intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
		 * Intent.FLAG_ACTIVITY_NEW_TASK); startActivity(intent); finish(); }
		 * });
		 */

	}

	public void runnable(final int size) {/*

		pager.setCurrentItem(size - 1);
		new CountDownTimer(180000, 3000) {

			public void onTick(long millisUntilFinished) {

				if (pager.getCurrentItem() == size - 1) {
					pager.setCurrentItem(0);
				} else {
					pager.setCurrentItem(pager.getCurrentItem() + 1, true);
				}
			}

			public void onFinish() {

			}
		}.start();
	*/}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// super.onBackPressed();

	}

	public class FreeBiesTask extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... params) {
			try {
				// String response = RequestClass
				// .getInstance()
				// .readPay1B2CRequest(
				// MainActivity.this,
				// Constants.B2C_URL
				// + "Get_all_deal_data/?latitude="
				// + params[0]
				// + "&longitude="
				// + params[1]
				// + "&mobile="
				// + Utility
				// .getMobileNumber(
				// MainActivity.this,
				// Constants.SHAREDPREFERENCE_MOBILE));

				String result = Constants.loadJSONFromAsset(
						TourFirstActivity.this, "gift.json");
				if (!result.startsWith("Error")) {

					try {

						freeBiesDataSource.open();
						freeBiesLocationDataSource.open();

						freeBiesDataSource.deleteFreeBies();
						freeBiesLocationDataSource.deleteFreeBiesLocation();

						long t = System.currentTimeMillis();

						freeBiesDataSource.createFreeBies(result, t);

						// freeBiesLocationDataSource.createFreeBiesLocation(
						// result, t);

						// Utility.setSharedPreferences(MainActivity.this,
						// Constants.LAST_GIFT_UPDATED,
						// String.valueOf(System.currentTimeMillis()));
					} catch (SQLException exception) {
						// TODO: handle exception
						Log.e("Er ", exception.getMessage());
					} catch (Exception e) {
						// TODO: handle exception
						Log.e("Er ", e.getMessage());
					} finally {
						freeBiesDataSource.close();
						freeBiesLocationDataSource.close();
					}
				}
			} catch (Exception e) {

			}
			return "Error";
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

		}

		protected void onPreExecute() {
			super.onPreExecute();
		}

	}

}
