//
//  SignInService.m
//  Pay1
//
//  Created by Annapurna on 12/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "SignInService.h"
#import "SignInCriteria.h"
#import "SignInResponse.h"
#import "OfferDetailList.h"
#import "Pay1TransactionList.h"
#import "Session.h"
@implementation SignInService

-(void)callSignInService:(SignInCriteria *)criteria callback:(signInResponse)callback{
    
    NSMutableDictionary *postData = [[NSMutableDictionary alloc]init];
    [postData setObject:criteria.userMobNum forKey:@"username"];
    [postData setObject:criteria.password forKey:@"password"];
    [postData setObject:criteria.udid forKey:@"uuid"];
    [postData setObject:criteria.userLat forKey:@"latitude"];
    [postData setObject:criteria.userLong forKey:@"longitude"];
    [postData setObject:@"iOS" forKey:@"device_type"];
    
    [self fillDefaultData:postData];

    
    NSLog(@"PostData for SignIn:%@",postData);
    
    
    RequestData *requestData = [[RequestData alloc]init];
    requestData.baseURL = kBaseURL;
    requestData.webServiceURL = kSignInURL;
    requestData.postData = postData;
    requestData.showLoader = FALSE;
    
    requestData.requestSuccessBlock = ^(RequestData *request) {
        
    [self printResponse:request.responseData forRequest:@"SignIn User Response"];
    if ([self handleNullResponse:request])
    return;
        
    SignInResponse *signResponse =  [self parseSignInUserResponse:request];
    [[Session shared] setObjSignInResponse:signResponse];
    callback(nil,signResponse);
   // callback(nil, [self parseSignInUserResponse:request]);
    };
    
    
    requestData.requestFailureBlock = ^(RequestData *request) {
    [self handleNetworkError:request.responseData];
    callback(request.responseData, nil);
    };
    
    [[ServiceManager sharedInstance] makeRequest:requestData];

    
}

-(SignInResponse *)parseSignInUserResponse:(RequestData *)request{
    SignInResponse *signInResponse = [[SignInResponse alloc]init];
    
    signInResponse.status = [request.responseData objectForKey:@"status"];
    NSLog(@"signInResponse.status:%@",signInResponse.status);
    
    if([signInResponse.status isEqualToString:@"failure"]){
        NSLog(@"Error");
        
    signInResponse.signinDesc = [request.responseData objectForKey:@"description"];
    }
    
    
    
    
    if([signInResponse.status isEqualToString:@"success"]){
    NSMutableDictionary *descriptionDic = [request.responseData objectForKey:@"description"];
    signInResponse.userName = [descriptionDic objectForKey:@"name"];
    signInResponse.userMobileNum = [descriptionDic objectForKey:@"mobile"];
    signInResponse.userEmail = [descriptionDic objectForKey:@"email"];
    signInResponse.userGender = [descriptionDic objectForKey:@"gender"];
    signInResponse.userDOB = [descriptionDic objectForKey:@"date_of_birth"];
    signInResponse.userID = [descriptionDic objectForKey:@"user_id"];
    signInResponse.userLat = [descriptionDic objectForKey:@"latitude"];
    signInResponse.userLong = [descriptionDic objectForKey:@"longitude"];
    signInResponse.userManufacturer = [descriptionDic objectForKey:@"manufacturer"];
    signInResponse.userAPIVersion = [descriptionDic objectForKey:@"api_version"];
    signInResponse.userType = [descriptionDic objectForKey:@"user_type"];
    signInResponse.created = [descriptionDic objectForKey:@"created"];
    signInResponse.updated = [descriptionDic objectForKey:@"updated"];
    signInResponse.userRole = [descriptionDic objectForKey:@"user_role"];
    signInResponse.userLoyaltyPoints = [descriptionDic objectForKey:@"loyalty_points"];
    signInResponse.userWalletBalance = [descriptionDic objectForKey:@"wallet_balance"];
    signInResponse.serviceChargePercent = [descriptionDic objectForKey:@"service_charge_percent"];
    signInResponse.serviceTaxPercent = [descriptionDic objectForKey:@"service_tax_percent"];
    signInResponse.supportNumber = [descriptionDic objectForKey:@"support_number"];


    NSMutableDictionary *errorDic = [descriptionDic objectForKey:@"ErrorMsg"];
    signInResponse.billPaymentFailureInfo = [errorDic objectForKey:@"BILLPAYMENT_FAILURE"];
    signInResponse.rechargeFailureInfo = [errorDic objectForKey:@"RECHARGE_FAILURE"];
    signInResponse.walletReffiledRetailer = [errorDic objectForKey:@"WALLET_REFILLED_RETAILER"];
    
    signInResponse.userVal = [descriptionDic objectForKey:@"user_val"];
    signInResponse.userValName = [descriptionDic objectForKey:@"user_val_name"];
    signInResponse.totalGifts = [descriptionDic objectForKey:@"total_gifts"];
    signInResponse.totalRedeem = [descriptionDic objectForKey:@"total_redeem"];
    signInResponse.totalMyLikes = [descriptionDic objectForKey:@"total_mylikes"];
    signInResponse.totalMyReview = [descriptionDic objectForKey:@"total_myreviews"];
    signInResponse.profileImageURL = [descriptionDic objectForKey:@"profile_image"];

    NSMutableDictionary *dealsOfferDic = [descriptionDic objectForKey:@"Deals_offer"];
    NSMutableArray *offerDetailArray = [dealsOfferDic valueForKey:@"offer_details"];
    for(NSMutableDictionary *dic in offerDetailArray){
    OfferDetailList *objDetailList = [[OfferDetailList alloc]init];
    objDetailList.featured = [dic objectForKey:@"featured"];
    objDetailList.name = [dic objectForKey:@"name"];
    objDetailList.idVal = [dic objectForKey:@"id"];
    objDetailList.details = [dic objectForKey:@"details"];
        
    [signInResponse.offerDetailArray addObject:objDetailList];
    }
        
        
    signInResponse.offerLikes = [dealsOfferDic objectForKey:@"offer_likes"];
        
    NSMutableArray *pay1TransArray = [dealsOfferDic valueForKey:@"recent_pay1_transactions"];
    for(NSMutableDictionary *dic in pay1TransArray){
    Pay1TransactionList *objPay1TransList = [[Pay1TransactionList alloc]init];
    objPay1TransList.transID = [dic objectForKey:@"id"];
    objPay1TransList.flag = [dic objectForKey:@"flag"];
    objPay1TransList.name = [dic objectForKey:@"name"];
    objPay1TransList.amount = [dic objectForKey:@"amount"];
    objPay1TransList.userID = [dic objectForKey:@"user_id"];
    objPay1TransList.stv = [dic objectForKey:@"stv"];
    objPay1TransList.operatorID = [dic objectForKey:@"operator_id"];
    objPay1TransList.product = [dic objectForKey:@"product"];
    objPay1TransList.transNumber = [dic objectForKey:@"number"];
    objPay1TransList.delegateFlag = [dic objectForKey:@"delegate_flag"];
    objPay1TransList.missedNumber = [dic objectForKey:@"missed_number"];
    objPay1TransList.confirmation = [dic objectForKey:@"confirmation"];
    objPay1TransList.operatorName = [dic objectForKey:@"operator_name"];
    objPay1TransList.transactionFlag = [dic objectForKey:@"transaction_flag"];
    objPay1TransList.operatorCode = [dic objectForKey:@"operator_code"];
    objPay1TransList.dateTime = [dic objectForKey:@"datetime"];

    [signInResponse.recentPay1TransactionArray addObject:objPay1TransList];
    }
   
    NSLog(@"SignInResponse :%@ %@ %@ %@",signInResponse.recentPay1TransactionArray,signInResponse.offerDetailArray,signInResponse.userID,signInResponse.totalMyLikes);

    }
    
    
    return signInResponse;
}


@end
