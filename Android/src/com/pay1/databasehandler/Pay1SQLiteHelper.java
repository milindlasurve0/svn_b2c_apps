package com.pay1.databasehandler;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Pay1SQLiteHelper extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = "pay1b2cdatabase.db";
	private static final int DATABASE_VERSION = 2;

	public static final String TABLE_ENTERTAINMENT = "EntertainmentTable";
	public static final String TABLE_MOST_VISITED_OPERATOR = "recentlyVisited";

	public static final String ENTERTAINMENT_ID = "e_id";
	public static final String ENTERTAINMENT_PRODUCT_ID = "product_id";
	public static final String ENTERTAINMENT_PRODUCT_CODE = "product_code";
	public static final String ENTERTAINMENT_PRODUCT_NAME = "name";
	public static final String ENTERTAINMENT_PRODUCT_PRICE = "price";
	public static final String ENTERTAINMENT_PRODUCT_VALIDITY = "validity";
	public static final String ENTERTAINMENT_PRODUCT_SHORT_DESCRIPTION = "shortDesc";
	public static final String ENTERTAINMENT_PRODUCT_LONG_DESCRIPTION = "longDesc";
	public static final String ENTERTAINMENT_PRODUCT_IMAGE = "image";
	public static final String ENTERTAINMENT_PRODUCT_DATA_FIELD = "field";
	public static final String ENTERTAINMENT_PRODUCT_DATA_TYPE = "type";
	public static final String ENTERTAINMENT_PRODUCT_DATA_LENGTH = "length";
	public static final String ENTERTAINMENT_PRODUCT_UPDATE_TIME = "time";

	public static final String TABLE_NOTIFICATION = "NotificationTable";

	public static final String NOTIFICATION_ID = "n_id";
	public static final String NOTIFICATION_DATA = "data";
	public static final String NOTIFICATION_TYPE = "type";
	public static final String NOTIFICATION_TIME = "time";

	public static final String TABLE_PLAN = "PlanTable";

	public static final String PLAN_ID = "p_id";
	public static final String PLAN_OPERATOR_ID = "operator_id";
	public static final String PLAN_OPERATOR_NAME = "operator_name";
	public static final String PLAN_CIRCLE_ID = "circle_id";
	public static final String PLAN_CIRCLE_NAME = "circle_name";
	public static final String PLAN_TYPE = "plan_type";
	public static final String PLAN_AMOUNT = "amount";
	public static final String PLAN_VALIDITY = "validity";
	public static final String PLAN_DESCRIPTION = "description";
	public static final String PLAN_UPDATE_TIME = "time";

	public static final String TABLE_VMN = "VMNTable";

	public static final String VMN_ID = "v_id";
	public static final String VMN_NUMBER = "vmn_number";

	// public static final String OPERATOR_ID = "op_id";
	// public static final String OPERATOR_NAME = "op_name";
	// public static final String OPERATOR_CODE = "op_code";
	public static final String RECHARGE_TYPE = "rechargeType";
	public static final String RECHARGE_COUNT = "rechargeCount";

	public static final String TABLE_ADDRESS = "AddressTable";

	public static final String ADDRESS_ID = "address_id";
	public static final String ADDRESS_LINE = "address_line";
	public static final String ADDRESS_AREA = "address_area";
	public static final String ADDRESS_CITY = "address_city";
	public static final String ADDRESS_STATE = "address_state";
	public static final String ADDRESS_ZIP = "address_zip";
	public static final String ADDRESS_LATITUDE = "address_latitude";
	public static final String ADDRESS_LONGITUDE = "address_longitude";

	public static final String TABLE_CHATHISTORY = "ChatHistoryTable";

	public static final String CHATHISTORY_ID = "history_id";
	public static final String CHATHISTORY_TEXT = "history_text";
	public static final String CHATHISTORY_TIME = "history_time";
	public static final String CHATHISTORY_POSITION = "history_position";

	public static final String TABLE_SUPPORT = "SupportTable";

	public static final String SUPPORT_ID = "SUPPORT_ID";
	public static final String SUPPORT_ISSENT = "SUPPORT_ISSENT";
	public static final String SUPPORT_QUERY = "SUPPORT_QUERY";
	public static final String SUPPORT_TIME = "SUPPORT_TIME";

	public static final String TABLE_FAQ = "FAQTable";

	public static final String FAQ_ID = "FAQ_ID";
	public static final String FAQ_QUESTION = "FAQ_QUESTION";
	public static final String FAQ_ANSWER = "FAQ_ANSWER";
	public static final String FAQ_TIME = "FAQ_TIME";

	public static final String TABLE_OPERATOR = "OperatorTable";

	public static final String O_ID = "oID";
	public static final String OPERATOR_ID = "operatorID";
	public static final String OPERATOR_CODE = "operatorCode";
	public static final String OPERATOR_PRODUCT_ID = "opratorProductID";
	public static final String OPERATOR_NAME = "operatorName";
	public static final String OPERATOR_SERVICE_ID = "operatorServiceID";
	public static final String OPERATOR_FLAG = "opratorFlag";
	public static final String OPERATOR_DELEGATE = "operatorDelegate";
	public static final String OPERATOR_STV = "operatorSTV";
	public static final String OPERATOR_TYPE = "operatorType";
	public static final String OPERATOR_TIME = "operatorTime";
	public static final String OPERATOR_MAX = "operatorMax";
	public static final String OPERATOR_MIN = "operatorMin";
	public static final String OPERATOR_CHARGES_SLAB = "operatorChargesSlab";
	public static final String OPERATOR_SERVICE_CHARGE_AMOUNT = "operatorServiceChargeAmount";
	public static final String OPERATOR_SERVICE_TAX_PERCENT = "operatorServiceTaxPercent";
	public static final String OPERATOR_SERVICE_CHARGE_PERCENT = "operatorServiceChargePercent";

	private final String CREATE_ENTERTAINMENT = "CREATE TABLE "
			+ TABLE_ENTERTAINMENT + " ( " + ENTERTAINMENT_ID
			+ " integer primary key autoincrement, " + ENTERTAINMENT_PRODUCT_ID
			+ " text, " + ENTERTAINMENT_PRODUCT_CODE + " text, "
			+ ENTERTAINMENT_PRODUCT_NAME + " text, "
			+ ENTERTAINMENT_PRODUCT_PRICE + " text, "
			+ ENTERTAINMENT_PRODUCT_VALIDITY + " text, "
			+ ENTERTAINMENT_PRODUCT_SHORT_DESCRIPTION + " text, "
			+ ENTERTAINMENT_PRODUCT_LONG_DESCRIPTION + " text,"
			+ ENTERTAINMENT_PRODUCT_IMAGE + " text, "
			+ ENTERTAINMENT_PRODUCT_DATA_FIELD + " text, "
			+ ENTERTAINMENT_PRODUCT_DATA_TYPE + " text, "
			+ ENTERTAINMENT_PRODUCT_DATA_LENGTH + " text, "
			+ ENTERTAINMENT_PRODUCT_UPDATE_TIME + " text );";

	private final String CREATE_NOTIFICATION = "CREATE TABLE "
			+ TABLE_NOTIFICATION + " ( " + NOTIFICATION_ID
			+ " integer primary key autoincrement, " + NOTIFICATION_DATA
			+ " text, " + NOTIFICATION_TYPE + " integer, " + NOTIFICATION_TIME
			+ " text );";

	private final String CREATE_PLAN = "CREATE TABLE " + TABLE_PLAN + " ( "
			+ PLAN_ID + " integer primary key autoincrement, "
			+ PLAN_OPERATOR_ID + " text, " + PLAN_OPERATOR_NAME + " text, "
			+ PLAN_CIRCLE_ID + " text, " + PLAN_CIRCLE_NAME + " text, "
			+ PLAN_TYPE + " text, " + PLAN_AMOUNT + " integer, "
			+ PLAN_VALIDITY + " text, " + PLAN_DESCRIPTION + " text, "
			+ PLAN_UPDATE_TIME + " text );";

	private final String CREATE_VMN = "CREATE TABLE " + TABLE_VMN + " ( "
			+ VMN_ID + " integer primary key, " + VMN_NUMBER + " text );";

	private final String MOST_VISITED = "CREATE TABLE "
			+ TABLE_MOST_VISITED_OPERATOR + " ( " + OPERATOR_ID
			+ " integer primary key, " + OPERATOR_NAME + " text, "
			+ OPERATOR_CODE + " text, " + RECHARGE_TYPE + " text, "
			+ RECHARGE_COUNT + " integer );";

	private final String CREATE_ADDRESS = "CREATE TABLE " + TABLE_ADDRESS
			+ " ( " + ADDRESS_ID + " integer primary key autoincrement, "
			+ ADDRESS_LINE + " text, " + ADDRESS_AREA + " text, "
			+ ADDRESS_CITY + " text, " + ADDRESS_STATE + " text, "
			+ ADDRESS_ZIP + " text, " + ADDRESS_LATITUDE + " text, "
			+ ADDRESS_LONGITUDE + " text);";

	private final String CREATE_CHATHISTORY = "CREATE TABLE "
			+ TABLE_CHATHISTORY + " ( " + CHATHISTORY_ID
			+ " integer primary key autoincrement, " + CHATHISTORY_TEXT
			+ " text, " + CHATHISTORY_TIME + " text, " + CHATHISTORY_POSITION
			+ " text );";

	private final String CREATE_SUPPORT = "CREATE TABLE " + TABLE_SUPPORT
			+ " ( " + SUPPORT_ID + " integer primary key autoincrement, "
			+ SUPPORT_ISSENT + " integer, " + SUPPORT_QUERY + " text, "
			+ SUPPORT_TIME + " integer );";

	private final String CREATE_FAQ = "CREATE TABLE " + TABLE_FAQ + " ( "
			+ FAQ_ID + " integer primary key autoincrement, " + FAQ_QUESTION
			+ " text, " + FAQ_ANSWER + " text, " + FAQ_TIME + " integer );";

	/*
	 * private final String CREATE_OPERATOR = "CREATE TABLE " + TABLE_OPERATOR +
	 * " ( " + O_ID + " integer primary key autoincrement, " + OPERATOR_ID +
	 * " integer, " + OPERATOR_CODE + " text, " + OPERATOR_PRODUCT_ID +
	 * " integer, " + OPERATOR_NAME + " text, " + OPERATOR_SERVICE_ID +
	 * " integer, " + OPERATOR_FLAG + " integer, " + OPERATOR_DELEGATE +
	 * " integer, " + OPERATOR_STV + " integer, " + OPERATOR_TYPE + " integer, "
	 * + OPERATOR_TIME + " integer );";
	 */

	private final String CREATE_OPERATOR = "CREATE TABLE " + TABLE_OPERATOR
			+ " ( " + O_ID + " integer primary key autoincrement, "
			+ OPERATOR_ID + " integer, " + OPERATOR_CODE + " text, "
			+ OPERATOR_PRODUCT_ID + " integer, " + OPERATOR_NAME + " text, "
			+ OPERATOR_SERVICE_ID + " integer, " + OPERATOR_FLAG + " integer, "
			+ OPERATOR_DELEGATE + " integer, " + OPERATOR_STV + " integer, "
			+ OPERATOR_TYPE + " integer, " + OPERATOR_TIME + " integer, "
			+ OPERATOR_MAX + " integer, " + OPERATOR_MIN + " integer, "
			+ OPERATOR_CHARGES_SLAB + " text, "
			+ OPERATOR_SERVICE_CHARGE_AMOUNT + " integer, "
			+ OPERATOR_SERVICE_TAX_PERCENT + " integer, "
			+ OPERATOR_SERVICE_CHARGE_PERCENT + " integer );";

	public Pay1SQLiteHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		// TODO Auto-generated constructor stub

	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		// db.execSQL(CREATE_ENTERTAINMENT);
		// db.execSQL(CREATE_NOTIFICATION);
		db.execSQL(CREATE_PLAN);
		// db.execSQL(CREATE_VMN);
		// db.execSQL(CREATE_ADDRESS);
		// db.execSQL(CREATE_CHATHISTORY);
		// db.execSQL(MOST_VISITED);
		db.execSQL(CREATE_SUPPORT);
		db.execSQL(CREATE_FAQ);
		db.execSQL(CREATE_OPERATOR);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_ENTERTAINMENT);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NOTIFICATION);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_PLAN);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_VMN);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_ADDRESS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_CHATHISTORY);
		db.execSQL("DROP TABLE IF EXISTS " + MOST_VISITED);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_SUPPORT);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_FAQ);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_OPERATOR);
		onCreate(db);
	}

}
