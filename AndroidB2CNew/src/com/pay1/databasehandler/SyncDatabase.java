package com.pay1.databasehandler;

import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class SyncDatabase {

	private SQLiteDatabase database;
	private Pay1SyncDatabaseHelper dbHelper;
	private Context dbContext;
	
	public SyncDatabase(Context context) {
		dbHelper = new Pay1SyncDatabaseHelper(context);
		dbContext = context;
	}

	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
		Log.e("sync database", "opened for writing");
	}

	public void close() {
		dbHelper.close();
		Log.e("sync database", "closed");
	}
	
	public boolean cloneDatabase(String inputDBName, String outputDBName) {
		return dbHelper.cloneDatabase(inputDBName, outputDBName);
	}
	
	public void updatePlansTime(String time){
		try{
			database.execSQL("	update " + Pay1SyncDataHelper.TABLE_PLAN 
							+ " set " + Pay1SyncDataHelper.PLAN_UPDATE_TIME + " = '" + time + "'");
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public boolean savePlansTodataBase(String planJson) {
		try {
			JSONObject planJsonObject = new JSONObject(planJson);
			Iterator<String> operatorKey = planJsonObject.keys();
			long time = System.currentTimeMillis();
			open();
//			Log.e("Plans JsonObject", planJsonObject.toString());
			while(operatorKey.hasNext()) {
				
				JSONObject jsonObject2 = planJsonObject
						.getJSONObject(operatorKey.next());

				String opr_name = jsonObject2.getString("opr_name");
				String prod_code_pay1 = jsonObject2.getString("prod_code_pay1");

				JSONObject circleJsonObject = jsonObject2.getJSONObject("circles");
				Iterator<String> circleKey = circleJsonObject.keys();
				while(circleKey.hasNext()) {
					JSONObject circleJsonObject2 = circleJsonObject
							.getJSONObject(circleKey.next().toString());
					String circleName = circleJsonObject2
							.getString("circle_name");

					JSONObject plansJsonObject = circleJsonObject2
							.getJSONObject("plans");
					String circle_id = circleJsonObject2.getString("circle_id");
					Iterator<String> planKey = plansJsonObject.keys();
					Iterator<String> planKeyDB = plansJsonObject.keys();
					
					while (planKey.hasNext()) {
						String planType = planKey.next().toString();
						JSONArray planArray = plansJsonObject
								.getJSONArray(planType);

						for (int i = 0; i < planArray.length(); i++) {
							JSONObject jsonObject = planArray.getJSONObject(i);
							String plan_desc = jsonObject.getString("plan_desc");

							String plan_amt = jsonObject.getString("plan_amt");

							String plan_validity = jsonObject.getString("plan_validity");
							String show_flag = jsonObject.getString("show_flag");
							
							deleteAndInsertPlan(prod_code_pay1, opr_name,
									circle_id, circleName, planType, plan_amt,
									plan_validity, plan_desc, "" + time,show_flag);
						}

					}
					
				}

			}
			updatePlansTime(String.valueOf(time));
			close();
			return true;
		} catch (JSONException exception) {
			exception.printStackTrace();
			Log.e("Er ", exception.getMessage());
		} catch (SQLException exception) {
			Log.e("Er ", exception.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			
		}
		return false;
	}
	
	public void createPlan(String OperatorID, String OperatorName,
			String CircleID, String CircleName, String PlanType, String Amount,
			String Validity, String Description, String Time,String showFlag) {
		
		try{
			ContentValues values = new ContentValues();
			values.put(Pay1SyncDataHelper.PLAN_OPERATOR_ID, OperatorID);
			values.put(Pay1SyncDataHelper.PLAN_OPERATOR_NAME, OperatorName);
			values.put(Pay1SyncDataHelper.PLAN_CIRCLE_ID, CircleID);
			values.put(Pay1SyncDataHelper.PLAN_CIRCLE_NAME, CircleName);
			values.put(Pay1SyncDataHelper.PLAN_TYPE, PlanType);
			values.put(Pay1SyncDataHelper.PLAN_AMOUNT, Amount);
			values.put(Pay1SyncDataHelper.PLAN_VALIDITY, Validity);
			values.put(Pay1SyncDataHelper.PLAN_DESCRIPTION, Description);
			values.put(Pay1SyncDataHelper.PLAN_UPDATE_TIME, Time);
			values.put(Pay1SyncDataHelper.PLAN_SHOW_FLAG, showFlag);
			
			database.insert(Pay1SyncDataHelper.TABLE_PLAN, null, values);
			Log.d("Plan", "Plan After Insert "+values);
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void deleteAndInsertPlan(String OperatorID, String OperatorName,
			String CircleID, String CircleName, String PlanType, String Amount,
			String Validity, String Description, String Time,String showFlag){
		try{
			database.execSQL("	delete from " + Pay1SyncDataHelper.TABLE_PLAN + " where "
								+ Pay1SyncDataHelper.PLAN_OPERATOR_ID + " = '" + OperatorID
								+ "' and " + Pay1SyncDataHelper.PLAN_CIRCLE_ID + " = '" + CircleID
								+ "' and " + Pay1SyncDataHelper.PLAN_AMOUNT + " = '" + Amount
								+ "' and " + Pay1SyncDataHelper.PLAN_TYPE + " = '" + PlanType + "'");
			Log.d("Plan", "Plan After Delete");
			
			if(showFlag.equals("1"))
				createPlan(OperatorID, OperatorName, CircleID, CircleName, PlanType, Amount,
					Validity, Description, Time, showFlag);
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void updateCircleTime(String time){
		try{
			database.execSQL("	update " + Pay1SyncDataHelper.TABLE_NUMBERANDCIRCLE 
							+ " set " + Pay1SyncDataHelper.NAC_UPDATE_TIME + " = '" + time + "'");
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public boolean saveCircleNumberToDatabase(JSONObject jsonObject) {
		try {
			open();
			String time = String.valueOf(System.currentTimeMillis());
			String details = jsonObject.getString("details");
			JSONArray detailsArray = new JSONArray(details);
			for (int i = 0; i < detailsArray.length(); i++) {
				JSONObject object = detailsArray.getJSONObject(i);
				String areaName = object.getString("area_name");
				String areaCode = object.getString("area");
				String operatorName = object.getString("opr_name");
				String operatorCode = object.getString("operator");
				String operatorId = object.getString("product_id");
				String number = object.getString("number");
				
				deleteAndInsertNumberAndCircle(areaName, areaCode,
						operatorName, operatorCode, operatorId,
						number, time);
			}
			updateCircleTime(time);
			close();
			return true;
		} catch (JSONException je) {
			Log.d("NAC", "row inserted   " + je.getMessage());
			je.printStackTrace();
		} catch (SQLException exception) {
			Log.d("NAC", "row inserted   " + exception.getMessage());
		} catch (Exception e) {
			Log.d("NAC", "row inserted   " + e.getMessage());

		} finally {

		}
		return false;
	}
	
	public void createNumberAndCircleList(String areaName, String areaCode,
			String operatorName, String operatorCode, String operatorId,
			String number, String timeStamp) {

		ContentValues values = new ContentValues();
		values.put(Pay1SyncDataHelper.NAC_AREA_NAME, areaName);
		values.put(Pay1SyncDataHelper.NAC_AREA_CODE, areaCode);
		values.put(Pay1SyncDataHelper.NAC_OPERATOR_NAME, operatorName);
		values.put(Pay1SyncDataHelper.NAC_OPERATOR_CODE, operatorCode);
		values.put(Pay1SyncDataHelper.NAC_OPERATOR_ID, operatorId);
		values.put(Pay1SyncDataHelper.NAC_MOBILE_NUMBER, number);
		values.put(Pay1SyncDataHelper.NAC_UPDATE_TIME, timeStamp);
		try {
			database.insert(Pay1SyncDataHelper.TABLE_NUMBERANDCIRCLE, null,
					values);
			Log.d("Circle", "Circle After Insert "+values);
		} catch (Exception ee) {
			ee.printStackTrace();
		}
	}
	public void deleteAndInsertNumberAndCircle(String areaName, String areaCode,
			String operatorName, String operatorCode, String operatorId,
			String number, String timeStamp){
		database.execSQL("	delete from " + Pay1SyncDataHelper.TABLE_NUMBERANDCIRCLE + " where "
							+ Pay1SyncDataHelper.NAC_AREA_CODE + " = '" + areaCode 
							+ "' and " + Pay1SyncDataHelper.NAC_MOBILE_NUMBER + " = '" + number +"'");
		Log.d("Circle", "Circle After Delete ");
		createNumberAndCircleList(areaName, areaCode,
				operatorName, operatorCode, operatorId,
				number, timeStamp);
	}
}	