package com.pay1.customviews;

import java.util.Calendar;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableLayout.LayoutParams;
import android.widget.TextView;

import com.pay1.R;
import com.pay1.datehandler.ArrayWheelAdapter;
import com.pay1.datehandler.NumericWheelAdapter;
import com.pay1.datehandler.OnWheelChangedListener;
import com.pay1.datehandler.WheelView;

public class DatePickerDailog extends Dialog {

	private Context Mcontex;

	private int NoOfYear = 100;
	Typeface Normal;

	public DatePickerDailog(Context context, Calendar calendar,
			final DatePickerListner dtp) {

		super(context);
		Mcontex = context;
		LinearLayout lytmain = new LinearLayout(Mcontex);
		lytmain.setOrientation(LinearLayout.VERTICAL);
		LinearLayout lytdate = new LinearLayout(Mcontex);
		LinearLayout lytbutton = new LinearLayout(Mcontex);
		// View view = new View(Mcontex);

		// lytbutton.setBackgroundColor(context.getResources().getColor(
		// R.color.Black));
		Normal = Typeface.createFromAsset(context.getAssets(),
				"EncodeSansNormal-100-Thin.ttf");

		Button btnset = new Button(Mcontex);
		// btnset.setBackgroundResource(R.drawable.button_login_selected);
		btnset.setTypeface(Normal, Typeface.BOLD);
		btnset.setTextSize(20);
		btnset.setTextColor(context.getResources().getColor(R.color.Black));

		Button btncancel = new Button(Mcontex);
		// btncancel.setBackgroundResource(R.drawable.button_login_selected);
		btncancel.setTypeface(Normal, Typeface.BOLD);
		btncancel.setTextSize(20);
		btncancel.setTextColor(context.getResources().getColor(R.color.Black));

		btnset.setText("Set");
		btncancel.setText("Cancel");

		final WheelView month = new WheelView(Mcontex);
		final WheelView year = new WheelView(Mcontex);
		final WheelView day = new WheelView(Mcontex);
		day.setCyclic(true);
		month.setCyclic(true);
		year.setCyclic(true);

		lytdate.addView(day, new LayoutParams(
				android.view.ViewGroup.LayoutParams.FILL_PARENT,
				android.view.ViewGroup.LayoutParams.WRAP_CONTENT, 1.2f));
		lytdate.addView(month, new LayoutParams(
				android.view.ViewGroup.LayoutParams.FILL_PARENT,
				android.view.ViewGroup.LayoutParams.WRAP_CONTENT, 0.8f));
		lytdate.addView(year, new LayoutParams(
				android.view.ViewGroup.LayoutParams.FILL_PARENT,
				android.view.ViewGroup.LayoutParams.WRAP_CONTENT, 1f));
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		android.view.ViewGroup.LayoutParams params = new LayoutParams(
				android.view.ViewGroup.LayoutParams.FILL_PARENT,
				android.view.ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
		LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) params;
		// lp.setMargins(2, 5, 2, 5);
		lytbutton.addView(btnset, params);

		lytbutton.addView(btncancel, params);
		lytbutton.setPadding(5, 5, 5, 5);

		// view.setBackgroundColor(context.getResources().getColor(R.color.Gray));

		lytmain.addView(lytdate);
		// lytmain.addView(view, new LayoutParams(
		// android.view.ViewGroup.LayoutParams.FILL_PARENT, 3));
		lytmain.addView(lytbutton);

		setContentView(lytmain);

		getWindow().setLayout(LayoutParams.FILL_PARENT,
				LayoutParams.WRAP_CONTENT);
		OnWheelChangedListener listener = new OnWheelChangedListener() {
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
				updateDays(year, month, day);

			}
		};

		// month
		int curMonth = calendar.get(Calendar.MONTH);
		String months[] = new String[] { "January", "February", "March",
				"April", "May", "June", "July", "August", "September",
				"October", "November", "December" };
		month.setViewAdapter(new DateArrayAdapter(context, months, curMonth));
		month.setCurrentItem(curMonth);
		month.addChangingListener(listener);

		Calendar cal = Calendar.getInstance();
		// year
		int curYear = calendar.get(Calendar.YEAR);
		int Year = cal.get(Calendar.YEAR);

		year.setViewAdapter(new DateNumericAdapter(context, 1900, Year,
				NoOfYear));
		year.setCurrentItem(curYear - 1900);
		year.addChangingListener(listener);

		// day
		updateDays(year, month, day);
		day.setCurrentItem(calendar.get(Calendar.DAY_OF_MONTH) - 1);

		btnset.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Calendar c = updateDays(year, month, day);
				dtp.OnDoneButton(DatePickerDailog.this, c);
			}
		});
		btncancel.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				dtp.OnCancelButton(DatePickerDailog.this);

			}
		});

	}

	Calendar updateDays(WheelView year, WheelView month, WheelView day) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, 1900 + year.getCurrentItem());
		calendar.set(Calendar.MONTH, month.getCurrentItem());
		calendar.set(Calendar.DATE, 1);
		int maxDays = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
		day.setViewAdapter(new DateNumericAdapter(Mcontex, 1, maxDays, calendar
				.get(Calendar.DAY_OF_MONTH) - 1));
		int curDay = Math.min(maxDays, day.getCurrentItem() + 1);
		day.setCurrentItem(curDay - 1, true);
		calendar.set(Calendar.DAY_OF_MONTH, curDay);
		return calendar;
	}

	private class DateNumericAdapter extends NumericWheelAdapter {
		int currentItem;
		int currentValue;

		public DateNumericAdapter(Context context, int minValue, int maxValue,
				int current) {
			super(context, minValue, maxValue);
			this.currentValue = current;
			setTextSize(25);
		}

		@Override
		protected void configureTextView(TextView view) {
			super.configureTextView(view);
			// if (currentItem == currentValue) {
			// //view.setTypeface(Normal, Typeface.BOLD);
			// }
			view.setTypeface(Normal, Typeface.BOLD);

		}

		@Override
		public View getItem(int index, View cachedView, ViewGroup parent) {
			currentItem = index;
			return super.getItem(index, cachedView, parent);
		}
	}

	private class DateArrayAdapter extends ArrayWheelAdapter<String> {
		int currentItem;
		int currentValue;

		public DateArrayAdapter(Context context, String[] items, int current) {
			super(context, items);
			this.currentValue = current;
			setTextSize(25);
		}

		@Override
		protected void configureTextView(TextView view) {
			super.configureTextView(view);
			// if (currentItem == currentValue) {
			// //view.setTypeface(Normal, Typeface.BOLD);
			// }
			view.setTypeface(Normal, Typeface.BOLD);
		}

		@Override
		public View getItem(int index, View cachedView, ViewGroup parent) {
			currentItem = index;
			return super.getItem(index, cachedView, parent);
		}
	}

	public interface DatePickerListner {
		public void OnDoneButton(Dialog datedialog, Calendar c);

		public void OnCancelButton(Dialog datedialog);
	}
}
