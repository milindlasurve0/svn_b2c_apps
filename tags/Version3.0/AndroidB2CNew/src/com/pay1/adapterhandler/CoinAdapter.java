package com.pay1.adapterhandler;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.larvalabs.svgandroid.SVGParser;
import com.pay1.R;
import com.pay1.constants.Constants;

public class CoinAdapter extends BaseAdapter {

	private Context context;
	private ArrayList<JSONObject> data;
	LayoutInflater inflater;
	Typeface Reguler, Medium;

	public CoinAdapter(Context context, ArrayList<JSONObject> data) {
		this.context = context;
		this.data = data;
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		Reguler = Typeface.createFromAsset(context.getAssets(),
				"Roboto-Regular.ttf");
		Medium = Typeface.createFromAsset(context.getAssets(),
				"Roboto-Regular.ttf");
	}

	@SuppressLint("NewApi")
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final ViewHolder viewHolder;
		// if (convertView == null) {

		convertView = inflater.inflate(R.layout.coin_adapter, null);
		// }
		viewHolder = new ViewHolder();
		viewHolder.textViewCoins = (TextView) convertView
				.findViewById(R.id.textViewCoins);
		viewHolder.textViewTransAmount = (TextView) convertView
				.findViewById(R.id.textViewTransAmount);
		viewHolder.textTransId = (TextView) convertView
				.findViewById(R.id.textTransId);
		viewHolder.textTransDate = (TextView) convertView
				.findViewById(R.id.textTransDate);

		viewHolder.imageView_Image = (ImageView) convertView
				.findViewById(R.id.imageView_Image);
		viewHolder.textViewTransAmount.setTypeface(Medium);
		viewHolder.textTransId.setTypeface(Reguler);
		viewHolder.textTransDate.setTypeface(Reguler);
		viewHolder.textViewCoins.setTypeface(Medium);
		try {

			if (data.get(position).getString("redeem_status")
					.equalsIgnoreCase("1")) {
				viewHolder.textViewCoins.setText(data.get(position).getString(
						"coins")
						+ " coins redeemed");
				viewHolder.textViewCoins.setTextColor(context.getResources().getColor(R.color.app_radio_button_color));
				viewHolder.textViewTransAmount.setText("Grabbed gift \""
						+ data.get(position).getString("Name") + "\"");

				convertView.setTag(viewHolder);
				viewHolder.imageView_Image.setImageDrawable(SVGParser
						.getSVGFromResource(context.getResources(),
								R.raw.ic_coin_minus).createPictureDrawable());
			} else if (data.get(position).getString("redeem_status")
					.equalsIgnoreCase("2")) {
				viewHolder.textViewCoins.setText(data.get(position).getString(
						"coins")
						+ " coins reversed");
				viewHolder.textViewCoins.setTextColor(context.getResources().getColor(R.color.app_radio_button_color));
				viewHolder.textViewTransAmount.setText("Grabbed gift \""
						+ data.get(position).getString("Name") + "\"");

				convertView.setTag(viewHolder);
				viewHolder.imageView_Image.setImageDrawable(SVGParser
						.getSVGFromResource(context.getResources(),
								R.raw.ic_coin_plus).createPictureDrawable());

			} else if (data.get(position).getString("redeem_status")
					.equalsIgnoreCase("3")) {
				viewHolder.textViewCoins.setText(data.get(position).getString(
						"coins")
						+ " coins added");
				viewHolder.textViewCoins.setTextColor(context.getResources().getColor(R.color.app_radio_button_color));

				viewHolder.textViewTransAmount
						.setText(data.get(position).get("rechargeType")+" of Rs. "
								+ data.get(position).getString(
										"transaction_amount") + "\n("
								+ data.get(position).getString("Name") + ")");
				viewHolder.imageView_Image.setImageDrawable(SVGParser
						.getSVGFromResource(context.getResources(),
								R.raw.ic_coin_plus).createPictureDrawable());

			} else if (data.get(position).getString("redeem_status")
					.equalsIgnoreCase("4")) {
				viewHolder.textViewCoins.setText(data.get(position).getString(
						"coins")
						+ " coins reversed");
				viewHolder.textViewCoins.setTextColor(context.getResources().getColor(R.color.app_radio_button_color));
				viewHolder.textViewTransAmount.setText(data.get(position).getString("msg"));

				convertView.setTag(viewHolder);
				viewHolder.imageView_Image.setImageDrawable(SVGParser
						.getSVGFromResource(context.getResources(),
								R.raw.ic_coin_minus).createPictureDrawable());
			} else if (data.get(position).getString("redeem_status")
					.equalsIgnoreCase("5")) {
				viewHolder.textViewCoins.setText(data.get(position).getString(
						"coins")
						+ " coins added");
				viewHolder.textViewCoins.setTextColor(context.getResources().getColor(R.color.app_radio_button_color));
				viewHolder.textViewTransAmount.setText(data.get(position).getString("msg"));
				viewHolder.imageView_Image.setImageDrawable(SVGParser
						.getSVGFromResource(context.getResources(),
								R.raw.ic_coin_plus).createPictureDrawable());

			}else{
				{
					viewHolder.textViewCoins.setText("NA");
					viewHolder.textViewCoins.setTextColor(context.getResources().getColor(R.color.app_radio_button_color));
					viewHolder.textViewTransAmount.setText("NA");
					viewHolder.imageView_Image.setImageDrawable(SVGParser
							.getSVGFromResource(context.getResources(),
									R.raw.ic_coin_plus).createPictureDrawable());

				}
			}

			viewHolder.textTransId.setText("TRANSACTION ID: "
					+ data.get(position).getString("trans_id"));
		/*	if (data.get(position).getString("rechargeType")
					.equalsIgnoreCase("deal")) {
				viewHolder.textViewCoins.setText(data.get(position).getString(
						"coins")
						+ " coins redeemed");
				viewHolder.textViewCoins.setTextColor(Color.RED);
				viewHolder.textViewTransAmount.setText("Pocketed gift \""
						+ data.get(position).getString("Name") + "\"");

				convertView.setTag(viewHolder);
				viewHolder.imageView_Image.setImageDrawable(SVGParser
						.getSVGFromResource(context.getResources(),
								R.raw.ic_coin_minus).createPictureDrawable());
			} else {
				viewHolder.textViewCoins.setText(data.get(position).getString(
						"coins")
						+ " coins added");
				viewHolder.textViewCoins.setTextColor(context.getResources()
						.getColor(R.color.app_button_green_color));

				if (data.get(position).getString("signup").equalsIgnoreCase("")
						& data.get(position).getString("signup").isEmpty()) {

					viewHolder.textViewTransAmount
							.setText("Mobile Recharge of Rs. "
									+ data.get(position).getString(
											"transaction_amount") + "\n("
									+ data.get(position).getString("Name")
									+ ")");
				} else {
					viewHolder.textViewTransAmount.setText(data.get(position)
							.getString("signup"));
				}
				convertView.setTag(viewHolder);
				viewHolder.imageView_Image.setImageDrawable(SVGParser
						.getSVGFromResource(context.getResources(),
								R.raw.ic_coin_plus).createPictureDrawable());
			}
*/
			viewHolder.textTransDate.setText(data.get(position).getString(
					"trans_datetime"));
		} catch (JSONException je) {

		}
		Log.d("data", "data" + data);
		Constants.setFont(context, (ViewGroup)convertView);
		return convertView;
	}

	private class ViewHolder {
		ImageView imageStatusSuccess, imageView_Image, imageView_Misscall;
		TextView textViewTransAmount, textTransId, textTransDate,
				textViewCoins;
		LinearLayout linearLayout_Image;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}
}
