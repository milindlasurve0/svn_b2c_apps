//
//  GetMobileResponse.h
//  Pay1
//
//  Created by Annapurna on 03/07/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GetMobileResponse : NSObject

@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) NSMutableArray *detailsArray;



@end
