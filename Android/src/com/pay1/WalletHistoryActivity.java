package com.pay1;

import java.util.ArrayList;
import java.util.WeakHashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.pay1.adapterhandler.WalletHistoryAdapter;
import com.pay1.constants.Constants;
import com.pay1.customviews.MyProgressDialog;
import com.pay1.requesthandler.RequestClass;
import com.pay1.utilities.Utility;

public class WalletHistoryActivity extends AppBaseActivity {

	private static final String SCREEN_LABEL = "Wallet History Screen";
	private EasyTracker easyTracker = null;
	private final String TAG = "Wallet History";
	int page = 0;
	private Button button_LoadMore;
	private ImageView imageView_Back;
	private ListView listView_WalletHistory;
	private WalletHistoryAdapter adapter;
	private TextView textView_Title;
	private ArrayList<WeakHashMap<String, String>> data;

	public static final String CATAGORY = "trans_category";
	public static final String TIME = "trans_datetime";
	public static final String AMOUNT = "transaction_amount";
	public static final String BALANCE = "closing_bal";
	public static final String TRANS_TYPE = "trans_type";
	public static final String SERVICE_ID = "service_id";
	public static final String NUMBER = "number";
	public static final String OPERATOR = "operator_name";

	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.wallet_history_activity);
		try {
			easyTracker = EasyTracker.getInstance(WalletHistoryActivity.this);
			easyTracker.set(Fields.SCREEN_NAME, SCREEN_LABEL);
			registerBaseActivityReceiver();

			// Typeface Normal = Typeface.createFromAsset(getAssets(),
			// "EncodeSansNormal-100-Thin.ttf");

			Typeface Reguler = Typeface.createFromAsset(getAssets(),
					"EncodeSansNormal-400-Regular.ttf");

			textView_Title = (TextView) findViewById(R.id.textView_Title);
			textView_Title.setTypeface(Reguler);

			imageView_Back = (ImageView) findViewById(R.id.imageView_Back);
			imageView_Back.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onBackPressed();
				}
			});

			data = new ArrayList<WeakHashMap<String, String>>();
			data.clear();
			listView_WalletHistory = (ListView) findViewById(R.id.listView_WalletHistory);
			// textView_NoData = (TextView) findViewById(R.id.textView_NoData);
			// listView_WalletHistory.setEmptyView(textView_NoData);
			adapter = new WalletHistoryAdapter(WalletHistoryActivity.this, data);

			button_LoadMore = new Button(WalletHistoryActivity.this);
			button_LoadMore.setHeight(50);
			button_LoadMore
					.setBackgroundResource(R.drawable.button_background_unselected);
			button_LoadMore.setText("Load more.....");
			button_LoadMore
					.setTextColor(getResources().getColor(R.color.White));
			button_LoadMore.setTypeface(Reguler);
			button_LoadMore.setTextSize(15f);
			button_LoadMore.setVisibility(View.GONE);
			button_LoadMore.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					// // Log.e("Last post ID ", "" + lastPostID);
					button_LoadMore.setText("Loading.....");
					page++;
					new WalletHistoryTask().execute("" + page);
				}
			});

			listView_WalletHistory.addFooterView(button_LoadMore);
			listView_WalletHistory.setAdapter(adapter);

			new WalletHistoryTask().execute("" + page);

			if (getIntent().getExtras().containsKey("bal")) {
				if (getIntent().getExtras().getString("bal")
						.equalsIgnoreCase("1")) {
					Constants.showOneButtonDialog(WalletHistoryActivity.this,
							"Notification",
							getIntent().getExtras().getString("desc").trim(),
							Constants.DIALOG_CLOSE);
					new CheckBalanceTask().execute();
				}

			}

		} catch (Exception e) {
		}
		overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

	}

	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView().build());
		// EasyTracker.getInstance(this).activityStart(this);
		easyTracker.send(MapBuilder.createAppView().build());
	}

	@Override
	public void onStop() {
		super.onStop();
		// EasyTracker.getInstance(this).activityStop(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unRegisterBaseActivityReceiver();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (Constants.isExpanded) {
			Constants.collapsView(this);
		} else {
			super.onBackPressed();
			overridePendingTransition(R.anim.slide_in_left,
					R.anim.slide_out_right);
		}
	}

	public class WalletHistoryTask extends AsyncTask<String, String, String>
			implements OnDismissListener {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			try {
				String response = RequestClass.getInstance()
						.readPay1B2CRequest(
								WalletHistoryActivity.this,
								Constants.B2C_URL + "wallet_history/?page="
										+ params[0] + "&limit=10");

				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			try {

				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONObject jsonObject = new JSONObject(replaced);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {

						JSONArray jsonArrayDesc = jsonObject
								.getJSONArray("description");
						int len = 0;
						for (int i = 0; i < jsonArrayDesc.length(); i++) {
							WeakHashMap<String, String> map = new WeakHashMap<String, String>();
							map.put(CATAGORY, jsonArrayDesc.getJSONObject(i)
									.getString("trans_category"));
							map.put(TIME, jsonArrayDesc.getJSONObject(i)
									.getString("trans_datetime"));
							map.put(AMOUNT, jsonArrayDesc.getJSONObject(i)
									.getString("transaction_amount"));
							map.put(BALANCE, jsonArrayDesc.getJSONObject(i)
									.getString("closing_bal"));
							map.put(TRANS_TYPE, jsonArrayDesc.getJSONObject(i)
									.getString("trans_type"));
							map.put(SERVICE_ID, jsonArrayDesc.getJSONObject(i)
									.getString("service_id"));
							map.put(NUMBER, jsonArrayDesc.getJSONObject(i)
									.getString("number"));
							map.put(OPERATOR, jsonArrayDesc.getJSONObject(i)
									.getString("operator_name"));

							data.add(map);
							len++;
						}

						if (len < 10) {
							button_LoadMore.setVisibility(View.GONE);
						} else {
							button_LoadMore.setText("Load more history");
							button_LoadMore.setVisibility(View.VISIBLE);
						}

						WalletHistoryActivity.this
								.runOnUiThread(new Runnable() {

									@Override
									public void run() {
										// TODO Auto-generated method stub
										adapter.notifyDataSetChanged();
									}
								});

					} else {
						Constants.showOneButtonDialog(
								WalletHistoryActivity.this, TAG,
								Constants.checkCode(replaced),
								Constants.DIALOG_CLOSE);
					}

				} else {
					// Constants.showOneButtonDialog(WalletHistoryActivity.this,
					// TAG,
					// Constants.ERROR_INTERNET, Constants.DIALOG_CLOSE);
					Intent intent = new Intent(WalletHistoryActivity.this,
							ConnectivityErrorActivity.class);
					intent.putExtra(Constants.RECHARGE_FOR,
							Constants.WALLET_PAYMENT);
					startActivity(intent);
					finish();

				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonDialog(WalletHistoryActivity.this, TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonDialog(WalletHistoryActivity.this, TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(WalletHistoryActivity.this);
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							WalletHistoryTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			WalletHistoryTask.this.cancel(true);
			dialog.cancel();
		}

	}

	public class CheckBalanceTask extends AsyncTask<String, String, String> {
		// MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {

			String response = RequestClass.getInstance()
					.readPay1B2CRequest(WalletHistoryActivity.this,
							Constants.B2C_URL + "check_bal");
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			// if (this.dialog.isShowing()) {
			// this.dialog.dismiss();
			// }
			super.onPostExecute(result);
			try {
				// JSONArray array = new JSONArray(result);
				JSONObject jsonObject = new JSONObject(result);
				String status = jsonObject.getString("status");
				if (status.equalsIgnoreCase("SUCCESS")) {
					JSONObject description = jsonObject
							.getJSONObject("description");
					String account_balance = description
							.getString("account_balance");
					Utility.setBalance(WalletHistoryActivity.this,
							Constants.SHAREDPREFERENCE_BALANCE,
							account_balance.trim());
				}
			} catch (JSONException e) {
				// e.printStackTrace();
				// Constants.showCustomToast(WalletHistoryActivity.this,
				// result);
			} catch (Exception e) {
				// TODO: handle exception
				// Constants.showCustomToast(WalletHistoryActivity.this,
				// result);
			}

		}

		@Override
		protected void onPreExecute() {
			// dialog = new MyProgressDialog(WalletHistoryActivity.this);
			// // this.dialog.setMessage("Please wait.....");
			// this.dialog.show();
			super.onPreExecute();

		}

	}
}
