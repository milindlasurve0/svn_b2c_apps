﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Net.Http;
using System.ComponentModel;

namespace Pay1B2C.Views
    {
    public partial class PaymentGatewayLayout: PhoneApplicationPage
        {
        string reason;
        public PaymentGatewayLayout()
            {
            InitializeComponent();

            //GoogleAnalytics.EasyTracker.GetTracker ().SendView ("PaymentGatewayLayout");
            }
        private void wb1_Loaded(object sender, RoutedEventArgs e)
            {
            paymentBrowser.ApplyTemplate();
            paymentBrowser.IsScriptEnabled = true;

            string paymentHtml = PhoneApplicationService.Current.State[Constant.PAYMENT_GATEWAY_HTML].ToString();
            paymentBrowser.NavigateToString(paymentHtml);

            }


        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
            {
            var result = MessageBox.Show("Are you sure you want to cancel the transaction?", "Please confirm", MessageBoxButton.OKCancel);
            e.Cancel = result != MessageBoxResult.OK;
            base.OnBackKeyPress(e);
            }

        private void wb1_unloaded(object sender, RoutedEventArgs e)
            {
            var res = e.OriginalSource;
            }
        bool rechargeStatus;
        private void Browser_Navigated(object sender, System.Windows.Navigation.NavigationEventArgs e)
            {
            string uri = e.Uri.OriginalString;
            if(uri.Contains("payu_update"))
                {
                if(uri.Contains("api_new/action/api/true/actiontype/payu_update/page/success"))
                    {
                    //MessageBox.Show ("success");
                    rechargeStatus = true;
                    reason = "Transaction completed successfully.";

                    }
                else if(uri.Contains("api_new/action/api/true/actiontype/payu_update/page/failure"))
                    {
                    rechargeStatus = false;
                    reason = "Transaction failed.";

                    }
                else if(uri.Contains("api_new/action/api/true/actiontype/payu_update/page/timeout"))
                    {
                    rechargeStatus = false;
                    reason = "Transaction request timeout.";

                    }
                else if(uri.Contains("api_new/action/api/true/actiontype/payu_update/page/cancel"))
                    {
                    rechargeStatus = false;
                    reason = "Transaction cancelled.";

                    }
                if(rechargeStatus)
                    {
                    getAndUpdateBalance();
                    PhoneApplicationService.Current.State["Wallet_Topup"] = reason;
                    if(PhoneApplicationService.Current.State[Constant.OPERATOR_RECHARGE_FLAG].ToString().Equals(Constant.WALLET_REFILL.ToString()))
                        {
                        //Constant.ShowSuccessPopUp(ContentOverlay, "5", Constant.getUserNumber(), PhoneApplicationService.Current.State[Constant.RECHARGE_AMOUNT].ToString(), "22", rechargeStatus,"","");
                        MessageBoxResult result = MessageBox.Show(reason+"\nAmount added to your wallet.",
                "WALLET REFILL", MessageBoxButton.OKCancel);

                        if(result == MessageBoxResult.OK)
                            {
                            NavigationService.Navigate(new Uri("/HomePage.xaml?", UriKind.RelativeOrAbsolute));
                            NavigationService.GoBack();
                            }
                        else if(result == MessageBoxResult.Cancel)
                            {
                            NavigationService.GoBack();
                           
                            }
                        }
                    else
                        {
           /*             Constant.ShowSuccessPopUp(ContentOverlay, PhoneApplicationService.Current.State[Constant.OPERATOR_RECHARGE_FLAG].ToString(), Constant.getUserNumber(), PhoneApplicationService.Current.State[Constant.RECHARGE_AMOUNT].ToString(), "40", rechargeStatus,"","");*/
                       // PhoneApplicationService.Current.State["txn_id"]
                        getTransWallet(rechargeStatus,false,"40",PhoneApplicationService.Current.State["txn_id"].ToString());

                        }
                    }
                else
                    {
                    MessageBoxResult result = MessageBox.Show(reason,
            "Transaction Failed!", MessageBoxButton.OKCancel);

                    if(result == MessageBoxResult.OK)
                        {
                        NavigationService.GoBack();
                        }
                    else if(result == MessageBoxResult.Cancel)
                        {
                       
                        }
                    }
                    BackgroundWorker backroungWorker = new BackgroundWorker();
                    backroungWorker.DoWork += new DoWorkEventHandler(backroungWorker_DoWork);
                    //backroungWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(backroungWorker_RunWorkerCompleted);

                    backroungWorker.RunWorkerAsync();
                    
                }
            }

        private async void getTransWallet(bool rechargeStatus, bool rechargeType, string flag, string txn_id)
            {
            var values = new List<KeyValuePair<string, string>>();
            values.Add(new KeyValuePair<string, string>("actiontype", "GetTrans_WalletDetail"));

            values.Add(new KeyValuePair<string, string>("txnid", txn_id));
            if(rechargeType==true)
                {
                values.Add(new KeyValuePair<string, string>("voucher", "1"));
                }
            else
                {
                }
            values.Add(new KeyValuePair<string, string>("partial", "1"));
            values.Add(new KeyValuePair<string, string>("api", "true"));
            FormUrlEncodedContent values1 = new FormUrlEncodedContent(values);
            Task<string> response = Constant.postParamsAndgetResponse(Constant.B2C_URL, values1);

            string result = await response;
            if(string.IsNullOrEmpty(result))
                {
                NavigationService.Navigate(new Uri("/Views/NoConnection.xaml?", UriKind.Relative));
                }
            else
                {
                var dict = (JObject)JsonConvert.DeserializeObject(result);
                string status = dict["status"].ToString();
                var description = dict["description"];
                if(status.Equals("success"))
                    {
                    string account_balance = description["account_balance"].ToString();
                    string loyalty_points = description["loyalty_points"].ToString();
                    string points = description["points"].ToString();
                    Constant.setbalance(account_balance);
                    Constant.setLoyalityPoints(loyalty_points);
                    Constant.setbalance(account_balance);

                    Constant.ShowSuccessPopUp(ContentOverlay, PhoneApplicationService.Current.State[Constant.OPERATOR_RECHARGE_FLAG].ToString(), Constant.getUserNumber(), PhoneApplicationService.Current.State[Constant.RECHARGE_AMOUNT].ToString(), PhoneApplicationService.Current.State[Constant.RECHARGE_OPERATOR].ToString(), rechargeStatus, "", points);
                    }
                }
            }

        private void backroungWorker_DoWork(object sender, DoWorkEventArgs e)
            {
            getAndUpdateBalance();
            }

        private async void getAndUpdateBalance()
            {
            var values = new List<KeyValuePair<string, string>>();
            values.Add(new KeyValuePair<string, string>("actiontype", "Check_bal"));

            values.Add(new KeyValuePair<string, string>("api", "true"));
            FormUrlEncodedContent values1 = new FormUrlEncodedContent(values);
            Task<string> response = Constant.postParamsAndgetResponse(Constant.B2C_URL, values1);

            string result = await response;
            if(string.IsNullOrEmpty(result))
                {
                NavigationService.Navigate(new Uri("/Views/NoConnection.xaml?", UriKind.Relative));
                }
            else
                {
                var dict = (JObject)JsonConvert.DeserializeObject(result);
                string status = dict["status"].ToString();
                var description = dict["description"];
                if(status.Equals("success"))
                    {
                    string account_balance = description["account_balance"].ToString();
                    Constant.setbalance(account_balance);
                    }
                }
            }
        private void buttonImage_Click_Back(object sender, RoutedEventArgs e)
            {
            var result = MessageBox.Show("Are you sure you want to cancel the transaction?", "Please confirm", MessageBoxButton.OKCancel);
            if(result == MessageBoxResult.OK)
                {
                NavigationService.GoBack();
                }
            else
                {
                }

            }

        private void buttonImage_Click(object sender, RoutedEventArgs e)
            {
            var result = MessageBox.Show("Are you sure you want to cancel the transaction?", "Please confirm", MessageBoxButton.OKCancel);
            if(result == MessageBoxResult.OK)
                {
                NavigationService.GoBack();
                }
            else
                {
                }

            }
        }
    }