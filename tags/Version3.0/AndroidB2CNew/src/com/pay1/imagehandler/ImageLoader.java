package com.pay1.imagehandler;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.pay1.R;

public class ImageLoader {

	Context context;
	MemoryCache memoryCache = new MemoryCache();
	FileCache fileCache;
	private Map<ImageView, String> imageViews = Collections
			.synchronizedMap(new WeakHashMap<ImageView, String>());
	ExecutorService executorService;
	// handler to display images in UI thread
	Handler handler = new Handler();

	// String URL = null;

	final int DEFAULT_DEAL_IMAGE = R.drawable.deal_default;

	public ImageLoader(Context c) {
		this.context = c;
		fileCache = new FileCache(c);
		executorService = Executors.newFixedThreadPool(5);
	}

	public void getDealImage(final String url, ImageView imageView) {
		imageViews.put(imageView, url);
		Bitmap bitmap = memoryCache.get(url);
		if (bitmap != null) {
			imageView.setImageBitmap(bitmap);
		} else {
			queueDealImage(url, imageView);
			// imageView.setImageResource(DEFAULT_DEAL_IMAGE);
			// progressBar1.setVisibility(View.GONE);
			// Bitmap bitmap_Default = BitmapFactory.decodeResource(
			// context.getResources(), R.drawable.connection_placeholder);
			// imageView.setImageBitmap(GlobalClass
			// .getRoundedBitmap(bitmap_Default));
		}
	}

	public void getDealImage(final String url, ImageView imageView,
			ProgressBar progressBar1) {
		progressBar1.setVisibility(View.VISIBLE);
		imageViews.put(imageView, url);
		Bitmap bitmap = memoryCache.get(url);
		if (bitmap != null) {
			imageView.setImageBitmap(bitmap);
			progressBar1.setVisibility(View.GONE);
		} else {
			queueDealImage(url, imageView, progressBar1);
			// imageView.setImageResource(DEFAULT_DEAL_IMAGE);
			// progressBar1.setVisibility(View.GONE);
			// Bitmap bitmap_Default = BitmapFactory.decodeResource(
			// context.getResources(), R.drawable.connection_placeholder);
			// imageView.setImageBitmap(GlobalClass
			// .getRoundedBitmap(bitmap_Default));
		}
	}

	// public void getFullScreenPostImage(String postID, String communityID,
	// ImageView imageView) {
	// String url = URL + "Images/" + communityID + "/" + communityID + "_"
	// + postID + "_530X400.png ";
	// // // Log..e("URL PAth ", URL);
	// imageViews.put(imageView, url);
	// Bitmap bitmap = memoryCache.get(url);
	// if (bitmap != null) {
	//
	// // Get current dimensions AND the desired bounding box
	// int width = bitmap.getWidth();
	// int height = bitmap.getHeight();
	// int bounding = dpToPx(250);
	// // // Log..i("Test", "original width = " + Integer.toString(width));
	// // // Log..i("Test", "original height = " + Integer.toString(height));
	// // // Log..i("Test", "bounding = " + Integer.toString(bounding));
	//
	// // Determine how much to scale: the dimension requiring less scaling
	// // is
	// // closer to the its side. This way the image always stays inside
	// // your
	// // bounding box AND either x/y axis touches it.
	// float xScale = ((float) bounding) / width;
	// float yScale = ((float) bounding) / height;
	// float scale = (xScale <= yScale) ? xScale : yScale;
	// // // Log..i("Test", "xScale = " + Float.toString(xScale));
	// // // Log..i("Test", "yScale = " + Float.toString(yScale));
	// // // Log..i("Test", "scale = " + Float.toString(scale));
	//
	// // Create a matrix for the scaling and add the scaling data
	// Matrix matrix = new Matrix();
	// matrix.postScale(scale, scale);
	//
	// // Create a new bitmap and convert it to a format understood by the
	// // ImageView
	// Bitmap scaledBitmap = Bitmap.createBitmap(bitmap, 0, 0, width,
	// height, matrix, true);
	// width = scaledBitmap.getWidth(); // re-use
	// height = scaledBitmap.getHeight(); // re-use
	// BitmapDrawable result = new BitmapDrawable(scaledBitmap);
	// // // Log..i("Test", "scaled width = " + Integer.toString(width));
	// // // Log..i("Test", "scaled height = " + Integer.toString(height));
	//
	// // Apply the scaled bitmap
	// imageView.setImageDrawable(result);
	// // imageView.setImageBitmap(bitmap);
	//
	// // Now change ImageView's dimensions to match the scaled image
	// LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) imageView
	// .getLayoutParams();
	// params.width = width;
	// params.height = height;
	// imageView.setLayoutParams(params);
	// } else {
	// queuePostImage(url, imageView);
	// imageView.setImageResource(DEFAULT_POST_IMAGE);
	// }
	// }

	// private int dpToPx(int dp) {
	// float density = context.getResources().getDisplayMetrics().density;
	// return Math.round((float) dp * density);
	// }

	private void queueDealImage(String url, ImageView imageView) {
		PhotoToLoad p = new PhotoToLoad(url, imageView);
		executorService.submit(new DealImageLoaderOnMap(p));
	}

	private void queueDealImage(String url, ImageView imageView,
			ProgressBar progressBar1) {
		PhotoToLoad p = new PhotoToLoad(url, imageView, progressBar1);
		executorService.submit(new DealImageLoader(p));
	}

	private Bitmap getBitmap(String url) {
		File f = fileCache.getFile(url);

		// from SD cache
		Bitmap b = decodeFile(f);
		if (b != null)
			return b;

		// from web
		try {
			Bitmap bitmap = null;
			URL imageUrl = new URL(url);
			HttpURLConnection conn = (HttpURLConnection) imageUrl
					.openConnection();
			conn.setConnectTimeout(30000);
			conn.setReadTimeout(30000);
			conn.setInstanceFollowRedirects(true);
			InputStream is = conn.getInputStream();
			OutputStream os = new FileOutputStream(f);
			Utils.CopyStream(is, os);
			os.close();
			conn.disconnect();
			bitmap = decodeFile(f);
			return bitmap;
		} catch (Throwable ex) {
			// ex.printStackTrace();
			if (ex instanceof OutOfMemoryError)
				memoryCache.clear();
			return null;
		}
	}

	// decodes image and scales it to reduce memory consumption
	private Bitmap decodeFile(File f) {
		try {
			// decode image size
			// BitmapFactory.Options o = new BitmapFactory.Options();
			// o.inJustDecodeBounds = true;
			// FileInputStream stream1 = new FileInputStream(f);
			// BitmapFactory.decodeStream(stream1, null, o);
			// stream1.close();

			// Find the correct scale value. It should be the power of 2.
			// final int REQUIRED_SIZE = 600;
			// int width_tmp = o.outWidth, height_tmp = o.outHeight;
			// int scale = 1;

			// WindowManager wm = (WindowManager) context
			// .getSystemService(Context.WINDOW_SERVICE);
			// Display display = wm.getDefaultDisplay();
			// int width = display.getWidth(); // deprecated
			// int height = display.getHeight(); // deprecated
			//
			// System.out.println("Scale device b4 width " + width + " Height "
			// + height);
			// System.out.println("Scale b4 width " + width_tmp + " height "
			// + height_tmp);
			// while (true) {
			// if (width_tmp / 2 < REQUIRED_SIZE
			// || height_tmp / 2 < REQUIRED_SIZE)
			// break;
			// width_tmp /= 2;
			// height_tmp /= 2;
			// scale *= 2;
			// System.out.println("Scale a8 width " + width_tmp + " height "
			// + height_tmp);
			// }

			// decode with inSampleSize
			BitmapFactory.Options o2 = new BitmapFactory.Options();
			// o2.inSampleSize = 1;
			// o2.outWidth = width;
			// o2.outHeight = (width * 2) / 3;
			o2.inDensity = DisplayMetrics.DENSITY_MEDIUM;
			o2.inTargetDensity = context.getResources().getDisplayMetrics().densityDpi;
			o2.inScaled = true;
			FileInputStream stream2 = new FileInputStream(f);
			Bitmap bitmap = BitmapFactory.decodeStream(stream2, null, o2);
			stream2.close();
			// System.out.println("Scale device a8 width " + width + " Height "
			// + (width * 2) / 3);
			return bitmap;
		} catch (FileNotFoundException e) {
		} catch (IOException e) {
			// e.printStackTrace();
		}
		return null;
	}

	// Task for the queue
	private class PhotoToLoad {
		public String url;
		public ImageView imageView;
		public ProgressBar progressBar1;

		public PhotoToLoad(String u, ImageView i) {
			url = u;
			imageView = i;
		}

		public PhotoToLoad(String u, ImageView i, ProgressBar p) {
			url = u;
			imageView = i;
			progressBar1 = p;
		}
	}

	class DealImageLoaderOnMap implements Runnable {
		PhotoToLoad photoToLoad;

		DealImageLoaderOnMap(PhotoToLoad photoToLoad) {
			this.photoToLoad = photoToLoad;
		}

		@Override
		public void run() {
			try {
				if (imageViewReused(photoToLoad))
					return;
				Bitmap bmp = getBitmap(photoToLoad.url);
				memoryCache.put(photoToLoad.url, bmp);
				if (imageViewReused(photoToLoad))
					return;
				DealBitmapDisplayerOnMap bd = new DealBitmapDisplayerOnMap(bmp,
						photoToLoad);
				handler.post(bd);
			} catch (Throwable th) {
				// th.printStackTrace();
			}
		}
	}

	class DealImageLoader implements Runnable {
		PhotoToLoad photoToLoad;

		DealImageLoader(PhotoToLoad photoToLoad) {
			this.photoToLoad = photoToLoad;
		}

		@Override
		public void run() {
			try {
				if (imageViewReused(photoToLoad))
					return;
				Bitmap bmp = getBitmap(photoToLoad.url);
				memoryCache.put(photoToLoad.url, bmp);
				if (imageViewReused(photoToLoad))
					return;
				DealBitmapDisplayer bd = new DealBitmapDisplayer(bmp,
						photoToLoad);
				handler.post(bd);
			} catch (Throwable th) {
				// th.printStackTrace();
			}
		}
	}

	boolean imageViewReused(PhotoToLoad photoToLoad) {
		String tag = imageViews.get(photoToLoad.imageView);
		if (tag == null || !tag.equals(photoToLoad.url))
			return true;
		return false;
	}

	// Used to display bitmap in the UI thread
	class DealBitmapDisplayerOnMap implements Runnable {
		Bitmap bitmap;
		PhotoToLoad photoToLoad;

		public DealBitmapDisplayerOnMap(Bitmap b, PhotoToLoad p) {
			bitmap = b;
			photoToLoad = p;
		}

		public void run() {
			if (imageViewReused(photoToLoad))
				return;
			if (bitmap != null) {
				photoToLoad.imageView.setImageBitmap(bitmap);
			} else {
				photoToLoad.imageView.setImageResource(DEFAULT_DEAL_IMAGE);
			}
		}
	}

	// Used to display bitmap in the UI thread
	class DealBitmapDisplayer implements Runnable {
		Bitmap bitmap;
		PhotoToLoad photoToLoad;

		public DealBitmapDisplayer(Bitmap b, PhotoToLoad p) {
			bitmap = b;
			photoToLoad = p;
		}

		public void run() {
			if (imageViewReused(photoToLoad))
				return;
			if (bitmap != null) {
				photoToLoad.imageView.setImageBitmap(bitmap);
				photoToLoad.progressBar1.setVisibility(View.GONE);
			} else {
				photoToLoad.imageView.setImageResource(DEFAULT_DEAL_IMAGE);
				photoToLoad.progressBar1.setVisibility(View.GONE);
			}
		}
	}

	public void clearCache() {
		memoryCache.clear();
		fileCache.clear();
	}

}
