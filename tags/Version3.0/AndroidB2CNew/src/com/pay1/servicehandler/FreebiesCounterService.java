package com.pay1.servicehandler;

import java.util.Timer;
import java.util.TimerTask;

import android.app.IntentService;
import android.content.Intent;
import android.database.SQLException;

import com.pay1.constants.Constants;
import com.pay1.databasehandler.ReClaimGift;
import com.pay1.databasehandler.ReClaimGiftDataSource;

public class FreebiesCounterService extends IntentService {

	private Timer timer;
	private int counter = 0, amount = 0;
	private String trans_id = "";
	private ReClaimGiftDataSource reClaimGiftDataSource;

	public FreebiesCounterService() {
		super("FreebiesCounterService");
		// TODO Auto-generated constructor stub
		reClaimGiftDataSource = new ReClaimGiftDataSource(
				FreebiesCounterService.this);
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		trans_id = intent.getStringExtra("T_ID");
		timer = new Timer();
		timer.schedule(new TimerTask() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				if (counter >= 30) {
					timer.cancel();

					try {
						reClaimGiftDataSource.open();
						ReClaimGift reClaimGift = reClaimGiftDataSource
								.getReClaimGift(trans_id, 1);
						if (reClaimGift != null) {
							Constants.Notify(FreebiesCounterService.this,
									"Pay1",
									"Looks like you missed your free gift",
									reClaimGift.getReClaimID(),
									reClaimGift.getReClaimTransactionID(),
									reClaimGift.getReClaimRechargeAmount());
							reClaimGiftDataSource.updateReClaimGift(trans_id,
									1, 2, System.currentTimeMillis());
						}
					} catch (SQLException exception) {
						// TODO: handle exception
						// // // Log.e("Er ", exception.getMessage());
					} catch (Exception e) {
						// TODO: handle exception
						// // // Log.e("Er ", e.getMessage());
					} finally {
						reClaimGiftDataSource.close();
					}
				}
				counter++;
			}
		}, 0, 1000);

	}
}
