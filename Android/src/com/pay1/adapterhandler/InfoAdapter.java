package com.pay1.adapterhandler;

import java.util.ArrayList;

import com.pay1.R;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class InfoAdapter extends BaseAdapter {

	private Context context;
	private ArrayList<String> list;
	private Typeface Reguler, Narrow, Narrow_thin;

	public InfoAdapter(Context context, ArrayList<String> list) {
		this.context = context;
		this.list = list;

		Reguler = Typeface.createFromAsset(context.getAssets(),
				"EncodeSansNormal-400-Regular.ttf");

		Narrow = Typeface.createFromAsset(context.getAssets(),
				"EncodeSansNarrow-500-Medium.ttf");

		Narrow_thin = Typeface.createFromAsset(context.getAssets(),
				"EncodeSansNarrow-100-Thin.ttf");
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this.context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.info_adapter, null);
		}

		TextView textView_Name = (TextView) convertView
				.findViewById(R.id.textView_Name);
		textView_Name.setTypeface(Reguler);

		textView_Name.setText(list.get(position));
		return convertView;
	}

}
