//
//  DetailImageView.m
//  Pay1
//
//  Created by Annapurna on 16/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "DetailImageView.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#import "UIImageView+WebCache.h"
#import "UIImage+Utility.h"
@implementation DetailImageView


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self awakeFromNib];
    }
    return self;
}

-(void)awakeFromNib {
    
  
    
    UIView* myView = [[[NSBundle mainBundle] loadNibNamed:@"DetailImageView" owner:self options:nil] objectAtIndex:0];
    myView.backgroundColor = [UIColor blackColor];
    CATransition* transition = [CATransition animation];
    transition.duration = 0.5f;//kAnimationDuration
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromTop; //kCATransitionFromLeft;
    transition.delegate = self;
    [myView.layer addAnimation:transition forKey:nil];

    [self addSubview:myView];
    
}

-(IBAction)crossClick:(id)sender{
    NSLog(@"#ButtonClick");
    if(_onButtonClick){
    _onButtonClick();
    }
}


//-(void)setImageOnDetailView:(NSString *)imageURL{
-(void)setImageOnDetailView:(NSMutableArray *)imageURLArray tagValue:(NSInteger)tagValue{
    
    //CGFloat x = 0.0 ;
    
    NSLog(@"Index Values :%lu %ld",(unsigned long)imageURLArray.count,(long)tagValue + 1);
    
    //imageArray = imageURLArray;
    
   // imageTagValue = tagValue;
    
    
   // [imageScrollView setContentSize:CGSizeMake(imageScrollView.frame.size.width*3, imageScrollView.frame.size.height)];
    
   /* for(int i=0;i<imageURLArray.count;i++){
        
    imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0 , 0, imageScrollView.frame.size.width, imageScrollView.frame.size.height)];
   NSString *imageURL = [imageURLArray objectAtIndex:tagValue];
        
   //NSString *imageURL = [imageURLArray objectAtIndex:i];
        
    [imageView setImageWithURL:[NSURL URLWithString:imageURL] placeholderImage:[UIImage imageNamed:@"deal_default.png"]completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
    UIImage *newImage = [UIImage imageWithImage:image  scaledToSize:CGSizeMake([UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height - 64)];
    imageView.image = newImage;
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    }usingActivityIndicatorStyle:(UIActivityIndicatorViewStyle)UIActivityIndicatorViewStyleGray];
        
        
    [imageScrollView addSubview:imageView];
        
        
    x+=self.frame.size.width ;
        
    }*/
    
    NSString *imageURL = [imageURLArray objectAtIndex:tagValue];

    
    [imageView setImageWithURL:[NSURL URLWithString:imageURL] placeholderImage:[UIImage imageNamed:@"deal_default.png"]completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
        UIImage *newImage = [UIImage imageWithImage:image  scaledToSize:CGSizeMake([UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height - 64)];
        imageView.image = newImage;
        imageView.contentMode = UIViewContentModeScaleAspectFit;
    }usingActivityIndicatorStyle:(UIActivityIndicatorViewStyle)UIActivityIndicatorViewStyleGray];
    
}

/*
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    pointNow = scrollView.contentOffset;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView.contentOffset.x<pointNow.x) {
        // NSLog(@"LEFT");
    }
    
    else if (scrollView.contentOffset.x>pointNow.x) {
        // NSLog(@"RIGHT");
        isMovingDirection = FALSE;
        
        
    
    CGFloat pageWidth = scrollView.frame.size.width;
    float fractionalPage = scrollView.contentOffset.x / pageWidth;
    NSInteger page = lround(fractionalPage);
    
    NSString *imageURL = [imageArray objectAtIndex:page];
   
    
    [imageView setImageWithURL:[NSURL URLWithString:imageURL] placeholderImage:[UIImage imageNamed:@"deal_default.png"]completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
        UIImage *newImage = [UIImage imageWithImage:image  scaledToSize:CGSizeMake([UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height - 64)];
        imageView.image = newImage;
        imageView.contentMode = UIViewContentModeScaleAspectFit;
    }usingActivityIndicatorStyle:(UIActivityIndicatorViewStyle)UIActivityIndicatorViewStyleGray];
}
}*/

@end
