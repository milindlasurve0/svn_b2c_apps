<?php $this->load->view('common/doctype_html'); 

    $current_page_url =  "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    $meteTagData = getMetaTagDetails();
    foreach ($meteTagData as $row){
      
      if($row->page_url == $current_page_url){
        $new_page_tittle =  $row->page_tittle;
        $new_page_meta_description =  $row->page_meta_description;  
        $new_page_meta_tag =  $row->page_meta_tag;  
      }
      
    }
?>
<meta name="description" content="<?php if($new_page_meta_description){ echo $new_page_meta_description; }else{ ?>Grab this gift today from my likes to make yourself happy and fulfill your aspiration. Use your gift coins to Grab a gift today. <?php } ?>">
<meta name="keywords" content="<?php if($new_page_meta_tag){ echo $new_page_meta_tag; }else{ ?>List of my likes from Gift store , Pay1<?php } ?>" />
<title><?php if($new_page_tittle){ echo $new_page_tittle; }else{ ?>List of my likes from Gift store | Pay1 <?php } ?></title>
<?php $this->load->view('common/header');  ?>
<style type="text/css">
    .autocomplete-suggestions {border: 1px solid #999; background: #fff; cursor: default; overflow: auto; }
    .autocomplete-suggestion { padding: 10px 5px; font-size: 1.0em; white-space: nowrap; overflow: hidden; }
    .autocomplete-selected { background: #f0f0f0; }
    .autocomplete-suggestions strong { font-weight: normal; color: #3399ff; }   
</style>   
    <link rel="stylesheet" href="/assets/css/style.css">
    
      <div class="clearfix" style="clear:both;"></div>
      <div class="bottom_bg" style=""></div>
      <div class="top_like well text-center">
         <p class="p_icon red_active tmid"><i class="glyphicon glyphicon-heart-empty"></i></p>  
          <p><b>Save Products as you browse</b></p>
          <p> Your Saved products will be stored till you remove them</p>

      </div>
      <div class="container-fluid">
         <div class="container border-both bdtop">
            <div class="row">
               <div class="col-md-12 text-center margin-top-25">
                  <div class="row">
                    <form action="" method="">
                        <div class="col-md-4 col-md-offset-3">
                           <!--<input type="text" id ="search_brand" name="search_brand" class="form-control" placeholder="Search by typing the product or brand name...">-->
                           <input type="text" id ="search_brand" name="search_brand" class="form-control autocomplete" placeholder="Search by typing the product or brand name...">
                        </div>
                        <div class="col-md-1">
                           <button type="button" class="btn btn-save search-btn" onclick="search_brands()" style="border-radius:5px" >SEARCH</button>
                        </div>
                    </form>
                  </div>
               </div>
              
            <div class="col-md-12">
                <p class="heading margin-top-40 mobile_left col-md-12 "><span class="page_title">My Likes </span>
                <div class="itemlikes">You have saved <span class="faghead"><?php echo count($response['description']); ?></span> items</div>
                    
                    <div id="products" class="row list-group gift_types">
                        <?php foreach($response['description'] as $row):
                              $deal_name_arr[] = array("value" => $row['deal'],"data"=> $row['deal']);
                        ?>    
                        <div class="item col-md-3 col-xs-6">
                            <div class="thumbnail">
                              <div class="col-md-10 col-xs-10">
                                <p class="p_detail"><?php echo $row['offer_desc']; ?></p>
                              </div>
                              <div class="col-md-2 col-xs-2">
                                <p class="p_icon red_active"><i class="glyphicon glyphicon-heart-empty"></i></p>
                              </div>
                            <a href="/giftstore/giftdetails/<?php echo $id_area = $row['deal_id'].'_'.trim($row['area']); ?>" style="padding-left: 1px;">
                                <img class="group list-group-image img-responsive" src="<?php echo $row['img_url']; ?>"  alt=""  style="width: 189px;height: 96px" />
                            </a>  
                              <div class="caption">
                                <div class="row">
                                    <div class="col-md-4 col-xs-4">
                                      <?php if($row['logo'] == ''){ ?>
                                        <img class="logo" src="/public/assets/images/default_img/ic_logo_entertainment.png" alt="">
                                      <?php }else{ ?>
                                            <img src="<?php echo $row['logo']; ?>" class="logo"> 
                                      <?php } ?>
                                    </div>
                                    <div class="col-md-8 col-xs-8" style="text-align:right">
                                        <?php if($row['by_voucher']==1){ ?><i class="fa fa-inr"></i> <?php echo $row['offer_price'] ; ?> + <?php } ?><img src="/assets/images/gift/iconall.png"><span><?php echo $row['min_amount'] ; ?></span> 
                                    </div>
                                    <div class="col-md-12 col-xs-12 text-left margin-top-20">  
                                        <p class="group inner list-group-item-text"><?php echo $row['deal']; ?></p>
                                        <?php if(!empty($row['area'])){  ?>
                                        <p><img src="/assets/images/gift/icongift.png" alt=""> <span><?php // echo $row['area']; ?></span></p>
                                        <?php } ?>
                                    </div>
                                </div>

                               </div>
                            </div>
                        </div>
                     <?php endforeach; ?>
                          
                    </div>    
            </div>
            </div>
                
            <div class="margin-top-30 clearfix"></div>
         </div>
       </div>
     
        <div class="clearfix"></div>
      
        <div class="modal fade" role="dialog" id="failure">
          <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="cls" data-dismiss="modal">&times;</button>
                  <h3>Payment Failure</h3>
               </div>
               <div class="modal-body">
                  <p>Sorry! Your previous payment attempt failed.</p>
                  <div class="pull-right">
                    <button class="modal_button" id="try-again">Try Again</button>
                    <button class="modal_button" id="cancel-button" data-dismiss="modal">Cancel</button>
                  </div>
                  <div class="clearfix"></div>
               </div>
            </div>
         </div>
        </div>
      <script type="text/javascript" src="/public/js/jquery.autocomplete.min.js"></script>  
      <script src="/assets/js/bootstrap.min.js"></script>
      <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>

      <script src="/assets/js/jquery-scrollToTop.js"></script>
      <script type="text/javascript">
         $(document).ready(function($) {
             $('body').scrollToTop({skin: 'cycle'});
         });
        
        var deal_name_data = <?php echo json_encode($deal_name_arr); ?> ;
        $('.autocomplete').autocomplete({
        lookup: deal_name_data,
        onSelect: function (suggestion) {
          search_brands();
         }
        }); 
      </script>
        <script type="text/javascript" src="/assets/js/gift_custom.js"> </script>

<?php $this->load->view('common/footer'); ?>