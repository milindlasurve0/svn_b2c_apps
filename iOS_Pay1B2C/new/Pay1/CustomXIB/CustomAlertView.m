//
//  CustomAlertView.m
//  Pay1
//
//  Created by Annapurna on 13/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "CustomAlertView.h"

@implementation CustomAlertView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self awakeFromNib];
    }
    return self;
}

-(void)awakeFromNib {
    UIView* myView = [[[NSBundle mainBundle] loadNibNamed:@"CustomAlertView" owner:self options:nil] objectAtIndex:0];
    myView.backgroundColor = [UIColor whiteColor];
    
    [myView setFrame:CGRectMake(30, 10, self.frame.size.width - 30, 200)];
    myView.transform = CGAffineTransformMakeScale(0.01, 0.01);
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        myView.transform = CGAffineTransformMakeScale(1.2, 1.2);
        [myView setAlpha:1.0];
    } completion:^(BOOL finished){
        
    }];
    CALayer *l = [myView layer];
    [l setMasksToBounds:YES];
    [l setCornerRadius:15.0];
    
    [doneButton setBackgroundColor:[UIColor colorWithRed:36/255.0 green:182/255.0 blue:82/255.0 alpha:1.0]];
    doneButton.layer.cornerRadius = 5.0;
    [topView setBackgroundColor:[UIColor colorWithRed:36/255.0 green:182/255.0 blue:82/255.0 alpha:1.0]];


    
    [self addSubview:myView];
    
}

-(IBAction)retryClick:(id)sender{
    NSLog(@"#ButtonClick");
    if(_onButtonClick)
        _onButtonClick();
}


-(void)setTextOnCustomView:(NSString *)topViewLabelText messageText:(NSString *)messageLabelText{
    topViewLabel.text = topViewLabelText;
    messageLabel.text = messageLabelText;
}

@end
