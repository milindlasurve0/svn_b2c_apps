package com.pay1.databasehandler;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

public class QuickPayDataSource {

	// Database fields
	private SQLiteDatabase database;
	private Pay1SQLiteHelper dbHelper;

	public QuickPayDataSource(Context context) {
		dbHelper = new Pay1SQLiteHelper(context);
	}

	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
		System.out.println("Released " + SQLiteDatabase.releaseMemory());
	}

	public void createQuickPay(JSONArray response, long timestamp) {
		try {
			open();
			String quickPay = "INSERT INTO " + Pay1SQLiteHelper.TABLE_QUICKPAY
					+ " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";

			SQLiteStatement insert_quickPay = database
					.compileStatement(quickPay);
			database.beginTransaction();

			for (int i = 0; i < response.length(); i++) {
				try {
					JSONObject jsonObject_inner = response.getJSONObject(i);

					insert_quickPay.bindLong(1,
							Integer.parseInt(jsonObject_inner.getString("id")));
					insert_quickPay.bindLong(2, Integer
							.parseInt(jsonObject_inner.getString("flag")));
					insert_quickPay.bindString(3,
							jsonObject_inner.getString("name"));
					insert_quickPay.bindLong(4, Integer
							.parseInt(jsonObject_inner.getString("amount")));
					insert_quickPay
							.bindLong(5, Integer.parseInt(jsonObject_inner
									.getString("stv")));
					insert_quickPay.bindLong(6,
							Integer.parseInt(jsonObject_inner
									.getString("operator_id")));
					insert_quickPay.bindLong(7, Integer
							.parseInt(jsonObject_inner.getString("product")));
					insert_quickPay.bindString(8,
							jsonObject_inner.getString("number"));
					insert_quickPay.bindLong(9, Integer
							.parseInt(jsonObject_inner
									.getString("delegate_flag")));
					insert_quickPay.bindString(10,
							jsonObject_inner.getString("missed_number"));
					insert_quickPay.bindString(11,
							jsonObject_inner.getString("operator_name"));
					insert_quickPay.bindString(12,
							jsonObject_inner.getString("operator_code"));
					insert_quickPay.bindLong(13, timestamp);
					insert_quickPay.bindString(14,
							jsonObject_inner.getString("datetime"));
					insert_quickPay.bindLong(15, Integer
							.parseInt(jsonObject_inner
									.getString("transaction_flag")));

					insert_quickPay.execute();
					insert_quickPay.clearBindings();
				} catch (Exception e) {
				}
			}
		} catch (SQLException exception) {
			// Log.d("Er ", "row inserted   " + exception.getMessage());
			// TODO: handle exception
			// // // Log.e("Er ", exception.getMessage());
		} catch (Exception e) {
			// Log.d("Er ", "row inserted   " + e.getMessage());
			// TODO: handle exception
			// // // Log.e("Er ", e.getMessage());
		} finally {
			database.setTransactionSuccessful();
			database.endTransaction();
			close();
		}
	}

	public QuickPay createQuickPay(int quickPayID, int quickPayFlag,
			String quickPayName, int quickPayAmount, int quickPaySTV,
			int quickPayOperatorID, int quickPayProductID,
			String quickPayMobile, int quickPayDelegateFlag,
			String quickPayMissCallNumber, String quickPayOperatorName,
			String quickPayOperatorCode, long quickPayUptateTime,
			String quickPayTransactionDate, int quickPayTransactionFlag) {

		ContentValues values = new ContentValues();
		values.put(Pay1SQLiteHelper.QP_ID, quickPayID);
		values.put(Pay1SQLiteHelper.QP_FLAG, quickPayFlag);
		values.put(Pay1SQLiteHelper.QP_NAME, quickPayName);
		values.put(Pay1SQLiteHelper.QP_AMOUNT, quickPayAmount);
		values.put(Pay1SQLiteHelper.QP_STV, quickPaySTV);
		values.put(Pay1SQLiteHelper.QP_OPERATOR_ID, quickPayOperatorID);
		values.put(Pay1SQLiteHelper.QP_PRODUCT_ID, quickPayProductID);
		values.put(Pay1SQLiteHelper.QP_MOBILE, quickPayMobile);
		values.put(Pay1SQLiteHelper.QP_DELEGATE_FLAG, quickPayDelegateFlag);
		values.put(Pay1SQLiteHelper.QP_MISSCALL_NUMBER, quickPayMissCallNumber);
		values.put(Pay1SQLiteHelper.QP_OPERATOR_NAME, quickPayOperatorName);
		values.put(Pay1SQLiteHelper.QP_OPERATOR_CODE, quickPayOperatorCode);
		values.put(Pay1SQLiteHelper.QP_UPDATE_TIME, quickPayUptateTime);
		values.put(Pay1SQLiteHelper.QP_TRANSATION_DATE, quickPayTransactionDate);
		values.put(Pay1SQLiteHelper.QP_TRANSATION_FLAG, quickPayTransactionFlag);

		long insertId = database.insert(Pay1SQLiteHelper.TABLE_QUICKPAY, null,
				values);
		Cursor cursor = database.query(Pay1SQLiteHelper.TABLE_QUICKPAY, null,
				Pay1SQLiteHelper.QP_ID + " = " + insertId, null, null, null,
				null);
		cursor.moveToFirst();
		QuickPay newQuickPay = cursorToQuickPay(cursor);
		cursor.close();
		return newQuickPay;
	}

	public void updateQuickPay(int quickPayID, int quickPayDelegateFlag,
			String quickPayMissCallNumber) {

		ContentValues values = new ContentValues();
		values.put(Pay1SQLiteHelper.QP_DELEGATE_FLAG, quickPayDelegateFlag);
		values.put(Pay1SQLiteHelper.QP_MISSCALL_NUMBER, quickPayMissCallNumber);
		long updatetId = database.update(Pay1SQLiteHelper.TABLE_QUICKPAY,
				values, Pay1SQLiteHelper.QP_ID + " = " + quickPayID, null);
		if (updatetId > 0) {
		}
		// // // Log.i(Tag, "Monitor updated with id: " + monitor_ID);
		else {
		}
	}

	public void deleteQuickPay() {
		database.delete(Pay1SQLiteHelper.TABLE_QUICKPAY, null, null);
	}

	public List<QuickPay> getQuickPayByLimit(int limit) {
		List<QuickPay> quickPays = new ArrayList<QuickPay>();

		Cursor cursor = database.query(Pay1SQLiteHelper.TABLE_QUICKPAY, null,
				Pay1SQLiteHelper.QP_OPERATOR_CODE + " !='null' ", null, null,
				null, Pay1SQLiteHelper.QP_TRANSATION_FLAG + ", datetime("
						+ Pay1SQLiteHelper.QP_TRANSATION_DATE + ") desc limit "
						+ limit);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			QuickPay quickPay = cursorToQuickPay(cursor);
			quickPays.add(quickPay);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return quickPays;
	}

	public List<QuickPay> getAllQuickPay() {
		List<QuickPay> quickPays = new ArrayList<QuickPay>();

		Cursor cursor = database.query(Pay1SQLiteHelper.TABLE_QUICKPAY, null,
				Pay1SQLiteHelper.QP_OPERATOR_CODE + " !='null' ", null, null,
				null, Pay1SQLiteHelper.QP_TRANSATION_FLAG + ", datetime("
						+ Pay1SQLiteHelper.QP_TRANSATION_DATE + ") desc");

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			QuickPay quickPay = cursorToQuickPay(cursor);
			quickPays.add(quickPay);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return quickPays;
	}

	public List<QuickPay> getAllQuickPay(String mobile, int flag) {
		List<QuickPay> quickPays = new ArrayList<QuickPay>();

		Cursor cursor = database.query(Pay1SQLiteHelper.TABLE_QUICKPAY, null,
				Pay1SQLiteHelper.QP_MOBILE + " = '" + mobile + "' AND "
						+ Pay1SQLiteHelper.QP_TRANSATION_FLAG + " = " + flag
						+ " AND " + Pay1SQLiteHelper.QP_OPERATOR_CODE
						+ " !='null' ", null, null, null, "datetime("
						+ Pay1SQLiteHelper.QP_TRANSATION_DATE + ") desc");

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			QuickPay quickPay = cursorToQuickPay(cursor);
			quickPays.add(quickPay);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return quickPays;
	}

	public List<QuickPay> getAllQuickPay(int flag) {
		List<QuickPay> quickPays = new ArrayList<QuickPay>();

		Cursor cursor = database.query(Pay1SQLiteHelper.TABLE_QUICKPAY, null,
				Pay1SQLiteHelper.QP_TRANSATION_FLAG + " = " + flag + " AND "
						+ Pay1SQLiteHelper.QP_OPERATOR_CODE + " !='null' ",
				null, null, null, "datetime("
						+ Pay1SQLiteHelper.QP_TRANSATION_DATE + ") desc ");

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			QuickPay quickPay = cursorToQuickPay(cursor);
			quickPays.add(quickPay);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return quickPays;
	}

	public QuickPay getQuickPay(int id) {

		Cursor cursor = database.query(Pay1SQLiteHelper.TABLE_QUICKPAY, null,
				Pay1SQLiteHelper.QP_ID + " = " + id, null, null, null, null);

		if (cursor.getCount() > 0) {
			cursor.moveToFirst();
			QuickPay quickPay = cursorToQuickPay(cursor);
			cursor.moveToNext();
			// make sure to close the cursor
			cursor.close();
			return quickPay;
		} else {
			return null;
		}

	}

	public QuickPay getTopQuickPay() {

		Cursor cursor = database.query(Pay1SQLiteHelper.TABLE_QUICKPAY, null,
				null, null, null, null, Pay1SQLiteHelper.QP_UPDATE_TIME
						+ " desc limit 1");

		if (cursor.getCount() > 0) {
			cursor.moveToFirst();
			QuickPay quickPay = cursorToQuickPay(cursor);
			cursor.moveToNext();
			// make sure to close the cursor
			cursor.close();
			return quickPay;
		} else {
			return null;
		}

	}

	private QuickPay cursorToQuickPay(Cursor cursor) {
		QuickPay quickPay = new QuickPay();
		quickPay.setQuickPayID(cursor.getInt(0));
		quickPay.setQuickPayFlag(cursor.getInt(1));
		quickPay.setQuickPayName(cursor.getString(2));
		quickPay.setQuickPayAmount(cursor.getInt(3));
		quickPay.setQuickPaySTV(cursor.getInt(4));
		quickPay.setQuickPayOperatorID(cursor.getInt(5));
		quickPay.setQuickPayProductID(cursor.getInt(6));
		quickPay.setQuickPayMobile(cursor.getString(7));
		quickPay.setQuickPayDelegateFlag(cursor.getInt(8));
		quickPay.setQuickPayMissCallNumber(cursor.getString(9));
		quickPay.setQuickPayOperatorName(cursor.getString(10));
		quickPay.setQuickPayOperatorCode(cursor.getString(11));
		quickPay.setQuickPayUptateTime(cursor.getLong(12));
		quickPay.setQuickPayTransactionDate(cursor.getString(13));
		quickPay.setQuickPayTransactionFlag(cursor.getInt(14));
		return quickPay;
	}
}
