<?php $this->load->view('common/doctype_html'); 
    $current_page_url =  "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    $meteTagData = getMetaTagDetails();
    foreach ($meteTagData as $row){
      
      if($row->page_url == $current_page_url){
        $new_page_tittle =  $row->page_tittle;
        $new_page_meta_description =  $row->page_meta_description;  
        $new_page_meta_tag =  $row->page_meta_tag;  
      }
      
    }
?>
<meta name="description" content="<?php if($new_page_meta_description){ echo $new_page_meta_description; }else{ ?>Get through the payment gateway to pass your successful transactions. Pay1 is a convenient app for Mobile, DTH, Data Card recharge & bill payment.   <?php } ?>">
<meta name="keywords" content="<?php if($new_page_meta_tag){ echo $new_page_meta_tag; }else{ ?>Payments , Pay1  <?php } ?>" />
<title><?php if($new_page_tittle){ echo $new_page_tittle; }else{ ?>Payments | Pay1  <?php } ?></title>   
<link rel="stylesheet" href="/assets/css/style.css">
<?php $this->load->view('common/header');  ?>
<?php
    $operatorName = "";
    $product_desc = "";
    if($productinfo=='refill' && !isset($category)){
        $product_desc = "Wallet Refill";
    }else if($productinfo=='refill' && isset($category) && $category=='deal'){
        $product_desc = "E-Voucher";
    }
    else if(($productinfo=='recharge' || $productinfo=='billing') && isset($product) && !empty($product)){
        
            $product_desc = ($productinfo=='recharge') ? 'Recharge' : 'Bill Payment';
            
    }else if($productinfo=='deal'){
        $product_desc = "E-Voucher";
    }else{
        $product_desc = '';
    }
    if(isset($partial)){
        $product_desc = "Wallet Balance";
    }
     $operatorName = isset($operator) ?  $operator: "";
?>
<form id="postpayu" name="postpayu" method="post" action="<?php echo $B2C_SUBMIT_URL; ?>" >
    <?php //var_dump($UserCardDetails);
    if($UserCardDetails['status']==1){ $i=0;
        foreach($UserCardDetails['user_cards'] as $row){
            if($row['is_expired']==0){
                $card_mode = $row['card_mode']=='CC' ? 'c_' : 'd_';
           ?>
    <input type="hidden" id="<?php echo $card_mode."name_on_card".$i; ?>" name="<?php echo $card_mode."name_on_card".$i; ?>" value="<?php echo $row['name_on_card']; ?>">
    <input type="hidden" id="<?php echo $card_mode."card_token".$i; ?>" name="<?php echo $card_mode."card_token".$i; ?>" value="<?php echo $row['card_token']; ?>">
    <input type="hidden" id="<?php echo $card_mode."card_no".$i; ?>" name="<?php echo $card_mode."card_no".$i; ?>" value="<?php echo $row['card_no']; ?>">
    
    <?php
           $i++;
            }
        }
    }
    ?>
    <div id="hidden_elm" >
    </div>
    <input type="hidden" name="key" id ="key" value="<?php echo $key; ?>" />
    <input type="hidden" id="card_name" name="card_name" value=""  />
    <input type="hidden" id="store_card" name="store_card" value="0" />
<!--input type="hidden" name="salt" value="<?php //echo $salt; ?>" /-->
<input type="hidden" name="email" id ="email" value="<?php echo $email; ?>" />
<input type="hidden" name="user_credentials" id ="user_credentials" value="<?php echo $user_credentials; ?>" />
<input type="hidden" name="hash" id ="hash" value="<?php echo $hash; ?>" />
<input type="hidden" name="txnid" id ="txnid" value="<?php echo $txnid; ?>" />
<input type="hidden" name="phone" id ="phone" value="<?php echo $phone; ?>" />
<input type="hidden" name="pg" id ="pg" value="DC" />

<input type="hidden" name="bankcode" id="bankcode" value="CC" />
<input type="hidden" name="cardtype" id ="cardtype"  id="cardtype" value="debit" >
<input type="hidden" name="amount" id ="amount"  value="<?php echo $amount; ?>" />
<input type="hidden" name="firstname" id ="firstname" value="<?php echo $firstname; ?>" />
<input type="hidden" name="surl" id ="surl" value="<?php echo $surl; ?>" />
<input type="hidden" name="curl" id ="curl" value="<?php echo $curl; ?>" />
<input type="hidden" name="furl" id ="furl" value="<?php echo $furl; ?>" />
<input type="hidden" name="productinfo" id ="productinfo" value="<?php echo $productinfo; ?>" />

<input type="hidden" name="ccnum" id ="ccnum" value="" />
<input type="hidden" name="ccname" id ="ccname" value="" />
<input type="hidden" name="ccvv" id ="ccvv" value="" />
<input type="hidden" name="ccexpmon" id ="ccexpmon" value="" />
<input type="hidden" name="ccexpyr" id ="ccexpyr" value="" />
<input class="form-control" type="hidden" name="store_card_token" id ="store_card_token" value="">
<input type="hidden" name="purchase_amount" id ="purchase_amount"  value="<?php echo $amount; ?>" />

<div class="content-section">
    <div class="content">
        <div class="container-fluid bg-color-white" >
            <div class="row margin-top-20" >
                <div class="col-md-12 margin-top-11" >
                            <div class="col-sm-12 col-md-9 margin-top-10 ">
                                <div class="row paymentoption_bg">
                                    <div class="col-sm-12 col-md-12">
                                       <h4><strong>PAYMENT OPTIONS</strong></h4>
                                    </div>
                                </div>
                                <!--class="active_payment"-->
                                <div class="row" >
<!--                                     <div class="col-sm-12 col-md-12" >-->
                                         <div class="col-sm-12 col-md-3 padding-top-20 bg-color-gray">
                                            <div id="payment_navigation_id" >
                                                        <ul class="tabs">
                                                                <li class="active "><a href="#savedcard"  data-toggle="tab">SAVED CARD(S)</a></li>
                                                                <li> <a href="#debit" onclick="enablePayment('debitcard');" data-toggle="tab">DEBIT CARD</a></li>
                                                                <li ><a href="#credit" data-toggle="tab" onclick="enablePayment('creditcard')">CREDIT CARD</a></li>
                                                                <li><a href="#netbank" data-toggle="tab" onclick="enablePayment('netbank');">NET BANKING</a></li>
                                                                <li><a href="#payu" data-toggle="tab" onclick="payumoney();">PAYU MONEY</a></li>
                                                        </ul>
                                                </div>
                                                <div class="clearfix"></div>
                                        </div>

                                            <div class="tab-content">
                                                <!--Saved card -->
                                                <div class="col-sm-12 col-md-9 border-left tab-pane active margin-top-20" id="savedcard"  style="background-color:initial;">
                                                    <?php if($UserCardDetails['status']==1){ ?>
                                                    <div class="col-sm-12 col-md-12 bg-color-gray" >
                                                        <label id="saved_card-error" class="error"  style="display:none">Please select card.</label>
                                                    <?php 
                                                        $nameOnCard ="";
                                                        $cardno = "";
                                                        $nickname = "";
                                                        $l=1;

                                                           foreach($UserCardDetails['user_cards'] as $row){
                                                                    $nameOnCard = $row['name_on_card'];
                                                                    $nickname = $row['card_name'];
                                                                    $cardno = $row['card_no'];
                                                                    $card_token= $row['card_token'];
                                                                    ?>
                                                                    <div class="row margin-top-30" >
                                                                        <input type="hidden" id="saved_card_token<?php echo $l; ?>" value="<?php echo $card_token; ?>">
                                                                        <div class="col-sm-4 col-md-4" >
                                                                            <input type="radio" <?php if($l==1){ ?> checked="checked" <?php } ?> name="saved_cards" value="<?php echo $l; ?>" /> &nbsp;&nbsp;&nbsp; <?php echo $cardno; ?>
                                                                        </div>
                                                                        <div class="col-sm-4 col-md-2" >
                                                                            <input type="password" id="saved_cvv<?php echo $l; ?>" name="saved_card_cvv" size="4" maxlength="4" placeholder="CVV" autocomplete="off" />
                                                                        </div>
                                                                        <div class="col-sm-6 col-md-6" >

                                                                        </div>
                                                                    </div>
                                                                    <?php $l++;
                                                            } 


                                                              ?>


                                                    <div class="row margin-top-20" >

                                                         <button type="modal"  style="margin-right:5px"  onclick="submit_savedCard();" class="confirm-payment " >Confirm Payment</button>


                                                    </div>
                                                    </div>
                                                    <?php }else{ ?>
                                                    <p></p>
                                                    <p></p>
                                                    <p></p>
                                                    <h3 class="text-center">CREDIT / DEBIT CARDS</h3>
                                                    <p class="text-center">You don’t have any<br> Saved Cards!</p>
                                                    <?php } ?>
                                                </div>
                                                <!--debit card -->

                                                <div class="col-sm-12 col-md-9 border-left tab-pane padding-top-15" id="debit"  style="background-color:initial;">
                                                    <div class="col-sm-12 col-md-12 bg-color-gray" >
                                                        <div class="row">

                                                                <div class="col-md-4 col-sm-12 margin-top-10">
                                                                    <label class="paymentoption_details">Card Type</label> <!--id="d_CardType" name="d_CardType"-->
                                                                        <select  class="card_type " id="d_CardType" name="d_CardType" onclick="selectCard('debit')"  >
                                                                                <option value="">Select</option>
                                                                                   <option value="VISA">Visa card (All Banks)</option>
                                                                                   <option value="MAST">Mastercard (All Banks)</option>
                                                                                   <!--option value="SMAE">SBI Maestro card</option-->
                                                                                   <option value="MAES">Maestro card</option>
                                                                                   <option value="CITD">Citi Debit card</option>
                                                                       </select>
                                                                        <label id="d_CardType-error" class="error" for="d_CardType" style="display:none">This field is required.</label>
                                                                </div>
                                                                <div class="col-md-4 col-sm-12 margin-top-10">
                                                                        <label class="paymentoption_details">Debit Card Number</label><!--id="d_num" name="d_num"-->
                                                                        <input type="text" id="d_num" name="d_num" onblur="validate_form('d_')" class="card_number" placeholder="xxxx-xxxx-xxxx-xxxx" onblur="validate_card('d_')" autocomplete="off" maxlength="19" >
                                                                        <label id="d_num-error" class="error" for="debit" style="display:none">This field is required.</label>
                                                                </div>
                                                                <div class="col-md-4 col-sm-12 margin-top-10">
                                                                        <label class="paymentoption_details">Card Holder Name</label>
                                                                        <input type="text"  onblur="validate_form('d_')" class="card_number" name="d_name" id="d_name"  size="20" autocomplete="off">
                                                                        <label id="d_name-error" class="error" for="d_name" style="display:none">This field is required.</label>
                                                                </div>
                                                        </div>
                                                           <div class="row margin-top-10">
                                                                <div class="col-md-4 col-sm-12 " ><!--id="d_num" name="d_num"-->
                                                                        <label class="paymentoption_details">Expiry</label>
                                                                        <select class="expiry" id="d_expmon" name="d_expmon" >
                                                                                <option value="01">Jan(01)</option>
                                                                                <option value="02">Feb(02)</option>
                                                                                <option value="03">Mar(03)</option>
                                                                                <option value="04">Apr(04)</option>
                                                                                <option value="05">May(05)</option>
                                                                                <option value="06">Jun(06)</option>
                                                                                <option value="07">Jul(07)</option>
                                                                                <option value="08">Aug(08)</option>
                                                                                <option value="09">Sept(09)</option>
                                                                                <option value="10">Oct(10)</option>
                                                                                <option value="11">Nov(11)</option>
                                                                                <option value="12">Dec(12)</option>
                                                                        </select>
                                                                            <!--id="d_expyr" name="d_expyr"-->
                                                                        <select class="expiry" id="d_expyr" name="d_expyr" >
                                                                                <?php
                                                                                $year = date('Y');
                                                                                for($i=$year;$i< $year+30;$i++){
                                                                                    echo "<option value='".$i."'>$i</option>";
                                                                                }

                                                                                ?>
                                                                        </select>

                                                                </div> <!--name="d_cvv" id="d_cvv"-->
                                                                <div class="col-md-4 col-sm-6" >
                                                                        <label class="paymentoption_details">CVV2/CVC2</label>
                                                                        <input type="password" size="4" maxlength="4" onblur="validate_form('d_')" class=""  placeholder="xxx" name="d_cvv" id="d_cvv" autocomplete="off">
                                                                        <label id="d_cvv-error" class="error" for="d_cvv" style="display:none">This field is required.</label>
                                                                </div>
                                                        </div>

                                                        <div class="row margin-top-10">
                                                                <div class="col-md-12 margin-top-10" id="d_span_storecard" >
                                                                <input type="checkbox" id="d_store_card" onclick="addlabel('d');" name="d_store_card" class="">Save my card for faster checkout in future.
                                                                        <input type="text" placeholder="Name for this card" class="margin-top-10"  id="d_card_name" name="d_card_name">
                                                                </div>

                                                        </div>


                                                        <div class="row margin-top-20" >

                                                                <button type="modal"  style="margin-right:5px"  onclick="checkCard('d');" class="confirm-payment disabled" id="d_payment" disabled="disabled">Confirm Payment</button>
                                                                <input type="button" onclick="cancel_transaction();" class="cancel-payment" style="text-align:center" value="Cancel"> 

                                                        </div>
                                                    </div>

                                                </div>

                                                <!--debit card End -->


                                                <!--credit card  -->
                                                <div class="col-sm-12 col-md-9 border-left tab-pane padding-top-15" id="credit"  style="background-color:initial;">
                                                    <div class="col-sm-12 bg-color-gray" >
                                                        <div class="row">
                                                                <div class="col-md-4 col-sm-12 margin-top-10">
                                                                        <label class="paymentoption_details">Card Type</label>
                                                                        <select class="card_type" id="c_CardType" name="c_CardType" onchange="selectCard('credit')">
                                                                                <option value="">Select</option>
                                                                                <option value="CC">Visa/Master</option>
                                                                                <option value="AMEX">AMEX cards</option>
                                                                                <option value="DINR">Diners</option>
                                                                        </select>
                                                                        <label id="c_CardType-error" class="error" for="c_CardType" style="display:none">This field is required.</label>
                                                                </div>
                                                                <div class="col-md-4 col-sm-12 margin-top-10">
                                                                        <label class="paymentoption_details">Credit Card Number</label>
                                                                        <input name="c_num"  onblur="validate_card('c_')" value="" id="c_num" type="text" class="card_number read_only"  placeholder="xxxx-xxxx-xxxx-xxxx" autocomplete="off" maxlength="19">
                                                                        <label id="c_num-error" class="error" for="credit" style="display:none">This field is required.</label>
                                                                </div>
                                                                <div class="col-md-4 col-sm-12 margin-top-10">
                                                                        <label class="paymentoption_details">Card Holder Name</label>
                                                                        <input type="text"  name="c_name" onblur="validate_form('c_')"  id="c_name" autocomplete="off" class="card_number read_only" size="20" data-prompt-position="bottomRight"   value="">
                                                                        <label id="c_name-error" class="error" for="c_name" style="display:none">This field is required.</label>
                                                                </div>
                                                        </div>
                                                        <div class="row margin-top-10">
                                                                <div class="col-md-4 col-sm-12">
                                                                        <label class="paymentoption_details">Expiry</label>
                                                                        <select class="expiry read_only" id="c_expmon" name="c_expmon" >
                                                                                <option value="01">Jan(01)</option>
                                                                                <option value="02">Feb(02)</option>
                                                                                <option value="03">Mar(03)</option>
                                                                                <option value="04">Apr(04)</option>
                                                                                <option value="05">May(05)</option>
                                                                                <option value="06">Jun(06)</option>
                                                                                <option value="07">Jul(07)</option>
                                                                                <option value="08">Aug(08)</option>
                                                                                <option value="09">Sept(09)</option>
                                                                                <option value="10">Oct(10)</option>
                                                                                <option value="11">Nov(11)</option>
                                                                                <option value="12">Dec(12)</option>
                                                                        </select>
                                                                        <select class="expiry read_only" id="c_expyr" name="c_expyr" >
                                                                                <?php
                                                                                    $year = date('Y');
                                                                                    for($i=$year;$i< $year+30;$i++){
                                                                                        echo "<option value='".$i."'>$i</option>";
                                                                                    }
                                                                               ?>
                                                                        </select>
                                                                </div>
                                                                <div class="col-md-4 col-sm-6">
                                                                        <label class="paymentoption_details">CVV2/CVC2</label>
                                                                        <input type="password" onblur="validate_form('c_')"  name="c_cvv" id="c_cvv" maxlength="4" size=4  placeholder="xxx" autocomplete="off">
                                                                        <label id="c_cvv-error" class="error" for="c_cvv" style="display:none">This field is required.</label>
                                                                </div>
                                                        </div>
                                                        <div class="row margin-top-10">
                                                                <div class="col-md-12 col-sm-12 margin-top-10 storecard" id="c_span_storecard" >
                                                                        <input type="checkbox" onclick="addlabel('c');" id="c_store_card" name="c_store_card" class="">Save my card for faster checkout in future. 
                                                                        <input type="text" placeholder="Name for this card" class="margin-top-10 " id="c_card_name" name="c_card_name">

                                                                </div>

                                                        </div>
                                                        <div class="row margin-top-20">

                                                            <button id="c_payment" type="modal" style="margin-right:5px" onclick="checkCard('c');" class="confirm-payment"  disabled="disabled">Confirm Payment</button>
                                                                    <input type="button" onclick="cancel_transaction();" class="cancel-payment" style="text-align:center" value="Cancel"> 


                                                        </div>
                                                     </div>
                                                </div>

                                                <!--debit card End -->

                                                <!--Net banking start-->
                                                <div class="col-sm-12 col-md-9 border-left tab-pane padding-top-15" id="netbank"  style="background-color:initial;">
                                                    <div class="col-sm-12 col-md-12 bg-color-gray">
                                                        <div class="row margin-top-10">
                                                                <div class="col-md-6 col-sm-12 margin-top-10" >
                                                                    <div class="bank_opt">
                                                                            <input type="radio" name="bank" class="" value="ICIB"><img src="/assets/images/banks/icici_logo.png">
                                                                    </div>
                                                                 </div>
                                                                <div class="col-md-6 col-sm-12 margin-top-10" >
                                                                    <div class="bank_opt">
                                                                            <input type="radio" name="bank" class="" value="HDFB"><img src="/assets/images/banks/hdfc_logo.png">
                                                                    </div>
                                                               </div>

                                                       </div>
<!--                                                        <div class="row">
                                                            <div class="col-sm-12 col-md-12">
                                                                    <div class="bank_opt">
                                                                            <input type="radio" name="bank" class="" value="SBIB"><img src="/assets/images/banks/sbi_logo.png">
                                                                    </div>
                                                            </div>
                                                        </div>-->
                                                        <div class="row margin-top-10">
                                                            <div class="col-md-6 col-sm-12 ">
                                                                <select class="card_type" id="netbanking_select" name="netbanking_select" >
                                                                            <option value="">All other banks</option>

                                                                        <?php foreach($banklist as $list){ ?>

                                                                            <option value="<?php echo $list['code']; ?>"><?php echo $list['bank']; ?></option>
                                                                      <?php  } ?>
                                                                    </select>
                                                                    <label id="netbanking_select-error" class="error" for="netbanking_select" style="display:none;text-align: right">Select your Bank.</label>
                                                            </div>

                                                         </div>
                                                        <div class="row margin-top-20" >
                                                              <div class="col-md-12 col-sm-12 ">
                                                                    <button type="modal"  style="margin-right:5px"  onclick="checkCard('netbank');" class="confirm-payment" id="cpayment3" disabled="disabled">Confirm Payment</button>
                                                                    <input type="button" onclick="cancel_transaction();" class="cancel-payment" style="text-align:center" value="Cancel"> 

                                                           </div>
                                                        </div>
                                                   </div>
                                            </div>
                                            <!--Net banking end-->
                                            <!-- payu start -->

                                                <div class="col-sm-12 col-md-9 tab-pane border-left margin-top-20" id="payu">
                                                <div class="row">
                                                    <div class="col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1">
                                                        <p></p>
                                                        <p></p>
                                                        <p></p>
                                                        <p></p>
                                                        <p class="text-center">In the next step you will be redirected to PayUMoney website.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            </div>
                                    </div>
<!--                                </div>-->
                            </div>
                   
                            <div class="col-sm-12 col-md-3 margin-top-10 color-white bg-color-darkgray">
                                <h4><strong>PAYMENT SUMMARY</strong></h4>
                            </div>
                            <div class="row">
                                    <div class="col-sm-12 col-md-3 padding-top-15 payment-text-color payment-summary-bg" >
                                        <?php if(isset($partial) && isset($wallet_balance)){ ?> 
                                        <div class="row padding-top-10" >
                                                <div class="col-sm-6 col-md-6">Total Amount</div>
                                                <div class="col-sm-6 col-md-6 text-right"><?php echo "Rs. ".($wallet_balance+$amount); ?></div>
                                        </div>
                                        <?php } ?>
                                        <div class="row padding-top-10" >
                                            <div class="col-sm-6 col-md-6">
                                                <?php echo $product_desc; ?> <br>
                                                <small><?php echo $operatorName; ?></small>
                                            </div>
                                            <div class="col-sm-6 col-md-6 text-right">
                                                Rs. <?php if(isset($partial) && isset($wallet_balance)){ echo round($wallet_balance); }elseif(isset($actual_price) && isset($offer_price)){ echo $actual_price; }else{ echo round($amount); } ?>
                                            </div>
                                        </div>
                                        <div class="row padding-top-10" >
                                                <div class="col-sm-6 col-md-6">Transaction ID</div>
                                                <div class="col-sm-6 col-md-6 text-right"><?php echo $txnid; ?></div>
                                        </div>
                    <!--                    <div class="row padding-top-10" >
                                                      <div class="col-sm-6 col-md-6">Promo Code</div>
                                                      <div class="col-sm-6 col-md-6 text-right">CB02</div>
                                              </div>-->
                                         <?php if(isset($actual_price) && isset($offer_price)){ ?>
                                        <div class="row padding-top-10" >
                                            <div class="col-sm-12 col-md-12">

                                            <div class="col-sm-12 col-md-12 payment-text-color bg-color-white padd_all" style="border-radius: 10px;">
                                                <div class="row">
                                                    <div class="col-sm-6 col-md-6">Subtotal</div>
                                                    <div class="col-sm-6 text-right">Rs.  <?php  echo $actual_price;  ?></div>

                                                    <div class="col-sm-6 col-md-6">Discount </div>
                                                    <div class="col-sm-6 col-md-6 text-right">-<?php  echo ($actual_price-$offer_price);  ?></div>   
                                                </div>
                                            </div>
                                          </div>
                                         </div> <?php } ?>
                                        <div class="row color-white bg-color-pink">
                                            <div class="col-sm-6 col-md-6"><h5><strong>AMOUNT PAYABLE</strong></h5></div>
                                                      <div class="col-sm-6 col-md-6 text-right"><h5><strong>Rs. <?php echo $amount; ?></strong></h5></div>
                                              </div>

                                 </div>
                            </div>
                    </div>
            </div>
            <div class="row margin-top-10">
                <div class="col-sm-9 col-sm-offset-3 col-md-9 col-md-offset-3">
                        <img src="/assets/images/secured/3d_secure.png">
                </div>
             </div>
          </div>
    </div>
</div>
            <!-- payu end -->
            </form>

           

       
                    

<script>
     
    function selectCard(type){
        var card = "";
        if(type=='credit'){
            card = 'c_';
            
        }else if(type=='debit'){
             card = 'd_';
        }
        var val = $("#"+card+"CardType").val(); 
        if($.isNumeric(val)){
            $("#"+card+"name").val($("#"+card+"name_on_card"+val).val());
            $("#store_card_token").val($("#"+card+"card_token"+val).val());
            //console.log(card+"cnum "+$("#"+card+"card_no"+val).val());
            $("#"+card+"num").val($("#"+card+"card_no"+val).val());
            $("#"+card+"name").attr("disabled","disabled");
            $("#"+card+"num").attr("disabled","disabled");
           // $("."+card+"hideExpiry").css('display','block');
            $("#"+card+"expmon").attr("disabled","disabled");
            $("#"+card+"expyr").attr("disabled","disabled");
            $("#"+card+"span_storecard").css('display','none');
        }else{
            $("#"+card+"name").val('');
            $("#store_card_token").val('');
            $("#"+card+"num").val('');
            $("#"+card+"name").removeAttr("disabled");
            $("#"+card+"num").removeAttr("disabled");
            $("#"+card+"expmon").removeAttr("disabled");
            $("#"+card+"expyr").removeAttr("disabled");
            //$("."+card+"hideExpiry").css('display','none');
            $("#"+card+"span_storecard").css('display','block');
        }
        if(val==''){
            
        }
    }    
    
    function validate_form(type){
        if($("#"+type+"CardType").val()==''){
            $("#"+type+"CardType-error").css('display','block');
        }else{
            $("#"+type+"CardType-error").css('display','none');
            
        }
        
        if($("#"+type+"name").val()==''){
            $("#"+type+"name-error").css('display','block');
        }else{
            $("#"+type+"name-error").css('display','none');
        }
        if($("#"+type+"num").val()==''){
            $("#"+type+"num-error").css('display','block');
        }else{
            $("#"+type+"num-error").css('display','none');
            validate_card(type);
        }
        if($("#"+type+"cvv").val()==''){
            $("#"+type+"cvv-error").css('display','block');
        }else{
            $("#"+type+"cvv-error").css('display','none');
        }
        if($("#"+type+"CardType").val()!='' && $("#"+type+"num").val()!='' && $("#"+type+"name").val()!='' && $("#"+type+"cvv").val()!=''){
            $("#"+type+"CardType-error").css('display','none');
            $("#"+type+"num-error").css('display','none');
            $("#"+type+"name-error").css('display','none');
            $("#"+type+"cvv-error").css('display','none');
            $("#"+type+"payment").prop('disabled', false);
            validate_card(type);
        }
        
    }
    
    function submit_savedCard(){
        var suffix = 0;
        $("#saved_card-error").hide();
        suffix = $('input[name=saved_cards]:checked').val();
        //console.log('suffix '+suffix);
        if(suffix==0 || $.type(suffix)==='undefined'){
            $("#saved_card-error").html('please select card for payment');
            $("#saved_card-error").show();
            event.preventDefault();
            return false;
        }
        if($("#saved_cvv"+suffix).val()==''){
             $("#saved_card-error").html('please enter cvv to proceed');
            $("#saved_card-error").show();
            event.preventDefault();
            return false;
        }
        $("#store_card_token").val($("#saved_card_token"+suffix).val());
        $("#ccvv").val($("#saved_cvv"+suffix).val()); 
        $("#postpayu").submit();
    }
    
    function checkCard(card){
        var errorFlag=0;
//          if($("#cardtype").val()=='credit'){
//               card = 'c';
//          }else if($("#cardtype").val()=='debit'){
//               card = 'd';
//          }else 
          if(card=='netbank'){
              paybyNetBank();
          }
           
            if(card!='' ){
                if($("#"+card+"_cvv").val()=='' && $("#"+card+"_cvv").length!=3){
                   // console.log('_carddetailserror '+card);
                     $("#"+card+"_carddetailserror").show();
                     errorFlag=1;
                }else{
                       $("#ccnum").val($("#"+card+"_num").val());
                        $("#ccname").val($("#"+card+"_name").val());
                        $("#ccvv").val($("#"+card+"_cvv").val()); 
                        $("#ccexpmon").val($("#"+card+"_expmon").val());
                        $("#ccexpyr").val($("#"+card+"_expyr").val());
                         var cardToken =  $("#store_card_token").val();
                         var val = $("#"+card+"_CardType").val(); 
                        // console.log('cardToken '+cardToken+'val'+val);
                        if(cardToken =='' && typeof($("#"+card+"_card_token0").val())!=="undefined" && !isNaN(val)){
                            $("#store_card_token").val($("#"+card+"_card_token0").val());
                        }
                        if( $("#store_card_token").val()!=='' && errorFlag==0){
                             //console.log('cardToken '+cardToken+' val '+$("#ccvv").val()+' store_card_token '+$("#store_card_token").val());
                                $("#"+card+"_loading").css('display','block');
                                $("#postpayu").submit();
                             
                        }else{
                        
                            if($("#"+card+"_expmon").val()==''){
                                $("#"+card+"_carddetailserror").show();
                                errorFlag=1;
                            }
                            if($("#"+card+"_expyr").val()==''){
                                $("#"+card+"_carddetailserror").show();
                                errorFlag=1;
                            }
                            if(errorFlag===0){
                                
                               $("#"+card+"_num").validateCreditCard(function(result){
                                 if(result.luhn_valid){
                                       var cardName = $("#"+card+"_CardType").val();
                                        $("#bankcode").val(cardName);
                                        if($("#"+card+"_card_name").val()!=''){ 
                                            $("#card_name").val($("#"+card+"_card_name").val());
                                            $("#store_card").val('1');
                                        }
                                        $("#"+card+"_loading").css('display','block');
                                        $("#"+card+"num-error").css('display','none');
                                        $("#"+card+"payment").prop('disabled', false);
                                        $("#postpayu").submit();
                                   }
                                   else{
                                        $("#"+card+"num-error").html('Invalid card number');
                                        $("#"+card+"num-error").css('display','block');
                                        $("#"+card+"payment").prop('disabled', true);
                                        event.preventDefault();
                                   }
                               });
                               
                            }
                    }
                }
        }
    }
    
    function paybyNetBank(){
      var netbanking_select = $("#netbanking_select").val();
       $("#bankcode").val(netbanking_select);
        if(netbanking_select!=''){
            $("#netbanking_error").hide();
            $("#postpayu").submit();
        }
        else{
            $("#netbanking_error").show();
            event.preventDefault();
        }
    }
    function selectCardType(cardType,cardName){
        switch (cardType){
            case 'c_' :
                if(cardName=='visa' || cardName=='mastercard'){
                    $("#bankcode").val('CC');
                }
                else if(cardName=='amex'){
                    $("#bankcode").val('AMEX');
                }
                else if(cardName=='diners'){
                    $("#bankcode").val('DINR');
                }
            break;
            case 'd_' :
                if(cardName=='visa'){
                   $("#bankcode").val('VISA'); 
                }
                else if(cardName=='mastercard'){
                    $("#bankcode").val('MAST');
                }
                else if(cardName=='maestro'){
                    $("#bankcode").val('MAES');
                }
              break;

        }
        return  $("#bankcode").val();
    }
        
    function validate_card(type){ 
        if($("#"+type+"CardType").val()=='' || $("#"+type+"CardType").val()=='CC' || $("#"+type+"CardType").val()=='AMEX' || $("#"+type+"CardType").val()=='DINR' || $("#"+type+"CardType").val()=='VISA' || $("#"+type+"CardType").val()=='MAST' || $("#"+type+"CardType").val()=='SMAE'  || $("#"+type+"CardType").val()=='MAES'  || $("#"+type+"CardType").val()=='CITD')
         $('#'+type+'num').validateCreditCard(function(result){ 
            // console.log(result);
            if(result.luhn_valid){
                $("#"+type+"carddetailserror").hide();
                var dropdown = ''+type+'CardType';
               $('select[name="'+dropdown+'"]').find('option[value="'+selectCardType(type,result.card_type.name)+'"]').attr("selected",true);
               $("#"+type+"num-error").css('display','none');
               return true;
            }else{
                $("#"+type+"num-error").html('Invalid card number');
                $("#"+type+"num-error").css('display','block');
                $("#"+type+"payment").prop('disabled', true);
                return false;
            }
         });
    }
   
    $(function() {
    $('#mobilenum,#c_num,#c_cvv,#d_num,#d_cvv,[name=saved_card_cvv]').keydown(function(e) {
        
            if (e.shiftKey || e.ctrlKey || e.altKey) {
                e.preventDefault();
            } else {
                var key = e.keyCode;
                if (!((key == 8) || (key == 9) || (key == 16) || (key == 46) || (key >= 35 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105))) {
                    e.preventDefault();
                }
            }
        });
        
       $('#c_name,#d_name').keypress(function(e)
      {
          var code = e.keyCode ? e.keyCode:e.which; // Get the key code.
          
          var pressedKey = String.fromCharCode(code); // Find the key pressed.
          if(!pressedKey.match(/[a-zA-Z ]/g) && code!=8) // Check if it's a alphabet char or not.
          {
              e.preventDefault(); // If it is not then prevent the event from happening.
         
          }
      });
    });
    
    function paybyPayu(){
            var email =  $("#email").val();
           if(email!='' && validateEmail(email)==false){
                $("#emailerror").show();
                return false;
                
            }
            else{
                $("#emailerror").hide();
                $("#postpayu").submit();   
            }
        
    }
    
    function validateEmail(Email) {
        var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
        if (filter.test(Email)) {
            return true;
        }
        else {
            return false;
        }
    }
    
    function luhnChk() {
        var numbID = document.getElementById('ccnum');
        if(numbID && numbID.value!=''){
            var numb = numbID.value;
            var len = numb.length;
            var mul = 0;
            var prodArr = [[0, 1, 2, 3, 4, 5, 6, 7, 8, 9], [0, 2, 4, 6, 8, 1, 3, 5, 7, 9]];
            var sum = 0;

            while (len--) {
                sum += prodArr[mul][parseInt(numb.charAt(len), 10)];
                mul ^= 1;
            }
            
            var flag = (sum % 10 === 0 && sum > 0);
            if(flag==true){
                return flag;
            }
            else{
                alert('Pls check your card number');
                return flag;
            }
        }
        else {
            return true;
        }
    }
   
   function enablePayment(payMethod){
       $("#emailerror").hide();
       $("#carddetails_submit").hide();
       $("#paymentMethod").show(500);
       $("#paymentOption").hide(500);
       $("#payudetails").hide(500);
       $("#carddetails").hide(500);
       $("#d_carddetails").hide(500);
       $("#netbankdetails").hide(500);
        switch (payMethod){
            case 'creditcard':
                 $("#cardtype").val('credit');
                 $("#pg").val('CC');
                 $("#carddetails").show(500);
                  $("#CCCardType_p").show();
                  $("#carddetails_submit").show();
                break;
            case 'debitcard':
                $("#debitCardType_p").show();
                $("#cardtype").val('debit');
                $("#pg").val('DC');
                $("#d_carddetails").show(500);
                $("#carddetails_submit").show();
                break;
            case 'netbank':
                $("#cardtype").val('netbank');
                $("#pg").val('NB');
                $("#netbankdetails").show(500);
                break;
            case 'payu':
                $("#cardtype").val('');
                $("#pg").val('-');
                $("#payudetails").show(500);
                break;
        }
    }
    
    function select_netbanking(ElemObj){
        var bankcode = ElemObj.value;
         $("#bankcode").val(bankcode);
    }
    function displayPayOption(){
        $("#emailerror").hide();
        $("#paymentMethod").hide(500);
        $("#paymentOption").show(500);
    }
    function addlabel(card){
        if( document.getElementById(card+"_store_card").checked){
           // $("#card_label_p").show(500);
            $("#"+card+"_store_card").val(1);
        }else{
           // $("#card_label_p").hide();
            $("#"+card+"_store_card").val(0);
        }
    }
    function d_addlabel(){
        if( document.getElementById("d_store_card").checked){
            $("#d_card_label_p").show(500);
            $("#d_store_card").val(1);
        }else{
            $("#d_card_label_p").hide();
            $("#d_store_card").val(0);
        }
    }
    
    function payumoney(){
        $("#pg").val("Wallet");
        $("#bankcode").val("payuw");
        $("#postpayu").submit();
    }
    
    
    $("[name='bank']").on('click', function(){
        var val = $("input:radio[name=bank]:checked").val();
        $("#netbanking_select").val(val);
         $("#netbanking_select-error").css('display','none');
                $("#cpayment3").prop('disabled', false);
    });
    
    $("#netbanking_select").on('change', function(){
        if($("#netbanking_select"). val()==''){
            $("#netbanking_select-error").css('display','block');
                $("#cpayment3").prop('disabled', true);
        }else{
            var val = $("#netbanking_select"). val();
            if($("input:radio[name=bank]:checked").length && $("input:radio[name=bank]:checked").val()!=val){
                $("input:radio[name=bank]").attr('checked',false);
                //$("input:radio[name=bank]").checked=false;
            }
            
            if(val=='ICIB' || val=='HDFB'){
                $("input:radio[name=bank][value="+val+"]").prop('checked', true);
            }
            if(val!='ICIB' || val!='HDFB')
            $("#netbanking_select-error").css('display','none');
                $("#cpayment3").prop('disabled', false);
        }
    });
    
    function cancel_transaction(){
        var amount =  $("#purchase_amount").val();
        var transaction_id =  $("#txnid").val();
        b2c.core.cancel_transaction(amount,transaction_id);
        window.location='/'
    }
    
</script>
<?php echo $this->load->view('common/footer'); ?>