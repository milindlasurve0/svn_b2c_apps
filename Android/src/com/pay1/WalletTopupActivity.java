package com.pay1;

import java.net.URLEncoder;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import com.pay1.customviews.MyProgressDialog;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.pay1.adapterhandler.InfoAdapter;
import com.pay1.constants.Constants;
import com.pay1.requesthandler.RequestClass;
import com.pay1.utilities.Utility;

public class WalletTopupActivity extends AppBaseActivity {

	private final String TAG = "Wallet Topup";
	private ListView listView_Info;
	private ArrayList<String> list;
	private InfoAdapter adapter;
	private ImageView imageView_Back;
	private TextView textView_Title;

	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.wallet_topup_activity);
		try {

			registerBaseActivityReceiver();

			Typeface Reguler = Typeface.createFromAsset(getAssets(),
					"EncodeSansNormal-400-Regular.ttf");

			textView_Title = (TextView) findViewById(R.id.textView_Title);
			textView_Title.setTypeface(Reguler);

			imageView_Back = (ImageView) findViewById(R.id.imageView_Back);
			imageView_Back.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onBackPressed();
				}
			});

			listView_Info = (ListView) findViewById(R.id.listView_Info);
			list = new ArrayList<String>();
			list.clear();
			list.add("Shop locator");
			list.add("Debit/Credit card");
			list.add("Coupon code");
			adapter = new InfoAdapter(WalletTopupActivity.this, list);
			listView_Info.setAdapter(adapter);
			listView_Info
					.setOnItemClickListener(new AdapterView.OnItemClickListener() {

						@Override
						public void onItemClick(AdapterView<?> parent,
								View view, int position, long id) {
							// TODO Auto-generated method stub
							switch (position) {
							case 0:
								boolean gps_enabled = false;
								boolean network_enabled = false;
								LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
								try {
									gps_enabled = lm
											.isProviderEnabled(LocationManager.GPS_PROVIDER);
								} catch (Exception ex) {
								}
								try {
									network_enabled = lm
											.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
								} catch (Exception ex) {
								}

								// don't start listeners if no provider is
								// enabled
								if (gps_enabled || network_enabled) {
									startActivity(new Intent(
											WalletTopupActivity.this,
											ShopLocatorActivity.class));
								} else {
									Constants
											.showTwoButtonDialog(
													WalletTopupActivity.this,
													TAG,
													"To enhance your Shop Locator experience:\n\n1.Turn on GPS and mobile network location\n2.Turn on Wi-Fi",
													Constants.DIALOG_CLOSE_LOCATION);
								}
								break;
							case 1: {
								String str = "";
								try {
									Bundle bundle = getIntent().getExtras();
									if (bundle != null) {
										str = String.valueOf(bundle
												.getDouble("AMOUNT"));
										str = String.valueOf(str.subSequence(0,
												str.indexOf(".")));
									}
								} catch (Exception e) {
									str = "";
								}
								Intent intent = new Intent(
										WalletTopupActivity.this,
										DebitCreditActivity.class);

								intent.putExtra("AMOUNT", str);
								startActivity(intent);
							}
								break;
							case 2: {
								String str = "";
								try {
									Bundle bundle = getIntent().getExtras();
									if (bundle != null) {
										str = String.valueOf(bundle
												.getDouble("AMOUNT"));
										str = String.valueOf(str.subSequence(0,
												str.indexOf(".")));
									}
								} catch (Exception e) {
									str = "";
								}
								Intent intent = new Intent(
										WalletTopupActivity.this,
										CouponCodeActivity.class);
								intent.putExtra("AMOUNT", str);
								startActivity(intent);
							}
								break;
							default:
								break;
							}

						}
					});

		} catch (Exception e) {
		}
		overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unRegisterBaseActivityReceiver();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (Constants.isExpanded) {
			Constants.collapsView(this);
		} else {
			super.onBackPressed();
			overridePendingTransition(R.anim.slide_in_left,
					R.anim.slide_out_right);
		}
	}

	public class WalletTopupTask extends AsyncTask<String, String, String>
			implements OnDismissListener {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			try {
				String response = RequestClass
						.getInstance()
						.readPay1B2CRequest(
								WalletTopupActivity.this,
								Constants.B2C_URL
										+ "refillwallet/?mobile_number="
										+ URLEncoder.encode(params[0], "utf-8")
										+ "&amount="
										+ URLEncoder.encode(params[1], "utf-8"));

				return response;

			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			try {

				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONObject jsonObject = new JSONObject(replaced);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {

						JSONObject jsonObject2 = jsonObject
								.getJSONObject("description");

						Utility.setBalance(WalletTopupActivity.this,
								Constants.SHAREDPREFERENCE_BALANCE,
								jsonObject2.getString("closing_balance"));
						Constants.showOneButtonDialog(
								WalletTopupActivity.this,
								TAG,
								"Wallet topup successful.\nYour transaction ID is"
										+ jsonObject2
												.getString("transaction_id"),
								Constants.DIALOG_CLOSE_CONFIRM);

					} else {
						Constants.showOneButtonDialog(WalletTopupActivity.this,
								TAG, Constants.checkCode(replaced),
								Constants.DIALOG_CLOSE);
					}

				} else {
					// Constants.showOneButtonDialog(WalletTopupActivity.this,
					// TAG, Constants.ERROR_INTERNET,
					// Constants.DIALOG_CLOSE);
					Intent intent = new Intent(WalletTopupActivity.this,
							ConnectivityErrorActivity.class);
					intent.putExtra(Constants.RECHARGE_FOR,
							Constants.WALLET_PAYMENT);
					startActivity(intent);

				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonDialog(WalletTopupActivity.this, TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonDialog(WalletTopupActivity.this, TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(WalletTopupActivity.this);
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							WalletTopupTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			WalletTopupTask.this.cancel(true);
			dialog.cancel();
		}

	}

	public class WalletTopupPGTask extends AsyncTask<String, String, String>
			implements OnDismissListener {
		private MyProgressDialog dialog;
		String url;

		@Override
		protected String doInBackground(String... params) {
			try {
				String response = RequestClass.getInstance()
						.readPay1B2CRequest(
								WalletTopupActivity.this,
								Constants.B2C_URL
										+ "online_walletrefill/?amount="
										+ params[0]);

				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onCancelled() {
			// TODO Auto-generated method stub
			super.onCancelled();
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			try {

				if (!result.startsWith("Error")) {
					// result = result.replaceAll("\\\\", "");
					JSONObject jsonObject = new JSONObject(result);

					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {
						JSONObject description = jsonObject
								.getJSONObject("description");

						Intent intent = new Intent(WalletTopupActivity.this,
								PGActivity.class);

						intent.putExtra(Constants.MOBILE_NUMBER, "");
						// intent.putExtra(Constants.RECHARGE_AMOUNT,
						// editText_Amount.getText().toString().trim());

						intent.putExtra(Constants.RECHARGE_FOR,
								Constants.WALLET_PAYMENT);
						intent.putExtra("CONTENT",
								description.getString("form_content")
										.replaceAll("\\\\", ""));

						startActivity(intent);
						finish();
					}
				} else {
					// Constants.showOneButtonDialog(WalletTopupActivity.this,
					// TAG, Constants.ERROR_INTERNET,
					// Constants.DIALOG_CLOSE_ORDER);
					Intent intent = new Intent(WalletTopupActivity.this,
							ConnectivityErrorActivity.class);
					intent.putExtra(Constants.RECHARGE_FOR,
							Constants.WALLET_PAYMENT);
					startActivity(intent);

				}
			} catch (JSONException e) {

			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(WalletTopupActivity.this);
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(false);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							WalletTopupPGTask.this.cancel(true);
							dialog.cancel();
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			WalletTopupPGTask.this.cancel(true);
			dialog.cancel();
		}

	}
}
