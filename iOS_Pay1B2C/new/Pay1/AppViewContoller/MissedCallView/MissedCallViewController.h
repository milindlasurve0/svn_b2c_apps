//
//  MissedCallViewController.h
//  Pay1
//
//  Created by Annapurna on 14/07/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "BaseViewController.h"
@class MissedCallCustomCell;
@interface MissedCallViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate>{
    IBOutlet MissedCallCustomCell *objMissedCallCell;
   
    IBOutlet UILabel *txtLabel;
   
    
    NSMutableArray *quickPayNumberArray;
    NSMutableArray *quickPayOperatorNameArray;
    NSMutableArray *quiclPayAmountArray;
    NSMutableArray *operatorIDArray;
    NSMutableArray *quickPayDelegateArray;
    NSMutableArray *quickPayFlagArray;
    NSMutableArray *quickPayIDArray;
    NSMutableArray *quickPaySTVArray;
    NSMutableArray *quickPayProductIDArray;
    NSMutableArray *quickPayMissedCallArray;
    NSMutableArray *quickPayNameArray;

    
    IBOutlet UITableView *missedTV;
    
    NSInteger indexRow;
    
    
    IBOutlet UILabel *noMissedCallFound;
    
    IBOutlet UIView *rechargeMsgView;
    

    

}

@end
