//
//  RechargeCriteria.h
//  Pay1
//
//  Created by Annapurna on 15/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EnumType.h"
@interface RechargeCriteria : NSObject

@property (nonatomic, strong) NSString *userMobileNumber;
@property (nonatomic, strong) NSString *userSubscriberID;
@property (nonatomic, strong) NSString *operatorID;
@property (nonatomic, strong) NSString *flag;
@property (nonatomic, strong) NSString *amount;
@property (nonatomic, strong) NSString *paymentOption;
@property (nonatomic, strong) NSString *payment;
@property (nonatomic, strong) NSString *stv;
@property (nonatomic, strong) NSString *partial;

@property (nonatomic, strong) NSString *baseAmount;
@property (nonatomic, strong) NSString *serviceCharge;
@property (nonatomic, strong) NSString *serviceTax;



@property (nonatomic, assign) ServiceType serviceType;


@end
