package com.pay1.adapterhandler;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.WeakHashMap;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.pay1.R;
import com.pay1.SupportHistoryActivity;

public class SupportHistoryAdapter extends BaseAdapter {

	private Context context;
	private ArrayList<WeakHashMap<String, String>> data;
	private Typeface Reguler, Narrow, Narrow_thin;

	public SupportHistoryAdapter(Context context,
			ArrayList<WeakHashMap<String, String>> data) {
		this.context = context;
		this.data = data;
		Reguler = Typeface.createFromAsset(context.getAssets(),
				"EncodeSansNormal-400-Regular.ttf");

		Narrow = Typeface.createFromAsset(context.getAssets(),
				"EncodeSansNarrow-500-Medium.ttf");

		Narrow_thin = Typeface.createFromAsset(context.getAssets(),
				"EncodeSansNarrow-100-Thin.ttf");
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder viewHolder;
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.support_history_adapter,
					null);
			viewHolder = new ViewHolder();

			viewHolder.textView_Title = (TextView) convertView
					.findViewById(R.id.textView_Title);
			viewHolder.textView_Title.setTypeface(Narrow);
			viewHolder.textView_Time = (TextView) convertView
					.findViewById(R.id.textView_Time);
			viewHolder.textView_Time.setTypeface(Narrow_thin);
			convertView.setTag(viewHolder);
			viewHolder.textView_Query = (TextView) convertView
					.findViewById(R.id.textView_Query);
			viewHolder.textView_Query.setTypeface(Narrow_thin);
			convertView.setTag(viewHolder);

		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		WeakHashMap<String, String> map = new WeakHashMap<String, String>();
		map = data.get(position);

		viewHolder.textView_Title.setText(map.get(SupportHistoryActivity.TITLE)
				.toString().trim());

		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat(
					"dd-MMM-yyyy,hh:mm aa");
			Date dt = new Date(Long.parseLong(map
					.get(SupportHistoryActivity.TIME).toString().trim()));
			viewHolder.textView_Time.setText(dateFormat.format(dt));
		} catch (Exception e) {
		}

		viewHolder.textView_Query.setText(map.get(SupportHistoryActivity.QUERY)
				.toString().trim());

		return convertView;
	}

	private class ViewHolder {
		TextView textView_Title, textView_Time, textView_Query;
	}
}
