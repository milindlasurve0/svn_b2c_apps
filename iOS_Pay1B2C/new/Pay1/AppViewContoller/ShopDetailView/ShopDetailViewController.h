//
//  ShopDetailViewController.h
//  Pay1
//
//  Created by Annapurna on 20/08/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "BaseViewController.h"
@class StoreList;
@interface ShopDetailViewController : BaseViewController<GMSMapViewDelegate>{
    IBOutlet UILabel *shopNameLabel;
    IBOutlet UILabel *shopAddressLabel;
    IBOutlet UILabel *shopAddress1Label;
    IBOutlet UILabel *shopAddress2Label;

    IBOutlet UILabel *shopNumberLabel;
    
    IBOutlet UILabel *shopNameheaderLabel;
    IBOutlet UILabel *shopAddressheaderLabel;
    IBOutlet UILabel *shopDistanceheaderLabel;

    IBOutlet UIView *shopHeaderView;
    IBOutlet UIView *shopInfoView;
    
    IBOutlet GMSMapView *_mapView;

    
    GMSMarker *myMarker;
    GMSMarker *shopMarker;
    
    NSMutableArray * _coordinates;
    
    
    IBOutlet UIImageView *shopImageView;
    
    BOOL isShopViewTapped;
    
    
}

@property (nonatomic, strong) StoreList *objStoreList;

@end
