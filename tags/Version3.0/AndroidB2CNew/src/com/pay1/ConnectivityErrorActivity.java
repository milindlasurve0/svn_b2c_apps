package com.pay1;

import android.app.Activity;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.larvalabs.svgandroid.SVGParser;
import com.pay1.constants.Constants;
import com.pay1.utilities.Utility;

public class ConnectivityErrorActivity extends Activity {

	private static final String SCREEN_LABEL = "Connectivity Error Screen";
	private EasyTracker easyTracker = null;
	private ImageView imageView_Close;
	private TextView textView_Title, textView_Message, textView_Retry;
	private Button button_Ok;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		 getWindow().setBackgroundDrawableResource(
				 android.R.color.transparent);
		setContentView(R.layout.connectivity_error_activity);
		try {
			easyTracker = EasyTracker
					.getInstance(ConnectivityErrorActivity.this);
			easyTracker.set(Fields.SCREEN_NAME, SCREEN_LABEL);

			Typeface Reguler = Typeface.createFromAsset(getAssets(),
					"Roboto-Regular.ttf");
			Typeface Medium = Typeface.createFromAsset(getAssets(),
					"Roboto-Regular.ttf");

			
			ImageView imageView_no_connection=(ImageView)findViewById(R.id.imageView_no_connection);
			imageView_no_connection.setImageDrawable(SVGParser.getSVGFromResource(
					this.getResources(), R.raw.ic_home_line)
					.createPictureDrawable());
			
			textView_Title = (TextView) findViewById(R.id.textView_Title);
			textView_Title.setTypeface(Reguler);
			textView_Message = (TextView) findViewById(R.id.textView_Message);
			textView_Message.setTypeface(Medium);
			button_Ok = (Button) findViewById(R.id.button_Ok);
			button_Ok.setTypeface(Reguler);
			button_Ok.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onBackPressed();
				}
			});

			imageView_Close = (ImageView) findViewById(R.id.imageView_Close);
			imageView_Close.setVisibility(View.GONE);
			imageView_Close.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onBackPressed();
				}
			});

		} catch (Exception e) {
		}
		overridePendingTransition(R.anim.entry, 0);
	}

	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView().build());
		// EasyTracker.getInstance(this).activityStart(this);
		easyTracker.send(MapBuilder.createAppView().build());
	}

	@Override
	public void onStop() {
		super.onStop();
		// EasyTracker.getInstance(this).activityStop(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		overridePendingTransition(R.anim.exit, 0);
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		Utility.setBackRequired(ConnectivityErrorActivity.this,
				Constants.SHAREDPREFERENCE_BACK_LOCATION, false);
		overridePendingTransition(R.anim.exit, 0);
	}
}