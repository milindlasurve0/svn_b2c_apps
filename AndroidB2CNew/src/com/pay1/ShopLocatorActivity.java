package com.pay1;

import java.util.ArrayList;
import java.util.WeakHashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.location.LocationManager;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.larvalabs.svgandroid.SVGParser;
import com.pay1.adapterhandler.ShopLocatorAdapter;
import com.pay1.constants.Constants;
import com.pay1.customviews.MyProgressDialog;
import com.pay1.requesthandler.RequestClass;
import com.pay1.utilities.Utility;

public class ShopLocatorActivity extends FragmentActivity {

	private static final String SCREEN_LABEL = "Shop Locator List Screen";
	private EasyTracker easyTracker = null;
	private final String TAG = "Shop Locator";

	private ListView listView_Dealers;
	// private GoogleMap googleMap;
	private TextView textView_Title, textView_MSG;
	private ImageView imageView_Back;
	private ArrayList<WeakHashMap<String, String>> data;
	private ShopLocatorAdapter adapter;

	public static final String SHOP_MOBILE = "mobile";
	public static final String SHOP_NAME = "shopname";
	public static final String SHOP_SALE = "sale";
	public static final String SHOP_LATITUDE = "latitude";
	public static final String SHOP_LONGITUDE = "logitude";
	public static final String SHOP_ADDRESS = "address";
	public static final String SHOP_PIN = "pin";
	public static final String SHOP_AREA = "area";
	public static final String SHOP_CITY = "city";
	public static final String SHOP_STATE = "state";
	public static final String SHOP_DISTANCE = "distance";

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.shop_locator_activity);
		try {

			easyTracker = EasyTracker.getInstance(ShopLocatorActivity.this);
			easyTracker.set(Fields.SCREEN_NAME, SCREEN_LABEL);
			Typeface Reguler = Typeface.createFromAsset(getAssets(),
					"");

			textView_Title = (TextView) findViewById(R.id.textView_Title);
			textView_Title.setTypeface(Reguler);
			textView_MSG = (TextView) findViewById(R.id.textView_MSG);
			textView_MSG.setTypeface(Reguler);

			imageView_Back = (ImageView) findViewById(R.id.imageView_Back);
			imageView_Back.setImageDrawable(SVGParser.getSVGFromResource(
					getResources(), R.raw.ic_back_old).createPictureDrawable());
			if (Build.VERSION.SDK_INT >= 11)
				imageView_Back.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
			imageView_Back.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onBackPressed();
				}
			});
			
			RelativeLayout back_layout=(RelativeLayout)findViewById(R.id.back_layout);
			back_layout.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onBackPressed();
				}
			});

			data = new ArrayList<WeakHashMap<String, String>>();
			listView_Dealers = (ListView) findViewById(R.id.listView_Dealers);
			// textView_NoData = (TextView)
			// findViewById(R.id.textView_NoData);
			// listView_Dealers.setEmptyView(textView_NoData);
			adapter = new ShopLocatorAdapter(ShopLocatorActivity.this, data);
			listView_Dealers.setAdapter(adapter);
			listView_Dealers
					.setOnItemClickListener(new AdapterView.OnItemClickListener() {

						@Override
						public void onItemClick(AdapterView<?> parent,
								View view, int position, long id) {
							// TODO Auto-generated method stub
							Intent intent = new Intent(
									ShopLocatorActivity.this,
									ShopLocatorDetailActivity.class);
							WeakHashMap<String, String> map = data
									.get(position);
							intent.putExtra("MOBILE",
									map.get(ShopLocatorActivity.SHOP_MOBILE));
							intent.putExtra("NAME",
									map.get(ShopLocatorActivity.SHOP_NAME));
							intent.putExtra("SALE",
									map.get(ShopLocatorActivity.SHOP_SALE));
							intent.putExtra("LATITUDE",
									map.get(ShopLocatorActivity.SHOP_LATITUDE));
							intent.putExtra("LONGITUDE",
									map.get(ShopLocatorActivity.SHOP_LONGITUDE));
							intent.putExtra("ADDRESS",
									map.get(ShopLocatorActivity.SHOP_ADDRESS));
							intent.putExtra("PIN",
									map.get(ShopLocatorActivity.SHOP_PIN));
							intent.putExtra("AREA",
									map.get(ShopLocatorActivity.SHOP_AREA));
							intent.putExtra("CITY",
									map.get(ShopLocatorActivity.SHOP_CITY));
							intent.putExtra("STATE",
									map.get(ShopLocatorActivity.SHOP_STATE));
							intent.putExtra("DISTANCE",
									map.get(ShopLocatorActivity.SHOP_DISTANCE));
							startActivity(intent);
						}
					});

			// if (googleMap == null) {
			// // Try to obtain the map from the SupportMapFragment.
			// googleMap = ((SupportMapFragment) getSupportFragmentManager()
			// .findFragmentById(R.id.map_full)).getMap();
			// googleMap
			// .setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
			//
			// @Override
			// public void onMarkerDragStart(Marker marker) {
			// // TODO Auto-generated method stub
			//
			// }
			//
			// @Override
			// public void onMarkerDragEnd(Marker marker) {
			// // TODO Auto-generated method stub
			// // googleMap.clear();
			// // LatLng latLng = marker.getPosition();
			// // MarkerOptions options = new
			// // MarkerOptions();
			// // options.position(latLng);
			// // options.title("You are here");
			// // options.draggable(false);
			// // options.icon(BitmapDescriptorFactory
			// // .defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
			// // //
			// //
			// options.icon(BitmapDescriptorFactory.fromResource(R.drawable.close));
			// // location = new Location("Test");
			// // location.setLatitude(latLng.latitude);
			// // location.setLongitude(latLng.longitude);
			// // googleMap.addMarker(options);
			// }
			//
			// @Override
			// public void onMarkerDrag(Marker marker) {
			// // TODO Auto-generated method stub
			//
			// }
			// });
			// googleMap
			// .setOnMapClickListener(new GoogleMap.OnMapClickListener() {
			//
			// @Override
			// public void onMapClick(LatLng latLng) {
			// // TODO Auto-generated method stub
			// // googleMap.clear();
			// // MarkerOptions options = new
			// // MarkerOptions();
			// // options.position(latLng);
			// // options.title("You are here");
			// // options.draggable(false);
			// // options.icon(BitmapDescriptorFactory
			// // .defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
			// // //
			// //
			// options.icon(BitmapDescriptorFactory.fromResource(R.drawable.close));
			// // location = new Location("Test");
			// // location.setLatitude(latLng.latitude);
			// // location.setLongitude(latLng.longitude);
			// // googleMap.addMarker(options);
			// }
			// });
			// // Check if we were successful in obtaining the map.
			// if (googleMap != null) {
			// try {
			//
			// Location location = new Location("Current");
			// location.setLatitude(Double.parseDouble(Utility
			// .getCurrentLatitude(
			// ShopLocatorActivity.this,
			// Constants.SHAREDPREFERENCE_CURRENT_LATITUDE)));
			// location.setLongitude(Double.parseDouble(Utility
			// .getCurrentLongitude(
			// ShopLocatorActivity.this,
			// Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE)));
			//
			// setUpMap(location);
			// } catch (Exception e) {
			// }
			//
			// }
			// }

			boolean gps_enabled = false;
			boolean network_enabled = false;
			LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
			try {
				gps_enabled = lm
						.isProviderEnabled(LocationManager.GPS_PROVIDER);
			} catch (Exception ex) {
			}
			try {
				network_enabled = lm
						.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
			} catch (Exception ex) {
			}

			// don't start listeners if no provider is
			// enabled
			boolean hasGPS = getPackageManager().hasSystemFeature(
					PackageManager.FEATURE_LOCATION_GPS);
			WifiManager wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
			if (!(network_enabled && wifi.isWifiEnabled())
					&& (hasGPS && !gps_enabled)) {

				showTwoButtonDialog(
						ShopLocatorActivity.this,
						"Shop Locator",
						"Turn on your location service on the phone to locate nearby Pay1 merchant.",
						Constants.DIALOG_CLOSE_LOCATION);
			} else if (gps_enabled || network_enabled) {
				new DealorLocatorTask().execute(Utility.getCurrentLatitude(
						ShopLocatorActivity.this,
						Constants.SHAREDPREFERENCE_CURRENT_LATITUDE), Utility
						.getCurrentLongitude(ShopLocatorActivity.this,
								Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE),
						Utility.getMobileNumber(ShopLocatorActivity.this,
								Constants.SHAREDPREFERENCE_MOBILE));
			} else {
				showTwoButtonDialog(
						ShopLocatorActivity.this,
						"Shop Locator",
						"Turn on your location service on the phone to locate nearby Pay1 merchant.",
						Constants.DIALOG_CLOSE_LOCATION);
			}

		} catch (Exception exception) {
			new DealorLocatorTask().execute(Utility.getCurrentLatitude(
					ShopLocatorActivity.this,
					Constants.SHAREDPREFERENCE_CURRENT_LATITUDE), Utility
					.getCurrentLongitude(ShopLocatorActivity.this,
							Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE),
					Utility.getMobileNumber(ShopLocatorActivity.this,
							Constants.SHAREDPREFERENCE_MOBILE));
		}
		overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}

	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView().build());
		// EasyTracker.getInstance(this).activityStart(this);
		easyTracker.send(MapBuilder.createAppView().build());
	}

	@Override
	public void onStop() {
		super.onStop();
		// EasyTracker.getInstance(this).activityStop(this);
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (Utility.getBackRequired(ShopLocatorActivity.this,
				Constants.SHAREDPREFERENCE_BACK_LOCATION))
			new DealorLocatorTask().execute(Utility.getCurrentLatitude(
					ShopLocatorActivity.this,
					Constants.SHAREDPREFERENCE_CURRENT_LATITUDE), Utility
					.getCurrentLongitude(ShopLocatorActivity.this,
							Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE),
					Utility.getMobileNumber(ShopLocatorActivity.this,
							Constants.SHAREDPREFERENCE_MOBILE));
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
	}

	public class DealorLocatorTask extends AsyncTask<String, String, String>
			implements OnDismissListener {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			try {
				String response = RequestClass.getInstance()
						.readPay1B2BRequest(
								ShopLocatorActivity.this,
								Constants.B2B_URL
										+ "method=getNearByRetailer&lat="
										+ params[0] + "&lng=" + params[1]
										+ "&mobile=" + params[2]);
				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			try {

				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONArray jsonArray = new JSONArray(replaced);
					JSONArray jsonArrayIn = jsonArray.getJSONArray(0);
					int len = jsonArrayIn.length();

					data.clear();

					for (int i = 0; i < len; i++) {
						JSONObject jsonObject = jsonArrayIn.getJSONObject(i);

						String shop_mobile = jsonObject.getJSONObject("t")
								.getString("mobile").toString().trim();
						String shop_name = jsonObject.getJSONObject("t")
								.getString("shopname").toString().trim();
						String shop_sale = jsonObject.getJSONObject("t")
								.getString("sale").toString().trim();
						String latitude = jsonObject.getJSONObject("t")
								.getString("latitude").toString().trim();
						String longitude = jsonObject.getJSONObject("t")
								.getString("longitude").toString().trim();
						// String user_id = jsonObject.getJSONObject("t")
						// .getString("user_id");
						String address = jsonObject.getJSONObject("t")
								.getString("address").toString().trim();
						String pin = jsonObject.getJSONObject("t")
								.getString("pin").toString().trim();
						String area_name = jsonObject.getJSONObject("t")
								.getString("area_name").toString().trim();
						String city_name = jsonObject.getJSONObject("t")
								.getString("city_name").toString().trim();
						String state_name = jsonObject.getJSONObject("t")
								.getString("state_name").toString().trim();
						String distance = jsonObject.getJSONObject("t")
								.getString("D").toString().trim();
						System.out.println("Dis API " + distance);

						String lat = Utility.getCurrentLatitude(
								ShopLocatorActivity.this,
								Constants.SHAREDPREFERENCE_CURRENT_LATITUDE);
						String lng = Utility.getCurrentLongitude(
								ShopLocatorActivity.this,
								Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE);
						if (lat != "" && lng != "") {
							// System.out.println("Dis " + lat + " " + lng + " "
							// + latitude + " " + longitude);
							System.out.println("Dis Cal "
									+ Constants.distanceFrom(
											Double.parseDouble(lat),
											Double.parseDouble(lng),
											Double.parseDouble(latitude),
											Double.parseDouble(longitude)));
						}
						StringBuffer sb = new StringBuffer();
						sb.append(address).append(",\n").append(area_name)
								.append(",\n").append(city_name).append(" - ")
								.append(pin);
						// // Log.w(TAG, sb.toString());

						WeakHashMap<String, String> map = new WeakHashMap<String, String>();
						map.put(SHOP_MOBILE, shop_mobile);
						map.put(SHOP_NAME, shop_name);
						map.put(SHOP_SALE, shop_sale);
						map.put(SHOP_LATITUDE, latitude);
						map.put(SHOP_LONGITUDE, longitude);
						map.put(SHOP_ADDRESS, address);
						map.put(SHOP_PIN, pin);
						map.put(SHOP_AREA, area_name);
						map.put(SHOP_CITY, city_name);
						map.put(SHOP_STATE, state_name);
						map.put(SHOP_DISTANCE, distance);
						data.add(map);

					}

					ShopLocatorActivity.this.runOnUiThread(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							adapter.notifyDataSetChanged();
						}
					});
				} else {
					Intent intent = new Intent(ShopLocatorActivity.this,
							ConnectivityErrorActivity.class);
					startActivity(intent);
					finish();
				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonDialog(ShopLocatorActivity.this, TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonDialog(ShopLocatorActivity.this, TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(ShopLocatorActivity.this);
			// // this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							DealorLocatorTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			DealorLocatorTask.this.cancel(true);
			dialog.cancel();
		}

	}

	public void showTwoButtonDialog(final Context context, String Title,
			String message, final int Type) {
		try {

			final Dialog dialog = new Dialog(context);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			// dialog.getWindow().setBackgroundDrawableResource(
			// android.R.color.transparent);
			dialog.setContentView(R.layout.dialog_two_button);
			dialog.setCancelable(false);
			dialog.setCanceledOnTouchOutside(false);
			// dialog.setTitle(null);

			Typeface Reguler = Typeface.createFromAsset(context.getAssets(),
					"");

			Typeface Medium = Typeface.createFromAsset(context.getAssets(),
					"");

			// set the custom dialog components - text, image and button
			TextView textView_Title = (TextView) dialog
					.findViewById(R.id.textView_Title);
			textView_Title.setText(Title);
			textView_Title.setTypeface(Reguler);

			TextView textView_Message = (TextView) dialog
					.findViewById(R.id.textView_Message);
			textView_Message.setText(message);
			textView_Message.setTypeface(Medium);

			Button button_Ok = (Button) dialog.findViewById(R.id.button_Ok);
			button_Ok.setTypeface(Reguler);
			// if button is clicked, close the custom dialog
			button_Ok.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
					Utility.setBackRequired(ShopLocatorActivity.this,
							Constants.SHAREDPREFERENCE_BACK_LOCATION, true);
					Intent viewIntent = new Intent(
							Settings.ACTION_LOCATION_SOURCE_SETTINGS);
					context.startActivity(viewIntent);
				}
			});

			Button button_Cancel = (Button) dialog
					.findViewById(R.id.button_Cancel);
			button_Cancel.setTypeface(Reguler);
			// if button is clicked, close the custom dialog
			button_Cancel.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
					new DealorLocatorTask().execute(
							Utility.getCurrentLatitude(
									ShopLocatorActivity.this,
									Constants.SHAREDPREFERENCE_CURRENT_LATITUDE),
							Utility.getCurrentLongitude(
									ShopLocatorActivity.this,
									Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE),
							Utility.getMobileNumber(ShopLocatorActivity.this,
									Constants.SHAREDPREFERENCE_MOBILE));
				}
			});

			button_Ok.setText("Ok");
			button_Cancel.setText("Not now");

			dialog.show();

		} catch (Exception exception) {
		}
	}
}
