//b2cUrl = "http://b2c.pay1.loc";
var proto = 'https';
if(window.location.protocol!='https')
    //proto = 'http';
b2cUrl = proto+"://b2c.pay1.in";
panelUrl = proto+"://panel.pay1.in/";
baseUrl = proto+'://b2c.pay1.in/';
MAX_WALLET_BALANCE_ALLOWED = 5000;
$(document).ready(function() {


    $('.bxslider').bxSlider({
        minSlides: 5,
        maxSlides: 5,
        slideWidth: 170,
        slideMargin: 10,
        pager: false,
        caption: true
    });

    $('.bxslidercategorydetail').bxSlider({
        minSlides: 1,
        maxSlides: 1,
        slideWidth: 1350,
        slideMargin: 10,
        pager: false,
    });

    $('.bxslidercat_store').bxSlider({
        minSlides: 3,
        maxSlides: 3,
        slideWidth: 450,
        slideMargin: 80,
        pager: false
    });

//https://github.com/seiyria/bootstrap-slider

    var gifticonsrange = $("input.giftcoins").slider();
    
    if (typeof (gifticonsrange) != "undefined" && gifticonsrange !== null) {
       // console.log(gifticonsrange.slider('getValue'));

        gifticonsrange.change(function(data) {
           // console.log(data.value);
            
           $('div.gift_types').children().each(function(index,element){
            
            if(($(this).attr('data-gift-points')>=data.value.newValue[0] )&& ($(this).attr('data-gift-points')<=data.value.newValue[1]))
            {
                $(this).show('slow');
            }
            else
            {
                $(this).hide('slow');
            }
                
           });
           
        });
        
    }
    
    
                      
});



function set_mylike(id,offer_id){
                var flag = $("#isloggedin").val();
                if(flag !=1){
                    $("#basicModal").modal('toggle');

                } else { 
                var isfav =offer_id;
                var url = b2cUrl+"/index.php/api_new/action/api/true/actiontype/MyLikes/?";
                $.ajax({
                url: url,
                type:"GET",
                data:{id:isfav,
                      res_format : "jsonp"
                     },
                timeout: 50000,
                dataType: "jsonp",
                jsonpCallback: 'callback',
                crossDomain: true,
                success:function(data){ console.log(data);
                    if(data.description.status==1){
                        document.getElementById(id).className="p_icon red_active";
                    }else{
                        document.getElementById(id).className="p_icon";
                    }
                  // $("#".id).addClass('red_active');
    //                alert("Test");
              if(data.status == 'success'){
                var url = b2cUrl+"/index.php/api_new/action/api/true/actiontype/MyLikesCount/?";
                $.ajax({
                url: url,
                type:"GET",
                data:{
                      res_format : "jsonp"
                     },
                timeout: 50000,
                dataType: "jsonp",
                jsonpCallback: 'callback',
                crossDomain: true,
                success:function(data){
                    if(data.status=='success'){
                   b2c.core.updateprofile(data.description);
                    }else if(data.status=='failure' && data.description.toUpperCase()=='UNAUTHORISED ACCESS'){
                         $("#basicModal").modal('toggle');
                    }
                },
                error: function (xhr,error) {
                 },  

                });
                    }
                },
                complete : function(){

                },
                error: function (xhr,error) {
                 },  
                });
            }

          }

//For Gift access Login and Reddeem Gift button
function GoToGiftDetails(deal_id , d_url ) {
    var flag = $("#isloggedin").val();
        if(flag !=1){
            $("#basicModal").modal('toggle');
        }else{ 
        //window.location='/giftstore/giftdetails/'+deal_id+'_'+area;
            if(d_url=='')
                d_url = 'gift';
            window.location='/gifts-'+d_url+'-'+deal_id;
        }
}

function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

    return true
}
var attach_Slider = function(div_classname, min, max) {
    $('.' + div_classname).bxSlider({
        minSlides: min,
        maxSlides: max,
        slideWidth: 450,
        slideMargin: 80,
        pager: false
    });
}

var attach_Slider_with_caption = function(div_classname, min, max) {
    $('.' + div_classname).bxSlider({
        minSlides: min,
        maxSlides: max,
        slideWidth: 170,
        slideMargin: 10,
        pager: false,
        caption: true
    });
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function showOverlay(){
        $('.categoryOverlaydiv').show();
}

$(document).ready(function() {
    
    $('.categoryOverlay').on('click',function(){
         $('.categoryOverlaydiv').hide();
        });
        
});

$(function(){
        $('.addtofavdiv img').on('click',function(e) {
            //alert("helllo");
            var flag = $("#isloggedin").val();
            if(flag !=1){
                $("#basicModal").modal('toggle');

            } else {
            var _this = $(this);
            var current = _this.attr("src");
            var swap = _this.attr("data-swap");
            var isfav = _this.attr("value");
            _this.attr('src', swap).attr("data-swap", current);
            var url = b2cUrl+"/index.php/api_new/action/api/true/actiontype/MyLikes/?";
            $.ajax({
            url: url,
            type:"GET",
            data:{id:isfav,
                  res_format : "jsonp"
                 },
            timeout: 50000,
            dataType: "jsonp",
            jsonpCallback: 'callback',
            crossDomain: true,
            success:function(data){
          if(data.status == 'success'){
            var url = b2cUrl+"/index.php/api_new/action/api/true/actiontype/MyLikesCount/?";
            $.ajax({
            url: url,
            type:"GET",
            data:{
                  res_format : "jsonp"
                 },
            timeout: 50000,
            dataType: "jsonp",
            jsonpCallback: 'callback',
            crossDomain: true,
            success:function(data){
                if(data.status=='success'){
               b2c.core.updateprofile(data.description);
                }
            },
            error: function (xhr,error) {
             },  
         
            });
                }
            },
            error: function (xhr,error) {
             },  
         
            });
            
        }
        });
        
    });
    
    $(function(){
        var url = b2cUrl+"/index.php/api_new/action/api/true/actiontype/MyLikesCount/?";
            $.ajax({
            url: url,
            type:"GET",
            data:{
                  res_format : "jsonp"
                 },
            timeout: 50000,
            dataType: "jsonp",
            jsonpCallback: 'callback',
            crossDomain: true,
            success:function(data){
                if(data.status=='success'){
                    console.log(data);
               b2c.core.updateprofile(data.description);
                }
            },
            error: function (xhr,error) {
                
             },  
         
            });
        
    });
    
    
    $(function(){
        $('input[name=searchbyname]').keyup(function(){
            
            var keyword=$(this).val().toLowerCase();
            
            
            $('div.dealslisting').children().each(function(k,v){
                
                    if($(this).find('div.caption').text().toLowerCase().indexOf(keyword)>=0)
                    {   
                         $(this).show('slow');
                    }
                    else
                    {
                        $(this).hide('slow');
                    }
            });
            
        });
    })
    $(function() {
    $("#redeem_coin").click(function() {

        var flag = $("#isloggedin").val();
        
        if (flag != 1) {
            $("#basicModal").modal('toggle');

        } else {
            window.location = '/store/';
        }

    });
});
    $(function() {
    $("#redeem_gift").click(function() {
        var flag = $("#isloggedin").val();
        if (flag != 1) {
            $("#basicModal").modal('toggle');

        } else {
        }

    });
});

$(function(){
        $('.addtofavdiv1 img').on('click',function(e) {
            //alert("hello");
            var flag = $("#isloggedin").val();
            if(flag !=1){
                $("#basicModal").modal('toggle');

            } else {
            var _this = $(this);
            var current = _this.attr("src");
            var swap = _this.attr("data-swap");
            var isfav = _this.attr("value");
            _this.attr('src', swap).attr("data-swap", current);
            if (isfav == 0) {
                $(this).attr('value', '1');
                //--api to add in favourite
            } else {
                $(this).attr('value', '0');
                //--api to remove in favourite
            }
        }
        });
        
    });

        function search_brands(){
          var brand_name = $("#search_brand").val(); 
          var lat = localStorage.getItem('latitude');
          var lng = localStorage.getItem('longitude');
            var search_brand_html = '';
            var url = b2cUrl+"/index.php/api_new/action/api/true/actiontype/SearchGift_WEB/?";
                $.ajax({
                    url: url,
                    type: "GET",
                    data: { type:"brand",
                           name: brand_name,
                           latitude: lat,
                           longitude: lng,
                           res_format: "jsonp"
                    },
                    timeout: 50000,
                    dataType: "jsonp",
                    jsonpCallback: 'callback',
                    crossDomain: true,
                    success: function(data) {
                           $.each(data.description, function(index, element) {
//                               console.log(element);
                            if(element.logo){
                                element_logo = element.logo;
                            }else{
                                element_logo = '/public/assets/images/default_img/ic_logo_evoucher.png'; 
                            }
                            search_brand_html+= '<div  data-gift-points='+element.min_amount+' class="item col-md-4 col-xs-6">'
                                             +'<div class="thumbnail"><div class="col-md-10 col-xs-10">'
                                             + '<p class="p_detail">'+element.offer_desc+'</p>'
                                             + '</div>'
                                             +'<div class="col-md-2 col-xs-2">'
                                             +'<p class="p_icon"  value='+element.id+'><i class="glyphicon glyphicon-heart-empty"></i></p>'
                                             +'</div>'
                                             +'<a href="/giftstore/giftdetails/'+ element.deal_id+'_'+element.area.trim() +'"style="padding-left: 1px;">'
                                             +'<img class="group list-group-image img-responsive" src='+element.img_url+' alt=""  />'
                                             +'</a>'
                                             +'<div class="caption"><div class="row"><div class="col-md-6 col-xs-6">'
                                             +'<img src='+element_logo+' class="logo"></div>'
                                             + '<div class="col-md-6 col-xs-6">'
                                             +'<img src="/assets/images/gift/iconall.png" alt=""><span>'+element.min_amount+'</span>'
                                             +'</div>'
                                             +'<div class="col-md-12 col-xs-12 text-left margin-top-20">'
                                             +'<p class="group inner list-group-item-text">'+element.deal+'</p>';
                                             if(element.area!='')
                            search_brand_html+='<p><img src="/assets/images/gift/icongift.png" alt=""> <span>'+element.area+'</span></p>'
                                             +'</div></div></div></div></div>';

                            }); 
                            if(search_brand_html != ''){
                              $("#products").html("");
                              $("#products").append(search_brand_html);
                            }
                         },
                    beforeSend: function(){
                        $('.loader').show();
                        },
                    complete: function(){
                        $('.loader').hide();
                        },
                    error: function(xhr, error) {
                        console.log(error);     
                    }
            });
        }

        function search_location(area_name){
          var loc_filter_html = '';
          var lat = localStorage.getItem('latitude');
          var lng = localStorage.getItem('longitude');
            var url = b2cUrl+"/index.php/api_new/action/api/true/actiontype/SearchGift_WEB/?";
                $.ajax({
                    url: url,
                    type: "GET",
                    data: { type:"area",
                           name: area_name,
                           latitude: lat,
                           longitude: lng,
                           res_format: "jsonp"
                    },
                    timeout: 50000,
                    dataType: "jsonp",
                    jsonpCallback: 'callback',
                    crossDomain: true,
                    success: function(data) {
                           $.each(data.description, function(index, element) {
//                               console.log(element);
                            if(element.logo){
                                element_logo = element.logo;
                            }else{
                                element_logo = '/public/assets/images/default_img/ic_logo_evoucher.png'; 
                            }
                            loc_filter_html+= '<div  data-gift-points='+element.min_amount+' class="item col-md-4 col-xs-6">'
                                             +'<div class="thumbnail"><div class="col-md-10 col-xs-10">'
                                             + '<p class="p_detail">'+element.offer_desc+'</p>'
                                             + '</div>'
                                             +'<div class="col-md-2 col-xs-2">'
                                             +'<p class="p_icon" value='+element.id+'><i class="glyphicon glyphicon-heart-empty"></i></p>'
                                             +'</div>'
                                             +'<a href="/giftstore/giftdetails/'+ element.deal_id+'_'+element.area.trim() +'"style="padding-left: 1px;">'
                                             +'<img class="group list-group-image img-responsive" src='+element.img_url+' alt=""  style="width: 189px;height: 96px"  />'
                                             +'</a>'
                                             +'<div class="caption"><div class="row"><div class="col-md-6 col-xs-6">'
                                             +'<img src='+element_logo+' class="logo"></div>'
                                             + '<div class="col-md-6 col-xs-6">'
                                             +'<img src="/assets/images/gift/iconall.png" alt=""><span>'+element.min_amount+'</span>'
                                             +'</div>'
                                             +'<div class="col-md-12 col-xs-12 text-left margin-top-20">'
                                             +'<p class="group inner list-group-item-text">'+element.deal+'</p>';
                                             if(element.area!='')
                            loc_filter_html+='<p><img src="/assets/images/gift/icongift.png" alt=""> <span>'+element.area+'</span></p>'
                                             +'</div></div></div></div></div>';

                            });
                            if(loc_filter_html != ''){
                              $("#products").html("");
                              $("#products").append(loc_filter_html);
                            }
                         },
                    beforeSend: function(){
                        $('.loader').show();
                        },
                    complete: function(){
                        $('.loader').hide();
                        },
                    error: function(xhr, error) {
                        console.log(error);     
                    }
            });
        }
//        scrolldown_more_gift.next_value = 0;  
//        function scrolldown_more_gift(id){
//            scrolldown_more_gift.next_value++;
//            var more_gift_html = '';
//            var url = b2cUrl+"/index.php/api_new/action/api/true/actiontype/GetGiftsByType_WEB/?";
//                $.ajax({
//                    url: url,
//                    type: "GET",
//                    data: {type: "gift",
//                           type_id: id,
//                           next : scrolldown_more_gift.next_value,
//                           res_format: "jsonp"
//                    },
//                    timeout: 50000,
//                    dataType: "jsonp",
//                    jsonpCallback: 'callback',
//                    crossDomain: true,
//                    success: function(data) {
//                          // console.log(data);
//                           $.each(data.description, function(index, element) {
//                            more_gift_html+= '<div  data-gift-points='+element.min_amount+' class="item col-md-4 col-xs-6">'
//                                             +'<div class="thumbnail"><div class="col-md-10 col-xs-10">'
//                                             + '<p class="p_detail">'+element.offer+'</p>'
//                                             + '</div>'
//                                             +'<div class="col-md-2 col-xs-2">'
//                                             +'<p class="p_icon" value='+element.id+'><i class="glyphicon glyphicon-heart-empty"></i></p>'
//                                             +'</div>'
//                                             +'<a href="/giftstore/giftdetails/'+ element.id+'_'+element.area.trim() +'"style="padding-left: 1px;">'
//                                             +'<img class="group list-group-image img-responsive" src='+element.img_url+' alt="" style="width: 189px;height: 96px"   />'
//                                             +'</a>'
//                                             +'<div class="caption"><div class="row"><div class="col-md-6 col-xs-6">'
//                                             +'<img src='+element.logo+' class="logo"></div>'
//                                             + '<div class="col-md-6 col-xs-6">'
//                                             +'<img src="/assets/images/gift/iconall.png" alt=""><span>'+element.min_amount+'</span>'
//                                             +'</div>'
//                                             +'<div class="col-md-12 col-xs-12 text-left margin-top-20">'
//                                             +'<p class="group inner list-group-item-text">'+element.deal+'</p>'
//                                             +'<p><img src="/assets/images/gift/icongift.png" alt=""> <span>'+element.area+'</span></p>'
//                                             +'</div></div></div></div></div>';
//
//                            });     
//                            if(more_gift_html != ''){
//                              $("#products").append(more_gift_html);
//                            }
//                         },
//                    beforeSend: function(){
//                        $('.loader').show();
//                        },
//                    complete: function(){
//                        $('.loader').hide();
//                        },
//                    error: function(xhr, error) {
//                        console.log(error);     
//                    }
//                });  
//            
//        }
        
        function gift_filter_button(type,typeID,type){
            
           var range = $( "#slider1" ).val(); 
           if(type == 'plus'){
             range = parseInt(range) + 10;   
           }else{
             range = parseInt(range) - 10;  
           } 
         showVal(range,typeID,type);  
        }
        
        function showVal(newVal,typeID,type){
//            document.getElementById("gift_coin").innerHTML=newVal;
        
            var gift_filter_html = '';
            var url = b2cUrl+"/index.php/api_new/action/api/true/actiontype/GiftCoinDetails_WEB/?";
                $.ajax({
                    url: url,
                    type: "GET",
                    data: {type: type,
                           type_id: typeID,
                           first_amt: 0,
                           last_amt: newVal,
                           res_format: "jsonp"
                    },
                    timeout: 50000,
                    dataType: "jsonp",
                    jsonpCallback: 'callback',
                    crossDomain: true,
                    success: function(data) {
                          // console.log(data);

                           $.each(data.description, function(index, element) {
//                               console.log(element.area);
                              var area = (element.area==null) ? '' : element.area.trim();
                            
                            if(element.logo){
                                element_logo = element.logo;
                            }else{
                                element_logo = '/public/assets/images/default_img/ic_logo_other.png'; 
                            }
                               
                            gift_filter_html+= '<div  data-gift-points='+element.min_amount+' class="item col-md-4 col-xs-6">'
                                             +'<div class="thumbnail"><div class="col-md-10 col-xs-10">'
                                             + '<p class="p_detail">'+element.offer_desc+'</p>'
                                             + '</div>'
                                             +'<div class="col-md-2 col-xs-2">'
                                             +'<p class="p_icon"  value='+element.id+'><i class="glyphicon glyphicon-heart-empty"></i></p>'
                                             +'</div>'
                                             +'<a href="/giftstore/giftdetails/'+ element.id+'_'+ area +'" style="padding-left: 1px;">'
                                             +'<img class="group list-group-image img-responsive" src='+element.img_url+' alt=""  style="width: 189px;height: 96px"  />'
                                             +'</a>'
                                             +'<div class="caption"><div class="row"><div class="col-md-6 col-xs-6">'
                                             +'<img class="logo" src='+element_logo+'></div>'
                                             + '<div class="col-md-6 col-xs-6">'
                                             +'<img src="/assets/images/gift/iconall.png" alt=""><span>'+element.min_amount+'</span>'
                                             +'</div>'
                                             +'<div class="col-md-12 col-xs-12 text-left margin-top-20">'
                                             +'<p class="group inner list-group-item-text">'+element.deal+'</p>';
                                             if(element.area!='')
                            gift_filter_html+='<p><img src="/assets/images/gift/icongift.png" alt=""> <span>'+element.area+'</span></p>'
                                             +'</div></div></div></div></div>';

                            });     
                            if(gift_filter_html != ''){
                              $("#products").html("");
                              $("#products").append(gift_filter_html);
                            }
                         },
                    beforeSend: function(){
                        $('.loader').show();
                        },
                    complete: function(){
                        $('.loader').hide();
                        },
                    error: function(xhr, error) {
                        console.log(error);     
                    }
                });
            }
