//
//  OptionViewController.h
//  Pay1
//
//  Created by Annapurna on 04/08/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "BaseViewController.h"
#import <FacebookSDK/FacebookSDK.h>

@interface OptionViewController : BaseViewController<FBLoginViewDelegate,UIGestureRecognizerDelegate>{
    
    
    
    IBOutlet  FBLoginView *loginview;
    
    
    
    IBOutlet UIScrollView *scrollView;
    
    IBOutlet UILabel *walletDataLabel;
    IBOutlet UILabel *coinDataLabel;
    IBOutlet UILabel *userNameLabel;
    
    IBOutlet UIView *walletView;
    IBOutlet UIView *coinView;

    
    
    
}

@end
