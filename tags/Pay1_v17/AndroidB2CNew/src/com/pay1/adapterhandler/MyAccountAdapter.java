package com.pay1.adapterhandler;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.larvalabs.svgandroid.SVGParser;
import com.pay1.R;

public class MyAccountAdapter extends BaseAdapter {

	private Context context;
	private ArrayList<String> list;
	private Typeface Reguler;

	public MyAccountAdapter(Context context, ArrayList<String> list) {
		this.context = context;
		this.list = list;

		Reguler = Typeface.createFromAsset(context.getAssets(),
				"EncodeSansNormal-400-Regular.ttf");

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@SuppressLint("NewApi")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this.context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.my_account_adapter, null);
		}

		TextView textView_Name = (TextView) convertView
				.findViewById(R.id.textView_Name);
		textView_Name.setTypeface(Reguler);

		ImageView imageView_Arrow = (ImageView) convertView
				.findViewById(R.id.imageView_Arrow);
		imageView_Arrow.setImageDrawable(SVGParser.getSVGFromResource(
				context.getResources(), R.raw.ic_next).createPictureDrawable());
		if (Build.VERSION.SDK_INT >= 11)
			imageView_Arrow.setLayerType(View.LAYER_TYPE_SOFTWARE, null);

		textView_Name.setText(list.get(position));
		return convertView;
	}

}
