﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace Pay1B2C.UserControls
    {
    public partial class GiftCells: UserControl
        {
        public GiftCells()
            {
            InitializeComponent();
           // mainPanel.Tap+=mainPanel_Tap;


            System.Windows.Shapes.Path newPath = (System.Windows.Shapes.Path)System.Windows.Markup.XamlReader.Load("<Path xmlns='http://schemas.microsoft.com/winfx/2006/xaml/presentation'  Width='24' Height='24'  Fill='#333333' Data='M12,2.267c-5.366,0-9.731,4.366-9.731,9.733c0,5.366,4.365,9.732,9.731,9.732s9.731-4.366,9.731-9.732 C21.731,6.633,17.366,2.267,12,2.267z M12,20.599c-4.741,0-8.599-3.857-8.599-8.599S7.259,3.401,12,3.401S20.599,7.259,20.599,12 S16.741,20.599,12,20.599z M12,0C5.384,0,0,5.383,0,12s5.384,12,12,12s12-5.383,12-12S18.616,0,12,0z M12,22.865 C6.009,22.865,1.133,17.991,1.133,12S6.009,1.134,12,1.134S22.867,6.009,22.867,12S17.991,22.865,12,22.865z M8.403,16.605 c0,0.473,0.385,0.855,0.855,0.855h2.255v-3.179h-3.11V16.605z M8.403,11.125v2.184h3.11v-3.04H9.258 C8.788,10.269,8.403,10.652,8.403,11.125z M14.979,6.741c-1.094-1.09-2.976,2.576-2.976,2.576s-1.914-3.479-2.975-2.576 c-1.248,1.068,1.333,3.01,2.975,3.01C13.646,9.751,16.139,7.903,14.979,6.741z M12.488,17.461h2.252 c0.473,0,0.857-0.383,0.857-0.855v-2.323h-3.11V17.461z M15.596,11.125c0-0.472-0.383-0.855-0.855-0.855h-2.254v3.04h3.11V11.125z' HorizontalAlignment='Center' VerticalAlignment='Center' Margin='0,0,0,0'/>");

            coinPane1l.Children.Add(newPath);
            }

      
        }
    }
