//
//  GiftDetailService.m
//  Pay1
//
//  Created by Annapurna on 18/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "GiftDetailService.h"
#import "GiftDetailResponse.h"
#import "GiftLocationList.h"
#import "CurrentLocation.h"
#import "GiftDetailCriteria.h"
#import "GiftOfferDetailList.h"
@implementation GiftDetailService
-(void)callGiftDetailService:(GiftDetailCriteria *)giftDealCriteria callback:(giftDetailResponse)callback{
    NSMutableDictionary *postData = [[NSMutableDictionary alloc]init];
    [postData setObject:giftDealCriteria.giftDealID forKey:@"id"];
    [postData setObject:giftDealCriteria.userLat forKey:@"latitude"];
    [postData setObject:giftDealCriteria.userLong forKey:@"longitude"];
    [self fillDefaultData:postData];

    NSLog(@"#PostData :%@",postData);
    
    RequestData *requestData = [[RequestData alloc]init];
    requestData.baseURL = kBaseURL;
    requestData.webServiceURL = kGiftDetailURL;
    requestData.postData = postData;
    requestData.showLoader = FALSE;
    
    requestData.requestSuccessBlock = ^(RequestData *request) {
    [self printResponse:request.responseData forRequest:@"Gift Detail Response"];
    if ([self handleNullResponse:request])
    return;
        
    callback(nil, [self parseGiftDetailResponse:request]);
    };
    
    
    requestData.requestFailureBlock = ^(RequestData *request) {
        [self handleNetworkError:request.responseData];
        callback(request.responseData, nil);
    };
    
    [[ServiceManager sharedInstance] makeRequest:requestData];
}
-(GiftDetailResponse *)parseGiftDetailResponse:(RequestData *)request{
   GiftDetailResponse *objGiftDetail = [[GiftDetailResponse alloc]init];
   objGiftDetail.status = [request.responseData objectForKey:@"status"];
   objGiftDetail.errorCode = [request.responseData objectForKey:@"errCode"];
    
    
    if([[objGiftDetail status]isEqualToString:@"success"]){
    NSMutableDictionary *descriptionDic = [request.responseData objectForKey:@"description"];
    NSLog(@"#DescriptionDic :%@",descriptionDic);
    
    if([descriptionDic objectForKey:@"brand_description"])
    objGiftDetail.brandDescription = [descriptionDic objectForKey:@"brand_description"];
    
    if([descriptionDic objectForKey:@"company_url"])
    objGiftDetail.companyURL = [descriptionDic objectForKey:@"company_url"];
    
    if([descriptionDic valueForKey:@"deal_detail"]){
    NSMutableArray *dealDetailArray = [descriptionDic valueForKey:@"deal_detail"];
    for(NSMutableDictionary *dic in dealDetailArray){
    objGiftDetail.contentText = [dic objectForKey:@"content_txt"];
        
    NSLog(@"#Content :%@",objGiftDetail.contentText);
    }
    }
    
    objGiftDetail.imageListArray = [descriptionDic valueForKey:@"image_list"];
    
    NSMutableArray *locationDetailArray = [descriptionDic valueForKey:@"location_detail"];
    
    for(NSMutableDictionary *locDic in locationDetailArray){
    GiftLocationList *objGiftLocList = [[GiftLocationList alloc]init];
    objGiftLocList.address = [locDic objectForKey:@"address"];
    objGiftLocList.area = [locDic objectForKey:@"area"];
    objGiftLocList.city = [locDic objectForKey:@"city"];
    objGiftLocList.dealID = [locDic objectForKey:@"deal_id"];
    objGiftLocList.dealerContract = [locDic objectForKey:@"dealer_contact"];
    objGiftLocList.dealDistance = [locDic objectForKey:@"distance"];
    objGiftLocList.fullAddress = [locDic objectForKey:@"full_address"];
    objGiftLocList.locationLat = [locDic objectForKey:@"lat"];
    objGiftLocList.locationLong = [locDic objectForKey:@"lng"];
    objGiftLocList.state = [locDic objectForKey:@"state"];
        
    [objGiftDetail.locationDetailArray addObject:objGiftLocList];

    }
    
    objGiftDetail.offerAverage = [descriptionDic objectForKey:@"off_avg"];
    NSLog(@"#Value :%@",objGiftDetail.offerAverage);
    
    NSMutableArray *offerDetailArrayList = [descriptionDic valueForKey:@"offer_detail"];
    for(NSMutableDictionary *offerDic in offerDetailArrayList){
    GiftOfferDetailList *objGiftOfferList = [[GiftOfferDetailList alloc]init];
    objGiftOfferList.actualPrice = [offerDic objectForKey:@"actual_price"];
    objGiftOfferList.byVouchers = [offerDic objectForKey:@"by_voucher"];

    objGiftOfferList.couponExpiryDate = [offerDic objectForKey:@"coupon_expiry_date"];
    objGiftOfferList.created = [offerDic objectForKey:@"created"];
    objGiftOfferList.dealID = [offerDic objectForKey:@"deal_id"];
    objGiftOfferList.dealName = [offerDic objectForKey:@"deal_name"];
    objGiftOfferList.dealerContact = [offerDic objectForKey:@"dealer_contact"];
    objGiftOfferList.discount = [offerDic objectForKey:@"discount"];
    objGiftOfferList.globalFlag = [offerDic objectForKey:@"global_flag"];
    objGiftOfferList.giftOfferID = [offerDic objectForKey:@"id"];
    objGiftOfferList.imageURL = [offerDic objectForKey:@"img_url"];
    objGiftOfferList.longDesc = [offerDic objectForKey:@"long_desc"];
    objGiftOfferList.mailStatus = [offerDic objectForKey:@"mail_status"];
    objGiftOfferList.maxLimitPerUser = [offerDic objectForKey:@"max_limit_peruser"];
    objGiftOfferList.maxQuantity = [offerDic objectForKey:@"max_quantity"];
    objGiftOfferList.minAmount = [offerDic objectForKey:@"min_amount"];
    objGiftOfferList.myLikes = [offerDic objectForKey:@"mylikes"];
    objGiftOfferList.name = [offerDic objectForKey:@"name"];
    objGiftOfferList.offerAverage = [offerDic objectForKey:@"off_avg"];
    objGiftOfferList.offerDesc = [offerDic objectForKey:@"offer_desc"];
    objGiftOfferList.offerPrice = [offerDic objectForKey:@"offer_price"];
    objGiftOfferList.orderFlag = [offerDic objectForKey:@"order_flag"];
    objGiftOfferList.review = [offerDic objectForKey:@"review"];
    objGiftOfferList.sequence = [offerDic objectForKey:@"sequence"];
    objGiftOfferList.shortDesc = [offerDic objectForKey:@"short_desc"];
    objGiftOfferList.status = [offerDic objectForKey:@"status"];
    objGiftOfferList.stockSold = [offerDic objectForKey:@"stock_sold"];
    objGiftOfferList.totalStock = [offerDic objectForKey:@"total_stock"];
    objGiftOfferList.updated = [offerDic objectForKey:@"updated"];
    objGiftOfferList.validity = [offerDic objectForKey:@"validity"];
    objGiftOfferList.validityMode = [offerDic objectForKey:@"validity_mode"];
        
    [objGiftDetail.offerDetailArray addObject:objGiftOfferList];


    }
    
    objGiftDetail.offerInfoDic = [descriptionDic objectForKey:@"offer_info"];
    objGiftDetail.appointment = [objGiftDetail.offerInfoDic objectForKey:@"appointment"];
    objGiftDetail.days = [objGiftDetail.offerInfoDic objectForKey:@"day"];
    objGiftDetail.dealerContact = [objGiftDetail.offerInfoDic objectForKey:@"dealer_contact"];
    objGiftDetail.exclusiveGender = [objGiftDetail.offerInfoDic objectForKey:@"exclusive_gender"];
    objGiftDetail.time = [objGiftDetail.offerInfoDic objectForKey:@"time"];
    
    objGiftDetail.offerInfoHTML = [descriptionDic objectForKey:@"offer_info_html"];
    objGiftDetail.offerLike = [descriptionDic objectForKey:@"offer_like"];
    objGiftDetail.pocketGift = [descriptionDic objectForKey:@"pocketGift"];

    }
    
    return objGiftDetail;
}

@end
