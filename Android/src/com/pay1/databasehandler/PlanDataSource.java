package com.pay1.databasehandler;

import java.util.ArrayList;
import java.util.WeakHashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class PlanDataSource {

	// Database fields
	private SQLiteDatabase database;
	private Pay1SQLiteHelper dbHelper;

	public PlanDataSource(Context context) {
		dbHelper = new Pay1SQLiteHelper(context);
	}

	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
	}

	public void createPlan(String OperatorID, String OperatorName,
			String CircleID, String CircleName, String PlanType, String Amount,
			String Validity, String Description, String Time) {
		ContentValues values = new ContentValues();
		values.put(Pay1SQLiteHelper.PLAN_OPERATOR_ID, OperatorID);
		values.put(Pay1SQLiteHelper.PLAN_OPERATOR_NAME, OperatorName);
		values.put(Pay1SQLiteHelper.PLAN_CIRCLE_ID, CircleID);
		values.put(Pay1SQLiteHelper.PLAN_CIRCLE_NAME, CircleName);
		values.put(Pay1SQLiteHelper.PLAN_TYPE, PlanType);
		values.put(Pay1SQLiteHelper.PLAN_AMOUNT, Amount);
		values.put(Pay1SQLiteHelper.PLAN_VALIDITY, Validity);
		values.put(Pay1SQLiteHelper.PLAN_DESCRIPTION, Description);
		values.put(Pay1SQLiteHelper.PLAN_UPDATE_TIME, Time);

		database.insert(Pay1SQLiteHelper.TABLE_PLAN, null, values);
		// Cursor cursor = database.query(Pay1SQLiteHelper.TABLE_Plan,
		// null, Pay1SQLiteHelper.Plan_ID + " = " + insertId, null,
		// null, null, null);
		// cursor.moveToFirst();
		// Plan nePlan = cursorToPlan(cursor);
		// cursor.close();
		// return newPlan;
	}

	public void deletePlan() {
		database.delete(Pay1SQLiteHelper.TABLE_PLAN, null, null);
	}

	public void deletePlan(Plan plan) {
		long id = plan.getPlanID();
		database.delete(Pay1SQLiteHelper.TABLE_PLAN, Pay1SQLiteHelper.PLAN_ID
				+ " = " + id, null);
	}

	public ArrayList<String> getPlanType(String operator, String circle,
			int isDataCardPlan) {
		ArrayList<String> list = new ArrayList<String>();
		Cursor cursor = null;
		if (isDataCardPlan == 0) {// false
			cursor = database.query(Pay1SQLiteHelper.TABLE_PLAN, null,
					Pay1SQLiteHelper.PLAN_OPERATOR_ID + " = '" + operator
							+ "' AND " + Pay1SQLiteHelper.PLAN_CIRCLE_ID
							+ " = '" + circle + "'", null,
					Pay1SQLiteHelper.PLAN_TYPE, null,
					Pay1SQLiteHelper.PLAN_TYPE);
		} else {// true
			cursor = database.query(Pay1SQLiteHelper.TABLE_PLAN, null,
					Pay1SQLiteHelper.PLAN_OPERATOR_ID + " = '" + operator
							+ "' AND " + Pay1SQLiteHelper.PLAN_CIRCLE_ID
							+ " = '" + circle + "' AND ("
							+ Pay1SQLiteHelper.PLAN_TYPE
							+ " = 'Data-Plans' OR "
							+ Pay1SQLiteHelper.PLAN_TYPE + " = '3G-Plans')",
					null, Pay1SQLiteHelper.PLAN_TYPE, null,
					Pay1SQLiteHelper.PLAN_TYPE);
		}
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Plan plan = cursorToPlan(cursor);
			list.add(plan.getPlanType());
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return list;
	}

	public List<Plan> getAllPlan() {
		List<Plan> plans = new ArrayList<Plan>();

		Cursor cursor = database.query(Pay1SQLiteHelper.TABLE_PLAN, null, null,
				null, null, null, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Plan plan = cursorToPlan(cursor);
			plans.add(plan);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return plans;
	}

	public List<Plan> getAllPlan(String operator, String circle) {
		List<Plan> plans = new ArrayList<Plan>();

		Cursor cursor = database.query(Pay1SQLiteHelper.TABLE_PLAN, null,
				Pay1SQLiteHelper.PLAN_OPERATOR_ID + " = '" + operator
						+ "' AND " + Pay1SQLiteHelper.PLAN_CIRCLE_ID + " = '"
						+ circle + "'", null, null, null, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Plan plan = cursorToPlan(cursor);
			plans.add(plan);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return plans;
	}

	public JSONArray getAllPlanDetails(String operator, String circle,
			String plantype, int isDataCardPlan) {
		ArrayList<WeakHashMap<String, String>> wordList = new ArrayList<WeakHashMap<String, String>>();
		Cursor cursor = null;
		if (plantype.equalsIgnoreCase("") || plantype == null) {
			if (isDataCardPlan == 0) {// false
				cursor = database.query(Pay1SQLiteHelper.TABLE_PLAN, null,
						Pay1SQLiteHelper.PLAN_OPERATOR_ID + " = '" + operator
								+ "' AND " + Pay1SQLiteHelper.PLAN_CIRCLE_ID
								+ " = '" + circle + "'", null, null, null,
						Pay1SQLiteHelper.PLAN_AMOUNT);
			} else {// true
				cursor = database
						.query(Pay1SQLiteHelper.TABLE_PLAN, null,
								Pay1SQLiteHelper.PLAN_OPERATOR_ID + " = '"
										+ operator + "' AND "
										+ Pay1SQLiteHelper.PLAN_CIRCLE_ID
										+ " = '" + circle + "' AND ("
										+ Pay1SQLiteHelper.PLAN_TYPE
										+ " = 'Data-Plans' OR "
										+ Pay1SQLiteHelper.PLAN_TYPE
										+ " = '3G-Plans')", null, null, null,
								Pay1SQLiteHelper.PLAN_AMOUNT);

			}
		} else {
			cursor = database.query(Pay1SQLiteHelper.TABLE_PLAN, null,
					Pay1SQLiteHelper.PLAN_OPERATOR_ID + " = '" + operator
							+ "' AND " + Pay1SQLiteHelper.PLAN_CIRCLE_ID
							+ " = '" + circle + "' AND "
							+ Pay1SQLiteHelper.PLAN_TYPE + " = '" + plantype
							+ "'", null, null, null,
					Pay1SQLiteHelper.PLAN_AMOUNT);
		}

		JSONArray array = new JSONArray();
		JSONObject json1 = null;

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Plan plan = cursorToPlan(cursor);
			WeakHashMap<String, String> map = new WeakHashMap<String, String>();
			map.put("description", plan.getPlanDescription());
			map.put("validity", plan.getPlanValidity());
			map.put("amount", "" + plan.getPlanAmount());
			map.put("plantype", plan.getPlanType());
			wordList.add(map);
			json1 = new JSONObject(map);

			array.put(json1);
			cursor.moveToNext();
		}
		cursor.close();
		// return contact list
		// // Log.e("json", "json " + array);
		return array;
	}

	public List<Plan> getPlanDetails(String operator, String circle,
			int amount, int isDataCardPlan) {
		List<Plan> plans = new ArrayList<Plan>();

		Cursor cursor = null;
		if (isDataCardPlan == 0) {// false
			cursor = database.query(Pay1SQLiteHelper.TABLE_PLAN, null,
					Pay1SQLiteHelper.PLAN_OPERATOR_ID + " = '" + operator
							+ "' AND " + Pay1SQLiteHelper.PLAN_CIRCLE_ID
							+ " = '" + circle + "' AND "
							+ Pay1SQLiteHelper.PLAN_AMOUNT + " = " + amount,
					null, null, null, null);
		} else {// true
			cursor = database.query(Pay1SQLiteHelper.TABLE_PLAN, null,
					Pay1SQLiteHelper.PLAN_OPERATOR_ID + " = '" + operator
							+ "' AND " + Pay1SQLiteHelper.PLAN_CIRCLE_ID
							+ " = '" + circle + "' AND "
							+ Pay1SQLiteHelper.PLAN_AMOUNT + " = " + amount
							+ " AND (" + Pay1SQLiteHelper.PLAN_TYPE
							+ " = 'Data-Plans' OR "
							+ Pay1SQLiteHelper.PLAN_TYPE + " = '3G-Plans')",
					null, null, null, null);
		}
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Plan plan = cursorToPlan(cursor);
			plans.add(plan);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return plans;
	}

	private Plan cursorToPlan(Cursor cursor) {
		Plan plan = new Plan();
		plan.setPlanID(cursor.getInt(0));
		plan.setPlanOpertorID(cursor.getString(1));
		plan.setPlanOperatorName(cursor.getString(2));
		plan.setPlanCircleID(cursor.getString(3));
		plan.setPlanCircleName(cursor.getString(4));
		plan.setPlanType(cursor.getString(5));
		plan.setPlanAmount(cursor.getInt(6));
		plan.setPlanValidity(cursor.getString(7));
		plan.setPlanDescription(cursor.getString(8));
		plan.setPlanUptateTime(cursor.getString(9));

		return plan;
	}
}
