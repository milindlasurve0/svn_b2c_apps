package com.pay1;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.SQLException;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.larvalabs.svgandroid.SVGParser;
import com.pay1.adapterhandler.GiftGridAdapter;
import com.pay1.adapterhandler.SampleGiftGridAdapter;
import com.pay1.constants.Constants;
import com.pay1.customviews.MyProgressDialog;
import com.pay1.databasehandler.FreeBies;
import com.pay1.databasehandler.FreeBiesDataSource;
import com.pay1.model.GiftGridItem;
import com.pay1.requesthandler.RequestClass;
import com.pay1.utilities.Utility;

public class GiftGridActivity extends Activity {

	private static final String SCREEN_LABEL = "Gift Category Screen";
	private EasyTracker easyTracker = null;

	private ImageView imageView_Back;
	private TextView textView_Title;
	private GridView gridView_Gifts;
	//private GiftGridAdapter adapter;

	private SampleGiftGridAdapter adapterGift;
	private ArrayList<FreeBies> data;
	private ArrayList<GiftGridItem> giftGridItem;
	
	private FreeBiesDataSource freeBiesDataSource;
	int typeID = 0, page = 0;

	private int mVisibleThreshold = 5;
	private int mCurrentPage = 0;
	private int mPreviousTotal = 0;
	private boolean mLoading = true;
	private boolean mLastPage = false;
	int fromView = 0;

	@SuppressLint("NewApi")
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.gift_grid_activity);
		try {
			easyTracker = EasyTracker.getInstance(GiftGridActivity.this);
			easyTracker.set(Fields.SCREEN_NAME, SCREEN_LABEL);
			freeBiesDataSource = new FreeBiesDataSource(GiftGridActivity.this);

			Typeface Medium = Typeface.createFromAsset(getAssets(),
					"Roboto-Regular.ttf");

			textView_Title = (TextView) findViewById(R.id.textView_Title);
			textView_Title.setTypeface(Medium);

			imageView_Back = (ImageView) findViewById(R.id.imageView_Back);
			imageView_Back.setImageDrawable(SVGParser.getSVGFromResource(
					getResources(), R.raw.ic_back_old).createPictureDrawable());
			if (Build.VERSION.SDK_INT >= 11)
				imageView_Back.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
			imageView_Back.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onBackPressed();
				}
			});

			giftGridItem=new ArrayList<GiftGridItem>();
			
			RelativeLayout back_layout = (RelativeLayout) findViewById(R.id.back_layout);
			back_layout.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onBackPressed();
				}
			});

			data = new ArrayList<FreeBies>();
			gridView_Gifts = (GridView) findViewById(R.id.gridView_Gifts);

		/*	adapter = new GiftGridAdapter(GiftGridActivity.this, data, false,
					false, false);*/
			adapterGift = new SampleGiftGridAdapter(GiftGridActivity.this, giftGridItem, false,
					false, false);
			// items.clear();
			gridView_Gifts.setAdapter(adapterGift);
			gridView_Gifts
					.setOnItemClickListener(new AdapterView.OnItemClickListener() {

						@Override
						public void onItemClick(AdapterView<?> parent,
								View view, int position, long id) {
							// TODO Auto-generated method stub
							try {
								FreeBies freeBie = data.get(position);
								Intent intent = new Intent(
										GiftGridActivity.this,
										MyFreeBiesDetailsActivity.class);
								intent.putExtra("GIFT", freeBie);
								intent.putExtra("IS_BUY", 0);
								startActivity(intent);
							} catch (Exception e) {
							}
						}
					});

			gridView_Gifts
					.setOnScrollListener(new AbsListView.OnScrollListener() {

						@Override
						public void onScrollStateChanged(AbsListView view,
								int scrollState) {
							// TODO Auto-generated method stub
						}

						@Override
						public void onScroll(AbsListView view,
								int firstVisibleItem, int visibleItemCount,
								int totalItemCount) {
							// TODO Auto-generated method stub
							// if (firstVisibleItem + visibleItemCount >=
							// totalItemCount) {
							// // End has been reached
							// ++page;
							// System.out.println("PAGE " + page);
							// new GiftByCategoryTask().execute(
							// String.valueOf(typeID),
							// String.valueOf(page));
							// }

							if (mLoading) {
								if (totalItemCount > mPreviousTotal) {

									mLoading = false;
									mPreviousTotal = totalItemCount;
									mCurrentPage++;

									// Find your own condition in order to know
									// when you
									// have finished displaying all items
									if (mCurrentPage + 1 > 50) {
										mLastPage = true;
									}
								}
							}
							if (!mLastPage
									&& !mLoading
									&& (totalItemCount - visibleItemCount) <= (firstVisibleItem + mVisibleThreshold)) {
								++page;
								/*
								 * new GiftByCategoryTask().execute(
								 * String.valueOf(typeID),
								 * String.valueOf(page));
								 */

								if (fromView == 1) {
									new GiftByCategoryTask().execute(
											String.valueOf(typeID),
											String.valueOf(page),
											String.valueOf(fromView));
								} else if (fromView == 0) {
									new GiftByTypeTask().execute(
											String.valueOf(typeID),
											String.valueOf(page),
											String.valueOf(fromView));
								}
								mLoading = true;
							}
						}
					});

			try {

				typeID = Integer.parseInt(getIntent().getStringExtra("TYPE"));
				textView_Title.setText(getIntent().getStringExtra("NAME"));
				fromView = getIntent().getExtras().getInt("FROM");

			} catch (Exception e) {
				typeID = 0;
			}

			if (fromView == 1) {
				new GiftByCategoryTask().execute(String.valueOf(typeID),
						String.valueOf(page), String.valueOf(fromView));
			} else if (fromView == 0) {
				new GiftByTypeTask().execute(String.valueOf(typeID),
						String.valueOf(page), String.valueOf(fromView));
			}
		} catch (Exception e) {
		}
		overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}

	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView().build());
		// EasyTracker.getInstance(this).activityStart(this);
		easyTracker.send(MapBuilder.createAppView().build());
	}

	@Override
	public void onStop() {
		super.onStop();
		// EasyTracker.getInstance(this).activityStop(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		 adapterGift.imageLoader.clearCache();
         adapterGift.notifyDataSetChanged();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
	}

	public class GiftByCategoryTask extends AsyncTask<String, String, String>
			implements OnDismissListener {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			try {

				String response = RequestClass.getInstance()
						.readPay1B2CRequest(
								GiftGridActivity.this,
								Constants.B2C_URL
										+ "GetAllOfferCategory/?type="
										+ params[0] + "&next=" + params[1]);
				/*
				 * }else{ response = RequestClass.getInstance()
				 * .readPay1B2CRequest( GiftGridActivity.this, Constants.B2C_URL
				 * + "GetAllOfferTypes/?type=" + params[0] + "&next=" +
				 * params[1]); }
				 */
				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				if (this.dialog.isShowing()) {
					this.dialog.dismiss();
				}
			} catch (Exception e) {
			}
			try {
				if (!result.startsWith("Error")) {

					JSONObject jsonObject = new JSONObject(result);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {

						try {

							freeBiesDataSource.open();

							JSONArray jsonArrayDescription = jsonObject
									.getJSONArray("description");

							for (int i = 0; i < jsonArrayDescription.length(); i++) {

								String str = jsonArrayDescription
										.getJSONObject(i).getString("details");
								List<FreeBies> list = freeBiesDataSource
										.getAllFreeBies(str);
								int len = list.size();
								if (len != 0) {
									for (FreeBies freebie : list){
										data.add(freebie);
										
										
										

										GiftGridItem giftGrid = new GiftGridItem();
										
										giftGrid.setFreeBieItem(freebie);
										if (freebie.getFreebieLike() == 0)
											giftGrid.setDealLikeUnlike(R.drawable.ic_unlike);
										else
											giftGrid.setDealLikeUnlike(R.drawable.ic_like);

										
										
										try {
											String lat1 = Utility.getCurrentLatitude(GiftGridActivity.this,
													Constants.SHAREDPREFERENCE_CURRENT_LATITUDE);
											String lng1 = Utility.getCurrentLongitude(GiftGridActivity.this,
													Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE);
											String d = "0";
											if (lat1 != "" && lng1 != "")
												d = String.valueOf(Constants.distanceFrom(
														Double.parseDouble(lat1), Double.parseDouble(lng1),
														freebie.getFreebieLat(), freebie.getFreebieLng()));
											Double dis = Double.parseDouble(d);
											DecimalFormat decimalFormat = new DecimalFormat("0.0");
											if (dis == 0) {
												//holder.textView_Distance.setVisibility(View.INVISIBLE);
												giftGrid.setDealDistanceArea("");
											} else if (dis < 1) {
												dis = dis * 1000;
												
												giftGrid.setDealDistanceArea(freebie.getFreebieArea()+" : < 1KM ");
											} else {
												giftGrid.setDealDistanceArea(freebie.getFreebieArea()+" : "
														+ decimalFormat.format(dis) + " KM");
												
											}
										} catch (Exception e) {
											giftGrid.setDealDistanceArea("");
										}
									
										ImageSpan imageSpanCoin=new ImageSpan(GiftGridActivity.this, R.drawable.sample_gift_coin,ImageSpan.ALIGN_BASELINE);
										
										ImageSpan imageSpanRupee=new ImageSpan(GiftGridActivity.this, R.drawable.sample_gift_coin,ImageSpan.ALIGN_BASELINE);
										
										
										if (freebie.getFreeBieByVoucher() == 1) {
											
											SpannableStringBuilder builder = new SpannableStringBuilder();
											builder.append("").append(" ");
											builder.setSpan(imageSpanRupee,
													builder.length() - 1, builder.length(), 0);
											builder.append(" "+freebie.getFreeBieOfferPrice() + "").append(" ");

											builder.append(" + ");
											builder.setSpan(imageSpanCoin, builder.length() - 1,
													builder.length(), 0);
											builder.append(" "+freebie.getFreebieMinAmount() + "").append(" ");
											giftGrid.setLoyaltyPoints(builder);
										} else {
											SpannableStringBuilder builder = new SpannableStringBuilder();
											builder.append("").append(" ");
											
											builder.setSpan(imageSpanCoin, builder.length() - 1,
													builder.length(), 0);
											builder.append(" "+freebie.getFreebieMinAmount() + "").append(" ");
											giftGrid.setLoyaltyPoints(builder);
										}
										
										giftGrid.setLogoImageUrl(freebie.getFreebieLogoURL());
										giftGrid.setDealImageUrl(freebie.getFreebieURL());
										giftGrid.setOfferID(freebie.getFreebieID());
										giftGrid.setOfferName(freebie.getFreebieShortDesc());
										giftGrid.setDealName(freebie.getFreebieDealName());
										
										
										giftGridItem.add(giftGrid);
									
										
										
									}
								} else {
									// new FreeBiesTask()
									// .execute(
									// Utility.getCurrentLatitude(
									// GiftGridActivity.this,
									// Constants.SHAREDPREFERENCE_CURRENT_LATITUDE),
									// Utility.getCurrentLongitude(
									// GiftGridActivity.this,
									// Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE));
									// pager.setVisibility(View.GONE);

								}
								GiftGridActivity.this
										.runOnUiThread(new Runnable() {

											@Override
											public void run() {
												// TODO Auto-generated
												// method stub
												adapterGift.notifyDataSetChanged();
											}
										});

							}

						} catch (SQLException exception) {
							// TODO: handle exception
							// Log.e("Er ", exception.getMessage());
						} catch (Exception e) {
							// TODO: handle exception
							// Log.e("Er ", e.getMessage());
						} finally {
							freeBiesDataSource.close();
						}
					}
				}

			} catch (JSONException e) {
				// e.printStackTrace();
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(GiftGridActivity.this);
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							dialog.dismiss();
							GiftByCategoryTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			dialog.dismiss();
			GiftByCategoryTask.this.cancel(true);
		}
	}

	public class GiftByTypeTask extends AsyncTask<String, String, String>
			implements OnDismissListener {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			try {

				String response = RequestClass.getInstance()
						.readPay1B2CRequest(
								GiftGridActivity.this,
								Constants.B2C_URL + "GetAllOfferTypes/?type="
										+ params[0] + "&next=" + params[1]);
				/*
				 * }else{ response = RequestClass.getInstance()
				 * .readPay1B2CRequest( GiftGridActivity.this, Constants.B2C_URL
				 * + "GetAllOfferTypes/?type=" + params[0] + "&next=" +
				 * params[1]); }
				 */
				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				if (this.dialog.isShowing()) {
					this.dialog.dismiss();
				}
			} catch (Exception e) {
			}
			try {
				if (!result.startsWith("Error")) {

					JSONObject jsonObject = new JSONObject(result);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {

						try {

							freeBiesDataSource.open();

							JSONObject description = jsonObject
									.getJSONObject("description");
							JSONArray jsonArrayDescription = description
									.getJSONArray("offer_details");

							for (int i = 0; i < jsonArrayDescription.length(); i++) {

								String str = jsonArrayDescription
										.getJSONObject(i).getString("details");
								List<FreeBies> list = freeBiesDataSource
										.getAllFreeBies(str);
								int len = list.size();
								if (len != 0) {
									for (FreeBies freebie : list){
										data.add(freebie);
										
										
										

										GiftGridItem giftGrid = new GiftGridItem();
										
										giftGrid.setFreeBieItem(freebie);
										if (freebie.getFreebieLike() == 0)
											giftGrid.setDealLikeUnlike(R.drawable.ic_unlike);
										else
											giftGrid.setDealLikeUnlike(R.drawable.ic_like);

										
										
										try {
											String lat1 = Utility.getCurrentLatitude(GiftGridActivity.this,
													Constants.SHAREDPREFERENCE_CURRENT_LATITUDE);
											String lng1 = Utility.getCurrentLongitude(GiftGridActivity.this,
													Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE);
											String d = "0";
											if (lat1 != "" && lng1 != "")
												d = String.valueOf(Constants.distanceFrom(
														Double.parseDouble(lat1), Double.parseDouble(lng1),
														freebie.getFreebieLat(), freebie.getFreebieLng()));
											Double dis = Double.parseDouble(d);
											DecimalFormat decimalFormat = new DecimalFormat("0.0");
											if (dis == 0) {
												//holder.textView_Distance.setVisibility(View.INVISIBLE);
												giftGrid.setDealDistanceArea("");
											} else if (dis < 1) {
												dis = dis * 1000;
												
												giftGrid.setDealDistanceArea(freebie.getFreebieArea()+" : < 1KM ");
											} else {
												giftGrid.setDealDistanceArea(freebie.getFreebieArea()+" : "
														+ decimalFormat.format(dis) + " KM");
												
											}
										} catch (Exception e) {
											giftGrid.setDealDistanceArea("");
										}
									
										ImageSpan imageSpanCoin=new ImageSpan(GiftGridActivity.this, R.drawable.sample_gift_coin,ImageSpan.ALIGN_BASELINE);
										
										ImageSpan imageSpanRupee=new ImageSpan(GiftGridActivity.this, R.drawable.sample_gift_coin,ImageSpan.ALIGN_BASELINE);
										
										
										if (freebie.getFreeBieByVoucher() == 1) {
											
											SpannableStringBuilder builder = new SpannableStringBuilder();
											builder.append("").append(" ");
											builder.setSpan(imageSpanRupee,
													builder.length() - 1, builder.length(), 0);
											builder.append(" "+freebie.getFreeBieOfferPrice() + "").append(" ");

											builder.append(" + ");
											builder.setSpan(imageSpanCoin, builder.length() - 1,
													builder.length(), 0);
											builder.append(" "+freebie.getFreebieMinAmount() + "").append(" ");
											giftGrid.setLoyaltyPoints(builder);
										} else {
											SpannableStringBuilder builder = new SpannableStringBuilder();
											builder.append("").append(" ");
											
											builder.setSpan(imageSpanCoin, builder.length() - 1,
													builder.length(), 0);
											builder.append(" "+freebie.getFreebieMinAmount() + "").append(" ");
											giftGrid.setLoyaltyPoints(builder);
										}
										
										giftGrid.setLogoImageUrl(freebie.getFreebieLogoURL());
										giftGrid.setDealImageUrl(freebie.getFreebieURL());
										giftGrid.setOfferID(freebie.getFreebieID());
										giftGrid.setOfferName(freebie.getFreebieShortDesc());
										giftGrid.setDealName(freebie.getFreebieDealName());
										
										
										giftGridItem.add(giftGrid);
									
										
										
									}
								} else {
									// new FreeBiesTask()
									// .execute(
									// Utility.getCurrentLatitude(
									// GiftGridActivity.this,
									// Constants.SHAREDPREFERENCE_CURRENT_LATITUDE),
									// Utility.getCurrentLongitude(
									// GiftGridActivity.this,
									// Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE));
									// pager.setVisibility(View.GONE);

								}
								GiftGridActivity.this
										.runOnUiThread(new Runnable() {

											@Override
											public void run() {
												// TODO Auto-generated
												// method stub
												adapterGift.notifyDataSetChanged();
											}
										});

							}

						} catch (SQLException exception) {
							// TODO: handle exception
							// Log.e("Er ", exception.getMessage());
						} catch (Exception e) {
							// TODO: handle exception
							// Log.e("Er ", e.getMessage());
						} finally {
							freeBiesDataSource.close();
						}
					}
				}

			} catch (JSONException e) {
				// e.printStackTrace();
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(GiftGridActivity.this);
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							dialog.dismiss();
							GiftByTypeTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			dialog.dismiss();
			GiftByTypeTask.this.cancel(true);
		}
	}
}
