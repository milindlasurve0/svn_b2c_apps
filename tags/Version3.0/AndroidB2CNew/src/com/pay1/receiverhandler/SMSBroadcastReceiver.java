package com.pay1.receiverhandler;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.SQLException;
import android.os.Bundle;
import android.telephony.SmsMessage;

import com.pay1.ChangePinActivity;
import com.pay1.NotificationActivity;
import com.pay1.UpdateBalanceActivity;
import com.pay1.VerificationActivity;
import com.pay1.constants.Constants;
import com.pay1.databasehandler.Operator;
import com.pay1.databasehandler.OperatorDataSource;
import com.pay1.servicehandler.OperatorService;
import com.pay1.utilities.Utility;

public class SMSBroadcastReceiver extends BroadcastReceiver {

	private static final String SMS_RECEIVED = "android.provider.Telephony.SMS_RECEIVED";
	private static final String BAL_RECEIVED = "check.balance";

	private OperatorDataSource operatorDataSource;

	// private static final String TAG = "SMSBroadcastReceiver";

	@Override
	public void onReceive(Context context, Intent intent) {
		// // Log.i(TAG, "Intent received: " + intent.getAction());
		try {
			if (Constants.isOnline(context)) {

				ActivityManager activityManager = (ActivityManager) context
						.getSystemService(Context.ACTIVITY_SERVICE);
				String str = activityManager.getRunningTasks(1).get(0).topActivity
						.getPackageName();

				if (!str.equalsIgnoreCase("com.pay1")) {
					String tm = Utility.getSharedPreferences(context,
							Constants.LAST_GIFT_UPDATED);
					tm = tm != null ? tm : "0";
					int days = (int) ((System.currentTimeMillis() - Long
							.parseLong(tm)) / (1000 * 60 * 60 * 24));
					/*if (days >= 1)
						context.startService(new Intent(context,
								FreeBiesUpdateService.class));*/

					try {
						operatorDataSource = new OperatorDataSource(context);

						operatorDataSource.open();

						Operator operator = operatorDataSource.getTopOperator();

						if (operator != null) {
							int days1 = (int) ((System.currentTimeMillis() - operator
									.getOperatorUptateTime()) / (1000 * 60 * 60 * 24));
							if (days1 >= 2) {
								context.startService(new Intent(context,
										OperatorService.class));
							}
						} else {
							context.startService(new Intent(context,
									OperatorService.class));
						}

					} catch (SQLException exception) {
						// TODO: handle exception
						// // // Log.e("Er ", exception.getMessage());
					} catch (Exception e) {
						// TODO: handle exception
						// // // Log.e("Er ", e.getMessage());
					} finally {
						operatorDataSource.close();
					}
				}
			}
		} catch (Exception e) {
		}
		if (intent.getAction().equals(SMS_RECEIVED)) {
			Bundle bundle = intent.getExtras();
			if (bundle != null) {
				Object[] pdus = (Object[]) bundle.get("pdus");
				final SmsMessage[] messages = new SmsMessage[pdus.length];
				for (int i = 0; i < pdus.length; i++) {
					messages[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
					if (messages.length > -1) {
						try {
							String FROM = messages[i].getOriginatingAddress();

							if (FROM.contains("PAYONE")) {

								String str = messages[0].getMessageBody()
										.toString().trim();
								if (str.toLowerCase().contains("otp")) {
									Pattern p = Pattern.compile("(\\d+)");
									Matcher m = p.matcher(str);
									while (m.find()) {
										String temp = m.group(1);
										if (temp.length() == 4
												|| temp.length() == 5) {

											ActivityManager am = (ActivityManager) context
													.getSystemService(Activity.ACTIVITY_SERVICE);
											String className = am
													.getRunningTasks(1).get(0).topActivity
													.getClassName();

											if (className
													.equalsIgnoreCase("com.pay1.ChangePinActivity")) {
												Intent intent2 = new Intent(
														context,
														ChangePinActivity.class);
												intent2.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
												intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
												intent2.putExtra("OTP", temp);
												context.startActivity(intent2);
											} else if (className
													.equalsIgnoreCase("com.pay1.VerificationActivity")) {
												Intent intent2 = new Intent(
														context,
														VerificationActivity.class);
												intent2.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
												intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
												intent2.putExtra("OTP", temp);
												context.startActivity(intent2);
											}
											break;
										}
									}
									// if (str.substring(0, str.indexOf(":"))
									// .trim()
									// .equalsIgnoreCase(
									// "Dear Pay1 User, Your One time Password (OTP) for you app registration is"))
									// {

								} else if (Constants
										.machTemplate(
												str,
												Utility.getTemplate(
														context,
														Constants.SHAREDPREFERENCE_SMS_BILLPAYMENT_FAILURE),
												"<", ">")
										|| Constants
												.machTemplate(
														str,
														Utility.getTemplate(
																context,
																Constants.SHAREDPREFERENCE_SMS_RECHARGE_FAILURE),
														"<", ">")
										|| Constants
												.machTemplate(
														str,
														Utility.getTemplate(
																context,
																Constants.SHAREDPREFERENCE_SMS_WALLET_REFILLED_RETAILER),
														"<", ">")) {
									Utility.setRefreshRequired(
											context,
											Constants.SHAREDPREFERENCE_REFESH_QUICKPAY,
											true);
									Utility.setRefreshRequired(
											context,
											Constants.SHAREDPREFERENCE_REFESH_MY_GIFT,
											true);
									Utility.setRefreshRequired(
											context,
											Constants.SHAREDPREFERENCE_REFESH_TRANSATION,
											true);
									Utility.setRefreshRequired(
											context,
											Constants.SHAREDPREFERENCE_REFESH_WALLET,
											true);
									Intent intent2 = new Intent(context,
											UpdateBalanceActivity.class);
									intent2.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
									intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
									// intent2.putExtra("MSG", str);
									context.startActivity(intent2);
								} else {
									Utility.setRefreshRequired(
											context,
											Constants.SHAREDPREFERENCE_REFESH_QUICKPAY,
											true);
									Utility.setRefreshRequired(
											context,
											Constants.SHAREDPREFERENCE_REFESH_MY_GIFT,
											true);
									Utility.setRefreshRequired(
											context,
											Constants.SHAREDPREFERENCE_REFESH_TRANSATION,
											true);
									Utility.setRefreshRequired(
											context,
											Constants.SHAREDPREFERENCE_REFESH_WALLET,
											true);
								}
							}
						} catch (Exception e) {
						}
					}
				}

			}
		} else if (intent.getAction().equals(BAL_RECEIVED)) {
			Bundle extras = intent.getExtras();
			if (extras != null) {
				if (extras.containsKey("value")) {
					String str = extras.getString("value").trim();
					// System.out.println("Value is:" + str);

					Intent intent2 = new Intent(context,
							NotificationActivity.class);
					intent2.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
					intent2.putExtra("MSG", str);
					context.startActivity(intent2);
				}
			}
		}
	}
}