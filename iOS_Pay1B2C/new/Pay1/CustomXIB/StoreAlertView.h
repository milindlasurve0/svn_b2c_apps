//
//  StoreAlertView.h
//  Pay1
//
//  Created by webninjaz on 03/09/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StoreAlertView : UIView{
    IBOutlet UIButton *checkButton;
}


@property (nonatomic, copy)void (^onCheckClick)();
@property (nonatomic, copy)void (^onDoneClick)();

@end
