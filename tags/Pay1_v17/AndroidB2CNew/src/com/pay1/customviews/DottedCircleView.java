package com.pay1.customviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.view.View;

import com.pay1.R;

public class DottedCircleView extends View {

	private static final float DASH_INTERVAL = 0;
	int width, height, radius, pinXOffset, pinYOffset, color;
	Paint p;
	BitmapDrawable pin;

	public DottedCircleView(Context context, AttributeSet attrs,
			int defStyleAttr) {
		super(context, attrs, defStyleAttr);

		TypedArray a = context.obtainStyledAttributes(attrs,
				R.styleable.DottedCircleView, 0, 0);
		color = a.getColor(R.styleable.DottedCircleView_circleColor,
				R.color.app_blue_color);
		radius = a.getInteger(R.styleable.DottedCircleView_circleRadius, 0);
		a.recycle();

		setup();
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		width = MeasureSpec.getSize(widthMeasureSpec);
		height = MeasureSpec.getSize(heightMeasureSpec);

		if (radius == 0) {
			radius = Math.min(width, height) / 2 - (int) p.getStrokeWidth();
		}

		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	}

	@Override
	public void onDraw(Canvas canvas) {
		super.onDraw(canvas);

		canvas.drawCircle(width / 2, height / 2, radius, p);
		// draw the map marker in middle of the circle
		canvas.drawBitmap(pin.getBitmap(), (width / 2) - pinXOffset,
				(height / 2) - pinYOffset, null);
		invalidate();
	}

	private void setup() {
		p = new Paint();
		p.setColor(getResources().getColor(color));
		p.setStrokeWidth(100);
		DashPathEffect dashPath = new DashPathEffect(new float[] {
				DASH_INTERVAL, DASH_INTERVAL }, (float) 1.0);
		p.setPathEffect(dashPath);
		p.setStyle(Style.STROKE);
		pin = (BitmapDrawable) getResources().getDrawable(R.drawable.location);
		pinXOffset = pin.getIntrinsicWidth() / 2;
		pinYOffset = pin.getIntrinsicHeight();
	}
}
