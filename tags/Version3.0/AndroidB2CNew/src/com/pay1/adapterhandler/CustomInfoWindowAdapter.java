package com.pay1.adapterhandler;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.model.Marker;
import com.pay1.R;

public class CustomInfoWindowAdapter implements InfoWindowAdapter {

	private View view;
	private Context context;
	private Typeface Reguler, Medium;

	public CustomInfoWindowAdapter(Context context) {
		this.context = context;
		LayoutInflater inflater = (LayoutInflater) this.context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		view = inflater.inflate(R.layout.custom_info_window, null);

		Reguler = Typeface.createFromAsset(context.getAssets(),
				"Roboto-Regular.ttf");

		Medium = Typeface.createFromAsset(context.getAssets(),
				"Roboto-Regular.ttf");

	}

	@Override
	public View getInfoContents(Marker marker) {

		if (marker != null && marker.isInfoWindowShown()) {
			marker.hideInfoWindow();
			marker.showInfoWindow();
		}
		return null;
	}

	@Override
	public View getInfoWindow(final Marker marker) {
		// final ImageView image = ((ImageView) view.findViewById(R.id.badge));
		//
		// image.setImageResource(R.drawable.status_notification);

		final String title = marker.getTitle();
		final TextView titleUi = ((TextView) view.findViewById(R.id.title));
		titleUi.setTypeface(Medium);
		if (title != null) {
			titleUi.setText(title);
		} else {
			titleUi.setText("");
		}

		final ImageView image = ((ImageView) view
				.findViewById(R.id.imageView_Icon));
		image.setVisibility(View.GONE);

		final String snippet = marker.getSnippet();
		final TextView snippetUi = ((TextView) view.findViewById(R.id.snippet));
		snippetUi.setTypeface(Reguler);
		if (snippet != null) {
			snippetUi.setText(snippet);
		} else {
			snippetUi.setText("");
		}

		return view;
	}
}