//
//  TransactionCancelAlertView.m
//  Pay1
//
//  Created by Annapurna on 10/07/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "TransactionCancelAlertView.h"

@implementation TransactionCancelAlertView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self awakeFromNib];
    }
    return self;
}
-(void)awakeFromNib {
    
    UIView* myView = [[[NSBundle mainBundle] loadNibNamed:@"TransactionCancelAlertView" owner:self options:nil] objectAtIndex:0];
    
    if([UIScreen mainScreen].bounds.size.width == 320){
    myView.frame = CGRectMake(5, 0, 290, 414);
    }
    
    
    myView.backgroundColor = [UIColor whiteColor];
    
    detailInfoView.layer.borderColor = [UIColor grayColor].CGColor;
    detailInfoView.layer.borderWidth = 1;
    
    logoImageView.layer.borderColor = [UIColor grayColor].CGColor;
    logoImageView.layer.borderWidth = 1;
    
    logoImageView.layer.cornerRadius = logoImageView.frame.size.height/2;
    logoImageView.clipsToBounds = YES;
    logoImageView.layer.masksToBounds = YES;

    [self addSubview:myView];
    
}

-(void)PutDataOnCancelAlert:(NSString *)imageName phoneNumber:(NSString *)phoneNumber amount:(NSString *)amount type:(NSString *)type{
   // NSLog(@"#ImageName :%@",imageName);
    amountPriceLabel.text = [NSString stringWithFormat:@"Rs. %@",amount];
    numberLabel.text = phoneNumber;
    operatorLogoImageView.image = [UIImage imageNamed:imageName];
    coinLabel.text = [NSString stringWithFormat:@"You have earned %@ gift coins!",amount];
    typeLabel.text = type;
}


-(IBAction)awesomeBtnClick:(id)sender{
    if(_onAwesomeClick)
    _onAwesomeClick();
}

-(IBAction)laterBtnClick:(id)sender{
    if(_onLaterClick)
    _onLaterClick();
}



@end
