<!DOCTYPE html>
 <html>
   <head>
    <title>Partner with us | Start your Business on a Smartphone | Pay1</title>
   
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Meta Description: Join India's fastest growing cash to digital retail network & expand your Business. More than 1000 Distributors & 70,000 Retailers have trusted Pay1">
    <!-- Bootstrap -->
 
 	<link rel="stylesheet" href="css/reset.css"><!-- CSS reset -->
 
	<link href="css/bootstrap.css" rel="stylesheet" media="screen">
    
	<link href="css/style.css" rel="stylesheet" media="screen">
    
    <link href="css/style_main.css" rel="stylesheet" media="screen">
    
    <link href="css/social-buttons.css" rel="stylesheet" media="screen">
    
    <link rel="stylesheet" href="css/font-awesome.min.css">
    
    <link rel="shortcut icon" href="img/pay1_favic.png">

  </head>
 
  <body>
    
	 <!-- Include all compiled plugins (below), or include individual files as needed -->
 
	 <script src="js/bootstrap.js"></script>
	 <script id="twitter-wjs" async src="https://platform.twitter.com/widgets.js"></script>
     <script>
     (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
     (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
     m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
     })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

     ga('create', 'UA-54897637-1', 'auto');
     ga('send', 'pageview');

   </script>
   <script>(function() {
var _fbq = window._fbq || (window._fbq = []);
if (!_fbq.loaded) {
var fbds = document.createElement('script');
fbds.async = true;
fbds.src = '//connect.facebook.net/en_US/fbds.js';
var s = document.getElementsByTagName('script')[0];
s.parentNode.insertBefore(fbds, s);
_fbq.loaded = true;
}
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', '6028981762035', {'value':'0.00','currency':'INR'}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6028981762035&amp;cd[value]=0.00&amp;cd[currency]=INR&amp;noscript=1" /></noscript>
<style>
.form-group {
    margin-bottom:5px;
}

.loading {
	width: 100%;
	height: 100%;
	z-index: 999;
	position: absolute;
	top: 0px;
	left: 0px;
	opacity: 0.3;
	background: none repeat scroll 0% 0% #939393;
}

.loading-container .loading-content {
	background: none repeat scroll 0% 0% #FFF;
	border: 5px solid #AAA;
	left: 40%;
	top: 50px;
	position: absolute;
	text-align: center;
	vertical-align: middle;
	z-index: 999;
	padding: 10px 50px;
}

.loading-container .loading-content img {
	vertical-align: middle;
}

.loading-container .loading-content {
	text-align: center;
}
</style>
<header>
 <nav class="navbar navbar-inverse navbar-fixed-top bgnav">
  <div class="container-fluid">
    <div class="navbar-header" style="margin-left: 37px;">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>      </button>
    <a class="navbar-brand" href="https://pay1.in/partners" target="_blank"><img src="img/pay1_logo.svg" width="110" border="0" alt="" class=""></a></div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
<!--       	<li><a href="#registration_step2" role="button" data-toggle="modal">Step2</a></li> -->
<!--       	<li><a href="#registration_step3" role="button" data-toggle="modal">Step3</a></li> -->
<!--       	<li><a href="#registration_step4" role="button" data-toggle="modal">Step4</a></li> -->
         <li class="homebtn"><h4><a class="btn-home-rev" href="https://pay1.in" target="blank" role="button">Home</a></h4></li>
	  	 <li class="dloadpay1"><h4><a  class="btn-main"  href="https://pay1.in/contact" role="button"  style="text-decoration: none">Contact Us</a></h4></li>

     
      </ul>
    </div>
  </div>
</nav>
	</header>
	<div class="modal fade" id="registration_step1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Pay1 Partnership Registration</h4>
      </div>
      <div class="modal-body">
      	<div id="registration_step1_loader" class="loading-container" style="display:none">
			<div class="loading"></div>
			<div class="loading-content" style="top:180px;">
				<img src="/img/ajax-loader-2.gif" alt="">
				<span class="text"></span>
			</div>
		</div>
        <form id="registration_step1_form">
        <div class="row form-group">
        	<div class="col-lg-3" style="margin-top: 17px;margin-left: 15px;font-size: 12px;">I want to become a</div>
        	<div class="col-lg-8 input-group input-group-sm">
        		<select id="interest" name="interest" class="form-control input-sm">
        			<option value="">Select an option</option>
	            	<option value="Distributor">Channel Partner (Distributor)</option>
	            	<option value="Retailer">Business Partner (Retailer)</option>
	            </select>
        	</div>
        </div>
        <div class="form-group row">
        	<div class="col-lg-11">
        		<input type="text" id="name" name="name" onkeypress="return alphaOnly(event)" class="form-control input-sm" placeholder="Your full name" />
        	</div>	
        </div>
        <div class="form-group row">
        	<div class="col-lg-11">
        		<input type="text" id="shop_name" name="shop_name" class="form-control input-sm" placeholder="Shop Name" />
        	</div>	
        </div>
        <div class="form-group row">
        	<div class="col-lg-6">
        		<input type="text" id="email" name="email" class="form-control input-sm" placeholder="Email" />
        	</div>	
        	<div class="col-lg-5">
        		<input type="text" id="mobile" name="mobile" maxlength="10" onkeypress="return isNumberKey(event)" class="form-control input-sm" placeholder="Mobile" />
        	</div>
        </div>
        <div class="form-group row">
        	<div class="col-lg-11">
        		<input type="text" id="area" name="area" class="form-control input-sm" placeholder="Area" />
        	</div>	
        </div>
        <div class="form-group row">
        	<div class="col-lg-6">
        		<input type="text" id="city" name="city" onkeypress="return alphaOnly(event)" class="form-control input-sm" placeholder="City" />
        	</div>	
        	<div class="col-lg-5">
        		<input type="text" id="state" name="state" onkeypress="return alphaOnly(event)" class="form-control input-sm" placeholder="State" />
        	</div>
        </div>
        <div class="form-group row">
        	<div class="col-lg-6">
        		<input type="text" id="pin_code" name="pin_code" maxlength="6" onkeypress="return isNumberKey(event)" class="form-control input-sm" placeholder="Pin code" />
        	</div>
        </div>
         <div class="form-group row">
        	<div class="col-lg-11">
        		<textarea id="message" name="message" class="form-control" placeholder="Message" ></textarea>
        	</div>	
        </div>	
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onClick="create_lead()" >Continue</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="registration_step2">
  <div class="modal-dialog" style="width:400px">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Please choose a payment option</h4>
      </div>
      <div class="modal-body">
        <div id="registration_step2_loader" class="loading-container" style="display:none">
			<div class="loading"></div>
			<div class="loading-content" style="top: 100px; left: 80px;">
				<img src="/img/ajax-loader-2.gif" alt="">
				<span class="text"></span>
			</div>
		</div>
		<form id="registration_step2_form">
			<div class="row form-group">
        		<div class="col-lg-11" style="margin-top: 17px;margin-left: 15px;font-size: 12px;">I want to take online limit</div>
	        </div>
        	<div class="form-group row">
	        	<div class="col-lg-11">
	        		<div class="col-lg-8 input-group">
		        		<div class="radio" style="margin-left:45px;">
						  <input type="radio" name="payment_option" value="Netbanking" ><label style="margin-top: 3px;padding-left:0px;">Netbanking</label>
						</div>
						<div class="radio" style="margin-left:45px;">
						  <input type="radio" name="payment_option" value="Deposit" ><label style="margin-top: 3px;padding-left:0px;">Bank / Cash Deposit</label>
						</div>
		        	</div>
	        	</div>	
	        </div>
	        <div class="form-group row" id="r_u_d_div">
	        	<div class="col-lg-11" style="margin-left:13px;">
	        		<input type="checkbox" value="" id="retailer_under_distributor"><label style="padding-left: 10px;margin-bottom: 6px;font-size: 12px;" class="checkbox-inline">Are you also looking for nearby distributors?</label>
	        	</div>	
	        </div>
	    </form>    
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onClick="setPaymentOption()">Continue</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!--<div class="modal fade" id="registration_block">
    <div class="modal-dialog" style="width:400px">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">System Update</h4>
      </div>
      <div class="modal-body">
    <h5>Due to Server Maintenance you cannot sign up at the moment. We will get back to you soon.
Sorry for the inconvenience caused.<br>
-Pay1</h5>
      </div>
        </div>
        </div>
</div>-->
<div class="modal fade" id="registration_step3">
  <div class="modal-dialog" style="width:300px">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Verify Registration</h4>
      </div>
      <div class="modal-body">
      	<div id="registration_step3_loader" class="loading-container" style="display:none">
			<div class="loading"></div>
			<div class="loading-content" style="top: 100px; left: 80px;">
				<img src="/img/ajax-loader-2.gif" alt="">
				<span class="text"></span>
			</div>
		</div>
        <form id="registration_step3_form">
        	<div class="form-group row">
	        	<div class="col-lg-11" style="margin-left: 10px;">
	        		An OTP has been sent to mobile number <span id="mobile_number" style="font-weight: bold;"></span>. Kindly, verify.
	        	</div>	
	        </div>
        	<div class="form-group row">
	        	<div class="col-lg-11">
	        		<input type="text" id="otp" class="form-control input-sm" placeholder="One Time Password" />
	        	</div>	
	        </div>
	        <div class="form-group row">
	        	<div class="col-lg-11">
	        		<a href="javascript:sendOTP()" style="margin-left:10px">Send OTP again</a>
	        	</div>	
	        </div>
	        <div class="form-group row">
	        	<div class="col-lg-11">
	        		<input type="password" id="pin" class="form-control input-sm" placeholder="Create Pin" />
	        	</div>	
	        </div>
	        <div class="form-group row">
	        	<div class="col-lg-11">
	        		<input type="password" id="confirm_pin" class="form-control input-sm" placeholder="Confirm Pin" />
	        	</div>	
	        </div>
	        <div class="form-group row">
	        	<div class="col-lg-11" style="margin-left: 10px;" id="OTA_message">
	        		
	        	</div>	
	        </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onClick="verifyLead();">Confirm</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="registration_step4">
  <div class="modal-dialog" style="width:700px">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Bank/Cash Deposit</h4>
      </div>
      <div class="modal-body">
        <div class="row form-group">
        	<div class="col-lg-11" style="margin-top: 17px;margin-left: 15px;font-size: 12px;">
        		You can transfer funds to following Bank Accounts via NEFT/RTGS/Cash
        	</div>
        </div>
        <div class="form-group row">
        	<div class="col-lg-11" style="margin-left: 10px;margin-top:10px;font-size:12px;">
        		<table class="table table-hover table-bordered">
				    <thead>
				      <tr>
				        <th>Bank</th>
				        <th>Details</th>
				        <th>Account Name</th>
				        <th>Account No.</th>
				        <th>Account type</th>
				        <th>IFSC</th>
				        <th>Branch</th>
				      </tr>
				    </thead>
				    <tbody>
				      <tr>
				        <td>ICICI Bank</td>
				        <td>Cash Deposit, NEFT & IMPS</td>
				        <td>Mindsarray Technologies Pvt. Ltd.</td>
				        <td>054405006714</td>
				        <td>Current</td>
				        <td>ICICI0000544</td>
				        <td>MIDC</td>
				      </tr>
				      <tr>
				        <td>Bank of Maharashtra</td>
				        <td>Cash Deposit, NEFT & IMPS</td>
				        <td>Mindsarray Technologies Pvt. Ltd.</td>
				        <td>60117424079</td>
				        <td>Current</td>
				        <td>MAHB0000117</td>
				        <td>Malad (W)</td>
				      </tr>
				      <tr>
				        <td>State Bank of India</td>
				        <td>Cash Deposit, NEFT & IMPS</td>
				        <td>Pay1</td>
				        <td>33619466476</td>
				        <td>Current</td>
				        <td>SBIN0006055</td>
				        <td>Gokuldham (Goregaon - E)</td>
				      </tr>
				    </tbody>
				  </table>
        	</div>	
        </div>
        <div class="row form-group">
        	<div class="col-lg-11" style="margin-top: 10px;margin-left: 15px;font-size: 12px;">
        		The minimum deposit amount for limit balance is Rs. 1000/-
        	</div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onClick="goToShop()">Login Now</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="container-fluid whitebg">
	<div class="rowcustom">
    
    <div class="col-md-8 col-sm-8 col-xs-12">
    <div class="main_toptxt">
    <div><span class="main_txt">India's first and only Cash-To-Digital retail network<br/>is looking for dynamic partners to expand nationwide!</span><br/>
    <ul class="">
     <span class="you_txt"><li>India's fastest growing recharge channel</li></span>
     <span class="you_txt"><li>Most error - free and reliable recharge technology platform</li></span>
     <span class="you_txt"><li>Multiple income sources from single business</li> </span>
     <span class="you_txt"><li>Control your entire business from the palm of your hand with the Pay1 dealer mobile App</li></span>
    </ul>
    
    </div>
    <h3 class="knowmore"><a  class="btn-main"  href="#registration_step1" role="button" data-toggle="modal" style="text-decoration: none">Partner With Us</a></h3>
    </div>
    </div>
    <div class="col-md-4 col-sm-4 col-xs-12 imgtop"><img src="img/pay1_mobile_home.png" class="img-responsive"></div>
  </div>
</div>



<div class="container-fluid stat_bg">
<div class="row"><h1 align="center" class="how_title">Why Pay1?</h1></div>
<div class="row row80">
<div class="col-md-3 col-sm-3 col-xs-12 stat_in_rg">23 Million <br/><span class="reg_txt">Over 23 million customers <br/>  served so far</span></div>
<div class="col-md-3 col-sm-3 col-xs-12 stat_in_daily">5 Million <br/><span class="reg_txt">5 million monthly Walk In Customers at retail stores
</span></div>
<div class="col-md-3 col-sm-3 col-xs-12 stat_in_shipments">70,000 Retailers<br/><span class="reg_txt">500 channel partners and <br/> 70,000 retailer</span></div>
<div class="col-md-3 col-sm-3 col-xs-12 stat_in_new">20+ Cities<br/><span class="reg_txt">Presence over 20+ Cities <br/> across country</span></div>

</div>
</div>

<!-- start pay1 merchant video -->

<div class="container-fluid">
<div class="row rowvid">

        <div class="col-sm-6">
     <!--     <div class="panel-default">-->
     
            <div class="panel-body">
              <div class="embed-responsive embed-responsive-16by9">
  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/tooidZtEOhk" frameborder="0" allowfullscreen></iframe>
</div>
            </div>
          </div>
          
        <div class="col-sm-6">
         <!--   <div class="panel panel-default">-->
            <div class="panel-body">
              <h2 class="panel-title2">Merchant App for Retail Partners of Pay1</h2>
            </div>
            <div class="panel-body">
            <h3>Download now </h3>
              <div class="download-btn">
              <!-- Indicates a successful or positive action -->
              <a href="https://goo.gl/0wgz5j" target="_blank">
<button type="button" class="btn btn-success btnspace">Android</button></a>

<!-- Contextual button for informational alert messages -->
<a href="https://goo.gl/emVbfv" target="_blank">
<button type="button" class="btn btn-info btnspace">Windows</button></a>
			</div>
            </div>
          </div>
        </div><!-- /.col-sm-4 -->
        
      </div>
      </div>

<!-- end pay1 merchant video -->

<div class="container-fluid testimonials">
<h1 align="center">Testimonials</h1>


<div class="cd-testimonials-wrapper cd-container">
<h4 class="test_tit">Hear what our Channel Partners have to say:</h4><br/><br/><br/>
	<ul class="cd-testimonials">
		<li>
			
             <div class="row">
             <div class="col-lg-2"><img src="img/5.png" alt="Author image">
                        <div class="thum_space"><span class="testimonials-author-name">Sunil Chauhan</span></div>
						 <div class="thum_space"><span class="testimonials-author-name">Distributor</span></div>
                        <div class="thum_space"><span class="testimonials-author-name">Jabalpur</span></div>
                 </div>
                <div class="col-lg-10 testimonial_text">The journey with Pay1 till now has been amazing. Because Pay1 solved so many existing problems in the recharge business with technology, it has helped to grow more sales, reach more retailers and find more customers across my city.</div>
                 
            </div>
		</li>

		<li>
		 <div class="row">
             <div class="col-lg-2"><img src="img/2.png" alt="Author image">
					<div class="thum_space"><span class="testimonials-author-name">Manoj Verma</span></div>	
					<div class="thum_space"><span class="testimonials-author-name">Distributor</span></div>
                     <div class="thum_space"><span class="testimonials-author-name">Ahmedabad</span></div>		
                 </div>
                <div class="col-lg-10 testimonial_text">Pay1 has helped immensely in addressing the market needs and creating a mutually beneficial win-win relationship with retailers and customers. I stared with recharge and now I have so many income sources thanks to Pay1's expanding business channels!</div>
                 
            </div>
		</li>

		<li>
   <div class="row">
             <div class="col-lg-2"><img src="img/3.png" alt="Author image">
				
					<div class="thum_space"><span class="testimonials-author-name">Aslam Shaikh</span></div>
					<div class="thum_space"><span class="testimonials-author-name">Distributor</span></div>
                     <div class="thum_space"><span class="testimonials-author-name">Bangalore</span></div>
                 </div>
                <div class="col-lg-10 testimonial_text">Working with Pay1 has been very exciting. They were very helpful and supportive  </br>on  day-to-day basis. Missed Call recharging feature is </br>very popular with my customers.</div>
                 
            </div> 
            
		</li>
        <li>
	
            
               <div class="row">
             <div class="col-lg-2"><img src="img/4.png" alt="Author image">				
					<div class="thum_space"><span class="testimonials-author-name">Mayur Desai</span></div>
					<div class="thum_space"><span class="testimonials-author-name">Distributor</span></div>
                     <div class="thum_space"><span class="testimonials-author-name">Mumbai</span></div>	
                 </div>
                <div class="col-lg-10 testimonial_text">This was my first time in business. Luckily for me, Pay 1 had a dedicated Mobile Mentor team that guided me in the early days till I learned my ropes and my business become steady.</div>
                 
            </div> 
            
		</li>
	</ul> <!-- cd-testimonials -->

	


	
</div>

</div>


<footer class="" > 

<div class="footer_img"> 

   <span class="contact_title" id="contact">Contact Us</span>
  
   <div class="col-md-6 col-sm-6 col-xs-12">
       <span class="footer_text">726, Raheja's Metroplex (IJMIMA),<br/>
        Link Road, Malad (W), Mumbai - 400064.</span>
    </div>
    
     <div class="col-md-6 col-sm-6 col-xs-12">
     <span class="footer_text"><span class="glyphicon glyphicon-earphone">&nbsp;</span>022 67242238 <br/><span class="glyphicon glyphicon-envelope">&nbsp;</span><a href="mailto:info@pay1.in" style="color:white;text-decoration:none">info@pay1.in</a></span>
     </div>

<div class="col-xs-12"><hr class="hrsty"></div>

 <div class="row social_top">
 <ul class="social">
<li><a href="https://twitter.com/pay1app" class="btn btn-social-icon btn-twitter" target="_blank" name="contact"><i class="fa fa-twitter"></i></a></li>
<li><a href="https://www.facebook.com/pay1app?_rdr" class="btn btn-social-icon btn-facebook" target="_blank"><i class="fa fa-facebook"></i></a>
</li>
<li><a href="https://plus.google.com/+Pay1Inapp" class="btn btn-social-icon btn-google-plus" target="_blank"><i class="fa fa-google-plus"></i></a></li>
<li><a href="https://www.pinterest.com/pay1app" class="btn btn-social-icon btn-pinterest" target="_blank"><i class="fa fa-pinterest"></i></a></li>
<li><a href="https://instagram.com/pay1app" class="btn btn-social-icon btn-instagram" target="_blank"><i class="fa fa-instagram"></i></a></li>
	</ul>

    </div>
   &copy;  Mindsarray Technologies Pvt. Ltd. All rights reserved. 2015.
    
    </footer>
  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    
     <!-- cd-testimonials-all -->
        <script src="js/jquery-2.1.1.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/masonry.pkgd.min.js"></script>
        <script src="js/jquery.flexslider-min.js"></script>
        <script src="js/main.js"></script> <!-- Resource jQuery -->
        <script src="js/d_u_i.js"></script>
        
		<script>
		$(document).ready(function(){ 
			$('#registration_step1').modal('show');

			$('#registration_step1').on('shown.bs.modal', function() {
				$('#registration_step1_form').find("input, textarea").val("");
				$('#registration_step1_loader').hide();
			})

			$('#registration_step3').on('shown.bs.modal', function() {
				$('#registration_step3_form').find("input, textarea").val("");
				$('#registration_step3_loader').hide();
			})
		});	
		
                var apisURL = "https://panel.pay1.in/";
		var mobile = "";
		var pin = "";
		var is_retailer = true;
		var interest = "";
		var payment_option = "";
		var r_u_d = "";
		var ota = "";
		
		function create_lead(){
			interest = $('#interest').val();
			var name = $('#name').val();
			var shop_name = $('#shop_name').val();
			var email = $('#email').val();
			mobile = $('#mobile').val();
			var area = $('#area').val();
			var city = $('#city').val();
			var state = $('#state').val();
			var pin_code = $('#pin_code').val();
			var message = $('#message').val();

			if(interest == "" || name == "" || shop_name == "" || email == "" || area == "" || 
					city == "" || state == ""){
				alert("Field cannot be left blank");
				return false;
			}	
			if(!validateEmail(email)){
				alert("Type a valid email");
				return false;
			}	
			if(!validateMobile(mobile)){
				alert("Type a valid mobile number");
				return false;
			}
			if(!validatePincode(pin_code)){
				alert("Type a valid pin code");
				return false;
			}
			if(interest == "Distributor"){
				is_retailer = confirm("Do you want to start with a trial retailer account?");
			}	
			var data = "reg_i=" + interest + "&r_n=" + name + "&r_sn=" + shop_name + "&r_e=" + email + "&r_m=" + mobile + "&r_a=" + area + "&r_c=" + city +
						"&r_s=" + state + "&r_p=" + pin_code + "&c=" + message + "&req_by=pay1.in.partners" + "&ref=<?php if(isset($_GET['ref'])) echo "".$_GET['ref'] ?>";
			var url = apisURL+"apis/receiveWeb/mindsarray/mindsarray/json?method=createRetailerLeads&root=step1_response";
			$('#registration_step1_loader').show();

			$.post(url, data, "", "jsonp");
		}	

		function step1_response(response){
			jsonResponse = response[0];
			$('#registration_step1_loader').hide();
			if(jsonResponse['status'] == "success"){
				if(interest == "Distributor")
					$('#r_u_d_div').hide();
				else
					$('#r_u_d_div').show();
				$('#registration_step1').modal('hide');
				if(!is_retailer){
					alert("We have forwarded your distributorship request. Our team will contact you soon.");
					return false;
				}	
				$('#registration_step2').modal('show');
//                                $('#registration_step1').modal('hide');
//                                $('#registration_block').modal('show');
			}	
			else {
//                            $('#registration_step1').modal('hide');
//                            $('#registration_block').modal('show');
				alert(jsonResponse['description']);
			}
		}

		function setPaymentOption(){
			payment_option = $('input[name=payment_option]:checked').val();
			r_u_d = $('#retailer_under_distributor').is(":checked");
			if(r_u_d == false)
				r_u_d = "";
			if(payment_option == ""){
				alert("Choose a payment option");
				return false;
			}	
			sendOTP();
			$('#registration_step2').modal('hide');
			$('#registration_step3').modal('show');
		}
			
		function sendOTP(){
			var data = "r_m=" + mobile;
			var url = apisURL+"apis/receiveWeb/mindsarray/mindsarray/json?method=sendOTPToRetailer&root=otp_response";
			$('#mobile_number').html(mobile);
			$('#registration_step3_loader').show();
			$.post(url, data, "", "jsonp");
		}	

		function otp_response(response){
			jsonResponse = response[0];
			if(jsonResponse['status'] == "success"){
				ota = jsonResponse['OTA_Fee'];
// 				$('#OTA_message').html("<span style='color:#9C4F0E'>* A one time activation fee of Rs. " + jsonResponse['OTA_Fee'] + " will be charged initially.</span>");
				alert("OTP has been sent to mobile number " + mobile);
			}	
			else {
				alert(jsonResponse['description']);
			}
			$('#registration_step3_loader').hide();
		}

		function verifyLead(){
			var otp = $('#otp').val();
			pin = $('#pin').val();
			var confirm_pin = $('#confirm_pin').val();

			if(otp == "" || pin == "" || confirm_pin == ""){
				alert("All fields are necessary");
				return false;
			}	
			if(pin != confirm_pin){
				alert("Pins do not match");
				return false;
			}	
			$('#registration_step3_loader').show();

			var url = apisURL+"apis/receiveWeb/mindsarray/mindsarray/json?method=verifyRetailerLead&root=verification_response";
			var data = "r_m=" + mobile + "&otp=" + otp + "&pin=" + pin + "&r_u_d=" + r_u_d;
			
			$.post(url, data, "", "jsonp");
		}

		function verification_response(response){
			jsonResponse = response[0];
			if(jsonResponse['status'] == "success"){
				$('#registration_step3').modal('hide');
				alert("Congratulations! You are a retailer with Pay1");
				
				ga('send','event','contact_Form','submitform','contact-form-complete');
				
				login();
			}	
			else {
				alert(jsonResponse['description']);
			}
			$('#registration_step3_loader').hide();
		}

		function login(){
                        var div_n =  new Fingerprint().get();
			var url = apisURL+"apis/receiveWeb/mindsarray/mindsarray/json?root=login_response";//"http://panel.pay1.in/users/afterLogin";
			var data =  {
                method:'authenticate_new',   
                uuid:div_n,
                mobile:mobile,
                password:pin,
                type:'a_auth',
                gcm_reg_id:div_n,
                device_type:'web'
            };
// 			var data = {'mobile':mobile, 'password':pin, 'param':"retail"};
			
			$.post(url, data, "", "jsonp");
		}	

		function login_response(response){
			jsonResponse = response[0];
			if(jsonResponse['status'] == "success"){
				var description = jsonResponse['description'];
				var cookie_value = description['CAKEPHP'];

				var lat =  description['latitude'];
                var lng =  description['longitude'];
                var profile_details = mobile+"|<>|"+lat+"|<>|"+lng;
                clear_cookies();
                
                set_cookie("profileDetails", profile_details, 1)
                set_cookie("shopname", description['shopname'], 1);                                                                
                set_cookie("street", description['address'], 1);
                set_cookie("city", description['city_name'], 1);
                set_cookie("pincode", description['pin'], 1);
                set_cookie("state", description['state_name'], 1);
                set_cookie("area", description['area_name'], 1);
                set_cookie("area_id", description['area_id'], 1);
                set_cookie("balance", description['balance'], 1);
                set_cookie("flag", jsonResponse['pg_flag'], 1);
                set_cookie("ota_fee", ota, 1);
                
                set_cookie("mobile", mobile, 1);
                set_cookie("pinStatus", jsonResponse['passFlag'], 1);

                if(payment_option == "Netbanking")
                	window.location.href = "https://shop.pay1.in/online";
                else
                	$('#registration_step4').modal('show');
			}	
			else {
				alert('Invalid login');
			}	
		}

		function goToShop(){
			window.location.href = "https://shop.pay1.in";
		}

		function isNumberKey(evt) {
		    var charCode = (evt.which) ? evt.which : event.keyCode;
		    if (charCode != 46 && charCode > 31
		    && (charCode < 48 || charCode > 57))
		        return false;

		    return true;
		}

		function alphaOnly(evt) {
			var charCode = (evt.which) ? evt.which : event.keyCode;
		  	return ((charCode >= 65 && charCode <= 90) || charCode == 8 || (charCode >= 97 && charCode <= 122));
		}
				
		function validateEmail(email) {
		    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
		    return re.test(email);
		}

	    function validateMobile(number){
	      var phoneno = /^\d{10}$/;  
	      return number.match(phoneno) && (number > 7000000000);
	    }  

	    function validatePincode(number){
		      var pin_code = /^\d{6}$/;  
		      return number.match(pin_code);
		} 
		
	   	function set_cookie(name, value, days){
	   		var date, expires;
			if (days){
				date = new Date();
				date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
				expires = '; expires=' + date.toGMTString();
			}
			else {
				expires = ';expires=Thu, 01-Jan-1970 00:00:01 GMT';
			}
			var cookieDomain = window.location.host;
	        document.cookie = name + '=' + value + expires + ';domain='+cookieDomain+';path=' + "/";
	   	}  

	    function clear_cookies(){
            var cookies = document.cookie.split(";");
            for (var i = 0; i < cookies.length; i++) {
                var cookie = cookies[i];
                var eqPos = cookie.indexOf("=");
                var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
                set_cookie(name, '', -1);
            }   	
		} 	
		</script>
        
</body>
</html>
