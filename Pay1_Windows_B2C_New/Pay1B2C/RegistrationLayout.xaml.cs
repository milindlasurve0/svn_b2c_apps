﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Windows.Storage;
using Windows.ApplicationModel;
using System.Threading.Tasks;
using SQLite;
using Pay1B2C.Model;
using Pay1B2C.ViewModel;
using System.Collections.ObjectModel;
using Microsoft.Phone.Shell;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Device.Location;
using Microsoft.Phone.Notification;

namespace Pay1B2C
    {
    public partial class RegistrationLayout: PhoneApplicationPage
        {
        ObservableCollection<Contacts> DB_ContactList = new ObservableCollection<Contacts>();
        ProgressBarManager ProgressBarManager;
        GeoCoordinateWatcher watcher;
        SQLiteConnection db = null;
        public RegistrationLayout()
            {
            InitializeComponent();
            GoogleAnalytics.EasyTracker.GetTracker().SendView("Registration Layout");
            ProgressIndicator prog = new ProgressIndicator();
            ProgressBarManager = new ProgressBarManager();
            SystemTray.SetProgressIndicator(this, prog);
            ProgressBarManager.Hook(prog);
            Constant.getCurrentLatLong();
            getAllNumbers();
            getPushUrl();

            if(watcher == null)
                {
                watcher = new GeoCoordinateWatcher(GeoPositionAccuracy.High); // Use high accuracy. 
                watcher.MovementThreshold = 20; // Use MovementThreshold to ignore noise in the signal. 
                watcher.StatusChanged += new EventHandler<GeoPositionStatusChangedEventArgs>(watcher_StatusChanged);
                if(watcher.Permission == GeoPositionPermission.Denied)
                    {
                    //Location services is disable on the phone. Show a message to the user.
                    MessageBox.Show("Your location is not enabled.");

                    }
                else
                    {
                    watcher.Start();
                    }
                }


            MisscallDataHelper missCall=new MisscallDataHelper();
            missCall.DeleteAllMissCall();


            DateTime currentDateOp = DateTime.Now;//.ToString();

            DateTime updatedDateOp = Constant.LoadPersistent<DateTime>(Constant.OPERATOR_LIST_UPDATE_TIME);
            TimeSpan durationOp = currentDateOp - updatedDateOp;
            if(durationOp.Days >= 4)
                {
                //string strDel = "DELETE FROM OperatorTable";
                //(Application.Current as App).dbh.Delete<NewFreeBieAllData>(strDel);

                GetAndSaveOperator();
                }


            }



        string channelName = "Pay1B2cChannel";

        HttpNotificationChannel pushChannel;
        public static string MPNSString;
        public static Collection<Uri> allowedDomains =
		  new Collection<Uri> { 
                                                new Uri("http://b2c.pay1.in"), 
                                                new Uri("http://cdev.pay1.in") 
                                              };

        private void getPushUrl()
            {
            try
                {
                pushChannel = HttpNotificationChannel.Find(channelName);

                if(pushChannel == null)
                    {
                    pushChannel = new HttpNotificationChannel(channelName);

                    pushChannel.ChannelUriUpdated += new EventHandler<NotificationChannelUriEventArgs>(PushChannel_ChannelUriUpdated);
                    pushChannel.ErrorOccurred += new EventHandler<NotificationChannelErrorEventArgs>(PushChannel_ErrorOccurred);
                    pushChannel.ShellToastNotificationReceived += new EventHandler<NotificationEventArgs>(PushChannel_ShellToastNotificationReceived);
                    pushChannel.Open();



                    //pushChannel.BindToShellTile();

                    pushChannel.BindToShellToast();
                    //pushChannel.BindToShellTile(allowedDomains);

                    }
                else
                    {
                    pushChannel.ChannelUriUpdated += new EventHandler<NotificationChannelUriEventArgs>(PushChannel_ChannelUriUpdated);
                    pushChannel.ErrorOccurred += new EventHandler<NotificationChannelErrorEventArgs>(PushChannel_ErrorOccurred);

                    pushChannel.ShellToastNotificationReceived += new EventHandler<NotificationEventArgs>(PushChannel_ShellToastNotificationReceived);

                    pushChannel.BindToShellTile(allowedDomains);
                    System.Diagnostics.Debug.WriteLine(pushChannel.ChannelUri.ToString());
                    /* MessageBox.Show(String.Format("Channel Uri is {0}",
                         pushChannel.ChannelUri.ToString()));*/
                    pushChannel.BindToShellToast();
                    MPNSString = pushChannel.ChannelUri.ToString();
                    Constant.SavePersistent(Constant.NOTIFICATION_URL, MPNSString);
                    // MessageBox.Show(MPNSString);

                    }
                }
            catch(Exception e)
                {
                //Splash_Screen();
                //Console.WriteLine("dfghf");
                }
            }



        void PushChannel_ChannelUriUpdated(object sender, NotificationChannelUriEventArgs e)
            {
            Dispatcher.BeginInvoke(() =>
            {
                System.Diagnostics.Debug.WriteLine(e.ChannelUri.ToString());

                MPNSString = e.ChannelUri.ToString();
                Constant.SavePersistent(Constant.NOTIFICATION_URL, MPNSString);
                //MessageBox.Show(MPNSString);
            });
            }


        void PushChannel_ErrorOccurred(object sender, NotificationChannelErrorEventArgs e)
            {

            }

        void PushChannel_ShellToastNotificationReceived(object sender, NotificationEventArgs e)
            {
            if(e.Collection.ContainsKey("wp:Text1"))
                {
                if(e.Collection.ContainsKey("wp:Text2"))
                    {
                    
                        Dispatcher.BeginInvoke(() => MessageBox.Show(e.Collection["wp:Text1"] + "\n" + e.Collection["wp:Text2"]));
                       
                    }

                }
            else
                {
                }

            }





        private void watcher_StatusChanged(object sender, GeoPositionStatusChangedEventArgs e)
            {
            try
                {
                if(e.Status == GeoPositionStatus.Ready)
                    {
                    // Use the Position property of the GeoCoordinateWatcher object to get the current location. 
                    GeoCoordinate co = watcher.Position.Location;
                      String  latti = co.Latitude.ToString("0.00000000");
                      String longi = co.Longitude.ToString("0.00000000");
                    Constant.SavePersistent(Constant.SETTINGS_USER_LATTITUDE, latti);
                    Constant.SavePersistent(Constant.SETTINGS_USER_LONGITUDE, longi);

                    watcher.Stop();
                    }
                }
            catch(Exception exc)
                {
                }
            }

        void textMouseEnter(object sender, MouseEventArgs e)
            {

            NavigationService.Navigate(new Uri("/Views/TermsAndCondition.xaml?", UriKind.RelativeOrAbsolute));
            PhoneApplicationService.Current.State["isTermsAndCondition"] = "1";
            PhoneApplicationService.Current.State[Constant.IS_PROFILE] = true;
            }


        void textPrivacyMouseEnter(object sender, MouseEventArgs e)
            {
            NavigationService.Navigate(new Uri("/Views/TermsAndCondition.xaml?", UriKind.RelativeOrAbsolute));
            PhoneApplicationService.Current.State["isTermsAndCondition"] = "2";
            PhoneApplicationService.Current.State[Constant.IS_PROFILE] = true;
            }

       
        private void btnRegister_Click(object sender, RoutedEventArgs e)
            {
            if(textMobileNumber.Text.Length==10 && (textMobileNumber.Text.StartsWith("7")||textMobileNumber.Text.StartsWith("8")||textMobileNumber.Text.StartsWith("9")))
                {
                registerUser(textMobileNumber.Text);
                }
            else
                {
                MessageBox.Show("Please enter correct number");
                }

            }

        private async void registerUser(String number)
            {
            ProgressBarManager.Show(1, "Please wait...", false, 10);
            var values = new List<KeyValuePair<string, string>>
                        { 
                        new KeyValuePair<string, string>("actiontype", "create_user"),
                        new KeyValuePair<string, string>("mobile_number",number),
                       new KeyValuePair<string, string>("api","true"),
                       new KeyValuePair<string, string>("api_version",Constant.API_VERSION)
                        };

            FormUrlEncodedContent values1 = new FormUrlEncodedContent(values);
            Task<string> response = Constant.postParamsAndGetResponse(values1);
            string result = await response;
            if(string.IsNullOrEmpty(result))
                {
                NavigationService.Navigate(new Uri("/Views/NoConnection.xaml?", UriKind.Relative));
                }
            else
                {
                var dict = (JObject)JsonConvert.DeserializeObject(result);
                var status = dict["status"];

                if(status.ToString() == "success")
                    {
                    Constant.SavePersistent(Constant.SETTINGS_IS_NEW, true);
                    var description = dict["description"];
                    Constant.setOtpHash(description["otp"].ToString());
                    Constant.setUserNumber(number);
                    Constant.SavePersistent(Constant.SETTINGS_USER_PIN, Constant.LoadPersistent<String>(Constant.SETTINGS_USER_PIN));
                   // Constant.SavePersistent(Constant.SETTINGS_USER_GENDER, description["gender"].ToString());
                   // Constant.SavePersistent(Constant.SETTINGS_USER_DOB, description["date_of_birth"].ToString());
                    Constant.setLastTransaction(description["recent_pay1_transactions"].ToString());
                    Constant.setbalance(description["balance"].ToString());
                   // Constant.setUserName(description["name"].ToString());
                  //  Constant.setEmail(description["email"].ToString());
                    //Constant.SavePersistent("session_id", description["session_id"].ToString());
                    //Constant.SavePersistent("session_name", description["session_name"].ToString());
                    SaveOldTransactions(description["recent_pay1_transactions"].ToString());


                    try
                        {
                        var offer_likes=description["Deals_offer"]["offer_likes"];
                        string[] offer_likes_str=offer_likes.ToString().Split(',');
                        GiftDataHelper gdh=new GiftDataHelper();
                        foreach(string ids in offer_likes_str)
                            {
                            gdh.updateGiftForLikeByOfferId(Convert.ToInt16(ids.ToString()), 1);
                            }

                        }
                    catch(Exception exc)
                        {
                        }

                    NavigationService.Navigate(new Uri("/Views/VerifyPassword.xaml?", UriKind.RelativeOrAbsolute));
                    NavigationService.RemoveBackEntry();
                    }
                else
                    {
                    String errCode=dict["errCode"].ToString();
                    if(errCode.Equals("202"))
                        {
                        Constant.SavePersistent(Constant.SETTINGS_IS_NEW, false);
                        var description = dict["description"];
                        Constant.setOtpHash(description["otp"].ToString());
                        Constant.setUserNumber(number);
                        Constant.SavePersistent(Constant.SETTINGS_USER_PIN, Constant.LoadPersistent<String>(Constant.SETTINGS_USER_PIN));
                       // Constant.SavePersistent(Constant.SETTINGS_USER_GENDER, description["gender"].ToString());
                      //  Constant.SavePersistent(Constant.SETTINGS_USER_DOB, description["date_of_birth"].ToString());
                        Constant.setLastTransaction(description["recent_pay1_transactions"].ToString());
                        Constant.setbalance(description["balance"].ToString());
                       // Constant.setUserName(description["name"].ToString());
                       // Constant.setEmail(description["email"].ToString());
                        SaveOldTransactions(description["recent_pay1_transactions"].ToString());
                        //Constant.SavePersistent("session_id", description["session_id"].ToString());
                        //Constant.SavePersistent("session_name", description["session_name"].ToString());

                        try
                            {
                            var offer_likes=description["Deals_offer"]["offer_likes"];
                            string[] offer_likes_str=offer_likes.ToString().Split(',');
                            GiftDataHelper gdh=new GiftDataHelper();
                            foreach(string ids in offer_likes_str)
                                {
                                gdh.updateGiftForLikeByOfferId(Convert.ToInt16(ids.ToString()), 1);
                                }

                            }
                        catch(Exception exc)
                            {
                            }
                        NavigationService.Navigate(new Uri("/Views/VerifyPassword.xaml?", UriKind.RelativeOrAbsolute));
                        NavigationService.RemoveBackEntry();
                        }
                    else
                        {
                        MessageBox.Show("Something went wrong");
                        }
                    }
                }
            ProgressBarManager.Hide(1);
            }

        private async void getAllNumbers()
            {
            DateTime lastStorageCircle = Constant.LoadPersistent<DateTime>(Constant.CIRCLE_INSERT_TIME);
            // Use current time
            string format = "yyyy-MM-dd HH:mm:ss";    // Use this format
            Console.WriteLine(lastStorageCircle.ToString(format));
            String lastUrl = "https://panel.pay1.in/apis/receiveWeb/mindsarray/mindsarray/json?method=getMobileDetails&mobile=all&timestamp=" + lastStorageCircle.ToString(format);
            var httpClient = new HttpClient(new HttpClientHandler());
            HttpResponseMessage response = await httpClient.PostAsync(lastUrl, null);
            response.EnsureSuccessStatusCode();
            long cl = (long)response.Content.Headers.ContentLength;
            var responseString = await response.Content.ReadAsStringAsync();
            int count = 0;
            try
                {
                //db.Open ();


                string output = responseString.Remove(responseString.Length - 1, 1);
                string output1 = output.Remove(output.Length - 1, 1);
                string output2 = output1.Remove(0, 1);
                string output3 = output2.Remove(0, 1);
                string output4 = output3.Remove(output3.Length - 1, 1);

                var dict = (JObject)JsonConvert.DeserializeObject(output4);
                string status = dict["status"].ToString();
                int inseted = 0;
                var details = dict["details"];
                }
            catch(Exception ee)
                {
                }
            }


           private void getAllCircles(string allNumberDetails)
            {

            NumberAndCircle numberAndCircle = new NumberAndCircle();
            var dict = (JObject)JsonConvert.DeserializeObject(allNumberDetails);
            string status = dict["status"].ToString();
            int inseted = 0;
            var details = dict["details"];
           
            foreach(var item in details)
                {
                NumberCircleHelper numberHelper=new NumberCircleHelper();
                inseted = inseted + 1;
                numberAndCircle.area = item["area"].ToString();
                numberAndCircle.area_name = item["area_name"].ToString();
                numberAndCircle.number = item["number"].ToString();
                numberAndCircle.numberOperator = item["operator"].ToString();
                numberAndCircle.opr_name = item["opr_name"].ToString();
                numberAndCircle.product_id = item["product_id"].ToString();
                numberHelper.Insert(numberAndCircle);
                }
            
            Constant.SavePersistent(Constant.CIRCLE_INSERT_TIME, DateTime.Now);
            System.Diagnostics.Debug.WriteLine("Inserted  Circle  " + inseted);

            }



        private void SaveOldTransactions(string data)
            {
            try
                {
                JArray jObj=(JArray)JsonConvert.DeserializeObject(data);
                using(var db = new SQLiteConnection(App.PAY1_DB_PATH))
                    {
                    MisscallDataHelper misscallHelper=new MisscallDataHelper();
                    foreach(JObject jOject in jObj)
                        {
                        QuickPayAllDataTable quickPay1=new QuickPayAllDataTable();

                        quickPay1.id=jOject["id"].ToString();
                        quickPay1.flag=jOject["flag"].ToString();
                        quickPay1.amount=jOject["amount"].ToString();
                        quickPay1.stv=jOject["stv"].ToString();
                        quickPay1.operator_id=jOject["operator_id"].ToString();
                        quickPay1.product=jOject["product"].ToString();
                        quickPay1.number=jOject["number"].ToString();
                        quickPay1.delegate_flag=jOject["delegate_flag"].ToString();
                        quickPay1.missed_number=jOject["missed_number"].ToString();

                        quickPay1.confirmation=jOject["confirmation"].ToString();
                        quickPay1.users_id=jOject["users_id"].ToString();
                        quickPay1.name=jOject["name"].ToString();


                        quickPay1.operator_name=jOject["operator_name"].ToString();
                        quickPay1.transaction_flag=jOject["transaction_flag"].ToString();
                        quickPay1.operator_code=jOject["operator_code"].ToString();
                        quickPay1.datetime=jOject["datetime"].ToString();
                        try
                            {

                            misscallHelper.DeleteMisscallById(jOject["id"].ToString());

                            misscallHelper.Insert(quickPay1);
                            }
                        catch(SQLiteException swe)
                            {
                            }
                        catch(Exception es)
                            {
                            }
                        }

       
                   
                    }
                }
            catch(SQLiteException sqe)
                {
                }
            catch(Exception exc)
                {
                }
            }



        private async void GetAndSaveOperator()
            {
            //ProgressBarManager.Show(10, "Please wait...", false, 10);
            try
                {
                var values = new List<KeyValuePair<string, string>>
                        { 
                        new KeyValuePair<string, string>("actiontype", "get_all_operators"),
                        new KeyValuePair<string, string>("api","true"),
                        };

                FormUrlEncodedContent values1 = new FormUrlEncodedContent(values);
                Task<string> response = Constant.postParamsAndgetResponse(Constant.B2C_URL, values1);

                string result = await response;
                var dict = (JObject)JsonConvert.DeserializeObject(result);
                string status = dict["status"].ToString();
                if(status.Equals("success"))
                    {
                   
                    var description = dict["description"];
                    var mobile = description["mobile"];
                    foreach(var item in mobile)
                        {
                        OperatorDB opeDb = new OperatorDB();
                        opeDb.id = item["id"].ToString();
                        opeDb.code = item["opr_code"].ToString();
                        opeDb.delegateFlag = item["delegate"].ToString();
                        opeDb.flag = item["flag"].ToString();
                        opeDb.name = item["name"].ToString();
                        opeDb.p_id = item["product_id"].ToString();
                        opeDb.service_id = item["service_id"].ToString();
                        opeDb.stv = item["stv"].ToString();
                        opeDb.prefix = item["prefix"].ToString();
                        opeDb.length = item["length"].ToString();
                        opeDb.min = item["min"].ToString();
                        opeDb.max = item["max"].ToString();
                        opeDb.charges_slab = item["charges_slab"].ToString();
                        opeDb.service_charge_amount = item["service_charge_amount"].ToString();
                        opeDb.service_tax_percent = item["service_tax_percent"].ToString();
                        opeDb.service_charge_percent = item["service_charge_percent"].ToString();
                        insertOperatorToDb(opeDb);
                        }
                    var data = description["data"];
                    foreach(var item in data)
                        {
                        OperatorDB opeDb = new OperatorDB();
                        opeDb.id = item["id"].ToString();
                        opeDb.code = item["opr_code"].ToString();
                        opeDb.delegateFlag = item["delegate"].ToString();
                        opeDb.flag = item["flag"].ToString();
                        opeDb.name = item["name"].ToString();
                        opeDb.p_id = item["product_id"].ToString();
                        opeDb.service_id = item["service_id"].ToString();
                        opeDb.stv = item["stv"].ToString();
                        opeDb.prefix = item["prefix"].ToString();
                        opeDb.length = item["length"].ToString();
                        opeDb.min = item["min"].ToString();
                        opeDb.max = item["max"].ToString();
                        opeDb.charges_slab = item["charges_slab"].ToString();
                        opeDb.service_charge_amount = item["service_charge_amount"].ToString();
                        opeDb.service_tax_percent = item["service_tax_percent"].ToString();
                        opeDb.service_charge_percent = item["service_charge_percent"].ToString();
                        insertOperatorToDb(opeDb);
                        }
                    var postpaid = description["postpaid"];
                    foreach(var item in postpaid)
                        {
                        OperatorDB opeDb = new OperatorDB();
                        opeDb.id = item["id"].ToString();
                        opeDb.code = item["opr_code"].ToString();
                        opeDb.delegateFlag = item["delegate"].ToString();
                        opeDb.flag = item["flag"].ToString();
                        opeDb.name = item["name"].ToString();
                        opeDb.p_id = item["product_id"].ToString();
                        opeDb.service_id = item["service_id"].ToString();
                        opeDb.stv = item["stv"].ToString();
                        opeDb.prefix = item["prefix"].ToString();
                        opeDb.length = item["length"].ToString();
                        opeDb.min = item["min"].ToString();
                        opeDb.max = item["max"].ToString();
                        opeDb.charges_slab = item["charges_slab"].ToString();
                        opeDb.service_charge_amount = item["service_charge_amount"].ToString();
                        opeDb.service_tax_percent = item["service_tax_percent"].ToString();
                        opeDb.service_charge_percent = item["service_charge_percent"].ToString();
                        insertOperatorToDb(opeDb);
                        }
                    var dth = description["dth"];
                    foreach(var item in dth)
                        {
                        OperatorDB opeDb = new OperatorDB();
                        opeDb.id = item["id"].ToString();
                        opeDb.code = item["opr_code"].ToString();
                        opeDb.delegateFlag = item["delegate"].ToString();
                        opeDb.flag = item["flag"].ToString();
                        opeDb.name = item["name"].ToString();
                        opeDb.p_id = item["product_id"].ToString();
                        opeDb.service_id = item["service_id"].ToString();
                        opeDb.stv = item["stv"].ToString();
                        opeDb.prefix = item["prefix"].ToString();
                        opeDb.length = item["length"].ToString();
                        opeDb.min = item["min"].ToString();
                        opeDb.max = item["max"].ToString();
                        opeDb.charges_slab = item["charges_slab"].ToString();
                        opeDb.service_charge_amount = item["service_charge_amount"].ToString();
                        opeDb.service_tax_percent = item["service_tax_percent"].ToString();
                        opeDb.service_charge_percent = item["service_charge_percent"].ToString();
                        insertOperatorToDb(opeDb);
                        }

                    Constant.SavePersistent(Constant.OPERATOR_LIST_UPDATE_TIME, DateTime.Now);
                    }
                }
            catch(Exception e)
                {
                }
            // ProgressBarManager.Hide(10);
            }

        private void insertOperatorToDb(OperatorDB opeDb)
            {
          
            try
                {
                  OperatorHelper operatorHelper=new OperatorHelper();
                  operatorHelper.Insert(opeDb);
                }
            catch(SQLiteException sle)
                {
                }
            catch(Exception exc)
                {
                }
            }



        }
    }