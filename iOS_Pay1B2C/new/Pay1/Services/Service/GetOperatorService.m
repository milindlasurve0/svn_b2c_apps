//
//  GetOperatorService.m
//  Pay1
//
//  Created by Annapurna on 13/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "GetOperatorService.h"
#import "GetOperatorResponse.h"
#import "OperatorList.h"
#import "QuickPayResponse.h"
#import "QucickPayList.h"
#import "DBManager.h"
#import "EditQuickPayCriteria.h"
#import "Constant.h"
#import "Session.h"
@implementation GetOperatorService

-(void)callGetOperatorService:(getOeratorResponse)callback{
    
    RequestData *requestData = [[RequestData alloc]init];
    requestData.baseURL = kBaseURL;
    requestData.webServiceURL = kGetOperatorURL;
    requestData.showLoader = FALSE;
    
    requestData.requestSuccessBlock = ^(RequestData *request) {
        
    [self printResponse:request.responseData forRequest:@"GetOperator Response"];
    if ([self handleNullResponse:request])
    return;
        
    callback(nil, [self parseGetOperatorResponse:request]);
    };
    
    
    requestData.requestFailureBlock = ^(RequestData *request) {
    [self handleNetworkError:request.responseData];
    callback(request.responseData, nil);
    };
    
    [[ServiceManager sharedInstance] makeRequest:requestData];

    
}

-(GetOperatorResponse *)parseGetOperatorResponse:(RequestData *)request{
    
    DBManager *  objDBManager = [[DBManager alloc]initWithDatabaseFilename:@"operatorDB.sql"];
    NSString *query;
    
    query = [NSString stringWithFormat:@"delete from operator"];
    
    [objDBManager executeQuery:query];
    
    if (objDBManager.affectedRows != 0) {
        //  NSLog(@"Query was executed successfully.");
    }
    else{
        //  NSLog(@"Could not execute the query.");
    }
    
    GetOperatorResponse *operatorResponse = [[GetOperatorResponse alloc]init];
    operatorResponse.status = [request.responseData objectForKey:@"status"];
    operatorResponse.errorCode = [request.responseData objectForKey:@"errCode"];
    operatorResponse.discriptionOperator = [request.responseData objectForKey:@"description"];
    NSMutableArray *mobileArray = [operatorResponse.discriptionOperator valueForKey:@"mobile"];
    
    for(NSMutableDictionary *mobileDic in mobileArray){
    OperatorList *operatorList = [[OperatorList alloc]init];
    operatorList.operatorID = [mobileDic objectForKey:@"id"];
    operatorList.operatorCode = [mobileDic objectForKey:@"opr_code"];
    operatorList.productID = [mobileDic objectForKey:@"product_id"];
    operatorList.operatorName = [mobileDic objectForKey:@"name"];
    operatorList.serviceID = [mobileDic objectForKey:@"service_id"];
    operatorList.flag = [mobileDic objectForKey:@"flag"];
    operatorList.operatorDelegate = [mobileDic objectForKey:@"delegate"];
    operatorList.operatorSTV = [mobileDic objectForKey:@"stv"];
    operatorList.operatorMin = [mobileDic objectForKey:@"min"];
    operatorList.operatorMax = [mobileDic objectForKey:@"max"];
    operatorList.operatorChargeSlab = [mobileDic objectForKey:@"charges_slab"];
    operatorList.serviceChargeAmount = [mobileDic objectForKey:@"service_charge_amount"];
    operatorList.serviceTaxPercent = [mobileDic objectForKey:@"service_tax_percent"];
    operatorList.serviceChargePercent = [mobileDic objectForKey:@"service_charge_percent"];
    operatorList.operatorLength = [mobileDic objectForKey:@"length"];
    operatorList.operatorPrefix = [mobileDic objectForKey:@"prefix"];

        
    [operatorResponse.mobileArrayList addObject:operatorList];
    }
    
    NSMutableArray *dataArray = [operatorResponse.discriptionOperator valueForKey:@"data"];
    
    for(NSMutableDictionary *dataDic in dataArray) {
    OperatorList *operatorList = [[OperatorList alloc]init];

    operatorList.operatorID = [dataDic objectForKey:@"id"];
    operatorList.operatorCode = [dataDic objectForKey:@"opr_code"];
    operatorList.productID = [dataDic objectForKey:@"product_id"];
    operatorList.operatorName = [dataDic objectForKey:@"name"];
    operatorList.serviceID = [dataDic objectForKey:@"service_id"];
    operatorList.flag = [dataDic objectForKey:@"flag"];
    operatorList.operatorDelegate = [dataDic objectForKey:@"delegate"];
    operatorList.operatorSTV = [dataDic objectForKey:@"stv"];
    operatorList.operatorMin = [dataDic objectForKey:@"min"];
    operatorList.operatorMax = [dataDic objectForKey:@"max"];
    operatorList.operatorChargeSlab = [dataDic objectForKey:@"charges_slab"];
    operatorList.serviceChargeAmount = [dataDic objectForKey:@"service_charge_amount"];
    operatorList.serviceTaxPercent = [dataDic objectForKey:@"service_tax_percent"];
    operatorList.serviceChargePercent = [dataDic objectForKey:@"service_charge_percent"];
    operatorList.operatorLength = [dataDic objectForKey:@"length"];
    operatorList.operatorPrefix = [dataDic objectForKey:@"prefix"];
        
    [operatorResponse.dataCardArrayList addObject:operatorList];

    }
    
    
    
    NSMutableArray *postPaidArray = [operatorResponse.discriptionOperator valueForKey:@"postpaid"];
    
    for(NSMutableDictionary *postPaidDic in postPaidArray) {
    OperatorList *operatorList = [[OperatorList alloc]init];
        
    operatorList.operatorID = [postPaidDic objectForKey:@"id"];
    operatorList.operatorCode = [postPaidDic objectForKey:@"opr_code"];
    operatorList.productID = [postPaidDic objectForKey:@"product_id"];
    operatorList.operatorName = [postPaidDic objectForKey:@"name"];
    operatorList.serviceID = [postPaidDic objectForKey:@"service_id"];
    operatorList.flag = [postPaidDic objectForKey:@"flag"];
    operatorList.operatorDelegate = [postPaidDic objectForKey:@"delegate"];
    operatorList.operatorSTV = [postPaidDic objectForKey:@"stv"];
    operatorList.operatorMin = [postPaidDic objectForKey:@"min"];
    operatorList.operatorMax = [postPaidDic objectForKey:@"max"];
    operatorList.operatorChargeSlab = [postPaidDic objectForKey:@"charges_slab"];
    operatorList.serviceChargeAmount = [postPaidDic objectForKey:@"service_charge_amount"];
    operatorList.serviceTaxPercent = [postPaidDic objectForKey:@"service_tax_percent"];
    operatorList.serviceChargePercent = [postPaidDic objectForKey:@"service_charge_percent"];
    operatorList.operatorLength = [postPaidDic objectForKey:@"length"];
    operatorList.operatorPrefix = [postPaidDic objectForKey:@"prefix"];
        
    [operatorResponse.postPaidArrayList addObject:operatorList];
 
    }
    
    
    NSMutableArray *dthArray = [operatorResponse.discriptionOperator valueForKey:@"dth"];
    
    for(NSMutableDictionary *postPaidDic in dthArray) {
    OperatorList *operatorList = [[OperatorList alloc]init];
    operatorList.operatorID = [postPaidDic objectForKey:@"id"];
    operatorList.operatorCode = [postPaidDic objectForKey:@"opr_code"];
    operatorList.productID = [postPaidDic objectForKey:@"product_id"];
    operatorList.operatorName = [postPaidDic objectForKey:@"name"];
    operatorList.serviceID = [postPaidDic objectForKey:@"service_id"];
    operatorList.flag = [postPaidDic objectForKey:@"flag"];
    operatorList.operatorDelegate = [postPaidDic objectForKey:@"delegate"];
    operatorList.operatorSTV = [postPaidDic objectForKey:@"stv"];
    operatorList.operatorMin = [postPaidDic objectForKey:@"min"];
    operatorList.operatorMax = [postPaidDic objectForKey:@"max"];
    operatorList.operatorChargeSlab = [postPaidDic objectForKey:@"charges_slab"];
    operatorList.serviceChargeAmount = [postPaidDic objectForKey:@"service_charge_amount"];
    operatorList.serviceTaxPercent = [postPaidDic objectForKey:@"service_tax_percent"];
    operatorList.serviceChargePercent = [postPaidDic objectForKey:@"service_charge_percent"];
    operatorList.operatorLength = [postPaidDic objectForKey:@"length"];
    operatorList.operatorPrefix = [postPaidDic objectForKey:@"prefix"];
    [operatorResponse.dthArrayList addObject:operatorList];
        
    }
    
    [self saveMobileOperatorDataInSQLite:operatorResponse.mobileArrayList];
    [self savePostPaidOperatorDataInSQLite:operatorResponse.postPaidArrayList];
    [self saveDataOperatorDataInSQLite:operatorResponse.dataCardArrayList];
    [self saveDTHOperatorDataInSQLite:operatorResponse.dthArrayList];


    return operatorResponse;
}

#pragma mark - SQLite

-(void)saveMobileOperatorDataInSQLite:(NSMutableArray *)operatorArrayList{
    DBManager *  objDBManager = [[DBManager alloc]initWithDatabaseFilename:@"operatorDB.sql"];
    NSString *query;
    for(int i=0;i<operatorArrayList.count;i++){
    OperatorList *objOpList = [operatorArrayList objectAtIndex:i];
    query = [NSString stringWithFormat:@"insert into operator values(null,'%@','%d','%@','%d','%@','%d','%d','%d','%d','%d','%d','%d','%d','%@','%d','%d','%d','%d','%@')",@"Mobile",[objOpList.operatorID intValue],objOpList.operatorCode,[objOpList.productID intValue],objOpList.operatorName,[objOpList.serviceID intValue],[objOpList.flag intValue],[objOpList.operatorDelegate intValue],[objOpList.operatorSTV intValue],1,1,[objOpList.operatorMax intValue],[objOpList.operatorMin intValue],objOpList.operatorChargeSlab,[objOpList.serviceChargeAmount intValue],[objOpList.serviceTaxPercent intValue],[objOpList.serviceChargePercent intValue],[objOpList.operatorLength intValue],objOpList.operatorPrefix];
        
        //  NSLog(@"OperatorValues :%@ %@",objOpList.operatorName,objOpList.operatorChargeSlab);
        
        [objDBManager executeQuery:query];
        
    }
    
    //   NSLog(@"DBQuery is :%@",query);
    
    
    if (objDBManager.affectedRows != 0) {
       // NSLog(@"Query was executed successfully.");
    }
    else{
       // NSLog(@"Could not execute the query.");
    }
}

-(void)saveDataOperatorDataInSQLite:(NSMutableArray *)operatorArrayList{
    DBManager *  objDBManager = [[DBManager alloc]initWithDatabaseFilename:@"operatorDB.sql"];
    NSString *query;
    for(int i=0;i<operatorArrayList.count;i++){
    OperatorList *objOpList = [operatorArrayList objectAtIndex:i];
    query = [NSString stringWithFormat:@"insert into operator values(null,'%@','%d','%@','%d','%@','%d','%d','%d','%d','%d','%d','%d','%d','%@','%d','%d','%d','%d','%@')",@"DataCard",[objOpList.operatorID intValue],objOpList.operatorCode,[objOpList.productID intValue],objOpList.operatorName,[objOpList.serviceID intValue],[objOpList.flag intValue],[objOpList.operatorDelegate intValue],[objOpList.operatorSTV intValue],1,1,[objOpList.operatorMax intValue],[objOpList.operatorMin intValue],objOpList.operatorChargeSlab,[objOpList.serviceChargeAmount intValue],[objOpList.serviceTaxPercent intValue],[objOpList.serviceChargePercent intValue],[objOpList.operatorLength intValue],objOpList.operatorPrefix];
        [objDBManager executeQuery:query];
        
    }
    
    if (objDBManager.affectedRows != 0) {
       // NSLog(@"Query was executed successfully.");
    }
    else{
       // NSLog(@"Could not execute the query.");
    }
}

-(void)savePostPaidOperatorDataInSQLite:(NSMutableArray *)operatorArrayList{
    DBManager *  objDBManager = [[DBManager alloc]initWithDatabaseFilename:@"operatorDB.sql"];
    NSString *query;
    for(int i=0;i<operatorArrayList.count;i++){
    OperatorList *objOpList = [operatorArrayList objectAtIndex:i];
    query = [NSString stringWithFormat:@"insert into operator values(null,'%@','%d','%@','%d','%@','%d','%d','%d','%d','%d','%d','%d','%d','%@','%d','%d','%d','%d','%@')",@"PostPaid",[objOpList.operatorID intValue],objOpList.operatorCode,[objOpList.productID intValue],objOpList.operatorName,[objOpList.serviceID intValue],[objOpList.flag intValue],[objOpList.operatorDelegate intValue],[objOpList.operatorSTV intValue],1,1,[objOpList.operatorMax intValue],[objOpList.operatorMin intValue],objOpList.operatorChargeSlab,[objOpList.serviceChargeAmount intValue],[objOpList.serviceTaxPercent intValue],[objOpList.serviceChargePercent intValue],[objOpList.operatorLength intValue],objOpList.operatorPrefix];
        [objDBManager executeQuery:query];
    }
    
    if (objDBManager.affectedRows != 0) {
       // NSLog(@"Query was executed successfully.");
    }
    else{
       // NSLog(@"Could not execute the query.");
    }
}

-(void)saveDTHOperatorDataInSQLite:(NSMutableArray *)operatorArrayList{
    DBManager *  objDBManager = [[DBManager alloc]initWithDatabaseFilename:@"operatorDB.sql"];
    NSString *query;
    for(int i=0;i<operatorArrayList.count;i++){
        OperatorList *objOpList = [operatorArrayList objectAtIndex:i];
        query = [NSString stringWithFormat:@"insert into operator values(null,'%@','%d','%@','%d','%@','%d','%d','%d','%d','%d','%d','%d','%d','%@','%d','%d','%d','%d','%@')",@"DTH",[objOpList.operatorID intValue],objOpList.operatorCode,[objOpList.productID intValue],objOpList.operatorName,[objOpList.serviceID intValue],[objOpList.flag intValue],[objOpList.operatorDelegate intValue],[objOpList.operatorSTV intValue],1,1,[objOpList.operatorMax intValue],[objOpList.operatorMin intValue],objOpList.operatorChargeSlab,[objOpList.serviceChargeAmount intValue],[objOpList.serviceTaxPercent intValue],[objOpList.serviceChargePercent intValue],[objOpList.operatorLength intValue],objOpList.operatorPrefix];
        [objDBManager executeQuery:query];
        
    }
    
    if (objDBManager.affectedRows != 0) {
       // NSLog(@"Query was executed successfully.");
    }
    else{
       // NSLog(@"Could not execute the query.");
    }
}


#pragma mark - QuickPay

-(void)callQuickPayService:(quickPayResponse)callback{
    RequestData *requestData = [[RequestData alloc]init];
    requestData.baseURL = kBaseURL;
    requestData.webServiceURL = kQuickPayURL;
    requestData.showLoader = FALSE;
    
    requestData.requestSuccessBlock = ^(RequestData *request) {
        
    [self printResponse:request.responseData forRequest:@"QuickPay Response"];
    if ([self handleNullResponse:request])
    return;
        
    callback(nil, [self parseQuickPayResponse:request]);
    };
    
    
    requestData.requestFailureBlock = ^(RequestData *request) {
    [self handleNetworkError:request.responseData];
    callback(request.responseData, nil);
    };
    
    [[ServiceManager sharedInstance] makeRequest:requestData];
}

-(QuickPayResponse *)parseQuickPayResponse:(RequestData *)request{
    
    DBManager *  objDBManager = [[DBManager alloc]initWithDatabaseFilename:@"quickPayDB.sql"];
    NSString *query;
    
    query = [NSString stringWithFormat:@"delete from QuickTable"];
    
    [objDBManager executeQuery:query];
    
    if (objDBManager.affectedRows != 0) {
        //  NSLog(@"Query was executed successfully.");
    }
    else{
        //  NSLog(@"Could not execute the query.");
    }
    [Constant saveQuickPayRecord:FALSE];
    
    QuickPayResponse *quickResponse = [[QuickPayResponse alloc]init];
    quickResponse.status = [request.responseData objectForKey:@"status"];
    quickResponse.errorCode = [request.responseData objectForKey:@"errCode"];
    NSMutableArray *descArray = [request.responseData valueForKey:@"description"];
    
    for(NSMutableDictionary *dic in descArray){
    QucickPayList *objQuickList = [[QucickPayList alloc]init];
        
    if([NSNull null]!= [dic objectForKey:@"id"])
    objQuickList.quickID = [dic objectForKey:@"id"];
        
    if([NSNull null]!= [dic objectForKey:@"flag"])
    objQuickList.qucikFlag = [dic objectForKey:@"flag"];
        
    if([NSNull null]!= [dic objectForKey:@"name"])
    objQuickList.quickName = [dic objectForKey:@"name"];
        
   if([NSNull null]!= [dic objectForKey:@"amount"])
    objQuickList.amount = [dic objectForKey:@"amount"];
        
   if([NSNull null]!= [dic objectForKey:@"users_id"])
    objQuickList.userID = [dic objectForKey:@"users_id"];
        
    if([NSNull null]!= [dic objectForKey:@"stv"])
    objQuickList.stv = [dic objectForKey:@"stv"];
        
    if([NSNull null]!= [dic objectForKey:@"operator_id"])
    objQuickList.operatorID = [dic objectForKey:@"operator_id"];
        
    if([NSNull null]!= [dic objectForKey:@"product"])
    objQuickList.product = [dic objectForKey:@"product"];
        
    if([NSNull null]!= [dic objectForKey:@"number"])
    objQuickList.phNumber = [dic objectForKey:@"number"];
        
    if([NSNull null]!= [dic objectForKey:@"delegate_flag"])
    objQuickList.delegateFlag = [dic objectForKey:@"delegate_flag"];
        
    if([NSNull null]!= [dic objectForKey:@"missed_number"])
    objQuickList.missedNumber = [dic objectForKey:@"missed_number"];
        
    if([NSNull null]!= [dic objectForKey:@"confirmation"])
    objQuickList.confirmation = [dic objectForKey:@"confirmation"];
        
    if([NSNull null]!= [dic objectForKey:@"operator_name"])
    objQuickList.operatorName = [dic objectForKey:@"operator_name"];
        
   if([NSNull null]!= [dic objectForKey:@"transaction_flag"])
    objQuickList.transactionFlag = [dic objectForKey:@"transaction_flag"];
        
    if([NSNull null]!= [dic objectForKey:@"operator_code"])
    objQuickList.operatorCode = [dic objectForKey:@"operator_code"];
        
   if([NSNull null]!= [dic objectForKey:@"datetime"])
    objQuickList.dateTime = [dic objectForKey:@"datetime"];
        
    [quickResponse.descriptionArray addObject:objQuickList];
        
    }
    
    [self saveQuickPayDataInDB:quickResponse];
    
    return quickResponse;

}

-(void)saveQuickPayDataInDB:(QuickPayResponse *)quickResponse{
    DBManager* objMobDBManager = [[DBManager alloc]initWithDatabaseFilename:@"quickPayDB.sql"];
    NSString *query;
    for(int i=0;i<quickResponse.descriptionArray.count;i++){
    QucickPayList *objQuickList = [quickResponse.descriptionArray objectAtIndex:i];
    //NSLog(@"#InsertValues :%@,%d,%@,%d,%d,%d,%d,%@,%d,%@,%@,%@,%@,%@,%d",objQuickList.quickID,[objQuickList.qucikFlag intValue],objQuickList.quickName,[objQuickList.amount intValue],[objQuickList.stv intValue],[objQuickList.operatorID intValue],[objQuickList.product intValue],objQuickList.phNumber,[objQuickList.delegateFlag intValue],objQuickList.missedNumber,objQuickList.operatorName ,objQuickList.operatorCode,[Constant currentSystemDateTime],objQuickList.dateTime,[objQuickList.transactionFlag intValue]);
        
        
    query = [NSString stringWithFormat:@"insert into QuickTable values(null,'%@','%d','%@','%d','%d','%d','%d','%@','%d','%@','%@','%@','%@','%@','%d')",objQuickList.quickID,[objQuickList.qucikFlag intValue],objQuickList.quickName,[objQuickList.amount intValue],[objQuickList.stv intValue],[objQuickList.operatorID intValue],[objQuickList.product intValue],objQuickList.phNumber,[objQuickList.delegateFlag intValue],objQuickList.missedNumber,objQuickList.operatorName ,objQuickList.operatorCode,[Constant currentSystemDateTime],objQuickList.dateTime,[objQuickList.transactionFlag intValue]];
        
  //  NSLog(@"#InsertQuery :%@",query);
        
    [objMobDBManager executeQuery:query];
        
    }
    
    
    if (objMobDBManager.affectedRows != 0) {
       // NSLog(@"Query was executed successfully.");
    }
    else{
        //NSLog(@"Could not execute the query.");
    }
    
}

#pragma mark - Edit QuickPay

-(void)callEditQuickPayService:(EditQuickPayCriteria *)criteria callback:(editQuickPayRessponse)callback{
    NSMutableDictionary *postData = [[NSMutableDictionary alloc]init];
    [postData setObject:criteria.mobileNumber forKey:@"number"];
    [postData setObject:criteria.name forKey:@"name"];
    [postData setObject:criteria.amount forKey:@"amount"];
    [postData setObject:criteria.operatorID forKey:@"operator_id"];
    [postData setObject:criteria.delagteFlag forKey:@"delegate_flag"];
    [postData setObject:criteria.flag forKey:@"flag"];
    [postData setObject:criteria.quickID forKey:@"id"];
    [postData setObject:criteria.quickSTV forKey:@"stv"];
    [postData setObject:criteria.confirmation forKey:@"confirmation"];


    NSLog(@"#PostForEditQuickPay :%@",postData);
    
    RequestData *requestData = [[RequestData alloc]init];
    requestData.baseURL = kBaseURL;
    requestData.webServiceURL = kEditPayURL;
    requestData.postData = postData;
    requestData.showLoader = FALSE;
    
    requestData.requestSuccessBlock = ^(RequestData *request) {
        
    [self printResponse:request.responseData forRequest:@"Edit QuickPay Response"];
    if ([self handleNullResponse:request])
    return;
        
    callback(nil, [self parseEditQuickPayResponse:request]);
    };
    
    
    requestData.requestFailureBlock = ^(RequestData *request) {
    [self handleNetworkError:request.responseData];
    callback(request.responseData, nil);
    };
    
    [[ServiceManager sharedInstance] makeRequest:requestData];
}
-(QuickPayResponse *)parseEditQuickPayResponse:(RequestData *)request{
    QuickPayResponse *response = [[QuickPayResponse alloc]init];
    response.errorCode = [request.responseData objectForKey:@"errCode"];
    response.status = [request.responseData objectForKey:@"status"];
    
    if([[request.responseData objectForKey:@"description"] isKindOfClass:[NSDictionary class]]){
    NSMutableDictionary *dic = [request.responseData objectForKey:@"description"];
    response.missedNumber = [dic objectForKey:@"missed_number"];
    response.message = [dic objectForKey:@"msg"];
    }
    else{
    response.message = [request.responseData objectForKey:@"description"];
  
    }
    return response;
}
@end
