//
//  WalletHistoryResponse.m
//  Pay1
//
//  Created by webninjaz on 19/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "WalletHistoryResponse.h"

@implementation WalletHistoryResponse

-(id)init {
    self = [super init];
    if(self) {
        self.walletDataArray = [[NSMutableArray alloc] init] ;
    }
    return self;
}

@end
