﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace NewPay1B2C
	{
	public partial class TermsAndCondition : PhoneApplicationPage
		{//get_faq/?req=tnc
		ProgressBarManager ProgressBarManager;
		public TermsAndCondition ()
			{
			InitializeComponent ();
			GoogleAnalytics.EasyTracker.GetTracker ().SendView ("TermsAndCondition");
			if (Convert.ToInt16 (PhoneApplicationService.Current.State["isTermsAndCondition"]) == 1)
				{
				
				textTitlePath.Text = "Terms & Condition";
				GoogleAnalytics.EasyTracker.GetTracker ().SendView ("TermsAndCondition");
				}
			else if (Convert.ToInt16 (PhoneApplicationService.Current.State["isTermsAndCondition"]) == 3)
				{
				
				textTitlePath.Text = "FAQs";
				GoogleAnalytics.EasyTracker.GetTracker ().SendView ("FAQs");
				}
			else if (Convert.ToInt16 (PhoneApplicationService.Current.State["isTermsAndCondition"]) == 2)
				{
				
				textTitlePath.Text = "Privacy Policy";
				GoogleAnalytics.EasyTracker.GetTracker ().SendView ("PrivacyPolicy");
				}

			ProgressIndicator prog = new ProgressIndicator ();
			ProgressBarManager = new ProgressBarManager ();
			SystemTray.SetProgressIndicator (this, prog);
			ProgressBarManager.Hook (prog);
			}

		private async void wb1_Loaded (object sender, RoutedEventArgs e)
			{
			string loadedHtmlString = "";
			if (Convert.ToInt16 (PhoneApplicationService.Current.State["isTermsAndCondition"]) == 1)
				loadedHtmlString = Constant.LoadPersistent<string> ("tnc");//loadedHtmlString = "api/true/actiontype/get_faq/?req=tnc";
			else if (Convert.ToInt16 (PhoneApplicationService.Current.State["isTermsAndCondition"]) == 2)
				loadedHtmlString = Constant.LoadPersistent<string> ("pnp");//loadedHtmlString = "api/true/actiontype/get_faq/?req=pnp";
			else if (Convert.ToInt16 (PhoneApplicationService.Current.State["isTermsAndCondition"]) == 3)
				loadedHtmlString = Constant.LoadPersistent<string> ("faq");// "api/true/actiontype/get_faq/?req=faq";

			if (string.IsNullOrEmpty (loadedHtmlString))
				{

				ProgressBarManager.Show (23, "Please wait...", false, 10);
				string answer = "";
				string sub_url = "";
				if (Convert.ToInt16 (PhoneApplicationService.Current.State["isTermsAndCondition"]) == 1)
					sub_url = "api/true/actiontype/get_faq/?req=tnc";
				else if (Convert.ToInt16 (PhoneApplicationService.Current.State["isTermsAndCondition"]) == 2)
					sub_url = "api/true/actiontype/get_faq/?req=pnp";
				else if (Convert.ToInt16 (PhoneApplicationService.Current.State["isTermsAndCondition"]) == 3)
					sub_url = "api/true/actiontype/get_faq/?req=faq";

				Task<string> response = Constant.postParamsAndgetResponse (Constant.B2C_URL + sub_url);

				string result = await response;
				if (string.IsNullOrEmpty (result))
					{
					NavigationService.Navigate (new Uri ("/NoConnection.xaml?", UriKind.Relative));
					}
				else
					{

					//var dict = JObject.Parse(result);
					var dict = (JObject)JsonConvert.DeserializeObject (result);
					string status = dict["status"].ToString ();
					if (status.Equals ("success"))
						{
						var description = dict["description"];
						foreach (var item in description)
							{
							answer = item["answer"].ToString ();
							}


						string htmlString = "<html><head><style>@font-face {font-family: Normal;src: url(\"/NewPay1B2C/Fonts/EncodeSansNormal-100-Thin.ttf\")}"
																+ "@font-face {font-family: Narrow;src: url(\"/NewPay1B2C/Fonts/EncodeSansNarrow-500-Medium.ttf\")}"
																+ "body {margin: 0px;padding:15px;text-align:justify;}"
																+ "p {margin: 0px;text-align:justify;padding:0 0 10px 0;font-size:14px;font-family:\"Normal\";}"
																+ "h2{font-size:14px;font-family:\"Narrow\";margin:0;padding:10px 0 5px 0;color:#513D98;}"
																+ "ul{margin:0 0 10px 0;padding-left:20px;}"
																+ "li{font-size:12px;text-align:justify;margin:0 0px 2px 0px;font-family:\"Normal\";}</style></head><body>"
																+ answer
																+ "</body></html>";



						webBrowser1.NavigateToString (htmlString);
						webBrowser1.IsScriptEnabled = true;
						if (Convert.ToInt16 (PhoneApplicationService.Current.State["isTermsAndCondition"]) == 1)
							//sub_url = "api/true/actiontype/get_faq/?req=tnc";
							Constant.SavePersistent ("tnc", htmlString);
						else if (Convert.ToInt16 (PhoneApplicationService.Current.State["isTermsAndCondition"]) == 2)
							//sub_url = "api/true/actiontype/get_faq/?req=pnp";
							Constant.SavePersistent ("pnp", htmlString);
						else if (Convert.ToInt16 (PhoneApplicationService.Current.State["isTermsAndCondition"]) == 3)
							//sub_url = "api/true/actiontype/get_faq/?req=faq";
							Constant.SavePersistent ("faq", htmlString);
						}
					else
						{
						Constant.checkForErrorCode (result); //MessageBox.Show ("Currently not available");
						}
					}
				ProgressBarManager.Hide (23);
				}
			else
				{
				webBrowser1.NavigateToString (loadedHtmlString);
				webBrowser1.IsScriptEnabled = true;
				}
			}

		private void buttonImage_Click_Back (object sender, RoutedEventArgs e)
			{
			NavigationService.GoBack ();
			}
		}
	}