//
//  RateView.h
//  Pay1
//
//  Created by Annapurna on 24/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RateView : UIView


@property (nonatomic, strong) IBOutlet UIButton *loveITButton;
@property (nonatomic, strong) IBOutlet UIButton *likeITButton;
@property (nonatomic, strong) IBOutlet UIButton *justOKButton;
@property (nonatomic, strong) IBOutlet UIButton *poorButton;
@property (nonatomic, strong) IBOutlet UIButton *badButton;


@property (nonatomic, strong) IBOutlet UILabel *loveITLabel;
@property (nonatomic, strong) IBOutlet UILabel *likeITLabel;
@property (nonatomic, strong) IBOutlet UILabel *justOKLabel;
@property (nonatomic, strong) IBOutlet UILabel *poorLabel;
@property (nonatomic, strong) IBOutlet UILabel *badLabel;

@property (nonatomic, copy)void (^onCrossClick)();
@property (nonatomic, copy)void (^onLoveITClick)();
@property (nonatomic, copy)void (^onLikeITClick)();
@property (nonatomic, copy)void (^onJustOKClick)();
@property (nonatomic, copy)void (^onPoorClick)();
@property (nonatomic, copy)void (^onItsBadClick)();


@end
