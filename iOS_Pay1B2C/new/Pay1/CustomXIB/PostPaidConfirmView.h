//
//  PostPaidConfirmView.h
//  Pay1
//
//  Created by webninjaz on 06/09/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <UIKit/UIKit.h>
@class TPKeyboardAvoidingScrollView;
@interface PostPaidConfirmView : UIView<UITextFieldDelegate>{
    
    IBOutlet UILabel *mobileNumLabel;
    IBOutlet UITextField *amountTF;

    IBOutlet UILabel *serviceChargeLabel;
    IBOutlet UIButton *checkButton;
    
    NSString *chargePerStr;
    NSString *chargeSlabStr;
    NSString *chargeAmountStr;

    
    NSString *netAmountForRecharge;
    UIView* myView;
    
    IBOutlet UITextField * errorTF;
    IBOutlet UIView *sepView;

}
-(void)PutDataOnCancelAlert:(NSString *) phoneNumber chargeSlab:(NSString *)chargeSlab chargePercent:(NSString *)chargePercent chargeAmount:(NSString *)chargeAmount;
@property (nonatomic, copy)void (^onProceedClick)(NSString *totalAmount,NSString *baseAmount,NSString *serviceCharge,NSString *serviceTax);
@property (nonatomic, copy)void (^onCancelClick)();

@property (nonatomic, copy)void (^onCheckClick)(BOOL isClicked);
@property (nonatomic, strong) IBOutlet TPKeyboardAvoidingScrollView *scrollView;



@end
