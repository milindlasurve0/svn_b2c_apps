﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Pay1B2C.Model;
using Pay1B2C.ViewModel;
using SQLite;
using Pay1B2C.Utilities;

namespace Pay1B2C.Views
    {
    public partial class OperatorList: PhoneApplicationPage
        {
        TextBlock textBlock1;
        string prevText = "";
        bool LayoutUpdateFlag = true;
        string flag;
      
        List<OperatorDB> result = null;
        List<Operator> resOperator = null;

        public OperatorList()
            {
            InitializeComponent();
         
            GoogleAnalytics.EasyTracker.GetTracker().SendView("Search Operator");
           // flag = PhoneApplicationService.Current.State[Constant.OPERATOR_RECHARGE_FLAG].ToString();
            flag = RechargeMain.flag;
           
            resOperator = new List<Operator>();
            if(flag.Equals("1"))
                {
                textTitlePath.Text = "Recharge > Mobile > Operator ";
                }
            else if(flag.Equals("3"))
                {
                textTitlePath.Text = "Recharge > Data card > Operator ";
                }
            else if(flag.Equals("2"))
                {
                textTitlePath.Text = "Recharge > Dth > Operator ";
                }
            else if(flag.Equals("4"))
                {
                textTitlePath.Text = "Mobile Bill > Operator ";
                }
            //Listloading();
            }

        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
            {
            ListBox listBox = sender as ListBox;

            if(listBox.SelectedIndex != -1)
                {
                var selected = result[listBox.SelectedIndex]; 
                string selectedId = selected.id;
               
                PhoneApplicationService.Current.State["operatorData"] = selected;
                PhoneApplicationService.Current.State["data"] = "1";
               
                NavigationService.GoBack();

                }
            }

        protected override void OnNavigatedTo(NavigationEventArgs e)
            {
            // flag = PhoneApplicationService.Current.State[Constant.OPERATOR_RECHARGE_FLAG].ToString ();
            Listloading();
            }

       
        private void Listloading()
            {

            try
                {
               
                OperatorHelper operatorHelper=new OperatorHelper();
                 result=operatorHelper.ReadOperatorDB(RechargeMain.flag).ToList();
                 resOperator.Clear();
                foreach(var item in result)
                    {
                    //OperatorUserControl oper = new OperatorUserControl ();
                    Operator oper = new Operator();
                    oper.name = item.name;

                    oper.imagePath = OperatorConstant.getOperatorResource(Convert.ToInt16(item.p_id));
                    //listBoxobj.Items.Add (oper);
                    resOperator.Add(oper);
                    }

                }
            catch(SQLiteException ed)
                {
                MessageBox.Show("Error");
                }
          
            listBoxobj.ItemsSource = resOperator;
            }



        private void buttonImage_Click_Back(object sender, RoutedEventArgs e)
            {
            NavigationService.GoBack();
            }

       
        }
    }