//
//  GiftSingleViewController.h
//  Pay1
//
//  Created by Annapurna on 10/08/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "BaseViewController.h"
#import "EnumType.h"
@class GiftLocationList;
@interface GiftSingleViewController : BaseViewController<GMSMapViewDelegate>{
     IBOutlet GMSMapView *_mapView;
    
    
    NSMutableArray *_coordinates;
    
    GMSMarker *myMarker;
    GMSMarker *shopMarker;
    
    

}

@property (nonatomic, assign) GiftSelectedType objGiftType;
@property (nonatomic, strong) UIImage *thumbImage;
@property (nonatomic, strong) NSString *shopName;

@property (nonatomic, strong) NSString *locationLat;
@property (nonatomic, strong) NSString *locationLong;
@property (nonatomic, strong) NSString *locationAddress;



@end
