﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Net.Http;

namespace Pay1B2C.Views
    {
    public partial class OnlineRefill: PhoneApplicationPage
        {
        ProgressBarManager ProgressBarManager;
        public OnlineRefill()
            {
            InitializeComponent ();
			 
          //  Constant.laodNavigationBar(TitlePanel, LayoutRoot, canvas,"Online Topup");
            ProgressIndicator prog = new ProgressIndicator ();
            ProgressBarManager = new ProgressBarManager ();
            SystemTray.SetProgressIndicator (this, prog);
            ProgressBarManager.Hook (prog);
			GoogleAnalytics.EasyTracker.GetTracker ().SendView ("Online Topup");
            }

		protected override void OnBackKeyPress (System.ComponentModel.CancelEventArgs e)
			{
			//Do your work here
			base.OnBackKeyPress (e);
			NavigationService.Navigate (new Uri ("/HomePage.xaml?", UriKind.RelativeOrAbsolute));
			while (this.NavigationService.BackStack.Any ())
				{
				this.NavigationService.RemoveBackEntry ();
				}
			}
        private async void taskReffilViaCard (string amount)
            {
            ProgressBarManager.Show (15, "Please wait...", false, 10);
            var values = new List<KeyValuePair<string, string>>
                        { 
                        new KeyValuePair<string, string>("actiontype","online_walletrefill"),
                        new KeyValuePair<string, string>("amount",amount),
                        new KeyValuePair<string, string>("api_version", Constant.API_VERSION),
                        new KeyValuePair<string, string>("api","true"),
                        };

            FormUrlEncodedContent values1 = new FormUrlEncodedContent (values);
            Task<string> response = Constant.postParamsAndgetResponse (Constant.B2C_URL, values1);

            string result = await response;
            if (string.IsNullOrEmpty (result))
                {
                NavigationService.Navigate (new Uri ("/Views/NoConnection.xaml?", UriKind.Relative));
                }
            else
                {
                PhoneApplicationService.Current.State[Constant.OPERATOR_RECHARGE_NAME] = "Wallet refill";
                var dict = (JObject)JsonConvert.DeserializeObject (result);
                string status = dict["status"].ToString ();

                if (status.Equals ("success"))
                    {
                    var description = dict["description"];

                    PhoneApplicationService.Current.State["txn_id"]=description["txnid"].ToString();


                    string form_content = description["form_content"].ToString ();
                    PhoneApplicationService.Current.State[Constant.PAYMENT_GATEWAY_HTML] = form_content;
                    PhoneApplicationService.Current.State[Constant.OPERATOR_RECHARGE_FLAG] = Constant.WALLET_REFILL;
                    NavigationService.Navigate (new Uri ("/Views/PaymentGatewayLayout.xaml?page=TopUpWallet", UriKind.RelativeOrAbsolute));
                    //NavigationService.RemoveBackEntry ();
                    }
                else
                    {

					Constant.checkForErrorCode (result); //MessageBox.Show (dict["description"].ToString ());
                    }
                }
            ProgressBarManager.Hide (15);
            }


        private void buttonImage_Click (object sender, RoutedEventArgs e)
            {
            string amount = textAmount.Text;
            if (amount.Length != 0)
                {
                taskReffilViaCard (amount);
                PhoneApplicationService.Current.State[Constant.RECHARGE_AMOUNT] = amount;
                }
            else
                {
                MessageBox.Show ("Please enter correct amount.");
                }
            }


        private void buttonImage_Click_Cancel (object sender, RoutedEventArgs e)
            {
			textAmount.Text = "";
            }

        private void buttonImage_Click_Back(object sender, RoutedEventArgs e)
            {
            NavigationService.GoBack ();
            }


        }
    }