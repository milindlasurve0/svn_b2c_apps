<div class="clearfix"></div>

    <?php
    $data = getOperatorDetails();
    $operatorData = json_decode($data, true);
    $parameters = $this->uri->uri_to_assoc();
    foreach ($operatorData as $operatorkey => $operatorval) {
            if (is_array($operatorval)) {
                    foreach ($operatorval as $operatorkey => $operatorvalue) {
                            if ($operatorkey == "mobile") {
                                    $arrayval["Mobile"] = $operatorvalue;
                            } elseif ($operatorkey == "data") {
                                    $arrayval["data"] = $operatorvalue;
                            } elseif ($operatorkey == "dth") {
                                    $arrayval["dth"] = $operatorvalue;
                            } elseif ($operatorkey == "postpaid") {
                                    $arrayval["postpaid"] = $operatorvalue;
                            }
                    }
            }
    }

    ?>



    <div class="footer-section">
         <div class="container no-disp visible-md visible-lg">
            <div class="row">
               <div class="col-md-4">
                  <div class="footer_list">
                    <ul class="footer-list">
                        <li class="footer_heading">MOBILE RECHARGE</li>
                            <?php foreach ($arrayval['Mobile'] as $mobval){ ?>
                <a href="/recharge-<?php echo strtolower(str_replace(" ", "-",$mobval['name'])); ?>-mobile-online"><?php echo $mobval['name'] ?>&nbsp;&nbsp;(Recharge)</a><br><br>
		            <?php } ?>
                    </ul>
                     <ul class="footer-list">
                        <li class="footer_heading">SUPPORT</li>
                        <li class="no-marg">For help, send email to<br><a href='/contact'>listen@pay1.in</a></li>
                     </ul>
                     <ul class="footer-list">
                        <li class="footer_heading">SECURED</li>
                        <li class="no-marg"><img src="/assets/images/footer/secured_footer.png" alt=""></li>
                     </ul>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="footer_list">
                     <ul class="footer-list">
                        <li class="footer_heading">DATA CARD RECHARGE</li>
                        <?php foreach ($arrayval['data'] as $dataval){ ?>
                <a href="/recharge-<?php echo strtolower(str_replace(" ", "-",$dataval['name'])); ?>-data-online"><?php echo $dataval['name'] ?>&nbsp;&nbsp;(Recharge)</a><br><br>
	                <?php } ?>
                     </ul>
                     <ul class="footer-list">
                        <li class="footer_heading">DTH (TV) RECHARGE</li>
                        <?php foreach ($arrayval['dth'] as $dthval){ ?>
                <a href="/recharge-<?php echo strtolower(str_replace(" ","-",$dthval['name'])); ?>-online"><?php echo $dthval['name'] ?>&nbsp;&nbsp;</a><br><br>
				<?php } ?>
                     </ul>
<!--                     <ul class="footer-list">
                        <li class="footer_heading">GIFTS</li>
                        <li><a href="/giftstore/mygifts">My Gifts</a></li>
                        <li><a href="/profile/index">Gift Coins</a></li>
                     </ul>-->
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="footer_list">
                     <ul class="footer-list">
                        <li class="footer_heading">POSTPAID</li>
                        <?php foreach ($arrayval['postpaid'] as $postpaidval){ ?>
                           <a href="/recharge-<?php echo strtolower(str_replace(" ","-",$postpaidval['name'])); ?>-online"><?php echo $postpaidval['name'] ?>&nbsp;&nbsp;(Bill Payment)</a><br><br>
			<?php } ?>
                     </ul>
                     <ul class="footer-list">
                        <li class="footer_heading">PAY1</li>
                        <li><a href="/about-us">About Us</a></li>
                        <li><a href="/career">Careers</a></li>
                        <li><a href="/faqs">FAQ</a></li>
                        <li><a href="/contact-us">Contact Us</a></li>
                        <li><a href="/privacy-policy">Privacy Policy</a></li>
                        <li><a href="/sitemap">Sitemap</a></li>
                        <li><a href="/terms-and-conditions">T&amp;C </a></li>
                        <li><a target="_blank" href="/partners-blog/">Blog</a></li>
                        <!--<li><a href="/security_policy">Security Policy </a></li>-->
                     </ul>
                     <ul class="footer-list">
                        <li class="footer_heading">DOWNLOAD APP</li>
<!--                        <li><a target="_blank" href="https://goo.gl/lqAaqU">Android App</a></li>-->
<!--                            <li><a target="_blank" href="http://goo.gl/XF3d12">Windows</a></li>-->
                        <li><a target="_blank" href="https://play.google.com/store/apps/details?id=com.pay1&hl=en">Android App</a></li>
                        <li><a target="_blank" href="https://www.microsoft.com/en-us/store/apps/pay1/9nblggh0dd65">Windows</a></li>
                        <!--<li><a href="#">iOS</a></li>-->
                     </ul>
                     <ul class="footer-list">
                        <li class="footer_heading">CONNECT US ON</li>
                        <li class="social"><a target="_blank" href="https://www.facebook.com/pay1app?_rdr"><i class="fa fa-facebook"></i></a></li>
                        <li class="social"><a target="_blank" href="https://twitter.com/pay1app"><i class="fa fa-twitter"></i></a></li>
                        <li class="social"><a target="_blank" href="https://plus.google.com/+Pay1Inapp"><i class="fa fa-google-plus"></i></a></li>
                        <li class="social"><a target="_blank" href="https://www.pinterest.com/pay1app"><i class="fa fa-pinterest"></i></a></li>
                        <li class="social"><a target="_blank" href="https://instagram.com/pay1app"><i class="fa fa-instagram"></i></a></li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
         <div class="margin-top-30"></div>
         <div class="margin-top-30"></div>
         <div class="copyright">
            <div class="container text-center">
               <span class="glyphicon glyphicon-copyright-mark"></span>Mindsarray Technologies Pvt. Ltd. All Rights Reserved.
            </div>
         </div>
      </div>
       
        <script>var HOST="<?php echo base_url();  ?>";</script>
        <script type="text/javascript" src="<?php echo base_url('js/md5.js'); ?>"></script>
        
        <script type="text/javascript"  src="<?php echo base_url('js/gifts.js'); ?>"></script>
        <script type="text/javascript"  src="<?php echo base_url('js/json2.js'); ?>"></script>
        <script type="text/javascript"  src="<?php echo base_url('js/jstorage.js'); ?>"></script>
        <script type="text/javascript"  src="<?php echo base_url('assets/js/jquery.CC.js'); ?>" ></script>
<!--        <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>-->
        <!--<script src="https://maps.googleapis.com/maps/api/js"></script>-->
<!--        <script type="text/javascript"  src="https://maps.googleapis.com/maps/api/js"></script>-->

        <script type="text/javascript" src="<?php echo base_url('js/bootstrap-validator.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('js/bootstrap-datepicker.js'); ?>"></script>
        
        <script type="text/javascript"  src="<?php echo base_url('assets/js/jquery-scrollToTop.js'); ?>" ></script>

        <script type="text/javascript"  src="<?php echo base_url('assets/js/payment.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/slider.js'); ?>"></script>
       
        <script src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/js/jssor.slider.mini.js');?>"></script>
        <script type="text/javascript"  src="<?php echo base_url('assets/validation/jquery.validationEngine.js'); ?>"></script>
        <script type="text/javascript"  src="<?php echo base_url('assets/validation/jquery.validationEngine-en.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery.carouFredSel.js'); ?>" ></script>


        <script src="<?php echo base_url('assets/js/ie10-viewport-bug-workaround.js');?>"></script>
         <script type="text/javascript" src="<?php echo base_url('js/profile.js'); ?>"></script>

        
          <script type="text/javascript">
         $(document).ready(function($) {
             $('body').scrollToTop({skin: 'cycle'});
         });
      </script>
    </body>
    </html>
