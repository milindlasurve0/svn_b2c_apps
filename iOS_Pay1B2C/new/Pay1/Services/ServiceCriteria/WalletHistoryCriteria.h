//
//  WalletHistoryCriteria.h
//  Pay1
//
//  Created by webninjaz on 19/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WalletHistoryCriteria : NSObject

@property (nonatomic,strong) NSString *walletHistoryParamCriteria;
@property (nonatomic,strong) NSString *walletHistoryPageCriteria;
@property (nonatomic,strong) NSString *walletHistoryLimitCriteria;


@end
