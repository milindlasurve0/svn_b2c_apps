package com.pay1.adapterhandler;

import java.util.WeakHashMap;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.pay1.R;

public class MenuAdapter extends BaseExpandableListAdapter {

	private Context _context;
	private List<String> _listDataHeader; // header titles
	// child data in format of header title, child title
	private WeakHashMap<String, List<String>> _listDataChild;
	private Typeface Reguler, Narrow, Narrow_thin;

	public MenuAdapter(Context context, List<String> listDataHeader,
			WeakHashMap<String, List<String>> listChildData) {
		this._context = context;
		this._listDataHeader = listDataHeader;
		this._listDataChild = listChildData;

		Reguler = Typeface.createFromAsset(context.getAssets(),
				"EncodeSansNormal-400-Regular.ttf");

		Narrow = Typeface.createFromAsset(context.getAssets(),
				"EncodeSansNarrow-500-Medium.ttf");

		Narrow_thin = Typeface.createFromAsset(context.getAssets(),
				"EncodeSansNarrow-100-Thin.ttf");
	}

	@Override
	public Object getChild(int groupPosition, int childPosititon) {
		return this._listDataChild.get(this._listDataHeader.get(groupPosition))
				.get(childPosititon);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@SuppressLint("ClickableViewAccessibility")
	@Override
	public View getChildView(int groupPosition, final int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {

		final String childText = (String) getChild(groupPosition, childPosition);

		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this._context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.menu_listview_adapter,
					null);
		}

		TextView textView_Item = (TextView) convertView
				.findViewById(R.id.textView_Item);
		textView_Item.setTypeface(Reguler);
		ImageView imageView_Background = (ImageView) convertView
				.findViewById(R.id.imageView_Background);
		// imageView_Background.setOnTouchListener(new View.OnTouchListener() {
		//
		// @Override
		// public boolean onTouch(View v, MotionEvent event) {
		// // TODO Auto-generated method stub
		// final int X = (int) event.getRawX();
		// final int Y = (int) event.getRawY();
		// int _xDelta = 0, _yDelta = 0;
		// switch (event.getAction() & MotionEvent.ACTION_MASK) {
		// case MotionEvent.ACTION_DOWN:
		// RelativeLayout.LayoutParams lParams = (RelativeLayout.LayoutParams)
		// imageView_Background
		// .getLayoutParams();
		// _xDelta = X - lParams.leftMargin;
		// _yDelta = Y - lParams.topMargin;
		// break;
		// case MotionEvent.ACTION_UP:
		// break;
		// case MotionEvent.ACTION_POINTER_DOWN:
		// break;
		// case MotionEvent.ACTION_POINTER_UP:
		// break;
		// case MotionEvent.ACTION_MOVE:
		// RelativeLayout.LayoutParams layoutParams =
		// (RelativeLayout.LayoutParams) imageView_Background
		// .getLayoutParams();
		// layoutParams.leftMargin = X - _xDelta;
		// layoutParams.topMargin = Y - _yDelta;
		// layoutParams.rightMargin = -250;
		// layoutParams.bottomMargin = -250;
		// imageView_Background.setLayoutParams(layoutParams);
		// break;
		// }
		// imageView_Background.invalidate();
		// return true;
		// }
		// });

		switch (groupPosition) {
		case 0: {
			// textView_Item.setBackgroundColor(_context.getResources().getColor(
			// R.color.app_green_color));
			switch (childPosition) {
			case 0: {
				imageView_Background.setImageDrawable(_context.getResources()
						.getDrawable(R.drawable.menu_mobile));
				Animation animation2 = AnimationUtils.loadAnimation(_context,
						R.anim.decelerate);
				animation2.setDuration(1050);
				convertView.startAnimation(animation2);
			}
				break;
			case 1: {
				imageView_Background.setImageDrawable(_context.getResources()
						.getDrawable(R.drawable.menu_dth));
				Animation animation2 = AnimationUtils.loadAnimation(_context,
						R.anim.decelerate);
				animation2.setDuration(1000);
				convertView.startAnimation(animation2);
			}
				break;
			case 2: {
				imageView_Background.setImageDrawable(_context.getResources()
						.getDrawable(R.drawable.menu_datacard));
				Animation animation2 = AnimationUtils.loadAnimation(_context,
						R.anim.decelerate);
				animation2.setDuration(950);
				convertView.startAnimation(animation2);
			}
				break;
			default:
				break;
			}
		}

			break;
		case 1: {
			// textView_Item.setBackgroundColor(_context.getResources().getColor(
			// R.color.app_orange_color));
			switch (childPosition) {
			case 0: {
				imageView_Background.setImageDrawable(_context.getResources()
						.getDrawable(R.drawable.menu_deals));
				Animation animation2 = AnimationUtils.loadAnimation(_context,
						R.anim.decelerate);
				animation2.setDuration(900);
				convertView.startAnimation(animation2);
			}
				break;
			default:
				break;
			}
		}
			break;
		case 2: {
			// textView_Item.setBackgroundColor(_context.getResources().getColor(
			// R.color.app_skyblue_color));
			switch (childPosition) {
			case 0: {
				imageView_Background.setImageDrawable(_context.getResources()
						.getDrawable(R.drawable.menu_postpaid));
				Animation animation2 = AnimationUtils.loadAnimation(_context,
						R.anim.decelerate);
				animation2.setDuration(850);
				convertView.startAnimation(animation2);
			}
				break;
			case 1: {
				imageView_Background.setImageDrawable(_context.getResources()
						.getDrawable(R.drawable.menu_electricity));
				Animation animation2 = AnimationUtils.loadAnimation(_context,
						R.anim.decelerate);
				animation2.setDuration(800);
				convertView.startAnimation(animation2);
			}
				break;
			default:
				break;
			}
		}
			break;

		default:
			break;
		}
		textView_Item.setText(childText);

		return convertView;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return this._listDataChild.get(this._listDataHeader.get(groupPosition))
				.size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		return this._listDataHeader.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		return this._listDataHeader.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		String headerTitle = (String) getGroup(groupPosition);
		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this._context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.menu_header_adapter,
					null);
		}
		ExpandableListView mExpandableListView = (ExpandableListView) parent;
		mExpandableListView.expandGroup(groupPosition);

		TextView textView_Header = (TextView) convertView
				.findViewById(R.id.textView_Header);
		textView_Header.setTypeface(Narrow);
		textView_Header.setText(headerTitle);
		if (groupPosition == 1)
			textView_Header.setVisibility(View.GONE);
		convertView.setClickable(false);
		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}
}
