package com.pay1;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.pay1.customviews.MyProgressDialog;

import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pay1.adapterhandler.PurchaseAdapter;
import com.pay1.constants.Constants;
import com.pay1.requesthandler.RequestClass;

public class PurchaseHistoryActivity extends AppBaseActivity {

	private static final String SCREEN_LABEL = "Purchase History Screen";
	private EasyTracker easyTracker = null;
	public static final String TAG = "Purchase History";
	int page = 0;
	ArrayList<JSONObject> arrayList;
	ListView listViewPurchaseHistory;
	PurchaseAdapter purchaseAdapter;
	Button button_LoadMore;
	ImageView imageView_Back;
	TextView textView_Title, textView_NoData, textView_Shop;
	private RelativeLayout relativeLayout_EmptyView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.purchase_history_activity);
		try {
			easyTracker = EasyTracker.getInstance(PurchaseHistoryActivity.this);
			easyTracker.set(Fields.SCREEN_NAME, SCREEN_LABEL);
			registerBaseActivityReceiver();
			Typeface Reguler = Typeface.createFromAsset(getAssets(),
					"EncodeSansNormal-400-Regular.ttf");

			textView_Title = (TextView) findViewById(R.id.textView_Title);
			textView_Title.setTypeface(Reguler);
			textView_NoData = (TextView) findViewById(R.id.textView_NoData);
			textView_NoData.setTypeface(Reguler);
			textView_Shop = (TextView) findViewById(R.id.textView_Shop);
			textView_Shop.setTypeface(Reguler);

			arrayList = new ArrayList<JSONObject>();
			listViewPurchaseHistory = (ListView) findViewById(R.id.listViewPurchaseHistory);
			purchaseAdapter = new PurchaseAdapter(PurchaseHistoryActivity.this,
					arrayList);
			relativeLayout_EmptyView = (RelativeLayout) findViewById(R.id.relativeLayout_EmptyView);
			listViewPurchaseHistory.setEmptyView(relativeLayout_EmptyView);
			imageView_Back = (ImageView) findViewById(R.id.imageView_Back);
			imageView_Back.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onBackPressed();
				}
			});

			listViewPurchaseHistory
					.setOnItemClickListener(new OnItemClickListener() {

						@Override
						public void onItemClick(AdapterView<?> arg0, View arg1,
								int position, long arg3) {
							// TODO Auto-generated method stub
							Intent intent = new Intent(
									PurchaseHistoryActivity.this,
									PurchaseHistoryDetailActivity.class);
							intent.putExtra(Constants.PURCHASE_DETAILS,
									arrayList.get(position).toString());
							startActivity(intent);
						}
					});

			button_LoadMore = new Button(PurchaseHistoryActivity.this);
			button_LoadMore.setHeight(50);
			button_LoadMore
					.setBackgroundResource(R.drawable.button_background_unselected);
			button_LoadMore.setText("Load more.....");
			button_LoadMore
					.setTextColor(getResources().getColor(R.color.White));
			button_LoadMore.setTypeface(Reguler);
			button_LoadMore.setTextSize(15f);
			button_LoadMore.setVisibility(View.GONE);
			button_LoadMore.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					// // Log.e("Last post ID ", "" + lastPostID);
					button_LoadMore.setText("Loading.....");
					page++;
					new GetPurchaseHistoryTask().execute();
				}
			});

			listViewPurchaseHistory.addFooterView(button_LoadMore);
			listViewPurchaseHistory.setAdapter(purchaseAdapter);
			new GetPurchaseHistoryTask().execute();

		} catch (Exception e) {
		}
		overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}

	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView().build());
		// EasyTracker.getInstance(this).activityStart(this);
		easyTracker.send(MapBuilder.createAppView().build());
	}

	@Override
	public void onStop() {
		super.onStop();
		// EasyTracker.getInstance(this).activityStop(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (Constants.isExpanded) {
			Constants.collapsView(this);
		} else {
			Constants.SELECTED_MENU_INDEX = Constants.PROFILE_MENU_INDEX;
			Constants.showNavigationBar(PurchaseHistoryActivity.this);
		}
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unRegisterBaseActivityReceiver();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (Constants.isExpanded) {
			Constants.collapsView(this);
		} else {
			super.onBackPressed();
			overridePendingTransition(R.anim.slide_in_left,
					R.anim.slide_out_right);
		}
	}

	public class GetPurchaseHistoryTask extends
			AsyncTask<String, String, String> implements OnDismissListener {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String url = Constants.B2C_URL + "purchase_history/?page=" + page
					+ "&limit=10";
			String response = RequestClass.getInstance().readPay1B2CRequest(
					PurchaseHistoryActivity.this, url);
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			try {

				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONObject jsonObject = new JSONObject(replaced);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {
						JSONArray description = jsonObject
								.getJSONArray("description");

						int len = 0;
						for (int j = 0; j < description.length(); j++) {
							JSONObject jObject = description.getJSONObject(j);
							arrayList.add(jObject);
							len++;
						}
						if (len < 10) {
							button_LoadMore.setVisibility(View.GONE);
						} else {
							button_LoadMore.setText("Load more history");
							button_LoadMore.setVisibility(View.VISIBLE);
						}
						purchaseAdapter.notifyDataSetChanged();
						/*
						 * if(arrayList.size()%10==0){ AbsListView.LayoutParams
						 * lp = new
						 * AbsListView.LayoutParams(AbsListView.LayoutParams
						 * .MATCH_PARENT, 150); TextView mFooterView = new
						 * TextView(PurchaseHistoryActivity.this);
						 * mFooterView.setTextColor(Color.WHITE);
						 * mFooterView.setBackgroundColor(Color.BLUE);
						 * mFooterView.setGravity(Gravity.CENTER);
						 * mFooterView.setLayoutParams(lp);
						 * mFooterView.setText("Footer");
						 * 
						 * listViewPurchaseHistory.addFooterView(mFooterView);
						 * }else{
						 * 
						 * }
						 */
					} else {
						Constants.showOneButtonDialog(
								PurchaseHistoryActivity.this, TAG,
								Constants.checkCode(replaced),
								Constants.DIALOG_CLOSE);
					}

				} else {
					// Constants.showOneButtonDialog(PurchaseHistoryActivity.this,
					// TAG, Constants.ERROR_INTERNET,
					// Constants.DIALOG_CLOSE);
					Intent intent = new Intent(PurchaseHistoryActivity.this,
							ConnectivityErrorActivity.class);
					intent.putExtra(Constants.RECHARGE_FOR,
							Constants.WALLET_PAYMENT);
					startActivity(intent);
					finish();

				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonDialog(PurchaseHistoryActivity.this,
						TAG, Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonDialog(PurchaseHistoryActivity.this,
						TAG, Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(PurchaseHistoryActivity.this);
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							GetPurchaseHistoryTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			GetPurchaseHistoryTask.this.cancel(true);
			dialog.cancel();
		}

	}
}
