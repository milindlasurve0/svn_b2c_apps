﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Pay1B2C.UserControls;
using Pay1B2C.ViewModel;
using Pay1B2C.Model;
using System.Windows.Media.Imaging;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Windows.Devices.Geolocation;

namespace Pay1B2C
    {
    public partial class GiftMainPivot: PhoneApplicationPage
        {
        ProgressBarManager ProgressBarManager;
       public static string myOfferDetails;
        public GiftMainPivot()
            {
            InitializeComponent();
            Constant.laodNavigationBar(TitlePanel, LayoutRoot, canvas, "Gift Main Page");
            GoogleAnalytics.EasyTracker.GetTracker().SendView("GiftMainPage");
            ProgressIndicator prog = new ProgressIndicator();
            ProgressBarManager = new ProgressBarManager();
            SystemTray.SetProgressIndicator(this, prog);
            ProgressBarManager.Hook(prog);
          //  Constant.getCurrentLatLong();
            }
   
        private void home_Click(object sender, EventArgs e)
            {
            NavigationService.Navigate(new Uri("/HomePage.xaml?", UriKind.RelativeOrAbsolute));
            }

        private void recharges_Click(object sender, EventArgs e)
            {
            NavigationService.Navigate(new Uri("/Views/RechargeMain.xaml?", UriKind.RelativeOrAbsolute));
            
            }

        private void gift_Click(object sender, EventArgs e)
            {
            NavigationService.Navigate(new Uri("/GiftMainPivot.xaml?", UriKind.RelativeOrAbsolute));
            }

        private void money_Click(object sender, EventArgs e)
            {
            NavigationService.Navigate(new Uri("/Views/TopupWalletLayout.xaml?", UriKind.RelativeOrAbsolute));
            }


        protected override void OnNavigatedTo(NavigationEventArgs e)
            {
            //NavigationService.Navigate(new Uri(string.Format(NavigationService.Source +
            //                            "?Refresh=true&random={0}", Guid.NewGuid())));
            Constant.laodNavigationBar(TitlePanel, LayoutRoot, canvas, "Gift Main Page");
            }



        private async Task<String> loadOffersByType( )
            {
            ProgressBarManager.Show(27, "Please wait...", false, 10);
            if(string.IsNullOrEmpty(Constant.getTypesGiftsOrder()))
                {
                }
            else
                {
                JArray offer_details = (JArray)JsonConvert.DeserializeObject(Constant.getTypesGiftsOrder().ToString());
                //Constant.setGiftsOrder(offer_details.ToString());
                loadFeatureOffer(offer_details);
                }
                  
           

            var values = new List<KeyValuePair<string, string>>();
            values.Add(new KeyValuePair<string, string>("actiontype", "GetAllofferTypes"));
            values.Add(new KeyValuePair<string, string>("type", ""));
            values.Add(new KeyValuePair<string, string>("api_version", Constant.API_VERSION));

            values.Add(new KeyValuePair<string, string>("api", "true"));
            FormUrlEncodedContent values1 = new FormUrlEncodedContent(values);
            Task<string> response = Constant.postParamsAndgetResponse(Constant.B2C_URL, values1);


            string result = await response;
            if(string.IsNullOrEmpty(result))
                {
                NavigationService.Navigate(new Uri("/Views/NoConnection.xaml?", UriKind.Relative));
                }
            else
                {
                var dict = (JObject)JsonConvert.DeserializeObject(result);
                string status = dict["status"].ToString();

                if(status.Equals("success"))
                    {

                    var description = dict["description"];

                    JArray offer_details = (JArray)JsonConvert.DeserializeObject(description["offer_details"].ToString());
                    Constant.setTypesGiftsOrder(offer_details.ToString());
                    loadFeatureOffer(offer_details);
                    }
                }

            ProgressBarManager.Hide(27);


            return "";
            }

        private async void thisPivot_SelectionChanged(object sender, SelectionChangedEventArgs e)
            {
            System.Diagnostics.Debug.WriteLine(mainGiftPivot.SelectedIndex);

            switch(mainGiftPivot.SelectedIndex)
                {
                case 0:
                  await loadOffersByType();
                    break;
                case 1:
                   // loadNearByGifts(1);
                    loadNearByGifts(2);
                   
                    break;
                case 2: loadOffersByCategories();
                    break;  
                case 3: loadMyGifts();
                    break;
                case 4: loadLikedGifts();
                    break;
                
                }
            }

        private void loadLikedGifts()
            {
         
            try
                {
                //ProgressBarManager.Show(27, "Please wait...", false, 10);
                myLikedGiftsListBox.Items.Clear();
                GiftDataHelper gdh=new GiftDataHelper();

                List<GiftsTable> gifts=   gdh.getAllLikedFreeBies();
                if(gifts.Count!=0){
                emptyLikeViewPanel.Visibility=Visibility.Collapsed;
                myLikedGiftsListBox.Visibility=Visibility.Visible;
                foreach(GiftsTable gift in gifts)
                    {
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    GiftListLayout giftLayout=new GiftListLayout();

                    if(gift.freebieLike==0)
                        {
                        giftLayout.imgLike.Source=new BitmapImage(new Uri("/Images/ic_unlike.png", UriKind.RelativeOrAbsolute));
                        }
                    else
                        {
                        giftLayout.imgLike.Source=new BitmapImage(new Uri("/Images/ic_like.png", UriKind.RelativeOrAbsolute));
                        }
                    //  giftLayout.likeRadioButton.IsChecked=gift.freebieLike!=0;

                    giftLayout.imgLike.MouseEnter+=(a, b) =>
                    {
                        if(gift.freebieLike==0)
                            {
                            giftLayout.imgLike.Source=new BitmapImage(new Uri("/Images/ic_like.png", UriKind.RelativeOrAbsolute));
                            updateLikeDealById(gift.freebieOfferID, gdh, 1);
                            }
                        else
                            {
                            giftLayout.imgLike.Source=new BitmapImage(new Uri("/Images/ic_unlike.png", UriKind.RelativeOrAbsolute));
                            updateLikeDealById(gift.freebieOfferID, gdh, 0);
                            }


                    };
                    giftLayout.offerImage.Source=new BitmapImage(new Uri(gift.freebieURL));
                    giftLayout.offerImageLogo.Source=new BitmapImage(new Uri(gift.freebieLogoURL));
                    giftLayout.offerDealname.Text=gift.freebieDealName;
                    giftLayout.offerText.Text=gift.freebieShortDesc;
                    double distance=Constant.distance(Constant.LoadPersistent<double>(Constant.SETTINGS_USER_LATTITUDE_DOUBLE), Constant.LoadPersistent<double>(Constant.SETTINGS_USER_LONGITUDE_DOUBLE), gift.freebieLat,
                        gift.freebieLng);
                    if(distance<1)
                        {
                        giftLayout.textLocation.Text=" "+gift.freebieArea+" : <1 KM";
                        }
                    else
                        {
                        giftLayout.textLocation.Text=" "+gift.freebieArea+" : "+distance.ToString("0.00")+" KM";
                        }
                    giftLayout.expiredGiftGrid.Visibility=Visibility.Collapsed;




                    giftLayout.textCoin.Text=gift.freebieMinAmount.ToString();

                    giftLayout.mainPanel.Tap+=(a, b) =>
                    {
                        (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri("/Views/GiftDetailsPage.xaml?offerId="+gift.freebieOfferID+"&fromMyDeal=0", UriKind.RelativeOrAbsolute));
                    };

                    if(gift.by_voucher.Equals(0))
                        {
                        giftLayout.rupeePanel.Visibility=Visibility.Collapsed;

                        }
                    else
                        {
                        giftLayout.textRupee.Text=gift.offer_price.ToString();
                        giftLayout.rupeePanel.Visibility=Visibility.Visible;

                        }
                    myLikedGiftsListBox.Items.Add(giftLayout);
                    }
                }else{
                emptyLikeViewPanel.Visibility=Visibility.Visible;
                myLikedGiftsListBox.Visibility=Visibility.Collapsed;
                    }
               
                }
            catch(Exception exc)
                {
                }
            //ProgressBarManager.Hide(27);
            }

        private async void loadMyGifts()
            {
                
                ProgressBarManager.Show(27, "Please wait...", false, 10);

                var values = new List<KeyValuePair<string, string>>();
                values.Add(new KeyValuePair<string, string>("actiontype", "get_my_deal"));
                values.Add(new KeyValuePair<string, string>("freebie", "true"));
                values.Add(new KeyValuePair<string, string>("api_version", Constant.API_VERSION));

                values.Add(new KeyValuePair<string, string>("api", "true"));
                FormUrlEncodedContent values1 = new FormUrlEncodedContent(values);
                Task<string> response = Constant.postParamsAndgetResponse(Constant.B2C_URL, values1);


                string result = await response;
                if(string.IsNullOrEmpty(result))
                    {
                    NavigationService.Navigate(new Uri("/Views/NoConnection.xaml?", UriKind.Relative));
                    }
                else
                    {
                    var dict = (JObject)JsonConvert.DeserializeObject(result);
                    string status = dict["status"].ToString();

                    if(status.Equals("success"))
                        {

                        var description = dict["description"];

                        JArray offer_details = (JArray)JsonConvert.DeserializeObject(dict["description"].ToString());
                        loadMyDeals(offer_details);
                        }
                    }

                ProgressBarManager.Hide(27);
                }
            

        private void loadMyDeals(JArray offer_details)
            {
            if(offer_details.Count!=0)
                {
                emptyViewPanel.Visibility=Visibility.Collapsed;
                myGiftsListBox.Visibility=Visibility.Visible;
                try
                    {
                    myGiftsListBox.Items.Clear();
                    foreach(JObject jObj in offer_details)
                        {
                        GC.Collect();
                        GC.WaitForPendingFinalizers();
                        GiftListLayout giftLayout=new GiftListLayout();
                        if(!String.IsNullOrEmpty(jObj["img_url"].ToString()))
                            giftLayout.offerImage.Source=new BitmapImage(new Uri(jObj["img_url"].ToString()));



                        if(!String.IsNullOrEmpty(jObj["logo_url"].ToString()))
                            giftLayout.offerImageLogo.Source=new BitmapImage(new Uri(jObj["logo_url"].ToString()));

                        giftLayout.imgLike.Visibility=Visibility.Collapsed;
                        giftLayout.offerDealname.Text=jObj["deal_name"].ToString();
                        giftLayout.textLocation.Visibility=Visibility.Collapsed;
                        giftLayout.offerText.Text=jObj["offer_desc"].ToString();
                        if(jObj["status"].ToString().Equals("1"))
                            {
                            giftLayout.expiredGiftGrid.Visibility=Visibility.Collapsed;
                            giftLayout.textExpired.Visibility=Visibility.Collapsed;
                            giftLayout.textExpired.Text="";
                            }
                        else if(jObj["status"].ToString().Equals("2"))
                            {
                            giftLayout.expiredGiftGrid.Visibility=Visibility.Visible;
                            giftLayout.textExpired.Visibility=Visibility.Visible;
                            giftLayout.textExpired.Text=jObj["coupon_status"].ToString();
                            }
                        else if(jObj["status"].ToString().Equals("3"))
                            {
                            giftLayout.expiredGiftGrid.Visibility=Visibility.Visible;
                            giftLayout.textExpired.Visibility=Visibility.Visible;
                            giftLayout.textExpired.Text=jObj["coupon_status"].ToString();
                            }
                        giftLayout.locationPanel.Visibility=Visibility.Collapsed;
                        giftLayout.textCoin.Text=jObj["min_amount"].ToString();
                        if(jObj["by_voucher"].ToString().Equals("0"))
                            {
                            giftLayout.rupeePanel.Visibility=Visibility.Collapsed;

                            }
                        else
                            {
                            giftLayout.textRupee.Text=jObj["offer_price"].ToString();
                            giftLayout.rupeePanel.Visibility=Visibility.Visible;

                            }
                        giftLayout.mainPanel.Tap+=(a, b) =>
                        {
                            (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri("/Views/GiftDetailsPage.xaml?offerId="+jObj["offer_id"].ToString()+"&fromMyDeal=1&offer=1111", UriKind.RelativeOrAbsolute));
                            myOfferDetails=jObj.ToString();
/*
                            (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri("/Views/GiftDetailsPage.xaml?offer="+jObj.ToString()+"&fromMyDeal=1", UriKind.RelativeOrAbsolute));*/
                        };

                        myGiftsListBox.Items.Add(giftLayout);

                        }

                    }
                catch(JsonException je)
                    {
                    }
                }
            else
                {
                myGiftsListBox.Visibility=Visibility.Collapsed;
                emptyViewPanel.Visibility=Visibility.Visible;
                }
            }

        private async void updateLikeDealById(int offerId,GiftDataHelper gdh,int setLike)
            {

            
            var values = new List<KeyValuePair<string, string>>();
            values.Add(new KeyValuePair<string, string>("actiontype", "MyLikes"));
            values.Add(new KeyValuePair<string, string>("id", ""+offerId));
            values.Add(new KeyValuePair<string, string>("api_version", Constant.API_VERSION));

            values.Add(new KeyValuePair<string, string>("api", "true"));
            FormUrlEncodedContent values1 = new FormUrlEncodedContent(values);
            Task<string> response = Constant.postParamsAndgetResponse(Constant.B2C_URL, values1);


            string result = await response;
            if(string.IsNullOrEmpty(result))
                {
                NavigationService.Navigate(new Uri("/Views/NoConnection.xaml?", UriKind.Relative));
                }
            else
                {
                var dict = (JObject)JsonConvert.DeserializeObject(result);
                string status = dict["status"].ToString();

                if(status.Equals("success"))
                    {
                    gdh.updateGiftForLikeByOfferId(offerId, setLike);
                    //var description = dict["description"];

                    //JArray offer_details = (JArray)JsonConvert.DeserializeObject(dict["description"].ToString());
                    //loadMyDeals(offer_details);

                   
                    }
                }

           
            }

        private  void loadNearByGifts(int dis)
            {
            ProgressBarManager.Show(29, "Please wait...", false, 10);

            GiftDataHelper gdh=new GiftDataHelper();

            List<GiftsTable> gifts=    gdh.ReadNearByGift(Constant.LoadPersistent<double>(Constant.SETTINGS_USER_LATTITUDE_DOUBLE), Constant.LoadPersistent<double>(Constant.SETTINGS_USER_LONGITUDE_DOUBLE), dis);

            foreach(GiftsTable gift in gifts)
                {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GiftListLayout giftLayout=new GiftListLayout();

                if(gift.freebieLike==0)
                    {
                    giftLayout.imgLike.Source=new BitmapImage(new Uri("/Images/ic_unlike.png", UriKind.RelativeOrAbsolute));
                    }
                else
                    {
                    giftLayout.imgLike.Source=new BitmapImage(new Uri("/Images/ic_like.png",UriKind.RelativeOrAbsolute));
                    }
              //  giftLayout.likeRadioButton.IsChecked=gift.freebieLike!=0;

                giftLayout.imgLike.MouseEnter+=(a, b) =>
                {
                if(gift.freebieLike==0)
                    {
                    giftLayout.imgLike.Source=new BitmapImage(new Uri("/Images/ic_like.png", UriKind.RelativeOrAbsolute));
                      updateLikeDealById(gift.freebieOfferID,gdh,1);
                    }
                else
                    {
                    giftLayout.imgLike.Source=new BitmapImage(new Uri("/Images/ic_unlike.png", UriKind.RelativeOrAbsolute));
                      updateLikeDealById(gift.freebieOfferID,gdh,0);
                    }
              
               
                };
                if(string.IsNullOrEmpty(gift.freebieURL))
                    {
                    }
                else
                    {
                    giftLayout.offerImage.Source=new BitmapImage(new Uri(gift.freebieURL, UriKind.RelativeOrAbsolute));
                    }

                if(string.IsNullOrEmpty(gift.freebieLogoURL))
                    {
                    }
                else
                    {
                    giftLayout.offerImageLogo.Source=new BitmapImage(new Uri(gift.freebieLogoURL, UriKind.RelativeOrAbsolute));
                    }
                giftLayout.offerDealname.Text=gift.freebieDealName;
                giftLayout.offerText.Text=gift.freebieShortDesc;
                double distance=Constant.distance(Constant.LoadPersistent<double>(Constant.SETTINGS_USER_LATTITUDE_DOUBLE), Constant.LoadPersistent<double>(Constant.SETTINGS_USER_LONGITUDE_DOUBLE), gift.freebieLat,
                    gift.freebieLng);
                if(distance<1){
                      giftLayout.textLocation.Text=" "+gift.freebieArea+" : <1 KM" ;
                    }else{
                    giftLayout.textLocation.Text=" "+gift.freebieArea+" : "+distance.ToString("0.00")+" KM";
                    }
                giftLayout.expiredGiftGrid.Visibility=Visibility.Collapsed;


              

                giftLayout.textCoin.Text=gift.freebieMinAmount.ToString();

                giftLayout.mainPanel.Tap+=(a, b) =>
                {
                (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri("/Views/GiftDetailsPage.xaml?offerId="+gift.freebieOfferID+"&fromMyDeal=0", UriKind.RelativeOrAbsolute));
                };

                if(gift.by_voucher.Equals(0))
                    {
                    giftLayout.rupeePanel.Visibility=Visibility.Collapsed;

                    }
                else
                    {
                    giftLayout.textRupee.Text=gift.offer_price.ToString();
                    giftLayout.rupeePanel.Visibility=Visibility.Visible;

                    }
             
                nearYouListBox.Items.Add(giftLayout);
                }

            ProgressBarManager.Hide(29);
            }

        private async void loadOffersByCategories()
            {
            ProgressBarManager.Show(41, "Please wait...", false, 10);
            try
                {

                if(string.IsNullOrEmpty(Constant.getCategoriesGiftsOrder()))
                    {
                    }
                else
                    {
                    JArray offer_details = (JArray)JsonConvert.DeserializeObject(Constant.getCategoriesGiftsOrder().ToString());
                    //Constant.setGiftsOrder(offer_details.ToString());
                  //  loadCategoryOffer(offer_details);
                    }

            

                var values = new List<KeyValuePair<string, string>>();
                values.Add(new KeyValuePair<string, string>("actiontype", "GetAllOfferCategory"));
                values.Add(new KeyValuePair<string, string>("type", ""));
                values.Add(new KeyValuePair<string, string>("next", "0"));
                values.Add(new KeyValuePair<string, string>("api_version", Constant.API_VERSION));
                values.Add(new KeyValuePair<string, string>("api", "true"));
                FormUrlEncodedContent values1 = new FormUrlEncodedContent(values);
                Task<string> response = Constant.postParamsAndgetResponse(Constant.B2C_URL, values1);


                string result = await response;
                if(string.IsNullOrEmpty(result))
                    {
                    NavigationService.Navigate(new Uri("/Views/NoConnection.xaml?", UriKind.Relative));
                    }
                else
                    {
                    var dict = (JObject)JsonConvert.DeserializeObject(result);
                    string status = dict["status"].ToString();

                    if(status.Equals("success"))
                        {

                        var description = dict["description"];

                        JArray offer_details = (JArray)JsonConvert.DeserializeObject(dict["description"].ToString());
                        Constant.setCategoriesGiftsOrder(offer_details.ToString());
                        loadCategoryOffer(offer_details);
                        }
                    }

              
                }
            catch(JsonException je)
                {
                }
            catch(Exception ex)
                {
                }
            ProgressBarManager.Hide(41);
            }

        private void loadCategoryOffer(JArray offer_details)
            {
            offerPanelCategory.Children.Clear();

            GiftDataHelper gdh=new GiftDataHelper();


            foreach(JObject jObj in offer_details)
                {

                List<GiftsTable> lst=   gdh.ReadGiftByOfferIds(jObj["details"].ToString());
                GiftCellContainer wpc1=new GiftCellContainer();
                String catName=jObj["name"].ToString();
                String catId=jObj["id"].ToString();
                wpc1.seeAllPanel.Tap+=(a, b) =>
                {

                    if(catName.Contains("&"))
                        {
                      catName=  catName.Replace("&","AND");
                        }

                    String navigationUrl="/Views/AllOffersByType.xaml?catName="+catName+"&catId="+catId+"&catType=1";
              //  FormUrlEncodedContent values1 = new FormUrlEncodedContent(navigationUrl);
                    (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri(navigationUrl, UriKind.RelativeOrAbsolute));
                };
                wpc1.textOfferType.Text=jObj["name"].ToString();
                // offerPanel.Children.Clear();// wpc1.listBoxOffer.Items.Clear();
                foreach(GiftsTable gifts in lst)
                    {

                    GiftCells fgc=new GiftCells();
                    fgc.imageGift.Source= new BitmapImage(new Uri(gifts.freebieURL));
                    fgc.textOffer.Text=gifts.freebieShortDesc;
                    fgc.textDealName.Text=gifts.freebieDealName;
                    fgc.textCoin.Text=gifts.freebieMinAmount.ToString();
                    fgc.textLocation.Text=gifts.freebieArea;
                    fgc.textDealId.Text=gifts.freebieOfferID.ToString();
                    fgc.textRupee.Text=gifts.offer_price.ToString();
                    // fgc.offerId.Visibility=Visibility.Collapsed;
                    if(gifts.by_voucher.Equals(0))
                        {
                        fgc.rupeePanel.Visibility=Visibility.Collapsed;
                        fgc.locationPanel.Visibility=Visibility.Visible;
                        }
                    else
                        {
                        fgc.locationPanel.Visibility=Visibility.Collapsed;
                        fgc.rupeePanel.Visibility=Visibility.Visible;
                       
                        }

                    LocationTable locationTable=   gdh.getNearestLocationFromLocationTable(Constant.LoadPersistent<double>(Constant.SETTINGS_USER_LATTITUDE_DOUBLE), Constant.LoadPersistent<double>(Constant.SETTINGS_USER_LONGITUDE_DOUBLE), gifts.freebieOfferID);
                    if(locationTable!=null)
                        {
                        fgc.textLocation.Text=locationTable.locationArea;
                        }
                    else
                        {
                        fgc.textLocation.Text=gifts.freebieArea;
                        }



                    wpc1.listBoxOffer.Items.Add(fgc);


                    }

                try
                    {
                    offerPanelCategory.Children.Add(wpc1);
                    }
                catch(Exception exc)
                    {
                    System.Diagnostics.Debug.WriteLine("srgdrh");
                    }

                lst.Clear();
                }

            }

        private void loadFeatureOffer(JArray offer_details)
            {
            offerPanel.Children.Clear();
            infoPivot.Items.Clear();
            GiftDataHelper gdh=new GiftDataHelper();


            foreach(JObject jObj in offer_details)
                {
                if(jObj["featured"].ToString().Equals("1"))
                    {
                    List<GiftsTable> lst=   gdh.ReadGiftByOfferIds(jObj["details"].ToString());
                    PivotItem pivotItem=null;
                    foreach(GiftsTable gifts in lst)
                        {
                        FeatureGiftControl fgc=new FeatureGiftControl();
                        fgc.imageOffer.Source= new BitmapImage(new Uri(gifts.freebieURL));
                        fgc.textOffer.Text=gifts.freebieShortDesc;
                        fgc.textDealName.Text=gifts.freebieDealName;
                        fgc.Tap+=(a, b) =>
                        {
                        (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri("/Views/GiftDetailsPage.xaml?offerId="+gifts.freebieOfferID+"&fromMyDeal=0", UriKind.RelativeOrAbsolute));
                        };
                        pivotItem = new PivotItem();
                        pivotItem.Content = fgc;
                        infoPivot.Items.Add(pivotItem);
                        }

                    lst.Clear();
                    }
                else
                    {
                    List<GiftsTable> lst=   gdh.ReadGiftByOfferIds(jObj["details"].ToString());
                    GiftCellContainer wpc1=new GiftCellContainer();
                    wpc1.textOfferType.Text=jObj["name"].ToString();

                    String catName=jObj["name"].ToString();
                    String catId=jObj["id"].ToString();
                    wpc1.seeAllPanel.Tap+=(a, b) =>
                    {
                    (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri("/Views/AllOffersByType.xaml?catName="+catName+"&catId="+catId+"&catType=0", UriKind.RelativeOrAbsolute));
                    };

                   // offerPanel.Children.Clear();// wpc1.listBoxOffer.Items.Clear();
                    foreach(GiftsTable gifts in lst)
                        {

                        GiftCells fgc=new GiftCells();
                        fgc.imageGift.Source= new BitmapImage(new Uri(gifts.freebieURL));
                        fgc.textOffer.Text=gifts.freebieShortDesc;
                        fgc.textDealName.Text=gifts.freebieDealName;
                        fgc.textCoin.Text=gifts.freebieMinAmount.ToString();
                        fgc.textRupee.Text=gifts.offer_price.ToString();
                        fgc.textDealId.Text=gifts.freebieOfferID.ToString();

                    LocationTable locationTable=   gdh.getNearestLocationFromLocationTable(Constant.LoadPersistent<double>(Constant.SETTINGS_USER_LATTITUDE_DOUBLE), Constant.LoadPersistent<double>(Constant.SETTINGS_USER_LONGITUDE_DOUBLE), gifts.freebieOfferID);
                    if(locationTable!=null)
                        {
                        fgc.textLocation.Text=locationTable.locationArea;
                        }
                    else
                        {
                        fgc.textLocation.Text=gifts.freebieArea;
                        }


                    if(gifts.by_voucher.Equals(0))
                        {
                        fgc.rupeePanel.Visibility=Visibility.Collapsed;
                        fgc.locationPanel.Visibility=Visibility.Visible;
                        }
                    else
                        {
                        fgc.locationPanel.Visibility=Visibility.Collapsed;
                        fgc.rupeePanel.Visibility=Visibility.Visible;

                        }

                        wpc1.listBoxOffer.Items.Add(fgc);


                        }

                    try
                        {
                        offerPanel.Children.Add(wpc1);
                        }
                    catch(Exception exc)
                        {
                        System.Diagnostics.Debug.WriteLine("srgdrh");
                        }

                    lst.Clear();
                    }
                }






            }

       /* private async void mainGiftPivot_Loaded(object sender, RoutedEventArgs e)
            {
             switch(mainGiftPivot.SelectedIndex)
                {
                case 0:
                  await loadOffersByType();
                    break;
                case 1: loadNearByGifts();
                    break;
                case 2: loadOffersByCategories();
                    break;
                case 3: loadMyGifts();
                    break;
                case 4: loadLikedGifts();
                    break;
                
                }
            }*/
        }
    }