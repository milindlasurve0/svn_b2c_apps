//
//  SignInService.h
//  Pay1
//
//  Created by Annapurna on 12/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "Service.h"
@class SignInCriteria;
@class SignInResponse;
typedef void (^signInResponse)(NSError *error, SignInResponse *objSignInResponse);
@interface SignInService : Service
-(void)callSignInService:(SignInCriteria *)criteria callback:(signInResponse)callback;
@end
