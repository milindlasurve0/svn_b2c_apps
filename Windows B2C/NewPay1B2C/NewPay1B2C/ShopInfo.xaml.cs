﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Windows.Input;
using Microsoft.Phone.Tasks;
using System.Device.Location;

using Microsoft.Phone.Controls.Maps.Platform;
using Microsoft.Phone.Controls.Maps;
using System.Windows.Media;
using System.Collections.ObjectModel;


using System.Windows;

using Microsoft.Phone.Controls;


using System.Windows.Shapes;
using System.Windows.Media.Imaging;

using NewPay1B2C;

namespace NewPay1B2C
    {
    public partial class ShopInfo : PhoneApplicationPage
        {
        Location location = new Location ();
        string shopName, shopNumber, shopLattitude, shopLongitude, currentLattitude, currentLongitude;
        double doubleShopLattitude, doubleShopLongitude, doubleCurrentLattitude, doubleCurrentLongitude;
        string area_name, shopAddress;
        ProgressBarManager ProgressBarManager;
        public ShopInfo ()
            {
            InitializeComponent ();

            GoogleAnalytics.EasyTracker.GetTracker ().SendView ("ShopInfo");
            string data = PhoneApplicationService.Current.State[Constant.SHOP_DATA].ToString ();
            ProgressIndicator prog = new ProgressIndicator ();
            ProgressBarManager = new ProgressBarManager ();
            SystemTray.SetProgressIndicator (this, prog);
            ProgressBarManager.Hook (prog);

            var dict = (JObject)JsonConvert.DeserializeObject (data);
            dict["shopname"].ToString ();
            double dis = Convert.ToDouble (dict["D"].ToString ());
            if (dis < 1)
                {
                dis = dis * 1000;
                textShopDistance.Text = dis.ToString ("#.##") + " M";
                }
            else
                {
                textShopDistance.Text = dis.ToString ("#.##") + " KM";
                }



            currentLattitude = Constant.LoadPersistent<string> (Constant.SETTINGS_USER_LATTITUDE);
            currentLongitude = Constant.LoadPersistent<string> (Constant.SETTINGS_USER_LONGITUDE);
            shopLattitude = dict["latitude"].ToString ();
            shopLongitude = dict["longitude"].ToString ();



            doubleShopLattitude = Convert.ToDouble (shopLattitude);
            doubleShopLongitude = Convert.ToDouble (shopLongitude);
            doubleCurrentLattitude = Convert.ToDouble (currentLattitude);
            doubleCurrentLongitude = Convert.ToDouble (currentLongitude);


            string pin = dict["pin"].ToString ();
            area_name = dict["area_name"].ToString ();
            shopAddress = dict["address"].ToString ();
            string city_name = dict["city_name"].ToString ();
            string state_name = dict["state_name"].ToString ();
            string address = area_name + "\n" + city_name + "\n" + state_name + " - " + pin;
            shopName = dict["shopname"].ToString ().ToUpper ();
            textShopName.Text = shopName;
            textShopName1.Text = shopName;
            shopNumber = dict["mobile"].ToString ();
            textShopArea.Text = dict["area_name"].ToString ().ToUpperInvariant ();
            textShopAddress.Text = address;
            callImage.Tap += callImage_Tapped;
            locateImage.Tap += locateImage_Tapped;

            loadAllShopsOnMap (shopLattitude, shopLongitude, address);


            WebClient client = new WebClient ();
           
            client.DownloadStringCompleted += client_DownloadStringCompleted;
           
            string Url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=" + shopLattitude + "," + shopLongitude + "&sensor=true";
            client.DownloadStringAsync (new Uri (Url, UriKind.RelativeOrAbsolute));
          
            }

        private void client_DownloadStringCompleted (object sender, DownloadStringCompletedEventArgs e)
            {

            var getResult = e.Result;
            JObject parseJson = JObject.Parse (getResult);
            var getJsonres = parseJson["results"][0];
            var getJson = getJsonres["address_components"][1];
            var getJson2 = getJsonres["address_components"][2];
            var getJson3 = getJsonres["address_components"][3];
            var sub_area = getJson["long_name"];
            var area = getJson["long_name"];
            var address = getJson["long_name"];
            //Address = sub_area.ToString() + ", " + area.ToString() + ", " + address.ToString();
            shopAddress = getJsonres["formatted_address"].ToString ();

            }

        private void callImage_Tapped (object sender, System.Windows.Input.GestureEventArgs e)
            {
            PhoneCallTask phoneCallTask = new PhoneCallTask ();

            phoneCallTask.PhoneNumber = shopNumber;
            phoneCallTask.DisplayName = shopName;

            phoneCallTask.Show ();
            }

        MapLayer _routeLayer;

        internal geocodeservice.GeocodeResult[] geocodeResults;
        private void locateImage_Tapped (object sender, System.Windows.Input.GestureEventArgs e)
            {
           
            geocodeResults = new geocodeservice.GeocodeResult[2];

          
            Geocode (shopAddress, 0);
            Geocode (HomePage.currentAddress, 1);
            }
        bool isTappedFirst;
        private void Rectangle_Tapped (object sender, MouseEventArgs e)
            {
            if (!isTappedFirst)
                {
                isTappedFirst = true;
                myStoryboard.Begin ();

                }
            else
                {
                isTappedFirst = false;
                myStoryboard.Stop ();
                }

            }

        private void buttonImage_Click_Back (object sender, RoutedEventArgs e)
            {
            /*NavigationService.Navigate(new Uri("/ShopLocator.xaml?", UriKind.RelativeOrAbsolute));
            NavigationService.RemoveBackEntry();*/
            NavigationService.GoBack ();
            }

        MapLayer PinLayer1;
        private void loadAllShopsOnMap (string latiitude, string longitude, string address)
            {
            PinLayer1 = new MapLayer ();
            Pushpin MyPin = new Pushpin ();

            Border border = new Border ();
            border.BorderBrush = new SolidColorBrush (Colors.Black);
            border.Background = new SolidColorBrush (Colors.Black);
            TextBlock textblok = new TextBlock ();
            textblok.Text = address;
            border.BorderThickness = new Thickness (5);
            border.Child = textblok;
            border.Visibility = Visibility.Collapsed;
            MyPin.Content = border;
            MyPin.Tag = "FindMe";

            Uri imgUri = new Uri ("Images/location.png", UriKind.RelativeOrAbsolute);
            BitmapImage imgSourceR = new BitmapImage (imgUri);
            ImageBrush imgBrush = new ImageBrush () { ImageSource = imgSourceR };

            MyPin.Background = imgBrush;
            MyPin.Tap += pushPin_Tap;
            MyPin.Location = new Microsoft.Phone.Controls.Maps.Platform.Location ();
            MyPin.Location.Latitude = Convert.ToDouble (latiitude);
            MyPin.Tap += pushPin_Tap;
            MyPin.Location.Longitude = Convert.ToDouble (longitude);
            //MyPin.Content = address;
            PinLayer1.Children.Add (MyPin);

            sampleMap.Children.Add (PinLayer1);

            sampleMap.Center = MyPin.Location;
            sampleMap.ZoomLevel = 19;

            }

        void pushPin_Tap (object sender, System.Windows.Input.GestureEventArgs e)
            {
            var push = sender as Pushpin;

            Border brodr = (Border)push.Content;
            if (brodr.Visibility == Visibility.Visible)
                {
                brodr.Visibility = System.Windows.Visibility.Collapsed;
                }
            else
                {
                brodr.Visibility = System.Windows.Visibility.Visible;
                }

            //stop the event from going to the parent map control
            e.Handled = true;
            }

        private void Geocode (string strAddress, int waypointIndex)
            {
            geocodeservice.GeocodeServiceClient geocodeService = new geocodeservice.GeocodeServiceClient ("BasicHttpBinding_IGeocodeService");
            geocodeService.GeocodeCompleted += new EventHandler<geocodeservice.GeocodeCompletedEventArgs> (geocodeService_GeocodeCompleted);

            geocodeservice.GeocodeRequest geocodeRequest = new geocodeservice.GeocodeRequest ();
            geocodeRequest.Credentials = new geocodeservice.Credentials ();
            geocodeRequest.Credentials.ApplicationId = ((ApplicationIdCredentialsProvider)sampleMap.CredentialsProvider).ApplicationId;
            geocodeRequest.Query = strAddress;

            geocodeService.GeocodeAsync (geocodeRequest, waypointIndex);
            }

        private void geocodeService_GeocodeCompleted (object sender, geocodeservice.GeocodeCompletedEventArgs e)
            {

            int waypointIndex = System.Convert.ToInt32 (e.UserState);


            try
                {
                geocodeResults[waypointIndex] = e.Result.Results[0];
                }
            catch (Exception ex)
                {
                }

            bool doneGeocoding = true;

            foreach (geocodeservice.GeocodeResult gr in geocodeResults)
                {
                if (gr == null)
                    {
                    doneGeocoding = false;
                    }
                }

            if (doneGeocoding)
                CalculateRoute (geocodeResults);
            }




        private void routeService_CalculateRouteCompleted (object sender, routeservice.CalculateRouteCompletedEventArgs e)
            {
            if ((e.Result.ResponseSummary.StatusCode == routeservice.ResponseStatusCode.Success) & (e.Result.Result.Legs.Count != 0))
                {
                Color routeColor = Colors.Blue;
                SolidColorBrush routeBrush = new SolidColorBrush (routeColor);
                MapPolyline routeLine = new MapPolyline ();
                routeLine.Locations = new LocationCollection ();
                routeLine.Stroke = routeBrush;
                routeLine.Opacity = 0.65;
                routeLine.StrokeThickness = 5.0;

                foreach (NewPay1B2C.routeservice.Location p in e.Result.Result.RoutePath.Points)
                    {
                    routeLine.Locations.Add (new Microsoft.Phone.Controls.Maps.Platform.Location { Latitude = p.Latitude, Longitude = p.Longitude });

                    }

                MapLayer myRouteLayer = new MapLayer ();
                sampleMap.Children.Add (myRouteLayer);

                myRouteLayer.Children.Add (routeLine);

                double centerlatitude = (routeLine.Locations[0].Latitude + routeLine.Locations[routeLine.Locations.Count - 1].Latitude) / 2;
                double centerlongitude = (routeLine.Locations[0].Longitude + routeLine.Locations[routeLine.Locations.Count - 1].Longitude) / 2;
                Microsoft.Phone.Controls.Maps.Platform.Location centerloc = new Microsoft.Phone.Controls.Maps.Platform.Location ();
                centerloc.Latitude = centerlatitude;
                centerloc.Longitude = centerlongitude;
                double north, south, east, west;

                if ((routeLine.Locations[0].Latitude > 0) && (routeLine.Locations[routeLine.Locations.Count - 1].Latitude > 0))
                    {
                    north = routeLine.Locations[0].Latitude > routeLine.Locations[routeLine.Locations.Count - 1].Latitude ? routeLine.Locations[0].Latitude : routeLine.Locations[routeLine.Locations.Count - 1].Latitude;
                    south = routeLine.Locations[0].Latitude < routeLine.Locations[routeLine.Locations.Count - 1].Latitude ? routeLine.Locations[0].Latitude : routeLine.Locations[routeLine.Locations.Count - 1].Latitude;
                    }
                else
                    {
                    north = routeLine.Locations[0].Latitude < routeLine.Locations[routeLine.Locations.Count - 1].Latitude ? routeLine.Locations[0].Latitude : routeLine.Locations[routeLine.Locations.Count - 1].Latitude;
                    south = routeLine.Locations[0].Latitude > routeLine.Locations[routeLine.Locations.Count - 1].Latitude ? routeLine.Locations[0].Latitude : routeLine.Locations[routeLine.Locations.Count - 1].Latitude;

                    }
                if ((routeLine.Locations[0].Longitude < 0) && (routeLine.Locations[routeLine.Locations.Count - 1].Longitude < 0))
                    {
                    west = routeLine.Locations[0].Longitude < routeLine.Locations[routeLine.Locations.Count - 1].Longitude ? routeLine.Locations[0].Longitude : routeLine.Locations[routeLine.Locations.Count - 1].Longitude;
                    east = routeLine.Locations[0].Longitude > routeLine.Locations[routeLine.Locations.Count - 1].Longitude ? routeLine.Locations[0].Longitude : routeLine.Locations[routeLine.Locations.Count - 1].Longitude;
                    }
                else
                    {
                    west = routeLine.Locations[0].Longitude > routeLine.Locations[routeLine.Locations.Count - 1].Longitude ? routeLine.Locations[0].Longitude : routeLine.Locations[routeLine.Locations.Count - 1].Longitude;
                    east = routeLine.Locations[0].Longitude < routeLine.Locations[routeLine.Locations.Count - 1].Longitude ? routeLine.Locations[0].Longitude : routeLine.Locations[routeLine.Locations.Count - 1].Longitude;
                    }
                // For each geocode result (which are the waypoints of the route), draw a dot on the map.
                foreach (geocodeservice.GeocodeResult gr in geocodeResults)
                    {
                    Ellipse point = new Ellipse ();
                    point.Width = 10;
                    point.Height = 10;
                    point.Fill = new SolidColorBrush (Colors.Red);
                    point.Opacity = 0.65;
                    location.Latitude = gr.Locations[0].Latitude;
                    location.Longitude = gr.Locations[0].Longitude;
                    /*  MapLayer.SetPosition(point, location);
                      MapLayer.SetPositionOrigin(point, PositionOrigin.Center);

                      // Add the drawn point to the route layer.                    
                      myRouteLayer.Children.Add(point);*/

                    loadAllShopsOnMap (location.Latitude.ToString (), location.Longitude.ToString (), gr.Address.FormattedAddress.ToString ());
                    }

                // Set the map view using the rectangle which bounds the rendered route.
                //map1.SetView(rect);
                double latitude = 0.0;
                double longtitude = 0.0;
                sampleMap.SetView (location, 19);
                sampleMap.Center = location;
                //  ProgressBarManager.Hide(21);
                GeoCoordinate CurrentLocCoordinate = new System.Device.Location.GeoCoordinate (latitude, longtitude);
                }
            }


        private void CalculateRoute (geocodeservice.GeocodeResult[] results)
            {
            // Create the service variable and set the callback method using the CalculateRouteCompleted property.
            routeservice.RouteServiceClient routeService = new routeservice.RouteServiceClient ("BasicHttpBinding_IRouteService");
            routeService.CalculateRouteCompleted += new EventHandler<routeservice.CalculateRouteCompletedEventArgs> (routeService_CalculateRouteCompleted);

            // Set the token.
            routeservice.RouteRequest routeRequest = new routeservice.RouteRequest ();
            routeRequest.Credentials = new NewPay1B2C.routeservice.Credentials ();
            routeRequest.Credentials.ApplicationId = "AgeNp7j94vCR-iEa33Xufv-3pxfDlRoV-zTnci3AKFZuc2MrT7i-hAU81dbH7DtC";

            // Return the route points so the route can be drawn.
            routeRequest.Options = new routeservice.RouteOptions ();
            routeRequest.Options.RoutePathType = routeservice.RoutePathType.Points;

            // Set the waypoints of the route to be calculated using the Geocode Service results stored in the geocodeResults variable.
            routeRequest.Waypoints = new System.Collections.ObjectModel.ObservableCollection<routeservice.Waypoint> ();
            foreach (geocodeservice.GeocodeResult result in results)
                {
                routeRequest.Waypoints.Add (GeocodeResultToWaypoint (result));
                }

            // Make the CalculateRoute asnychronous request.
            routeService.CalculateRouteAsync (routeRequest);
            }
        private routeservice.Waypoint GeocodeResultToWaypoint (geocodeservice.GeocodeResult result)
            {
            routeservice.Waypoint waypoint = new routeservice.Waypoint ();
            waypoint.Description = result.DisplayName;
            waypoint.Location = new NewPay1B2C.routeservice.Location ();
            waypoint.Location.Latitude = result.Locations[0].Latitude;
            waypoint.Location.Longitude = result.Locations[0].Longitude;
            return waypoint;
            }



        }
    }