package com.pay1.adapterhandler;

import java.util.ArrayList;
import java.util.WeakHashMap;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.pay1.DealOfferActivity;
import com.pay1.OrderSummaryActivity;
import com.pay1.OrderSummaryForLowBalActivity;
import com.pay1.R;
import com.pay1.constants.Constants;
import com.pay1.utilities.Utility;

public class DealOfferExpandedAdapter extends BaseExpandableListAdapter {

	private Context context;
	private ArrayList<WeakHashMap<String, String>> data;
	ViewHolder viewHolder;
	static String str = "1";
	private Typeface Reguler, Narrow, Narrow_thin;

	public DealOfferExpandedAdapter(Context context,
			ArrayList<WeakHashMap<String, String>> data) {
		this.context = context;
		this.data = data;

		Reguler = Typeface.createFromAsset(context.getAssets(),
				"EncodeSansNormal-400-Regular.ttf");

		Narrow = Typeface.createFromAsset(context.getAssets(),
				"EncodeSansNarrow-500-Medium.ttf");

		Narrow_thin = Typeface.createFromAsset(context.getAssets(),
				"EncodeSansNarrow-100-Thin.ttf");
	}

	@Override
	public Object getChild(int groupPosition, int childPosititon) {
		return null;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public View getChildView(final int groupPosition, final int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {

		// final String childText = (String) getChild(groupPosition,
		// childPosition);

		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this.context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(
					R.layout.deal_offer_content_adapter, null);
		}

		TextView textView_Title = (TextView) convertView
				.findViewById(R.id.textView_Title);
		textView_Title.setTypeface(Reguler);
		viewHolder.spinner_Quantity = (Spinner) convertView
				.findViewById(R.id.spinner_Quantity);
		viewHolder.spinner_Quantity
				.setAdapter(new DealQuantityAdapter(context));
		viewHolder.spinner_Quantity
				.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView<?> parent,
							View view, int position, long id) {
						// TODO Auto-generated method stub
						try {

							str = String.valueOf(parent
									.getItemAtPosition(position));
							// int i = viewHolder.spinner_Quantity
							// .getSelectedItemPosition();
							// System.out.println(str);
						} catch (Exception e) {
						}
					}

					@Override
					public void onNothingSelected(AdapterView<?> parent) {
						// TODO Auto-generated method stub

					}
				});
		ImageView imageView_Ok = (ImageView) convertView
				.findViewById(R.id.imageView_Ok);
		imageView_Ok.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				try {
					// String str = String.valueOf(viewHolder.spinner_Quantity
					// .getSelectedItem());
					// int i = viewHolder.spinner_Quantity
					// .getSelectedItem();

					if (str != null) {
						WeakHashMap<String, String> map = new WeakHashMap<String, String>();
						map = data.get(groupPosition);

						double rechargeAmount, balance;
						try {

							rechargeAmount = Integer.parseInt(map
									.get(DealOfferActivity.OFFER_PRICE))
									* Integer.parseInt(str);
							balance = Double.parseDouble(Utility
									.getBalance(context,
											Constants.SHAREDPREFERENCE_BALANCE));
						} catch (Exception e) {
							rechargeAmount = 0;
							balance = 0;
						}
						if (rechargeAmount >= balance) {
							Intent orderIntent = new Intent(context,
									OrderSummaryForLowBalActivity.class);
							orderIntent.putExtra("DEAL_ID",
									map.get(DealOfferActivity.DEAL_ID));
							orderIntent.putExtra("DEAL_NAME",
									map.get(DealOfferActivity.DEAL_NAME));
							orderIntent.putExtra("OFFER_ID",
									map.get(DealOfferActivity.OFFER_ID));
							orderIntent.putExtra("OFFER_NAME",
									map.get(DealOfferActivity.SHORT_DESC));
							orderIntent.putExtra("COUPON_ID", "");
							orderIntent.putExtra("AMOUNT",
									map.get(DealOfferActivity.OFFER_PRICE));
							orderIntent.putExtra("QUANTITY", str);

							orderIntent.putExtra(Constants.RECHARGE_FOR,
									Constants.SUBSCRIBE_DEAL);
							context.startActivity(orderIntent);

						} else {
							Intent orderIntent = new Intent(context,
									OrderSummaryActivity.class);
							orderIntent.putExtra("DEAL_ID",
									map.get(DealOfferActivity.DEAL_ID));
							orderIntent.putExtra("DEAL_NAME",
									map.get(DealOfferActivity.DEAL_NAME));
							orderIntent.putExtra("OFFER_ID",
									map.get(DealOfferActivity.OFFER_ID));
							orderIntent.putExtra("OFFER_NAME",
									map.get(DealOfferActivity.SHORT_DESC));
							orderIntent.putExtra("COUPON_ID", "");
							orderIntent.putExtra("AMOUNT",
									map.get(DealOfferActivity.OFFER_PRICE));
							orderIntent.putExtra("QUANTITY", str);

							orderIntent.putExtra(Constants.RECHARGE_FOR,
									Constants.SUBSCRIBE_DEAL);
							context.startActivity(orderIntent);
						}
					}
				} catch (Exception e) {
					WeakHashMap<String, String> map = new WeakHashMap<String, String>();
					map = data.get(groupPosition);
					double rechargeAmount, balance;
					try {

						rechargeAmount = Integer.parseInt(map
								.get(DealOfferActivity.OFFER_PRICE))
								* Integer.parseInt(str);
						balance = Double.parseDouble(Utility.getBalance(
								context, Constants.SHAREDPREFERENCE_BALANCE));
					} catch (Exception ex) {
						rechargeAmount = 0;
						balance = 0;
					}
					if (rechargeAmount >= balance) {
						Intent orderIntent = new Intent(context,
								OrderSummaryForLowBalActivity.class);
						orderIntent.putExtra("DEAL_ID",
								map.get(DealOfferActivity.DEAL_ID));
						orderIntent.putExtra("DEAL_NAME",
								map.get(DealOfferActivity.DEAL_NAME));
						orderIntent.putExtra("OFFER_ID",
								map.get(DealOfferActivity.OFFER_ID));
						orderIntent.putExtra("OFFER_NAME",
								map.get(DealOfferActivity.OFFER_NAME));
						orderIntent.putExtra("COUPON_ID", "");
						orderIntent.putExtra("AMOUNT",
								map.get(DealOfferActivity.OFFER_PRICE));
						orderIntent.putExtra("QUANTITY", "1");

						orderIntent.putExtra(Constants.RECHARGE_FOR,
								Constants.SUBSCRIBE_DEAL);
						context.startActivity(orderIntent);

					} else {
						Intent orderIntent = new Intent(context,
								OrderSummaryActivity.class);
						orderIntent.putExtra("DEAL_ID",
								map.get(DealOfferActivity.DEAL_ID));
						orderIntent.putExtra("DEAL_NAME",
								map.get(DealOfferActivity.DEAL_NAME));
						orderIntent.putExtra("OFFER_ID",
								map.get(DealOfferActivity.OFFER_ID));
						orderIntent.putExtra("OFFER_NAME",
								map.get(DealOfferActivity.OFFER_NAME));
						orderIntent.putExtra("COUPON_ID", "");
						orderIntent.putExtra("AMOUNT",
								map.get(DealOfferActivity.OFFER_PRICE));
						orderIntent.putExtra("QUANTITY", "1");

						orderIntent.putExtra(Constants.RECHARGE_FOR,
								Constants.SUBSCRIBE_DEAL);
						context.startActivity(orderIntent);
					}
				}
			}
		});

		// final Animation animation = AnimationUtils.loadAnimation(context,
		// R.anim.linear);
		//
		// animation.setAnimationListener(new AnimationListener() {
		//
		// public void onAnimationStart(Animation animation) {
		// }
		//
		// public void onAnimationRepeat(Animation animation) {
		// }
		//
		// public void onAnimationEnd(Animation animation) {
		// convertView.clearAnimation();
		// }
		// });
		//
		// convertView.startAnimation(animation);

		// Animation animation1 = AnimationUtils.loadAnimation(context,
		// R.anim.rotate);
		//
		// animation1.setAnimationListener(new AnimationListener() {
		//
		// public void onAnimationStart(Animation animation) {
		// }
		//
		// public void onAnimationRepeat(Animation animation) {
		// }
		//
		// public void onAnimationEnd(Animation animation) {
		// viewHolder.imageView_Details.clearAnimation();
		// }
		// });
		//
		// viewHolder.imageView_Details.setAnimation(animation1);

		return convertView;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return 1;
	}

	@Override
	public Object getGroup(int groupPosition) {
		return this.data.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		return this.data.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {

		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.deal_offer_adapter, null);
			viewHolder = new ViewHolder();
			viewHolder.textView_OfferName = (TextView) convertView
					.findViewById(R.id.textView_OfferName);
			viewHolder.textView_OfferName.setTypeface(Reguler);
			viewHolder.textView_Amount = (TextView) convertView
					.findViewById(R.id.textView_Amount);
			viewHolder.textView_Amount.setTypeface(Reguler);
			viewHolder.textView_OfferAmount = (TextView) convertView
					.findViewById(R.id.textView_OfferAmount);
			viewHolder.textView_OfferAmount.setTypeface(Reguler);
			viewHolder.textView_Sold = (TextView) convertView
					.findViewById(R.id.textView_Sold);
			viewHolder.textView_Sold.setTypeface(Reguler);
			viewHolder.textView_Discount = (TextView) convertView
					.findViewById(R.id.textView_Discount);
			viewHolder.textView_Discount.setTypeface(Reguler);
			viewHolder.imageView_Details = (ImageView) convertView
					.findViewById(R.id.imageView_Details);
			convertView.setTag(viewHolder);

		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		WeakHashMap<String, String> map = new WeakHashMap<String, String>();
		map = data.get(groupPosition);

		viewHolder.textView_OfferName.setText(map
				.get(DealOfferActivity.OFFER_NAME)
				+ " : "
				+ map.get(DealOfferActivity.SHORT_DESC));
		viewHolder.textView_Amount.setText("Rs. "
				+ map.get(DealOfferActivity.ACTUAL_PRICE));
		viewHolder.textView_Amount.setPaintFlags(viewHolder.textView_Amount
				.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
		viewHolder.textView_OfferAmount.setText("Rs. "
				+ map.get(DealOfferActivity.OFFER_PRICE));
		viewHolder.textView_Sold.setText(map.get(DealOfferActivity.STOCK_SOLD)
				+ " Bought");
		viewHolder.textView_Discount.setText(map
				.get(DealOfferActivity.DISCOUNT) + "% Discount");

		double actual_amt = 0;
		try {
			actual_amt = Double.parseDouble(map
					.get(DealOfferActivity.ACTUAL_PRICE));
		} catch (Exception e) {
			actual_amt = 0;
		}

		if (actual_amt == 0) {
			viewHolder.textView_Amount.setVisibility(View.GONE);
			viewHolder.textView_Discount.setVisibility(View.GONE);
		} else {
			viewHolder.textView_Amount.setVisibility(View.VISIBLE);
			viewHolder.textView_Discount.setVisibility(View.VISIBLE);
		}

		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

	private class ViewHolder {
		TextView textView_OfferName, textView_Amount, textView_OfferAmount,
				textView_Sold, textView_Discount;
		Spinner spinner_Quantity;
		ImageView imageView_Details;
	}
}
