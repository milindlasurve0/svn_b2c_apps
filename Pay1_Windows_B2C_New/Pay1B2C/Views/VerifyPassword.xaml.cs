﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Info;
using System.Device.Location;
using Microsoft.Phone.Notification;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace Pay1B2C.Views
    {
    public partial class VerifyPassword: PhoneApplicationPage
        {
        ProgressBarManager ProgressBarManager;
       
        public VerifyPassword()
            {
            InitializeComponent();
           
            GoogleAnalytics.EasyTracker.GetTracker().SendView("Verify Password Layout");
            ProgressIndicator prog = new ProgressIndicator();
            ProgressBarManager = new ProgressBarManager();
            SystemTray.SetProgressIndicator(this, prog);
            ProgressBarManager.Hook(prog);

            textNumber.Text="+91 "+Constant.getUserNumber();
            imgEditNummber.MouseEnter+=(a, b) =>
            {
            (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri("/RegistrationLayout.xaml?", UriKind.RelativeOrAbsolute));
            NavigationService.GoBack();
            };
            if(Constant.LoadPersistent<Boolean>(Constant.SETTINGS_IS_NEW))
                {

                textNewUser.Visibility=Visibility.Visible;
                }
            else
                {
                textNewUser.Visibility=Visibility.Collapsed;
                }


            }

        private void PasswordLostFocus(object sender, RoutedEventArgs e)
            {
            CheckPasswordWatermark();
            }

        public void CheckPasswordWatermark()
            {
            var passwordEmpty = string.IsNullOrEmpty(Password.Password);
            PasswordWatermark.Opacity = passwordEmpty ? 100 : 0;
            Password.Opacity = passwordEmpty ? 0 : 100;
            }

        private void PasswordGotFocus(object sender, RoutedEventArgs e)
            {
            PasswordWatermark.Opacity = 0;
            Password.Opacity = 100;
            }

        private async void updatePinTask()
            {
            ProgressBarManager.Show(19, "Please wait...", false, 10);


            var values = new List<KeyValuePair<string, string>>();
            values.Add(new KeyValuePair<string, string>("actiontype", "forgotpwd"));
            values.Add(new KeyValuePair<string, string>("user_name", Constant.getUserNumber()));
            values.Add(new KeyValuePair<string, string>("api_version", Constant.API_VERSION));
            values.Add(new KeyValuePair<string, string>("api", "true"));
            values.Add(new KeyValuePair<string, string>("vtype", "1"));
            FormUrlEncodedContent values1 = new FormUrlEncodedContent(values);
            Task<string> response = Constant.postParamsAndgetResponse(Constant.B2C_URL, values1);

            string result = await response;
            if(string.IsNullOrEmpty(result))
                {
                NavigationService.Navigate(new Uri("/Views/NoConnection.xaml?", UriKind.RelativeOrAbsolute));
                }
            else
                {
                var dict = (JObject)JsonConvert.DeserializeObject(result);
                string status = dict["status"].ToString();
                if(status.Equals("success"))
                    {
                    PhoneApplicationService.Current.State["updateFromLogin"] = 3;
                    MessageBox.Show("OTP has been sent to your registered Pay1 number");
                    NavigationService.Navigate(new Uri("/Views/UpdatePassword.xaml?", UriKind.RelativeOrAbsolute));
                    }
                else
                    {
                    Constant.checkForErrorCode(result);
                    }
                }
            ProgressBarManager.Hide(19);
            }



         protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
            {
           (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri("/RegistrationLayout.xaml?", UriKind.RelativeOrAbsolute));
           (Application.Current.RootVisual as PhoneApplicationFrame).RemoveBackEntry();
            base.OnBackKeyPress(e);
            }

        private async void btnLogin_Click(object sender, RoutedEventArgs e)
            {
            String strOtp = Password.Password;
			String hashNumber = Constant.EncodeString (Constant.getUserNumber ());
			String hashOtp = Constant.EncodeString (strOtp);
			String serverOtp = Constant.getOtpHash ();
            if((hashNumber + hashOtp).Equals(serverOtp))
                {
                if(Constant.LoadPersistent<Boolean>(Constant.SETTINGS_IS_NEW))
                    {
                    await Task.Run(() => loginUser(strOtp));
                    }
                else
                    {
                    await Task.Run(() => loginUser(strOtp));


                    }
                }
            else
                {
                Constant.showOneButtonDialog("OTP", "INVALID OTP");
                }
            }

        private async void loginUser(String strOtp)
            {
            ProgressBarManager.Show(11, "Please wait...", false, 10);
            PhoneApplicationService.Current.State["loginCall"] = 1;
            var values = new List<KeyValuePair<string, string>>();
                         
                        values.Add(new KeyValuePair<string, string>("actiontype", "signin"));
                        values.Add(new KeyValuePair<string, string>("username",Constant.getUserNumber()));
                        values.Add(new KeyValuePair<string, string>("password",strOtp));
                       // values.Add(new KeyValuePair<string, string>("uuid", Constant.getUUID()));
                        values.Add(new KeyValuePair<string, string>("latitude",Constant.LoadPersistent<string>(Constant.SETTINGS_USER_LATTITUDE)));
                        values.Add(new KeyValuePair<string, string>("longitude",Constant.LoadPersistent<string>(Constant.SETTINGS_USER_LONGITUDE)));
                        values.Add(new KeyValuePair<string, string>("device_type","windows"));
                        values.Add(new KeyValuePair<string, string>("manufacturer",DeviceStatus.DeviceManufacturer));
                        values.Add(new KeyValuePair<string, string>("os_info",Environment.OSVersion.Version.ToString()));
                        values.Add(new KeyValuePair<string, string>("gcm_reg_id",Constant.LoadPersistent<string>(Constant.NOTIFICATION_URL)));
                        values.Add(new KeyValuePair<string, string>("api","true"));
                        values.Add(new KeyValuePair<string, string>("api_version", Constant.API_VERSION));
                        

            FormUrlEncodedContent values1 = new FormUrlEncodedContent(values);
            Task<string> response = Constant.postParamsAndgetResponse(Constant.B2C_URL, values1);

            string result = await response;
            if(string.IsNullOrEmpty(result))
                {
                (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri("/Views/NoConnection.xaml?", UriKind.Relative));
                }
            else
                {
                var dict = (JObject)JsonConvert.DeserializeObject(result);
                string status = dict["status"].ToString();
                var description = dict["description"];
                if(status.Equals("success"))
                    {
                    //string account_balance = description["account_balance"].ToString();
                    //Constant.setbalance(account_balance);
                   // getAndUpdateBalance();
                  
                       
                        Constant.SavePersistent(Constant.SETTINGS_USER_NAME, description["name"].ToString());
                        Constant.SavePersistent(Constant.SETTINGS_USER_PIN, strOtp);
                        Constant.setUserNumber(description["mobile"].ToString());
                        Constant.SavePersistent(Constant.SETTINGS_USER_GENDER, description["gender"].ToString());
                        Constant.SavePersistent(Constant.SETTINGS_USER_EMAIL, description["email"].ToString());
                        Constant.SavePersistent(Constant.SETTINGS_USER_ID, description["user_id"].ToString());
                        Constant.SavePersistent(Constant.SETTINGS_USER_DOB, description["date_of_birth"].ToString());
                        Constant.setbalance(description["wallet_balance"].ToString());

                        Constant.setEmail(description["email"].ToString());
                        Constant.setUserName(description["name"].ToString());
                        Constant.SavePersistent(Constant.SETTINGS_USER_PIN, Constant.LoadPersistent<String>(Constant.SETTINGS_USER_PIN));
                        Constant.SavePersistent(Constant.SETTINGS_USER_GENDER, description["gender"].ToString());
                        Constant.SavePersistent(Constant.SETTINGS_USER_DOB, description["date_of_birth"].ToString());

                        Constant.setLoyalityPoints(description["loyalty_points"].ToString());
                        Constant.SavePersistent(Constant.SETTINGS_USER_LATTITUDE, description["latitude"].ToString());
                        Constant.SavePersistent(Constant.SETTINGS_USER_LONGITUDE, description["longitude"].ToString());
                        Constant.SavePersistent(Constant.FEATURED_DEALS, description["Deals_offer"].ToString());
                        Constant.SavePersistent(Constant.SETTINGS_USER_SUPPORT_NUMBER, description["support_number"].ToString());
                        Constant.SavePersistent(Constant.SETTINGS_IS_LOGIN, true);



                        Constant.setUserimage(description["image"].ToString());
                      //  NavigationService.Navigate(new Uri("/HomePage.xaml?", UriKind.RelativeOrAbsolute));
                        try
                            {
                            Dispatcher.BeginInvoke(() =>
                            (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri("/HomePage.xaml?", UriKind.RelativeOrAbsolute)));
                            (Application.Current.RootVisual as PhoneApplicationFrame).RemoveBackEntry();
                            }
                        catch(Exception exc)
                            {
                            }
                        }
                    else
                        {
                        Constant.checkForErrorCode(result);
                        }

                    }
                }
            






        private static async void getAndUpdateBalance()
            {
            var values = new List<KeyValuePair<string, string>>();
            values.Add(new KeyValuePair<string, string>("actiontype", "Check_bal"));

            values.Add(new KeyValuePair<string, string>("api", "true"));
            FormUrlEncodedContent values1 = new FormUrlEncodedContent(values);
            Task<string> response = Constant.postParamsAndgetResponse(Constant.B2C_URL, values1);

            string result = await response;
            if(string.IsNullOrEmpty(result))
                {
                (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri("/Views/NoConnection.xaml?", UriKind.Relative));
                }
            else
                {
                var dict = (JObject)JsonConvert.DeserializeObject(result);
                string status = dict["status"].ToString();
                var description = dict["description"];
                if(status.Equals("success"))
                    {
                    string account_balance = description["account_balance"].ToString();
                    Constant.setbalance(account_balance);
                    }
                }
            }

        private void btnForgetPassword_Click(object sender, RoutedEventArgs e)
            {
            updatePinTask();
            }



        }
    }