package com.pay1.constants;

import com.pay1.R;

public class OperatorConstant {

	public static int getOperatorResource(int op_code) {

		switch (op_code) {

		case 1:
		case 44:
			return R.drawable.m_aircel;

		case 2:
		case 45:
		case 42:
			return R.drawable.m_airtel;

		case 3:
		case 46:
		case 38:
			return R.drawable.m_bsnl;

		case 4:
		case 47:
		case 39:
			return R.drawable.m_idea;

		case 5:
		case 37:
			return R.drawable.m_loop;

		case 6:
		case 49:
			return R.drawable.m_mts;

		case 7:
		case 50:
		case 43:
			return R.drawable.m_reliance_cdma;

		case 8:
			return R.drawable.m_reliance;

		case 9:
		case 51:
		case 36:
			return R.drawable.m_docomo;

		case 10:
		case 40:
			return R.drawable.m_indicom;

		case 11:
			return R.drawable.m_uninor;

		case 12:
			return R.drawable.m_videocon;

		case 15:
		case 52:
		case 41:
			return R.drawable.m_vodafone;

		case 30:
		case 48:
			return R.drawable.m_mtnl;

		case 16:
			return R.drawable.m_airtel;

		case 17:
			return R.drawable.d_bigtv;

		case 18:
			return R.drawable.d_dishtv;

		case 19:
			return R.drawable.d_sundirect;

		case 20:
			return R.drawable.d_tatasky;

		case 21:
			return R.drawable.d_videocon;

		default:
			return R.drawable.m_loop;

		}

	}

}
