//
//  TopOfferCell.h
//  Pay1
//
//  Created by Annapurna on 11/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#import "UIImageView+WebCache.h"
#import "UIImage+Utility.h"
#import "GetMyDealList.h"
@interface MyGiftsCell : UITableViewCell{

    IBOutlet UIView *blackView;
    IBOutlet UIImageView *giftImageView;
    IBOutlet UIImageView *giftThumbNailImageView;
    IBOutlet UILabel *giftDetailLabel;
    IBOutlet UILabel *giftPointLabel;
    IBOutlet UILabel *giftNameLabel;
    
    IBOutlet UIImageView *moneyImageView;
    
    IBOutlet UILabel *expiredLabel;
    
   IBOutlet UILabel *rupeeLabel;
    IBOutlet UIImageView *rupeeImageView;
    
    IBOutlet UIImageView *plusImageView;
    
    IBOutlet UIView *offerPriceBGView;

}



-(void)showMyGiftDeal:(GetMyDealList *)myDeal;


@end
