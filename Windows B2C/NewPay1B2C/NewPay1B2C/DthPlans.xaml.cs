﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

using System.Diagnostics;
using System.Windows.Threading;
using System;
using NewPay1B2C;
using NewPay1B2C.Model;
using SQLiteClient;
using System.Windows.Media;
using NewPay1B2C.UserControls;

namespace NewPay1B2C
	{
	public partial class DthPlans : PhoneApplicationPage
		{
		DthPlanDB DthAllPlans = new DthPlanDB ();
		PivotItem pvt;
		string code;
		int i = 0;
		SQLiteConnection db = null;
		DispatcherTimer newTimer;
		string op_id;
		public DthPlans ()
			{
			InitializeComponent ();
			CreateDatabase ();
			try
				{
				GoogleAnalytics.EasyTracker.GetTracker ().SendView ("PlansActivity");
				}
			catch (Exception ee)
				{
				}
			
			textTitlePath.Text = "Recharge > DTH > Plans ";
			//opeNcircle.Text=HomePage.operatorName;
			}

		protected override void OnNavigatedTo (NavigationEventArgs e)
			{
			CreateDatabase ();

			if (NavigationContext.QueryString.ContainsKey ("op_id"))
				{
				op_id = NavigationContext.QueryString["op_id"];

				getData ();
				}


			}

		public async Task<string> MakeWebRequest (string apiUrl)
			{
			String result = "";
			var httpClient = new HttpClient (new HttpClientHandler ());

			HttpResponseMessage response = await httpClient.PostAsync (apiUrl, null);
			response.EnsureSuccessStatusCode ();
			var responseString = await response.Content.ReadAsStringAsync ();
			try
				{

				string output = responseString.Remove (responseString.Length - 1, 1);
				string output1 = output.Remove (output.Length - 1, 1);
				string output2 = output1.Remove (0, 1);
				string output3 = output2.Remove (0, 1);
				string output4 = output3.Remove (output3.Length - 1, 1);
				var dict = (JObject)JsonConvert.DeserializeObject (output4);
				var desc = dict[op_id];
				int i = 0;

				var prod_code_pay1 = desc["prod_code_pay1"];
				DthAllPlans.dthPlanOpertorID = prod_code_pay1.ToString ();

				var opr_name = desc["opr_name"];
				DthAllPlans.dthPlanOperatorName = opr_name.ToString ();

				var circles = desc["circles"];


				/*code = prod_code_pay1.ToString();
				var BR = circles[code];
				var circle_id = BR["circle_id"];
				DthAllPlans.dthPlanCircleID = circle_id.ToString();
				var circle_name = BR["circle_name"];
				DthAllPlans.dthPlanCircleName = circle_name.ToString();*/
				JObject jObject1 = (JObject)JsonConvert.DeserializeObject (circles.ToString ());
				var all = circles["all"];
				var plans = all["plans"];


				//var plans = kvp1["plans"];
				JObject jObject = (JObject)JsonConvert.DeserializeObject (plans.ToString ());
				Dictionary<string, string> res = new Dictionary<string, string> (jObject.Count);
				foreach (var kvp in jObject)
					{
					var planType = kvp.Key;
					var planDetails = kvp.Value;
					DthAllPlans.dthPlanType = planType.ToString ();
					foreach (var plansDetails in planDetails)
						{
						var plan_amt = plansDetails["plan_amt"];
						DthAllPlans.dthPlanAmount = Convert.ToInt32 (plansDetails["plan_amt"].ToString ());
						var plan_validity = plansDetails["plan_validity"];
						DthAllPlans.dthPlanValidity = plan_validity.ToString ();
						var plan_desc = plansDetails["plan_desc"];
						DthAllPlans.dthPlanDescription = plan_desc.ToString ();
						DthAllPlans.dthPlanUptateTime = DateTime.Now;//.ToString();
						SQLiteCommand cmd = db.CreateCommand ("");

						cmd.CommandText = " Insert into DthPlans (dthPlanAmount,dthPlanOpertorID,dthPlanOperatorName,dthPlanCircleID,dthPlanCircleName,dthPlanType,dthPlanValidity,dthPlanDescription,dthPlanUptateTime) values (@dthPlanAmount,@dthPlanOpertorID,@dthPlanOperatorName,@dthPlanCircleID,@dthPlanCircleName,@dthPlanType,@dthPlanValidity,@dthPlanDescription,@dthPlanUptateTime)";
						try
							{
							int rec = cmd.ExecuteNonQuery (DthAllPlans);
							}
						catch (SQLiteException sed)
							{
							string mms = sed.Message;
							}
						}
					}
				}
			catch (Exception e)
				{
				Console.WriteLine ("");
				}


			getData ();


			return result;

			}

		private void CreateDatabase ()
			{
			if (db == null)
				{
				db = new SQLiteConnection (Constant.DATABASE_NAME);
				db.Open ();
				try
					{
					SQLiteCommand cmd = db.CreateCommand ("Create table DthPlans (dthPlanAmount int,dthPlanOpertorID text,dthPlanOperatorName text,dthPlanCircleID text,dthPlanCircleName text,dthPlanType text,dthPlanValidity text,dthPlanDescription text,dthPlanUptateTime datetime)");
					int i = cmd.ExecuteNonQuery ();

					}
				catch (SQLiteException ex)
					{

					}
				}
			}



		public async void getData ()
			{
			try
				{
				List<DthPlanDB> result = null;

				SQLiteCommand cmd1 = db.CreateCommand ("SELECT * FROM DthPlans WHERE dthPlanOpertorID='" + op_id + "' ORDER BY dthPlanType DESC,dthPlanAmount ASC");
				var lst = cmd1.ExecuteQuery<DthPlanDB> ();
				result = lst.ToList ();


				if (result.Count == 0)
					{

					Task<string> abc = MakeWebRequest (Constant.B2B_URL + "method=getPlanDetails&operator=" + op_id + "&circle=" + code);

					}
				else
					{
					var item1 = result[0];

					DateTime currentDate = DateTime.Now;//.ToString();
					DateTime updatedDate = item1.dthPlanUptateTime;
					TimeSpan duration = currentDate - updatedDate;
					if (duration.Days >= 2)
						{
						SQLiteCommand cmd2 = db.CreateCommand ("DELETE FROM DthPlans WHERE dthPlanCircleID='" + code + "' AND dthPlanOpertorID='" + op_id + "'");
						int i = cmd2.ExecuteNonQuery ();
						Task<string> abc = MakeWebRequest (Constant.B2B_URL + "method=getPlanDetails&operator=" + op_id + "&circle=" + code);
						}
					else
						{

						List<PlanUserControl>[] a = new List<PlanUserControl>[25];
						List<String> ptype = new List<String> ();



						foreach (var item in result)
							{
							PlanUserControl row = new PlanUserControl ();
							row.Validity.Text = item.dthPlanValidity;

							row.amount.Text = "" + item.dthPlanAmount;
							row.PlanDesc.Text = item.dthPlanDescription;
							string typr = item.dthPlanType;
							//L1.FindName(item.planType);
							int x = 0;

							if (!ptype.Contains (typr))//ptype does not contain itrType
								{
								ptype.Add (typr);
								x = ptype.IndexOf (typr);
								a[x] = new List<PlanUserControl> ();

								}
							else
								{
								x = ptype.IndexOf (typr);
								}
							a[x].Add (row);

							i++;

							//L1.Items.Add(row);
							}
						//Pivot myPivot = new Pivot();
						System.Diagnostics.Debug.WriteLine (a);
						for (int j = 0; j <= a.Count (); j++)
							{
							if (a[j] != null)
								{

								TextBlock tb = new TextBlock ();
								tb.Text = ptype[j];
								tb.Foreground = new SolidColorBrush (Colors.Blue);
								tb.FontSize = 60;
								pvt = new PivotItem ();
								pvt.Header = ptype[j];


								var stack = new StackPanel ();
								pvt.Content = stack;

								Grid Gird_Root = new Grid ();

								PivotItem PI = new PivotItem () { Content = Gird_Root };

								PI.Header = ptype[j];




								Grid myNewGrid = new Grid ();
								myNewGrid.Margin = new Thickness (0, 0, 0, 0);
								ListBox listPlan = new ListBox ();

								listPlan.ItemsSource = a[j];
								myNewGrid.Children.Add (listPlan);

								PI.Content = myNewGrid;
								myPivot.Items.Add (PI);
								pvt = null;
								listPlan.SelectionChanged += new SelectionChangedEventHandler (listBox1_SelectedIndexChanged);

								}
							}
						}
					}
				}
			catch (Exception e2)
				{
				Debug.WriteLine (e2.StackTrace);
				}

			if (i == 1)
				{
				MessageBox.Show ("No Plans Available.");


				NavigationService.GoBack ();

				}
			else
				{
				}

			}

		private void listBox1_SelectedIndexChanged (object sender, SelectionChangedEventArgs e)
			{
			PlanUserControl control = (sender as ListBox).SelectedItem as PlanUserControl;
			string validity = control.Validity.Text;
			string desc = control.PlanDesc.Text;
			string amt = control.amount.Text;
			//NavigationService.Navigate(new Uri("/RechargeDTH.xaml?amt=" + amt + "&validity=" + validity + "&desc=" + desc, UriKind.Relative));
			NavigationService.Navigate (new Uri ("/HomePage.xaml?amt=" + amt + "&validity=" + validity + "&desc=" + desc + "&op_id=" + op_id, UriKind.Relative));
			NavigationService.RemoveBackEntry ();
			}

		private void buttonImageBack_Click (object sender, RoutedEventArgs e)
			{
			NavigationService.GoBack ();
			}
		}
	}