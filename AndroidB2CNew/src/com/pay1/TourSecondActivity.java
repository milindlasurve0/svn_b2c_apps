package com.pay1;

import com.pay1.utilities.Utility;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class TourSecondActivity extends Activity{
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tour_second_activity);
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
		Button buttonContinue=(Button)findViewById(R.id.buttonContinue);
		Typeface Reguler = Typeface.createFromAsset(getAssets(),
				"EncodeSansNormal-400-Regular.ttf");
		buttonContinue.setTypeface(Reguler);
		buttonContinue.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Utility.setIsNewTour(TourSecondActivity.this,false);
				Intent intent = new Intent(TourSecondActivity.this,
						MainActivity.class);
				intent.putExtra("IS_FLAG", getIntent().getExtras().getBoolean("IS_FLAG"));
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
						| Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
				finish();
			}
		});
	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		//super.onBackPressed();
		
	}
}
