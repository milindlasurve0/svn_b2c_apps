//
//  GiftDetailCommonView.h
//  Pay1
//
//  Created by Annapurna on 23/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GiftDetailCommonView : UIView<UITableViewDataSource,UITableViewDelegate>{
    IBOutlet UILabel *headerLabel;
    NSString *numberStr;
    IBOutlet UITableView *tv;
    
    NSMutableArray *dataArray;
    
    BOOL isShow;
    
    
}
-(void)showCommonViewForGiftDetails:(NSMutableArray *)array fromView:(BOOL)fromView rowID:(NSInteger)rowID shopName:(NSString *)shopName;

@property (nonatomic, copy)void (^onCrossClick)();
@end
