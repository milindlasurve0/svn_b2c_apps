package com.pay1.databasehandler;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

public class FreeBiesLocationDataSource {
	// Database fields
	private SQLiteDatabase database;
	private Pay1DataGiftHelper dbHelper;

	public FreeBiesLocationDataSource(Context context) {
		dbHelper = new Pay1DataGiftHelper(context);
	}

	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
		System.out.println("Released " + SQLiteDatabase.releaseMemory());
	}

	public void createFreeBiesLocation(int locationDealID, double locationLat,
			double locationLng, String locationAddress, String locationArea,
			String locationCity, String locationState, int locationPin,
			double locationDistance, long locationUpdatedTime) {

		ContentValues values = new ContentValues();
		values.put(Pay1GiftsSQLiteHelper.LOCATION_DEAL_ID, locationDealID);
		values.put(Pay1GiftsSQLiteHelper.LOCATION_LAT, locationLat);
		values.put(Pay1GiftsSQLiteHelper.LOCATION_LNG, locationLng);
		values.put(Pay1GiftsSQLiteHelper.LOCATION_ADDRESS, locationAddress);
		values.put(Pay1GiftsSQLiteHelper.LOCATION_AREA, locationArea);
		values.put(Pay1GiftsSQLiteHelper.LOCATION_CITY, locationCity);
		values.put(Pay1GiftsSQLiteHelper.LOCATION_STATE, locationState);
		values.put(Pay1GiftsSQLiteHelper.LOCATION_PIN, locationPin);
		values.put(Pay1GiftsSQLiteHelper.LOCATION_DISTANCE, locationDistance);
		values.put(Pay1GiftsSQLiteHelper.LOCATION_UPDATED_TIME,
				locationUpdatedTime);

		database.insert(Pay1GiftsSQLiteHelper.TABLE_LOCATION, null, values);
		// Cursor cursor =
		// database.query(Pay1GiftsSQLiteHelper.TABLE_FreeBiesLocation,
		// null, Pay1GiftsSQLiteHelper.FreeBiesLocation_ID + " = " + insertId,
		// null,
		// null, null, null);
		// cursor.moveToFirst();
		// FreeBiesLocation neFreeBiesLocation =
		// cursorToFreeBiesLocation(cursor);
		// cursor.close();
		// return newFreeBiesLocation;
	}

	public void deleteFreeBiesLocation() {
		database.delete(Pay1GiftsSQLiteHelper.TABLE_LOCATION, null, null);
	}

	public void deleteFreeBiesLocation(int offer_ID) {
		database.delete(Pay1GiftsSQLiteHelper.TABLE_LOCATION,
				Pay1GiftsSQLiteHelper.LOCATION_DEAL_ID + " = " + offer_ID, null);
	}

	public void deleteFreeBiesLocation(String offer_ID) {
		database.delete(Pay1GiftsSQLiteHelper.TABLE_LOCATION,
				Pay1GiftsSQLiteHelper.LOCATION_DEAL_ID + " In ( " + offer_ID
						+ " )", null);
	}

	public List<FreeBiesLocation> getAllFreeBiesLocation() {
		List<FreeBiesLocation> FreeBiesLocation = new ArrayList<FreeBiesLocation>();

		Cursor cursor = database.query(Pay1GiftsSQLiteHelper.TABLE_LOCATION,
				null, null, null, null, null, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			FreeBiesLocation freebie = cursorToFreeBiesLocation(cursor);
			FreeBiesLocation.add(freebie);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return FreeBiesLocation;
	}

	public List<FreeBiesLocation> getAllFreeBiesLocation(int offer_id) {
		List<FreeBiesLocation> FreeBiesLocation = new ArrayList<FreeBiesLocation>();

		Cursor cursor = database.query(Pay1GiftsSQLiteHelper.TABLE_LOCATION,
				null,
				Pay1GiftsSQLiteHelper.LOCATION_DEAL_ID + " = " + offer_id,
				null, null, null, Pay1GiftsSQLiteHelper.LOCATION_ID);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			FreeBiesLocation freebie = cursorToFreeBiesLocation(cursor);
			FreeBiesLocation.add(freebie);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return FreeBiesLocation;
	}
	
	public List<FreeBiesLocation> getAllFreeBiesLocationID(int location_id) {
		List<FreeBiesLocation> FreeBiesLocation = new ArrayList<FreeBiesLocation>();

		Cursor cursor = database.query(Pay1GiftsSQLiteHelper.TABLE_LOCATION,
				null,
				Pay1GiftsSQLiteHelper.LOCATION_ID + " = " + location_id,
				null, null, null, Pay1GiftsSQLiteHelper.LOCATION_ID);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			FreeBiesLocation freebie = cursorToFreeBiesLocation(cursor);
			FreeBiesLocation.add(freebie);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return FreeBiesLocation;
	}

	public FreeBiesLocation getFreeBiesLocation() {

		Cursor cursor = database.query(Pay1GiftsSQLiteHelper.TABLE_LOCATION,
				null, null, null, null, null,
				Pay1GiftsSQLiteHelper.LOCATION_UPDATED_TIME + " desc limit 1");

		if (cursor.getCount() > 0) {
			cursor.moveToFirst();
			FreeBiesLocation FreeBiesLocation = cursorToFreeBiesLocation(cursor);
			cursor.moveToNext();
			// make sure to close the cursor
			cursor.close();
			return FreeBiesLocation;
		} else {
			return null;
		}
	}

	private FreeBiesLocation cursorToFreeBiesLocation(Cursor cursor) {
		FreeBiesLocation freeBiesLocation = new FreeBiesLocation();
		freeBiesLocation.setLocationID(cursor.getInt(0));
		freeBiesLocation.setLocationDealID(cursor.getInt(1));
		freeBiesLocation.setLocationLat(cursor.getDouble(2));
		freeBiesLocation.setLocationLng(cursor.getDouble(3));
		freeBiesLocation.setLocationAddress(cursor.getString(4));
		freeBiesLocation.setLocationArea(cursor.getString(5));
		freeBiesLocation.setLocationCity(cursor.getString(6));
		freeBiesLocation.setLocationState(cursor.getString(7));
		freeBiesLocation.setLocationPin(cursor.getInt(8));
		freeBiesLocation.setLocationDistance(cursor.getDouble(9));
		freeBiesLocation.setLocationUpdatedTime(cursor.getLong(10));
		return freeBiesLocation;
	}
}
