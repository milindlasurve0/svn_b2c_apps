﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pay1B2C.Utilities
    {
    public class GoogleTile: Microsoft.Phone.Controls.Maps.TileSource
        {
        private int _server;
        private char _mapmode;
        private GoogleTileTypes _tiletypes;

        public GoogleTileTypes TileTypes
            {
            get { return _tiletypes; }
            set
                {
                _tiletypes = value;
                MapMode = MapModeConverter(value);
                }
            }

        public char MapMode
            {
            get { return _mapmode; }
            set { _mapmode = value; }
            }

        public int Server
            {
            get { return _server; }
            set { _server = value; }
            }

        public GoogleTile()
            {
            UriFormat = @"http://mt{0}.google.com/vt/lyrs={1}&z={2}&x={3}&y={4}";
            Server = 0;
            }

        public override Uri GetUri(int x, int y, int zoomLevel)
            {
            if(zoomLevel > 0)
                {
                var Url = string.Format(UriFormat, Server, MapMode, zoomLevel, x, y);
                return new Uri(Url);
                }
            return null;
            }

        private char MapModeConverter(GoogleTileTypes tiletype)
            {
            switch(tiletype)
                {
                case GoogleTileTypes.Hybrid:
                        {
                        return 'y';
                        }
                case GoogleTileTypes.Physical:
                        {
                        return 't';
                        }
                case GoogleTileTypes.Satellite:
                        {
                        return 's';
                        }
                case GoogleTileTypes.Street:
                        {
                        return 'm';
                        }
                case GoogleTileTypes.WaterOverlay:
                        {
                        return 'r';
                        }
                }
            return ' ';
            }
        }


    public enum GoogleTileTypes
        {
        Hybrid,
        Physical,
        Street,
        Satellite,
        WaterOverlay
        }


    }
