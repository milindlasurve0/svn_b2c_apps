package com.pay1;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import com.larvalabs.svgandroid.SVG;
import com.larvalabs.svgandroid.SVGParser;
import com.pay1.adapterhandler.InfoAdapter;

public class WalletTopupFragement extends Fragment {

	private ListView listView_Topup;
	private ArrayList<String> list;
	private InfoAdapter adapter;

	public static WalletTopupFragement newInstance() {
		WalletTopupFragement fragment = new WalletTopupFragement();
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// if ((savedInstanceState != null)
		// && savedInstanceState.containsKey(KEY_CONTENT)) {
		// mContent = savedInstanceState.getString(KEY_CONTENT);
		// }
	}

	@SuppressLint("NewApi")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.wallet_topup_fragment, container,
				false);
		try {

			listView_Topup = (ListView) view.findViewById(R.id.listView_Topup);

			ImageView imageViewWallet = (ImageView) view
					.findViewById(R.id.imageViewWallet);
			SVG svgwallet = SVGParser.getSVGFromResource(getActivity()
					.getResources(), R.raw.ic_profile_wallet);
			Drawable svgsvgwalletDrawable = svgwallet.createPictureDrawable();
			imageViewWallet.setImageDrawable(svgsvgwalletDrawable);
			
			ImageView imageViewBalance = (ImageView) view
					.findViewById(R.id.imageViewBalance);

			SVG svgbalance = SVGParser.getSVGFromResource(getActivity()
					.getResources(), R.raw.ic_home_rupee);
			Drawable svgbalanceDrawable = svgbalance.createPictureDrawable();
			imageViewBalance.setImageDrawable(svgbalanceDrawable);
			
			list = new ArrayList<String>();
			list.clear();
			list.add("Locate cash\ntopup store");
			list.add("Online card\npayment");
			list.add("Redeem\ncoupon code");
			adapter = new InfoAdapter(getActivity(), 1, list);
			listView_Topup.setAdapter(adapter);
			listView_Topup
					.setOnItemClickListener(new AdapterView.OnItemClickListener() {

						@Override
						public void onItemClick(AdapterView<?> parent,
								View view, int position, long id) {
							// TODO Auto-generated method stub
							switch (position) {
							case 0:
								Intent intent = new Intent(getActivity(),
										MapActivity.class);
								intent.putExtra("SHOW_GIFT", false);
								intent.putExtra("SHOW_RETAILER", true);
								startActivity(intent);
								break;
							case 1:
								startActivity(new Intent(getActivity(),
										DebitCreditActivity.class));
								break;
							case 2:
								startActivity(new Intent(getActivity(),
										CouponCodeActivity.class));
								break;
							default:
								break;
							}

						}
					});

		} catch (Exception e) {
		}

		return view;
	}

	@Override
	public void onStart() {
		super.onStart();
	}

	@Override
	public void onStop() {
		super.onStop();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.Fragment#onDestroy()
	 */
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();

	}

}
