﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Tasks;
using System.Windows.Media.Imaging;
using Microsoft.Phone.Shell;



using Microsoft.Phone.UserData;
using System.Xml;
using System.Globalization;
using System.Threading.Tasks;
using System.ComponentModel;
using SQLiteClient;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using NewPay1B2C.Model;
using System.Device.Location;
using System.Threading;
using Microsoft.Phone.Info;
using NewPay1B2C.UserControls;
using NewPay1B2C.UtilityClass;
using System.Windows.Controls.Primitives;

namespace NewPay1B2C
    {
    public partial class HomePage : PhoneApplicationPage
        {
        SQLiteConnection db = null;
        public static string latti, longi;
        GeoCoordinateWatcher watcher;
        public static string currentAddress;
        ProgressBarManager ProgressBarManager;
		List<OperatorDB> queryResult = null;
        public static long applicationPeakMemoryUsage, applicationCurrentMemoryUsage, deviceTotalMemory;
        public HomePage ()
            {
            InitializeComponent ();
            CheckForUpdatedVersion ();
            ProgressIndicator prog = new ProgressIndicator ();
            ProgressBarManager = new ProgressBarManager ();
            SystemTray.SetProgressIndicator (this, prog);
            ProgressBarManager.Hook (prog);
			
            Constant.laodNavigationBar (TitlePanel, LayoutRoot, canvas, "Home");
			PhoneApplicationService.Current.State["data"] = "2";
			try
				{
				GoogleAnalytics.EasyTracker.GetTracker ().SendView ("HomePage");
				}
			catch (Exception ee)
				{
				}
            if (db == null)
                {
                db = new SQLiteConnection (Constant.DATABASE_NAME);
                db.Open ();


				try																																											 
					{
					SQLiteCommand cmd = db.CreateCommand ("Create table OperatorTable(id text,name text,code text,p_id text UNIQUE,stv text,flag text,service_id text,delegateFlag text,min text,max text,charges_slab text,service_charge_amount text,service_tax_percent text,service_charge_percent text,prefix text,length text)");
					int i = cmd.ExecuteNonQuery ();
					}
				catch (SQLiteException ex)
					{

					}
				try
					{
					SQLiteCommand cmd1 = db.CreateCommand ("Create table Plans(planAmount int,planOpertorID text,planOperatorName text,planCircleID text,planCircleName text,planType text,planValidity text,planDescription text,planUptateTime datetime)");
					int i1 = cmd1.ExecuteNonQuery ();
					}
				catch (SQLiteException ex)
					{

					}
				try
					{
					SQLiteCommand cmd2 = db.CreateCommand ("Create table DthPlans (dthPlanAmount int,dthPlanOpertorID text,dthPlanOperatorName text,dthPlanCircleID text,dthPlanCircleName text,dthPlanType text,dthPlanValidity text,dthPlanDescription text,dthPlanUptateTime datetime)");
					int i2 = cmd2.ExecuteNonQuery ();
					}
				catch (SQLiteException ex)
					{

					}

                try
                    {
                    SQLiteCommand cmdFreeBie = db.CreateCommand ("Create table FreeBieDataBase (freeBie text)");
                    int i3 = cmdFreeBie.ExecuteNonQuery ();
                    }
                catch (Exception ee)
                    {
                    System.Diagnostics.Debug.WriteLine (ee.Message);
                    }
                 try
                    {
                    SQLiteCommand cmdFreeBie = db.CreateCommand ("Create table QuickPayData (quickPay text)");
                    int i3 = cmdFreeBie.ExecuteNonQuery ();
                    }
                catch (Exception ee)
                    {
                    System.Diagnostics.Debug.WriteLine (ee.Message);
                    }
                try
                    {
                    SQLiteCommand cmdPlans = db.CreateCommand ("Create table AllPlans (allPlanOpertorID text,allPlanOperatorName text,allPlanCircleID text,allPlanCircleName text,allCirclePlans text)");
                    int j = cmdPlans.ExecuteNonQuery ();
                    }
                catch (Exception ee)
                    {
                    System.Diagnostics.Debug.WriteLine (ee.Message);
                    }
                
                }
            deviceTotalMemory = (long)DeviceExtendedProperties.GetValue ("DeviceTotalMemory");
            applicationCurrentMemoryUsage = (long)DeviceExtendedProperties.GetValue ("ApplicationCurrentMemoryUsage");
            applicationPeakMemoryUsage = (long)DeviceExtendedProperties.GetValue ("ApplicationPeakMemoryUsage");
            StartLoadingData ();
            this.Loaded += (a, b) => 
            {

            LoadOldtransaction ();
			try
				{

				SQLiteCommand cmd1 = db.CreateCommand ("SELECT *  FROM OperatorTable");
				var lst = cmd1.ExecuteQuery<OperatorDB> ();
				queryResult = lst.ToList ();
				if (queryResult.Count <= 1)
					{
					DateTime currentDate = DateTime.Now;//.ToString();
					DateTime updatedDate = Constant.LoadPersistent<DateTime> (Constant.OPERATOR_LIST_UPDATE_TIME);
					TimeSpan duration = currentDate - updatedDate;
					if (duration.Days >= 1)
						{
						SQLiteCommand cmd2 = db.CreateCommand ("DELETE FROM OperatorTable");
						int i = cmd2.ExecuteNonQuery ();
						StartLoadingData ();
						}
					}
				}
			catch (SQLiteException se)
				{
				string see = se.Message;
				}
           
            };
            }


		protected override void OnBackKeyPress (System.ComponentModel.CancelEventArgs e)
			{
			var result = MessageBox.Show ("Are you sure you want to exit the app?", "Please confirm", MessageBoxButton.OKCancel);
			e.Cancel = result != MessageBoxResult.OK;
			base.OnBackKeyPress (e);
			}


        private void LoadOldtransaction ()
            {
            mytransactionPanel.Children.Clear();
            String myLastTransaction = Constant.getLastTransaction ();
            JArray myLastArray = (JArray)JsonConvert.DeserializeObject (myLastTransaction);
            int totalTransaction = myLastArray.Count;
            
            int noOfRows = 0;
            if ((totalTransaction % 2) == 0)
                {
                noOfRows = totalTransaction / 2;
                }
            else
                {
                noOfRows = totalTransaction / 2;
                noOfRows = noOfRows + 1;
                }
            int j = 0;
            for (var rows = 0; rows < noOfRows; rows++)
                {
                StackPanel panel = new StackPanel ();
                panel.Orientation = System.Windows.Controls.Orientation.Horizontal;
                int i;
                for (i = 0; i < 2; i++)
                    {
                    j = j + 1;
                    if (j <= totalTransaction)
                        {
                        if (j <= 6)
                            {
                            HomePageTransaction MyOldTransactionControl = new HomePageTransaction ();
                            JObject jObj = (JObject)myLastArray[j - 1];
                            MyOldTransactionControl.imageOperator.Source = OperatorConstant.getOperatorResource (Convert.ToInt32 (jObj["product"].ToString ()));
                            if (jObj["flag"].ToString ().Equals ("4"))
                                {
                                DateTime time = DateTime.Parse (jObj["datetime"].ToString ());
                                string formattedDate = time.ToString ("dd-MMM");
                                
                                MyOldTransactionControl.textPopupAmount.Text = formattedDate;
                                }
                            else
                                {
                                MyOldTransactionControl.textPopupAmount.Text = "Rs. " + jObj["amount"].ToString ();
                                }
                            MyOldTransactionControl.textPopupNumber.Text = jObj["number"].ToString ();
                            MyOldTransactionControl.Tag = j;

                            MyOldTransactionControl.Tap += (s, e) =>
                            {
                            doRecharge (jObj, j - 1);
                            };
                            panel.Children.Add (MyOldTransactionControl);
                            }
                        else
                            {
                            break;
                            }

                        }
                    else
                        {
                        break;
                        }
                    }
                mytransactionPanel.Children.Add (panel);

                }
            }

        private void doRecharge (JObject jObj, int index)
            {
           // JObject jObj = (JObject)myLastArray[index - 1];
            string id = jObj["operator_id"].ToString ();
            string flag = jObj["flag"].ToString ();
            string amount = jObj["amount"].ToString ();
            string number = jObj["number"].ToString ();
            string stv = jObj["stv"].ToString ();
            string product = jObj["product"].ToString ();
            int recAmount=Convert.ToInt32(amount);
            bool rechargeViaWallet = true;
            int mainBalance = Constant.getbalanceInInt ();
            
            
                if (flag.Equals ("1") || flag.Equals ("2") || flag.Equals ("3"))
                    {
                    if (recAmount > mainBalance || mainBalance == 0)
                        {
                        rechargeViaWallet = false;
                    Popup popup = new Popup ();
                    RechargeMobileControl recControl = new RechargeMobileControl ();
                    ContentOverlay.Visibility = Visibility.Visible;
                    popup.VerticalOffset = 200;
                    popup.IsOpen = true;
                    popup.Child = recControl;
                    recControl.imgClose.Tap += (a, b) =>
                    {
                    popup.IsOpen = false;
                    ContentOverlay.Visibility = Visibility.Collapsed;
                    };
                    recControl.textMessage.Text = "Recharge the transaction of Rs. " + amount + " on " + number + ".";
                    recControl.buttonImageOk.Content = "Pay via card or Netbanking";
                    recControl.textCCDC.Text = "Pay via  Credit/Debit Card";
                    recControl.buttonImageOk.Click += (s, e) =>
                    {
                    popup.IsOpen = false;
                         ContentOverlay.Visibility = Visibility.Collapsed;
                         rechargeTask (rechargeViaWallet, id, flag, amount,amount, number, stv, product);
                    };

                    
                    }
                else
                    {
                    rechargeViaWallet = true;
                    Popup popup = new Popup ();
                    RechargeMobileControl recControl = new RechargeMobileControl ();
                    ContentOverlay.Visibility = Visibility.Visible;
                    popup.VerticalOffset = 200;
                    popup.IsOpen = true;
                    popup.Child = recControl;
                    recControl.imgClose.Tap += (a, b) =>
                    {
                        popup.IsOpen = false;
                        ContentOverlay.Visibility = Visibility.Collapsed;
                    };
                    recControl.textMessage.Text = "Recharge the transaction of Rs. " + amount + " on " + number + ".";
                    recControl.buttonImageOk.Content = "Pay via card or Netbanking";
                    recControl.textCCDC.Text = "Cash Topup? Locate shop";
                    recControl.textCCDC.Tap += (a, b) =>
                    {
                        popup.IsOpen = false;
                        ContentOverlay.Visibility = Visibility.Collapsed;
                        (Application.Current.RootVisual as PhoneApplicationFrame).Navigate (new Uri ("/ShopLocator.xaml?no-cache=" + Guid.NewGuid (), UriKind.Relative));

                        (Application.Current.RootVisual as PhoneApplicationFrame).RemoveBackEntry ();

                    };
                    recControl.buttonImageOk.Click += (s, e) =>
                    {
                        popup.IsOpen = false;
                        ContentOverlay.Visibility = Visibility.Collapsed;
						rechargeTask (rechargeViaWallet, id, flag, amount, amount, number, stv, product);
                    };
                    
                    }
               
                }
            else if (flag.Equals ("4"))
                    {

                    Popup popup = new Popup ();
                    MobilePostpaidControl recControl = new MobilePostpaidControl ();
                    ContentOverlay.Visibility = Visibility.Visible;
                    popup.VerticalOffset = 200;
                    popup.IsOpen = true;
                    popup.Child = recControl;
                    string totalAmount = "";
                int wholeAmount=0;
                    recControl.imgClose.Tap += (a, b) =>
                    {
                        popup.IsOpen = false;
                        ContentOverlay.Visibility = Visibility.Collapsed;
                    };
                    recControl.textMessage.Text = "Make payment of your mobile bill " + number + ".";
                   
                    recControl.textAmount.TextChanged += (s, e) =>
                    {
                        recControl.textService.Text = "Complete transaction using";
                        recControl.textService.Foreground =new SolidColorBrush( Constant.ConvertStringToColor ("#53419A"));
                    amount = recControl.textAmount.Text;
                     
                     if (amount.Length != 0)
                         {
                         
                         totalAmount = CalculateTotalAmount (recControl.textService, amount, id);
                         wholeAmount=Convert.ToInt32(totalAmount);
                         recAmount = Convert.ToInt32 (totalAmount);
                         recControl.textService.Visibility = Visibility.Visible;
                         }
                     else
                         {
                         recControl.textService.Text = "Complete transaction using";
                         recControl.textService.Foreground = new SolidColorBrush (Constant.ConvertStringToColor ("#53419A"));
                        
                         }

                    if (recAmount > mainBalance || mainBalance == 0)
                        {
                        rechargeViaWallet = false;
                        recControl.buttonImageOk.Content = "Pay via card or Netbanking";
                        recControl.textCCDC.Text = "Cash Topup? Locate shop";
                        recControl.textCCDC.Tap += (a, b) =>
                        {
                            popup.IsOpen = false;
                            ContentOverlay.Visibility = Visibility.Collapsed;
                            (Application.Current.RootVisual as PhoneApplicationFrame).Navigate (new Uri ("/ShopLocator.xaml?no-cache=" + Guid.NewGuid (), UriKind.Relative));

                            (Application.Current.RootVisual as PhoneApplicationFrame).RemoveBackEntry ();
                           
                        };
                        }
                    else
                        {
                        rechargeViaWallet = true;
                        recControl.buttonImageOk.Content = "Continue to payment";
                        recControl.textCCDC.Text = "Pay via  Credit/Debit Card";
                        recControl.textCCDC.Tap += (a, b) =>
                        {
                            popup.IsOpen = false;
                            ContentOverlay.Visibility = Visibility.Collapsed;
                            if (recControl.textAmount.Text.Length != 0)
                                    {
                                    if (wholeAmount > Convert.ToInt32 (min))
                                        {
                                        if (wholeAmount < Convert.ToInt32 (max))
                                            {
											rechargeTask (rechargeViaWallet, id, flag, totalAmount, amount, number, stv, product);
                                            }
                                        else
                                            {
                                            recControl.textService.Text = "Maximum amount should be less than " + max;
                                            recControl.textService.Foreground = new SolidColorBrush (Colors.Red);
                                            }
                                        }
                                    else
                                        {
                                        recControl.textService.Text = "Minimum amount should be more than " + min;
                                        recControl.textService.Foreground = new SolidColorBrush (Colors.Red);
                                        }
                                   
                                    }
                                else
                                    {
                                recControl.textService.Text = "Please enter amount";
                                recControl.textService.Foreground = new SolidColorBrush (Colors.Red);
                                    }
                        };
                        }
                    

                    

                    };
                    recControl.buttonImageOk.Click += (s, e) =>
                    {
                        popup.IsOpen = false;
                        ContentOverlay.Visibility = Visibility.Collapsed;
                       // string totalAmount = CalculateTotalAmount (recControl.textService,amount, id);



                        if (recControl.textAmount.Text.Length != 0)
                            {
                            if (wholeAmount > Convert.ToInt32 (min))
                                {
                                if (wholeAmount < Convert.ToInt32 (max))
                                    {
                                    rechargeTask (rechargeViaWallet, id, flag, totalAmount,amount, number, stv, product);
                                    }
                                else
                                    {
                                    recControl.textService.Text = "Maximum amount should be less than " + max;
                                    recControl.textService.Foreground = new SolidColorBrush (Colors.Red);
                                    }
                                }
                            else
                                {
                                recControl.textService.Text = "Minimum amount should be more than " + min;
                                recControl.textService.Foreground = new SolidColorBrush (Colors.Red);
                                }

                            }
                        else
                            {
                            recControl.textService.Text = "Please enter amount";
                            recControl.textService.Foreground = new SolidColorBrush (Colors.Red);
                            }
                    };
                   

                    }
                
                
            }


        String charges_slab = null;
        string max, min;
        double service_charge_amount = 0, service_charge_percent = 0, service_tax_percent = 0, dblMin = 0, dblMax = 0, dblAmount = 0;
        private string CalculateTotalAmount (TextBlock txtBlk, string amount, string opId)
            {
            try
                {
                dblAmount = Convert.ToDouble (amount);
                SQLiteCommand cmd1 = db.CreateCommand ("SELECT *  FROM OperatorTable WHERE id='" + opId + "' AND flag='4'");
                var lst = cmd1.ExecuteQuery<OperatorDB> ();

                List<OperatorDB> result = lst.ToList ();

                if (result.Count != 0)
                    {
                    charges_slab = result[0].charges_slab;
                    service_charge_amount = Convert.ToDouble (result[0].service_charge_amount);
                    service_charge_percent = Convert.ToDouble (result[0].service_charge_percent);
                    service_tax_percent = Convert.ToDouble (result[0].service_tax_percent);
                    dblMin = Convert.ToDouble (result[0].min);
                    dblMax = Convert.ToDouble (result[0].max);
                    max = result[0].max;
                    min = result[0].min;
                    }
                }
            catch (SQLiteException se)
                {
                string see = se.Message;
                }

            int slab_amount = 0;
            if (string.IsNullOrEmpty (charges_slab) == true)
                {
                dblAmount = (dblAmount + (dblAmount * service_charge_percent));
                dblAmount = (dblAmount + (dblAmount * service_tax_percent));
                }
            else
                {
                String[] comma_seperator = charges_slab.Split (',');

                foreach (String s in comma_seperator)
                    {
                    String[] colon_sperator = s.Split (':');
                    if (dblAmount >= Convert.ToDouble (colon_sperator[0]))
                        {
                        slab_amount = Convert.ToInt32 (colon_sperator[1]);
                        }
                    }
                }
            double tot = dblAmount + slab_amount;
            txtBlk.Text = "( " + tot + " = " + amount + " + " + slab_amount + " service charge inclusive of all taxes.)";
            return Convert.ToString(dblAmount + slab_amount);
           
            

            }


        private async void rechargeTask (bool rechargeViaWallet, string id, string flag, string amount,string postPaidamount, string number, string stv, string product)
            {

            //Constant.SetProgressIndicator(true);
            ProgressBarManager.Show (1, "Please Wait", false, 10);
            var values = new List<KeyValuePair<string, string>> ();


            if (rechargeViaWallet)
                {
                values.Add (new KeyValuePair<string, string> ("paymentopt", "wallet"));
                }
            else
                {
                values.Add (new KeyValuePair<string, string> ("paymentopt", "online"));
                }
            values.Add (new KeyValuePair<string, string> ("payment", "1"));
            values.Add (new KeyValuePair<string, string> ("api", "true"));
            values.Add (new KeyValuePair<string, string> ("name", ""));
            values.Add (new KeyValuePair<string, string> ("operator", id));
            values.Add (new KeyValuePair<string, string> ("flag", flag));


            if (flag.Equals ("1") || flag.Equals ("3"))
                {
                values.Add (new KeyValuePair<string, string> ("actiontype", "recharge"));
                values.Add (new KeyValuePair<string, string> ("amount", amount));
                values.Add (new KeyValuePair<string, string> ("mobile_number", number));
                values.Add (new KeyValuePair<string, string> ("stv", stv));
                values.Add (new KeyValuePair<string, string> ("recharge", "1"));
                }
            else if (flag.Equals ("2"))
                {
                values.Add (new KeyValuePair<string, string> ("actiontype", "recharge"));
                values.Add (new KeyValuePair<string, string> ("amount", amount));
                values.Add (new KeyValuePair<string, string> ("subscriber_id", number));
				values.Add (new KeyValuePair<string, string> ("recharge", "1"));
                }
            else if (flag.Equals ("4"))
                {
                //string quick_flag = PhoneApplicationService.Current.State[Constant.OPERATOR_RECHARGE_FOR].ToString ();
                //string totallRechargeAmount = "", charges_slab = "", service_tax_percent = "";
                values.Add (new KeyValuePair<string, string> ("actiontype", "billpayment"));
				values.Add (new KeyValuePair<string, string> ("amount", amount));
                values.Add (new KeyValuePair<string, string> ("payment_flag", "1"));
                values.Add (new KeyValuePair<string, string> ("billpayment", "1"));
				values.Add (new KeyValuePair<string, string> ("base_amount", postPaidamount));
                values.Add (new KeyValuePair<string, string> ("service_charge", "" + charges_slab));
                values.Add (new KeyValuePair<string, string> ("service_tax", "" + service_tax_percent));
                values.Add (new KeyValuePair<string, string> ("mobile_number", number));
                }

            FormUrlEncodedContent values1 = new FormUrlEncodedContent (values);
            Task<string> response = Constant.postParamsAndgetResponse (Constant.B2C_URL, values1);

            string result = await response;
            if (string.IsNullOrEmpty (result))
                {
                NavigationService.Navigate (new Uri ("/NoConnection.xaml?", UriKind.Relative));
                }
            else
                {
                var dict = (JObject)JsonConvert.DeserializeObject (result);
                string status = dict["status"].ToString ();

                if (rechargeViaWallet)
                    {
                    if (status.Equals ("success"))
                        {
                        var description = dict["description"];
                        Constant.setbalance (description["closing_balance"].ToString ());

                        Constant.ShowSuccessPopUp (ContentOverlay, flag, number, amount, id, product);
                        }
                    else
                        {
                        string errCode = dict["errCode"].ToString ();
                        if (errCode.Equals ("201"))
                            {
                            (Application.Current.RootVisual as PhoneApplicationFrame).Navigate (new Uri ("/Login.xaml?no-cache=" + Guid.NewGuid (), UriKind.Relative));

                            (Application.Current.RootVisual as PhoneApplicationFrame).RemoveBackEntry ();

                            //return responseString.ToString();
                            }
                        else
                            {
							Constant.checkForErrorCode (result);//MessageBox.Show( dict["description"].ToString());
                            }
                        }
                    }
                else
                    {
                    if (status.Equals ("success"))
                        {
                        var description = dict["description"];
                        string form_content = description["form_content"].ToString ();
                        PhoneApplicationService.Current.State[Constant.PAYMENT_GATEWAY_HTML] = form_content;
                        NavigationService.Navigate (new Uri ("/PaymentGatewayLayout.xaml?page=HomePage", UriKind.Relative));
                        //NavigationService.RemoveBackEntry ();
                        }
                    else
                        {
                        string errCode = dict["errCode"].ToString ();
                        if (errCode.Equals ("201"))
                            {
                            (Application.Current.RootVisual as PhoneApplicationFrame).Navigate (new Uri ("/Login.xaml?no-cache=" + Guid.NewGuid (), UriKind.Relative));

                            (Application.Current.RootVisual as PhoneApplicationFrame).RemoveBackEntry ();

                            //return responseString.ToString();
                            }
                        else
                            {
							Constant.checkForErrorCode (result); //MessageBox.Show (dict["description"].ToString ());
                            }
                        }
                    }
               
                }
            ProgressBarManager.Hide (1);
           }




      
        private void StartLoadingData ()
            {
            BackgroundWorker backroungWorker = new BackgroundWorker ();
            backroungWorker.DoWork += new DoWorkEventHandler (backroungWorker_DoWork);
            //backroungWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(backroungWorker_RunWorkerCompleted);

            backroungWorker.RunWorkerAsync ();
            }

        string lattitude, longitude;
        private void backroungWorker_DoWork (object sender, DoWorkEventArgs e)
            {
			try
				{

				SQLiteCommand cmd11 = db.CreateCommand ("SELECT *  FROM OperatorTable");
				var lst = cmd11.ExecuteQuery<OperatorDB> ();
				List<OperatorDB> queryResult = lst.ToList ();
				if (queryResult.Count <= 1)
					{
					DateTime currentDateOp = DateTime.Now;//.ToString();
					DateTime updatedDateOp = Constant.LoadPersistent<DateTime> (Constant.OPERATOR_LIST_UPDATE_TIME);
					TimeSpan durationOp = currentDateOp - updatedDateOp;
					if (durationOp.Days >= 1)
						{
						SQLiteCommand cmd2 = db.CreateCommand ("DELETE FROM OperatorTable");
						int i = cmd2.ExecuteNonQuery ();
						GetAndSaveOperator ();
						}
					}
				//GetAndSaveOperator ();
				}
			catch (SQLiteException se)
				{
				string see = se.Message;
				}
            if (watcher == null)
                {
                watcher = new GeoCoordinateWatcher (GeoPositionAccuracy.High); // Use high accuracy. 
                watcher.MovementThreshold = 20; // Use MovementThreshold to ignore noise in the signal. 
                watcher.StatusChanged += new EventHandler<GeoPositionStatusChangedEventArgs> (watcher_StatusChanged);
                if (watcher.Permission == GeoPositionPermission.Denied)
                    {
                    //Location services is disable on the phone. Show a message to the user.
                    MessageBox.Show ("Your location is not enabled.");

                    }
                else
                    {
                    watcher.Start ();
                    }
                }

                        SQLiteCommand cmd1 = db.CreateCommand ("SELECT * FROM AllPlans");
                        var lst11 = cmd1.ExecuteQuery<AllPlans> ();
                        List<AllPlans> result = lst11.ToList ();
                        if (result.Count >= 1)
                            {
                            DateTime CurrentTime = DateTime.Now;
                            DateTime EndTime = Constant.LoadPersistent<DateTime> (Constant.PLAN_INSERT_TIME);
                            if (EndTime != null)
                                {
                                TimeSpan ts = EndTime - CurrentTime;

                                if (ts.Days > 2)
                                    {
                                 
                                    if (Constant.checkInternetConnection ())
                                        {
                                        getAllPlan ();
                                        }
                                    }
                                }
                            }
                        else
                            {
                            if (Constant.checkInternetConnection ())
                                {
                                getAllPlan ();
                                }
                            }




						getDealsTask (Constant.LoadPersistent<string> (Constant.SETTINGS_USER_LATTITUDE), Constant.LoadPersistent<string> (Constant.SETTINGS_USER_LONGITUDE));
                       getQuickPayTask ();
           
            }



        AllPlans CirclePlans = new AllPlans ();
        private async void getAllPlan ()
            {

            String result = "";
            var httpClient = new HttpClient (new HttpClientHandler ());

            HttpResponseMessage response = await httpClient.PostAsync (Constant.B2B_URL + "method=getPlanDetails&operator=all&circle=all", null);
            response.EnsureSuccessStatusCode ();
            long cl = (long)response.Content.Headers.ContentLength;
            var responseString = await response.Content.ReadAsStringAsync ();
            int count = 0;
            try
                {
                if (cl < applicationPeakMemoryUsage)
                    {
                    //db.Open();


                    string output = responseString.Remove (responseString.Length - 1, 1);
                    string output1 = output.Remove (output.Length - 1, 1);
                    string output2 = output1.Remove (0, 1);
                    string output3 = output2.Remove (0, 1);
                    string output4 = output3.Remove (output3.Length - 1, 1);

                    var dict = (JObject)JsonConvert.DeserializeObject (output4);
                    int cnt = dict.Count;
					int ii=0;
                    DateTime StartTime = DateTime.Now;
                    foreach (var operatorKey in dict)
                        {

						  ii++;
                        var desc = dict[operatorKey.Key];


                        var prod_code_pay1 = desc["prod_code_pay1"];
                        CirclePlans.allPlanOpertorID = prod_code_pay1.ToString ();

                        var opr_name = desc["opr_name"];
                        CirclePlans.allPlanOperatorName = opr_name.ToString ();

                        JObject circles = (JObject)desc["circles"];
                        foreach (var circleKey in circles)
                            {

                            
                            var BR = circles[circleKey.Key];
                            var circle_id = BR["circle_id"];
                            CirclePlans.allPlanCircleID = circle_id.ToString ();
                            var circle_name = BR["circle_name"];
                            CirclePlans.allPlanCircleName = circle_name.ToString ();
                            var plans = BR["plans"];
                            CirclePlans.allCirclePlans = plans.ToString ();
							try
								{
								SQLiteCommand cmd = db.CreateCommand ("");
								cmd.CommandText = "Insert into AllPlans (allPlanOpertorID,allPlanOperatorName,allPlanCircleID,allPlanCircleName,allCirclePlans) values (@allPlanOpertorID,@allPlanOperatorName,@allPlanCircleID,@allPlanCircleName,@allCirclePlans)";
								int resInserted = cmd.ExecuteNonQuery (CirclePlans);
								}
							catch (Exception se)
								{
								System.Diagnostics.Debug.WriteLine (se.Message);
								
								}

                            }

						System.Diagnostics.Debug.WriteLine (ii + " Circle  inserted && time taken " + ii);
                        }

                    DateTime EndTime = DateTime.Now;
                    //Constant.SavePersistent (Constant.PLAN_INSERT_TIME, EndTime);
                    TimeSpan ts = EndTime - StartTime;
                    //System.Diagnostics.Debug.WriteLine (countInserted + " Circle  inserted && time taken " + ts.Seconds);
                    }
                }
            catch (Exception e)
                {
                Console.WriteLine ("");
                }


            }

        private void watcher_StatusChanged (object sender, GeoPositionStatusChangedEventArgs e)
            {
            if (e.Status == GeoPositionStatus.Ready)
                {
                // Use the Position property of the GeoCoordinateWatcher object to get the current location. 
                GeoCoordinate co = watcher.Position.Location;
                latti = co.Latitude.ToString ("0.000");
                longi = co.Longitude.ToString ("0.000");
                Constant.SavePersistent (Constant.SETTINGS_USER_LATTITUDE, latti);
                Constant.SavePersistent (Constant.SETTINGS_USER_LONGITUDE, longi);
                WebClient client = new WebClient ();

                client.DownloadStringCompleted += client_DownloadStringCompleted;

                string Url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=" + latti + "," + longi + "&sensor=true";
                client.DownloadStringAsync (new Uri (Url, UriKind.RelativeOrAbsolute));

                watcher.Stop ();
                }
            }


        private void client_DownloadStringCompleted (object sender, DownloadStringCompletedEventArgs e)
            {
			try
				{
				var getResult = e.Result;
				JObject parseJson = JObject.Parse (getResult);
				var getJsonres = parseJson["results"][0];
				var getJson = getJsonres["address_components"][1];
				var getJson2 = getJsonres["address_components"][2];
				var getJson3 = getJsonres["address_components"][3];
				var sub_area = getJson["long_name"];
				var area = getJson["long_name"];
				var address = getJson["long_name"];
				//Address = sub_area.ToString() + ", " + area.ToString() + ", " + address.ToString();
				currentAddress = getJsonres["formatted_address"].ToString ();
				}
			catch (Exception ee)
				{
				}

            }


        private async void CheckForUpdatedVersion ()
            {
            var currentVersion = new Version (GetManifestAttributeValue ("Version"));
            var updatedVersion = await GetUpdatedVersion1 ();

            if (updatedVersion > currentVersion
                && MessageBox.Show ("Do you want to install the new version now?", "Pay1 App New Update Available", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
                {
                new MarketplaceDetailTask ().Show ();
                }
            }


        public static string GetManifestAttributeValue (string attributeName)
            {
            var xmlReaderSettings = new XmlReaderSettings
            {
                XmlResolver = new XmlXapResolver ()
            };

            using (var xmlReader = XmlReader.Create ("WMAppManifest.xml", xmlReaderSettings))
                {
                xmlReader.ReadToDescendant ("App");

                return xmlReader.GetAttribute (attributeName);
                }
            }

        private async Task<Version> GetUpdatedVersion1 ()
            {
            var cultureInfoName = CultureInfo.CurrentUICulture.Name;

            var url = string.Format ("http://marketplaceedgeservice.windowsphone.com/v8/catalog/apps/{0}?os={1}&cc={2}&oc=&lang={3}​",
                GetManifestAttributeValue ("ProductID"),
                Environment.OSVersion.Version,
                cultureInfoName.Substring (cultureInfoName.Length - 2).ToUpperInvariant (),
                cultureInfoName);

            var request = WebRequest.Create (url);
            try
                {
                WebResponse response1 = await request.GetResponseAsync ();


                using (var outputStream = response1.GetResponseStream ())
                    {
                    using (var reader = XmlReader.Create (outputStream))
                        {
                        reader.MoveToContent ();

                        var aNamespace = reader.LookupNamespace ("a");

                        reader.ReadToFollowing ("entry", aNamespace);

                        reader.ReadToDescendant ("version");

                        return new Version (reader.ReadElementContentAsString ());
                        }
                    }
                }
            catch (WebException e)
                {
                return new Version ("1.0.0.0");
                }
            }


       

        private async void getQuickPayTask ()
            {

            var values = new List<KeyValuePair<string, string>> ();
            values.Add (new KeyValuePair<string, string> ("actiontype", "get_quickpaylist"));
           
            values.Add (new KeyValuePair<string, string> ("api", "true"));
            FormUrlEncodedContent values1 = new FormUrlEncodedContent (values);
            Task<string> response = Constant.postParamsAndgetResponse (Constant.B2C_URL, values1);

            string result = await response;
            if (string.IsNullOrEmpty (result))
                {
                NavigationService.Navigate (new Uri ("/NoConnection.xaml?", UriKind.Relative));
                }
            else
                {
                var dict = (JObject)JsonConvert.DeserializeObject (result);
                string status = dict["status"].ToString ();

                if (status.Equals ("success"))
                    {
					
						try
							{
							//SQLiteCommand cmd2 = db.CreateCommand ("DELETE FROM QuickPayData");
							//int i = cmd2.ExecuteNonQuery ();
							string strDel = " DELETE * FROM QuickPayData";
							(Application.Current as App).dbh.Delete<QuickPayData> (strDel);
							}
						catch (SQLiteException see)
							{
							}

						try
							{
							QuickPayData quickData = new QuickPayData ();
							quickData.quickPay = dict["description"].ToString ();
							Constant.setLastTransaction (dict["description"].ToString ());
							string strInsert = " Insert into QuickPayData (quickPay) values (@quickPay)";
							int rec = (Application.Current as App).dbh.Insert<QuickPayData> (quickData, strInsert);
							//cmd.CommandText = "Insert into QuickPayData (quickPay) values (@quickPay)";
							//int rec = cmd.ExecuteNonQuery (quickData);
							Constant.SavePersistent (Constant.FREEBIE_LIST_UPDATE_TIME, DateTime.Now);

							Dispatcher.BeginInvoke (delegate ()
							{
							LoadOldtransaction ();

							});
							}
						catch (SQLiteException see)
							{
							}
						
                    }
                else
                    {
                    }
                }
            }



        
			   
                               
        private void rechargesTap (object sender, RoutedEventArgs e)
            {
            NavigationService.Navigate (new Uri ("/RechargeAndBills.xaml?", UriKind.RelativeOrAbsolute));
            NavigationService.RemoveBackEntry ();
            }

        private void freeGiftTap (object sender, RoutedEventArgs e)
            {
            NavigationService.Navigate (new Uri ("/FreeBieMainLayout.xaml?", UriKind.RelativeOrAbsolute));
            NavigationService.RemoveBackEntry ();
            }

		private async void GetAndSaveOperator ()
			{
			//ProgressBarManager.Show(10, "Please wait...", false, 10);
			var values = new List<KeyValuePair<string, string>>
                        { 
                        new KeyValuePair<string, string>("actiontype", "get_all_operators"),
                        new KeyValuePair<string, string>("api","true"),
                        };

			FormUrlEncodedContent values1 = new FormUrlEncodedContent (values);
			Task<string> response = Constant.postParamsAndgetResponse (Constant.B2C_URL, values1);

			string result = await response;
			var dict = (JObject)JsonConvert.DeserializeObject (result);
			string status = dict["status"].ToString ();
			if (status.Equals ("success"))
				{
				
				var description = dict["description"];
				var mobile = description["mobile"];
				foreach (var item in mobile)
					{
					OperatorDB opeDb = new OperatorDB ();
					opeDb.id = item["id"].ToString ();
					opeDb.code = item["opr_code"].ToString ();
					opeDb.delegateFlag = item["delegate"].ToString ();
					opeDb.flag = item["flag"].ToString ();
					opeDb.name = item["name"].ToString ();
					opeDb.p_id = item["product_id"].ToString ();
					opeDb.service_id = item["service_id"].ToString ();
					opeDb.stv = item["stv"].ToString ();
					opeDb.prefix = item["prefix"].ToString ();
					opeDb.length = item["length"].ToString ();
					opeDb.min = item["min"].ToString ();
					opeDb.max = item["max"].ToString ();
					opeDb.charges_slab = item["charges_slab"].ToString ();
					opeDb.service_charge_amount = item["service_charge_amount"].ToString ();
					opeDb.service_tax_percent = item["service_tax_percent"].ToString ();
					opeDb.service_charge_percent = item["service_charge_percent"].ToString ();
					insertOperatorToDb (opeDb);
					}
				var data = description["data"];
				foreach (var item in data)
					{
					OperatorDB opeDb = new OperatorDB ();
					opeDb.id = item["id"].ToString ();
					opeDb.code = item["opr_code"].ToString ();
					opeDb.delegateFlag = item["delegate"].ToString ();
					opeDb.flag = item["flag"].ToString ();
					opeDb.name = item["name"].ToString ();
					opeDb.p_id = item["product_id"].ToString ();
					opeDb.service_id = item["service_id"].ToString ();
					opeDb.stv = item["stv"].ToString ();
					opeDb.prefix = item["prefix"].ToString ();
					opeDb.length = item["length"].ToString ();
					opeDb.min = item["min"].ToString ();
					opeDb.max = item["max"].ToString ();
					opeDb.charges_slab = item["charges_slab"].ToString ();
					opeDb.service_charge_amount = item["service_charge_amount"].ToString ();
					opeDb.service_tax_percent = item["service_tax_percent"].ToString ();
					opeDb.service_charge_percent = item["service_charge_percent"].ToString ();
					insertOperatorToDb (opeDb);
					}
				var postpaid = description["postpaid"];
				foreach (var item in postpaid)
					{
					OperatorDB opeDb = new OperatorDB ();
					opeDb.id = item["id"].ToString ();
					opeDb.code = item["opr_code"].ToString ();
					opeDb.delegateFlag = item["delegate"].ToString ();
					opeDb.flag = item["flag"].ToString ();
					opeDb.name = item["name"].ToString ();
					opeDb.p_id = item["product_id"].ToString ();
					opeDb.service_id = item["service_id"].ToString ();
					opeDb.stv = item["stv"].ToString ();
					opeDb.prefix = item["prefix"].ToString ();
					opeDb.length = item["length"].ToString ();
					opeDb.min = item["min"].ToString ();
					opeDb.max = item["max"].ToString ();
					opeDb.charges_slab = item["charges_slab"].ToString ();
					opeDb.service_charge_amount = item["service_charge_amount"].ToString ();
					opeDb.service_tax_percent = item["service_tax_percent"].ToString ();
					opeDb.service_charge_percent = item["service_charge_percent"].ToString ();
					insertOperatorToDb (opeDb);
					}
				var dth = description["dth"];
				foreach (var item in dth)
					{
					OperatorDB opeDb = new OperatorDB ();
					opeDb.id = item["id"].ToString ();
					opeDb.code = item["opr_code"].ToString ();
					opeDb.delegateFlag = item["delegate"].ToString ();
					opeDb.flag = item["flag"].ToString ();
					opeDb.name = item["name"].ToString ();
					opeDb.p_id = item["product_id"].ToString ();
					opeDb.service_id = item["service_id"].ToString ();
					opeDb.stv = item["stv"].ToString ();
					opeDb.prefix = item["prefix"].ToString ();
					opeDb.length = item["length"].ToString ();
					opeDb.min = item["min"].ToString ();
					opeDb.max = item["max"].ToString ();
					opeDb.charges_slab = item["charges_slab"].ToString ();
					opeDb.service_charge_amount = item["service_charge_amount"].ToString ();
					opeDb.service_tax_percent = item["service_tax_percent"].ToString ();
					opeDb.service_charge_percent = item["service_charge_percent"].ToString ();
					insertOperatorToDb (opeDb);
					}
				Constant.SavePersistent (Constant.OPERATOR_LIST_UPDATE_TIME, DateTime.Now);
				}
			// ProgressBarManager.Hide(10);
			}

		public void insertOperatorToDb (OperatorDB opeDb)
			{
			if (db != null)
				{
				try
					{
					SQLiteCommand cmd = db.CreateCommand ("");

					cmd.CommandText = "Insert into OperatorTable (id,name,code,p_id,stv,flag,service_id,delegateFlag,min,max,charges_slab,service_charge_amount,service_tax_percent,service_charge_percent,prefix,length) values (@id,@name,@code,@p_id,@stv,@flag,@service_id,@delegateFlag,@min,@max,@charges_slab,@service_charge_amount,@service_tax_percent,@service_charge_percent,@prefix,@length)";
					int rec = cmd.ExecuteNonQuery (opeDb);
					}
				catch (SQLiteException exc)
					{
					System.Diagnostics.Debug.WriteLine (exc.Message);
					}
				}
			}

		private void Canvas_Tap_1 (object sender, System.Windows.Input.GestureEventArgs e)
			{

			}

		private async void getDealsTask (string lattitude, string longitude)
			{

			var values = new List<KeyValuePair<string, string>> ();
			values.Add (new KeyValuePair<string, string> ("actiontype", "Get_all_deal_data"));
			values.Add (new KeyValuePair<string, string> ("longitude", longitude));
			values.Add (new KeyValuePair<string, string> ("latitude", lattitude));
			values.Add (new KeyValuePair<string, string> ("mobile", Constant.getUserNumber ()));
			values.Add (new KeyValuePair<string, string> ("api", "true"));
			FormUrlEncodedContent values1 = new FormUrlEncodedContent (values);
			Task<string> response = Constant.postParamsAndgetResponse (Constant.B2C_URL, values1);
			try
				{
				string result = await response;

				if (string.IsNullOrEmpty (result))
					{
					NavigationService.Navigate (new Uri ("/NoConnection.xaml?", UriKind.Relative));
					}
				else
					{
					var dict = (JObject)JsonConvert.DeserializeObject (result);
					string status = dict["status"].ToString ();

					if (status.Equals ("success"))
						{
						if (db != null)
							{
							try
								{
								SQLiteCommand cmd = db.CreateCommand ("Insert into FreeBieDataBase (freeBie) values (@freeBie)");
								SQLiteCommand cmd2 = db.CreateCommand ("DELETE FROM FreeBieDataBase");
								int i = cmd2.ExecuteNonQuery ();
								FreeBieDataBase tst = new FreeBieDataBase ();
								tst.freeBie = dict["description"].ToString ();
								//cmd.CommandText = "Insert into FreeBieDataBase (freeBie) values (@freeBie)";
								//string strInsert = "Insert into FreeBieDataBase (freeBie) values (@freeBie)";
								//int rec = (Application.Current as App).dbh.Insert<FreeBieDataBase> (tst, strInsert);
								int rec = cmd.ExecuteNonQuery (tst);
								Constant.SavePersistent (Constant.FREEBIE_LIST_UPDATE_TIME, DateTime.Now);
								}
							catch (SQLiteException exc)
								{
								System.Diagnostics.Debug.WriteLine (exc.Message);
								}
							catch (Exception eee)
								{
								System.Diagnostics.Debug.WriteLine (eee.Message);
								}
							}
						}
					else
						{
						}



					}
				}
			catch (Exception www)
				{
				}
			}


        }
    }