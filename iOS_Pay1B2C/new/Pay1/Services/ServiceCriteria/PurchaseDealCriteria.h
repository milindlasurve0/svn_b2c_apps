//
//  PurchaseDealCriteria.h
//  Pay1
//
//  Created by Annapurna on 23/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PurchaseDealCriteria : NSObject

@property (nonatomic, strong) NSString *dealID;
@property (nonatomic, strong) NSString *offerID;
@property (nonatomic, strong) NSString *couponID;
@property (nonatomic, strong) NSString *paymentOption;
@property (nonatomic, strong) NSString *amount;
@property (nonatomic, strong) NSString *quantity;
@property (nonatomic, strong) NSString *transCategory;
@property (nonatomic, strong) NSString *freebie;

@property (nonatomic, strong) NSString *partial;
@property (nonatomic, strong) NSString *refID;
@property (nonatomic, strong) NSString *api;
@property (nonatomic, strong) NSString *offerPrice;

@property (nonatomic, assign) BOOL isFromEVoucher;







@end
