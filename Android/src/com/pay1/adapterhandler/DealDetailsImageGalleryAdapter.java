package com.pay1.adapterhandler;

import java.util.ArrayList;
import java.util.WeakHashMap;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.pay1.DealDetailsActivity;
import com.pay1.R;
import com.pay1.imagehandler.ImageLoader;

public class DealDetailsImageGalleryAdapter extends PagerAdapter {

	private Context context;
	private ArrayList<WeakHashMap<String, String>> path;
	private LayoutInflater inflater;
	private ImageLoader imageLoader;

	// constructor
	public DealDetailsImageGalleryAdapter(Context context,
			ArrayList<WeakHashMap<String, String>> path) {
		this.context = context;
		this.path = path;
		imageLoader = new ImageLoader(context);
	}

	@Override
	public int getCount() {
		return this.path.size();
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return view == ((RelativeLayout) object);
	}

	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		ImageView imgDisplay;
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View viewLayout = inflater.inflate(
				R.layout.deal_detail_gallery_adapter, container, false);

		imgDisplay = (ImageView) viewLayout.findViewById(R.id.imgDisplay);

		imageLoader.getDealImage(
				path.get(position).get(DealDetailsActivity.GALARY_URL),
				imgDisplay);
		((ViewPager) container).addView(viewLayout);

		return viewLayout;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		// ((ViewPager) container).removeView((LinearLayout) object);

	}

}