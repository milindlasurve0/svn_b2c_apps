//
//  TransactionHistoryViewController.m
//  Pay1
//
//  Created by webninjaz on 19/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "TransactionHistoryViewController.h"
#import "TransactionHistoryView.h"
#import "TransactionHistoryService.h"
#import "TransactionHistoryCriteria.h"
#import "TransactionHistoryResponse.h"
#import "TransactionCustomCell.h"
#import "TransactionList.h"
#import "TransactionHistoryDetailView.h"

@interface TransactionHistoryViewController ()

@end

@implementation TransactionHistoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    transactionTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

    
    _transactionArray = [[NSMutableArray alloc]init];
    
    transValue = 0;
    

    
  /*  if([UIScreen mainScreen].bounds.size.width ==  320){
    transactionImageView.frame = CGRectMake([UIScreen mainScreen].bounds.size.width /2 -50, [UIScreen mainScreen].bounds.size.height/2 -100, 150, 150);
    }*/
    
    transactionTableView.frame = CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height - 70);
    
    [transactionTableView removeFromSuperview];
    
    [self setNaviationButtonWithText:@"Transaction History" selector:@selector(backClicked:) isLeft:TRUE isReduceSize:TRUE isMyFont:18.0];

    [self callTransactionHistoryServiceBlock];
   
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.transactionArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"TransactionCustomCell";
    
    transactionCustomCell=(TransactionCustomCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(transactionCustomCell==nil){
        [[NSBundle mainBundle] loadNibNamed:@"TransactionCustomCell" owner:self options:nil];
    }
    
    TransactionList *transactionData = [self.transactionArray objectAtIndex:indexPath.row];
    transactionCustomCell.transactionPhoneNumberLabel.text = transactionData.transactionPhoneNumber;
    transactionCustomCell.transactionMoneyLabel.text = [NSString stringWithFormat:@"Rs. %@.00",transactionData.transactionAmount];
    
    
    [self setRoundedCornerForImageView:transactionCustomCell.transactionImageView];
    
    transactionCustomCell.transactionImageView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    transactionCustomCell.transactionImageView.layer.borderWidth = 1;

    
    
    
    if ([transactionData.transactionStatus isEqualToString:@"1"]) {
        transactionCustomCell.transactionCorrectImageView.image = [UIImage imageNamed:@""];
    }
    
    if ([transactionData.transactionStatus isEqualToString:@"2"]) {
        transactionCustomCell.transactionCorrectImageView.image = [UIImage imageNamed:@"green_check_button.png"];
    }
    
    if ([transactionData.transactionStatus isEqualToString:@"3"] || [transactionData.transactionStatus isEqualToString:@"4"]) {
        transactionCustomCell.transactionCorrectImageView.image = [UIImage imageNamed:@"red_circle.png"];
    }

    
    if([transactionData.transactionOperatorName isEqual:[NSNull null]]){
        NSLog(@"Its Null");
    }
    
    else{
        
      //  NSLog(@"#OperatorName :%@",transactionData.transactionOperatorName);
        
    transactionCustomCell.operatorLogoImageView.image = [UIImage imageNamed:[self giveLogoImageAccordingToOperator:transactionData.transactionOperatorName]];
        
     
    
    }
    
    
    if ([transactionData.transactionServiceID isEqualToString:@"98"] ) {
    transactionCustomCell.operatorLogoImageView.image = [UIImage imageNamed:@"wallet_top_ups.png"];
    transactionCustomCell.transactionTopUpsLabel.text = @"WALLET TOPUP";

    }
    if ([transactionData.transactionServiceID isEqualToString:@"1"] ) {
        transactionCustomCell.transactionTopUpsLabel.text = @"PREPAID RECHARGE";
        
    }
    
    if ([transactionData.transactionServiceID isEqualToString:@"4"] ) {
        transactionCustomCell.transactionTopUpsLabel.text = @"POSTPAID BILL";
        
    }
    
    if ([transactionData.transactionServiceID isEqualToString:@"2"] ) {
        transactionCustomCell.transactionTopUpsLabel.text = @"DTH RECHARGE";
        
    }
    
    if ([transactionData.transactionServiceID isEqualToString:@"5"] ) {
        transactionCustomCell.transactionTopUpsLabel.text = @"DATA CARD";
        
    }
    
    if ([transactionData.transactionServiceID isEqualToString:@"99"] ) {
        transactionCustomCell.transactionTopUpsLabel.text = @"UNKNOWN";
        
    }
    
    
    return transactionCustomCell;
}

-(void)callTransactionHistoryServiceBlock{
    [self showHUDOnView];
    TransactionHistoryService *transactionHistoryService = [[TransactionHistoryService alloc]init];
    [transactionHistoryService callTransactionHistoryService:self.transactionHistoryCriteria callback:^(NSError *error, TransactionHistoryResponse *transactionHistoryResponse) {
        
    if(transactionHistoryResponse.transactionHistoryResponse.count == 0){
    NSLog(@"The Status get error");
    [transactionTableView removeFromSuperview];

    [self.view addSubview:transactionImageView];
    [self.view addSubview:transactionLabel];
        
    [self hideHUDOnView];
            
    }
    
    else{
        [self hideHUDOnView];
    NSLog(@"The status is Success %lu",(unsigned long)transactionHistoryResponse.transactionHistoryResponse.count);
  //  self.transactionArray = transactionHistoryResponse.transactionHistoryResponse;
        
    for(int i=0;i<transactionHistoryResponse.transactionHistoryResponse.count;i++){
    TransactionList *transactionList = [transactionHistoryResponse.transactionHistoryResponse objectAtIndex:i];
    [self.transactionArray addObject:transactionList];
    }
        
    if(transactionHistoryResponse.transactionHistoryResponse.count > 9){
    bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, transactionTableView.frame.origin.y + transactionTableView.frame.size.height, transactionTableView.frame.size.width, 40)];
    bottomButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    bottomButton.frame = CGRectMake(0,0 , bottomView.frame.size.width, bottomView.frame.size.height);
    bottomButton.backgroundColor = [UIColor colorWithRed:36/255.0 green:182/255.0 blue:82/255.0 alpha:1.0]; //[UIColor greenColor];

    [bottomButton setTitle:@"Load More Transactions" forState:UIControlStateNormal];
    [bottomButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [bottomButton addTarget:self action:@selector(buttonLoadMoreTouched) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:bottomButton];
    transactionTableView.tableFooterView = bottomView;

    }
    else{
    [bottomView removeFromSuperview];
    }
        
    [self.view addSubview:transactionTableView];
    [transactionTableView reloadData];
    }
        
    }];
    
}


-(TransactionHistoryCriteria *)transactionHistoryCriteria{
    
    TransactionHistoryCriteria *criteria = [[TransactionHistoryCriteria alloc]init];
    criteria.transactionPageCriteria = [NSString stringWithFormat:@"%d",transValue];//@"0";
    criteria.transactionLimitCriteria = @"10";
    
    return criteria;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    TransactionList *transactionData = [self.transactionArray objectAtIndex:indexPath.row];
    NSLog(@"The transaction data is%@",transactionData);
    TransactionHistoryDetailView *transactionHistoryVC = [self.storyboard instantiateViewControllerWithIdentifier:@"TransactionHistoryDetailView"];
    [transactionHistoryVC setTransactionList:transactionData];
    [self.navigationController pushViewController:transactionHistoryVC animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath;
{
    return 80;
}

-(void)buttonLoadMoreTouched{
    NSLog(@"#ReloadMore");
    
    [bottomButton setTitle:@"Loading..." forState:UIControlStateNormal];

    
    [self showHUDOnView];
    
    transValue++;
    
    [self callTransactionHistoryServiceBlock];
}

-(void)backClicked:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
