package com.pay1;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.WeakHashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.pay1.customviews.MyProgressDialog;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.pay1.constants.Constants;
import com.pay1.requesthandler.RequestClass;
import com.pay1.utilities.EditTextValidator;
import com.pay1.utilities.Utility;

public class CouponCodeActivity extends AppBaseActivity {

	private static final String SCREEN_LABEL = "Coupon Code Screen";
	private EasyTracker easyTracker = null;

	private final String TAG = "Wallet Topup";
	private EditText editText_CouponCode;
	private ImageView imageView_Ok, imageView_Cancel, imageView_Back;
	private TextView textView_Title, textView_TitleCode;

	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.couponcode_activity);
		try {
			easyTracker = EasyTracker.getInstance(CouponCodeActivity.this);
			easyTracker.set(Fields.SCREEN_NAME, SCREEN_LABEL);
			registerBaseActivityReceiver();

			Typeface Normal = Typeface.createFromAsset(getAssets(),
					"EncodeSansNormal-100-Thin.ttf");

			Typeface Reguler = Typeface.createFromAsset(getAssets(),
					"EncodeSansNormal-400-Regular.ttf");

			textView_Title = (TextView) findViewById(R.id.textView_Title);
			textView_Title.setTypeface(Reguler);
			textView_TitleCode = (TextView) findViewById(R.id.textView_TitleCode);
			textView_TitleCode.setTypeface(Reguler);

			editText_CouponCode = (EditText) findViewById(R.id.editText_CouponCode);
			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(editText_CouponCode.getWindowToken(), 0);
			editText_CouponCode.setTypeface(Normal);
			try {
				Bundle bundle = getIntent().getExtras();
				if (bundle != null) {
					String str = bundle.getString("AMOUNT");
					editText_CouponCode.setText(str);
				}
			} catch (Exception e) {

			}
			editText_CouponCode.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					// TODO Auto-generated method stub
					if (editText_CouponCode.getText().toString().trim()
							.length() > 0)
						EditTextValidator.hasText(CouponCodeActivity.this,
								editText_CouponCode,
								Constants.ERROR_COUPON_BLANK_FIELD);
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					// TODO Auto-generated method stub

				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

				}
			});

			imageView_Ok = (ImageView) findViewById(R.id.imageView_Ok);
			imageView_Ok.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					// TODO Auto-generated method stub
					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

					if (EditTextValidator.hasText(CouponCodeActivity.this,
							editText_CouponCode,
							Constants.ERROR_COUPON_BLANK_FIELD)) {
						new WalletTopupViaCouponTask()
								.execute(editText_CouponCode.getText()
										.toString().trim());
					}
				}
			});
			imageView_Cancel = (ImageView) findViewById(R.id.imageView_Cancel);
			imageView_Cancel.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					editText_CouponCode.setText("");
				}
			});
			imageView_Back = (ImageView) findViewById(R.id.imageView_Back);
			imageView_Back.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onBackPressed();
				}
			});

		} catch (Exception e) {

		}
		overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}

	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView().build());
		// EasyTracker.getInstance(this).activityStart(this);
		easyTracker.send(MapBuilder.createAppView().build());
	}

	@Override
	public void onStop() {
		super.onStop();
		// EasyTracker.getInstance(this).activityStop(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unRegisterBaseActivityReceiver();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (Constants.isExpanded) {
			Constants.collapsView(this);
		} else {
			super.onBackPressed();
			overridePendingTransition(R.anim.slide_in_left,
					R.anim.slide_out_right);
		}
	}

	public class WalletTopupViaCouponTask extends
			AsyncTask<String, String, String> implements OnDismissListener {
		private MyProgressDialog dialog;
		String url;

		@Override
		protected String doInBackground(String... params) {
			try {
				String response = RequestClass
						.getInstance()
						.readPay1B2CRequest(
								CouponCodeActivity.this,
								Constants.B2C_URL
										+ "refillwallet/?mobile_number=&amount=&vcode="
										+ URLEncoder.encode(params[0], "utf-8"));

				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onCancelled() {
			// TODO Auto-generated method stub
			super.onCancelled();
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			try {

				if (!result.startsWith("Error")) {
					// result = result.replaceAll("\\\\", "");
					JSONObject jsonObject = new JSONObject(result);

					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {
						JSONObject description = jsonObject
								.getJSONObject("description");

						/*
						 * Constants.showDialog(
						 * OrderSummaryMobileActivity.this, TAG,
						 * "Recharge request sent successfully.\nYour transaction ID is"
						 * + description .getString("transaction_id"),
						 * Constants.DIALOG_CLOSE_ORDER);
						 */

						Utility.setBalance(CouponCodeActivity.this,
								Constants.SHAREDPREFERENCE_BALANCE,
								description.getString("closing_balance"));
						Intent intent = new Intent(CouponCodeActivity.this,
								SuccessOrFailureActivity.class);

						ArrayList<WeakHashMap<String, String>> data = new ArrayList<WeakHashMap<String, String>>();
						JSONArray jsonArray = jsonObject.getJSONArray("deal");
						for (int i = 0; i < jsonArray.length(); i++) {
							WeakHashMap<String, String> map = new WeakHashMap<String, String>();
							map.put("DEAL_ID", jsonArray.getJSONObject(i)
									.getString("id"));
							map.put("DEAL_NAME", jsonArray.getJSONObject(i)
									.getString("dealname"));
							map.put("OFFER_PRICE", jsonArray.getJSONObject(i)
									.getString("offer_price"));
							map.put("CATAGORY", jsonArray.getJSONObject(i)
									.getString("category"));
							map.put("CATAGORY_ID", jsonArray.getJSONObject(i)
									.getString("category_id"));
							map.put("LATITUDE", jsonArray.getJSONObject(i)
									.getString("latitude"));
							map.put("LONGITUDE", jsonArray.getJSONObject(i)
									.getString("longitude"));
							map.put("DISTANCE", jsonArray.getJSONObject(i)
									.getString("distance"));
							map.put("ACTUAL_PRICE", jsonArray.getJSONObject(i)
									.getString("actual_price"));
							map.put("DISCOUNT", jsonArray.getJSONObject(i)
									.getString("discount"));
							map.put("TOTAL_STOCK", jsonArray.getJSONObject(i)
									.getString("total_stock"));
							map.put("URL", jsonArray.getJSONObject(i)
									.getString("img_url"));
							map.put("STOCK_SOLD", jsonArray.getJSONObject(i)
									.getString("stock_sold"));

							data.add(map);
						}
						intent.putExtra(Constants.SUCCESS_OR_FAILURE, true);
						intent.putExtra("DATA", data);
						intent.putExtra(Constants.MOBILE_NUMBER,
								"Your wallet refilled successfully.");
						intent.putExtra(Constants.RECHARGE_AMOUNT,
								description.getString("closing_balance") + "");

						intent.putExtra(Constants.RECHARGE_FOR,
								Constants.WALLET_PAYMENT);
						startActivity(intent);
						finish();
					} else {

						Constants.showOneButtonDialog(CouponCodeActivity.this,
								TAG, jsonObject.getString("description"),
								Constants.DIALOG_CLOSE);

					}
				} else {
					// Constants.showOneButtonDialog(CouponCodeActivity.this,
					// TAG,
					// Constants.ERROR_INTERNET, Constants.DIALOG_CLOSE);
					Intent intent = new Intent(CouponCodeActivity.this,
							ConnectivityErrorActivity.class);
					intent.putExtra(Constants.RECHARGE_FOR,
							Constants.WALLET_PAYMENT);
					startActivity(intent);

				}
			} catch (JSONException e) {

			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(CouponCodeActivity.this);
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(false);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							WalletTopupViaCouponTask.this.cancel(true);
							dialog.cancel();
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			WalletTopupViaCouponTask.this.cancel(true);
			dialog.cancel();
		}

	}
}
