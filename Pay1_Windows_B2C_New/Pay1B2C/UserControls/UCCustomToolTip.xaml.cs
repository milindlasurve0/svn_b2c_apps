﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace Pay1B2C.UserControls
    {
    public partial class UCCustomToolTip: UserControl
        {
        private string _description;

        public string Description
            {
            get { return _description; }
            set { _description = value; }
            }

        public UCCustomToolTip()
            {
            InitializeComponent();
            Loaded += UCCustomToolTip_Loaded;
            }

        void UCCustomToolTip_Loaded(object sender, RoutedEventArgs e)
            {

            Lbltext.Text = Description;
            }
        public void FillDescription()
            {
            Lbltext.Text = Description;

            }
/*
        private void imgmarker_Tap(object sender, System.Windows.Input.GestureEventArgs e)
            {

            if(borderPanel.Visibility == Visibility.Collapsed)
                {
                borderPanel.Visibility = Visibility.Visible;
                //borderPanel.Opacity = 1;
                //imgborder.Opacity = 1;
                }
            else
                {
                borderPanel.Visibility = Visibility.Collapsed;
                //borderPanel.Opacity = 0;
                //imgborder.Opacity = 0;
                }
            }
        */

        }
    }
