package com.pay1;

import java.net.URLEncoder;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.pay1.constants.Constants;
import com.pay1.customviews.GifMovieView;
import com.pay1.requesthandler.RequestClass;
import com.pay1.utilities.ArrayHelper;
import com.pay1.utilities.Utility;

public class SplashScreenActivity extends Activity {

	private static final String SCREEN_LABEL = "Splash Screen";
	private EasyTracker easyTracker = null;
	// private final String TAG = "Splash";
	// Splash screen timer
	private static int SPLASH_TIME_OUT = 3000;
	// private String version, manufacturer;
	private GifMovieView imageView_Icon;
	private String uuid;
	private String code = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// if (Build.VERSION.SDK_INT < 16) {
		// this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		// this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
		// WindowManager.LayoutParams.FLAG_FULLSCREEN);
		// }
		setContentView(R.layout.splashscreen_activity);
		easyTracker = EasyTracker.getInstance(SplashScreenActivity.this);
		easyTracker.set(Fields.SCREEN_NAME, SCREEN_LABEL);

		imageView_Icon = (GifMovieView) findViewById(R.id.imageView_Icon);
		imageView_Icon.setMovieResource(R.drawable.loader);
		// Animation animation = AnimationUtils.loadAnimation(
		// SplashScreenActivity.this, R.anim.rotate_down);
		// imageView_Icon.setAnimation(animation);
		try {
			TelephonyManager tManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
			uuid = tManager.getDeviceId();
			if (uuid == null) {
				uuid = Secure.getString(
						SplashScreenActivity.this.getContentResolver(),
						Secure.ANDROID_ID);
			}
			if (Utility.getUUID(SplashScreenActivity.this,
					Constants.SHAREDPREFERENCE_UUID) == null)
				Utility.setUUID(SplashScreenActivity.this,
						Constants.SHAREDPREFERENCE_UUID, uuid);
		} catch (Exception exception) {
		}

		// try {
		// LocationResult locationResult = new LocationResult() {
		// @Override
		// public void gotLocation(Location location) {
		// // Got the location!
		// try {
		//
		// Utility.setCurrentLatitude(SplashScreenActivity.this,
		// Constants.SHAREDPREFERENCE_CURRENT_LATITUDE,
		// String.valueOf(location.getLatitude()));
		// Utility.setCurrentLongitude(SplashScreenActivity.this,
		// Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE,
		// String.valueOf(location.getLongitude()));
		//
		// } catch (Exception exception) {
		// }
		// }
		// };
		// MyLocation myLocation = new MyLocation();
		// myLocation.getLocation(this, locationResult);
		// } catch (Exception exception) {
		// }

		if (Utility.getLoginFlag(SplashScreenActivity.this,
				Constants.SHAREDPREFERENCE_IS_LOGIN)) {

			// version = android.os.Build.VERSION.RELEASE;
			// manufacturer = android.os.Build.MANUFACTURER;

			new AutoLoginTask().execute(Utility.getMobileNumber(
					SplashScreenActivity.this,
					Constants.SHAREDPREFERENCE_MOBILE), Utility.getPin(
					SplashScreenActivity.this, Constants.SHAREDPREFERENCE_PIN),
					Utility.getGCMID(SplashScreenActivity.this,
							Constants.SHAREDPREFERENCE_GCMID), Utility.getUUID(
							SplashScreenActivity.this,
							Constants.SHAREDPREFERENCE_UUID),
					Utility.getCurrentLatitude(SplashScreenActivity.this,
							Constants.SHAREDPREFERENCE_CURRENT_LATITUDE),
					Utility.getCurrentLongitude(SplashScreenActivity.this,
							Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE));

		} else {
			new Handler().postDelayed(new Runnable() {

				/*
				 * Showing splash screen with a timer. This will be useful when
				 * you want to show case your app logo / company
				 */

				@Override
				public void run() {
					// This method will be executed once the timer is over
					// Start your app main activity
					// if (Utility.isFirstTime(SplashScreenActivity.this)) {
					// Intent i = new Intent(SplashScreenActivity.this,
					// TourActivity.class);
					//
					// startActivity(i);
					// } else {
					Intent i = new Intent(SplashScreenActivity.this,
							RechargeMobileActivity.class);
					i.putExtra(Constants.RECHARGE_FOR,
							Constants.RECHARGE_MOBILE);
					startActivity(i);
					// }
					// close this activity
					finish();
				}
			}, SPLASH_TIME_OUT);
		}

	}

	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView().build());
		// EasyTracker.getInstance(getApplicationContext()); //
		// this).activityStart(this);
		easyTracker.send(MapBuilder.createAppView().build());
	}

	@Override
	public void onStop() {
		super.onStop();
		// EasyTracker.getInstance(getApplicationContext()); //
		// this).activityStop(this);
	}

	public class AutoLoginTask extends AsyncTask<String, String, String> {
		// private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			try {
				try {
					long LastUpdateTime = Utility.getLongSharedPreferences(
							SplashScreenActivity.this,
							Constants.CONTACT_UPDATE_TIME);

					int days = (int) ((System.currentTimeMillis() - LastUpdateTime) / (1000 * 60 * 60 * 24));

					if (days >= 2) {
						ArrayHelper arrayHelper = new ArrayHelper(
								SplashScreenActivity.this);
						arrayHelper.clearArray(Constants.CONATACT_JSON);
						arrayHelper
								.saveArray(
										Constants.CONATACT_JSON,
										Constants
												.getAllContacts(SplashScreenActivity.this));
					}
				} catch (Exception e) {
				}

				String response = RequestClass.getInstance()
						.readPay1B2CRequest(
								SplashScreenActivity.this,
								Constants.B2C_URL + "signin/?username="
										+ URLEncoder.encode(params[0], "utf-8")
										+ "&password="
										+ URLEncoder.encode(params[1], "utf-8")
										+ "&gcm_reg_id="
										+ URLEncoder.encode(params[2], "utf-8")
										+ "&uuid="
										+ URLEncoder.encode(params[3], "utf-8")
										+ "&latitude=" + params[4]
										+ "&longitude=" + params[5]);
				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			// if (this.dialog.isShowing()) {
			// this.dialog.dismiss();
			// }
			try {

				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONObject jsonObject = new JSONObject(replaced);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {
						Utility.setLoginFlag(SplashScreenActivity.this,
								Constants.SHAREDPREFERENCE_IS_LOGIN, true);
						JSONObject jsonObject2 = jsonObject
								.getJSONObject("description");

						Utility.setUserName(SplashScreenActivity.this,
								Constants.SHAREDPREFERENCE_NAME, jsonObject2
										.getString("name").trim());
						Utility.setMobileNumber(SplashScreenActivity.this,
								Constants.SHAREDPREFERENCE_MOBILE, jsonObject2
										.getString("mobile").trim());
						Utility.setEmail(SplashScreenActivity.this,
								Constants.SHAREDPREFERENCE_EMAIL, jsonObject2
										.getString("email").trim());
						Utility.setGender(SplashScreenActivity.this,
								Constants.SHAREDPREFERENCE_GENDER, jsonObject2
										.getString("gender").trim());
						Utility.setUserID(SplashScreenActivity.this,
								Constants.SHAREDPREFERENCE_USER_ID, jsonObject2
										.getString("user_id").trim());
						Utility.setBalance(SplashScreenActivity.this,
								Constants.SHAREDPREFERENCE_BALANCE, jsonObject2
										.getString("wallet_balance").trim());
						Utility.setPin(SplashScreenActivity.this,
								Constants.SHAREDPREFERENCE_PIN, Utility.getPin(
										SplashScreenActivity.this,
										Constants.SHAREDPREFERENCE_PIN));
						Utility.setDOB(SplashScreenActivity.this,
								Constants.SHAREDPREFERENCE_DOB, jsonObject2
										.getString("date_of_birth").trim());
						Utility.setLatitude(SplashScreenActivity.this,
								Constants.SHAREDPREFERENCE_LATITUDE,
								jsonObject2.getString("latitude").trim());
						Utility.setLongitude(SplashScreenActivity.this,
								Constants.SHAREDPREFERENCE_LONGITUDE,
								jsonObject2.getString("longitude").trim());
						Utility.setSupportNumber(SplashScreenActivity.this,
								Constants.SHAREDPREFERENCE_SUPPORT_NUMBER,
								jsonObject2.getString("support_number").trim());

						Utility.setSharedPreference(
								SplashScreenActivity.this,
								Constants.SHAREDPREFERENCE_SERVICE_CHARGE_PERCENT,
								jsonObject2.getString("service_charge_percent")
										.trim());

						Utility.setSharedPreference(SplashScreenActivity.this,
								Constants.SHAREDPREFERENCE_SERVICE_TAX_PERCENT,
								jsonObject2.getString("service_tax_percent")
										.trim());

					} else {
						code = jsonObject.getString("errCode");
					}

				} else {
					// Constants.showDialog(SplashScreenActivity.this, TAG,
					// Constants.ERROR_INTERNET, Constants.DIALOG_CLOSE);
				}
			} catch (JSONException e) {
				// e.printStackTrace();
				// Constants.showDialog(SplashScreenActivity.this, TAG,
				// Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
				// Constants.DIALOG_CLOSE);
			} catch (Exception e) {
				// TODO: handle exception
				// Constants.showDialog(SplashScreenActivity.this, TAG,
				// Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
				// Constants.DIALOG_CLOSE);
			}

			// if (Utility.isFirstTime(SplashScreenActivity.this)) {
			// Intent i = new Intent(SplashScreenActivity.this,
			// TourActivity.class);
			//
			// startActivity(i);
			// } else {

			// }

			if (code.equalsIgnoreCase("205")) {
				Utility.setLoginFlag(SplashScreenActivity.this,
						Constants.SHAREDPREFERENCE_IS_LOGIN, false);
				startActivity(new Intent(SplashScreenActivity.this,
						LoginActivity.class));
				finish();
			} else {
				Intent i = new Intent(SplashScreenActivity.this,
						RechargeMobileActivity.class);
				i.putExtra(Constants.RECHARGE_FOR, Constants.RECHARGE_MOBILE);
				startActivity(i);
				finish();
			}
		}

		@Override
		protected void onPreExecute() {
			// dialog = new MyProgressDialog(SplashScreenActivity.this);
			// // this.dialog.setMessage("Please wait.....");
			// this.dialog.setCancelable(true);
			// this.dialog
			// .setOnCancelListener(new DialogInterface.OnCancelListener() {
			//
			// @Override
			// public void onCancel(DialogInterface dialog) {
			// // TODO Auto-generated method stub
			// AutoLoginTask.this.cancel(true);
			// }
			// });
			// this.dialog.show();
			super.onPreExecute();
		}

	}

}
