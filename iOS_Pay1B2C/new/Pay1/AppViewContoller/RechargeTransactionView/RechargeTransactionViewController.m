//
//  RechargeTransactionViewController.m
//  Pay1
//
//  Created by Annapurna on 10/07/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "RechargeTransactionViewController.h"
#import "RechargeResponse.h"
//#import "TransactionCancelView.h"
#import "TransactionCancelAlertView.h"
@interface RechargeTransactionViewController ()

@end

@implementation RechargeTransactionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    isMoveBack = FALSE;
    
   // coverView = [self onAddDimViewOnSuperView:self.view];
   // [coverView addSubview:[self addTransactionMessageView:FALSE]];
    
    
    urlArray = [[NSMutableArray alloc]init];
    
    NSLog(@"#check :%@ %@ %@ %@ %@",_operatorName,_amountToRecharge,_numberToRecharge,_typeStr,_postPaidLabelText);
    
    [self showHUDOnView];
    [self hideNavBackButton];
    [self setNaviationButtonWithText:@"Card/Netbanking" selector:@selector(backClicked:) isLeft:TRUE isReduceSize:TRUE isMyFont:18.0];
    
    [self loadRequestFromString:_objRechargeResponse.formContent];
}

- (void)loadRequestFromString:(NSString*)urlString
{
  //  NSLog(@"#URLString :%@",urlString);
    
    [self findAllURLFromHTML:urlString];
    
    [theWebView loadHTMLString:urlString baseURL:nil];
}

-(void)findAllURLFromHTML:(NSString *)htmlStr{
    NSError *error1 = nil;
    NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink error:&error1];
    
    [detector enumerateMatchesInString:htmlStr options:0 range:NSMakeRange(0, htmlStr.length) usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop)
    {
    if (result.resultType == NSTextCheckingTypeLink)
    {
    NSString * newStr = [[result.URL absoluteString] substringToIndex:[result.URL absoluteString].length-1];
 //   NSLog(@"#newStr %@",newStr);
    NSString *str = [NSString stringWithFormat:@"%@",newStr];
       
    NSLog(@">>>>>>>%@",str);
    [urlArray addObject:str];
    }
    }];
    
  //  NSLog(@"url array count :%lu",(unsigned long)urlArray.count);
    
    
    http://b2c.pay1.in/

    if([urlArray containsObject:@"https://b2c.pay1.in/index.php/api_new/action/api/true/actiontype/payu_update/page/success"]){
    NSUInteger fooIndex = [urlArray indexOfObject: @"https://b2c.pay1.in/index.php/api_new/action/api/true/actiontype/payu_update/page/success"];
    successString = [urlArray objectAtIndex:fooIndex];
    }
    
    if([urlArray containsObject:@"https://b2c.pay1.in/index.php/api_new/action/api/true/actiontype/payu_update/page/failure"]){
    NSUInteger fooIndex = [urlArray indexOfObject: @"https://b2c.pay1.in/index.php/api_new/action/api/true/actiontype/payu_update/page/failure"];
    failureString = [urlArray objectAtIndex:fooIndex];
    }
    
    if([urlArray containsObject:@"https://b2c.pay1.in/index.php/api_new/action/api/true/actiontype/payu_update/page/cancel"]){
    NSUInteger fooIndex = [urlArray indexOfObject: @"https://b2c.pay1.in/index.php/api_new/action/api/true/actiontype/payu_update/page/cancel"];
    cancelString = [urlArray objectAtIndex:fooIndex];
    }
    
    if([urlArray containsObject:@"https://b2c.pay1.in/index.php/api_new/action/api/true/actiontype/payu_update/page/timeout"]){
    NSUInteger fooIndex = [urlArray indexOfObject: @"https://b2c.pay1.in/index.php/api_new/action/api/true/actiontype/payu_update/page/timeout"];
    timeOutString = [urlArray objectAtIndex:fooIndex];
    }
    
   /* if([urlArray containsObject:@"http://cdev.pay1.in/index.php/api_new/action/api/true/actiontype/payu_update/page/success"]){
        NSUInteger fooIndex = [urlArray indexOfObject: @"http://cdev.pay1.in/index.php/api_new/action/api/true/actiontype/payu_update/page/success"];
        successString = [urlArray objectAtIndex:fooIndex];
    }
    
    if([urlArray containsObject:@"http://cdev.pay1.in/index.php/api_new/action/api/true/actiontype/payu_update/page/failure"]){
        NSUInteger fooIndex = [urlArray indexOfObject: @"http://cdev.pay1.in/index.php/api_new/action/api/true/actiontype/payu_update/page/failure"];
        failureString = [urlArray objectAtIndex:fooIndex];
    }
    
    if([urlArray containsObject:@"http://cdev.pay1.in/index.php/api_new/action/api/true/actiontype/payu_update/page/cancel"]){
        NSUInteger fooIndex = [urlArray indexOfObject: @"http://cdev.pay1.in/index.php/api_new/action/api/true/actiontype/payu_update/page/cancel"];
        cancelString = [urlArray objectAtIndex:fooIndex];
    }
    
    if([urlArray containsObject:@"http://cdev.pay1.in/index.php/api_new/action/api/true/actiontype/payu_update/page/timeout"]){
        NSUInteger fooIndex = [urlArray indexOfObject: @"http://cdev.pay1.in/index.php/api_new/action/api/true/actiontype/payu_update/page/timeout"];
        timeOutString = [urlArray objectAtIndex:fooIndex];
    }*/
    
   
    
    NSLog(@"#URL Strings :%@ %@ %@ %@",successString,failureString,cancelString,timeOutString);

}

#pragma mark - WebView Delegate


- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSLog(@"#WebView URL :%@",request.URL.absoluteString);
    
    if([request.URL.absoluteString isEqualToString:failureString]){
    if(_isComingFrom == FALSE && _isComingFromGift == FALSE){
    [Constant saveQuickPayRecord:TRUE];
    }
    [self showAlertView:@"Transaction Failed" isShow:FALSE];
    }
    
    if([request.URL.absoluteString isEqualToString:successString]){
    isMoveBack = TRUE;

    if(_isComingFrom == FALSE && _isComingFromGift == FALSE){
    [Constant saveQuickPayRecord:TRUE];
    [self showUIForSuccessRecharge];
    }
        
        
    else if(_isComingFrom == TRUE && _isComingFromGift == FALSE){
  //  int netWalletBalance = [[Session shared].objSignInResponse.userWalletBalance intValue] + [_amountToRecharge intValue];
        int netWalletBalance = [[Constant getUserWalletBalance] intValue] + [_amountToRecharge intValue];

   // [Session shared].objSignInResponse.userWalletBalance = [NSString stringWithFormat:@"%d",netWalletBalance];
        
        [Constant saveUserWalletBalance:[NSString stringWithFormat:@"%d",netWalletBalance]];

    coverView = [self onAddDimViewOnSuperView:self.view];
    [coverView addSubview:[self addTransactionMessageView:TRUE]];
    }
        
        
    else if(_isComingFromGift == TRUE && _isComingFrom == FALSE){
    [self showAlertForGiftView:@"CONGRATULATIONS" alertMessage:@"This is yours now. Visit store and get it." isShow:TRUE];
    }
        
    }
    
    if([request.URL.absoluteString isEqualToString:cancelString]){
    if(_isComingFromGift == TRUE && _isComingFrom == FALSE)
    [self showAlertForGiftView:@"GIFT" alertMessage:@"Transaction Canceled. Please try again" isShow:TRUE];
    else
    [self showTransactionCancelView];
    }
    
    if([request.URL.absoluteString isEqualToString:timeOutString]){
    if(_isComingFrom == FALSE && _isComingFromGift == FALSE){
    [Constant saveQuickPayRecord:TRUE];
    }
    if(_isComingFromGift == TRUE && _isComingFrom == FALSE)
    [self showAlertForGiftView:@"GIFT" alertMessage:@"Transaction Timeout" isShow:TRUE];
        
    else
    [self showAlertView:@"Transaction Timeout" isShow:FALSE];
    }
    
    
    if ( navigationType == UIWebViewNavigationTypeLinkClicked ) {
        
    NSLog(@"#TheCancelURL is :%@",request.URL.absoluteString);
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:request.URL];
    [theWebView loadRequest:urlRequest];
    return NO;
    }
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    // [self updateButtons];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSLog(@"#FinishedLoadedHTML");
    [self hideHUDOnView];
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    NSLog(@"#FailedLoadedHTML");
    [self hideHUDOnView];

   // [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:[self.navigationController.viewControllers count]-2] animated:1 ] ;

}

-(void)showAlertView:(NSString *)alertMessage isShow:(BOOL)isShow{
    isMoveBack = isShow;

    UIAlertView *alert;
    if(isShow == FALSE){
    alert = [[UIAlertView alloc]initWithTitle:@"PAY1" message:alertMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    }
    else{
    alert = [[UIAlertView alloc]initWithTitle:@"PAY1" message:alertMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"CANCEL", nil];
    }
    
    [alert show];
    
}

-(void)showAlertForGiftView:(NSString *)alertTitle alertMessage:(NSString *)alertMessage isShow:(BOOL)isShow{
    isMoveBack = isShow;
    
    UIAlertView *alert;
    
    alert = [[UIAlertView alloc]initWithTitle:alertTitle message:alertMessage delegate:self cancelButtonTitle:@"DONE" otherButtonTitles:nil, nil];
    
    
    [alert show];
    
}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex == 0){
    
    if(isMoveBack == TRUE)
    [self.navigationController popViewControllerAnimated:YES];
    }
    
}

-(void)showUIForSuccessRecharge{
    
    coverView = [self onAddDimViewOnSuperView:self.view];
    
    TransactionCancelAlertView * transAlertView;
    
    if([UIScreen mainScreen].bounds.size.height == 480){
    transAlertView  = [[TransactionCancelAlertView alloc]initWithFrame:CGRectMake(10, self.view.frame.origin.y + 50, 270 , 410)];
        
    }
    if([UIScreen mainScreen].bounds.size.height == 568){
    transAlertView  = [[TransactionCancelAlertView alloc]initWithFrame:CGRectMake(10, self.view.frame.origin.y + 70, 270 , 410)];
        
    }
    
    
    else{
    transAlertView  = [[TransactionCancelAlertView alloc]initWithFrame:CGRectMake(20, self.view.frame.origin.y + 120, self.view.frame.size.width - 20 , 410)];
        
    }
    
    [coverView addSubview:transAlertView];
  //  int netGiftPoint = [[Session shared].objSignInResponse.userLoyaltyPoints intValue] + [_amountToRecharge intValue];

    
    int netGiftPoint = [[Constant getUserCoinBalance] intValue] + [_amountToRecharge intValue];
    
    [Constant saveUserCoinBalance: [NSString stringWithFormat:@"%d",netGiftPoint]];
    
    //[Session shared].objSignInResponse.userLoyaltyPoints =   [NSString stringWithFormat:@"%d",netGiftPoint];



    [transAlertView PutDataOnCancelAlert: [self giveLogoImageAccordingToOperator:_operatorName] phoneNumber:_numberToRecharge amount:_amountToRecharge type:[_typeStr uppercaseString]];
    
    
    [transAlertView setOnAwesomeClick:^{
    [self.navigationController popViewControllerAnimated:YES];
        
    }];
}


-(void)showTransactionCancelView{
    if(_isComingFrom == FALSE && _isComingFromGift == FALSE){
    [Constant saveQuickPayRecord:TRUE];
    }
    
    
    coverView = [self onAddDimViewOnSuperView:self.view];
    [coverView addSubview:[self addTransactionMessageView:FALSE]];
}



-(UIView *)addTransactionMessageView:(BOOL)isShow{
    
    
   UIView * cancelView = [[UIView alloc]initWithFrame:CGRectMake(20, 64+50, [UIScreen mainScreen].bounds.size.width - 40, 350)];
    
    cancelView.backgroundColor = [UIColor whiteColor];
    
    UILabel *statusLabel ;
    UILabel *txtLabel;

    
    if(!isShow){
    statusLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 10, 100, 21)];
    statusLabel.text = @"Failure";
    txtLabel =  [[UILabel alloc]initWithFrame:CGRectMake(50, 64, 206, 21)];
    txtLabel.text = @"Transaction cancelled";
    }
    
    else{
    statusLabel = [[UILabel alloc]initWithFrame:CGRectMake(cancelView.frame.origin.x + 70, 10, 100, 21)];
    statusLabel.text = @"SUCCESS";
    statusLabel.textAlignment = NSTextAlignmentCenter;
    txtLabel.textAlignment = NSTextAlignmentCenter;
    txtLabel =  [[UILabel alloc]initWithFrame:CGRectMake(80, 64, 206, 21)];
   
    txtLabel.text = _typeStr;
    }
    statusLabel.font = [UIFont fontWithName:@"Open Sans" size:18];
    [cancelView addSubview:statusLabel];
    
    
    [self setTextOnLabel:txtLabel fontSize:18.0];

    [cancelView addSubview:txtLabel];
    
    
    UIView *detailInfoView = [[UIView alloc]initWithFrame:CGRectMake(50, txtLabel.frame.origin.y + txtLabel.frame.size.height + 30, 190, 60)];
    UIImageView *logoImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 60, 60)];
    logoImageView.layer.borderColor = [UIColor grayColor].CGColor;
    logoImageView.layer.borderWidth = 1;
    
    [self setRoundedCornerForImageView:logoImageView];
    
    
    UIImageView *operatotrLogoImageView = [[UIImageView alloc]initWithFrame:CGRectMake(15, 15, 30, 30)];
    if(!_isComingFrom && !_isComingFromGift)
    operatotrLogoImageView.image = [UIImage imageNamed:[self giveLogoImageAccordingToOperator:_operatorName]];
    else if(_isComingFromGift == FALSE && _isComingFrom == TRUE){
    logoImageView.image = [UIImage imageNamed:@"walletFill.png"];
    }
    
    [logoImageView addSubview:operatotrLogoImageView];
    
    
    
    
    
    detailInfoView.layer.borderColor = [UIColor grayColor].CGColor;
    detailInfoView.layer.borderWidth = 1;
    
    [detailInfoView addSubview:logoImageView];
    
    UIView *horiSepView = [[UIView alloc]initWithFrame:CGRectMake(logoImageView.frame.origin.x + logoImageView.frame.size.width, logoImageView.frame.origin.y + logoImageView.frame.size.height/2, 130 , 1)];
    horiSepView.backgroundColor = [UIColor lightGrayColor];
    [detailInfoView addSubview:horiSepView];
    
    UILabel *numLabel = [[UILabel alloc]initWithFrame:CGRectMake(logoImageView.frame.origin.x + logoImageView.frame.size.width + 10, 5, 100, 21)];
    numLabel.text = _numberToRecharge;
    [self setTextOnLabel:numLabel fontSize:17.0];
    numLabel.textAlignment = NSTextAlignmentCenter;


    [detailInfoView addSubview:numLabel];
    
  
    
    UILabel *amountLabel = [[UILabel alloc]initWithFrame:CGRectMake(logoImageView.frame.origin.x + logoImageView.frame.size.width + 10, horiSepView.frame.origin.y + horiSepView.frame.size.height + 5, 100, 21)];
    amountLabel.text = [NSString stringWithFormat:@"Rs. %@",_amountToRecharge];
    [self setTextOnLabel:amountLabel fontSize:17.0];
    amountLabel.textAlignment = NSTextAlignmentCenter;

    [detailInfoView addSubview:amountLabel];
    
    
    [cancelView addSubview:detailInfoView];
    
    if([_typeStr isEqualToString:@"Postpaid Recharge"]){
    UILabel *postpaidInfoLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, detailInfoView.frame.origin.y + detailInfoView.frame.size.height + 5, cancelView.frame.size.width - 20, 21)];
        postpaidInfoLabel.text = _postPaidLabelText;

    [self setTextOnLabel:postpaidInfoLabel fontSize:12.0];
    [cancelView addSubview:postpaidInfoLabel];
    }
    
    
    UIView *missedInfoView = [[UIView alloc]initWithFrame:CGRectMake(20, detailInfoView.frame.origin.y + detailInfoView.frame.size.height + 38, 190, 60)];
    UIImageView *phoneImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 10, 30, 30)];
    logoImageView.layer.borderColor = [UIColor grayColor].CGColor;
    logoImageView.layer.borderWidth = 1;
    phoneImageView.image = [UIImage imageNamed:@"telephone_mark.png"];
    
    
    UILabel *infoLabel = [[UILabel alloc]initWithFrame:CGRectMake(phoneImageView.frame.origin.x + phoneImageView.frame.size.width + 10, 5,(missedInfoView.frame.origin.x+ missedInfoView.frame.size.width - 30), 80)];
    infoLabel.text = @"Give us a missed call to raise the compliant of this transaction on 02267242266";
    infoLabel.font = [UIFont fontWithName:@"Open Sans" size:12];
    infoLabel.lineBreakMode = NSLineBreakByWordWrapping;
    infoLabel.numberOfLines = 0;
    [infoLabel sizeToFit];
    //[detailInfoView addSubview:infoLabel];
    

    [missedInfoView addSubview:phoneImageView];
    [missedInfoView addSubview:infoLabel];
    
    [cancelView addSubview:missedInfoView];
    
    
    UIView *horizontalSepView;
    
   
    horizontalSepView = [[UIView alloc]initWithFrame:CGRectMake(20, missedInfoView.frame.origin.y + missedInfoView.frame.size.height + 20, cancelView.frame.size.width - 40, 1)];
    
    
    horizontalSepView.backgroundColor = [UIColor lightGrayColor];
    [cancelView addSubview:horizontalSepView];
    
    UIButton *tokButton = [UIButton buttonWithType:UIButtonTypeCustom];
    tokButton.frame = CGRectMake(0, horizontalSepView.frame.origin.y + horizontalSepView.frame.size.height +1 , cancelView.frame.size.width, cancelView.frame.size.height - horizontalSepView.frame.origin.y + horizontalSepView.frame.size.height +1);
    if(!isShow)
    [tokButton setTitle:@"OK" forState:UIControlStateNormal];
    else
    [tokButton setTitle:@"DONE" forState:UIControlStateNormal];
  
    [tokButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [tokButton addTarget:self action:@selector(okCLicked:) forControlEvents:UIControlEventTouchUpInside];
    [cancelView addSubview:tokButton];
    
    
    
    return cancelView;
}

-(void)okCLicked:(id)sender{
    NSLog(@"#OK Clicked");
    
    [coverView removeFromSuperview];
    
  //  AppDelegate *appDelegate =  (AppDelegate*)[[UIApplication sharedApplication] delegate];
    //[appDelegate.tabBarController setSelectedIndex:0];
    
    
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(void)backClicked:(id)sender{
    [self showAlertView:@"Are you sure you want to cancel this transaction?" isShow:TRUE];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
