package com.pay1.adapterhandler;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.WeakHashMap;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pay1.R;
import com.pay1.WalletHistoryActivity;

public class WalletHistoryAdapter extends BaseAdapter {

	private Context context;
	private ArrayList<WeakHashMap<String, String>> data;
	private Typeface Reguler, Narrow, Narrow_thin;

	public WalletHistoryAdapter(Context context,
			ArrayList<WeakHashMap<String, String>> data) {
		this.context = context;
		this.data = data;

		Reguler = Typeface.createFromAsset(context.getAssets(),
				"EncodeSansNormal-400-Regular.ttf");

		Narrow = Typeface.createFromAsset(context.getAssets(),
				"EncodeSansNarrow-500-Medium.ttf");

		Narrow_thin = Typeface.createFromAsset(context.getAssets(),
				"EncodeSansNarrow-100-Thin.ttf");
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder viewHolder;
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.wallet_history_adapter,
					null);
			viewHolder = new ViewHolder();
			viewHolder.textView_Name = (TextView) convertView
					.findViewById(R.id.textView_Name);
			viewHolder.textView_Name.setTypeface(Reguler);
			viewHolder.textView_Operator = (TextView) convertView
					.findViewById(R.id.textView_Operator);
			viewHolder.textView_Operator.setTypeface(Narrow);
			viewHolder.textView_Amount = (TextView) convertView
					.findViewById(R.id.textView_Amount);
			viewHolder.textView_Amount.setTypeface(Reguler);
			viewHolder.textView_Date = (TextView) convertView
					.findViewById(R.id.textView_Date);
			viewHolder.textView_Date.setTypeface(Narrow);
			viewHolder.textView_ClosingBalance = (TextView) convertView
					.findViewById(R.id.textView_ClosingBalance);
			viewHolder.textView_ClosingBalance.setTypeface(Narrow);
			viewHolder.relativeLayout_Image = (RelativeLayout) convertView
					.findViewById(R.id.relativeLayout_Image);
			viewHolder.imageView_Image = (ImageView) convertView
					.findViewById(R.id.imageView_Image);
			convertView.setTag(viewHolder);

		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		WeakHashMap<String, String> map = new WeakHashMap<String, String>();
		map = data.get(position);

		DecimalFormat df = new DecimalFormat("00.00");
		double amt = 0;
		try {
			amt = Double.parseDouble(map.get(WalletHistoryActivity.AMOUNT)
					.toString().trim());
		} catch (Exception e) {
			amt = 0;
		}
		viewHolder.textView_Amount.setText("Rs. " + df.format(amt));

		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"dd-MMM-yyyy,hh:mm aa");
		try {
			String str = map.get(WalletHistoryActivity.TIME).toString().trim()
					.replaceAll("-", "/");
			Date dt = new Date(str);
			viewHolder.textView_Date.setText(dateFormat.format(dt));
		} catch (Exception e) {

		}

		double bal = 0;
		try {
			bal = Double.parseDouble(map.get(WalletHistoryActivity.BALANCE)
					.toString().trim());
		} catch (Exception e) {
			bal = 0;
		}

		double type = 0;
		try {
			type = Double.parseDouble(map.get(WalletHistoryActivity.TRANS_TYPE)
					.toString().trim());
		} catch (Exception e) {
			type = 0;
		}

		viewHolder.textView_ClosingBalance.setText("Closing balance: Rs. "
				+ df.format(bal));

		if (type == 0) {
			viewHolder.textView_ClosingBalance.setTextColor(context
					.getResources().getColor(R.color.Red));
		} else if (type == 1) {
			viewHolder.textView_ClosingBalance.setTextColor(context
					.getResources().getColor(R.color.Green));
		}
		int service_id = 1;
		try {
			service_id = Integer.parseInt(map
					.get(WalletHistoryActivity.SERVICE_ID));
		} catch (Exception e) {
			service_id = 1;
		}
		// Bitmap bitmap=Constants.getBitmapById(statusId,context);
		switch (service_id) {
		case 0:
			viewHolder.relativeLayout_Image.setBackgroundColor(context
					.getResources().getColor(R.color.app_blue_color));
			viewHolder.imageView_Image.setImageResource(R.drawable.wal_balance);
			viewHolder.textView_Name.setText(map
					.get(WalletHistoryActivity.NUMBER).toString().trim());
			viewHolder.textView_Operator.setText(map
					.get(WalletHistoryActivity.OPERATOR).toString().trim());
			viewHolder.textView_Operator.setVisibility(View.VISIBLE);
			break;
		case 1:
			viewHolder.relativeLayout_Image.setBackgroundColor(context
					.getResources().getColor(R.color.app_green_color));
			viewHolder.imageView_Image.setImageResource(R.drawable.wal_mobile);
			viewHolder.textView_Name.setText(map
					.get(WalletHistoryActivity.NUMBER).toString().trim());
			viewHolder.textView_Operator.setText(map
					.get(WalletHistoryActivity.OPERATOR).toString().trim());
			viewHolder.textView_Operator.setVisibility(View.VISIBLE);
			break;
		case 2:
			viewHolder.relativeLayout_Image.setBackgroundColor(context
					.getResources().getColor(R.color.app_green_color));
			viewHolder.imageView_Image.setImageResource(R.drawable.wal_dth);
			viewHolder.textView_Name.setText(map
					.get(WalletHistoryActivity.NUMBER).toString().trim());
			viewHolder.textView_Operator.setText(map
					.get(WalletHistoryActivity.OPERATOR).toString().trim());
			viewHolder.textView_Operator.setVisibility(View.VISIBLE);
			break;
		case 4:
			viewHolder.relativeLayout_Image.setBackgroundColor(context
					.getResources().getColor(R.color.app_skyblue_color));
			viewHolder.imageView_Image
					.setImageResource(R.drawable.wal_postpaid);
			viewHolder.textView_Name.setText(map
					.get(WalletHistoryActivity.NUMBER).toString().trim());
			viewHolder.textView_Operator.setText(map
					.get(WalletHistoryActivity.OPERATOR).toString().trim());
			viewHolder.textView_Operator.setVisibility(View.VISIBLE);
			break;
		case 5:
			viewHolder.relativeLayout_Image.setBackgroundColor(context
					.getResources().getColor(R.color.app_green_color));
			viewHolder.imageView_Image
					.setImageResource(R.drawable.wal_datacard);
			viewHolder.textView_Name.setText(map
					.get(WalletHistoryActivity.NUMBER).toString().trim());
			viewHolder.textView_Operator.setText(map
					.get(WalletHistoryActivity.OPERATOR).toString().trim());
			viewHolder.textView_Operator.setVisibility(View.VISIBLE);
			break;
		case 98:
			viewHolder.relativeLayout_Image.setBackgroundColor(context
					.getResources().getColor(R.color.app_blue_color));
			viewHolder.imageView_Image.setImageResource(R.drawable.wal_balance);
			viewHolder.textView_Name.setText(map
					.get(WalletHistoryActivity.NUMBER).toString().trim());
			viewHolder.textView_Operator.setText("Wallet topup");
			viewHolder.textView_Operator.setVisibility(View.VISIBLE);
			break;
		case 99:
			viewHolder.relativeLayout_Image.setBackgroundColor(context
					.getResources().getColor(R.color.app_orange_color));
			viewHolder.imageView_Image.setImageResource(R.drawable.wal_deals);
			viewHolder.textView_Name.setText(map
					.get(WalletHistoryActivity.CATAGORY).toString().trim());
			viewHolder.textView_Operator.setVisibility(View.GONE);
			break;
		}
		return convertView;
	}

	private class ViewHolder {
		TextView textView_Name, textView_Operator, textView_Amount,
				textView_Date, textView_ClosingBalance;
		ImageView imageView_Image;
		RelativeLayout relativeLayout_Image;
	}
}
