package com.pay1.adapterhandler;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.WeakHashMap;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.larvalabs.svgandroid.SVGParser;
import com.pay1.R;
import com.pay1.WalletHistoryActivity;
import com.pay1.constants.Constants;
import com.pay1.constants.OperatorConstant;

public class WalletHistoryAdapter extends BaseAdapter {

	private Context context;
	private ArrayList<WeakHashMap<String, String>> data;
	private Typeface Reguler, Medium;

	public WalletHistoryAdapter(Context context,
			ArrayList<WeakHashMap<String, String>> data) {
		this.context = context;
		this.data = data;

		Reguler = Typeface.createFromAsset(context.getAssets(),
				"Roboto-Regular.ttf");

		Medium = Typeface.createFromAsset(context.getAssets(),
				"Roboto-Regular.ttf");
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@SuppressLint("NewApi")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder viewHolder;
		// if (convertView == null) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView = inflater.inflate(R.layout.wallet_history_adapter, null);
		viewHolder = new ViewHolder();
		viewHolder.textView_Name = (TextView) convertView
				.findViewById(R.id.textView_Name);
		viewHolder.textView_Name.setTypeface(Reguler);
		viewHolder.textView_Operator = (TextView) convertView
				.findViewById(R.id.textView_Operator);
		viewHolder.textView_Operator.setTypeface(Medium);
		viewHolder.textView_Amount = (TextView) convertView
				.findViewById(R.id.textView_Amount);
		viewHolder.textView_Amount.setTypeface(Reguler);
		viewHolder.textView_Date = (TextView) convertView
				.findViewById(R.id.textView_Date);
		viewHolder.textView_Date.setTypeface(Medium);
		viewHolder.textView_ClosingBalance = (TextView) convertView
				.findViewById(R.id.textView_ClosingBalance);
		viewHolder.textView_ClosingBalance.setTypeface(Medium);
		viewHolder.imageView_Image = (ImageView) convertView
				.findViewById(R.id.imageView_Image);
		viewHolder.linearLayout_Image = (LinearLayout) convertView
				.findViewById(R.id.linearLayout_Image);
		viewHolder.linearLayout = (LinearLayout) convertView
				.findViewById(R.id.linearLayout);

		convertView.setTag(viewHolder);

		// } else {
		// viewHolder = (ViewHolder) convertView.getTag();
		// }

		WeakHashMap<String, String> map = new WeakHashMap<String, String>();
		map = data.get(position);

		DecimalFormat df = new DecimalFormat("00.00");
		double amt = 0;
		try {
			amt = Double.parseDouble(map.get(WalletHistoryActivity.AMOUNT)
					.toString().trim());
		} catch (Exception e) {
			amt = 0;
		}
		viewHolder.textView_Amount.setText("Rs. " + df.format(amt));

		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"dd-MMM-yyyy,hh:mm aa");
		try {
			String str = map.get(WalletHistoryActivity.TIME).toString().trim()
					.replaceAll("-", "/");
			Date dt = new Date(str);
			viewHolder.textView_Date.setText(dateFormat.format(dt));
		} catch (Exception e) {

		}

		double bal = 0;
		try {
			bal = Double.parseDouble(map.get(WalletHistoryActivity.BALANCE)
					.toString().trim());
		} catch (Exception e) {
			bal = 0;
		}

		double type = 0;
		try {
			type = Double.parseDouble(map.get(WalletHistoryActivity.TRANS_TYPE)
					.toString().trim());
		} catch (Exception e) {
			type = 0;
		}

		viewHolder.textView_ClosingBalance.setText("Closing balance: Rs. "
				+ df.format(bal));

		if (type == 0) {
			viewHolder.textView_ClosingBalance.setTextColor(context
					.getResources().getColor(R.color.Red));
		} else if (type == 1) {
			viewHolder.textView_ClosingBalance.setTextColor(context
					.getResources().getColor(R.color.Green));
		}
		int service_id = 1;
		try {
			service_id = Integer.parseInt(map
					.get(WalletHistoryActivity.SERVICE_ID));
		} catch (Exception e) {
			service_id = 1;
		}
		// Bitmap bitmap=Constants.getBitmapById(statusId,context);

		switch (service_id) {
		case 0:
			viewHolder.linearLayout_Image.setBackgroundColor(context
					.getResources().getColor(R.color.white));
			viewHolder.imageView_Image.setImageDrawable(SVGParser
					.getSVGFromResource(context.getResources(),
							R.raw.ic_profile_wallet).createPictureDrawable());
			viewHolder.linearLayout.setBackground(context.getResources()
					.getDrawable(R.drawable.wallet_circle_view));
/*
			viewHolder.linearLayout.setBackground(context.getResources()
					.getDrawable(R.drawable.wallet_circle_view));*/
			if (Build.VERSION.SDK_INT >= 11)
				viewHolder.imageView_Image.setLayerType(
						View.LAYER_TYPE_SOFTWARE, null);
			viewHolder.textView_Name.setText(map
					.get(WalletHistoryActivity.NUMBER).toString().trim());
			viewHolder.textView_Operator.setText("Transaction reversed");
			viewHolder.textView_Operator.setVisibility(View.VISIBLE);
			break;
		case 1:

			viewHolder.imageView_Image.setImageResource(OperatorConstant
					.getOperatorResource(Integer.parseInt(map
							.get(WalletHistoryActivity.PRODUCT))));
			viewHolder.textView_Name.setText(map
					.get(WalletHistoryActivity.NUMBER).toString().trim());
			viewHolder.textView_Operator.setText("Prepaid recharge");
			viewHolder.textView_Operator.setVisibility(View.VISIBLE);
			break;
		case 2:

			viewHolder.imageView_Image.setImageResource(OperatorConstant
					.getOperatorResource(Integer.parseInt(map
							.get(WalletHistoryActivity.PRODUCT))));
			viewHolder.textView_Name.setText(map
					.get(WalletHistoryActivity.NUMBER).toString().trim());
			viewHolder.textView_Operator.setText("DTH recharge");
			viewHolder.textView_Operator.setVisibility(View.VISIBLE);
			break;
		case 4:

			viewHolder.imageView_Image.setImageResource(OperatorConstant
					.getOperatorResource(Integer.parseInt(map
							.get(WalletHistoryActivity.PRODUCT))));
			viewHolder.textView_Name.setText(map
					.get(WalletHistoryActivity.NUMBER).toString().trim());
			viewHolder.textView_Operator.setText("Postpaid bill");
			viewHolder.textView_Operator.setVisibility(View.VISIBLE);
			break;
		case 5:

			viewHolder.imageView_Image.setImageResource(OperatorConstant
					.getOperatorResource(Integer.parseInt(map
							.get(WalletHistoryActivity.PRODUCT))));
			viewHolder.textView_Name.setText(map
					.get(WalletHistoryActivity.NUMBER).toString().trim());
			viewHolder.textView_Operator.setText("Datacard recharge");
			viewHolder.textView_Operator.setVisibility(View.VISIBLE);
			break;
		case 98:
			/*
			 * viewHolder.linearLayout_Image.setBackgroundColor(context
			 * .getResources().getColor(R.color.white));
			 * viewHolder.imageView_Image
			 * .setImageDrawable(SVGParser.getSVGFromResource(
			 * context.getResources(), R.raw.wallet) .createPictureDrawable());
			 */

			viewHolder.linearLayout_Image.setBackgroundColor(context
					.getResources().getColor(R.color.white));
			viewHolder.imageView_Image.setImageDrawable(SVGParser
					.getSVGFromResource(context.getResources(),
							R.raw.ic_profile_wallet).createPictureDrawable());
			viewHolder.linearLayout.setBackground(context.getResources()
					.getDrawable(R.drawable.wallet_circle_view));

			/*viewHolder.linearLayout.setBackground(context.getResources()
					.getDrawable(R.drawable.wallet_circle_view));*/
			if (Build.VERSION.SDK_INT >= 11)
				viewHolder.imageView_Image.setLayerType(
						View.LAYER_TYPE_SOFTWARE, null);
			viewHolder.textView_Name.setText(map
					.get(WalletHistoryActivity.NUMBER).toString().trim());
			viewHolder.textView_Operator.setText("Wallet topup");
			viewHolder.textView_Operator.setVisibility(View.VISIBLE);
			break;
		case 99:
			/*
			 * viewHolder.linearLayout_Image.setBackgroundColor(context
			 * .getResources().getColor(R.color.app_blue_color));
			 * viewHolder.imageView_Image.setImageDrawable(SVGParser
			 * .getSVGFromResource(context.getResources(), R.raw.ic_gifts)
			 * .createPictureDrawable());
			 */

			viewHolder.linearLayout_Image.setBackgroundColor(context
					.getResources().getColor(R.color.white));
			viewHolder.imageView_Image.setImageDrawable(SVGParser
					.getSVGFromResource(context.getResources(),
							R.raw.ic_profile_wallet).createPictureDrawable());
			viewHolder.linearLayout.setBackground(context.getResources()
					.getDrawable(R.drawable.wallet_circle_view));

		/*	viewHolder.linearLayout.setBackground(context.getResources()
					.getDrawable(R.drawable.wallet_circle_view));*/
			if (Build.VERSION.SDK_INT >= 11)
				viewHolder.imageView_Image.setLayerType(
						View.LAYER_TYPE_SOFTWARE, null);
			viewHolder.textView_Name.setText(map
					.get(WalletHistoryActivity.NUMBER).toString().trim());
			viewHolder.textView_Operator.setText(map
					.get(WalletHistoryActivity.OPERATOR));
			break;
		}
		
		Constants.setFont(context, (ViewGroup)convertView);
		return convertView;
	}

	private class ViewHolder {
		TextView textView_Name, textView_Operator, textView_Amount,
				textView_Date, textView_ClosingBalance;
		ImageView imageView_Image;
		LinearLayout linearLayout_Image, linearLayout;
	}
}
