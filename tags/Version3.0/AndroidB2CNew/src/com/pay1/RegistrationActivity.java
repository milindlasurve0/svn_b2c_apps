package com.pay1;

import java.net.URLEncoder;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.SQLException;
import android.database.sqlite.SQLiteException;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings.Secure;
import android.support.v4.app.FragmentActivity;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.android.gcm.GCMRegistrar;
import com.larvalabs.svgandroid.SVGParser;
import com.pay1.constants.Constants;
import com.pay1.customviews.MyProgressDialog;
import com.pay1.databasehandler.Pay1DataGiftHelper;
import com.pay1.databasehandler.Pay1SyncDataHelper;
import com.pay1.databasehandler.QuickPay;
import com.pay1.databasehandler.QuickPayDataSource;
import com.pay1.requesthandler.RequestClass;
import com.pay1.servicehandler.GPSTracker;
import com.pay1.utilities.EditTextValidator;
import com.pay1.utilities.MemoryManager;
import com.pay1.utilities.MyLocation;
import com.pay1.utilities.MyLocation.LocationResult;
import com.pay1.utilities.Utility;

public class RegistrationActivity extends FragmentActivity {

	private static final String SCREEN_LABEL = "Registration Screen";
	private EasyTracker easyTracker = null;
	private static final long ANIM_VIEWPAGER_DELAY = 1000;
	private Runnable animateViewPager;
	private Handler handler;

	private final String TAG = "Sign Up";
	private TextView textView_CountryCode, textView_TC, textView_PP;
	// private ImageView imageView_Logo;
	private EditText editText_Mobile;
	private Button button_Submit;
	// private ViewPager pager;
	// private RegistrationPagerAdapter adapter;
	private QuickPayDataSource quickPayDataSource;
	private Double longitude, latitude;
	private String uuid, version, manufacturer;

	public static final String AUTHORITY = "com.pay1.provider";
	public static final String ACCOUNT_TYPE = "b2c.pay1.in";
	public static final String ACCOUNT = "Pay1";
	public static final long SECONDS_PER_HOUR = 3600L;
	public static final long SYNC_INTERVAL_IN_HOURS = 2L;
	public static final long SYNC_INTERVAL = SYNC_INTERVAL_IN_HOURS
			* SECONDS_PER_HOUR;

	Account syncAccount;
	ContentResolver syncResolver;
	Pay1SyncDataHelper dbHelper;
	Pay1DataGiftHelper	dataGiftHelper;
	@SuppressLint("NewApi")
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.registration_activity);
		try {
			MemoryManager.clearCache(RegistrationActivity.this);
			MemoryManager.clearApplicationData(RegistrationActivity.this);
			Utility.setRefreshRequired(RegistrationActivity.this,
					Constants.SHAREDPREFERENCE_REFESH_QUICKPAY, true);
			Utility.setRefreshRequired(RegistrationActivity.this,
					Constants.SHAREDPREFERENCE_REFESH_MY_GIFT, true);
			Utility.setRefreshRequired(RegistrationActivity.this,
					Constants.SHAREDPREFERENCE_REFESH_TRANSATION, true);
			Utility.setRefreshRequired(RegistrationActivity.this,
					Constants.SHAREDPREFERENCE_REFESH_WALLET, true);
			
			LinearLayout parentlayout=(LinearLayout)findViewById(R.id.parentlayout);
			
			Constants.setFont(RegistrationActivity.this, parentlayout);

			/*
			 * PackageManager manager = this.getPackageManager(); PackageInfo
			 * info = manager.getPackageInfo( this.getPackageName(), 0); String
			 * version = info.versionName;
			 * Utility.setSharedPreferences(RegistrationActivity.this,
			 * "version", version);
			 */

			ImageView splash_image = (ImageView) findViewById(R.id.splash_image);
			splash_image.setImageDrawable(SVGParser.getSVGFromResource(
					RegistrationActivity.this.getResources(),
					R.raw.pay1_new_logo).createPictureDrawable());
			easyTracker = EasyTracker.getInstance(RegistrationActivity.this);
			easyTracker.set(Fields.SCREEN_NAME, SCREEN_LABEL);
			try {
				dbHelper = new Pay1SyncDataHelper(RegistrationActivity.this);
				dbHelper.getWritableDatabase();

			} catch (SQLiteException se) {
				se.printStackTrace();
			} catch (Exception se) {
				se.printStackTrace();
			} finally {
				dbHelper.close();
			}
			
			
			try {
				dataGiftHelper = new Pay1DataGiftHelper(RegistrationActivity.this);
				dataGiftHelper.getWritableDatabase();

			} catch (SQLiteException se) {
				se.printStackTrace();
			} catch (Exception se) {
				se.printStackTrace();
			} finally {
				dataGiftHelper.close();
			}
			
			syncAccount = CreateSyncAccount(this);
			syncResolver = getContentResolver();
			new SyncTask().execute();
			ContentResolver.setSyncAutomatically(syncAccount, AUTHORITY, true);
			ContentResolver.addPeriodicSync(syncAccount, AUTHORITY,
					Bundle.EMPTY, SYNC_INTERVAL);
			
			if (Utility.getOneTimeRegistration(RegistrationActivity.this,
					Constants.SHAREDPREFERENCE_REGISTRATION)) {
				
				if(Utility.getLoginFlag(RegistrationActivity.this, Constants.SHAREDPREFERENCE_IS_LOGIN)){
				
				Intent intent = new Intent(RegistrationActivity.this,
						SplashScreenActivity.class);
				intent.putExtra("IS_FLAG", false);
				startActivity(intent);
				// Utility.saveFirstTimeAfterUpdate(RegistrationActivity.this,
				// false);
				finish();
				}

			} else {
				// Utility.saveFirstTimeAfterUpdate(RegistrationActivity.this,
				// true);
				// try {
				// startService(new Intent(RegistrationActivity.this,
				// FreeBiesService.class));
				// } catch (Exception e) {
				// }

				
				easyTracker = EasyTracker
						.getInstance(RegistrationActivity.this);
				easyTracker.set(Fields.SCREEN_NAME, SCREEN_LABEL);
				Typeface Reguler = Typeface.createFromAsset(getAssets(),
						"Roboto-Regular.ttf");
				Typeface Medium = Typeface.createFromAsset(getAssets(),
						"Roboto-Regular.ttf");

				textView_CountryCode = (TextView) findViewById(R.id.textView_CountryCode);
				textView_CountryCode.setTypeface(Medium);

				quickPayDataSource = new QuickPayDataSource(
						RegistrationActivity.this);

				// imageView_Logo = (ImageView)
				// findViewById(R.id.imageView_Logo);

				// SVG svg = SVGParser.getSVGFromResource(getResources(),
				// R.raw.ic_pay1);
				// // Picture picture = svg.getPicture();
				// Drawable drawable = svg.createPictureDrawable();
				//
				// imageView_Logo.setImageDrawable(drawable);
				// if (Build.VERSION.SDK_INT >= 11)
				// imageView_Logo.setLayerType(View.LAYER_TYPE_SOFTWARE, null);

				// pager = (ViewPager) findViewById(R.id.pager);
				/*
				 * adapter = new RegistrationPagerAdapter(
				 * this.getSupportFragmentManager()); pager.setAdapter(adapter);
				 * CirclePageIndicator indicator = (CirclePageIndicator)
				 * findViewById(R.id.indicator); indicator.setViewPager(pager);
				 * 
				 * runnable(5);
				 */

				editText_Mobile = (EditText) findViewById(R.id.editText_Mobile);
				editText_Mobile.setTypeface(Reguler);
				editText_Mobile.addTextChangedListener(new TextWatcher() {

					@Override
					public void onTextChanged(CharSequence s, int start,
							int before, int count) {
						// TODO Auto-generated method stub

						EditTextValidator.hasText(RegistrationActivity.this,
								editText_Mobile,
								Constants.ERROR_MOBILE_BLANK_FIELD);

					}

					@Override
					public void beforeTextChanged(CharSequence s, int start,
							int count, int after) {
						// TODO Auto-generated method stub

					}

					@Override
					public void afterTextChanged(Editable s) {
						// TODO Auto-generated method stub

					}
				});

				editText_Mobile
						.setOnEditorActionListener(new TextView.OnEditorActionListener() {

							@Override
							public boolean onEditorAction(TextView v,
									int actionId, KeyEvent event) {
								// TODO Auto-generated method stub
								if (actionId == EditorInfo.IME_ACTION_GO) {
									submitClick(v);
									return true;
								}
								return false;
							}
						});

				button_Submit = (Button) findViewById(R.id.button_Submit);
				button_Submit.setTypeface(Reguler);
				button_Submit.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						submitClick(v);
					}
				});

				textView_TC = (TextView) findViewById(R.id.textView_TC);
				textView_TC.setPaintFlags(textView_TC.getPaintFlags()
						| Paint.UNDERLINE_TEXT_FLAG);
				textView_TC.setTypeface(Reguler);
				textView_TC.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						startActivity(new Intent(RegistrationActivity.this,
								TCActivity.class));
					}
				});
				textView_PP = (TextView) findViewById(R.id.textView_PP);
				textView_PP.setPaintFlags(textView_TC.getPaintFlags()
						| Paint.UNDERLINE_TEXT_FLAG);
				textView_PP.setTypeface(Reguler);
				textView_PP.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						startActivity(new Intent(RegistrationActivity.this,
								PPActivity.class));
					}
				});

				try {
					TelephonyManager tManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
					uuid = tManager.getDeviceId();
					if (uuid == null) {
						uuid = Secure.getString(
								RegistrationActivity.this.getContentResolver(),
								Secure.ANDROID_ID);
					}
					if (Utility.getUUID(RegistrationActivity.this,
							Constants.SHAREDPREFERENCE_UUID) == null)
						Utility.setUUID(RegistrationActivity.this,
								Constants.SHAREDPREFERENCE_UUID, uuid);
				} catch (Exception exception) {
				}

				if (Utility.getOSVersion(RegistrationActivity.this,
						Constants.SHAREDPREFERENCE_OS_VERSION) == null)
					Utility.setOSVersion(RegistrationActivity.this,
							Constants.SHAREDPREFERENCE_OS_VERSION,
							android.os.Build.VERSION.RELEASE);

				if (Utility.getOSManufacturer(RegistrationActivity.this,
						Constants.SHAREDPREFERENCE_OS_MANUFACTURER) == null)
					Utility.setOSManufacturer(RegistrationActivity.this,
							Constants.SHAREDPREFERENCE_OS_MANUFACTURER,
							android.os.Build.MANUFACTURER);

				try {
					LocationResult locationResult = new LocationResult() {
						@Override
						public void gotLocation(Location location) {
							// Got the location!
							try {
								if (location != null) {
									// //// // Log.w("Location", location + "");
									longitude = location.getLongitude();
									latitude = location.getLatitude();
									/*
									 * Utility.setCurrentLatitude(
									 * RegistrationActivity.this,
									 * Constants.SHAREDPREFERENCE_CURRENT_LATITUDE
									 * , String.valueOf(location
									 * .getLatitude()));
									 * Utility.setCurrentLongitude(
									 * RegistrationActivity.this,
									 * Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE
									 * , String.valueOf(location
									 * .getLongitude()));
									 */

									GPSTracker gps = new GPSTracker(
											RegistrationActivity.this);
									if (gps.canGetLocation()) { // gps enabled}
																// // return
																// boolean
																// true/false

										gps.getLatitude(); // returns latitude
										gps.getLongitude(); // returns longitude

										Utility.setCurrentLatitude(
												RegistrationActivity.this,
												Constants.SHAREDPREFERENCE_CURRENT_LATITUDE,
												String.valueOf(gps
														.getLatitude()));
										Utility.setCurrentLongitude(
												RegistrationActivity.this,
												Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE,
												String.valueOf(gps
														.getLongitude()));
									}

								}
							} catch (Exception exception) {
							}
						}
					};
					MyLocation myLocation = new MyLocation();
					myLocation.getLocation(this, locationResult);
				} catch (Exception exception) {
				}

				GCMRegistrar.checkDevice(this);
				GCMRegistrar.checkManifest(this);
				checkNotNull(Constants.SENDER_ID, "SENDER_ID");

				final String regId = GCMRegistrar.getRegistrationId(this);
				// // // Log.i(TAG, "registration id =====  " + regId);
				Utility.setGCMID(RegistrationActivity.this,
						Constants.SHAREDPREFERENCE_GCMID, regId);
				if (regId.equals("")) {
					GCMRegistrar.register(this, Constants.SENDER_ID);
				} else {
					// // // Log.v(TAG, "Already registered");
				}

				try {
					AccountManager accountManager = AccountManager.get(this);
					Account[] accounts = accountManager
							.getAccountsByType("com.google");
					Utility.setEmail(RegistrationActivity.this,
							Constants.SHAREDPREFERENCE_EMAIL, accounts[0].name
									.toString().trim());
				} catch (Exception e) {
				}
				
				
/*
				syncAccount = CreateSyncAccount(this);
				syncResolver = getContentResolver();
				
				try {
					dbHelper = new Pay1SyncDataHelper(RegistrationActivity.this);
					dbHelper.getWritableDatabase();

				} catch (SQLiteException se) {
					se.printStackTrace();
				} catch (Exception se) {
					se.printStackTrace();
				} finally {
					dbHelper.close();
				}*/

			}

			try {
				if (Utility.isFirstTimeAfterUpdate(RegistrationActivity.this)) {
					/*
					 * startService(new Intent(RegistrationActivity.this,
					 * PlanIntentService.class));
					 */
					Utility.saveFirstTimeAfterUpdate(RegistrationActivity.this,
							false);
				}
			} catch (Exception ee) {

			}

		} catch (Exception e) {
			// Log.e("SVG", e.getMessage());
		}
	}
	
	public class SyncTask extends AsyncTask<String, String, String>{

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			sync(syncAccount);
			return null;
		}
		
	}

	public static Account CreateSyncAccount(Context context) {
		Account newAccount = new Account(ACCOUNT, ACCOUNT_TYPE);
		try {
			AccountManager accountManager = (AccountManager) context
					.getSystemService(ACCOUNT_SERVICE);

			if (accountManager.addAccountExplicitly(newAccount, null, null)) {

			} else {
				/*
				 * The account exists or some other error occurred. Log this,
				 * report it, or handle it internally.
				 */
			}
		} catch (Exception ae) {
			Log.e("run", "run");
			ae.printStackTrace();
		}
		return newAccount;
	}

	public static void sync(Account sAccount) {
		Log.d("Inside Sync", "Insode Sync");
		Bundle settingsBundle = new Bundle();
		settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
		settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);

		ContentResolver.requestSync(sAccount, AUTHORITY, settingsBundle);
	}

	public void runnable(final int size) {/*
										 * 
										 * pager.setCurrentItem(size-1); new
										 * CountDownTimer(180000, 3000) {
										 * 
										 * public void onTick(long
										 * millisUntilFinished) {
										 * 
										 * if (pager.getCurrentItem() == size -
										 * 1) { pager.setCurrentItem(0); } else
										 * {
										 * pager.setCurrentItem(pager.getCurrentItem
										 * () + 1, true); } }
										 * 
										 * public void onFinish() {
										 * 
										 * } }.start();
										 */
	}

	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView().build());
		// EasyTracker.getInstance(this).activityStart(this);
		// easyTracker.send(MapBuilder.createAppView().build());
	}

	@Override
	public void onStop() {
		super.onStop();
		// EasyTracker.getInstance(this).activityStop(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
	}

	private void submitClick(View v) {
		try {

			if (editText_Mobile.getText().toString().startsWith("7")
					|| editText_Mobile.getText().toString().startsWith("8")
					|| editText_Mobile.getText().toString().startsWith("9")) {
				if (EditTextValidator.isValidMobileNumber(
						RegistrationActivity.this, editText_Mobile,
						Constants.ERROR_MOBILE_LENGTH_FIELD)) {
					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
					Utility.setMobileNumber(RegistrationActivity.this,
							Constants.SHAREDPREFERENCE_MOBILE, editText_Mobile
									.getText().toString().trim());
					new RegistrationTask().execute(editText_Mobile.getText()
							.toString().trim(), Utility.getEmail(
							RegistrationActivity.this,
							Constants.SHAREDPREFERENCE_EMAIL));
				}
			} else {
				editText_Mobile.setError("Please enter correct number");
			}
		} catch (Exception e) {
		}
	}

	private void checkNotNull(Object reference, String name) {
		if (reference == null) {
			throw new NullPointerException("Errror");
		}
	}

	public class RegistrationTask extends AsyncTask<String, String, String>
			implements OnDismissListener {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			try {
				String response = RequestClass.getInstance()
						.readPay1B2CRequest(
								RegistrationActivity.this,
								Constants.B2C_URL
										+ "create_user/?mobile_number="
										+ params[0] + "&email="
										+ URLEncoder.encode(params[1], "utf-8")
										+ "&isnew=1&api_version=3");

				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			try {

				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");
					String otp;
					JSONObject jsonObject = new JSONObject(replaced);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {

						JSONObject jsonObjectDesc = jsonObject
								.getJSONObject("description");

						otp = jsonObjectDesc.getString("otp");
						Utility.setBalance(RegistrationActivity.this,
								Constants.SHAREDPREFERENCE_BALANCE,
								jsonObjectDesc.getString("balance"));
						Utility.setBalance(RegistrationActivity.this,
								Constants.SHAREDPREFERENCE_LOYALTY_POINTS,
								jsonObjectDesc.getString("loyalty_points"));
						Utility.setSupportNumber(RegistrationActivity.this,
								Constants.SHAREDPREFERENCE_SUPPORT_NUMBER,
								jsonObjectDesc.getString("support_number")
										.trim());
						Utility.setCookieName(RegistrationActivity.this,
								Constants.SHAREDPREFERENCE_COOKIE_NAME,
								jsonObjectDesc.getString("user_val_name"));
						Utility.setCookieValue(RegistrationActivity.this,
								Constants.SHAREDPREFERENCE_COOKIE_VALUE,
								jsonObjectDesc.getString("user_val"));

						JSONObject jsonObjectTemplate = jsonObjectDesc
								.getJSONObject("ErrorMsg");
						Utility.setTemplate(
								RegistrationActivity.this,
								Constants.SHAREDPREFERENCE_SMS_BILLPAYMENT_FAILURE,
								jsonObjectTemplate
										.getString("BILLPAYMENT_FAILURE"));
						Utility.setTemplate(
								RegistrationActivity.this,
								Constants.SHAREDPREFERENCE_SMS_RECHARGE_FAILURE,
								jsonObjectTemplate
										.getString("RECHARGE_FAILURE"));
						Utility.setTemplate(
								RegistrationActivity.this,
								Constants.SHAREDPREFERENCE_SMS_WALLET_REFILLED_RETAILER,
								jsonObjectTemplate
										.getString("WALLET_REFILLED_RETAILER"));

						Utility.setSharedPreferences(RegistrationActivity.this,
								"Deals_offer",
								jsonObjectDesc.getString("Deals_offer"));

						Utility.setMyTotalGifts(RegistrationActivity.this,
								jsonObjectDesc.getString("total_gifts"));

						Utility.setMyLikes(RegistrationActivity.this,
								jsonObjectDesc.getString("total_mylikes"));

						Utility.setMyTotalRedeem(RegistrationActivity.this,
								jsonObjectDesc.getString("total_redeem"));

						Utility.setMyReviews(RegistrationActivity.this,
								jsonObjectDesc.getString("total_myreviews"));
						Utility.saveshowDialog(RegistrationActivity.this,
								jsonObjectDesc.getBoolean("newApp"));
						Utility.setSharedPreferences(RegistrationActivity.this,
								"msg_title",
								jsonObjectDesc.getString("msg_title"));
						Utility.setSharedPreferences(RegistrationActivity.this,
								"msg_body",
								jsonObjectDesc.getString("msg_body"));
						try {
							quickPayDataSource.open();

							List<QuickPay> list = quickPayDataSource
									.getAllQuickPay();
							if (list.size() == 0) {
								JSONArray jsonArray = jsonObjectDesc
										.getJSONArray("recent_pay1_transactions");

								if (jsonArray.length() != 0) {
									long timestamp = System.currentTimeMillis();
									quickPayDataSource.createQuickPay(
											jsonArray, timestamp);
								}
							}
						} catch (SQLException exception) {
							// TODO: handle exception
							// // // Log.e("Er ", exception.getMessage());
						} catch (Exception e) {
							// TODO: handle exception
							// // // Log.e("Er ", e.getMessage());
						} finally {
							quickPayDataSource.close();
						}

						Intent intent = new Intent(RegistrationActivity.this,
								VerificationActivity.class);
						intent.putExtra("MOBILE", editText_Mobile.getText()
								.toString().trim());
						intent.putExtra("OTP", otp);
						intent.putExtra("IS_NEW", true);
						startActivity(intent);
						finish();

					} else {

						JSONObject jsonObjectDesc = jsonObject
								.getJSONObject("description");

						otp = jsonObjectDesc.getString("otp");
						Utility.setBalance(RegistrationActivity.this,
								Constants.SHAREDPREFERENCE_LOYALTY_POINTS,
								jsonObjectDesc.getString("loyalty_points"));
						// System.out.println("Bal " + bal);
						Utility.setBalance(RegistrationActivity.this,
								Constants.SHAREDPREFERENCE_BALANCE,
								jsonObjectDesc.getString("balance"));
						Utility.setSupportNumber(RegistrationActivity.this,
								Constants.SHAREDPREFERENCE_SUPPORT_NUMBER,
								jsonObjectDesc.getString("support_number")
										.trim());
						Utility.setCookieName(RegistrationActivity.this,
								Constants.SHAREDPREFERENCE_COOKIE_NAME,
								jsonObjectDesc.getString("user_val_name"));
						Utility.setCookieValue(RegistrationActivity.this,
								Constants.SHAREDPREFERENCE_COOKIE_VALUE,
								jsonObjectDesc.getString("user_val"));

						Utility.setMyTotalGifts(RegistrationActivity.this,
								jsonObjectDesc.getString("total_gifts"));

						Utility.setMyLikes(RegistrationActivity.this,
								jsonObjectDesc.getString("total_mylikes"));

						Utility.setMyTotalRedeem(RegistrationActivity.this,
								jsonObjectDesc.getString("total_redeem"));

						Utility.setMyReviews(RegistrationActivity.this,
								jsonObjectDesc.getString("total_myreviews"));
						Utility.saveshowDialog(RegistrationActivity.this,
								Boolean.valueOf(jsonObjectDesc
										.getString("newApp")));
						Utility.setSharedPreferences(RegistrationActivity.this,
								"msg_title",
								jsonObjectDesc.getString("msg_title"));
						Utility.setSharedPreferences(RegistrationActivity.this,
								"msg_body",
								jsonObjectDesc.getString("msg_body"));

						try {
							quickPayDataSource.open();

							List<QuickPay> list = quickPayDataSource
									.getAllQuickPay();
							if (list.size() == 0) {
								JSONArray jsonArray = jsonObjectDesc
										.getJSONArray("recent_pay1_transactions");
								long timestamp = System.currentTimeMillis();
								quickPayDataSource.createQuickPay(jsonArray,
										timestamp);
							}
						} catch (SQLException exception) {
							// TODO: handle exception
							// // // Log.e("Er ", exception.getMessage());
						} catch (Exception e) {
							// TODO: handle exception
							// // // Log.e("Er ", e.getMessage());
						} finally {
							quickPayDataSource.close();
						}
						Utility.setSharedPreferences(RegistrationActivity.this,
								"Deals_offer",
								jsonObjectDesc.getString("Deals_offer"));
						Intent intent = new Intent(RegistrationActivity.this,
								VerificationActivity.class);
						intent.putExtra("MOBILE", editText_Mobile.getText()
								.toString().trim());
						intent.putExtra("OTP", otp);
						intent.putExtra("IS_NEW", false);
						startActivity(intent);
						finish();
					}
				} else {
					Intent intent = new Intent(RegistrationActivity.this,
							ConnectivityErrorActivity.class);
					startActivity(intent);
				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonDialog(RegistrationActivity.this, TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonDialog(RegistrationActivity.this, TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(RegistrationActivity.this);
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							RegistrationTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			RegistrationTask.this.cancel(true);
			dialog.cancel();
		}

	}

}
