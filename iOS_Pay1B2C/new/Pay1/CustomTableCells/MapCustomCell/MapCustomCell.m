//
//  MapCustomCell.m
//  Pay1
//
//  Created by Annapurna on 18/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "MapCustomCell.h"
#import "GiftLocationList.h"
#import "Constant.h"
#import "CurrentLocation.h"
@implementation MapCustomCell

- (void)awakeFromNib {
    // Initialization code
    
    if([UIScreen mainScreen].bounds.size.width == 320){
  //  mapPinImgView.frame = CGRectMake(275, 17, 30, 40);
   // distanceLabel.frame = CGRectMake(225, 92, 80, 21);
   // addressLabel.frame = CGRectMake(8, 46, 275, 45);
        
    }
    
    buttonBottomView.backgroundColor = [UIColor colorWithRed:73/255.0 green:149/255.0 blue:238/255.0 alpha:1.0];
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        NSArray *tblView=[[NSBundle mainBundle]loadNibNamed:@"MapCustomCell" owner:self options:nil];
        self=[tblView objectAtIndex:0];
        
        
    }
    return self;
}

-(void)showGiftLocationOnMap:(GiftLocationList *)objList{
    
    
    phoneNumButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    phoneNumButton.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    
    addressLabel.text = objList.fullAddress;
    addressLabel.numberOfLines = 2;
    [addressLabel sizeToFit];
    
    NSLog(@"#TheDealerContract :%@",objList.dealerContract);
    
    [phoneNumButton setTitle:objList.dealerContract forState:UIControlStateNormal];

    CGRect rect = phoneNumButton.titleLabel.frame;
    buttonBottomView.frame = rect;
    
    giftLocLat = objList.locationLat;
    giftLocLong = objList.locationLong;
    
    CLLocation *location = [CurrentLocation sharedLocation].currentLocation;
    
    distanceLabel.text = [Constant calculateDistanceBetweenTwoPoints:objList.locationLat firstLong:objList.locationLong secondLat:[NSString stringWithFormat:@"%f",location.coordinate.latitude] secondLong:[NSString stringWithFormat:@"%f",location.coordinate.longitude]];
    
  
    
}

-(IBAction)locationButtonClick:(id)sender{
    if(_onMapButtonCLick){
    _onMapButtonCLick(giftLocLat,giftLocLong,addressLabel.text);
    }
}

-(IBAction)phoneNumCLick:(id)sender{
    NSLog(@"#PhoneNumber :%@",phoneNumButton.titleLabel.text);
    if(_onPhoneButtonCLick)
    _onPhoneButtonCLick(phoneNumButton.titleLabel.text);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
