package com.pay1;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.larvalabs.svgandroid.SVGParser;
import com.pay1.constants.Constants;
import com.pay1.constants.OperatorConstant;
import com.pay1.customviews.MyProgressDialog;
import com.pay1.databasehandler.FreeBiesDataSource;
import com.pay1.databasehandler.FreeBiesLocationDataSource;
import com.pay1.databasehandler.FreeBiesOrderDataSource;
import com.pay1.databasehandler.NumbberWithCircleDataSource;
import com.pay1.databasehandler.NumberWithCircle;
import com.pay1.databasehandler.Operator;
import com.pay1.databasehandler.OperatorDataSource;
import com.pay1.databasehandler.QuickPay;
import com.pay1.databasehandler.QuickPayDataSource;
import com.pay1.requesthandler.RequestClass;
import com.pay1.utilities.EditTextValidator;
import com.pay1.utilities.Utility;

public class DataCardFragment extends Fragment {

	static int PICK_CONTACT = 100;
	static int PICK_OPERATOR = 111;
	static int PICK_PLAN = 112;

	private TextView textView_TitleMobile, textView_TitleOperator,
			textView_Operator, textView_TitleAmount;
	private EditText editText_Mobile, editText_Amount;
	private ImageView imageView_Contact, imageView_Operator;
	private Button button_Submit, btnPlans,textViewPayBy;

	String circle_code = null, operatorCode = null, operatorID = null,
			operatorName = null, operatorProductID = null;
	static String isStv = "0";
	int rechargeType, rechAmount, service_charge;

	private OperatorDataSource operatorDataSource;
	private NumbberWithCircleDataSource circleDataSource;
	private QuickPayDataSource quickPayDataSource;
	private FreeBiesDataSource freeBiesDataSource;
	private FreeBiesLocationDataSource freeBiesLocationDataSource;
	private FreeBiesOrderDataSource freeBiesOrderDataSource;
	// private ScrollView scrollView;
	boolean isViaWallet;
	private EasyTracker easyTracker = null;

	public static DataCardFragment newInstance() {
		DataCardFragment fragment = new DataCardFragment();
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	
	
	@SuppressLint("NewApi")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		final View view = inflater.inflate(R.layout.datacard_fragment,
				container, false);
		try {

			easyTracker = EasyTracker.getInstance(getActivity());
			operatorDataSource = new OperatorDataSource(getActivity());
			circleDataSource = new NumbberWithCircleDataSource(getActivity());
			quickPayDataSource = new QuickPayDataSource(getActivity());
			freeBiesDataSource = new FreeBiesDataSource(getActivity());
			freeBiesLocationDataSource = new FreeBiesLocationDataSource(
					getActivity());
			freeBiesOrderDataSource = new FreeBiesOrderDataSource(getActivity());
			rechargeType = Constants.RECHARGE_DATA;

			Typeface Reguler = Typeface.createFromAsset(getActivity()
					.getAssets(), "Roboto-Regular.ttf");
			Typeface Medium = Typeface.createFromAsset(getActivity()
					.getAssets(), "Roboto-Regular.ttf");

			textView_TitleMobile = (TextView) view
					.findViewById(R.id.textView_TitleMobile);
			textView_TitleMobile.setTypeface(Medium);
			textView_TitleMobile.setVisibility(View.GONE);
			textView_TitleOperator = (TextView) view
					.findViewById(R.id.textView_TitleOperator);
			textView_TitleOperator.setTypeface(Medium);
			textView_TitleOperator.setVisibility(View.GONE);
			textView_TitleAmount = (TextView) view
					.findViewById(R.id.textView_TitleAmount);
			textView_TitleAmount.setTypeface(Medium);
			textView_TitleAmount.setVisibility(View.GONE);

			textViewPayBy = (Button) view.findViewById(R.id.textViewPayBy);
			textViewPayBy.setTypeface(Medium);
			textViewPayBy.setPaintFlags(textViewPayBy.getPaintFlags()
					| Paint.UNDERLINE_TEXT_FLAG);

			textView_Operator = (TextView) view
					.findViewById(R.id.textView_Operator);
			textView_Operator.setTypeface(Reguler);
			editText_Mobile = (EditText) view
					.findViewById(R.id.editText_Mobile);
			editText_Mobile.setTypeface(Reguler);
			editText_Amount = (EditText) view
					.findViewById(R.id.editText_Amount);
			editText_Amount.setTypeface(Reguler);
			imageView_Contact = (ImageView) view
					.findViewById(R.id.imageView_Contact);
			imageView_Contact.setVisibility(View.GONE);
			imageView_Contact.setImageDrawable(SVGParser.getSVGFromResource(
					getActivity().getResources(), R.raw.ic_contact)
					.createPictureDrawable());
			if (Build.VERSION.SDK_INT >= 11)
				imageView_Contact.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
			imageView_Operator = (ImageView) view
					.findViewById(R.id.imageView_Operator);
			imageView_Operator.setImageDrawable(SVGParser.getSVGFromResource(
					getActivity().getResources(), R.raw.ic_next)
					.createPictureDrawable());
			if (Build.VERSION.SDK_INT >= 11)
				imageView_Operator.setLayerType(View.LAYER_TYPE_SOFTWARE, null);

			button_Submit = (Button) view.findViewById(R.id.button_Submit);
			button_Submit.setTypeface(Reguler);
			btnPlans = (Button) view.findViewById(R.id.btnPlans);
			btnPlans.setTypeface(Reguler);
			btnPlans.setVisibility(View.GONE);

			editText_Mobile.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					// TODO Auto-generated method stub

					if (editText_Mobile.getText().toString().trim().length() > 0) {
						textView_TitleMobile.setVisibility(View.GONE);
						if (EditTextValidator.hasFragmentText(getActivity(),
								editText_Mobile,
								Constants.ERROR_DATA_LENGTH_FIELD))
							imageView_Contact.setVisibility(View.GONE);
						else
							imageView_Contact.setVisibility(View.GONE);

						textView_Operator.setText("");
						btnPlans.setVisibility(View.GONE);
						isStv = "0";
					} else {
						textView_TitleMobile.setVisibility(View.GONE);
					}

					if (editText_Mobile.getText().toString().trim().length() == 10) {
						if (editText_Mobile.getText().toString().trim()
								.startsWith("9")
								|| editText_Mobile.getText().toString().trim()
										.startsWith("8")
								|| editText_Mobile.getText().toString().trim()
										.startsWith("7")) {
							editText_Amount.requestFocus();
							// new GetPlansCircletask().execute();

							// try {
							// operatorCodeDataSource.open();
							// operatorDataSource.open();
							//
							// List<OperatorCode> list = operatorCodeDataSource
							// .getAllOperatorCode();
							//
							// if (list.size() != 0) {
							// JSONArray array = new JSONArray(list.get(0)
							// .getOperatorCodeData());
							// JSONObject jsonObject = array
							// .getJSONObject(0);
							// String status = jsonObject
							// .getString("status");
							// if (status.equalsIgnoreCase("SUCCESS")) {
							//
							// JSONArray jsonArrayDetails = jsonObject
							// .getJSONArray("details");
							//
							// for (int i = 0; i < jsonArrayDetails
							// .length(); i++) {
							// JSONObject jsonObjectdetails = jsonArrayDetails
							// .getJSONObject(i);
							//
							// if (jsonObjectdetails
							// .getString("number")
							// .trim()
							// .startsWith(
							// editText_Mobile
							// .getText()
							// .toString()
							// .trim())) {
							// circle_code = jsonObjectdetails
							// .getString("area")
							// .toString().trim();
							// operatorCode = jsonObjectdetails
							// .getString("operator")
							// .toString().trim();
							//
							// Operator operator;
							// if (rechargeType == Constants.RECHARGE_MOBILE) {
							// operator = operatorDataSource
							// .getOperator(
							// Constants.RECHARGE_MOBILE,
							// operatorCode);
							// } else {
							// operator = operatorDataSource
							// .getOperator(
							// Constants.BILL_PAYMENT,
							// operatorCode);
							// }
							// if (operator != null) {
							// operatorName = operator
							// .getOperatorName();
							// operatorProductID = String.valueOf(operator
							// .getOperatorProductID());
							// operatorID = String.valueOf(operator
							// .getOperatorID());
							// }
							//
							// textView_Operator
							// .setText(operatorName);
							// btnPlans.setVisibility(View.VISIBLE);
							//
							// break;
							// }
							// }
							// } else {
							// new GetPlansCircletask().execute();
							// }
							// } else {
							// new GetPlansCircletask().execute();
							// }
							//
							// } catch (SQLException exception) {
							// // TODO: handle exception
							// // // // Log.e("Er ", exception.getMessage());
							// } catch (Exception e) {
							// // TODO: handle exception
							// // // // Log.e("Er ", e.getMessage());
							// } finally {
							// operatorDataSource.close();
							// operatorCodeDataSource.close();
							// }

							try {
								circleDataSource.open();
								operatorDataSource.open();
								quickPayDataSource.open();

								List<QuickPay> list = quickPayDataSource
										.getAllQuickPay(editText_Mobile
												.getText().toString().trim(), 1);

								NumberWithCircle numberWithCircle = circleDataSource
										.getNumberDetails(editText_Mobile
												.getText().toString().trim());

								if (numberWithCircle != null) {
									circle_code = numberWithCircle
											.getAreaCode();
									if (list.size() != 0) {
										operatorCode = list.get(0)
												.getQuickPayOperatorCode();
									} else {
										operatorCode = numberWithCircle
												.getOperatorName();
									}
									Operator operator = operatorDataSource
											.getOperator(
													Constants.RECHARGE_DATA,
													operatorCode);

									if (operator != null) {
										operatorName = operator
												.getOperatorName();
										operatorProductID = String.valueOf(operator
												.getOperatorProductID());
										operatorID = String.valueOf(operator
												.getOperatorID());
									}

									textView_Operator.setText(operatorName);
									btnPlans.setVisibility(View.VISIBLE);

								} else {
									new GetPlansCircletask().execute();
								}

							} catch (SQLException exception) {
								// TODO: handle exception
								// Log.e("Er ", exception.getMessage());
							} catch (Exception e) {
								// TODO: handle exception
								// // // Log.e("Er ", e.getMessage());
							} finally {
								quickPayDataSource.close();
								operatorDataSource.close();
								circleDataSource.close();
							}
						} else {
							Constants.showOneButtonDialog(getActivity(),
									"Data Card",
									Constants.ERROR_DATA_LENGTH_FIELD,
									Constants.DIALOG_CLOSE);
						}
					}
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					// TODO Auto-generated method stub

				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

				}
			});

			editText_Mobile
					.setOnEditorActionListener(new TextView.OnEditorActionListener() {

						@Override
						public boolean onEditorAction(TextView v, int actionId,
								KeyEvent event) {
							// TODO Auto-generated method stub
							if (actionId == EditorInfo.IME_ACTION_NEXT) {
								if (EditTextValidator
										.isValidFragmentMobileNumber(
												getActivity(),
												editText_Mobile,
												Constants.ERROR_MOBILE_LENGTH_FIELD))
									imageView_Contact.setVisibility(View.GONE);
								else
									imageView_Contact.setVisibility(View.GONE);
								return true;
							}
							return false;
						}
					});

			textView_Operator.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					// TODO Auto-generated method stub
					if (textView_Operator.getText().toString().trim().length() > 0) {
						textView_TitleOperator.setVisibility(View.GONE);
						if (EditTextValidator.hasFragmentText(getActivity(),
								textView_Operator,
								Constants.ERROR_OPERATOR_BLANK_FIELD))
							imageView_Operator.setVisibility(View.VISIBLE);
						else
							imageView_Operator.setVisibility(View.GONE);
					} else {
						textView_TitleOperator.setVisibility(View.GONE);
					}
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					// TODO Auto-generated method stub

				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

				}
			});

			editText_Amount.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					// TODO Auto-generated method stub
					if (editText_Amount.getText().toString().trim().length() != 0) {
						textView_TitleAmount.setVisibility(View.GONE);
					} else {
						textView_TitleAmount.setVisibility(View.GONE);
					}

					if (editText_Amount.getText().toString().trim().length() > 0) {
						if (EditTextValidator.hasFragmentText(getActivity(),
								editText_Amount,
								Constants.ERROR_AMOUNT_BLANK_FIELD)) {
							btnPlans.setVisibility(View.VISIBLE);
						} else
							btnPlans.setVisibility(View.GONE);

						String charges_slab = null;
						double service_charge_amount = 0, service_charge_percent = 0, service_tax_percent = 0, min = 0, max = 0;
						try {
							operatorDataSource.open();

							Operator operator = operatorDataSource.getOperator(
									Constants.RECHARGE_DATA, operatorCode);

							if (operator != null) {
								charges_slab = operator.getOperatorChargeSlab();
								service_charge_amount = operator
										.getOperatorServiceChargeAmount();
								service_charge_percent = operator
										.getOperatorServiceChargePercent();
								service_tax_percent = operator
										.getOperatorServiceTaxPercent();
								min = operator.getOperatorMinAmount();
								max = operator.getOperatorMaxAmount();
							}

						} catch (SQLException exception) {
							// TODO: handle exception
							// // // Log.e("Er ", exception.getMessage());
						} catch (Exception e) {
							// TODO: handle exception
							// // // Log.e("Er ", e.getMessage());
						} finally {
							operatorDataSource.close();
						}

						double rechargeAmount = 0, balance = 0;
						try {
							rechargeAmount = Double.parseDouble(editText_Amount
									.getText().toString().trim());
							balance = Double.parseDouble(Utility.getBalance(
									getActivity(),
									Constants.SHAREDPREFERENCE_BALANCE));
						} catch (Exception e) {
							rechargeAmount = 0;
							balance = 0;
						}

						int slab_amount = 0;
						try {
							if (charges_slab.equalsIgnoreCase("null")
									|| charges_slab.equalsIgnoreCase("")) {
								rechargeAmount = (rechargeAmount + (rechargeAmount * service_charge_percent));
								rechargeAmount = (rechargeAmount + (rechargeAmount * service_tax_percent));
							} else {
								String[] comma_seperator = charges_slab
										.split(",");
								for (String s1 : comma_seperator) {
									String[] colon_sperator = s1.split(":");
									if (rechargeAmount > Double
											.parseDouble(colon_sperator[0])) {
										slab_amount = Integer
												.parseInt(colon_sperator[1]);
									}
								}
							}
						} catch (Exception e) {
						}
						if ((rechargeAmount + slab_amount) > balance) {
							button_Submit.setText(getActivity().getResources()
									.getString(R.string.online_payment));
							textViewPayBy.setText(getActivity().getResources()
									.getString(R.string.offline_payment));
							isViaWallet=false;
						} else {
							button_Submit.setText(getActivity().getResources()
									.getString(R.string.wallet_payment));
							textViewPayBy.setText(getActivity().getResources()
									.getString(R.string.online_payment));
							isViaWallet=true;
						}
					}
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					// TODO Auto-generated method stub

				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

				}
			});

			imageView_Contact.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent intent = new Intent(Intent.ACTION_PICK,
							ContactsContract.Contacts.CONTENT_URI);
					startActivityForResult(intent, PICK_CONTACT);
					getActivity().overridePendingTransition(
							R.anim.slide_in_right, R.anim.slide_out_left);
				}
			});

			textView_Operator.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent intent = new Intent(getActivity(),
							OperatorActivity.class);
					intent.putExtra(Constants.RECHARGE_FOR,
							Constants.RECHARGE_DATA);
					startActivityForResult(intent, PICK_OPERATOR);
					getActivity().overridePendingTransition(
							R.anim.slide_in_right, R.anim.slide_out_left);
				}
			});

			imageView_Operator.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent intent = new Intent(getActivity(),
							OperatorActivity.class);
					intent.putExtra(Constants.RECHARGE_FOR,
							Constants.RECHARGE_DATA);
					startActivityForResult(intent, PICK_OPERATOR);
					getActivity().overridePendingTransition(
							R.anim.slide_in_right, R.anim.slide_out_left);
				}
			});

			easyTracker.set(Fields.SCREEN_NAME, "Data Card Screen");

			button_Submit.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					if (checkValidation(getActivity())) {
						if (Utility.getLoginFlag(getActivity(),
								Constants.SHAREDPREFERENCE_IS_LOGIN)) {

							InputMethodManager imm = (InputMethodManager) getActivity()
									.getSystemService(
											Context.INPUT_METHOD_SERVICE);
							imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

							String charges_slab = null;
							double service_charge_amount = 0, service_charge_percent = 0, service_tax_percent = 0, min = 0, max = 0;
							try {
								operatorDataSource.open();

								Operator operator = operatorDataSource
										.getOperator(Constants.RECHARGE_DATA,
												operatorCode);

								if (operator != null) {
									charges_slab = operator
											.getOperatorChargeSlab();
									service_charge_amount = operator
											.getOperatorServiceChargeAmount();
									service_charge_percent = operator
											.getOperatorServiceChargePercent();
									service_tax_percent = operator
											.getOperatorServiceTaxPercent();
									min = operator.getOperatorMinAmount();
									max = operator.getOperatorMaxAmount();
								}

							} catch (SQLException exception) {
								// TODO: handle exception
								// // // Log.e("Er ", exception.getMessage());
							} catch (Exception e) {
								// TODO: handle exception
								// // // Log.e("Er ", e.getMessage());
							} finally {
								operatorDataSource.close();
							}

							double rechargeAmount = 0, balance = 0;
							try {
								rechargeAmount = Double
										.parseDouble(editText_Amount.getText()
												.toString().trim());
								balance = Double.parseDouble(Utility
										.getBalance(
												getActivity(),
												Constants.SHAREDPREFERENCE_BALANCE));
							} catch (Exception e) {
								rechargeAmount = 0;
								balance = 0;
							}
							if (rechargeAmount < min || rechargeAmount > max) {
								EditTextValidator.isValidFragmentAmount(
										getActivity(), editText_Amount,
										Constants.ERROR_AMOUNT_RANG_FIELD + min
												+ " to " + max, min, max);
								editText_Amount.requestFocus();
								btnPlans.setVisibility(View.GONE);
								return;
							}
							int slab_amount = 0;
							try {
								if (charges_slab.equalsIgnoreCase("null")
										|| charges_slab.equalsIgnoreCase("")) {
									rechargeAmount = (rechargeAmount + (rechargeAmount * service_charge_percent));
									rechargeAmount = (rechargeAmount + (rechargeAmount * service_tax_percent));
								} else {
									String[] comma_seperator = charges_slab
											.split(",");
									for (String s : comma_seperator) {
										String[] colon_sperator = s.split(":");
										if (rechargeAmount > Double
												.parseDouble(colon_sperator[0])) {
											slab_amount = Integer
													.parseInt(colon_sperator[1]);
										}
									}
								}
							} catch (Exception e) {
							}

							service_charge = slab_amount;

							if (button_Submit
									.getText()
									.toString()
									.trim()
									.equalsIgnoreCase(
											getActivity()
													.getResources()
													.getString(
															R.string.wallet_payment))) {
								showConfirmationDialog(getActivity(),
										"Confirmation",
										"Are you sure you want to recharge?",
										false);

							} else {
								showConfirmationDialog(getActivity(),
										"Confirmation",
										"Are you sure you want to recharge?",
										true);
							}
						} else {
							// Constants.showOneButtonDialog(RechargeMobileActivity.this,
							// "Recharge ", "Please Login First.",
							// Constants.DIALOG_CLOSE_LOGIN);
							Intent intent = new Intent(getActivity(),
									LoginActivity.class);
							startActivity(intent);
						}
					}
				}
			});

			textViewPayBy.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					if (checkValidation(getActivity())) {
						if (Utility.getLoginFlag(getActivity(),
								Constants.SHAREDPREFERENCE_IS_LOGIN)) {

							InputMethodManager imm = (InputMethodManager) getActivity()
									.getSystemService(
											Context.INPUT_METHOD_SERVICE);
							imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

							String charges_slab = null;
							double service_charge_amount = 0, service_charge_percent = 0, service_tax_percent = 0, min = 0, max = 0;
							try {
								operatorDataSource.open();

								Operator operator = operatorDataSource
										.getOperator(Constants.RECHARGE_DATA,
												operatorCode);

								if (operator != null) {
									charges_slab = operator
											.getOperatorChargeSlab();
									service_charge_amount = operator
											.getOperatorServiceChargeAmount();
									service_charge_percent = operator
											.getOperatorServiceChargePercent();
									service_tax_percent = operator
											.getOperatorServiceTaxPercent();
									min = operator.getOperatorMinAmount();
									max = operator.getOperatorMaxAmount();
								}

							} catch (SQLException exception) {
								// TODO: handle exception
								// // // Log.e("Er ", exception.getMessage());
							} catch (Exception e) {
								// TODO: handle exception
								// // // Log.e("Er ", e.getMessage());
							} finally {
								operatorDataSource.close();
							}

							double rechargeAmount = 0, balance = 0;
							try {
								rechargeAmount = Double
										.parseDouble(editText_Amount.getText()
												.toString().trim());
								balance = Double.parseDouble(Utility
										.getBalance(
												getActivity(),
												Constants.SHAREDPREFERENCE_BALANCE));
							} catch (Exception e) {
								rechargeAmount = 0;
								balance = 0;
							}
							if (rechargeAmount < min || rechargeAmount > max) {
								EditTextValidator.isValidFragmentAmount(
										getActivity(), editText_Amount,
										Constants.ERROR_AMOUNT_RANG_FIELD + min
												+ " to " + max, min, max);
								editText_Amount.requestFocus();
								btnPlans.setVisibility(View.GONE);
								return;
							}
							int slab_amount = 0;
							try {
								if (charges_slab.equalsIgnoreCase("null")
										|| charges_slab.equalsIgnoreCase("")) {
									rechargeAmount = (rechargeAmount + (rechargeAmount * service_charge_percent));
									rechargeAmount = (rechargeAmount + (rechargeAmount * service_tax_percent));
								} else {
									String[] comma_seperator = charges_slab
											.split(",");
									for (String s : comma_seperator) {
										String[] colon_sperator = s.split(":");
										if (rechargeAmount > Double
												.parseDouble(colon_sperator[0])) {
											slab_amount = Integer
													.parseInt(colon_sperator[1]);
										}
									}
								}
							} catch (Exception e) {
							}

							service_charge = slab_amount;

							if (button_Submit
									.getText()
									.toString()
									.trim()
									.equalsIgnoreCase(
											getActivity()
													.getResources()
													.getString(
															R.string.wallet_payment))) {
								showConfirmationDialog(getActivity(),
										"Confirmation",
										"Are you sure you want to recharge?",
										true);
							} else {
								Intent intent = new Intent(getActivity(),
										MapActivity.class);
								intent.putExtra("SHOW_GIFT", false);
								intent.putExtra("SHOW_RETAILER", true);
								getActivity().startActivity(intent);
							}

						} else {
							// Constants.showOneButtonDialog(RechargeMobileActivity.this,
							// "Recharge ", "Please Login First.",
							// Constants.DIALOG_CLOSE_LOGIN);
							Intent intent = new Intent(getActivity(),
									LoginActivity.class);
							startActivity(intent);
						}
					}
				}
			});

			btnPlans.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent plansIntent = new Intent(getActivity(),
							PlanFragmentActivity.class);
					// if (bundle.getInt(Constants.RECHARGE_FOR) ==
					// Constants.RECHARGE_MOBILE) {
					plansIntent.putExtra(Constants.RECHARGE_FOR,
							Constants.RECHARGE_DATA);
					plansIntent.putExtra("OPERATOR", textView_Operator
							.getText().toString().trim());
					// } else if (bundle.getInt(Constants.RECHARGE_FOR) ==
					// Constants.RECHARGE_DATA) {
					// plansIntent.putExtra(Constants.RECHARGE_FOR,
					// Constants.RECHARGE_DATA);
					// }
					int id = 0;
					try {
						operatorDataSource.open();

						Operator operator = operatorDataSource.getOperator(
								Constants.RECHARGE_MOBILE, operatorCode);

						if (operator != null) {
							id = operator.getOperatorProductID();
						}

					} catch (SQLException exception) {
						// TODO: handle exception
						// // // Log.e("Er ", exception.getMessage());
					} catch (Exception e) {
						// TODO: handle exception
						// // // Log.e("Er ", e.getMessage());
					} finally {
						operatorDataSource.close();
					}
					if (editText_Mobile.getText().toString().trim().length() != 0) {
						plansIntent.putExtra(Constants.PLAN_JSON,
								String.valueOf(id));
						plansIntent.putExtra("CIRCLE_CODE", circle_code);
						plansIntent.putExtra("DATA_CARD_PLAN", 1);
					} else {
						plansIntent.putExtra(Constants.PLAN_JSON,
								String.valueOf(id));
						plansIntent.putExtra("CIRCLE_CODE", "");
						plansIntent.putExtra("DATA_CARD_PLAN", 1);
					}
					startActivityForResult(plansIntent, PICK_PLAN);
				}
			});

			if (Utility.getBalance(getActivity(),
					Constants.SHAREDPREFERENCE_BALANCE).equalsIgnoreCase("0")) {
				button_Submit.setText(getActivity().getResources().getString(
						R.string.online_payment));
				textViewPayBy.setText(getActivity().getResources().getString(
						R.string.offline_payment));
			} else {
				button_Submit.setText(getActivity().getResources().getString(
						R.string.wallet_payment));
				textViewPayBy.setText(getActivity().getResources().getString(
						R.string.online_payment));
			}
		} catch (Exception e) {
		}
		return view;
	}

	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView().build());
		// EasyTracker.getInstance(this).activityStart(this);
		easyTracker.send(MapBuilder.createAppView().build());
	}

	@Override
	public void onStop() {
		super.onStop();
		// EasyTracker.getInstance(this).activityStop(this);
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
	}

	public boolean checkValidation(Context context) {
		boolean ret = true;

		if (!EditTextValidator.hasFragmentText(context, editText_Mobile,
				Constants.ERROR_DATA_LENGTH_FIELD)) {
			ret = false;
			editText_Mobile.requestFocus();
			imageView_Contact.setVisibility(View.GONE);
			return ret;
		} else if (!EditTextValidator.isValidFragmentMobileNumber(context,
				editText_Mobile, Constants.ERROR_DATA_LENGTH_FIELD)) {
			ret = false;
			editText_Mobile.requestFocus();
			imageView_Contact.setVisibility(View.GONE);
			return ret;
		} else if (!EditTextValidator.hasFragmentText(context,
				textView_Operator, Constants.ERROR_OPERATOR_BLANK_FIELD)) {
			ret = false;
			textView_Operator.requestFocus();
			imageView_Operator.setVisibility(View.GONE);
			return ret;
		} else if (!EditTextValidator.hasFragmentText(context, editText_Amount,
				Constants.ERROR_AMOUNT_BLANK_FIELD)) {
			ret = false;
			editText_Amount.requestFocus();
			btnPlans.setVisibility(View.GONE);
			return ret;
		} else if (!EditTextValidator.isValidFragmentAmount(context,
				editText_Amount, Constants.ERROR_AMOUNT_VALID_FIELD)) {
			ret = false;
			editText_Amount.requestFocus();
			btnPlans.setVisibility(View.GONE);
			return ret;
		} else
			return ret;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		try {
			if (resultCode == Activity.RESULT_OK) {
				if (requestCode == PICK_PLAN) {
					editText_Amount.setText(data.getExtras()
							.getString("result"));
				}
				if (requestCode == PICK_OPERATOR) {
					textView_Operator.setText(data.getExtras().getString(
							"OperatorName"));
					btnPlans.setVisibility(View.VISIBLE);
					operatorCode = data.getExtras().getString("OperatorCode");
					operatorID = data.getExtras().getString("OperatorID");
					operatorProductID = data.getExtras().getString(
							"OperatorProductID");
					int d = Integer.parseInt(data.getExtras().getString("STV"));
				}
				if (requestCode == PICK_CONTACT) {
					getActivity().overridePendingTransition(
							R.anim.slide_in_left, R.anim.slide_out_right);
					Uri contactData = data.getData();
					Cursor contactCursor = getActivity()
							.getContentResolver()
							.query(contactData,
									new String[] { ContactsContract.Contacts._ID },
									null, null, null);
					String id = null;
					if (contactCursor.moveToFirst()) {
						id = contactCursor.getString(contactCursor
								.getColumnIndex(ContactsContract.Contacts._ID));
					}
					contactCursor.close();
					String phoneNumber = null, displayName = null;
					Cursor phoneCursor = getActivity()
							.getContentResolver()
							.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
									new String[] {
											ContactsContract.CommonDataKinds.Phone.NUMBER,
											ContactsContract.PhoneLookup.DISPLAY_NAME },
									ContactsContract.CommonDataKinds.Phone.CONTACT_ID
											+ "= ? ", new String[] { id }, null);
					if (phoneCursor.moveToFirst()) {
						phoneNumber = phoneCursor
								.getString(phoneCursor
										.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
						displayName = phoneCursor
								.getString(phoneCursor
										.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
						phoneNumber = Normalizer.normalize(phoneNumber,
								Normalizer.Form.NFD).replaceAll("[^a-zA-Z0-9]",
								"");
						if (phoneNumber.length() >= 10) {
							phoneNumber = phoneNumber.substring(
									phoneNumber.length() - 10,
									phoneNumber.length());
							editText_Mobile.setText(phoneNumber);
							// if (displayName.equalsIgnoreCase(phoneNumber))
							// textView_Name.setText("");
							// else
							// textView_Name.setText(displayName);
						} else {
							Toast.makeText(getActivity(),
									"Not a valid mobile number.",
									Toast.LENGTH_LONG).show();
							phoneNumber = "";
							displayName = "";
							editText_Mobile.setText("");
							// textView_Name.setText("");
						}

					} else {
						Toast.makeText(getActivity(),
								"Not a valid mobile number.", Toast.LENGTH_LONG)
								.show();
						phoneNumber = "";
						displayName = "";
						editText_Mobile.setText("");
						// textView_Name.setText("");
					}
					phoneCursor.close();
				}

			} else {
				if (requestCode == PICK_CONTACT) {
					getActivity().overridePendingTransition(
							R.anim.slide_in_left, R.anim.slide_out_right);
				}
			}
		} catch (Exception e) {
		}
	}

	@SuppressLint("NewApi")
	public void showConfirmationDialog(final Context context,
			final String Title, final String message, final boolean is_online) {
		try {

			final Dialog dialog = new Dialog(context);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			 dialog.getWindow().setBackgroundDrawableResource(
			 android.R.color.transparent);
			dialog.setContentView(R.layout.dialog_confirmation);
			dialog.setCancelable(false);
			dialog.setCanceledOnTouchOutside(false);
			// dialog.setTitle(null);

			Typeface Reguler = Typeface.createFromAsset(context.getAssets(),
					"Roboto-Regular.ttf");

			Typeface Medium = Typeface.createFromAsset(context.getAssets(),
					"Roboto-Regular.ttf");

			// set the custom dialog components - text, image and button
			TextView textView_Title = (TextView) dialog
					.findViewById(R.id.textView_Title);
			textView_Title.setText(Title);
			textView_Title.setTypeface(Reguler);

			TextView textView_Message = (TextView) dialog
					.findViewById(R.id.textView_Message);
			textView_Message.setText(message);
			textView_Message.setTypeface(Medium);

			TextView textView_Mobile = (TextView) dialog
					.findViewById(R.id.textView_Mobile);
			textView_Mobile
					.setText(editText_Mobile.getText().toString().trim());
			textView_Mobile.setTypeface(Medium);

			TextView textView_Amount = (TextView) dialog
					.findViewById(R.id.textView_Amount);
			textView_Amount
					.setText(editText_Amount.getText().toString().trim());
			textView_Amount.setTypeface(Medium);

			ImageView imageView_Operator = (ImageView) dialog
					.findViewById(R.id.imageView_Operator);
			imageView_Operator.setImageResource(OperatorConstant
					.getOperatorResource(Integer.parseInt(operatorProductID)));

			TextView textView_Symbol = (TextView) dialog
					.findViewById(R.id.textView_Symbol);
			textView_Symbol.setTypeface(Reguler);
			
			
			final CheckBox checkBoxUseWallet = (CheckBox) dialog
					.findViewById(R.id.checkBoxUseWallet);

			checkBoxUseWallet.setTypeface(Medium);

			final String bal_wallet = Utility.getBalance(context,
					Constants.SHAREDPREFERENCE_BALANCE);
			if (Double.parseDouble(bal_wallet) <= 0) {
				checkBoxUseWallet.setVisibility(View.GONE);
				checkBoxUseWallet.setChecked(false);
				isViaWallet = false;
			} else {
				checkBoxUseWallet.setVisibility(View.VISIBLE);
				checkBoxUseWallet.setChecked(true);
				isViaWallet = true;
			}

			Button button_Confirm = (Button) dialog
					.findViewById(R.id.button_Confirm);
			button_Confirm.setTypeface(Reguler);
			// if button is clicked, close the custom dialog
			button_Confirm.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
					if (checkBoxUseWallet.isChecked()) {
						isViaWallet = true;
					} else {
						isViaWallet = false;
					}
					if (isViaWallet) {

						new RechargeViaWalletTask().execute(editText_Mobile
								.getText().toString().trim(), operatorID,
								String.valueOf(rechargeType), editText_Amount
										.getText().toString().trim(), isStv);
					} else {
						new RechargeViaPGTask().execute(editText_Mobile
								.getText().toString().trim(), operatorID,
								String.valueOf(rechargeType), editText_Amount
										.getText().toString().trim(), isStv);
					}
				}

			});

			Button button_Cancel = (Button) dialog
					.findViewById(R.id.button_Cancel);
			button_Cancel.setTypeface(Reguler);
			// if button is clicked, close the custom dialog
			button_Cancel.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
				}
			});

			ImageView imageView_Close = (ImageView) dialog
					.findViewById(R.id.imageView_Close);
			imageView_Close.setVisibility(View.GONE);
			imageView_Close.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					dialog.dismiss();
				}
			});
			dialog.show();

		} catch (Exception exception) {
		}
	}

	public class GetPlansCircletask extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... params) {

			String response = RequestClass.getInstance().readPay1B2BRequest(
					getActivity(),
					Constants.B2B_URL + "method=getMobileDetails&mobile="
							+ editText_Mobile.getText().toString().trim());
			/*
			 * String response = RequestClass.getInstance().readPay1Request(
			 * RechargeMobileActivity.this, Constants.URL, listValuePair);
			 */
			String replaced = response.replace("(", "").replace(")", "")
					.replace(";", "");

			return replaced;

		}

		@Override
		protected void onPostExecute(String result) {
			// if (this.dialog.isShowing()) {
			// this.dialog.dismiss();
			// }
			super.onPostExecute(result);
			try {
				JSONArray array = new JSONArray(result);
				JSONObject jsonObject = array.getJSONObject(0);
				String status = jsonObject.getString("status");
				if (status.equalsIgnoreCase("SUCCESS")) {

					JSONObject jsonObjectdetails = jsonObject
							.getJSONObject("details");
					circle_code = jsonObjectdetails.getString("area")
							.toString().trim();
					operatorCode = jsonObjectdetails.getString("operator")
							.toString().trim();

					try {
						operatorDataSource.open();

						Operator operator = operatorDataSource.getOperator(
								Constants.RECHARGE_DATA, operatorCode);

						if (operator != null) {
							operatorName = operator.getOperatorName();
							operatorProductID = String.valueOf(operator
									.getOperatorProductID());
							operatorID = String.valueOf(operator
									.getOperatorID());
						}

					} catch (SQLException exception) {
						// TODO: handle exception
						// // // Log.e("Er ", exception.getMessage());
					} catch (Exception e) {
						// TODO: handle exception
						// // // Log.e("Er ", e.getMessage());
					} finally {
						operatorDataSource.close();
					}
					textView_Operator.setText(operatorName);
					btnPlans.setVisibility(View.VISIBLE);

					// if (circle_code != null && operatorCode != null) {
					// btnPlans.setVisibility(View.VISIBLE);
					// }
				}
			} catch (JSONException e) {
				// e.printStackTrace();
				// Constants.showCustomToast(RechargeMobileActivity.this,
				// result);
			} catch (Exception e) {
				// TODO: handle exception
				// Constants.showCustomToast(RechargeMobileActivity.this,
				// result);
			}

		}

		@Override
		protected void onPreExecute() {
			// dialog = new MyProgressDialog(PlansActivity.this);
			// // this.dialog.setMessage("Please wait.....");
			// this.dialog.show();
			super.onPreExecute();

		}

	}

	public class RechargeViaWalletTask extends
			AsyncTask<String, String, String> implements OnDismissListener {
		private MyProgressDialog dialog;
		String url;

		@Override
		protected String doInBackground(String... params) {
			ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

			listValuePair
					.add(new BasicNameValuePair("mobile_number", params[0]));
			listValuePair.add(new BasicNameValuePair("operator", params[1]));
			listValuePair.add(new BasicNameValuePair("flag", params[2]));
			listValuePair.add(new BasicNameValuePair("paymentopt", "wallet"));
			listValuePair.add(new BasicNameValuePair("payment", "1"));
			listValuePair.add(new BasicNameValuePair("partial", "1"));
			listValuePair.add(new BasicNameValuePair("name", ""));
			listValuePair.add(new BasicNameValuePair("recharge_flag", "0"));
			listValuePair.add(new BasicNameValuePair("recharge", "1"));
			listValuePair.add(new BasicNameValuePair("amount", params[3]));
			listValuePair.add(new BasicNameValuePair("stv", params[4]));
			listValuePair.add(new BasicNameValuePair("payment", "1"));
			listValuePair.add(new BasicNameValuePair("name", ""));
			listValuePair.add(new BasicNameValuePair("api_version",Constants.API_VERSION));
			String method = "recharge";

			String response = RequestClass.getInstance().readPay1B2CRequest(
					getActivity(), Constants.B2C_URL + method, listValuePair);
			listValuePair.clear();
			return response;
		}

		@Override
		protected void onCancelled() {
			// TODO Auto-generated method stub
			super.onCancelled();
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			try {

				if (!result.startsWith("Error")) {

					JSONObject jsonObject = new JSONObject(result);

					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {
						JSONObject description = jsonObject
								.getJSONObject("description");
						if (description.getString("online_form")
								.equalsIgnoreCase("true")) {
	Intent intent = new Intent(getActivity(),
			PGActivity.class);
	intent.putExtra("OPERATOR_NAME", operatorProductID);
	intent.putExtra("online_form", true);
	intent.putExtra("TAX", "");
	intent.putExtra("MOBILE_NUMBER", editText_Mobile
			.getText().toString().trim());
	intent.putExtra("RECHARGE_AMOUNT", editText_Amount
			.getText().toString().trim());
	intent.putExtra("RECHARGE_FOR", rechargeType);
	intent.putExtra("TRANS_ID",
			description.getString("txnid"));
	intent.putExtra("CONTENT",
			description.getString("form_content")
					.replaceAll("\\\\", ""));

	startActivity(intent);
}else{
	
						
						
						
						
						JSONObject jsonObjectGift = jsonObject
								.getJSONObject("dealDetails");
						try {

							freeBiesDataSource.open();
							freeBiesLocationDataSource.open();

							JSONArray jsonArrayAlldeals = jsonObjectGift
									.getJSONArray("Alldeals");
							long timestamp = System.currentTimeMillis();

							freeBiesDataSource.createFreeBies(
									jsonArrayAlldeals, timestamp);
							// for (int i = 0; i < jsonArrayAlldeals.length();
							// i++) {
							//
							// String deal_id = jsonArrayAlldeals
							// .getJSONObject(i).getString("id");
							// String offer_id = jsonArrayAlldeals
							// .getJSONObject(i).getString("of_id");
							// try {
							// freeBiesDataSource.deleteFreeBies(Integer
							// .parseInt(deal_id));
							// freeBiesLocationDataSource
							// .deleteFreeBiesLocation(Integer
							// .parseInt(offer_id));
							// } catch (Exception e) {
							// continue;
							// }
							//
							// String deal_name = jsonArrayAlldeals
							// .getJSONObject(i).getString("name");
							// String url = jsonArrayAlldeals.getJSONObject(i)
							// .getString("i_url");
							// String catagory = jsonArrayAlldeals
							// .getJSONObject(i).getString("cat");
							// String catagory_id = jsonArrayAlldeals
							// .getJSONObject(i).getString("cat_id");
							// String min_amt = jsonArrayAlldeals
							// .getJSONObject(i).getString("min");
							// String offer_name = jsonArrayAlldeals
							// .getJSONObject(i).getString("of_name");
							// String validity = jsonArrayAlldeals
							// .getJSONObject(i).getString("valid");
							// String desc = jsonArrayAlldeals
							// .getJSONObject(i).getString("desc");
							// String offer_lat = "0", offer_lng = "0",
							// offer_add = "", offer_area = "", offer_city = "",
							// offer_state = "";
							//
							// JSONArray jsonArrayLocation = jsonArrayAlldeals
							// .getJSONObject(i).getJSONArray("locs");
							//
							// for (int j = 0; j < jsonArrayLocation.length();
							// j++) {
							// JSONObject jsonObjectInner = jsonArrayLocation
							// .getJSONObject(j);
							//
							// String loc_lat = jsonObjectInner
							// .getString("lat");
							// String loc_lng = jsonObjectInner
							// .getString("lng");
							// String loc_add = jsonObjectInner
							// .getString("addr");
							// String loc_area = jsonObjectInner
							// .getString("area");
							// String loc_city = jsonObjectInner
							// .getString("city");
							// String loc_state = jsonObjectInner
							// .getString("state");
							//
							// if (j == 0) {
							// offer_lat = loc_lat;
							// offer_lng = loc_lng;
							// offer_add = loc_add;
							// offer_area = loc_area;
							// offer_city = loc_city;
							// offer_state = loc_state;
							// }
							//
							// freeBiesLocationDataSource
							// .createFreeBiesLocation(
							// Integer.parseInt(offer_id),
							// Double.parseDouble(loc_lat),
							// Double.parseDouble(loc_lng),
							// loc_add, loc_area,
							// loc_city, loc_state, 0, 0,
							// timestamp);
							// }
							// freeBiesDataSource.createFreeBies(
							// Integer.parseInt(deal_id), deal_name,
							// url, catagory,
							// Integer.parseInt(catagory_id),
							// Double.parseDouble(offer_lat),
							// Double.parseDouble(offer_lng),
							// offer_add, offer_area, offer_city,
							// offer_state, 0,
							// jsonArrayLocation.length(),
							// Integer.parseInt(offer_id), offer_name,
							// validity, desc,
							// Integer.parseInt(min_amt), timestamp);
							// }

							// Utility.setSharedPreferences(getActivity(),
							// Constants.LAST_GIFT_UPDATED,
							// String.valueOf(System.currentTimeMillis()));
						} catch (SQLException exception) {
							// TODO: handle exception
							// Log.e("Er ", exception.getMessage());
						} catch (Exception e) {
							// TODO: handle exception
							// Log.e("Er ", e.getMessage());
						} finally {
							freeBiesDataSource.close();
							freeBiesLocationDataSource.close();
						}

						try {
							freeBiesOrderDataSource.open();
							freeBiesOrderDataSource.deleteFreeBiesOrder();
							freeBiesOrderDataSource.createFreeBiesOrder(1,
									jsonObjectGift.getString("offer_details"));
						} catch (SQLException exception) {
							// TODO: handle exception
							// Log.e("Er ", exception.getMessage());
						} catch (Exception e) {
							// TODO: handle exception
							// Log.e("Er ", e.getMessage());
						} finally {
							freeBiesOrderDataSource.close();
						}

						Utility.setOrder(getActivity(),
								Constants.SHAREDPREFERENCE_GIFT,
								jsonObjectGift.getString("claimedGifts"));
/*
						JSONObject description = jsonObject
								.getJSONObject("description");*/
						Utility.setBalance(getActivity(),
								Constants.SHAREDPREFERENCE_BALANCE,
								description.getString("closing_balance"));
						String loyalty_points=description.getString("loyalty_points");
						Constants.showSuccessFailureDialog(getActivity(),
								"Success", rechargeType, editText_Mobile
										.getText().toString().trim(),
								operatorProductID, editText_Amount.getText()
										.toString().trim(), "", true, "",
								description.getString("transaction_id"),loyalty_points);
					}
					} else {
						Constants.showSuccessFailureDialog(getActivity(),
								"Failure", rechargeType, editText_Mobile
										.getText().toString().trim(),
								operatorProductID, editText_Amount.getText()
										.toString().trim(), "", false,
								jsonObject.getString("description").toString(),
								"","");
					}
				} else {
					Intent intent = new Intent(getActivity(),
							ConnectivityErrorActivity.class);
					startActivity(intent);
				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonDialog(getActivity(),
						"Data Card Recharge",
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonDialog(getActivity(),
						"Data Card Recharge",
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(getActivity());
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(false);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							RechargeViaWalletTask.this.cancel(true);
							dialog.cancel();
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			RechargeViaWalletTask.this.cancel(true);
			dialog.cancel();
		}

	}

	public class RechargeViaPGTask extends AsyncTask<String, String, String>
			implements OnDismissListener {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {

			ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

			listValuePair
					.add(new BasicNameValuePair("mobile_number", params[0]));
			listValuePair.add(new BasicNameValuePair("operator", params[1]));
			listValuePair.add(new BasicNameValuePair("flag", params[2]));
			listValuePair.add(new BasicNameValuePair("paymentopt", "online"));
			listValuePair.add(new BasicNameValuePair("payment", "1"));
			listValuePair.add(new BasicNameValuePair("partial", "1"));
			listValuePair.add(new BasicNameValuePair("name", ""));
			listValuePair.add(new BasicNameValuePair("recharge_flag", "0"));
			listValuePair.add(new BasicNameValuePair("recharge", "1"));
			listValuePair.add(new BasicNameValuePair("amount", params[3]));
			listValuePair.add(new BasicNameValuePair("stv", params[4]));
			listValuePair.add(new BasicNameValuePair("payment", "1"));
			listValuePair.add(new BasicNameValuePair("name", ""));

			String method = "recharge";
			listValuePair.add(new BasicNameValuePair("api_version", Constants.API_VERSION));
			String response = RequestClass.getInstance().readPay1B2CRequest(
					getActivity(), Constants.B2C_URL + method, listValuePair);
			listValuePair.clear();
			return response;

		}

		@Override
		protected void onCancelled() {
			// TODO Auto-generated method stub
			super.onCancelled();
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			try {

				if (!result.startsWith("Error")) {
					// result = result.replaceAll("\\\\", "");
					JSONObject jsonObject = new JSONObject(result);

					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {

						JSONObject jsonObjectGift = jsonObject
								.getJSONObject("dealDetails");
						try {

							freeBiesDataSource.open();
							freeBiesLocationDataSource.open();

							JSONArray jsonArrayAlldeals = jsonObjectGift
									.getJSONArray("Alldeals");
							long timestamp = System.currentTimeMillis();

							freeBiesDataSource.createFreeBies(
									jsonArrayAlldeals, timestamp);
							// for (int i = 0; i < jsonArrayAlldeals.length();
							// i++) {
							//
							// String deal_id = jsonArrayAlldeals
							// .getJSONObject(i).getString("id");
							// String offer_id = jsonArrayAlldeals
							// .getJSONObject(i).getString("of_id");
							// try {
							// freeBiesDataSource.deleteFreeBies(Integer
							// .parseInt(deal_id));
							// freeBiesLocationDataSource
							// .deleteFreeBiesLocation(Integer
							// .parseInt(offer_id));
							// } catch (Exception e) {
							// continue;
							// }
							//
							// String deal_name = jsonArrayAlldeals
							// .getJSONObject(i).getString("name");
							// String url = jsonArrayAlldeals.getJSONObject(i)
							// .getString("i_url");
							// String catagory = jsonArrayAlldeals
							// .getJSONObject(i).getString("cat");
							// String catagory_id = jsonArrayAlldeals
							// .getJSONObject(i).getString("cat_id");
							// String min_amt = jsonArrayAlldeals
							// .getJSONObject(i).getString("min");
							// String offer_name = jsonArrayAlldeals
							// .getJSONObject(i).getString("of_name");
							// String validity = jsonArrayAlldeals
							// .getJSONObject(i).getString("valid");
							// String desc = jsonArrayAlldeals
							// .getJSONObject(i).getString("desc");
							// String offer_lat = "0", offer_lng = "0",
							// offer_add = "", offer_area = "", offer_city = "",
							// offer_state = "";
							//
							// JSONArray jsonArrayLocation = jsonArrayAlldeals
							// .getJSONObject(i).getJSONArray("locs");
							//
							// for (int j = 0; j < jsonArrayLocation.length();
							// j++) {
							// JSONObject jsonObjectInner = jsonArrayLocation
							// .getJSONObject(j);
							// WeakHashMap<String, String> map1 = new
							// WeakHashMap<String, String>();
							//
							// String loc_lat = jsonObjectInner
							// .getString("lat");
							// String loc_lng = jsonObjectInner
							// .getString("lng");
							// String loc_add = jsonObjectInner
							// .getString("addr");
							// String loc_area = jsonObjectInner
							// .getString("area");
							// String loc_city = jsonObjectInner
							// .getString("city");
							// String loc_state = jsonObjectInner
							// .getString("state");
							//
							// if (j == 0) {
							// offer_lat = loc_lat;
							// offer_lng = loc_lng;
							// offer_add = loc_add;
							// offer_area = loc_area;
							// offer_city = loc_city;
							// offer_state = loc_state;
							// }
							//
							// freeBiesLocationDataSource
							// .createFreeBiesLocation(
							// Integer.parseInt(offer_id),
							// Double.parseDouble(loc_lat),
							// Double.parseDouble(loc_lng),
							// loc_add, loc_area,
							// loc_city, loc_state, 0, 0,
							// timestamp);
							// }
							// freeBiesDataSource.createFreeBies(
							// Integer.parseInt(deal_id), deal_name,
							// url, catagory,
							// Integer.parseInt(catagory_id),
							// Double.parseDouble(offer_lat),
							// Double.parseDouble(offer_lng),
							// offer_add, offer_area, offer_city,
							// offer_state, 0,
							// jsonArrayLocation.length(),
							// Integer.parseInt(offer_id), offer_name,
							// validity, desc,
							// Integer.parseInt(min_amt), timestamp);
							// }

							// Utility.setSharedPreferences(getActivity(),
							// Constants.LAST_GIFT_UPDATED,
							// String.valueOf(System.currentTimeMillis()));
						} catch (SQLException exception) {
							// TODO: handle exception
							// Log.e("Er ", exception.getMessage());
						} catch (Exception e) {
							// TODO: handle exception
							// Log.e("Er ", e.getMessage());
						} finally {
							freeBiesDataSource.close();
							freeBiesLocationDataSource.close();
						}

						try {
							freeBiesOrderDataSource.open();
							freeBiesOrderDataSource.deleteFreeBiesOrder();
							freeBiesOrderDataSource.createFreeBiesOrder(1,
									jsonObjectGift.getString("offer_details"));
						} catch (SQLException exception) {
							// TODO: handle exception
							// Log.e("Er ", exception.getMessage());
						} catch (Exception e) {
							// TODO: handle exception
							// Log.e("Er ", e.getMessage());
						} finally {
							freeBiesOrderDataSource.close();
						}

						Utility.setOrder(getActivity(),
								Constants.SHAREDPREFERENCE_GIFT,
								jsonObjectGift.getString("claimedGifts"));

						JSONObject description = jsonObject
								.getJSONObject("description");
						Intent intent = new Intent(getActivity(),
								PGActivity.class);
						intent.putExtra("online_form", false);
						intent.putExtra("OPERATOR_NAME", operatorProductID);
						intent.putExtra("TAX", "");
						intent.putExtra("MOBILE_NUMBER", editText_Mobile
								.getText().toString().trim());
						intent.putExtra("RECHARGE_AMOUNT", editText_Amount
								.getText().toString().trim());
						intent.putExtra("RECHARGE_FOR", rechargeType);
						intent.putExtra("TRANS_ID",
								description.getString("txnid"));
						intent.putExtra("CONTENT",
								description.getString("form_content")
										.replaceAll("\\\\", ""));

						startActivity(intent);

					} else {
						Constants.showSuccessFailureDialog(getActivity(),
								"Failure", rechargeType, editText_Mobile
										.getText().toString().trim(),
								operatorProductID, editText_Amount.getText()
										.toString().trim(), "", false,
								jsonObject.getString("description").toString(),
								"","");
					}
				} else {
					Intent intent = new Intent(getActivity(),
							ConnectivityErrorActivity.class);
					startActivity(intent);
				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonDialog(getActivity(),
						"Data Card Recharge",
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonDialog(getActivity(),
						"Data Card Recharge",
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(getActivity());
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(false);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							RechargeViaPGTask.this.cancel(true);
							dialog.cancel();
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			RechargeViaPGTask.this.cancel(true);
			dialog.cancel();
		}

	}
}
