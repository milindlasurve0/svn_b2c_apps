

#import "UIImage+Utility.h"
#import <QuartzCore/QuartzCore.h>
@implementation UIImage (Utility)

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wobjc-protocol-method-implementation"

- (id)initWithCoder:(NSCoder *)decoder {
  NSData *pngData = [decoder decodeObjectForKey:@"PNGRepresentation"];
  self = [[UIImage alloc] initWithData:pngData];
  return self;
}
- (void)encodeWithCoder:(NSCoder *)encoder {
  [encoder encodeObject:UIImagePNGRepresentation(self) forKey:@"PNGRepresentation"];
}

#pragma clang diagnostic pop



- (UIImage *) strechedImage {
  float wLR = (self.size.width-1)/2;
  float hTB = (self.size.height-1)/2;  
  return [self strechedImage:hTB left:wLR bottom:hTB right:wLR];
}

- (UIImage *) strechedImage:(float)top left:(float)left bottom:(float)bottom right:(float)right {
   if([self respondsToSelector:@selector(resizableImageWithCapInsets: resizingMode:)])
    return  [self resizableImageWithCapInsets:UIEdgeInsetsMake(top,left ,bottom,right) resizingMode:UIImageResizingModeStretch];
  else if([self respondsToSelector:@selector(resizableImageWithCapInsets:)])
    return  [self resizableImageWithCapInsets:UIEdgeInsetsMake(top,left ,bottom,right)];
  else if ([self respondsToSelector:@selector(stretchableImageWithLeftCapWidth: topCapHeight:)])
    return [self stretchableImageWithLeftCapWidth:left topCapHeight:top];
  
  return nil;
}


- (CGRect) imagePositionForMaxFrame:(CGRect) rect {
  float xPos=rect.origin.x, yPos=rect.origin.y;
	int maxWidth=rect.size.width;
	int maxHeight=rect.size.height;
	float imgWidth=self.size.width;
	float imgHeight=self.size.height;
	
	
	float imgCWidth=imgWidth;
	float imgCHeight=imgHeight;
	
	float ratio=0;
	if(imgCHeight>0)
		ratio=(float)imgCHeight/(float)imgCWidth;
  
	if(imgCWidth>imgCHeight)
	{
		if(imgCWidth>maxWidth)
		{
			imgCWidth=maxWidth;
			imgCHeight=maxHeight*ratio;
      
		}
		if(imgCHeight>maxHeight)
		{
			imgCHeight=maxHeight;
			imgCWidth=imgCHeight/ratio;
		}
	}
	else
	{
		if(imgCHeight>maxHeight)
		{
			imgCHeight=maxHeight;
			imgCWidth=imgCHeight/ratio;
		}
		if(imgCWidth>maxWidth)
		{
			imgCWidth=maxWidth;
			imgCHeight=imgCWidth*ratio;
		}
	}
	
	
	maxWidth=imgCWidth;
	maxHeight=imgCHeight;
	
	if(imgCWidth<=imgWidth && imgCHeight<=imgHeight)
	{
		imgWidth=imgCWidth;
		imgHeight=imgCHeight;
		
	}
	
	int oMaxWidth=rect.size.width;
	int oMaxHeight=rect.size.height;
	
	xPos+=(oMaxWidth-imgWidth)/2;
	
	if(imgHeight>oMaxHeight)
	{
		yPos+=(oMaxHeight-imgHeight)/2;
	}
	else
	{
		yPos+=(oMaxHeight-imgHeight)/2;
	}
  
  return CGRectMake(xPos,yPos,imgWidth,imgHeight);
	
}

- (NSData *) data {
	return UIImagePNGRepresentation(self);
}

- (UIImage *)fixOrientation {
	
	// No-op if the orientation is already correct
	if (self.imageOrientation == UIImageOrientationUp) return self;
	
	// We need to calculate the proper transformation to make the image upright.
	// We do it in 2 steps: Rotate if Left/Right/Down, and then flip if Mirrored.
	CGAffineTransform transform = CGAffineTransformIdentity;
	
	switch (self.imageOrientation) {
		case UIImageOrientationDown:
		case UIImageOrientationDownMirrored:
			transform = CGAffineTransformTranslate(transform, self.size.width, self.size.height);
			transform = CGAffineTransformRotate(transform, M_PI);
			break;
			
		case UIImageOrientationLeft:
		case UIImageOrientationLeftMirrored:
			transform = CGAffineTransformTranslate(transform, self.size.width, 0);
			transform = CGAffineTransformRotate(transform, M_PI_2);
			break;
			
		case UIImageOrientationRight:
		case UIImageOrientationRightMirrored:
			transform = CGAffineTransformTranslate(transform, 0, self.size.height);
			transform = CGAffineTransformRotate(transform, -M_PI_2);
			break;
		case UIImageOrientationUp:
		case UIImageOrientationUpMirrored:
			break;
	}
	
	switch (self.imageOrientation) {
		case UIImageOrientationUpMirrored:
		case UIImageOrientationDownMirrored:
			transform = CGAffineTransformTranslate(transform, self.size.width, 0);
			transform = CGAffineTransformScale(transform, -1, 1);
			break;
			
		case UIImageOrientationLeftMirrored:
		case UIImageOrientationRightMirrored:
			transform = CGAffineTransformTranslate(transform, self.size.height, 0);
			transform = CGAffineTransformScale(transform, -1, 1);
			break;
		case UIImageOrientationUp:
		case UIImageOrientationDown:
		case UIImageOrientationLeft:
		case UIImageOrientationRight:
			break;
	}
	
	// Now we draw the underlying CGImage into a new context, applying the transform
	// calculated above.
	CGContextRef ctx = CGBitmapContextCreate(NULL, self.size.width, self.size.height,
																					 CGImageGetBitsPerComponent(self.CGImage), 0,
																					 CGImageGetColorSpace(self.CGImage),
																					 CGImageGetBitmapInfo(self.CGImage));
	CGContextConcatCTM(ctx, transform);
	switch (self.imageOrientation) {
		case UIImageOrientationLeft:
		case UIImageOrientationLeftMirrored:
		case UIImageOrientationRight:
		case UIImageOrientationRightMirrored:
			// Grr...
			CGContextDrawImage(ctx, CGRectMake(0,0,self.size.height,self.size.width), self.CGImage);
			break;
			
		default:
			CGContextDrawImage(ctx, CGRectMake(0,0,self.size.width,self.size.height), self.CGImage);
			break;
	}
	
	// And now we just create a new UIImage from the drawing context
	CGImageRef cgimg = CGBitmapContextCreateImage(ctx);
	UIImage *img = [UIImage imageWithCGImage:cgimg];
	CGContextRelease(ctx);
	CGImageRelease(cgimg);
	return img;
}


+ (UIImage *) makeImageOfView:(UIView*)view imageSize:(CGSize)imageSize {
	UIGraphicsBeginImageContext(imageSize);
	[view.layer renderInContext:UIGraphicsGetCurrentContext()];
	UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	return viewImage;
}




- (UIImage *) cropImageWithRect:(CGRect) rect {
	CGRect cropRect = rect;
	CGImageRef imageRef = CGImageCreateWithImageInRect([self CGImage], cropRect);
	return [UIImage imageWithCGImage:imageRef];
}

+ (UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize
{
    [image retain];
    UIGraphicsBeginImageContext( newSize );
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}


@end
