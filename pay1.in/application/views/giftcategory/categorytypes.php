<?php $gifttype = $this->config->item($id,'GiftCategories'); 

        $current_page_url =  "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $meteTagData = getMetaTagDetails();

        foreach ($meteTagData as $row){
           if($row->page_url == $current_page_url){
              $new_page_tittle =  $row->page_tittle;
              $new_page_meta_description =  $row->page_meta_description;  
              $new_page_meta_tag =  $row->page_meta_tag;  
            }

        }         

?>
<?php $this->load->view('common/doctype_html');  ?>
<meta name="description" content="<?php if($new_page_meta_description){ echo $new_page_meta_description; }else{ echo $gifttype['description']; }?>">
<meta name="keywords" content="<?php if($new_page_meta_tag){ echo $new_page_meta_tag; }else{ echo $gifttype['title']; ?> , My Gifts , Pay1  <?php } ?>" />
<title><?php if($new_page_tittle){ echo $new_page_tittle; }else{echo $gifttype['title']; ?> | My Gifts | Pay1  <?php } ?></title>
<?php $this->load->view('common/header');  

    $domain_url = CDEV_URL;
    $imageData = getImageDetails();    
    
    foreach ($imageData as $row){
        if($row->image_flag == 3){
            
        switch ($row->gift_type) {
            case 2:
            case 7:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
                $new_img_url = $domain_url.$row->image_url;   
                break;
            default:
                $new_img_url = $domain_url.$row->image_url;   
            }   
        }
    }
    
    switch ($id) {
        case 2:
            $img_url = '/assets/images/background/Beauty.jpg';
            break;
        case 7:
            $img_url = '/assets/images/background/service.jpg';
            break;
        case 10 :
            $img_url = '/assets/images/background/Food-and-beverages-page_03.jpg';
            break;
        case 11:
            $img_url = '/assets/images/background/health.jpg';
            break;
        case 12:
            $img_url = '/assets/images/background/fashion.jpg';
            break;
        case 13 :
            $img_url = '/assets/images/background/entertain.jpg';
            break;
        case 14:
            $img_url = '/assets/images/background/learning.jpg';
            break;
        case 15:
            $img_url = '/assets/images/background/others.jpg';
            break;
        case 16 :
            $img_url = '/assets/images/background/onlinebg.jpg';
            break;
        case 17:
            $img_url = '/assets/images/background/E-Voucher.jpg';
            break;
        default:
             $img_url = '/assets/images/background/Gift-Store.jpg';
    }
    
?>
<style type="text/css">
  .autocomplete1-suggestions {border: 1px solid #999; background: #fff; cursor: default; overflow: auto; }
  .autocomplete1-suggestion { padding: 10px 5px; font-size: 1.0em; white-space: nowrap; overflow: hidden; }
  .autocomplete1-selected { background: #f0f0f0; }
  .autocomplete1-suggestions strong { font-weight: normal; color: #3399ff; }
    
  .autocomplete-suggestions {border: 1px solid #999; background: #fff; cursor: default; overflow: auto; }
  .autocomplete-suggestion { padding: 10px 5px; font-size: 1.0em; white-space: nowrap; overflow: hidden; }
  .autocomplete-selected { background: #f0f0f0; }
  .autocomplete-suggestions strong { font-weight: normal; color: #3399ff; }
</style>
    <link rel="stylesheet" href="/assets/css/style.css">
   <div class="clearfix" style="clear:both;"></div>
           <!-- =========================  Gift Types Header Image     ========================= -->
      <input type="hidden" id="gift_categorytypes_img_hdn" value="<?php if(isset($new_img_url)){  echo $new_img_url; }else{ echo $img_url;}  ?>">
      <div class="service_page1">
          <img src="/assets/images/default_img/pay1_loader.jpg" alt="" class="img-responsive" style="" id="gift_categorytypes_img">
      </div>
      
      <div class="bottom_bg" style=""></div>
      <!-- ======================== Header Image end========================== -->
      <div class="container-fluid">
         <div class="container border-both">
            <div class="row">
               <div class="col-md-12 text-center margin-top-25">
                  <div class="row">
                 <form action="" method="">
                        <div class="col-md-4 col-md-offset-4">
                           <!--<input type="text" class="form-control" placeholder="Search by typing the product or brand name...">-->
                           <input type="text" id ="search_brand" name="search_brand" class="form-control autocomplete" placeholder="Search by typing the product or brand name...">     
                        </div>
                        <div class="col-md-1">
                            <input type="submit" value="SEARCH" class="btn btn-save search-btn" style="border-radius:5px">
                         </div>
                      </form>
               </div>
            </div>
               <div class="col-md-3 margin-top-30 ">
                  <div id="service_navigation" class="margin-top-50">
                    

                  <div id="accordian" class="border-right">
                     <ul> 
                         <?php foreach($sidebarCategories->gift as $row): ?>
                            <li><a href="<?php echo 'categories-'.$row->url.'-cid-'.$row->id; ?>"><?php echo $row->name; ?></a></li>
                         <?php endforeach; ?>
                        <!--<li><a href="/giftstore/near_you">Near you</a></li>-->
                        <li><a href="javascript:window.location.href='/near-you/'+lat_lng" >Near you</a></li>
                        <li class="active">
                           <p class="mslide" style="cursor:pointer">Categories <i class="glyphicon glyphicon-chevron-down sicon"></i></p>
                           <ul>
                               <?php foreach($sidebarCategories->categories as $row): ?>
                               <li <?php if($row->id== $id){ ?> class="active" 
                            <?php } ?>><a href="<?php echo $row->url.'-cid-'.$row->id; ?>"><?php echo $row->name; ?></a></li>
                               
                               <?php if($id == $row->id){ $cat_name = $row->name; } endforeach; ?>
                           </ul>
                        </li>
                      
                        <li>
                          <p class="mslide">Locations <i class="glyphicon glyphicon-chevron-down sicon"></i></p>
                           <ul>
                              <input type="text" id ="search_location" class="form-control autocomplete1" name="search_location" style="width:150px;" onblur="search_location(this.value)" placeholder="Area Name" >
                           </ul>
                        </li>                 
                       
                     </ul>
                     
                     <div class='col-md-11 clearfix'><h3 class="gcoin">Gift Coins</h3></div>
                     <div class="row">
                        <div class="col-md-3 col-xs-2"><button class="range_btn" data-change="-10"><img src="/assets/images/icons/minus.png" alt="" onclick="gift_filter_button('minus','<?php echo $id; ?>','category')"></button></div>
                          <div class="col-md-6 col-xs-4 row"><input id="slider1" type="range" min="10" max="500" step="10" class="" onchange="showVal(this.value,'<?php echo $id; ?>','category')" /></div>
                           <div class="col-md-3 col-xs-2"> <button class="range_btn" data-change="10"><img src="/assets/images/icons/plus.png" alt="" onclick="gift_filter_button('plus','<?php echo $id; ?>','category')"></button></div>
                          <div class="clearfix"></div><br>
                    </div>

                      <img src="/assets/images/gift/iconall.png" alt=""> <input id="rangeValue1" type="text" size="5" >
                   
                  </div>
               </div>

               </div>
               <div class="col-md-9">
                  <div class="page_title"><?php echo $cat_name; ?></div>
                  <div class='padding-both'>

                      <div id="products" class="row list-group gift_types">

                        <?php $k=1; foreach($response['description'] as $row):
                                $deal_name_arr[] = array("value" => $row['deal'],"data"=> $row['deal']);
                                $deal_area_arr[] = array("value" => ($row['area'] == null)?'':$row['area'],
                                                         "data"=>   ($row['area'] == null)?'':$row['area']);
                        ?>
                       <div data-gift-points="<?php echo $row['min_amount']; ?>" class="item col-md-4 col-xs-6">
                           <div class="thumbnail">
                              <div class="col-md-10 col-xs-10">
                                  <p class="p_detail"><?php echo $row['offer_desc']; ?></p>
                              </div>
                              <div class="col-md-2 col-xs-2">
                              <p class="<?php echo $cls = (($row['mylikes']==0)?"p_icon":"p_icon red_active") ; ?>"  id="<?php echo $k."_".$row['id']; ?>" onclick="set_mylike('<?php echo $k."_".$row['id']; ?>',<?php echo $row['id'];  ?>)" value="<?php echo $row['id'];  ?>" ><i class="glyphicon glyphicon-heart-empty"></i></p>
                              </div>
<!--                                <a href="/giftstore/giftdetails/<?php // echo $id_area = $row['deal_id'].'_'.trim($row['area']); ?>" style="padding-left: 1px;">
                               <img class="group list-group-image img-responsive" src="<?php // echo $row['img_url']; ?>"  alt=""  style="width: 189px;height: 96px;"  />
                               </a>-->
                               
                                <a onclick="GoToGiftDetails('<?php echo $row['deal_id']; ?>','<?php echo (isset($row['deal_url']) && !empty($row['deal_url'])) ? $row['deal_url'] : 'gift'; ?>')" style="padding-left: 1px;">
                               <img class="group list-group-image img-responsive" src="<?php echo $row['img_url']; ?>"  alt=""  style="width: 189px;height: 96px;"  />
                               </a>
                               
                               
                               <div class="caption" style="padding-top:0px">
                                 
                                   <div class="row">
                                       <div class="col-md-4 col-xs-4">
                                        <?php if($row['logo'] == ''){ ?>
                                        <img class="logo" src="/public/assets/images/default_img/ic_logo_evoucher.png" alt="">
                                        <?php }else{ ?>
                                            <img src="<?php echo $row['logo']; ?>" class="logo"> 
                                        <?php } ?>
                                       </div>
                                       <div class="col-md-8 col-xs-8" style="text-align:right">
                                           <?php if($row['by_voucher']==1) { ?><i class="fa fa-inr"></i> <?php echo $row['offer_price']; ?> + <?php } ?><img src="/assets/images/gift/iconall.png" alt="" > <span><?php echo $row['min_amount']; ?></span>
                                       </div>
                                       <div class="col-md-12 col-xs-12 text-left margin-top-20">  
                                          <p class="group inner list-group-item-text"><?php echo $row['deal']; ?></p>
                                          <?php if(!empty($row['area'])){ ?>
                                          <p><img src="/assets/images/gift/icongift.png" alt=""> <span><?php echo $row['area']; ?></span></p>
                                          <?php } ?>
                                       </div>
                                   </div>

                              </div>
                           </div>
                       </div>

                         <?php $k++; endforeach; ?>
                      </div>
                  </div>
                
 				    <div class="margin-top-30 clearfix"></div>
               </div>


            </div>
         </div>
      </div>
      <div class="clearfix"></div>
      
      <div class="modal fade" role="dialog" id="failure">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="cls" data-dismiss="modal">&times;</button>
                  <h3>Payment Failure</h3>
               </div>
               <div class="modal-body">
                  <p>Sorry! Your previous payment attempt failed.</p>
                  <div class="pull-right">
                     <button class="modal_button" id="try-again">Try Again</button>
                     <button class="modal_button" id="cancel-button" data-dismiss="modal">Cancel</button>
                  </div>
                  <div class="clearfix"></div>
               </div>
            </div>
         </div>
      </div>
     <script type="text/javascript" src="/public/js/jquery.autocomplete.min.js"></script>
      <script type="text/javascript">
         $(document).ready(function($) {
             $('body').scrollToTop({skin: 'cycle'});
             $("#gift_categorytypes_img").attr("src",$("#gift_categorytypes_img_hdn").val());
         });
         
        var lat_lng = localStorage.getItem("latitude")+'_'+localStorage.getItem("longitude"); 
         
        var data1 = <?php echo json_encode($deal_name_arr); ?>
        
        $('.autocomplete').autocomplete({
         lookup: data1,
         onSelect: function (suggestion) {
          search_brands();
         }
        });
        
        var deal_area_data = <?php echo json_encode($deal_area_arr); ?> ;
        $('.autocomplete1').autocomplete({
        lookup: deal_area_data,
        onSelect: function (suggestion) {
          search_location(suggestion.value);
         }
        }); 
        
        
         </script>
      <script type="text/javascript" src="/assets/js/gift_custom.js"> </script>
        
<?php $this->load->view('common/footer'); ?>