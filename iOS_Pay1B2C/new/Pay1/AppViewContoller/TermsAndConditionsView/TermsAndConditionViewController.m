//
//  TermsAndConditionViewController.m
//  Pay1
//
//  Created by webninjaz on 16/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "TermsAndConditionViewController.h"
#import "UIAlertView+BlockAddition.h"
#import "ASIHTTPRequest.h"
#import "SBJsonParser.h"
#import "MailCreator.h"
@interface TermsAndConditionViewController ()

@end

@implementation TermsAndConditionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self showHUDOnView];
    [self hideNavBackButton];
    [self setNaviationButtonWithText:@"Terms & Conditions" selector:@selector(backClicked:) isLeft:TRUE isReduceSize:TRUE isMyFont:18.0];

    [self callTermsAndConditionsService];
}

-(void)callTermsAndConditionsService{
    url = [NSString stringWithFormat:@"http://cdev.pay1.in/index.php/api_new/action/api/true/actiontype/get_faq/?Param=Description&req=tnc"];
    NSLog(@"The url is%@",url);
    
    url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSURL *surl = [NSURL URLWithString:url];
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:surl];
    request.shouldAttemptPersistentConnection = NO;
    request.timeOutSeconds = 120;
    [request setDelegate:self];
    [request startAsynchronous];
}


- (void)requestFinished:(ASIHTTPRequest *)request
{
    [self hideHUDOnView];
    NSString *newString = [request.responseString substringToIndex:[request.responseString length]-23];
    newStr =  [newString substringFromIndex:90];
    
    
    NSString *htmlString = [NSString stringWithFormat:@"<font face='Open Sans' size='2'>%@", newStr];
    
 //   NSString *htmlString = [NSString stringWithFormat:@"<div style=text-align:justify; text-justify:inter-word; font-family='Open Sans';padding:0px 5px;>%@</div>",newStr];
    NSLog(@"htmlString :%@",htmlString);
    
    NSString* removedString=[htmlString stringByReplacingOccurrencesOfString:@"\\" withString:@""];

    NSLog(@"#Removed :%@",removedString);
    
    [webView loadHTMLString:removedString baseURL:nil];

}


-(BOOL) webView:(UIWebView *)inWeb shouldStartLoadWithRequest:(NSURLRequest *)inRequest navigationType:(UIWebViewNavigationType)inType {
    if ( inType == UIWebViewNavigationTypeLinkClicked ) {
        
    [[UIApplication sharedApplication] openURL:[inRequest URL]];
    return NO;
    }
    
    
    return YES;
}


- (void)requestFailed:(ASIHTTPRequest *)request
{
    [self hideHUDOnView];

    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Pay1" message:@"Connection Lost" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    
    [alert show];
}
-(void)backClicked:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
