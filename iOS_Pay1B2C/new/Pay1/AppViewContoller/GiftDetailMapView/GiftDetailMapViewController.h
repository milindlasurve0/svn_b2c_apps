//
//  GiftDetailMapViewController.h
//  Pay1
//
//  Created by Annapurna on 06/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "BaseViewController.h"
#import "EnumType.h"
@class MapCustomCell;
@class GiftDetailCommonView;
@interface GiftDetailMapViewController : BaseViewController<GMSMapViewDelegate,UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate>{
    IBOutlet GMSMapView *_mapView;
    
    IBOutlet MapCustomCell *objMapCustomCell;
    
    IBOutlet UIView *mapAddressView;
    IBOutlet UIImageView *thumbImageView;
    IBOutlet UILabel *shopNameLabel;
    
    IBOutlet UITableView *mapLocationsTV;
    
    IBOutlet UIView *mapCOntainerView;
    
    BOOL isLocationCount;
    
    GiftDetailCommonView *commonView ;
    UIView *coverView;
    
    NSString *cityName,*stateName;
}

@property (nonatomic, strong) NSMutableArray *giftDetailArray;
@property (nonatomic, strong) UIImage *thumbImage;
@property (nonatomic, strong) NSString *shopName;

@property (nonatomic, assign) GiftSelectedType objGiftType;

@end
