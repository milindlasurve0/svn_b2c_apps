package com.pay1;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.database.SQLException;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.pay1.adapterhandler.GiftGridAdapter;
import com.pay1.constants.Constants;
import com.pay1.databasehandler.FreeBies;
import com.pay1.databasehandler.FreeBiesDataSource;
import com.pay1.utilities.Utility;

public class GiftGridFragment extends Fragment {

	private GridView gridView_Gifts;
	private GiftGridAdapter adapter;
	private ArrayList<FreeBies> data;
	private FreeBiesDataSource freeBiesDataSource;

	public static GiftGridFragment newInstance() {
		GiftGridFragment fragment = new GiftGridFragment();
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// if ((savedInstanceState != null)
		// && savedInstanceState.containsKey(KEY_CONTENT)) {
		// mContent = savedInstanceState.getString(KEY_CONTENT);
		// }
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		final View view = inflater.inflate(R.layout.gift_grid_fragment,
				container, false);
		try {

			freeBiesDataSource = new FreeBiesDataSource(getActivity());

			data = new ArrayList<FreeBies>();
			gridView_Gifts = (GridView) view.findViewById(R.id.gridView_Gifts);

			// String ExternalStorageDirectoryPath = Environment
			// .getExternalStorageDirectory().getAbsolutePath();
			//
			// String targetPath = ExternalStorageDirectoryPath + "/Pay1Cache/";
			//
			// File targetDirector = new File(targetPath);
			//
			// File[] files = targetDirector.listFiles();
			// int i = 1;
			// for (File file : files) {
			// items.add(new GiftItem(i, "Gift " + i, file.getAbsolutePath(),
			// i * 10));
			// i++;
			// }

			try {
				freeBiesDataSource.open();

				String temp = Utility.getSelectedGift(getActivity(),
						Constants.SHAREDPREFERENCE_SELECTED_GIFT);

				List<FreeBies> list = freeBiesDataSource.getAllFreeBies(temp);
				int len = list.size();
				if (len != 0) {

					for (FreeBies freebie : list)
						data.add(freebie);

				} else {
					// new FreeBiesTask()
					// .execute(
					// Utility.getCurrentLatitude(
					// getActivity(),
					// Constants.SHAREDPREFERENCE_CURRENT_LATITUDE),
					// Utility.getCurrentLongitude(
					// getActivity(),
					// Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE));
					// pager.setVisibility(View.GONE);

				}
			} catch (SQLException exception) {
				// TODO: handle exception
				// // // Log.e("Er ", exception.getMessage());
			} catch (Exception e) {
				// TODO: handle exception
				// Log.e("Er ", e.getMessage());
			} finally {
				freeBiesDataSource.close();
			}
			adapter = new GiftGridAdapter(getActivity(), data, false, false);
			// items.clear();
			gridView_Gifts.setAdapter(adapter);
			gridView_Gifts
					.setOnItemClickListener(new AdapterView.OnItemClickListener() {

						@Override
						public void onItemClick(AdapterView<?> parent,
								View view, int position, long id) {
							// TODO Auto-generated method stub
							try {
								FreeBies freeBie = data.get(position);
								Intent intent = new Intent(getActivity(),
										MyFreeBiesDetailsActivity.class);
								intent.putExtra("GIFT", freeBie);
								intent.putExtra("IS_BUY", 0);
								startActivity(intent);
							} catch (Exception e) {
							}
						}
					});
		} catch (Exception e) {
		}
		return view;
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		// outState.putString(KEY_CONTENT, mContent);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.Fragment#onDestroy()
	 */
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}
}
