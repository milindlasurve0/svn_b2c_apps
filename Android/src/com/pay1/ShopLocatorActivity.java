package com.pay1;

import java.util.ArrayList;
import java.util.WeakHashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.pay1.adapterhandler.CustomInfoWindowAdapter;
import com.pay1.adapterhandler.ShopLocatorAdapter;
import com.pay1.constants.Constants;
import com.pay1.customviews.MyProgressDialog;
import com.pay1.requesthandler.RequestClass;
import com.pay1.utilities.MyLocation;
import com.pay1.utilities.MyLocation.LocationResult;
import com.pay1.utilities.Utility;

public class ShopLocatorActivity extends FragmentActivity {

	private static final String SCREEN_LABEL = "Shop Locator List Screen";
	private EasyTracker easyTracker = null;
	private final String TAG = "Shop Locator";

	private ListView listView_Dealers;
	private GoogleMap googleMap;
	private TextView textView_Title;
	private ImageView imageView_Back;
	private ArrayList<WeakHashMap<String, String>> data;
	private ShopLocatorAdapter adapter;

	public static final String SHOP_MOBILE = "mobile";
	public static final String SHOP_NAME = "shopname";
	public static final String SHOP_SALE = "sale";
	public static final String SHOP_LATITUDE = "latitude";
	public static final String SHOP_LONGITUDE = "logitude";
	public static final String SHOP_ADDRESS = "address";
	public static final String SHOP_PIN = "pin";
	public static final String SHOP_AREA = "area";
	public static final String SHOP_CITY = "city";
	public static final String SHOP_STATE = "state";
	public static final String SHOP_DISTANCE = "distance";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.shop_locator_activity);
		try {

			easyTracker = EasyTracker.getInstance(ShopLocatorActivity.this);
			easyTracker.set(Fields.SCREEN_NAME, SCREEN_LABEL);
			Typeface Reguler = Typeface.createFromAsset(getAssets(),
					"EncodeSansNormal-400-Regular.ttf");

			textView_Title = (TextView) findViewById(R.id.textView_Title);
			textView_Title.setTypeface(Reguler);

			imageView_Back = (ImageView) findViewById(R.id.imageView_Back);
			imageView_Back.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onBackPressed();
				}
			});

			data = new ArrayList<WeakHashMap<String, String>>();
			listView_Dealers = (ListView) findViewById(R.id.listView_Dealers);
			// textView_NoData = (TextView)
			// findViewById(R.id.textView_NoData);
			// listView_Dealers.setEmptyView(textView_NoData);
			adapter = new ShopLocatorAdapter(ShopLocatorActivity.this, data);
			listView_Dealers.setAdapter(adapter);
			listView_Dealers
					.setOnItemClickListener(new AdapterView.OnItemClickListener() {

						@Override
						public void onItemClick(AdapterView<?> parent,
								View view, int position, long id) {
							// TODO Auto-generated method stub
							Intent intent = new Intent(
									ShopLocatorActivity.this,
									ShopLocatorDetailActivity.class);
							WeakHashMap<String, String> map = data
									.get(position);
							intent.putExtra("MOBILE",
									map.get(ShopLocatorActivity.SHOP_MOBILE));
							intent.putExtra("NAME",
									map.get(ShopLocatorActivity.SHOP_NAME));
							intent.putExtra("SALE",
									map.get(ShopLocatorActivity.SHOP_SALE));
							intent.putExtra("LATITUDE",
									map.get(ShopLocatorActivity.SHOP_LATITUDE));
							intent.putExtra("LONGITUDE",
									map.get(ShopLocatorActivity.SHOP_LONGITUDE));
							intent.putExtra("ADDRESS",
									map.get(ShopLocatorActivity.SHOP_ADDRESS));
							intent.putExtra("PIN",
									map.get(ShopLocatorActivity.SHOP_PIN));
							intent.putExtra("AREA",
									map.get(ShopLocatorActivity.SHOP_AREA));
							intent.putExtra("CITY",
									map.get(ShopLocatorActivity.SHOP_CITY));
							intent.putExtra("STATE",
									map.get(ShopLocatorActivity.SHOP_STATE));
							intent.putExtra("DISTANCE",
									map.get(ShopLocatorActivity.SHOP_DISTANCE));
							startActivity(intent);
						}
					});

			if (googleMap == null) {
				// Try to obtain the map from the SupportMapFragment.
				googleMap = ((SupportMapFragment) getSupportFragmentManager()
						.findFragmentById(R.id.map_full)).getMap();
				googleMap
						.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {

							@Override
							public void onMarkerDragStart(Marker marker) {
								// TODO Auto-generated method stub

							}

							@Override
							public void onMarkerDragEnd(Marker marker) {
								// TODO Auto-generated method stub
								// googleMap.clear();
								// LatLng latLng = marker.getPosition();
								// MarkerOptions options = new
								// MarkerOptions();
								// options.position(latLng);
								// options.title("You are here");
								// options.draggable(false);
								// options.icon(BitmapDescriptorFactory
								// .defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
								// //
								// options.icon(BitmapDescriptorFactory.fromResource(R.drawable.close));
								// location = new Location("Test");
								// location.setLatitude(latLng.latitude);
								// location.setLongitude(latLng.longitude);
								// googleMap.addMarker(options);
							}

							@Override
							public void onMarkerDrag(Marker marker) {
								// TODO Auto-generated method stub

							}
						});
				googleMap
						.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

							@Override
							public void onMapClick(LatLng latLng) {
								// TODO Auto-generated method stub
								// googleMap.clear();
								// MarkerOptions options = new
								// MarkerOptions();
								// options.position(latLng);
								// options.title("You are here");
								// options.draggable(false);
								// options.icon(BitmapDescriptorFactory
								// .defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
								// //
								// options.icon(BitmapDescriptorFactory.fromResource(R.drawable.close));
								// location = new Location("Test");
								// location.setLatitude(latLng.latitude);
								// location.setLongitude(latLng.longitude);
								// googleMap.addMarker(options);
							}
						});
				// Check if we were successful in obtaining the map.
				if (googleMap != null) {
					try {

						Location location = new Location("Current");
						location.setLatitude(Double.parseDouble(Utility
								.getCurrentLatitude(
										ShopLocatorActivity.this,
										Constants.SHAREDPREFERENCE_CURRENT_LATITUDE)));
						location.setLongitude(Double.parseDouble(Utility
								.getCurrentLongitude(
										ShopLocatorActivity.this,
										Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE)));

						setUpMap(location);
					} catch (Exception e) {
					}

				}
			}

			try {
				LocationResult locationResult = new LocationResult() {
					@Override
					public void gotLocation(Location location) {
						// Got the location!
						try {
							// //// Log.w("Location", location + "");
							location.getLongitude();
							location.getLatitude();

							if (location != null) {
								Utility.setCurrentLatitude(
										ShopLocatorActivity.this,
										Constants.SHAREDPREFERENCE_CURRENT_LATITUDE,
										String.valueOf(location.getLatitude()));
								Utility.setCurrentLongitude(
										ShopLocatorActivity.this,
										Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE,
										String.valueOf(location.getLongitude()));

								new DealorLocatorTask()
										.execute(String.valueOf(location
												.getLatitude()), String
												.valueOf(location
														.getLongitude()));
							} else if (Utility
									.getCurrentLatitude(
											ShopLocatorActivity.this,
											Constants.SHAREDPREFERENCE_CURRENT_LATITUDE) != null
									&& Utility
											.getCurrentLongitude(
													ShopLocatorActivity.this,
													Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE) != null) {

								new DealorLocatorTask()
										.execute(
												Utility.getCurrentLatitude(
														ShopLocatorActivity.this,
														Constants.SHAREDPREFERENCE_CURRENT_LATITUDE),
												Utility.getCurrentLongitude(
														ShopLocatorActivity.this,
														Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE));

							} else if (Utility
									.getLatitude(
											ShopLocatorActivity.this,
											Constants.SHAREDPREFERENCE_CURRENT_LATITUDE) != null
									&& Utility
											.getLongitude(
													ShopLocatorActivity.this,
													Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE) != null) {
								new DealorLocatorTask()
										.execute(
												Utility.getLatitude(
														ShopLocatorActivity.this,
														Constants.SHAREDPREFERENCE_CURRENT_LATITUDE),
												Utility.getLongitude(
														ShopLocatorActivity.this,
														Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE));
							} else {
								Constants.showOneButtonDialog(
										ShopLocatorActivity.this, TAG,
										"Please try after some time.",
										Constants.DIALOG_CLOSE);
							}

						} catch (Exception exception) {
							Constants.showOneButtonDialog(
									ShopLocatorActivity.this, TAG,
									"Please try after some time.",
									Constants.DIALOG_CLOSE);
						}
					}
				};
				MyLocation myLocation = new MyLocation();
				myLocation.getLocation(this, locationResult);
			} catch (Exception exception) {
				Constants.showOneButtonDialog(ShopLocatorActivity.this, TAG,
						"Please try after some time.", Constants.DIALOG_CLOSE);
			}

		} catch (Exception exception) {
		}
		overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}

	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView().build());
		// EasyTracker.getInstance(this).activityStart(this);
		easyTracker.send(MapBuilder.createAppView().build());
	}

	@Override
	public void onStop() {
		super.onStop();
		// EasyTracker.getInstance(this).activityStop(this);
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (Constants.isExpanded) {
			Constants.collapsView(this);
		} else {
			super.onBackPressed();
			overridePendingTransition(R.anim.slide_in_left,
					R.anim.slide_out_right);
		}
	}

	private void setUpMap(Location location) {
		try {
			googleMap.clear();

			googleMap.setInfoWindowAdapter(new CustomInfoWindowAdapter(
					ShopLocatorActivity.this));

			double lat = location.getLatitude(), lng = location.getLongitude();
			LatLng latLng = new LatLng(lat, lng);
			MarkerOptions options = new MarkerOptions();
			options.position(latLng);
			options.title("You are here");
			options.draggable(false);
			// options.icon(BitmapDescriptorFactory
			// .defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
			options.icon(BitmapDescriptorFactory
					.fromResource(R.drawable.location));
			googleMap.addMarker(options);

			// CameraPosition camPos = new CameraPosition.Builder()
			// .target(new LatLng(latitude, longitude)).zoom(18)
			// .bearing(location.getBearing()).tilt(70).build();

			CameraPosition camPos = new CameraPosition.Builder().target(latLng)
					.zoom(15).build();

			CameraUpdate camUpd3 = CameraUpdateFactory
					.newCameraPosition(camPos);

			googleMap.animateCamera(camUpd3);
		} catch (Exception exception) {
		}

	}

	public class DealorLocatorTask extends AsyncTask<String, String, String>
			implements OnDismissListener {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			try {
				String response = RequestClass.getInstance()
						.readPay1B2BRequest(
								ShopLocatorActivity.this,
								Constants.B2B_URL
										+ "method=getNearByRetailer&lat="
										+ params[0] + "&lng=" + params[1]);

				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			try {

				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONArray jsonArray = new JSONArray(replaced);
					JSONArray jsonArrayIn = jsonArray.getJSONArray(0);
					int len = jsonArrayIn.length();

					data.clear();

					for (int i = 0; i < len; i++) {
						JSONObject jsonObject = jsonArrayIn.getJSONObject(i);

						String shop_mobile = jsonObject.getJSONObject("t")
								.getString("mobile").toString().trim();
						String shop_name = jsonObject.getJSONObject("t")
								.getString("shopname").toString().trim();
						String shop_sale = jsonObject.getJSONObject("t")
								.getString("sale").toString().trim();
						String latitude = jsonObject.getJSONObject("t")
								.getString("latitude").toString().trim();
						String longitude = jsonObject.getJSONObject("t")
								.getString("longitude").toString().trim();
						// String user_id = jsonObject.getJSONObject("t")
						// .getString("user_id");
						String address = jsonObject.getJSONObject("t")
								.getString("address").toString().trim();
						String pin = jsonObject.getJSONObject("t")
								.getString("pin").toString().trim();
						String area_name = jsonObject.getJSONObject("t")
								.getString("area_name").toString().trim();
						String city_name = jsonObject.getJSONObject("t")
								.getString("city_name").toString().trim();
						String state_name = jsonObject.getJSONObject("t")
								.getString("state_name").toString().trim();
						String distance = jsonObject.getJSONObject("t")
								.getString("D").toString().trim();

						StringBuffer sb = new StringBuffer();
						sb.append(address).append(",\n").append(area_name)
								.append(",\n").append(city_name).append(" - ")
								.append(pin);
						// Log.w(TAG, sb.toString());

						WeakHashMap<String, String> map = new WeakHashMap<String, String>();
						map.put(SHOP_MOBILE, shop_mobile);
						map.put(SHOP_NAME, shop_name);
						map.put(SHOP_SALE, shop_sale);
						map.put(SHOP_LATITUDE, latitude);
						map.put(SHOP_LONGITUDE, longitude);
						map.put(SHOP_ADDRESS, address);
						map.put(SHOP_PIN, pin);
						map.put(SHOP_AREA, area_name);
						map.put(SHOP_CITY, city_name);
						map.put(SHOP_STATE, state_name);
						map.put(SHOP_DISTANCE, distance);
						data.add(map);

						// googleMap.clear();
						MarkerOptions options = new MarkerOptions();
						LatLng latLng = new LatLng(
								Double.parseDouble(latitude),
								Double.parseDouble(longitude));
						options.position(latLng);
						options.title(shop_name);
						options.snippet(sb.toString());
						options.draggable(false);
						// options.icon(BitmapDescriptorFactory
						// .defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
						options.icon(BitmapDescriptorFactory
								.fromResource(R.drawable.location));
						// location = new Location(shopname);
						// location.setLatitude(latLng.latitude);
						// location.setLongitude(latLng.longitude);
						googleMap.addMarker(options);
					}

					ShopLocatorActivity.this.runOnUiThread(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							adapter.notifyDataSetChanged();
						}
					});
				} else {
					// Constants.showOneButtonDialog(ShopLocatorActivity.this,
					// TAG, Constants.ERROR_INTERNET,
					// Constants.DIALOG_CLOSE);
					Intent intent = new Intent(ShopLocatorActivity.this,
							ConnectivityErrorActivity.class);
					intent.putExtra(Constants.RECHARGE_FOR,
							Constants.WALLET_PAYMENT);
					startActivity(intent);
					finish();

				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonDialog(ShopLocatorActivity.this, TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonDialog(ShopLocatorActivity.this, TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(ShopLocatorActivity.this);
			// // this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							DealorLocatorTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			DealorLocatorTask.this.cancel(true);
			dialog.cancel();
		}

	}

}
