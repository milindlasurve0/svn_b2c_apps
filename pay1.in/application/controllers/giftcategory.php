<?php

class Giftcategory extends CI_Controller
{
//	function __construct() {
//        parent::__construct();
//        $isLoggedIn = $this->session->userdata('isLoggedIn');
//		if($isLoggedIn != 1 ){
//		redirect('/home/');
//		}
//    }
//	
    public function index()
    {   
        $data = array();
        $user_id = isset($this->session->userdata['user']['user_id']) ? $this->session->userdata['user']['user_id'] : 0;
        $data['sidebarCategories']=parent::setSideBarCategories();
        $url=$this->config->item('api_domain')."index.php/api_new/action/api/true/actiontype/GetGiftsByType_WEB/?type=gift&type_id=2";
        if(!empty($user_id)){
            $url.="&user_id=$user_id";
        }
        $data['top_offer'] = json_decode($this->curl->get($url),TRUE);
        $this->load->view('giftcategory/index',$data);
    }
    
    
    public function categorytypes($id)
    {
        $data['sidebarCategories']=parent::setSideBarCategories();
        $user_id = isset($this->session->userdata['user']['user_id']) ? $this->session->userdata['user']['user_id'] : 0;
        $url=$this->config->item('api_domain')."index.php/api_new/action/api/true/actiontype/GetGiftsByType_WEB/?type=category&next=-1&type_id={$id}";
        if(!empty($user_id)){
            $url.="&user_id=$user_id";
        }
        $data['response'] = json_decode($this->curl->get($url),TRUE);
        $data['id'] = $id;
        $this->load->view('giftcategory/categorytypes',$data);
    }
}
