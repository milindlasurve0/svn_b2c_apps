//
//  PurchaseDealService.m
//  Pay1
//
//  Created by Annapurna on 23/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "PurchaseDealService.h"
#import "PurchasedealCriteria.h"
#import "PurchaseDealResponse.h"
#import "RedeemResponse.h"
@implementation PurchaseDealService
-(void)callPurchaseDealService:(PurchaseDealCriteria *)criteria callback:(purchaseDealResponse)callback{
    NSMutableDictionary *postData = [[NSMutableDictionary alloc]init];
    [postData setObject:criteria.dealID forKey:@"deal_id"];
    [postData setObject:criteria.offerID forKey:@"offer_id"];
    [postData setObject:criteria.couponID forKey:@"coupon_id"];
    [postData setObject:criteria.paymentOption forKey:@"paymentopt"];
    [postData setObject:criteria.amount forKey:@"amount"];
    [postData setObject:criteria.quantity forKey:@"quantity"];
    [postData setObject:criteria.transCategory forKey:@"trans_category"];
    [postData setObject:criteria.freebie forKey:@"freebie"];

    
    
    
    
    if(criteria.isFromEVoucher){
    [postData setObject:criteria.partial forKey:@"partial"];
    [postData setObject:criteria.offerPrice forKey:@"of_price"];
    [postData setObject:criteria.refID forKey:@"ref_id"];
    [postData setObject:criteria.api forKey:@"api"];
    }
    
    [self fillDefaultData:postData];
    
    NSLog(@"PostData for MyLike:%@",postData);
    
    RequestData *requestData = [[RequestData alloc]init];
    requestData.baseURL = kBaseURL;
    requestData.webServiceURL = kPurchaseDealURL;
    requestData.postData = postData;
    requestData.showLoader = FALSE;
    
    requestData.requestSuccessBlock = ^(RequestData *request) {
        
    [self printResponse:request.responseData forRequest:@"Purchase Response"];
    if ([self handleNullResponse:request])
    return;
        
    callback(nil, [self parsePurchaseDealResponse:request]);
    };
    
    
    requestData.requestFailureBlock = ^(RequestData *request) {
        [self handleNetworkError:request.responseData];
        callback(request.responseData, nil);
    };
    
    [[ServiceManager sharedInstance] makeRequest:requestData];
}


-(PurchaseDealResponse *)parsePurchaseDealResponse:(RequestData *)request{
    PurchaseDealResponse *objResponse = [[PurchaseDealResponse alloc]init];
    objResponse.dealArray = [request.responseData valueForKey:@"deal"];
    objResponse.errorCode = [request.responseData objectForKey:@"errCode"];
    objResponse.status = [request.responseData objectForKey:@"status"];
    
    
    if([objResponse.status isEqualToString:@"success"]){
    objResponse.purchaseInfoDic = [request.responseData objectForKey:@"description"];
    if(objResponse.purchaseInfoDic[@"form_content"]){
    objResponse.formContent = [objResponse.purchaseInfoDic objectForKey:@"form_content"];
    objResponse.onlineForm = [objResponse.purchaseInfoDic objectForKey:@"online_form"];
    objResponse.transactionID = [objResponse.purchaseInfoDic objectForKey:@"txnid"];
    }
    
    else{
    objResponse.closingBalance = [objResponse.purchaseInfoDic objectForKey:@"closing_balance"];
    objResponse.dealCouponCode = [objResponse.purchaseInfoDic objectForKey:@"deal_coupon_code"];
    objResponse.expiry = [objResponse.purchaseInfoDic objectForKey:@"expiry"];
    objResponse.loyaltyBalance = [objResponse.purchaseInfoDic objectForKey:@"loyalty_balance"];
    objResponse.loyaltyPoints = [objResponse.purchaseInfoDic objectForKey:@"loyalty_points"];
    objResponse.transactionID = [objResponse.purchaseInfoDic objectForKey:@"transaction_id"];
        
 //   if([objResponse.purchaseInfoDic objectForKey:@"pin"] != nil)
   // objResponse.pinCode = [objResponse.purchaseInfoDic objectForKey:@"pin"];

    }
    }
    
    else{
        objResponse.messageDesc = [request.responseData objectForKey:@"description"];
    }
    
    return objResponse;
}

#pragma mark - Redeem 

-(void)callPurchaseRedeemService:(NSString *)redeemCode callback:(redeemResponse)callback{
    NSMutableDictionary *postData = [[NSMutableDictionary alloc]init];
    [postData setObject:redeemCode forKey:@"vcode"];
    //[self fillDefaultData:postData];
    
    NSLog(@"PostData for Redeem:%@",postData);
    
    RequestData *requestData = [[RequestData alloc]init];
    requestData.baseURL = kBaseURL;
    requestData.webServiceURL = kRedeemURL;
    requestData.postData = postData;
    requestData.showLoader = FALSE;
    
    requestData.requestSuccessBlock = ^(RequestData *request) {
        
    [self printResponse:request.responseData forRequest:@"Purchase Response"];
    if ([self handleNullResponse:request])
    return;
        
    callback(nil, [self parseRedeemResponse:request]);
    };
    
    
    requestData.requestFailureBlock = ^(RequestData *request) {
    [self handleNetworkError:request.responseData];
    callback(request.responseData, nil);
    };
    
    [[ServiceManager sharedInstance] makeRequest:requestData];
}

-(RedeemResponse *)parseRedeemResponse:(RequestData *)request{
    RedeemResponse *objResponse = [[RedeemResponse alloc]init];
    objResponse.errorCode = [request.responseData objectForKey:@"errCode"];
    objResponse.errorCode = [request.responseData objectForKey:@"status"];
    objResponse.desc = [request.responseData objectForKey:@"description"];
    return objResponse;
}


@end
