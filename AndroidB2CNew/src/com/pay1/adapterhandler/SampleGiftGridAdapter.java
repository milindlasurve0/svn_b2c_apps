package com.pay1.adapterhandler;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.database.SQLException;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.BackgroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pay1.R;
import com.pay1.constants.Constants;
import com.pay1.databasehandler.FreeBies;
import com.pay1.databasehandler.FreeBiesDataSource;
import com.pay1.imagehandler.ImageLoader;
import com.pay1.model.GiftGridItem;
import com.pay1.requesthandler.RequestClass;
import com.pay1.utilities.RoundedImageView;

public class SampleGiftGridAdapter extends BaseAdapter {
	Context context;
	ArrayList<GiftGridItem> data;
	private int o_id = 0;
	private boolean isMyLike, isNear, isMyGift;
	Typeface Reguler;
	String isLiked = "1";
	FreeBies bies;

	FreeBiesDataSource freeBiesDataSource;
	public ImageLoader imageLoader;

	public SampleGiftGridAdapter(Context context, ArrayList<GiftGridItem> data,
			boolean isMyLike, boolean isNear, boolean isMyGift) {
		this.context = context;
		this.data = data;
		Reguler = Typeface.createFromAsset(this.context.getAssets(),
				"Roboto-Regular.ttf");
		this.isMyLike = isMyLike;
		this.isNear = isNear;
		this.isMyGift = isMyGift;
		freeBiesDataSource = new FreeBiesDataSource(this.context);
		freeBiesDataSource.open();
		imageLoader = new ImageLoader(this.context.getApplicationContext());
	}

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	private int lastPosition = -1;

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		try {
			bies = null;

			RecordHolder holder = null;
			if (row == null) {
				LayoutInflater inflater = ((Activity) context)
						.getLayoutInflater();
				row = inflater.inflate(R.layout.sample_gift_grid_item, null);

				holder = new RecordHolder();
				holder.textView_dealName1 = (TextView) row
						.findViewById(R.id.textView_dealName1);
				holder.imageView_Photo = (ImageView) row
						.findViewById(R.id.imageView_Photo);
				holder.textViewLayout = (LinearLayout) row
						.findViewById(R.id.textViewLayout);
				holder.textView_OfferStatus = (TextView) row
						.findViewById(R.id.textView_OfferStatus);

				holder.imageView_Logo = (RoundedImageView) row
						.findViewById(R.id.imageView_Logo);

				holder.imageView_Like = (ImageView) row
						.findViewById(R.id.imageView_Like);

				holder.textView_OfferName = (TextView) row
						.findViewById(R.id.textView_OfferName);

				holder.textView_OfferName.setTypeface(Reguler);

				holder.textView_Area = (TextView) row
						.findViewById(R.id.textView_Area);
				holder.textView_Area.setTypeface(Reguler);

				holder.locationLayout = (RelativeLayout) row
						.findViewById(R.id.locationLayout);

				row.setTag(holder);
			} else {
				holder = (RecordHolder) row.getTag();
			}

			final GiftGridItem freeBie = data.get(position);
			holder.textView_OfferName.setText(freeBie.getOfferName());

			holder.textView_dealName1.setText(freeBie.getDealName());
			holder.textView_dealName1.setTypeface(Reguler);

			try {

				imageLoader.DisplayImage(freeBie.getDealImageUrl(),
						holder.imageView_Photo);
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			try {

				imageLoader.DisplayImage(freeBie.getLogoImageUrl(),
						holder.imageView_Logo);
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}

			holder.textView_Area.setText(freeBie.getDealDistanceArea());
			final RecordHolder finalHolder = holder;
			bies = freeBie.getFreeBieItem();

			/*
			 * if (bies.getFreebieLike() == 0) {
			 * holder.imageView_Like.setImageResource(R.drawable.ic_unlike); }
			 * else {
			 * holder.imageView_Like.setImageResource(R.drawable.ic_like); }
			 */
			holder.imageView_Like.setImageResource(freeBie.getDealLikeUnlike());
			holder.imageView_Like
					.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {

							bies = freeBie.getFreeBieItem();

							o_id = bies.getFreebieOfferID();

							if (bies.getFreebieLike() == 0) {
								bies.setFreebieLike(1);
								freeBiesDataSource.updateFreeBies(1,
										String.valueOf(o_id));
								freeBie.setDealLikeUnlike(R.drawable.ic_like);
								finalHolder.imageView_Like
										.setImageResource(R.drawable.ic_like);
							} else {
								freeBie.setDealLikeUnlike(R.drawable.ic_unlike);
								freeBiesDataSource.updateFreeBies(0,
										String.valueOf(o_id));
								finalHolder.imageView_Like
										.setImageResource(R.drawable.ic_unlike);

								bies.setFreebieLike(0);
							}

							notifyDataSetChanged();

							new LikeTask().execute(o_id + "");

						}
					});

			if (isMyLike)
				holder.imageView_Like.setVisibility(View.GONE);
			else
				holder.imageView_Like.setVisibility(View.VISIBLE);

			if (isNear) {
				holder.textView_Area.setVisibility(View.VISIBLE);
			} else {
				holder.textView_Area.setVisibility(View.GONE);
			}

			if (isMyGift) {
				
				if(freeBie.getOfferStatus().isEmpty())
					holder.textViewLayout.setBackgroundColor(Color.TRANSPARENT);
				else
					holder.textViewLayout.setBackgroundColor(context.getResources().getColor(R.color.app_semi_trans_color));
				
				holder.textView_OfferStatus.setText(freeBie.getOfferStatus());
			} else{
				holder.textViewLayout.setBackgroundColor(Color.TRANSPARENT);
				holder.textView_OfferStatus.setText("");
			}

			TextView textView_Rupee = (TextView) row
					.findViewById(R.id.textView_Rupee);

			textView_Rupee.setText(freeBie.getLoyaltyPoints());

		} catch (Exception e) {
		}

		/*
		 * Animation animation = AnimationUtils.loadAnimation(context, (position
		 * > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
		 * row.startAnimation(animation); lastPosition = position;
		 */

		return row;

	}

	class RecordHolder {
		TextView textView_OfferName, textView_Area, textView_OfferStatus,
				textView_dealName1;
		ImageView imageView_Photo, imageView_Like, imageView_Point,
				imageView_location;
		RoundedImageView imageView_Logo;
		LinearLayout textViewLayout;
		RelativeLayout locationLayout;

	}

	public class LikeTask extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... params) {
			try {

				String response = RequestClass.getInstance()
						.readPay1B2CRequest(context,
								Constants.B2C_URL + "MyLikes/?id=" + params[0]);

				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {

				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONObject jsonObject = new JSONObject(replaced);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {

						JSONObject jsonObject2 = jsonObject
								.getJSONObject("description");
						FreeBiesDataSource freeBiesDataSource = new FreeBiesDataSource(
								context);
						try {

							freeBiesDataSource.open();
							freeBiesDataSource.updateFreeBies(Integer
									.parseInt(jsonObject2.getString("status")),
									String.valueOf(o_id));

						} catch (SQLException exception) {
						} catch (Exception e) {

						} finally {
							freeBiesDataSource.close();
						}
					}
				}
			} catch (JSONException e) {
			} catch (Exception e) {
			}
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.BaseAdapter#notifyDataSetChanged()
	 */
	/*
	 * @Override public void notifyDataSetChanged() { // TODO Auto-generated
	 * method stub super.notifyDataSetChanged(); }
	 */
}
