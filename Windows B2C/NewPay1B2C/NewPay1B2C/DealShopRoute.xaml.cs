﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Windows.Input;
using Microsoft.Phone.Tasks;
using System.Device.Location;

using Microsoft.Phone.Controls.Maps.Platform;
using Microsoft.Phone.Controls.Maps;
using System.Windows.Media;
using System.Collections.ObjectModel;
using Microsoft.Phone.Controls.Maps;
using Microsoft.Phone.Controls.Maps.Platform;
using System.Windows.Media;
using System.Windows;
using System.Windows.Media;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Controls.Maps;
using Microsoft.Phone.Controls.Maps.Platform;
using System.Device.Location;
using System.Windows.Shapes;
using NewPay1B2C.geocodeservice;
using System.Windows.Media.Imaging;
using System.Windows.Media.Imaging;

namespace NewPay1B2C
    {
    public partial class DealShopRoute : PhoneApplicationPage
        {
        Microsoft.Phone.Controls.Maps.Platform.Location location = new Microsoft.Phone.Controls.Maps.Platform.Location ();
        string shopName, shopNumber, shopLattitude, shopLongitude, currentLattitude, currentLongitude;
        double doubleShopLattitude, doubleShopLongitude, doubleCurrentLattitude, doubleCurrentLongitude;
        string area_name, shopAddress;
        ProgressBarManager ProgressBarManager;
        public DealShopRoute ()
            {
            InitializeComponent ();

            GoogleAnalytics.EasyTracker.GetTracker ().SendView ("DealShopRoute");
            ProgressIndicator prog = new ProgressIndicator ();

            ProgressBarManager = new ProgressBarManager ();
            SystemTray.SetProgressIndicator (this, prog);
            ProgressBarManager.Hook (prog);

            //ProgressBarManager.Show(8, "Please wait...", false, 10);
            string data = PhoneApplicationService.Current.State[DealLocator.DEAL_LOCATION_DATA].ToString ();
            geocodeResults = new geocodeservice.GeocodeResult[2];

            var dict = (JObject)JsonConvert.DeserializeObject (data);

            double dis = Convert.ToDouble (dict["distance"].ToString ());
            if (dis < 1)
                {
                dis = dis * 1000;
                //textShopDistance.Text = dis.ToString("#.##") + " M";
                }
            else
                {
                // textShopDistance.Text = dis.ToString("#.##") + " KM";
                }

            string address = dict["address"].ToString ();
            string city = dict["city"].ToString ();
            string state = dict["state"].ToString ();

            currentLattitude = Constant.LoadPersistent<string> (Constant.SETTINGS_USER_LATTITUDE);
            currentLongitude = Constant.LoadPersistent<string> (Constant.SETTINGS_USER_LONGITUDE);
            shopLattitude = dict["lat"].ToString ();
            shopLongitude = dict["lng"].ToString ();



            doubleShopLattitude = Convert.ToDouble (shopLattitude);
            doubleShopLongitude = Convert.ToDouble (shopLongitude);
            doubleCurrentLattitude = Convert.ToDouble (currentLattitude);
            doubleCurrentLongitude = Convert.ToDouble (currentLongitude);


            WebClient client = new WebClient ();

            client.DownloadStringCompleted += client_DownloadStringCompleted;
            string Url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=" + shopLattitude + "," + shopLongitude + "&sensor=true";
            client.DownloadStringAsync (new Uri (Url, UriKind.RelativeOrAbsolute));


            //shopAddress = address + ", " + city + ", " + state + ", India";
            //loadAllShopsOnMap(shopLattitude, shopLongitude, address);
            /*   Geocode(ExtendedSplashScreen.Address, 0);

               Geocode(shopAddress, 1);*/
            }

        private void client_DownloadStringCompleted (object sender, DownloadStringCompletedEventArgs e)
            {
            var getResult = e.Result;
            JObject parseJson = JObject.Parse (getResult);
            var getJsonres = parseJson["results"][0];
            var getJson = getJsonres["address_components"][1];
            var getJson2 = getJsonres["address_components"][2];
            var getJson3 = getJsonres["address_components"][3];
            var sub_area = getJson["long_name"];
            var area = getJson["long_name"];
            var address = getJson["long_name"];
            shopAddress = sub_area.ToString () + ", " + area.ToString () + ", " + address.ToString ();
            shopAddress = getJsonres["formatted_address"].ToString ();
            Geocode (HomePage.currentAddress, 0);

            Geocode (shopAddress, 1);
            }


        private void callImage_Tapped (object sender, System.Windows.Input.GestureEventArgs e)
            {
            PhoneCallTask phoneCallTask = new PhoneCallTask ();

            phoneCallTask.PhoneNumber = shopNumber;
            phoneCallTask.DisplayName = shopName;

            phoneCallTask.Show ();
            }

        MapLayer _routeLayer;

        internal geocodeservice.GeocodeResult[] geocodeResults;
        private void locateImage_Tapped (object sender, System.Windows.Input.GestureEventArgs e)
            {
            geocodeResults = new geocodeservice.GeocodeResult[2];

            // Make the two Geocode requests using the values of the text boxes. Also pass the waypoint indexes 
            //   of these two values within the route.
            Geocode (HomePage.currentAddress, 0);
            Geocode (shopAddress, 1);
            }
        bool isTappedFirst;


        private void buttonImage_Click_Back (object sender, RoutedEventArgs e)
            {
            /*NavigationService.Navigate(new Uri("/ShopLocator.xaml?", UriKind.RelativeOrAbsolute));
            NavigationService.RemoveBackEntry();*/
            NavigationService.GoBack ();
            }

        MapLayer PinLayer1;
        private void loadAllShopsOnMap (string latiitude, string longitude, string address)
            {
            PinLayer1 = new MapLayer ();
            Pushpin MyPin = new Pushpin ();

            Border border = new Border ();
            border.BorderBrush = new SolidColorBrush (Colors.Black);
            border.Background = new SolidColorBrush (Colors.Black);
            TextBlock textblok = new TextBlock ();
            textblok.Text = address;
            border.BorderThickness = new Thickness (5);
            border.Child = textblok;
            border.Visibility = Visibility.Collapsed;
            MyPin.Content = border;


            Uri imgUri = new Uri ("Images/location.png", UriKind.RelativeOrAbsolute);
            BitmapImage imgSourceR = new BitmapImage (imgUri);
            ImageBrush imgBrush = new ImageBrush () { ImageSource = imgSourceR };


            MyPin.Background = imgBrush;
            MyPin.Tap += pushPin_Tap;
            MyPin.Location = new Microsoft.Phone.Controls.Maps.Platform.Location ();
            MyPin.Location.Latitude = Convert.ToDouble (latiitude);
            MyPin.Tap += pushPin_Tap;
            MyPin.Location.Longitude = Convert.ToDouble (longitude);
            //MyPin.Content = address;
            PinLayer1.Children.Add (MyPin);

            sampleMap.Children.Add (PinLayer1);

            sampleMap.Center = MyPin.Location;
            sampleMap.ZoomLevel = 15;

            }

        void pushPin_Tap (object sender, System.Windows.Input.GestureEventArgs e)
            {
            var push = sender as Pushpin;

            Border brodr = (Border)push.Content;
            if (brodr.Visibility == Visibility.Visible)
                {
                brodr.Visibility = System.Windows.Visibility.Collapsed;
                }
            else
                {
                brodr.Visibility = System.Windows.Visibility.Visible;
                }

            //stop the event from going to the parent map control
            e.Handled = true;
            }

        private void Geocode (string strAddress, int waypointIndex)
            {
            // Create the service variable and set the callback method using the GeocodeCompleted property.
            NewPay1B2C.geocodeservice.GeocodeServiceClient geocodeService = new NewPay1B2C.geocodeservice.GeocodeServiceClient ("BasicHttpBinding_IGeocodeService");
            geocodeService.GeocodeCompleted += new EventHandler<NewPay1B2C.geocodeservice.GeocodeCompletedEventArgs> (geocodeService_GeocodeCompleted);

            // Set the credentials and the geocode query, which could be an address or location.
            NewPay1B2C.geocodeservice.GeocodeRequest geocodeRequest = new NewPay1B2C.geocodeservice.GeocodeRequest ();
            geocodeRequest.Credentials = new NewPay1B2C.geocodeservice.Credentials ();
            geocodeRequest.Credentials.ApplicationId = ((ApplicationIdCredentialsProvider)sampleMap.CredentialsProvider).ApplicationId;
            geocodeRequest.Query = strAddress;

            // Make the asynchronous Geocode request, using the ‘waypoint index’ as 
            //   the user state to track this request and allow it to be identified when the response is returned.
            geocodeService.GeocodeAsync (geocodeRequest, waypointIndex);
            }

        private void geocodeService_GeocodeCompleted (object sender, geocodeservice.GeocodeCompletedEventArgs e)
            {
            // Retrieve the user state of this response (the ‘waypoint index’) to identify which geocode request 
            //   it corresponds to.
            int waypointIndex = System.Convert.ToInt32 (e.UserState);

            // Retrieve the GeocodeResult for this response and store it in the global variable geocodeResults, using
            //   the waypoint index to position it in the array.
            try
                {
                geocodeResults[waypointIndex] = e.Result.Results[0];


                // Look at each element in the global gecodeResults array to figure out if more geocode responses still 
                //   need to be returned.

                bool doneGeocoding = true;

                foreach (geocodeservice.GeocodeResult gr in geocodeResults)
                    {
                    if (gr == null)
                        {
                        doneGeocoding = false;
                        }
                    }
                if (doneGeocoding)
                    CalculateRoute (geocodeResults);
                }
            catch (Exception ex)
                {
                }
            // If the geocodeResults array is totally filled, then calculate the route.

            }




        private void routeService_CalculateRouteCompleted (object sender, routeservice.CalculateRouteCompletedEventArgs e)
            {
            // If the route calculate was a success and contains a route, then draw the route on the map.
            if ((e.Result.ResponseSummary.StatusCode == routeservice.ResponseStatusCode.Success) & (e.Result.Result.Legs.Count != 0))
                {
                // Set properties of the route line you want to draw.
                Color routeColor = Colors.Blue;
                SolidColorBrush routeBrush = new SolidColorBrush (routeColor);
                MapPolyline routeLine = new MapPolyline ();
                routeLine.Locations = new LocationCollection ();
                routeLine.Stroke = routeBrush;
                routeLine.Opacity = 0.65;
                routeLine.StrokeThickness = 5.0;

                // Retrieve the route points that define the shape of the route.
                foreach (NewPay1B2C.routeservice.Location p in e.Result.Result.RoutePath.Points)
                    {
                    routeLine.Locations.Add (new Microsoft.Phone.Controls.Maps.Platform.Location { Latitude = p.Latitude, Longitude = p.Longitude });

                    }

                // Add a map layer in which to draw the route.
                MapLayer myRouteLayer = new MapLayer ();
                sampleMap.Children.Add (myRouteLayer);

                // Add the route line to the new layer.
                myRouteLayer.Children.Add (routeLine);

                // Figure the rectangle which encompasses the route. This is used later to set the map view.
                double centerlatitude = (routeLine.Locations[0].Latitude + routeLine.Locations[routeLine.Locations.Count - 1].Latitude) / 2;
                double centerlongitude = (routeLine.Locations[0].Longitude + routeLine.Locations[routeLine.Locations.Count - 1].Longitude) / 2;
                Microsoft.Phone.Controls.Maps.Platform.Location centerloc = new Microsoft.Phone.Controls.Maps.Platform.Location ();
                centerloc.Latitude = centerlatitude;
                centerloc.Longitude = centerlongitude;
                double north, south, east, west;

                if ((routeLine.Locations[0].Latitude > 0) && (routeLine.Locations[routeLine.Locations.Count - 1].Latitude > 0))
                    {
                    north = routeLine.Locations[0].Latitude > routeLine.Locations[routeLine.Locations.Count - 1].Latitude ? routeLine.Locations[0].Latitude : routeLine.Locations[routeLine.Locations.Count - 1].Latitude;
                    south = routeLine.Locations[0].Latitude < routeLine.Locations[routeLine.Locations.Count - 1].Latitude ? routeLine.Locations[0].Latitude : routeLine.Locations[routeLine.Locations.Count - 1].Latitude;
                    }
                else
                    {
                    north = routeLine.Locations[0].Latitude < routeLine.Locations[routeLine.Locations.Count - 1].Latitude ? routeLine.Locations[0].Latitude : routeLine.Locations[routeLine.Locations.Count - 1].Latitude;
                    south = routeLine.Locations[0].Latitude > routeLine.Locations[routeLine.Locations.Count - 1].Latitude ? routeLine.Locations[0].Latitude : routeLine.Locations[routeLine.Locations.Count - 1].Latitude;

                    }
                if ((routeLine.Locations[0].Longitude < 0) && (routeLine.Locations[routeLine.Locations.Count - 1].Longitude < 0))
                    {
                    west = routeLine.Locations[0].Longitude < routeLine.Locations[routeLine.Locations.Count - 1].Longitude ? routeLine.Locations[0].Longitude : routeLine.Locations[routeLine.Locations.Count - 1].Longitude;
                    east = routeLine.Locations[0].Longitude > routeLine.Locations[routeLine.Locations.Count - 1].Longitude ? routeLine.Locations[0].Longitude : routeLine.Locations[routeLine.Locations.Count - 1].Longitude;
                    }
                else
                    {
                    west = routeLine.Locations[0].Longitude > routeLine.Locations[routeLine.Locations.Count - 1].Longitude ? routeLine.Locations[0].Longitude : routeLine.Locations[routeLine.Locations.Count - 1].Longitude;
                    east = routeLine.Locations[0].Longitude < routeLine.Locations[routeLine.Locations.Count - 1].Longitude ? routeLine.Locations[0].Longitude : routeLine.Locations[routeLine.Locations.Count - 1].Longitude;
                    }
                // For each geocode result (which are the waypoints of the route), draw a dot on the map.
                foreach (geocodeservice.GeocodeResult gr in geocodeResults)
                    {
                    Ellipse point = new Ellipse ();
                    point.Width = 10;
                    point.Height = 10;
                    point.Fill = new SolidColorBrush (Colors.Red);
                    point.Opacity = 0.65;
                    location.Latitude = gr.Locations[0].Latitude;
                    location.Longitude = gr.Locations[0].Longitude;
                    /*  MapLayer.SetPosition(point, location);
                      MapLayer.SetPositionOrigin(point, PositionOrigin.Center);

                      // Add the drawn point to the route layer.                    
                      myRouteLayer.Children.Add(point);*/

                    loadAllShopsOnMap (location.Latitude.ToString (), location.Longitude.ToString (), gr.Address.FormattedAddress.ToString ());
                    }

                // Set the map view using the rectangle which bounds the rendered route.
                //map1.SetView(rect);
                double latitude = 0.0;
                double longtitude = 0.0;
                sampleMap.SetView (location, 15);
                sampleMap.Center = location;
                GeoCoordinate CurrentLocCoordinate = new System.Device.Location.GeoCoordinate (latitude, longtitude);
                //ProgressBarManager.Hide(8);
                }
            }


        private void CalculateRoute (geocodeservice.GeocodeResult[] results)
            {
            // Create the service variable and set the callback method using the CalculateRouteCompleted property.
            routeservice.RouteServiceClient routeService = new routeservice.RouteServiceClient ("BasicHttpBinding_IRouteService");
            routeService.CalculateRouteCompleted += new EventHandler<routeservice.CalculateRouteCompletedEventArgs> (routeService_CalculateRouteCompleted);

            // Set the token.
            routeservice.RouteRequest routeRequest = new routeservice.RouteRequest ();
            routeRequest.Credentials = new NewPay1B2C.routeservice.Credentials ();
            routeRequest.Credentials.ApplicationId = "AgeNp7j94vCR-iEa33Xufv-3pxfDlRoV-zTnci3AKFZuc2MrT7i-hAU81dbH7DtC";

            // Return the route points so the route can be drawn.
            routeRequest.Options = new routeservice.RouteOptions ();
            routeRequest.Options.RoutePathType = routeservice.RoutePathType.Points;

            // Set the waypoints of the route to be calculated using the Geocode Service results stored in the geocodeResults variable.
            routeRequest.Waypoints = new System.Collections.ObjectModel.ObservableCollection<routeservice.Waypoint> ();
            foreach (geocodeservice.GeocodeResult result in results)
                {
                routeRequest.Waypoints.Add (GeocodeResultToWaypoint (result));
                }

            // Make the CalculateRoute asnychronous request.
            routeService.CalculateRouteAsync (routeRequest);
            }
        private routeservice.Waypoint GeocodeResultToWaypoint (geocodeservice.GeocodeResult result)
            {
            routeservice.Waypoint waypoint = new routeservice.Waypoint ();
            waypoint.Description = result.DisplayName;
            waypoint.Location = new NewPay1B2C.routeservice.Location ();
            waypoint.Location.Latitude = result.Locations[0].Latitude;
            waypoint.Location.Longitude = result.Locations[0].Longitude;
            return waypoint;
            }



        }
    }