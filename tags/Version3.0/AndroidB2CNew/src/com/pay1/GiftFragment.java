package com.pay1;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.database.SQLException;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.pay1.constants.Constants;
import com.pay1.customviews.GiftCellLayout;
import com.pay1.customviews.MyProgressDialog;
import com.pay1.databasehandler.FreeBies;
import com.pay1.databasehandler.FreeBiesDataSource;
import com.pay1.requesthandler.RequestClass;
import com.squareup.picasso.Picasso;

@SuppressLint("NewApi")
public class GiftFragment extends Fragment {
	// private MyHorizontalLayout myHorizontalLayout;
	private LinearLayout linearLayout_Main;
	private FreeBiesDataSource freeBiesDataSource;
	private Typeface Reguler;
	private ViewPager pager;
	Typeface Medium;
	// private GiftMainPagerAdapter adapter;
	private List<FreeBies> freeBies;

	public static GiftFragment newInstance() {
		GiftFragment fragment = new GiftFragment();
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// if ((savedInstanceState != null)
		// && savedInstanceState.containsKey(KEY_CONTENT)) {
		// mContent = savedInstanceState.getString(KEY_CONTENT);
		// }
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		final View view = inflater.inflate(R.layout.gift_fragment, container,
				false);
		try {

			freeBiesDataSource = new FreeBiesDataSource(getActivity());
			Reguler = Typeface.createFromAsset(getActivity().getAssets(),
					"Roboto-Regular.ttf");
			 Medium = Typeface.createFromAsset(getActivity()
					.getAssets(), "Roboto-Regular.ttf");
			linearLayout_Main = (LinearLayout) view
					.findViewById(R.id.linearLayout_Main);

			pager = (ViewPager) view.findViewById(R.id.pager);

			freeBies = new ArrayList<FreeBies>();
			if (freeBies.size() != 0) {
				// adapter = new GiftMainPagerAdapter(getActivity()
				// .getSupportFragmentManager(), freeBies);
				// pager.setAdapter(adapter);
				MyPagerAdapter adapter = new MyPagerAdapter();
				pager.setAdapter(adapter);
				runnable(freeBies.size()-1);
			}
			new GiftTask().execute();

		} catch (Exception e) {
		}
		return view;
	}

	private class MyPagerAdapter extends PagerAdapter {

		@Override
		public int getCount() {
			return freeBies.size();
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view == object;
		}

		@Override
		public Object instantiateItem(ViewGroup container, final int position) {

			LayoutInflater inflater = (LayoutInflater) getActivity()
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View view = inflater.inflate(R.layout.gift_top_cell_adapter,
					container, false);

			try {
				Reguler = Typeface.createFromAsset(getActivity().getAssets(),
						"Roboto-Regular.ttf");

				TextView textView_OfferName = (TextView) view
						.findViewById(R.id.textView_OfferName);
				textView_OfferName.setText(freeBies.get(position)
						.getFreebieShortDesc());
				textView_OfferName.setTypeface(Reguler);
				
				TextView textView_MerchantName = (TextView) view
						.findViewById(R.id.textView_MerchantName);
				textView_MerchantName.setText(freeBies.get(position)
						.getFreebieDealName());
				textView_MerchantName.setTypeface(Reguler);
				

				ImageView imageView_Photo = (ImageView) view
						.findViewById(R.id.imageView_Photo);
				// Bitmap bm = null;
				// if (position < itemList.size()) {
				// bm = decodeSampledBitmapFromUri(itemList.get(position)
				// .getFreebieURL(), 60, 60);
				// }
				// imageView_Photo.setImageBitmap(getRoundedCornerBitmap(bm,
				// 10));
				try {
					Picasso.with(getActivity())
							.load(freeBies.get(position).getFreebieURL())
							.placeholder(R.drawable.deal_default).fit()
							.into(imageView_Photo);
				} catch (Exception e) {
				}

				view.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						try {
							Intent intent = new Intent(getActivity(),
									MyFreeBiesDetailsActivity.class);
							intent.putExtra("GIFT", freeBies.get(position));
							intent.putExtra("IS_BUY", 0);
							getActivity().startActivity(intent);
						} catch (Exception e) {
						}
					}
				});
			} catch (Exception e) {
			}
			container.addView(view);
			return view;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			container.removeView((RelativeLayout) object);
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		// outState.putString(KEY_CONTENT, mContent);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.Fragment#onDestroy()
	 */
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	public void runnable(final int size) {

		pager.setCurrentItem(size - 1);
		new CountDownTimer(180000, 5000) {

			public void onTick(long millisUntilFinished) {

				if (pager.getCurrentItem() == size - 1) {
					pager.setCurrentItem(0);
				} else {
					pager.setCurrentItem(pager.getCurrentItem() + 1, true);
				}
			}

			public void onFinish() {

			}
		}.start();
	}

	public class GiftTask extends AsyncTask<String, String, String> implements
			OnDismissListener {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			try {

				String response = RequestClass.getInstance()
						.readPay1B2CRequest(
								getActivity(),
								Constants.B2C_URL
										+ "GetAllofferTypes/?type=&next=0");

				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				if (this.dialog.isShowing()) {
					this.dialog.dismiss();
				}
			} catch (Exception e) {
			}
			try {
				if (!result.startsWith("Error")) {

					JSONObject jsonObject = new JSONObject(result);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {

						try {

							freeBiesDataSource.open();

							JSONObject jsonObjectDescription = jsonObject
									.getJSONObject("description");

							final JSONArray jsonArrayOffer = jsonObjectDescription
									.getJSONArray("offer_details");
							String offerIds = jsonObjectDescription
									.getString("offer_likes");
							freeBiesDataSource.updateFreeBies(1, offerIds);

							for (int i = 0; i < jsonArrayOffer.length(); i++) {
								final String catName = jsonArrayOffer
										.getJSONObject(i).getString("name");
								final String catID = jsonArrayOffer
										.getJSONObject(i).getString("id");
								if (i == 0) {
									// HorizontalScrollView hs = new
									// HorizontalScrollView(
									// getActivity());
									// hs.setHorizontalScrollBarEnabled(false);
									// GiftTopCellLayout giftCell = new
									// GiftTopCellLayout(
									// getActivity());
									// hs.addView(giftCell);
									// linearLayout_Main.addView(hs);

									String str = jsonArrayOffer
											.getJSONObject(i).getString(
													"details");
									List<FreeBies> list = freeBiesDataSource
											.getAllFreeBies(str);
									
									
									int len = list.size();
									if (len != 0) {

										for (FreeBies freebie : list) {
											// giftCell.add(freebie);
											freeBies.add(freebie);
										}

									} else {
										// new FreeBiesTask()
										// .execute(
										// Utility.getCurrentLatitude(
										// getActivity(),
										// Constants.SHAREDPREFERENCE_CURRENT_LATITUDE),
										// Utility.getCurrentLongitude(
										// getActivity(),
										// Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE));
										// pager.setVisibility(View.GONE);

									}

								} else {

									RelativeLayout relativeLayout = new RelativeLayout(
											getActivity());
									relativeLayout.setPadding(15, 0, 15, 0);
									relativeLayout.setGravity(Gravity.BOTTOM);
									{
										RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
												LayoutParams.WRAP_CONTENT,
												LayoutParams.WRAP_CONTENT);
										// Align bottom-right, and add
										// bottom-margin
										params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);

										TextView textView_Catagory = new TextView(
												getActivity());
										params.setMargins(0, 15, 0, 0);
										textView_Catagory
												.setGravity(Gravity.BOTTOM);
										textView_Catagory.setTypeface(Reguler);
										textView_Catagory.setText(catName);
										//textView_Catagory.setTextSize(15);
										
										//textView_Catagory.setAllCaps(true);
										textView_Catagory
												.setLayoutParams(params);
										relativeLayout
												.addView(textView_Catagory);
									}
									{
										RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
												LayoutParams.WRAP_CONTENT,
												LayoutParams.WRAP_CONTENT);
										// Align bottom-right, and add
										// bottom-margin
										params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
										params.addRule(RelativeLayout.ALIGN_BOTTOM);
										TextView textView = new TextView(
												getActivity());
										params.setMargins(0, 15, 0, 0);
										textView.setGravity(Gravity.BOTTOM);
										textView.setTypeface(Reguler);
										Constants.setAlpha(textView, 0.6f);
										textView.setText("See All ");
										
										//textView.setAllCaps(true);
										textView.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_next,0);
										textView.setTextSize(12);

										textView.setLayoutParams(params);

										relativeLayout.addView(textView);
									}
									relativeLayout
											.setOnClickListener(new View.OnClickListener() {

												@Override
												public void onClick(View v) {
													// TODO Auto-generated
													// method stub
													Intent intent = new Intent(
															getActivity(),
															GiftGridActivity.class);
													intent.putExtra("NAME",
															catName);
													intent.putExtra("TYPE",
															catID);
													intent.putExtra("FROM", 0);
													/**
													 * 0 for GIFT 1 for Category
													 **/
													startActivity(intent);
												}
											});
									linearLayout_Main.addView(relativeLayout);

									HorizontalScrollView hs = new HorizontalScrollView(
											getActivity());
									GiftCellLayout giftCell = new GiftCellLayout(
											getActivity());
									hs.addView(giftCell);
									linearLayout_Main.addView(hs);

								/*	View v = new View(getActivity());
									LayoutParams layoutParams = new LayoutParams(
											LayoutParams.FILL_PARENT, 20);
									v.setLayoutParams(layoutParams);
									v.setBackgroundColor(getResources()
											.getColor(R.color.LightGrey));
									v.setPadding(0, 0, 0, 0);
									linearLayout_Main.addView(v);*/
									
									
									View v1 = new View(getActivity());
									LayoutParams layoutParams1 = new LayoutParams(
											LayoutParams.FILL_PARENT, 1);
									v1.setLayoutParams(layoutParams1);
									v1.setBackgroundColor(getResources().getColor(
											R.color.divider_color));
									v1.setPadding(0, 0, 0, 0);
									linearLayout_Main.addView(v1);
									View v2 = new View(getActivity());
									LayoutParams layoutParams = new LayoutParams(
											LayoutParams.FILL_PARENT, 20);
									v2.setLayoutParams(layoutParams);
									v2.setBackgroundColor(getResources().getColor(
											R.color.app_header_color));
									v2.setPadding(0, 0, 0, 0);
									linearLayout_Main.addView(v2);
									
									View v3 = new View(getActivity());
									
									v3.setLayoutParams(layoutParams1);
									v3.setBackgroundColor(getResources().getColor(
											R.color.divider_color));
									v3.setPadding(0, 0, 0, 0);
									
									linearLayout_Main.addView(v3);
									

									// myHorizontalLayout = (MyHorizontalLayout)
									// view
									// .findViewById(R.id.mygallery);

									String str = jsonArrayOffer
											.getJSONObject(i).getString(
													"details");
									List<FreeBies> list = freeBiesDataSource
											.getAllFreeBies(str);
									int len = list.size();
									if (len != 0) {
										for (FreeBies freebie : list)
											giftCell.add(freebie);
									} else {
										// new FreeBiesTask()
										// .execute(
										// Utility.getCurrentLatitude(
										// getActivity(),
										// Constants.SHAREDPREFERENCE_CURRENT_LATITUDE),
										// Utility.getCurrentLongitude(
										// getActivity(),
										// Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE));
										// pager.setVisibility(View.GONE);
									}
								}
							}
						} catch (SQLException exception) {
							// TODO: handle exception
							// Log.e("Er ", exception.getMessage());
						} catch (Exception e) {
							// TODO: handle exception
							// Log.e("Er ", e.getMessage());
						} finally {
							freeBiesDataSource.close();
						}
					}
				}

			} catch (JSONException e) {
				// e.printStackTrace();
			} catch (Exception e) {
				// TODO: handle exception
			}
			MyPagerAdapter adapter = new MyPagerAdapter();
			pager.setAdapter(adapter);
			runnable(freeBies.size()-1);
		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(getActivity());
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							dialog.dismiss();
							GiftTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			dialog.dismiss();
			GiftTask.this.cancel(true);
		}
	}
}
