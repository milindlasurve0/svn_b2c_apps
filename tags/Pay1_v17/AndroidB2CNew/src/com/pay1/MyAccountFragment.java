package com.pay1;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.pay1.adapterhandler.MyAccountAdapter;

public class MyAccountFragment extends Fragment {

	private ListView listView_MyAccount;
	private ArrayList<String> list;
	private MyAccountAdapter adapter;

	public static MyAccountFragment newInstance() {
		MyAccountFragment fragment = new MyAccountFragment();
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// if ((savedInstanceState != null)
		// && savedInstanceState.containsKey(KEY_CONTENT)) {
		// mContent = savedInstanceState.getString(KEY_CONTENT);
		// }
	}

	@SuppressLint("NewApi")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.my_account_fragment, container,
				false);
		try {

			listView_MyAccount = (ListView) view
					.findViewById(R.id.listView_MyAccount);
			list = new ArrayList<String>();
			list.clear();
			list.add("My Profile");
			list.add("Support");
			list.add("FAQ");
			list.add("Privacy Policy");
			list.add("Terms & Conditions");
			list.add("Rate Us");
			adapter = new MyAccountAdapter(getActivity(), list);
			listView_MyAccount.setAdapter(adapter);
			listView_MyAccount
					.setOnItemClickListener(new AdapterView.OnItemClickListener() {

						@Override
						public void onItemClick(AdapterView<?> parent,
								View view, int position, long id) {
							// TODO Auto-generated method stub
							switch (position) {
							case 0:
								startActivity(new Intent(getActivity(),
										ProfileActivity.class));
								break;
							case 1:
								startActivity(new Intent(getActivity(),
										SupportActivity.class));
								break;
							case 2:
								startActivity(new Intent(getActivity(),
										FAQActivity.class));
								break;
							case 3:
								startActivity(new Intent(getActivity(),
										PPActivity.class));
								break;
							case 4:
								startActivity(new Intent(getActivity(),
										TCActivity.class));
								break;
							case 5:
								startActivity(new Intent(
										Intent.ACTION_VIEW,
										Uri.parse("market://details?id=com.pay1")));
								break;
							default:
								break;
							}

						}
					});

		} catch (Exception e) {
		}

		return view;
	}

	@Override
	public void onStart() {
		super.onStart();
	}

	@Override
	public void onStop() {
		super.onStop();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.Fragment#onDestroy()
	 */
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();

	}

}
