package com.pay1.databasehandler;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.WeakHashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

public class PlanDataSource {

	// Database fields
	private SQLiteDatabase database;
	private Pay1SyncDataHelper dbHelper;

	public PlanDataSource(Context context) {
		dbHelper = new Pay1SyncDataHelper(context);
	}

	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
		System.out.println("Released " + SQLiteDatabase.releaseMemory());
	}

	public void createPlan(String response, long timestamp) {
		try {
			open();
			String plan = "INSERT INTO " + Pay1SyncDataHelper.TABLE_PLAN
					+ " VALUES (?,?,?,?,?,?,?,?,?,?,?);";

			SQLiteStatement insert_plan = database.compileStatement(plan);
			database.beginTransaction();

			JSONObject planJsonObject = new JSONObject(response);
			Iterator<String> operatorKey = planJsonObject.keys();

			// Date date3 = new Date();

			// String timeStamp3 = dateFormat.format(date3);
			// Log.d("Start Time", "Plan Start Time  before loop " +
			// timeStamp3);
			while (operatorKey.hasNext()) {
				String operKey = operatorKey.next().toString();
				JSONObject jsonObject2 = planJsonObject.getJSONObject(operKey);
				String opr_name = jsonObject2.getString("opr_name");
				// // Log.w("Oper_id", "oper   " + operKey + "   opr_name" +
				// opr_name);
				// String prod_code_pay1 =
				// jsonObject2.getString("prod_code_pay1");
				JSONObject circleJsonObject = jsonObject2
						.getJSONObject("circles");
				Iterator<String> circleKey = circleJsonObject.keys();
				while (circleKey.hasNext()) {
					JSONObject circleJsonObject2 = circleJsonObject
							.getJSONObject(circleKey.next().toString());
					String circleName = circleJsonObject2
							.getString("circle_name");
					String circle_id = circleJsonObject2.getString("circle_id");
					JSONObject plansJsonObject = circleJsonObject2
							.getJSONObject("plans");
					Iterator<String> planKey = plansJsonObject.keys();
					while (planKey.hasNext()) {
						String planType = planKey.next().toString();
						if (!planType.trim().equalsIgnoreCase("None")) {
							JSONArray planArray = plansJsonObject
									.getJSONArray(planType);

							for (int i = 0; i < planArray.length(); i++) {
								JSONObject jsonObject = planArray
										.getJSONObject(i);
								String plan_desc = jsonObject
										.getString("plan_desc");
								String plan_show_flag = jsonObject
										.getString("show_flag");
								// contentValues.put("plan_desc", plan_desc);
								String plan_amt = jsonObject
										.getString("plan_amt");
								// contentValues.put("plan_amt", plan_amt);
								String plan_validity = jsonObject
										.getString("plan_validity");

								// insert_plan.bindString(1, prod_code_pay1);
								insert_plan.bindString(2, operKey);
								insert_plan.bindString(3, opr_name);
								insert_plan.bindString(4, circle_id);
								insert_plan.bindString(5, circleName);
								insert_plan.bindString(6, planType);
								insert_plan.bindString(7, plan_amt);
								insert_plan.bindString(8, plan_validity);
								insert_plan.bindString(9, plan_desc);
								insert_plan.bindString(10,
										String.valueOf(timestamp));
								insert_plan.bindLong(11,
										Integer.parseInt(plan_show_flag));

								insert_plan.execute();
								insert_plan.clearBindings();
							}
						}
					}
				}
			}

		} catch (JSONException je) {
			// Log.d("Er ", "row inserted   " + je.getMessage());
			// je.printStackTrace();
		} catch (SQLException exception) {
			// Log.d("Er ", "row inserted   " + exception.getMessage());
			// TODO: handle exception
			// // // Log.e("Er ", exception.getMessage());
		} catch (Exception e) {
			// Log.d("Er ", "row inserted   " + e.getMessage());
			// TODO: handle exception
			// // // Log.e("Er ", e.getMessage());
		} finally {
			database.setTransactionSuccessful();
			database.endTransaction();
			close();
		}
	}

	public void createPlan(String OperatorID, String OperatorName,
			String CircleID, String CircleName, String PlanType, int Amount,
			String Validity, String Description, String Time, int show_flag) {
		ContentValues values = new ContentValues();
		values.put(Pay1SyncDataHelper.PLAN_OPERATOR_ID, OperatorID);
		values.put(Pay1SyncDataHelper.PLAN_OPERATOR_NAME, OperatorName);
		values.put(Pay1SyncDataHelper.PLAN_CIRCLE_ID, CircleID);
		values.put(Pay1SyncDataHelper.PLAN_CIRCLE_NAME, CircleName);
		values.put(Pay1SyncDataHelper.PLAN_TYPE, PlanType);
		values.put(Pay1SyncDataHelper.PLAN_AMOUNT, Amount);
		values.put(Pay1SyncDataHelper.PLAN_VALIDITY, Validity);
		values.put(Pay1SyncDataHelper.PLAN_DESCRIPTION, Description);
		values.put(Pay1SyncDataHelper.PLAN_UPDATE_TIME, Time);
		values.put(Pay1SyncDataHelper.PLAN_SHOW_FLAG, show_flag);

		database.insert(Pay1SyncDataHelper.TABLE_PLAN, null, values);
		// Cursor cursor = database.query(Pay1SyncDataHelper.TABLE_Plan,
		// null, Pay1SyncDataHelper.Plan_ID + " = " + insertId, null,
		// null, null, null);
		// cursor.moveToFirst();
		// Plan nePlan = cursorToPlan(cursor);
		// cursor.close();
		// return newPlan;
	}

	public void deletePlan() {
		database.delete(Pay1SyncDataHelper.TABLE_PLAN, null, null);
	}

	public void deletePlan(Plan plan) {
		long id = plan.getPlanID();
		database.delete(Pay1SyncDataHelper.TABLE_PLAN,
				Pay1SyncDataHelper.PLAN_ID + " = " + id, null);
	}

	public void deletePlan(String operator, String circle) {
		database.delete(Pay1SyncDataHelper.TABLE_PLAN,
				Pay1SyncDataHelper.PLAN_OPERATOR_ID + " = '" + operator
						+ "' AND " + Pay1SyncDataHelper.PLAN_CIRCLE_ID
						+ " = '" + circle + "'", null);
	}

	public ArrayList<String> getPlanType(String operator, String circle,
			int isDataCardPlan) {
		ArrayList<String> list = new ArrayList<String>();
		Cursor cursor = null;
		if (isDataCardPlan == 0) {// false
			cursor = database.query(Pay1SyncDataHelper.TABLE_PLAN, null,
					Pay1SyncDataHelper.PLAN_OPERATOR_ID + " = '" + operator
							+ "' AND " + Pay1SyncDataHelper.PLAN_CIRCLE_ID
							+ " = '" + circle + "'", null,
					Pay1SyncDataHelper.PLAN_TYPE, null,
					Pay1SyncDataHelper.PLAN_TYPE);
		} else {// true
			cursor = database.query(Pay1SyncDataHelper.TABLE_PLAN, null,
					Pay1SyncDataHelper.PLAN_OPERATOR_ID + " = '" + operator
							+ "' AND " + Pay1SyncDataHelper.PLAN_CIRCLE_ID
							+ " = '" + circle + "' AND ("
							+ Pay1SyncDataHelper.PLAN_TYPE + " Like '%2%' OR "
							+ Pay1SyncDataHelper.PLAN_TYPE + " Like '%3%')",
					null, Pay1SyncDataHelper.PLAN_TYPE, null,
					Pay1SyncDataHelper.PLAN_TYPE);
		}
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Plan plan = cursorToPlan(cursor);
			list.add(plan.getPlanType());
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return list;
	}

	public List<Plan> getAllPlan() {
		List<Plan> plans = new ArrayList<Plan>();

		Cursor cursor = database.query(Pay1SyncDataHelper.TABLE_PLAN, null,
				null, null, null, null, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Plan plan = cursorToPlan(cursor);
			plans.add(plan);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return plans;
	}

	public List<Plan> getAllPlan(String operator, String circle) {
		List<Plan> plans = new ArrayList<Plan>();

		Cursor cursor = database.query(Pay1SyncDataHelper.TABLE_PLAN, null,
				Pay1SyncDataHelper.PLAN_OPERATOR_ID + " = '" + operator
						+ "' AND " + Pay1SyncDataHelper.PLAN_CIRCLE_ID
						+ " = '" + circle + "'", null, null, null, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Plan plan = cursorToPlan(cursor);
			plans.add(plan);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return plans;
	}

	public JSONArray getAllPlanDetails(String operator, String circle,
			String plantype, int isDataCardPlan) {
		ArrayList<WeakHashMap<String, String>> wordList = new ArrayList<WeakHashMap<String, String>>();
		Cursor cursor = null;
		if (plantype.equalsIgnoreCase("") || plantype == null) {
			if (isDataCardPlan == 0) {// false
				cursor = database.query(Pay1SyncDataHelper.TABLE_PLAN, null,
						Pay1SyncDataHelper.PLAN_OPERATOR_ID + " = '"
								+ operator + "' AND "
								+ Pay1SyncDataHelper.PLAN_CIRCLE_ID + " = '"
								+ circle + "' AND "
								+ Pay1SyncDataHelper.PLAN_SHOW_FLAG + " = 1 ",
						null, null, null, Pay1SyncDataHelper.PLAN_AMOUNT);
			} else {// true
				cursor = database.query(Pay1SyncDataHelper.TABLE_PLAN, null,
						Pay1SyncDataHelper.PLAN_OPERATOR_ID + " = '"
								+ operator + "' AND "
								+ Pay1SyncDataHelper.PLAN_CIRCLE_ID + " = '"
								+ circle + "' AND "
								+ Pay1SyncDataHelper.PLAN_SHOW_FLAG + " = 1 "
								+ " AND (" + Pay1SyncDataHelper.PLAN_TYPE
								+ " Like '%2%' OR "
								+ Pay1SyncDataHelper.PLAN_TYPE
								+ " Like '%3%')", null, null, null,
						Pay1SyncDataHelper.PLAN_AMOUNT);

			}
		} else {
			cursor = database.query(Pay1SyncDataHelper.TABLE_PLAN, null,
					Pay1SyncDataHelper.PLAN_OPERATOR_ID + " = '" + operator
							+ "' AND " + Pay1SyncDataHelper.PLAN_CIRCLE_ID
							+ " = '" + circle + "' AND "
							+ Pay1SyncDataHelper.PLAN_TYPE + " = '" + plantype
							+ "' AND " + Pay1SyncDataHelper.PLAN_SHOW_FLAG
							+ " = 1 ", null, null, null,
					Pay1SyncDataHelper.PLAN_AMOUNT);
		}

		JSONArray array = new JSONArray();
		JSONObject json1 = null;

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Plan plan = cursorToPlan(cursor);
			WeakHashMap<String, String> map = new WeakHashMap<String, String>();
			map.put("description", plan.getPlanDescription());
			map.put("validity", plan.getPlanValidity());
			map.put("amount", "" + plan.getPlanAmount());
			map.put("plantype", plan.getPlanType());
			wordList.add(map);
			json1 = new JSONObject(map);

			array.put(json1);
			cursor.moveToNext();
		}
		cursor.close();
		// return contact list
		// // // Log.e("json", "json " + array);
		return array;
	}

	public List<Plan> getPlanDetails(String operator, String circle,
			int amount, int isDataCardPlan) {
		List<Plan> plans = new ArrayList<Plan>();

		Cursor cursor = null;
		if (isDataCardPlan == 0) {// false
			cursor = database.query(Pay1SyncDataHelper.TABLE_PLAN, null,
					Pay1SyncDataHelper.PLAN_OPERATOR_ID + " = '" + operator
							+ "' AND " + Pay1SyncDataHelper.PLAN_CIRCLE_ID
							+ " = '" + circle + "' AND "
							+ Pay1SyncDataHelper.PLAN_SHOW_FLAG + " = 1 AND "
							+ Pay1SyncDataHelper.PLAN_AMOUNT + " = " + amount,
					null, null, null, null);
		} else {// true
			cursor = database.query(Pay1SyncDataHelper.TABLE_PLAN, null,
					Pay1SyncDataHelper.PLAN_OPERATOR_ID + " = '" + operator
							+ "' AND " + Pay1SyncDataHelper.PLAN_CIRCLE_ID
							+ " = '" + circle + "' AND "
							+ Pay1SyncDataHelper.PLAN_SHOW_FLAG + " = 1 AND"
							+ Pay1SyncDataHelper.PLAN_AMOUNT + " = " + amount
							+ " AND (" + Pay1SyncDataHelper.PLAN_TYPE
							+ " Like '%2%' OR " + Pay1SyncDataHelper.PLAN_TYPE
							+ " Like '%3%')", null, null, null, null);
		}
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Plan plan = cursorToPlan(cursor);
			plans.add(plan);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return plans;
	}

	public Plan getTopPlan() {

		Cursor cursor = database.query(Pay1SyncDataHelper.TABLE_PLAN, null,
				Pay1SyncDataHelper.PLAN_SHOW_FLAG + " = 1 ", null, null, null,
				Pay1SyncDataHelper.PLAN_UPDATE_TIME + " desc limit 1");

		if (cursor.getCount() > 0) {
			cursor.moveToFirst();
			Plan plan = cursorToPlan(cursor);
			cursor.moveToNext();
			// make sure to close the cursor
			cursor.close();
			return plan;
		} else {
			return null;
		}
	}

	private Plan cursorToPlan(Cursor cursor) {
		Plan plan = new Plan();
		plan.setPlanID(cursor.getInt(0));
		plan.setPlanOpertorID(cursor.getString(1));
		plan.setPlanOperatorName(cursor.getString(2));
		plan.setPlanCircleID(cursor.getString(3));
		plan.setPlanCircleName(cursor.getString(4));
		plan.setPlanType(cursor.getString(5));
		plan.setPlanAmount(cursor.getInt(6));
		plan.setPlanValidity(cursor.getString(7));
		plan.setPlanDescription(cursor.getString(8));
		plan.setPlanUptateTime(cursor.getString(9));
		plan.setPlanShowFlag(cursor.getInt(10));

		return plan;
	}
}
