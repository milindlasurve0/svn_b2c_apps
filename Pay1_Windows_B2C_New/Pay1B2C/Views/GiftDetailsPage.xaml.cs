﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Pay1B2C.ViewModel;
using Pay1B2C.Model;
using System.Windows.Media.Imaging;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Text;
using System.Windows.Media;
using System.Windows.Media.Animation;
using Pay1B2C.UserControls;
using Microsoft.Phone.Tasks;
using System.Windows.Controls.Primitives;
using Pay1B2C.PopUp;
using Pay1B2C.Utilities;
using System.Windows.Documents;

namespace Pay1B2C.Views
    {
    public partial class GiftDetailsPage: PhoneApplicationPage
        {
        JArray callLocationArray;
        String locationData;
        int locationCount=0;
        GiftsTable mGiftsTable;
        Popup popup;
        String webUrl;
        String dealer_number;
        String dealer_contact;
        string img_logo_url="";
        String locationString;
        String vCode;
        string fromMyDeal="0";
        System.Threading.Timer timer;
        Task<bool> success = null; ProgressBarManager ProgressBarManager;
        public GiftDetailsPage()
            {
            InitializeComponent();

          
            GoogleAnalytics.EasyTracker.GetTracker().SendView("Gift Details Page");
            ProgressIndicator prog = new ProgressIndicator();
            ProgressBarManager = new ProgressBarManager();
            SystemTray.SetProgressIndicator(this, prog);
            ProgressBarManager.Hook(prog);
            panelDealInfo.Tap+=(a, b) =>
            {
                if(panelDealInfoDetails.Visibility==Visibility.Collapsed)
                    {
                    panelDealInfoDetails.Visibility=Visibility.Visible;


                    Duration Time_duration = new Duration(TimeSpan.FromSeconds(0.5));
                    Storyboard MyStory = new Storyboard();
                    MyStory.Duration = Time_duration;
                    DoubleAnimation My_Double= new DoubleAnimation();
                    My_Double.Duration = Time_duration;
                    MyStory.Children.Add(My_Double);
                    RotateTransform MyTransform = new RotateTransform();
                    Storyboard.SetTarget(My_Double, MyTransform);
                    Storyboard.SetTargetProperty(My_Double, new PropertyPath("Angle"));
                    My_Double.To = -90;
                    imgMore.RenderTransform = MyTransform;
                    imgMore.RenderTransformOrigin = new Point(0.5, 0.5);
                    //stackPanel1.Children.Add(image1);
                    textMoreLess.Text="LESS";
                    MyStory.Begin();

                    }
                else if(panelDealInfoDetails.Visibility==Visibility.Visible)
                    {
                    panelDealInfoDetails.Visibility=Visibility.Collapsed;
                    Duration Time_duration = new Duration(TimeSpan.FromSeconds(0.5));
                    Storyboard MyStory = new Storyboard();
                    MyStory.Duration = Time_duration;
                    DoubleAnimation My_Double= new DoubleAnimation();
                    My_Double.Duration = Time_duration;
                    MyStory.Children.Add(My_Double);
                    RotateTransform MyTransform = new RotateTransform();
                    Storyboard.SetTarget(My_Double, MyTransform);
                    Storyboard.SetTargetProperty(My_Double, new PropertyPath("Angle"));
                    My_Double.To = 90;
                    imgMore.RenderTransform = MyTransform;
                    imgMore.RenderTransformOrigin = new Point(0.5, 0.5);
                    //stackPanel1.Children.Add(image1);
                    MyStory.Begin();
                    textMoreLess.Text="MORE";

                    }
            };


            /*  locationPanel.Tap+=(a, b) =>
              {
                  if(locationCount>=1)
                      {
              NavigationService.Navigate(new Uri("/Views/GiftLocationMap.xaml?shopName="+textTitle.Text, UriKind.RelativeOrAbsolute));
                  }else{
                  NavigationService.Navigate(new Uri("/Views/ShopRoute.xaml?address="+textFullAdderess.Text+"&shopName="+textTitle.Text, UriKind.RelativeOrAbsolute));
                  }
              };*/
            }

        private async void getDealDetails(String offerId)
            {
            var datetime = await GetValueAsync(offerId);
            }

        private async Task<String> GetValueAsync(String offerId)
            {
            ProgressBarManager.Show(22, "Please wait...", false, 10);

            var values = new List<KeyValuePair<string, string>>();
            values.Add(new KeyValuePair<string, string>("actiontype", "get_deal_details"));
            values.Add(new KeyValuePair<string, string>("id", offerId));

            values.Add(new KeyValuePair<string, string>("latitude", Constant.LoadPersistent<string>(Constant.SETTINGS_USER_LATTITUDE)));
            values.Add(new KeyValuePair<string, string>("longitude", Constant.LoadPersistent<string>(Constant.SETTINGS_USER_LONGITUDE)));
            values.Add(new KeyValuePair<string, string>("api_version", Constant.API_VERSION));
            values.Add(new KeyValuePair<string, string>("api", "true"));
            FormUrlEncodedContent values1 = new FormUrlEncodedContent(values);
            Task<string> response = Constant.postParamsAndgetResponse(Constant.B2C_URL, values1);

            try
                {
                string result = await response;
                if(string.IsNullOrEmpty(result))
                    {
                    NavigationService.Navigate(new Uri("/Views/NoConnection.xaml?", UriKind.Relative));
                    }
                else
                    {
                    var dict = (JObject)JsonConvert.DeserializeObject(result);
                    string status = dict["status"].ToString();

                    if(status.Equals("success"))
                        {
                        var description = dict["description"];


                        if(dict["description"]["offer_detail"].Count()!=0)
                            {
                            offerPanel.Visibility=Visibility.Visible;
                            foreach(var c in dict["description"]["offer_detail"])
                                {
                                if(fromMyDeal.Equals("1"))
                                    {
                                    String dealStatus=c["status"].ToString();
                                    if(c["status"].ToString().Equals("1"))
                                        {
                                        purchaseGiftButton.Content="REDEEM NOW";
                                        textExpiredGrid.Visibility=Visibility.Visible;
                                        textExpired.Text="REDEEM NOW";
                                        }
                                    else if(c["status"].ToString().Equals("2"))
                                        {
                                        purchaseGiftButton.Content="REDEEMED";
                                        textExpiredGrid.Visibility=Visibility.Visible;
                                        textExpired.Text="REDEEMED";
                                        }
                                    else if(c["status"].ToString().Equals("3"))
                                        {
                                        purchaseGiftButton.Content="EXPIRED";
                                        textExpiredGrid.Visibility=Visibility.Visible;
                                        textExpired.Text="EXPIRED";
                                        }
                                    purchaseGiftButton.Style = (Style)Application.Current.Resources["CustomButtonStyleGray"];
                                    purchaseGiftButton.Foreground=new SolidColorBrush(Colors.White);
                                    purchaseGiftButton.Background=new SolidColorBrush(Colors.Transparent);
                                    purchaseGiftButton.BorderBrush=new SolidColorBrush(Colors.Gray);
                                    }
                                else
                                    {
                                    textExpiredGrid.Visibility=Visibility.Collapsed;
                                    }
                                string itemTitle = (string)c["id"];




                                try
                                    {

                                    loadHtml(browserInfo, (string)description["offer_info_html"]);

                                    loadHtml(browserInfoDetails, (string)description["deal_detail"][0]["content_txt"]);
                                    }
                                catch(Exception exc)
                                    {
                                    }
                                break;
                                }
                            }

                        dealer_contact=(string)description["offer_info_html"];

                      

                        if(dict["description"]["image_list"].Count()!=0)
                            {
                            photosPanel.Visibility=Visibility.Visible;
                          //  photosPanel.Children.Clear();
                            int index=0;
                            foreach(var c in dict["description"]["image_list"])
                                {index++;
                                string imgUrl = (string)c;
                                ImageHolder imgHolder=new ImageHolder();
                                imgHolder.imageStore.Source=new BitmapImage(new Uri(imgUrl, UriKind.RelativeOrAbsolute));
                                imageContainer.listBoxOffer.Items.Add(imgHolder);
                                imgHolder.Tag=index;
                                imgHolder.Tap+=(a, b) =>
                                {
                                (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri("/Views/ShopImageGallery.xaml?imageArray="+dict["description"]["image_list"].ToString()+"&index="+imgHolder.Tag.ToString(), UriKind.RelativeOrAbsolute));
                                };
                                }
                            }

                        
                        if(string.IsNullOrEmpty(dict["description"]["brand_description"].ToString())){
                        aboutPanel.Visibility=Visibility.Collapsed;
                            }else{
                            aboutPanel.Visibility=Visibility.Visible;
                            textAbout.Text=dict["description"]["brand_description"].ToString();
                            }

                        if(dict["description"]["location_detail"].Count()!=0)
                            {
                            callButton.Visibility=Visibility.Visible;
                            locationString=dict["description"]["location_detail"].ToString();
                            PhoneApplicationService.Current.State["locationData"] = dict["description"]["location_detail"];
                            locationPanel.Visibility=Visibility.Visible;
                            callLocationArray=(JArray) dict["description"]["location_detail"];
                            foreach(JObject loc in dict["description"]["location_detail"])
                                {
                               // callList.Add(loc);
                                textFullAdderess.Text=(string)loc["full_address"];
                                textShopName.Text=textTitle.Text;

                                String a11="http://maps.google.com/maps/api/staticmap?center=";
                                String a12=",&zoom=13&markers=icon:http://b2c.pay1.in/images/pay1giftshop.png|";
                                String a13="&path=color:0x0000FF80|";
                                String a15="&size=480x240";
                                String a14=",";
                                String mapUrl=a11+loc["lat"]+a14+loc["lng"]+a12+loc["lat"]+a14+loc["lng"]+a13+loc["lat"]+a14+loc["lng"]+a15;
                                locationCount=dict["description"]["location_detail"].Count();
                                if(dict["description"]["location_detail"].Count()>1)
                                    {
                                    textLocationCount.Text=dict["description"]["location_detail"].Count()-1+" MORE LOCATIONS";

                                    }
                                else
                                    {
                                    textLocationCount.Visibility=Visibility.Collapsed;
                                    }
                                imgLocation.Source= new BitmapImage(new Uri(mapUrl, UriKind.RelativeOrAbsolute));
                                PhoneApplicationService.Current.State["LocationData"] = loc.ToString();
                                locationPanel.Tap+=(a, b) =>
                                {
                                    if(locationCount>=1)
                                        {
                                        NavigationService.Navigate(new Uri("/Views/GiftLocationMap.xaml?shopName="+textTitle.Text+"&img_logo="+img_logo_url, UriKind.RelativeOrAbsolute));
                                        }
                                    else
                                        {
                                        NavigationService.Navigate(new Uri("/Views/ShopRoute.xaml?address="+textFullAdderess.Text+"&shopName="+textTitle.Text, UriKind.RelativeOrAbsolute));
                                        }
                                };
                                break;
                                }
                            }
                        else
                            {
                            try
                                {
                                var offer_info=description["offer_info"];
                                dealer_number=  offer_info["dealer_contact"].ToString();
                                if(string.IsNullOrEmpty(offer_info["dealer_contact"].ToString()))
                                    {
                                    callButton.Visibility=Visibility.Collapsed;
                                    }
                                else
                                    {
                                    callButton.Visibility=Visibility.Visible;
                                    }



                                distancePanel.Visibility=Visibility.Collapsed;
                                }
                            catch(JsonException je)
                                {
                                }
                            catch(Exception e)
                                {
                                }
                            }

                       
                        string off_avg=description["off_avg"].ToString();
                        if(string.IsNullOrEmpty(off_avg))
                            {
                            imgRating.Visibility=Visibility.Collapsed;
                            }
                        else
                            {
                            imgRating.Visibility=Visibility.Visible;
                            imgRating.Source=Constant.imageForRating(off_avg);

                            }

                        if(string.IsNullOrEmpty(description["company_url"].ToString()))
                            {
                            webPanel.Visibility=Visibility.Collapsed;
                            }
                        else
                            {
                            webPanel.Visibility=Visibility.Visible;
                            textWebName.Text=textTitle.Text;
                            webUrl=description["company_url"].ToString();

                            }


                        //string itemTitle = (string)dict["description"]["offer_detail"][0]["title"];
                        }
                    }
                }
            catch(Exception exc)
                {
                }
            ProgressBarManager.Hide(22);
            return "";
            }
        public static string ConvertToUnicode(string text)
            {
            var t = Encoding.Convert(Encoding.Unicode, Encoding.UTF8, Encoding.Unicode.GetBytes(text));
            return Encoding.Unicode.GetString(t, 0, t.Length);
            }



        private void web_loaded(object sender, RoutedEventArgs e)
            {
            // SolidColorBrush scb = new SolidColorBrush(Colors.Transparent);
            // WebBrowser webBrowser1 = sender as WebBrowser;
            //  webBrowser1.Background = scb;

            }

        string htmlString;
        private void webBrowser1_Loaded(object sender, RoutedEventArgs e)
            {
            WebBrowser webBrowser1 = sender as WebBrowser;
            webBrowser1.ApplyTemplate();
            string na = webBrowser1.Name;
            webBrowser1.NavigateToString(htmlString);
            }

        private void wb1_ScriptNotify(object sender, NotifyEventArgs e)
            {
            WebBrowser webBrowser1 = sender as WebBrowser;
            webBrowser1.Height = Convert.ToDouble(e.Value) * 0.45;
            }



        public void loadHtml(WebBrowser browser1, String html)
            {
            htmlString =html;// deal_detail["content_txt"].ToString();
            var htmlScript = "<script>function getDocHeight() { " +
			  "return document.getElementById('pageWrapper').offsetHeight;" +
			  "}" +
			  "function SendDataToPhoneApp() {" +
			  "window.external.Notify('' + getDocHeight());" +
			  "}</script>";

            string newHtml = "<html><head>" + htmlScript + "<style>@font-face {font-family: Normal;src: url('../Fonts/OpenSans-Regular.ttf#Open Sans') format('truetype')}"
																	  + "@font-face {font-family: Narrow;src: url('../Fonts/OpenSans-Regular.ttf#Open Sans') format('truetype')}"
																	  + "body {margin: 0px;padding-left:20px: padding-top:5px: padding-right:20px: padding-bottom:5px;text-align:justify;opacity:0.5;background: linear-gradient(to bottom, #333333, #333333);}"
																	  + "h1{font-size:28px;font-family:'Normal';margin:0;padding:10px 0 5px 0;color:#333333;}"
																	  + "h2{font-size:18px;font-family:'Narrow';margin:0;padding:10px 0 5px 0;color:#333333;}"
																	  + "h3{font-size:17px;font-family:'Narrow';margin:0;padding:10px 0 5px 0;}"
																	  + "ol{margin:0 0 10px 0;padding-left:20px;}"
																	  + "li{font-size:17px;margin:0 0 2px 0;font-family:'Normal';}</style></head>" +
							"<body style=\"margin:0px;padding:0px;background-color:transparent;\" " +
							"onLoad=\"SendDataToPhoneApp()\">" +
							"<div id=\"pageWrapper\" style=\"width:100%; background-color: transparent;" +
							"\">" + htmlString + "</div></body></html>";


            browser1.ApplyTemplate();
            browser1.NavigateToString(newHtml);

            browser1.ScriptNotify +=
	new EventHandler<NotifyEventArgs>(wb1_ScriptNotify);


            }

        protected override void OnNavigatedTo(NavigationEventArgs e)
            {
            if(e.NavigationMode== NavigationMode.New){
            if(NavigationContext.QueryString.ContainsKey("offerId"))
                {
                string offerId = NavigationContext.QueryString["offerId"];
                if(string.IsNullOrEmpty(offerId))
                    {
                    }
                else
                    {
                    //offerId="765";
                    GiftDataHelper giftDataHelper=new GiftDataHelper();
                    GiftsTable gifts=   giftDataHelper.ReadGiftByOfferId(Convert.ToInt32(offerId));
                    List<GiftsTable> gif=giftDataHelper.ReadGiftByOfferIds(offerId);
                    if(gifts!=null)
                        {
                        getDealDetails(gifts.freebieDealID.ToString());
                        textTitle.Text=gifts.freebieDealName;
                        textName.Text=gifts.freebieOfferName;
                        textDisance.Text=gifts.freebieArea;

                        img_logo_url=gifts.freebieLogoURL;
                        if(gifts.freebieLike==0)
                            {
                            imgLike.Source=new BitmapImage(new Uri("/Images/ic_unlike.png", UriKind.RelativeOrAbsolute));
                            }
                        else
                            {
                            imgLike.Source=new BitmapImage(new Uri("/Images/ic_like.png", UriKind.RelativeOrAbsolute));
                            }
                        //  giftLayout.likeRadioButton.IsChecked=gift.freebieLike!=0;

                        imgLike.MouseEnter+=(a, b) =>
                        {
                            if(gifts.freebieLike==0)
                                {
                                imgLike.Source=new BitmapImage(new Uri("/Images/ic_like.png", UriKind.RelativeOrAbsolute));
                                updateLikeDealById(gifts.freebieOfferID, giftDataHelper, 1);
                                }
                            else
                                {
                                imgLike.Source=new BitmapImage(new Uri("/Images/ic_unlike.png", UriKind.RelativeOrAbsolute));
                                updateLikeDealById(gifts.freebieOfferID, giftDataHelper, 0);
                                }


                        };


                        textCoinCount.Text=gifts.freebieMinAmount.ToString();
                        textRupee.Text=gifts.offer_price.ToString();
                        mGiftsTable=gifts;

                        LocationTable locationTable=   giftDataHelper.getNearestLocationFromLocationTable(Constant.LoadPersistent<double>(Constant.SETTINGS_USER_LATTITUDE_DOUBLE), Constant.LoadPersistent<double>(Constant.SETTINGS_USER_LONGITUDE_DOUBLE), gifts.freebieOfferID);
                        if(locationTable!=null)
                            {

                            double dis=Constant.distance(Constant.LoadPersistent<double>(Constant.SETTINGS_USER_LATTITUDE_DOUBLE), Constant.LoadPersistent<double>(Constant.SETTINGS_USER_LONGITUDE_DOUBLE), locationTable.locationLat, locationTable.locationLng);
                            String distanceString="";
                            if(dis<1)
                                {
                                distanceString=" : <1km";
                                }
                            else
                                {
                                distanceString=" : "+dis.ToString("0.00")+"km";
                                }
                            textDisance.Text=locationTable.locationArea+distanceString;
                            }
                        else
                            {
                            double dis=Constant.distance(Constant.LoadPersistent<double>(Constant.SETTINGS_USER_LATTITUDE_DOUBLE), Constant.LoadPersistent<double>(Constant.SETTINGS_USER_LONGITUDE_DOUBLE), gifts.freebieLng, gifts.freebieLat);
                            String distanceString="";
                            if(dis<1)
                                {
                                distanceString=" : <1km";
                                }
                            else
                                {
                                distanceString=" : "+dis+"km";
                                }
                            textDisance.Text=gifts.freebieArea+distanceString;
                            }


                        if(gifts.by_voucher.Equals(0))
                            {
                            distancePanel.Visibility=Visibility.Visible;
                            rupeePanel.Visibility=Visibility.Collapsed;
                            }
                        else
                            {
                            distancePanel.Visibility=Visibility.Collapsed;
                            rupeePanel.Visibility=Visibility.Visible;
                            }
                        dealImage.Source= new BitmapImage(new Uri(gifts.freebieURL, UriKind.RelativeOrAbsolute));
                        imgLogoUrl.Source= new BitmapImage(new Uri(gifts.freebieLogoURL, UriKind.RelativeOrAbsolute));



                        if(NavigationContext.QueryString.ContainsKey("fromMyDeal"))
                            {


                            /*  if(gifts.by_voucher.Equals(0))
                                  {
                              */

                            fromMyDeal=NavigationContext.QueryString["fromMyDeal"];
                            if(fromMyDeal.Equals("0"))
                                {
                                // distancePanel.Visibility=Visibility.Visible;
                                panelDealVoucher.Visibility=Visibility.Collapsed;
                                }
                            else
                                {
                                if(NavigationContext.QueryString.ContainsKey("offer"))
                                    {
                                    string offer=NavigationContext.QueryString["offer"];
                                    System.Diagnostics.Debug.WriteLine(offer);
                                    if(!string.IsNullOrEmpty(GiftMainPivot.myOfferDetails))
                                        {
                                        var offferObject=(JObject)JsonConvert.DeserializeObject(GiftMainPivot.myOfferDetails);

                                        panelDealVoucher.Visibility=Visibility.Visible;
                                        textExpiryDate.Text=" EXPIRY DATE: "+offferObject["expiry"].ToString();
                                        textVoucherCode.Text=" VOUCHER CODE : " + offferObject["code"].ToString();
                                        vCode=offferObject["code"].ToString();
                                        var length = Constant.getUserNumber().Length;
                                        var mob_number = new String('X', length - 4) + Constant.getUserNumber().Substring(length - 4);
                                        textMobileNumberUser.Text = " MOBILE NUMBER: " + mob_number;
                                        purchaseGiftButton.Style = (Style)Application.Current.Resources["CustomButtonStyleGray"];
                                        purchaseGiftButton.Foreground=new SolidColorBrush(Colors.White);
                                        purchaseGiftButton.Background=new SolidColorBrush(Colors.Transparent);
                                        purchaseGiftButton.BorderBrush=new SolidColorBrush(Colors.Gray);
                                        vCode=offferObject["code"].ToString();
                                        //distancePanel.Visibility=Visibility.Visible;
                                        panelDealVoucher.Visibility=Visibility.Visible;

                                        if(offferObject["status"].ToString().Equals("1"))
                                            {
                                            purchaseGiftButton.Content="REDEEM NOW";
                                            textExpiredGrid.Visibility=Visibility.Visible;
                                            //textExpired.Text="<p margin-top: 0px;width: 1271px;height: 6px;'><h1><b>Yay! It's yours now.</b></h1></p><p><h4>Visit  store to get it.</h4></p>";
                                            textExpired.Visibility=Visibility.Collapsed;
                                            TextBlock tb=new TextBlock();
                                            tb.Inlines.Add(new Run { Text = "Yay! It's yours now.", FontWeight = FontWeights.Bold, FontSize=28 });
                                            tb.Inlines.Add(new LineBreak());
                                            tb.Inlines.Add(new LineBreak());
                                            tb.Inlines.Add(new Run { Text = "Visit  store to get it.", FontWeight = FontWeights.Bold });
                                            tb.VerticalAlignment=VerticalAlignment.Center;
                                            tb.TextAlignment=TextAlignment.Center;
                                            tb.FontFamily=(FontFamily)App.Current.Resources["NormalThin"];
                                            textExpiredGrid.Children.Add(tb);
                                            }
                                        else if(offferObject["status"].ToString().Equals("2"))
                                            {
                                            purchaseGiftButton.Content="REDEEMED";
                                            textExpiredGrid.Visibility=Visibility.Visible;
                                            textExpired.Text="REDEEMED";
                                            }
                                        else if(offferObject["status"].ToString().Equals("3"))
                                            {
                                            purchaseGiftButton.Content="EXPIRED";
                                            textExpiredGrid.Visibility=Visibility.Visible;
                                            textExpired.Text="EXPIRED";
                                            }


                                        if(string.IsNullOrEmpty(offferObject["pin"].ToString()))
                                            {
                                            textPIN.Visibility=Visibility.Collapsed;
                                            }
                                        else
                                            {
                                            textPIN.Visibility=Visibility.Visible;
                                            textPIN.Text=" PIN: " + offferObject["pin"].ToString();
                                            }

                                        }
                                    }
                                purchaseGiftButton.Style = (Style)Application.Current.Resources["CustomButtonStyleGray"];
                                purchaseGiftButton.Foreground=new SolidColorBrush(Colors.White);
                                purchaseGiftButton.Background=new SolidColorBrush(Colors.Transparent);
                                purchaseGiftButton.BorderBrush=new SolidColorBrush(Colors.Gray);

                                //distancePanel.Visibility=Visibility.Visible;
                                panelDealVoucher.Visibility=Visibility.Visible;
                                //purchaseGiftButton.Content="REDEEM NOW";
                                }
                            // }
                            }

                        }
                    else
                        {
                        MessageBox.Show("Sorry details of the  deal are not available right now");
                        NavigationService.GoBack();
                        }
                    }
              
                }

            }
         
            }

        private void ButtonImage_Click(object sender, RoutedEventArgs e)
            {
            if(popup!=null)
                {
                if(popup.IsOpen==true)
                    {
                    popup.IsOpen = false;
                    ContentOverlay.Visibility=Visibility.Collapsed;
                    }
                }
            NavigationService.GoBack();
            }

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
            {
            if(popup!=null)
                {
                if(popup.IsOpen==true)
                    {
                    popup.IsOpen = false;
                    ContentOverlay.Visibility=Visibility.Collapsed;
                    }
                }

            base.OnBackKeyPress(e);
            }

        private async void updateLikeDealById(int offerId, GiftDataHelper gdh, int setLike)
            {


            var values = new List<KeyValuePair<string, string>>();
            values.Add(new KeyValuePair<string, string>("actiontype", "MyLikes"));
            values.Add(new KeyValuePair<string, string>("id", ""+offerId));
            values.Add(new KeyValuePair<string, string>("api_version", Constant.API_VERSION));

            values.Add(new KeyValuePair<string, string>("api", "true"));
            FormUrlEncodedContent values1 = new FormUrlEncodedContent(values);
            Task<string> response = Constant.postParamsAndgetResponse(Constant.B2C_URL, values1);


            string result = await response;
            if(string.IsNullOrEmpty(result))
                {
                NavigationService.Navigate(new Uri("/Views/NoConnection.xaml?", UriKind.Relative));
                }
            else
                {
                var dict = (JObject)JsonConvert.DeserializeObject(result);
                string status = dict["status"].ToString();

                if(status.Equals("success"))
                    {
                    gdh.updateGiftForLikeByOfferId(offerId, setLike);
                   

                    }
                }


            }

        private async void updateReviewDealById(int offerId, int setReview)
            {


            var values = new List<KeyValuePair<string, string>>();
            values.Add(new KeyValuePair<string, string>("actiontype", "SetOfferReview"));
            values.Add(new KeyValuePair<string, string>("id", ""+offerId));
            values.Add(new KeyValuePair<string, string>("review", ""+setReview));
            values.Add(new KeyValuePair<string, string>("api_version", Constant.API_VERSION));

            values.Add(new KeyValuePair<string, string>("api", "true"));
            FormUrlEncodedContent values1 = new FormUrlEncodedContent(values);
            Task<string> response = Constant.postParamsAndgetResponse(Constant.B2C_URL, values1);


            string result = await response;
            if(string.IsNullOrEmpty(result))
                {
                NavigationService.Navigate(new Uri("/Views/NoConnection.xaml?", UriKind.Relative));
                }
            else
                {
                var dict = (JObject)JsonConvert.DeserializeObject(result);
                string status = dict["status"].ToString();

                if(status.Equals("success"))
                    {
                  //  gdh.updateGiftForLikeByOfferId(offerId, setLike);


                    }
                }


            }

        private void Button_Click_Web(object sender, RoutedEventArgs e)
            {
            WebBrowserTask webBrowserTask = new WebBrowserTask();

            webBrowserTask.Uri = new Uri(webUrl, UriKind.RelativeOrAbsolute);

            webBrowserTask.Show();
            }

        private void purchaseGiftButton_Click(object sender, RoutedEventArgs e)
            {
            
                if(purchaseGiftButton.Content.Equals("GET IT NOW"))
                    {

                    if(mGiftsTable.by_voucher==0)
                        {
                       


                    String loyalityPoints = Constant.getLoyalityPoints();
                    String giftPoints = mGiftsTable.freebieMinAmount.ToString();
                    int loyalityPointsInt =Convert.ToInt32(loyalityPoints);
                    int giftPointsInt =Convert.ToInt32(giftPoints);

                    if(giftPointsInt > loyalityPointsInt)
                        {
                        String message1 = "You have "+ loyalityPointsInt+ " Gift Coins.\nRecharge now to earn more\ngift coins and get this gift.";

                        if(MessageBox.Show(message1, "Low Coins!", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
                            {
                            NavigationService.Navigate(new Uri("/Views/RechargeMain.xaml/?", UriKind.RelativeOrAbsolute));
                            NavigationService.RemoveBackEntry();
                            }
                        else
                            {
                            // user clicked no
                            }
                        }
                    else
                        {

                        ShowFreeBiePopUp();
                       /* String msg="Do you want to pocket this gift ?";
                        if(MessageBox.Show(msg, "Confirmation", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
                            {
                            purchaseDealFromCoins("wallet", mGiftsTable.freebieMinAmount.ToString(), mGiftsTable.offer_price.ToString());
                            }
                        else
                            {
                            // user clicked no
                            }*/
                        }
                        }
                    else if(mGiftsTable.by_voucher==1)
                        {

                        ShowVoucherPopUp();
                        //  purchaseDealFromCoins("wallet",mGiftsTable.freebieMinAmount.ToString(),mGiftsTable.offer_price.ToString());
                        }
                    }
                else if(purchaseGiftButton.Content.Equals("REDEEM NOW"))
                    {
                    if(mGiftsTable.by_voucher==0)
                        {
                    PasswordBox tb = new PasswordBox();
                    CustomMessageBox messageBox = new CustomMessageBox()
                    {
                        Caption = "Redeem",
                        Message = "Enter password",
                        LeftButtonContent = "OK",
                        RightButtonContent = "Cancel",
                        Content = tb
                    };

                    messageBox.Show();

                    messageBox.Dismissed += (s1, e1) =>
                    {
                        switch(e1.Result)
                            {
                            case CustomMessageBoxResult.LeftButton:
                                redeemFreeBieTask(tb.Password);
                                break;
                            case CustomMessageBoxResult.RightButton:
                                NavigationService.Navigate(new Uri("/HomePage.xaml?", UriKind.RelativeOrAbsolute));
                                while(this.NavigationService.BackStack.Any())
                                    {
                                    this.NavigationService.RemoveBackEntry();
                                    }

                                break;
                            case CustomMessageBoxResult.None:
                                // Do something.
                                break;
                            default:
                                break;
                            }
                    };
                    }
                    }
                else
                    {
                    }

               
            }

        private void ShowFreeBiePopUp()
            {
           
            ContentOverlay.Visibility=Visibility.Visible;
            popup = new Popup();
            popup.Height = 300;
            popup.Width = 400;
            popup.VerticalOffset=220;
            popup.HorizontalOffset=20;

            FreeeBiePopupForCoin freeeBiePopupForCoin=new FreeeBiePopupForCoin();
            popup.Child = freeeBiePopupForCoin;
            popup.IsOpen = true;
            freeeBiePopupForCoin.buttonImageOk.Click+=(s, e) =>
            {
            popup.IsOpen = false;
            ContentOverlay.Visibility=Visibility.Collapsed;
            purchaseDealFromCoins("wallet", mGiftsTable.freebieMinAmount.ToString(), mGiftsTable.offer_price.ToString());
            };


            freeeBiePopupForCoin.buttonImageCancel.Click+=(s, e) =>
            {
                popup.IsOpen = false;
                ContentOverlay.Visibility=Visibility.Collapsed;
            };


            }


        private async void redeemFreeBieTask(string password)
            {
            ProgressBarManager.Show(13, "Please wait...", false, 10);
            // Constant.SetProgressIndicator(true);
            var values = new List<KeyValuePair<string, string>>();
            values.Add(new KeyValuePair<string, string>("actiontype", "Redeem_FreebieByApp"));
            values.Add(new KeyValuePair<string, string>("coupon", vCode));
            values.Add(new KeyValuePair<string, string>("mobile", Constant.getUserNumber()));
            values.Add(new KeyValuePair<string, string>("dealid", mGiftsTable.freebieDealID.ToString()));
            values.Add(new KeyValuePair<string, string>("password", password));
            values.Add(new KeyValuePair<string, string>("api", "true"));
            FormUrlEncodedContent values1 = new FormUrlEncodedContent(values);

            Task<string> response = Constant.postParamsAndgetResponse(Constant.B2C_URL, values1);

            string result = await response;

            if(string.IsNullOrEmpty(result))
                {
                NavigationService.Navigate(new Uri("/NoConnection.xaml?", UriKind.Relative));
                }
            else
                {
                var dict = (JObject)JsonConvert.DeserializeObject(result);
                string status = dict["status"].ToString();

                if(status.Equals("success"))
                    {
                    MessageBox.Show(dict["description"].ToString());
                    purchaseGiftButton.Style = (Style)Application.Current.Resources["CustomButtonStyleGray"];
                    purchaseGiftButton.Foreground=new SolidColorBrush(Colors.White);
                    purchaseGiftButton.Background=new SolidColorBrush(Colors.Transparent);
                    purchaseGiftButton.BorderBrush=new SolidColorBrush(Colors.Gray);

                    //distancePanel.Visibility=Visibility.Visible;
                    panelDealVoucher.Visibility=Visibility.Visible;
                    purchaseGiftButton.Content="Its Yours Now";
                    }
                else
                    {
                    MessageBox.Show(dict["description"].ToString());
                    }
                }
            ProgressBarManager.Show(13, "Please wait...", false, 10);
            }



        public void ShoewRatingPopUp()
            {
            RatingPopUp ratingPopUp=new RatingPopUp();
            ContentOverlay.Visibility=Visibility.Visible;
            popup = new Popup();
            popup.VerticalOffset=100;
            popup.HorizontalOffset=20;
            popup.Child = ratingPopUp;
            popup.IsOpen = true;

            ratingPopUp.imgClose.Tap+=(a, b) =>
            {
                
            popup.IsOpen = false;
            ContentOverlay.Visibility=Visibility.Collapsed;
            };

             ratingPopUp.imgRatingBad.Tap+=(a, b) =>
            {
            updateReviewDealById(mGiftsTable.freebieOfferID,5);
            popup.IsOpen = false;
            ContentOverlay.Visibility=Visibility.Collapsed;
            };

            ratingPopUp.imgRatingJustOk.Tap+=(a, b) =>
            {
            updateReviewDealById(mGiftsTable.freebieOfferID, 3);
            popup.IsOpen = false;
            ContentOverlay.Visibility=Visibility.Collapsed;
            };

            ratingPopUp.imgRatingLike.Tap+=(a, b) =>
            {
                popup.IsOpen = false;
                ContentOverlay.Visibility=Visibility.Collapsed;
            };

            ratingPopUp.imgRatingLove.Tap+=(a, b) =>
            {
                popup.IsOpen = false;
                ContentOverlay.Visibility=Visibility.Collapsed;
            };


            ratingPopUp.imgRatingPoor.Tap+=(a, b) =>
            {
                popup.IsOpen = false;
                ContentOverlay.Visibility=Visibility.Collapsed;
            };
            }

        private void closeDailog()
            {
         
            }


        //= new System.Threading.Timer(obj => { bar(); }, null, 1000, System.Threading.Timeout.Infinite);
        private void ShowVoucherPopUp()
            {
            bool isViaWallet = false;
            ContentOverlay.Visibility=Visibility.Visible;
            popup = new Popup();
            popup.Height = 300;
            popup.Width = 400;
            popup.VerticalOffset=100;
            popup.HorizontalOffset=20;
            FreeBiePopUp control = new FreeBiePopUp();
            popup.Child = control;
            popup.IsOpen = true;
            control.textRupee.Text=mGiftsTable.offer_price.ToString();
            control.textCoin.Text=mGiftsTable.freebieMinAmount.ToString();
            control.imageOperator.Source=new BitmapImage(new Uri(mGiftsTable.freebieLogoURL, UriKind.RelativeOrAbsolute));
            if(Constant.getbalanceInInt()<=0)
                {
                control.Chkcategories.Visibility=Visibility.Collapsed;
                control.Chkcategories.IsChecked=false;
                isViaWallet=false;
                }
            else
                {
                control.Chkcategories.Visibility=Visibility.Visible;
                control.Chkcategories.IsChecked=true;
                isViaWallet=true;
                }




            String loyalityPoints = Constant.getLoyalityPoints();
            String giftPoints = mGiftsTable.freebieMinAmount.ToString();
            int loyalityPointsInt =Convert.ToInt32(loyalityPoints);
            int giftPointsInt =Convert.ToInt32(giftPoints);

            if(giftPointsInt > loyalityPointsInt)
                {
               // control.Chkcategories.Visibility=Visibility.Collapsed;
               // control.Chkcategories.IsChecked=false;
                String message1 = "You have "+ loyalityPointsInt+ " Gift Coins. Recharge now to earn more gift coins.\n OR \n Continue to pay full Rs."+mGiftsTable.price;
                control.textLowCoinsMessage.Visibility=Visibility.Visible;
               control.textLowCoinsMessage.Text=message1;
               
                }
               
            else
                {
                control.textLowCoinsMessage.Visibility=Visibility.Collapsed;


                }


            control.buttonImageCancel.Click+=(a, b) =>
            {
                ContentOverlay.Visibility=Visibility.Collapsed;
                popup.IsOpen = false;
            };
            control.buttonImageOk.Click+=(a, b) =>
            {
                ContentOverlay.Visibility=Visibility.Collapsed;
                popup.IsOpen = false;
                String loyaltyPoints=  Constant.getLoyalityPoints();
                if(control.Chkcategories.IsChecked==true)
                    {
                    isViaWallet = true;
                    }
                else
                    {
                    isViaWallet = false;
                    }

                if(Convert.ToInt32(loyaltyPoints)>mGiftsTable.freebieMinAmount)
                    {

                    if(isViaWallet)
                        {
                        purchaseDealFromCoins("wallet", mGiftsTable.freebieMinAmount.ToString(), mGiftsTable.offer_price.ToString());
                        }
                    else
                        {
                        purchaseDealFromCoins("online", mGiftsTable.freebieMinAmount.ToString(), mGiftsTable.offer_price.ToString());
                        }
                    }
                else
                    {
                    if(isViaWallet)
                        {
                        purchaseDealFromCoins("wallet", "0", mGiftsTable.price.ToString());
                        }
                    else
                        {
                        purchaseDealFromCoins("online", "0", mGiftsTable.price.ToString());
                        }
                    }
            };

            }

        private async void purchaseDealFromCoins(String paymentopt, String amount, String of_price)
            {


            ProgressBarManager.Show(22, "Please wait...", false, 10);

            var values = new List<KeyValuePair<string, string>>();
            values.Add(new KeyValuePair<string, string>("actiontype", "purchase_deal"));
            values.Add(new KeyValuePair<string, string>("deal_id", mGiftsTable.freebieDealID.ToString()));

            values.Add(new KeyValuePair<string, string>("offer_id", mGiftsTable.freebieOfferID.ToString()));
            values.Add(new KeyValuePair<string, string>("coupon_id", ""));

            values.Add(new KeyValuePair<string, string>("paymentopt", paymentopt));
            values.Add(new KeyValuePair<string, string>("amount", amount));
            values.Add(new KeyValuePair<string, string>("of_price", of_price));

            values.Add(new KeyValuePair<string, string>("quantity", "1"));
            values.Add(new KeyValuePair<string, string>("trans_category", "deal"));
            values.Add(new KeyValuePair<string, string>("freebie", "true"));
            values.Add(new KeyValuePair<string, string>("partial", "1"));
            values.Add(new KeyValuePair<string, string>("ref_id", ""));




            values.Add(new KeyValuePair<string, string>("api_version", Constant.API_VERSION));
            values.Add(new KeyValuePair<string, string>("api", "true"));
            FormUrlEncodedContent values1 = new FormUrlEncodedContent(values);
            Task<string> response = Constant.postParamsAndgetResponse(Constant.B2C_URL, values1);

            try
                {
                string result = await response;
                if(string.IsNullOrEmpty(result))
                    {
                    NavigationService.Navigate(new Uri("/Views/NoConnection.xaml?", UriKind.Relative));
                    }
                else
                    {
                    var dict = (JObject)JsonConvert.DeserializeObject(result);
                    string status = dict["status"].ToString();

                    if(paymentopt=="wallet"/*amount<Constant.getbalanceInInt()*/)
                        {
                        if(status.Equals("success"))
                            {
                            var description = dict["description"];

                            bool online_form=(Boolean)description["online_form"];
                            if(online_form==false)
                                {
                                Constant.setbalance(description["closing_balance"].ToString());
                                Constant.setLoyalityPoints(description["loyalty_balance"].ToString());
                                PhoneApplicationService.Current.State["rechargeAmount"] = amount;
                                panelDealVoucher.Visibility=Visibility.Visible;
                                textExpiryDate.Text=" EXPIRY DATE: "+description["expiry"].ToString();
                                textVoucherCode.Text=" VOUCHER CODE : " + description["deal_coupon_code"].ToString();
                                vCode=description["deal_coupon_code"].ToString();
                                var length = Constant.getUserNumber().Length;
                                var mob_number = new String('X', length - 4) + Constant.getUserNumber().Substring(length - 4);
                                textMobileNumberUser.Text = " MOBILE NUMBER: " + mob_number;
                                purchaseGiftButton.Style = (Style)Application.Current.Resources["CustomButtonStyleGray"];
                                purchaseGiftButton.Foreground=new SolidColorBrush(Colors.White);
                                purchaseGiftButton.Background=new SolidColorBrush(Colors.Transparent);
                                purchaseGiftButton.BorderBrush=new SolidColorBrush(Colors.Gray);

                                //distancePanel.Visibility=Visibility.Visible;
                                panelDealVoucher.Visibility=Visibility.Visible;
                                purchaseGiftButton.Content="REDEEM NOW";

                                if(string.IsNullOrEmpty(description["pin"].ToString()))
                                    {
                                    textPIN.Visibility=Visibility.Collapsed;
                                    }
                                else
                                    {
                                    textPIN.Visibility=Visibility.Visible;
                                    textPIN.Text=" PIN: " + description["pin"].ToString();
                                    }
                               
                                //Constant.ShowSuccessPopUp(ContentOverlay, "5", "", amount.ToString(), "5", true, "", description["loyalty_balance"].ToString());
                                }
                            else if(online_form==true)
                                {
                                // var description = dict["description"];
                                string form_content = description["form_content"].ToString();
                                PhoneApplicationService.Current.State[Constant.PAYMENT_GATEWAY_HTML] = form_content;
                                PhoneApplicationService.Current.State[Constant.RECHARGE_AMOUNT] = amount;
                                PhoneApplicationService.Current.State[Constant.OPERATOR_RECHARGE_FLAG]="5";
                                PhoneApplicationService.Current.State["txn_id"]=description["txnid"].ToString();

                                NavigationService.Navigate(new Uri("/Views/PaymentGatewayLayout.xaml?page=HomePage", UriKind.RelativeOrAbsolute));
                                }


                            }
                        else
                            {

                            string errCode = dict["errCode"].ToString();
                            if(errCode.Equals("201"))
                                {
                                (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri("/Login.xaml?no-cache=" + Guid.NewGuid(), UriKind.Relative));

                                (Application.Current.RootVisual as PhoneApplicationFrame).RemoveBackEntry();


                                }
                            else
                                {
                                //Constant.ShowSuccessPopUp(ContentOverlay, "5", "5", amount.ToString(), oper.p_id.ToString(), false, dict["description"].ToString(), "");
                                }

                            //Constant.checkForErrorCode (result);
                            }
                        }
                    else
                        {
                        if(status.Equals("success"))
                            {
                            var description = dict["description"];
                            string form_content = description["form_content"].ToString();
                            PhoneApplicationService.Current.State[Constant.OPERATOR_RECHARGE_FLAG]="5";
                            PhoneApplicationService.Current.State[Constant.PAYMENT_GATEWAY_HTML] = form_content;
                            PhoneApplicationService.Current.State[Constant.RECHARGE_AMOUNT] = amount;
                           
                            PhoneApplicationService.Current.State["txn_id"]=description["txnid"].ToString();
                            NavigationService.Navigate(new Uri("/Views/PaymentGatewayLayout.xaml?page=HomePage", UriKind.RelativeOrAbsolute));

                            }
                        else
                            {

                            string errCode = dict["errCode"].ToString();
                            if(errCode.Equals("201"))
                                {
                                (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri("/Login.xaml?no-cache=" + Guid.NewGuid(), UriKind.Relative));

                                (Application.Current.RootVisual as PhoneApplicationFrame).RemoveBackEntry();

                                //return responseString.ToString();	  
                                }
                            else
                                {
                                //Constant.ShowSuccessPopUp(ContentOverlay, rechargeFlag, number, amount, op_id, pay1_pro_id, false);
                                }

                            //Constant.checkForErrorCode (result);
                            }
                        }
                    }
                }
            catch(Exception exc)
                {
                ProgressBarManager.Hide(22);
                }
            ProgressBarManager.Hide(22);
            }

        private void imgRating_Tap(object sender, System.Windows.Input.GestureEventArgs e)
            {
            ShoewRatingPopUp();
            }

      
        private void callButton_Click(object sender, RoutedEventArgs e)
            {
            try
                {

                if(callLocationArray!=null)
                    {

                    CallPopup callPopup=new CallPopup();
                    ContentOverlay.Visibility=Visibility.Visible;
                    popup = new Popup();
                    popup.VerticalOffset=100;
                    popup.HorizontalOffset=20;
                    popup.Child = callPopup;
                    popup.IsOpen = true;

                    callPopup.imgClose.Tap+=(a, b) =>
                    {

                        popup.IsOpen = false;
                        ContentOverlay.Visibility=Visibility.Collapsed;
                    };
                    if(callLocationArray.Count!=0)
                        {
                        foreach(JObject jObj in callLocationArray)
                            {
                            CallControl callControl=new CallControl();
                            callControl.textCallNumber.Text=jObj["address"].ToString()+" "+ jObj["area"].ToString();
                            callPopup.callListBox.Items.Add(callControl);
                            }
                        }
                    else
                        {
                        if(string.IsNullOrEmpty(dealer_number))
                            {
                            }
                        else
                            {
                            String callNumber=dealer_number.ToString();
                            String[] callNumbers=callNumber.Split(',');
                            PhoneCallTask phoneCallTask = new PhoneCallTask();

                            phoneCallTask.PhoneNumber = callNumbers[0];
                            phoneCallTask.DisplayName = textTitle.Text;

                            phoneCallTask.Show();
                            }
                        }


                    callPopup.callListBox.SelectionChanged+=(a, b) =>
                    {
                        int selectedIndex=callPopup.callListBox.SelectedIndex;
                        if(selectedIndex==-1)
                            return;



                        popup.IsOpen = false;
                        ContentOverlay.Visibility=Visibility.Collapsed;
                        String callNumber=callLocationArray[selectedIndex]["dealer_contact"].ToString();
                        String[] callNumbers=callNumber.Split(',');
                        PhoneCallTask phoneCallTask = new PhoneCallTask();

                        phoneCallTask.PhoneNumber = callNumbers[0];
                        phoneCallTask.DisplayName = textTitle.Text;

                        phoneCallTask.Show();



                    };


                    }
                else
                    {
                    if(string.IsNullOrEmpty(dealer_number))
                        {
                        }
                    else
                        {
                        String callNumber=dealer_number.ToString();
                        String[] callNumbers=callNumber.Split(',');
                        PhoneCallTask phoneCallTask = new PhoneCallTask();

                        phoneCallTask.PhoneNumber = callNumbers[0];
                        phoneCallTask.DisplayName = textTitle.Text;

                        phoneCallTask.Show();
                        }
                    }
                }
            catch(Exception exc)
                {
                }

            }




        }
    }