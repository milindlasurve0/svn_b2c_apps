//
//  GiftList.h
//  Pay1
//
//  Created by Annapurna on 16/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GiftList : NSObject

@property (nonatomic, strong) NSString *featured;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *giftID;
@property (nonatomic, strong) NSString *details;




@end
