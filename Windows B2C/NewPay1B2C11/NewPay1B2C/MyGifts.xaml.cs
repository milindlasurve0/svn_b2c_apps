﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Windows.Media.Imaging;
using System.Windows.Input;
using System.ComponentModel;

namespace NewPay1B2C
	{
	public partial class MyGifts : PhoneApplicationPage
		{
		List<string> myDealList;

		ProgressBarManager ProgressBarManager;
		public static string DEAL_DATA = "dealdata";
		public MyGifts ()
			{

			InitializeComponent ();
			
			Constant.laodNavigationBar (TitlePanel, LayoutRoot, canvas, "My Gifts");
			GoogleAnalytics.EasyTracker.GetTracker ().SendView ("MyGifts");
			myDealList = new List<string> ();
			ProgressIndicator prog = new ProgressIndicator ();
			ProgressBarManager = new ProgressBarManager ();
			SystemTray.SetProgressIndicator (this, prog);
			ProgressBarManager.Hook (prog);
			//getDealsTask ();
			}
		protected override void OnNavigatedTo (NavigationEventArgs e)
			{
			getDealsTask ();
			}

		protected override void OnBackKeyPress (System.ComponentModel.CancelEventArgs e)
			{
			//Do your work here
			base.OnBackKeyPress (e);
			NavigationService.Navigate (new Uri ("/HomePage.xaml?", UriKind.RelativeOrAbsolute));
			while (this.NavigationService.BackStack.Any ())
				{
				this.NavigationService.RemoveBackEntry ();
				}
			}

		private async void getDealsTask ()
			{
			myFreebieListBox.Items.Clear ();
			ProgressBarManager.Show (13, "Please wait...", false, 10);
			// Constant.SetProgressIndicator(true);
			var values = new List<KeyValuePair<string, string>> ();
			values.Add (new KeyValuePair<string, string> ("actiontype", "get_my_deal"));

			values.Add (new KeyValuePair<string, string> ("freebie", "true"));
			values.Add (new KeyValuePair<string, string> ("api", "true"));
			FormUrlEncodedContent values1 = new FormUrlEncodedContent (values);

			Task<string> response = Constant.postParamsAndgetResponse (Constant.B2C_URL, values1);

			string result = await response;

			if (string.IsNullOrEmpty (result))
				{
				NavigationService.Navigate (new Uri ("/NoConnection.xaml?", UriKind.Relative));
				}
			else
				{
				var dict = (JObject)JsonConvert.DeserializeObject (result);
				string status = dict["status"].ToString ();

				if (status.Equals ("success"))
					{
					var dealItems = dict["description"];
					foreach (var dealItem in dealItems)
						{
						myDealList.Add (dealItem.ToString ());
						MyDealControl dealsControl = new MyDealControl ();
						string img_url = dealItem["img_url"].ToString ();
						if (string.IsNullOrEmpty (img_url) == true)
							{
							}
						else
							{
							Uri uri = new Uri (img_url, UriKind.Absolute);

							dealsControl.imageDeal.Source = new BitmapImage (uri);
							}
						dealsControl.textDealsDesc.Text = dealItem["deal_name"].ToString ();
						dealsControl.textdealStatus.Text = dealItem["coupon_status"].ToString ();
						dealsControl.textdealExpiry.Text = dealItem["expiry"].ToString ();

						myFreebieListBox.Items.Add (dealsControl);
						}

					if (myFreebieListBox.Items.Count == 0)
						{
						emptyViewPanel.Visibility = Visibility.Visible;
						emptyText.Text = "Your free gifts vouchers will be listed here. Gift vouchers needs to be presented to the merchant at the time of redemption.\n\nYou can get free gifts after recharging or paying bills.";
						myFreebieListBox.Visibility = Visibility.Collapsed;
						}
					else
						{
						emptyViewPanel.Visibility = Visibility.Collapsed;
						myFreebieListBox.Visibility = Visibility.Visible;
						}
					}
				else
					{
					Constant.checkForErrorCode (result);
					}
				}
			//Constant.SetProgressIndicator(false);
			ProgressBarManager.Hide (13);
			}

		private void image_sms_MouseEnter (object sender, System.Windows.Input.GestureEventArgs e)
			{
			Image img = sender as Image;
			string tranId = img.Tag.ToString ();
			notifyMeTask ("sms", tranId);
			}

		private void image_email_MouseEnter (object sender, System.Windows.Input.GestureEventArgs e)
			{
			Image img = sender as Image;
			string tranId = img.Tag.ToString ();
			notifyMeTask ("email", tranId);
			}

		private async void notifyMeTask (string type, string transaction_id)
			{
			ProgressBarManager.Show (13, "Please wait...", false, 10);
			var values = new List<KeyValuePair<string, string>> ();
			values.Add (new KeyValuePair<string, string> ("actiontype", "notifyme"));
			values.Add (new KeyValuePair<string, string> ("type", type));
			values.Add (new KeyValuePair<string, string> ("transaction_id", transaction_id));
			values.Add (new KeyValuePair<string, string> ("api", "true"));
			FormUrlEncodedContent values1 = new FormUrlEncodedContent (values);
			Task<string> response = Constant.postParamsAndgetResponse (Constant.B2C_URL, values1);

			string result = await response;

			var dict = (JObject)JsonConvert.DeserializeObject (result);
			string status = dict["status"].ToString ();

			if (status.Equals ("success"))
				{
				var message = dict["description"];
				MessageBox.Show (message.ToString ());
				}
			ProgressBarManager.Hide (13);
			}

		private void image_qrCode_MouseEnter (object sender, System.Windows.Input.GestureEventArgs e)
			{
			Image img = sender as Image;
			string data = img.Tag.ToString ();
			PhoneApplicationService.Current.State[MyGifts.DEAL_DATA] = data;
			var data1 = (JObject)JsonConvert.DeserializeObject (data);
			string code = data1["code"].ToString ();
			if (string.IsNullOrEmpty (code))
				{
				MessageBox.Show ("Sorry, you do not have valid voucher code.");
				}
			else
				{
				NavigationService.Navigate (new Uri ("/MyDealVoucher.xaml?", UriKind.RelativeOrAbsolute));
				}
			}




		private void buttonImage_Click_Back (object sender, RoutedEventArgs e)
			{
			NavigationService.GoBack ();
			}


		private void myFreebieListBox_SelectedIndexChanged (object sender, System.EventArgs e)
			{
			if (myFreebieListBox.SelectedIndex == -1) return;


			MyDealControl item = myFreebieListBox.SelectedItem as MyDealControl;
			if (item.textdealStatus.Text.Equals ("Expired"))
				{
				}
			else
				{
				int selectedIndex = myFreebieListBox.SelectedIndex;
				NavigationService.Navigate (new Uri ("/MyFreeBieDetails.xaml?", UriKind.RelativeOrAbsolute));
				PhoneApplicationService.Current.State[MyGifts.DEAL_DATA] = myDealList[selectedIndex];
				}
			}

		private void Button_Click (object sender, RoutedEventArgs e)
			{
			PhoneApplicationService.Current.State["data"] = "2";
			(Application.Current.RootVisual as PhoneApplicationFrame).Navigate (new Uri ("/RechargeAndBills.xaml?", UriKind.RelativeOrAbsolute));
			(Application.Current.RootVisual as PhoneApplicationFrame).RemoveBackEntry ();
			}

		}
	}