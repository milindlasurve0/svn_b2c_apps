
<?php
//ini_set("display_errors","on");
session_start();

require_once 'config.php';
require_once 'common.php';

if ($_SESSION["flag"] != "true") {

    header("location:login.php?logout=logout");
}
?><!DOCTYPE html>
<html lang="en">
    <head>
        <title>Profile || PAY1</title>



        <link rel="stylesheet" href="jquery.ultimate-smartbanner.css" type="text/css" media="screen">
        <meta charset="utf-8">  
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,width=device-width,user-scalable=no" />
        <link rel="shortcut icon" type="image/png" href="/images/pay1.png"/>

        <!--	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
                 <link rel="stylesheet" href="css/jquery-ui-themes-1.11.0/themes/smoothness/jquery-ui.css">
                 
                <link rel="stylesheet" type="text/css" href="css/style.css">-->

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>/
      <div>
        <!-- navigation -->
    <div class="mobilemenu">
        <p class="mobiletoggle" data-idd="#mobnav"></p>
        <div class="walletTOP">
            <ul class="nav nav-pills dealtoplist">
                <li>
                    <span class="dropdown-toggle" data-toggle="dropdown">
                        <table>
                            <tr>
                                <td><span class="deals-balance"></span></td>
                                <td>Balance<br> <strong>60,000 <span class="caret"></span></strong></td>
                            </tr>
                        </table>
                    </span>
                    <ul class="dropdown-menu" role="menu">
                        <li>1,00,000</li>
                        <li>2,00,000</li>
                        <li>3,00,000</li>
                    </ul>
                </li>
            </ul>
        </div>
        <a href="index1.html" title="Pay1"><img src="images/logo.jpg" align="logo-image"></a>
    </div>
    <div class="opacitybg"></div>

    <?php include 'left.php'; ?>

    <!-- //close navigation -->

    <div class="result"></div>
    <div class="shiftbox">
        <div class="hutpart">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <?php
                        include 'header.php';
                        include 'analyticstracking.php';
                        ?>

                    </div>
                </div>
                <div class="row mT20">
                    <div class="col-sm-3">
                        <ul id="myTab" class="nav nav-tabs1 profilelist" role="tablist">
                            <li class="active">
                                <a data-toggle="tab" role="tab" href="#tab1">
                                    <span class="proicon-profile hideon767"></span> Profile</a>
                            </li>
                            <li class="">
                                <a data-toggle="tab" role="tab" onclick="getTransactionHistory('0');" href="#tab2">
                                    <span class="proicon-phistory hideon767"></span> Purchase History
                                </a>
                            </li>
                            <li class="">
                                <a data-toggle="tab" role="tab" href="#tab3" onclick="getquickpaylist('1')">
                                    <span class="proicon-qpay hideon767"></span> Quick Pay
                                </a>
                            </li>
                            <li class="">
                                <a data-toggle="tab" role="tab" href="#tab4">
                                    <span class="proicon-sup hideon767"></span> Support
                                </a>
                            </li>
                            <li class="">
                                <a data-toggle="tab" role="tab" onclick="getfaq('faq')" href="#tab5">
                                    <span class="proicon-faq hideon767"></span> FAQ's
                                </a>
                            </li>
                            <li class="">
                                <a data-toggle="tab" role="tab" onclick="getfaq('tnc')" href="#tab6">
                                    <span class="proicon-oterms hideon767"></span> Our Terms
                                </a>
                            </li>
                        </ul>
                    </div>

                    <div class="col-sm-9">
                        <div id="ListTabContent" class="tab-content clearfix">
                            <div id="tab1" class="tab-pane fade active in">
                                <div class="dealfullbox">
                                    <div class="dealboxx">
                                        <div class="dealsbread">
                                            <span class="proicon-profile proicmbT"></span> <strong>Profile</strong>
                                        </div>

                                        <div>
                                            <h4 class="prohead">Personal Details</h4>
<!--                                            <span id="update_msg" style="color:red;"></span>-->
                                            <form class="form-horizontal proform mT20" method="post"  id="updateprofile" role="form">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-3 control-label">Name</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="name" value="<?php echo $_SESSION["name"]; ?>" class="form-control" id="name">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="dob" class="col-sm-3 control-label">Date of Birth</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="" readonly="readonly" id="datepicker" value="<?php echo $_SESSION["dob"]; ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">Gender</label>
                                                    <div class="col-sm-8">
                                                        <div class="btn-group" data-toggle="buttons">
                                                            <label <?php if ($_SESSION["gender"] == "m") { ?>class="btn btn-primary active" selected="selected"<?php } else { ?> class="btn btn-primary"<?php } ?>>
                                                                <input type="radio" name="options" value="m" id="option1">M
                                                            </label>
                                                            <label <?php if ($_SESSION["gender"] == "f") { ?>class="btn btn-primary active" selected="selected"<?php } else { ?> class="btn btn-primary"<?php } ?>>
                                                                <input type="radio" name="options" value="f"  id="option2">F
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label for="mnumber" class="col-sm-3 control-label">Mobile Number</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" disabled="disabled"  name="number" value="<?php echo $_SESSION["mobile"]; ?>"id="mnumber">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="email_id" class="col-sm-3 control-label">Email Id</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control"  name="email_id" value="<?php echo $_SESSION["email"]; ?>"id="email_id">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="apin" class="col-sm-3 control-label">Account Pin</label>
                                                    <div class="col-sm-8">
                                                        <input type="password" disabled="disabled" class="form-control" value="<?php echo $_SESSION["password"]; ?>" name="pin" id="pin">
                                                    </div>
                                                </div>
                                                <!--										  <div class="form-group">
                                                                                                                                    <label for="plocation" class="col-sm-3 control-label">Preferred Location</label>
                                                                                                                                    <div class="col-sm-8">
                                                                                                                                      <select class="form-control" id="plocation">
                                                                                                                                        <option></option>
                                                                                                                                        <option>Jaipur</option>
                                                                                                                                        <option>Ajmer</option>
                                                                                                                                        <option>Bikaner</option>
                                                                                                                                        <option>Udaipur</option>
                                                                                                                                      </select>
                                                                                                                                    </div>
                                                                                                                                  </div>-->
                                                <!--										  <div class="form-group">
                                                                                                                                    <label for="flink" class="col-sm-3 control-label">Link to Facebook</label>
                                                                                                                                    <div class="col-sm-8">
                                                                                                                                      <input type="text" class="form-control" id="flink">
                                                                                                                                    </div>
                                                                                                                                  </div>-->
                                                <div class="form-group">
                                                    <div class="col-sm-offset-3 col-sm-8">
                                                        <input type="button" class="btn btn-primary" value="Save" onclick="return updateprofile();">
                                                        <input type="button" class="btn btn-default" value="Cancel">
                                                    </div>
                                                </div>
                                            </form>

                                            <!-- /////////////////////////////////////// -->
                                            <div class="panel-group mT20" id="proaccordion">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title" data-toggle="collapse" data-parent="#proaccordion" data-target="#collaps1">										        
                                                            Change Pin
                                                            <span  class="proediticon"></span>
                                                        </h4>
                                                        <span id="update_msg" style="color:red;"></span>
                                                    </div>
                                                    <div id="collaps1" class="panel-collapse collapse paddLnR">
                                                        <form class="form-horizontal proform mT20" role="form">
                                                            <div id="changepin" style="display: none;" >
                                                            <div class="form-group">

                                                                <label for="" class="col-sm-3 control-label">OTP</label>
                                                                <div class="col-sm-4">
                                                                    <input type="text" name="otp" class="form-control" id="otp">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="" class="col-sm-3 control-label">New Pin</label>
                                                                <div class="col-sm-4">
                                                                    <input type="text" name="new_pin" id="new_pin" class="form-control">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="" class="col-sm-3 control-label">Confirm Pin</label>
                                                                <div class="col-sm-4">
                                                                    <input type="text" name="confirm_pin" id="confirm_pin" class="form-control">
                                                                </div>
                                                            </div>
                                                                <div class="form-group">
                                                                <div class="col-sm-offset-3 col-sm-8">
                                                                    <input type="button" onclick="changepin();"  value="Save" class="btn btn-primary">
                                                                    <input type="button" class="btn btn-default" value="Cancel">
                                                                </div>
                                                            </div>
                                                            </div>
                                                            <div id="updatepassword">
                                                             <div class="form-group">

                                                                <label for="" class="col-sm-3 control-label">Mobile Number</label>
                                                                <div class="col-sm-4">
                                                                    <input type="text" name="mobile_number" class="form-control"  maxlength="10" id="mobile_number">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="col-sm-offset-3 col-sm-8">
                                                                    <input type="button" onclick="updatePassword();"  value="Save" class="btn btn-primary">
                                                                    <input type="button" class="btn btn-default" value="Cancel">
                                                                </div>
                                                            </div>
                                                            </div>
                                                            
                                                            
                                                        </form>
                                                        <!-- <div class="panel-body">
                                                          <input type="text" class="form-control">
                                                        </div> -->
                                                    </div>
                                                </div>
                                                <!--										  <div class="panel panel-default">
                                                                                                                                    <div class="panel-heading">
                                                                                                                                      <h4 class="panel-title" data-toggle="collapse" data-parent="#" data-target="#collaps2">
                                                                                                                                        Preferred Location
                                                                                                                                        <span  class="proediticon"></span>
                                                                                                                                      </h4>
                                                                                                                                    </div>
                                                                                                                                    <div id="collaps2" class="panel-collapse collapse paddLnR" style="overflow:hidden;">
                                                                                                                                      <form class="form-horizontal proform mT20" role="form">
                                                                                                                                                          <div class="form-group">
                                                                                                                                                            <label for="" class="col-sm-3 control-label">Preferred Location</label>
                                                                                                                                                            <div class="col-sm-4">
                                                                                                                                                              <input type="text" class="form-control" id="">
                                                                                                                                                            </div>
                                                                                                                                                          </div>
                                                                                                                                                          <div class="form-group">
                                                                                                                                                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d227748.86053520974!2d75.79055784999998!3d26.88521075000001!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x396c4adf4c57e281%3A0xce1c63a0cf22e09!2sJaipur%2C+Rajasthan!5e0!3m2!1sen!2sin!4v1408732519518" width="100%" height="200" frameborder="0" style="border:0"></iframe>
                                                                                                                                                          </div>
                                                                                                                                                        </form>
                                                                                                                                    </div>
                                                                                                                                  </div>-->
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="loader">
                                <center>
                                    <img class="loading-image" src="images/ajax-loader.gif" alt="loading..">
                                </center>
                            </div>


                            <div id="tab2" class="tab-pane fade">
                                <div class="dealfullbox">
                                    <div class="dealboxx">
                                        <div class="dealsbread">
                                            <span class="proicon-phistory proicmbT"></span> <strong>Purchase History</strong>
                                        </div>
                                        <div>
                                            <h4 class="prohead">Transaction Details</h4>
                                            <div class="table-responsive" id="transdetails">
                                                <table class='purchasetable mT20' id="purchasehistory">
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="modal fade in ordersuccess1"   tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-sm">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button class="close" data-dismiss="modal" type="button" title="Close">
                                                <span aria-hidden="true">×</span>
                                                <span class="sr-only">Close</span>
                                            </button>

                                        </div>
                                        <div class="modal-body">

                                            <div class="row">
                                                <div class="col-sm-5">
                                                    <small><b>Number</b></small>
                                                    <p id ="trans_number"></p>
                                                    <p id ="trans_operator"></p>
                                                </div>
                                                <div class="col-sm-3">
                                                    <small><b>Amount</b></small>
                                                    <p id ="trans_amount"</p>

                                                </div>


                                            </div>
                                            <div class="row">

                                                <div class="col-sm-12">
                                                    <small><b>Date</b></small>
                                                    <p id ="trans_date"></p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <small><b>Status</b></small>
                                                    <p id ="trans_status"></p>
                                                </div>
                                                <div class="col-sm-12">
                                                    <small><b>Transaction Id</b></small>
                                                    <p id ="trans_id"></p>
                                                </div>
                                                <div class="col-sm-12">
                                                    <small><b>Payment Mode</b></small>
                                                    <p id ="trans_mode"></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="tab3" class="tab-pane fade">
                                <div class="dealfullbox">
                                    <div class="dealboxx">
                                        <div class="dealsbread">
                                            <span class="proicon-qpay proicmbT"></span> <strong>Quick Pay</strong>
                                            <span style="color:red;" id="quickmsg"></span>
                                        </div>
                                        <div>
                                            <div class="table-responsive" id="quickpay">

                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <!-- popup open -->

                                <!-- //popup open -->
                                <!-- popup open -->
                                <div class="acknow" id="misscallpopup">
                                    <input type="hidden" name="recharge_number" id="recharge_number" value="">
                                    <input type="hidden" name="recharge_name" id="recharge_name" value="">
                                    <input type="hidden" name="recharge_amount" id="recharge_amount" value="">
                                    <input type="hidden" name="recharge_operator" id="recharge_operator" value="">
                                    <input type="hidden" name="delegate_flag" id="delegate_flag" value="">
                                    <input type="hidden" name="confiramtion" id="confiramtion" value="">
                                    <input type="hidden" name="flag" id="flag" value="">
                                    <input type="hidden" name="id" id="id" value="">
                                    <input type="hidden" name="stv" id="stv" value="">
                                    <input type="hidden" name="missedcall" id="missedcall" value="">


                                    <div class="acknowtextbox">
                                        <dl class="dl-horizontal">

                                            <dd>
                                                <h3>Set Missed call</h3>
                                                <p id ="on" style="display:none;">
                                                    Turn on to activate missed call recharge service for <span id="on_number"></span> of Rs.<span id="on_amt"></span>
                                                    <br/>
                                                    on<input type="radio" name="turn_on" value="1" id="turn_on">
                                                    off<input type="radio" name="turn_on" value="0" checked="checked"  id="turn_off">
                                                </p>
                                                <p id ="off" style="display:none;">
                                                    Request <span id="off_number"></span> to give missedcall on <span id="missed_number"></span> to recharge<br/>
                                                    on<input type="radio" name="req_on"   value="1" checked="ckecked" id="req_on">
                                                    off<input type="radio" name="req_on" value="0"  id="req_off">
                                                </p>

                                                <button onclick="hide('misscallpopup');
                                                        submitrequest('recharge_number', 'recharge_name', 'recharge_amount', 'recharge_operator', 'delegate_flag', 'flag', 'id', 'stv', '0')">Submit</button>
                                                <button onclick="hide('misscallpopup');">Cancel</button>
                                            </dd>
                                        </dl>
                                    </div>
                                </div>

                                <div class="modal fade in orderconfirmation"   id="orderconfirmation" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-sm">
                                        <form method="post" action="" id="payment_form">
                                            <input type="hidden" name="rec_amount" id="rec_amount" value="">
                                            <input type="hidden" name="rec_operator" id="rec_operator" value="">
                                            <input type="hidden" name="rec_flag" id="rec_flag" value="">
                                            <input type ="hidden" name="rec_number" id="rec_number" value="">
                                            <input type ="hidden" name="payment_option" value="" id="payment_option">
                                            <input type="hidden" name="service_charge" id ="service_charge" value="">
                                            <input type="hidden" name="charge_slab" id ="charge_slab" value="">
                                            <input type="hidden" name="service_charge_percent" id ="service_charge_percent" value="">
                                            <input type="hidden" name="total_amount" id ="total_amount" value="">
                                            <input type="hidden" name="max_amount" id ="max_amount" value="">
                                            <input type="hidden" name="min_amount" id ="min_amount" value="">

                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button class="close" data-dismiss="modal" type="button" title="Close">
                                                        <span aria-hidden="true">×</span>
                                                        <span class="sr-only">Close</span>
                                                    </button>

                                                    <h4 id="mySmallModalLabel" class="modal-title">
                                                        Order Confirmation
                                                    </h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-sm-5">
                                                            <small id="recharge_type">Number</small>
                                                            <p id ="number"></p>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <small>Amount</small>
                                                            <p id ="amount" style="display:none;"></p>
                                                            <p id="post_amount" style="display:none;"><input type="text" name="post_amount" onkeypress="return isNumberKey(event);" id="postpaid_amount"><input type="button" onclick="orderclick();" id="button"  class="common-btn-submit" value="submit"></p>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <small id ="description"></small>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <small style="display:none;" id ="service_msg"></small>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <small>Service Provider</small>
                                                            <p id ="provider_name"></p>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-sm-12 popupdivision">
                                                            <small>Pay now via</small>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <table>
                                                                <tr>
                                                                    <td><span class="blk-wallet"></span></td>
                                                                    <td >Balance<br> <strong id="balance"><?php echo ($_SESSION["wallet_balance"] > 0) ? $_SESSION["wallet_balance"] : 0 ?></strong></td>
                                                                </tr>
                                                            </table>
                                                            <input type="button" class="common-btn mT10 btn-block"  value="Pay with wallet" id="wallet"  onclick="payment('rec_amount', 'rec_operator', 'rec_flag', 'rec_number', this.value)">
                                                            <span style="color:red;" id="walletmsg"></span>

                                                        </div>
                                                        <div class="col-sm-6">
                                                            <table>
                                                                <tr>
                                                                    <td><span class="blk-atm"></span></td>
                                                                    <td><small>Pay with your Debit or Credit Card</small></td>
                                                                </tr>
                                                            </table>
                                                            <input type="button" class="common-btn mT10 btn-block"    value="Payment Gateway" onclick="payment('rec_amount', 'rec_operator', 'rec_flag', 'rec_number', this.value)">

                                                            <!--</button>-->
                                                        </div>
                                                    </div>
                                                    <!--				<div class="row mT15">
                                                                                            <div class="col-sm-12">
                                                                                                    <small>Have a coupon code?</small>
                                                                                                    <input type="text" class="form-control" placeholder="Redeem">
                                                                                            </div>
                                                                                    </div>-->
                                                </div>
                                            </div>
                                        </form>

                                    </div>
                                </div>






                                <!-- //popup open -->
                            </div>
                            <div id="tab4" class="tab-pane fade">
                                <div class="dealfullbox">
                                    <div class="dealboxx">
                                        <div class="dealsbread">
                                            <span class="proicon-sup proicmbT"></span> <strong>Support</strong>
                                        </div>
                                        <span id="querymsg" style="color:red;"></span>
                                        <div>
                                            <h4 class="prohead">Query</h4>
                                            <form class="form-horizontal mT20" role="form">
                                                <div class="form-group">
                                                    <div class="col-sm-7">
                                                        <textarea class="form-control" name="query" id="query" rows="6"></textarea>
                                                    </div>
                                                    <div class="col-sm-5 mT20">
                                                        <p class="supportcall">
                                                            <a href="mailto:listen@pay1.in"><span class="msgcall-msg"></span> listen@pay1.in</a>
                                                        </p>
                                                        <p class="supportcall mT20">
                                                            <a href="tel:+919009009000"><span class="msgcall-call"></span> Give us a Missed Call to Raise the Complaint of your last Transaction, on 02267242266
                                                            </a>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-8">
                                                        <input type="button" class="btn btn-primary" onclick="savequery();" value="Save">
                                                        <input type="button" class="btn btn-default" value="Cancel">
                                                    </div>
                                                </div>
                                            </form>


                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab5" class="tab-pane fade">
                                <div class="dealfullbox">
                                    <div class="dealboxx">
                                        <div class="dealsbread">
                                            <span class="proicon-faq proicmbT"></span> <strong>FAQ's</strong>
                                        </div>

                                        <div class="panel-group mT20" id="faq">

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab6" class="tab-pane fade">
                                <div class="dealfullbox">
                                    <div class="dealboxx">
                                        <div class="dealsbread">
                                            <span class="proicon-oterms proicmbT"></span> <strong>Our Terms</strong>
                                        </div>
                                        <div>
                                            <h4 class="prohead"></h4>
                                            <p class="text-justify" id="tnc">

                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <!-- footer box -->
        <?php include 'footer.php'; ?>
    </div><!-- //shiftbox -->
</div>
<!--<script src="js/jquery.min.2.1.1.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script src="js/jquery-ui-1.11.0/jquery-ui.js"></script>
<script src="js/jquery-ui-1.11.0/jquery-ui.min.js"></script>-->
<script type="text/javascript">

    $(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "images/cal.png",
            changeMonth: true,
            changeYear: true,
            buttonImageOnly: true,
            dateFormat: "yy-mm-dd",
            yearRange: "-100:+0"


        });
//       


    });



    function updateprofile()
    {
        var url = "<?php echo CDEV_URL ?>/index.php/api_new/action/api/true/actiontype/update_profile/?";
        var name = $("#name").val();
        var dob = $("#datepicker").val();
        var gender = $('input:radio[name=options]:checked').val();
        //alert(gender);
        var mobno = $("#mnumber").val();
        var accpin = $("#pin").val();
        var email = $("#email_id").val();
        var sessionemail = "<?php echo $_SESSION["email"]; ?>";
        if (sessionemail != email) {
            var data = {name: name,
                gender: gender,
                date_of_birth: dob,
                password: accpin,
                email: email,
                res_format: "jsonp"};
        }
        else {
            var data = {name: name,
                gender: gender,
                date_of_birth: dob,
                res_format: "jsonp"};
        }


        $.ajax({
            url: url,
            type: "GET",
            data: data,
            timeout: 50000,
            dataType: "jsonp",
            jsonpCallback: 'callback',
            crossDomain: true,
            success: function(data) {
                if (data.status == "success") {
                    setdatavalue();
                    $("#msg").html(data.description);
                }
                else
                {
                    $("#errormsg").html(data.description);
                }

                return false;


            },
            beforeSend: function() {
                $('.loader').show();
            },
            complete: function() {
                // Code to hide spinner.
                $('.loader').hide();
            },
            error: function(xhr, error) {
            },
        });
    }

    function getTransactionHistory(count)
    {

        var page = parseInt(count);
        if (page == "0") {
            $("#purchasehistory tbody").html('');
        }
        $("#purchasehistory tfoot").html('');
        var url = " <?php echo CDEV_URL ?>/index.php/api_new/action/api/true/actiontype/purchase_history/?";
        var html = "";
        var htmlcontent = "";
        $.ajax({
            url: url,
            type: "GET",
            timeout: 50000,
            dataType: "jsonp",
            jsonpCallback: 'callback',
            data: {
                page: page,
                res_format: "jsonp"
            },
            crossDomain: true,
            success: function(data) {
                if (data.status == "success")
                {
                    //  console.log(data);
                    $.each(data.description, function(key, val) {
                        if (val.operator_name == null)
                        {
                            var opt_name = "";
                        }
                        else
                        {
                            var opt_name = val.operator_name;
                        }

                        if (val.service_id == "1" || val.service_id == "2" || val.service_id == "3" || val.service_id == "5") {
                            var bodyclass = "mom";
                        }
                        else if (val.service_id == "4") {
                            var bodyclass = "postpaid";
                        }
                        else if (val.service_id == "99") {
                            var bodyclass = "dominos";
                        }
                        else if (val.service_id == "98" || val.service_id == "0") {
                            var bodyclass = "dominos";
                        }

                        if (val.service_id == "1") {
                            var classtype = "customicon-mobile";
                        }
                        else if (val.service_id == "2") {
                            var classtype = "customicon-dth";
                        }
                        else if (val.service_id == "3") {
                            var classtype = "customicon-dcable";
                        }
                        else if (val.service_id == "4") {
                            var classtype = "customicon-pmobile";
                        }
                        else if (val.service_id == "5") {
                            var classtype = "customicon-deals";
                        }
                        var transamount = "\'" + val.transaction_amount + "\'";
                        var operatorname = "\'" + val.operator_name + "\'";
                        var status = "\'" + val.status + "\'";
                        var transid = "\'" + val.transaction_id + "\'";
                        var transactionmode = "\'" + val.transaction_mode + "\'";
                        var transdatetime = "\'" + val.trans_datetime + "\'";
                        var number = "\'" + val.number + "\'";
                        var classname = (val.status == "2") ? "tickstatus" : "crosstatus";
                        html += '<tbody class=' + bodyclass + ' id =' + val.transaction_id + '><tr><th></th><th></th><th>&nbsp;</th><th>Status</th><th>&nbsp;</th></tr><tr><td><p>' + val.number + '</p><small>' + opt_name + '</small></td><td><span class=' + classtype + '></span></td><td>Rs.' + val.transaction_amount + '</td><td><span class=' + classname + '></span></td><td><span data-target=".ordersuccess1" data-toggle="modal"  onclick="viewdetails(' + operatorname + ',' + transamount + ',' + status + ',' + transid + ',' + transactionmode + ',' + transdatetime + ',' + number + ');"><a href="#">View Details</a></span></td></tr></tbody>'
                    });
                    page++;
                    if (data.description.length > 0 && data.description.length >= 10) {
                        html += '<tfoot><tr><td>&nbsp;</td><td><span id ="load" onclick="getTransactionHistory(' + page + ')" align="center" ><b><a href="#">Load more History</a></b></span></td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr></tfoot>';
                    }
                    html += "</table>";
                    $("#purchasehistory").append(html);

                }
                else if (data.status == "failure") {
                    window.location = "login.php"
                }
            },
            beforeSend: function() {
                // Code to display spinner
                $('.loader').show();
            },
            complete: function() {
                // Code to hide spinner.
                $('.loader').hide();
            },
            error: function(xhr, error) {
            },
        });

    }

    function getfaq(type)
    {
        var type = type;
        var serviceurl = "<?php echo CDEV_URL ?>/index.php/api_new/action/api/true/actiontype/get_faq/?";
        var html = "";

        $.ajax({
            url: serviceurl,
            type: "POST",
            dataType: "jsonp",
            data: {
                req: type,
                res_format: "jsonp"
            },
            success: function(data) {
                //console.log(data.description[0]);
                if (data.status == "success")
                {
                    $("#" + type).append(data.description[0]);

                }
                else if (data.status == "failure") {
                    window.location = "login.php?logout="
                }
            },
            beforeSend: function() {
                $('.loader').show();
            },
            complete: function() {
                $('.loader').hide();
            },
            error: function(xhr, error) {
                console.log(xhr);
                console.log(error);
            }
        });


    }

    function getquickpaylist(countlen)
    {
        //alert(countlen);
        $("#quickpay").html("");
        var url = "<?php echo CDEV_URL ?>/index.php/api_new/action/api/true/actiontype/get_quickpaylist/?";
        var html = "";

        $.ajax({
            url: url,
            type: "GET",
            timeout: 50000,
            dataType: "jsonp",
            jsonpCallback: 'callback',
            data: {
                res_format: "jsonp"
            },
            crossDomain: true,
            success: function(data) {
                if (data.status == "success")
                {
                    if (data.description.length > 0) {
                        html += "<table class='quicktable'>";
                        var i = 0;
                        var j = parseInt(countlen);
                        var count = data.description.length;
                        var numrows = j * 10;
                        $.each(data.description, function(key, val) {
                            if (i < numrows) {
                                var classname = (val.status == "2") ? "tickstatus" : "crosstatus";
                                if ((val.flag != "4") && (val.flag != "2")) {
                                    var reqclass = (val.delegate_flag == 0) ? "misscall_msg" : "quickpay-msg";
                                }
                                else {
                                    var reqclass = "";
                                }
                                var number = "\'" + val.number + "\'";
                                var amount = "\'" + val.amount + "\'";
                                var flag = "\'" + val.flag + "\'";
                                var name = "\'" + val.name + "\'";
                                var opreator = "\'" + val.operator_id + "\'";
                                var delegate_flag = "\'" + val.delegate_flag + "\'";
                                var confirmation = "\'" + val.confirmation + "\'";
                                var confirmation = "\'" + val.confirmation + "\'";
                                var id = "\'" + val.id + "\'";
                                var stv = "\'" + val.stv + "\'";
                                var missedcall = "\'" + val.missed_number + "\'";
                                var operatorname = "\'" + val.operator_name + "\'";
                                var operatorcode = "\'" + val.operator_code + "\'";
                                html += '<tbody class="momdad" id=' + i + '><tr><th><span>' + val.name + '</span></th>'
                                html += '<th>&nbsp;</th><th>&nbsp;</th><th>&nbsp;</th><th>&nbsp;</th></tr>'
                                html += '<tr><td><p>' + val.number + '</p><small>' + val.operator_name + '</small></td><td><span onclick="show(\'misscallpopup\');showpopdata(' + number + ',' + name + ',' + amount + ',' + opreator + ',' + delegate_flag + ',' + confirmation + ',' + flag + ',' + id + ',' + stv + ',' + missedcall + ')" class=' + reqclass + '></span</td>'
                                if (val.flag == "4") {
                                    html += '<td><strong></storng></td>'
                                    html += '<td><span class="quickpay-rup" data-toggle="modal"  data-target=".orderconfirmation" onclick="order(' + number + ',' + name + ',' + amount + ',' + opreator + ',' + delegate_flag + ',' + confirmation + ',' + flag + ',' + id + ',' + stv + ',' + missedcall + ',' + operatorname + ',\'postpaid\',' + operatorcode + ');"></span></td>'
                                }
                                else
                                {
                                    html += '<td><strong>Rs.' + val.amount + '</storng></td>'
                                    html += '<td><span class="quickpay-rup" data-toggle="modal"  data-target=".orderconfirmation" onclick="order(' + number + ',' + name + ',' + amount + ',' + opreator + ',' + delegate_flag + ',' + confirmation + ',' + flag + ',' + id + ',' + stv + ',' + missedcall + ',' + operatorname + ',\'other\',' + operatorcode + ');"></span></td>'
                                }
                                html += '<td><span class="quickpay-cross"   onclick="deletequickpay(' + id + ')"></span></td></tr></tbody>'
                                i++;

                            }
                        });
                        j++;
                        if (numrows >= 10) {
                            if (numrows <= count) {
                                html += '<tr><td>&nbsp;</td><td><span id ="load" onclick="getquickpaylist(' + j + ')" align="center" ><b><a href="#">Load more History</a></b></span></td><td>&nbsp;</td></tr>';

                          }
                        }
                        html += "</table>";
                    } else {
                        html = "";
                        html += "<table><h1>Looks like you have no quick payment set.</h1>";
                        html += "<h2>MAKE A RECHARGE</h2>";
                        html += "<div class='payment_ic'><a href='index.php?data'><img src='images/datacard.png' /><div class='ic_title'>Data card</div><a/></div>";
                        html += "<div class='payment_ic'><a href='index.php?dth'><img src='images/dth.png' /><div class='ic_title'>DTH</div></a></div>";
                        html += "<div class='payment_ic'><a href='index.php?mobile'><img src='images/mobile.png' /><div class='ic_title'>Mobile</div></a></div>"
                        html += "</table>";
                    }
                    $("#quickpay").append(html);
                    //html = '';
                }
                else if (data.errorcode == "201") {
                    window.location = "login.php?logout="
                }
            },
            beforeSend: function() {
                $('.loader').show();
            },
            complete: function() {
                $('.loader').hide();
            },
            error: function(xhr, error) {
            }

        });
    }

    function showpopdata(number, name, amount, operator, delegate_flag, confirmation, flag, id, stv, missedcall)
    {

        if (delegate_flag == "0")
        {
            $("#on").show();
            $("#off").hide();
            $("#on_number").html(number);
            $("#on_amt").html(amount);
        }
        else
        {
            $("#off").show();
            $("#on").hide();
            $("#off_number").html(number);
            $("#missed_number").html(missedcall);
        }
        $("#recharge_number").val(number);
        $("#recharge_name").val(name);
        $("#recharge_amount").val(amount);
        $("#recharge_operator").val(operator);
        $("#delegate_flag").val(delegate_flag);
        $("#confirmation").val(confirmation);

        $("#flag").val(flag);
        $("#id").val(id);
        $("#stv").val(stv);
        $("#missedcall").val(missedcall);

    }

    function submitrequest(number, name, amount, operator, delegate_flag, flag, id, stv, confirmation)
    {
        var delegateflag = $("#delegate_flag").val();
        if (delegateflag == "0") {
            var delegatevalue = $('input:radio[name=turn_on]:checked').val();
        }
        else {
            var delegatevalue = $('input:radio[name=req_on]:checked').val();
        }
        var number = $("#" + number).val();
        var name = $("#" + name).val();
        var amount = $("#" + amount).val();
        var operator = $("#" + operator).val();
        var flag = $("#" + flag).val();
        var id = $("#" + id).val();
        var stv = $("#" + stv).val();

        var url = "<?php echo CDEV_URL ?>/index.php/api_new/action/api/true/actiontype/edit_quickpay/?";
        var html = "";
        $.ajax({
            url: url,
            type: "GET",
            timeout: 50000,
            dataType: "jsonp",
            jsonpCallback: 'callback',
            data: {
                number: number,
                name: name,
                amount: amount,
                operator_id: operator,
                delegate_flag: delegatevalue,
                confirmation: confirmation,
                flag: flag,
                id: id,
                stv: stv,
                res_format: "jsonp"
            },
            crossDomain: true,
            success: function(data) {

                if (data.status == "success")
                {
                    getquickpaylist('1');
                    console.log(data);
                    $("#quickmsg").html(data.description.msg);
                }
                else if (data.status == "failure" && data.errCode == "241") {
                    getquickpaylist('1');
                    $("#quickmsg").html(data.description);

                }
                else if (data.errorcode == "201") {
                    window.location = "login.php?logout="
                }
            },
            beforeSend: function() {
                $('.loader').show();
            },
            complete: function() {
                // Code to hide spinner.
                $('.loader').hide();
            },
            error: function(xhr, error) {
            },
        });
    }


    function deletequickpay(id)
    {
        var confirmation = confirm("Are you sure you want to delete quickpay");
        if (confirmation == true) {
            var quick_pay_id = id;
            var url = "<?php echo CDEV_URL ?>/index.php/api_new/action/api/true/actiontype/del_quickpay/?";
            $.ajax({
                url: url,
                type: "GET",
                timeout: 50000,
                dataType: "jsonp",
                jsonpCallback: 'callback',
                data: {
                    id: quick_pay_id,
                    res_format: "jsonp"
                },
                crossDomain: true,
                success: function(data) {
                    if (data.status == "success")
                    {
                        getquickpaylist('1');
                        $("#quickmsg").html(data.description);
                    }
                    else if (data.status == "failure") {
                        window.location = "login.php?logout="
                    }
                },
                beforeSend: function() {
                    $('.loader').show();
                },
                complete: function() {
                    $('.loader').hide();
                },
                error: function(xhr, error) {
                    console.log(error);
                },
            });
        }
    }

    function order(number, name, amount, operator, delegate_flag, confirmation, flag, id, stv, missedcall, operatorname, type, operatorcode)
    {

        $("#service_msg").hide();
        var wallet_bal = "<?php echo $_SESSION["wallet_balance"] ?>";
        if (parseInt(wallet_bal) < parseInt(amount) || (parseInt(wallet_bal) == 0)) {
            $("#wallet").hide();
            $("#walletmsg").html("Insufficient balance in wallet");
        }
        else {
            $("#walletmsg").html("");
            $("#wallet").show();
        }
        if (type == "postpaid")
        {
            var operatorobj = JSON.parse(<?php echo json_encode($postpaiddata); ?>);
            var limit = JSON.parse(<?php echo json_encode($postpaidlimit); ?>);
            $("#service_charge_percent").val(limit[operatorcode][4]);
            $("#charge_slab").val(limit[operatorcode][3])
            $("#max_amount").val(limit[operatorcode][0]);
            $("#min_amount").val(limit[operatorcode][1]);
            $("#post_amount").show();
            $("#amount").hide();
            $("#postpaid_amount").val(amount);
        }

        else {
            $("#post_amount").hide();
            $("#amount").show();

        }

        $("#number").html(number);
        $("#amount").html(amount);
        $("#provider_name").html(operatorname);
        $("#rec_amount").val(amount);
        $("#rec_number").val(number);
        $("#rec_operator").val(operator);
        $("#rec_flag").val(flag);

    }

    function orderclick() {

        var amt = $("#postpaid_amount").val();
        var maxlimit = $("#max_amount").val();
        var minlimit = $("#min_amount").val();
        var charge = $("#charge_slab").val();
        var servicechrg = charge.split(",");
        var mincharge = servicechrg[0].split(":");
        var maxcharge = servicechrg[1].split(":");
        var service_charge_percent = $("#service_charge_percent").val();
        if (amt == '')
        {
            alert("Amount can not be blank");
            $("#postpaid_amount").focus();
            return false;
        }

        else if ((amt != '') && (amt > parseInt(maxlimit))) {
            alert("Amount can not be greater than  " + maxlimit);
            $("#postpaid_amount").focus();
            $("#postpaid_amount").val('');
            return false;
        }
        else if ((amt != '') && (amt < parseInt(minlimit))) {
            alert("Amount can not be less than  " + minlimit);
            $("#postpaid_amount").focus();
            $("#postpaid_amount").val('');
            return false;
        }
        else if ((amt) > (parseInt(mincharge[0]))) {
            var totalamt = (parseInt(amt) + parseInt(mincharge[1]) + parseInt(service_charge_percent));
            $("#service_charge").val(parseInt((mincharge[1])));
            $("#total_amount").val(totalamt);
            $("#rec_amount").val(amt);
            $("#post_amount").hide();
            $("#amount").show();
            $("#amount").html(totalamt);
            $("#service_msg").show();
            $("#service_msg").html("(" + parseInt(totalamt) + " =" + parseInt(amt) + "+" + $("#service_charge").val() + "    service charge inclusive of all taxes)");
        }
        else
        {
            var totalamt = (parseInt(amt) + parseInt(maxcharge[1]) + parseInt(service_charge_percent));
            $("#service_charge").val((maxcharge[1]));
            $("#total_amount").val(totalamt);
            $("#rec_amount").val(amt);
            $("#post_amount").hide();
            $("#amount").show();
            $("#amount").html(totalamt);
            $("#service_msg").show();
            $("#service_msg").html("(" + parseInt(totalamt) + " =" + parseInt(amt) + "+" + $("#service_charge").val() + "    service charge inclusive of all taxes)");
        }

        //modal fade in orderconfirmation


    }


    function payment(amt, opt, flag, number, option)
    {
        var amt = $("#" + amt).val();
        var operator = $("#" + opt).val();
        var flag = $("#" + flag).val();
        var number = $("#" + number).val();
        var option = option;

        if (parseInt(amt) == 0)
        {
            alert("Please Enter Amount");
            $("#" + amt).val('');
            return false;
        }

        if (option == "Payment Gateway")
        {
            $("#payment_option").val('online');
        }
        else
        {
            $("#payment_option").val('wallet');
        }
        var paymentmode = $("#payment_option").val();
        var paymenturl = "<?php echo "http://" . $_SERVER["SERVER_NAME"] . "/success.php" ?>";
        var provider_name = $("#provider_name").html();
        var type = $("#recharge_type").html();

        if (flag == "1" || flag == "3") {
            var url = "<?php echo CDEV_URL ?>/index.php/api_new/action/api/true/actiontype/recharge/?";
            $.ajax({
                url: url,
                type: "GET",
                data: {mobile_number: number,
                    operator: operator,
                    flag: flag,
                    amount: amt,
                    recharge: "1",
                    paymentopt: paymentmode,
                    payment: "1",
                    return_url: paymenturl,
                    res_format: "jsonp"
                },
                timeout: 50000,
                dataType: "jsonp",
                jsonpCallback: 'callback',
                crossDomain: true,
                success: function(data) {
                    if (data.status == "success") {
                        localStorage.setItem("amount", amt);
                        localStorage.setItem("number", number);
                        localStorage.setItem("provider", provider_name);
                        localStorage.setItem("type", type);
                        if (paymentmode == "wallet") {
                            window.location = "success.php?status=" + data.status + "&txnid=" + data.description.transaction_id + "&balance=" + data.description.closing_balance
                        }
                        else {
                            localStorage.setItem("data", data.description.form_content);
                            var url = "content.php?data=";
                            window.location.href = encodeURI(url);
                        }
                    }
                    else if (data.errorCode == "201") {
                        window.location = "login.php?logout="
                    }

                },
                beforeSend: function() {
                    $("#orderconfirmation").hide();
                    $('.loader').show();
                },
                complete: function() {
                    $('.loader').hide();
                },
                error: function(xhr, error) {

                },
            });
        }

        else if (flag == "2") {
            var url = "<?php echo CDEV_URL ?>/index.php/api_new/action/api/true/actiontype/recharge/?";
            $.ajax({
                url: url,
                type: "GET",
                data: {subscriber_id: number,
                    operator: operator,
                    flag: flag,
                    amount: amt,
                    recharge: "1",
                    paymentopt: paymentmode,
                    payment: "1",
                    return_url: paymenturl,
                    res_format: "jsonp"
                },
                timeout: 50000,
                dataType: "jsonp",
                jsonpCallback: 'callback',
                crossDomain: true,
                success: function(data) {
                    if (data.status == "success") {
                        if (paymentmode == "wallet") {
                            localStorage.setItem("amount", amt);
                            localStorage.setItem("number", number);
                            localStorage.setItem("provider", provider_name);
                            localStorage.setItem("type", type);
                            window.location = "success.php?status=" + data.status + "&txnid=" + data.description.transaction_id + "&balance=" + data.description.closing_balance
                        }
                        else {
                            localStorage.setItem("data", data.description.form_content);
                            var url = "content.php?data=";
                            window.location.href = encodeURI(url);
                        }
                    }
                    else if (data.errorCode == "201") {
                        window.location = "login.php?logout="
                    }
                },
                beforeSend: function() {
                    $("#orderconfirmation").hide();
                    $('.loader').show();
                },
                complete: function() {
                    $('.loader').hide();
                },
                error: function(xhr, error) {

                },
            });
        }
        else if (flag == "4")
        {
            var url = "<?php echo CDEV_URL ?>/index.php/api_new/action/api/true/actiontype/recharge/?";
            var service_charge = $("#service_charge").val();
            var totalamt = $("#total_amount").val();
            var service_charge_percent = $("#service_charge_percent").val();
            $.ajax({
                url: url,
                type: "GET",
                data: {mobile_number: number,
                    operator: operator,
                    flag: flag,
                    base_amount: amt,
                    service_charge: service_charge,
                    amount: totalamt,
                    payment_flag: "1",
                    service_tax: service_charge_percent,
                    billpayment: "1",
                    paymentopt: paymentmode,
                    payment: "1",
                    return_url: paymenturl,
                    res_format: "jsonp"
                },
                timeout: 50000,
                dataType: "jsonp",
                jsonpCallback: 'callback',
                crossDomain: true,
                success: function(data) {
                    if (data.status == "success") {
                        localStorage.setItem("amount", totalamt);
                        localStorage.setItem("number", number);
                        localStorage.setItem("provider", provider_name);
                        localStorage.setItem("type", type);
                        if (paymentmode == "wallet") {
                            window.location = "success.php?status=" + data.status + "&txnid=" + data.description.transaction_id + "&balance=" + data.description.closing_balance
                        }
                        else {
                            localStorage.setItem("data", data.description.form_content);
                            var url = "content.php?data=";
                            window.location.href = encodeURI(url);
                        }
                    } else if (data.errorCode == "201") {
                        window.location = "login.php?logout="

                    }

                },
                beforeSend: function() {
                    $("#orderconfirmation").hide();
                    $('.loader').show();
                },
                complete: function() {
                    $('.loader').hide();
                },
                error: function(xhr, error) {

                },
            });
        }

    }
    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : evt.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true
    }


    function setdatavalue()
    {
        var name = $("#name").val();
        var dob = $("#datepicker").val();
        var gender = $('input:radio[name=options]:checked').val();
        var mobno = $("#mnumber").val();
        var accpin = $("#pin").val();
        var email = $("#email_id").val();

        $.ajax({
            url: "info.php",
            type: "POST",
            data: {
                name: name,
                email: email,
                dob: dob,
                username: mobno,
                gender: gender
            },
            timeout: 50000,
            dataType: "json",
            success: function(data) {

                if (data.status == "success")
                {
                    $("#msg").html(data.description);
                }

            },
            error: function(error) {


            }
        });

    }

    function changepin()
    {
        var mobileno = localStorage.getItem("mob_number");
        var otp = $("#otp").val();
        var newpin = $("#new_pin").val();
        var confirmpin = $("#confirm_pin").val();

        if (otp == '')
        {
            alert("Please enter a OTP.");
            return false;
        }
        else if (newpin == '')
        {
            alert("Please enter new pin");
            return false;
        }
        else if (confirmpin == '')
        {
            alert("please enter confirm pin");
            return false;
        }

        else if (newpin != confirmpin)
        {
            alert("Password does not match");
            return false;
        }

        else {
            var url = "<?php echo CDEV_URL ?>/index.php/api_new/action/api/true/actiontype/update_password/?";
            $.ajax({
                url: url,
                type: "GET",
                timeout: 50000,
                dataType: "jsonp",
                jsonpCallback: 'callback',
                crossDomain: true,
//                data: {mobile_number: mobileno,
//                    password: newpin,
//                    confirm_password: confirmpin,
//                    current_password: currentpin,
//                    res_format: "jsonp"
//                },
                data:{mobile_number:mobileno,
                    password:newpin,
                    confirm_password:confirmpin,
                    code:otp,
                    vtype:"1",
                    res_format : "jsonp"
                 },
                success: function(data) {
                    if (data.status == "success")
                    {
                        $("#update_msg").html(data.description);
                    } else {
                        
                        $("#update_msg").html(data.description);
                    }

                }
            });
        }
    }
    
    function updatePassword()
    {
          var mobile = $("#mobile_number").val();
          var url = "<?Php echo CDEV_URL; ?>/index.php/api_new/action/api/true/actiontype/forgotpwd/?";
            $.ajax({
            url: url,
            type:"GET",
            data:{
                  user_name:mobile,
                  vtype:"1",
                  res_format : "jsonp"
                 },
            timeout: 50000,
            dataType: "jsonp",
            jsonpCallback: 'callback',
            crossDomain: true,
            success:function(data){
                
               if(data.status=="success")
               {
                   
                   localStorage.setItem("mob_number",mobile);
                   $("#updatepassword").hide();
                   $("#changepin").show();
                   $("#update_msg").html(data.description);
                   //window.location = "update_password.php"
               }
               else {
                    $("#update_msg").html(data.description);
               }
            },
            error: function (xhr,error) {
             },  
         
            });
    }

    function savequery()
    {
        var url = "<?php echo CDEV_URL ?>/index.php/api_new/action/api/true/actiontype/support_req/?";
        var mobileno = "<?php echo $_SESSION["mobile"]; ?>";
        var message = $("#query").val();
        if (message == '') {
            alert("Please Enter Query");
            $("#query").focus();
        }
        else {

            $.ajax({
                url: url,
                type: "GET",
                data: {mobile_number: mobileno,
                    message: message,
                    res_format: "jsonp"
                },
                timeout: 50000,
                dataType: "jsonp",
                jsonpCallback: 'callback',
                crossDomain: true,
                success: function(data) {

                    if (data.status == "success")
                    {
                        $("#querymsg").html("Query submitted successfully!");
                    }

                }
            });
        }
    }

    function updatebalance() {
        var url = "<?php echo CDEV_URL ?>/index.php/api_new/action/api/true/actiontype/check_bal";
        $.ajax({
            url: url,
            type: "GET",
            data: {
                res_format: "jsonp"
            },
            timeout: 50000,
            dataType: "jsonp",
            jsonpCallback: 'callback',
            crossDomain: true,
            success: function(data) {
                if (data.status == "success") {
                    $.ajax({
                        url: "info.php",
                        type: "POST",
                        data: {
                            walletbal: data.description.account_balance,
                        },
                        timeout: 50000,
                        dataType: "json",
                        success: function(data) {
                            if (data.status == "success")
                            {
                                location.reload();
                            }
                        },
                        error: function(error) {
                        }
                    });

                }
                else
                {
                    $("#couponcode").html(data.description);
                }
            }
        });
    }


    $("ul.nav-tabs1 > li > a").on("shown.bs.tab", function(e) {
        var id = $(e.target).attr("href").substr(1);
        window.location.hash = id;
    });

    $(document).ready(function()
    {


        var loc = location.search.substr(1);

        if (window.location.hash == "#tab3") {
            getquickpaylist('1');
            $('#myTab a[href="' + window.location.hash + '"]').tab('show');
        }
        else if (window.location.hash == "#tab2") {
            getTransactionHistory('0');
            $('#myTab a[href="' + window.location.hash + '"]').tab('show');
        }
        else if (window.location.hash == "#tab1") {
            //getTransactionHistory();
            $('#myTab a[href="' + window.location.hash + '"]').tab('show');
        }
        else if (window.location.hash == "#tab4") {
            //getTransactionHistory();
            $('#myTab a[href="' + window.location.hash + '"]').tab('show');
        }
        else if (window.location.hash == "#tab5") {
            getfaq('faq');
            $('#myTab a[href="' + window.location.hash + '"]').tab('show');
        }
        else if (window.location.hash == "#tab6") {
            //getTransactionHistory();
            getfaq('tnc');
            $('#myTab a[href="' + window.location.hash + '"]').tab('show');
        }
        else if (loc == "quickpay")
        {
            getquickpaylist('1');
            window.location.hash = "tab3";
            $('#myTab a[href="' + window.location.hash + '"]').tab('show');
        }

        var url = "<?php echo CDEV_URL ?>/index.php/api_new/action/api/true/actiontype/check_bal";
        $.ajax({
            url: url,
            type: "GET",
            data: {
                res_format: "jsonp"
            },
            timeout: 50000,
            dataType: "jsonp",
            crossDomain: true,
            success: function(data) {
                if (data.status == "success") {
                    var walletbal = data.description.account_balance;
                    var sessionwalletbal = "<?php echo $_SESSION["wallet_balance"]; ?>";
                    if (walletbal != sessionwalletbal) {
                        $.ajax({
                            url: "info.php",
                            type: "POST",
                            data: {
                                walletbal: walletbal,
                            },
                            timeout: 50000,
                            dataType: "json",
                            success: function(data) {
                                if (data.status == "success")
                                {
                                    location.reload();
                                }
                            },
                            error: function(error) {
                            }
                        });
                    }
                }
                else
                {
                    window.location = "login.php?logout=";
                }
            }
        });

    });

    function viewdetails(operatorname, transamount, status, transid, transactionmode, transdatetime, number)
    {
        //alert(status);
        if (status == "2") {
            $("#trans_status").html("Success");
        }
        else {

            $("#trans_status").html("failed");
        }

        $("#trans_number").html(number);
        $("#trans_amount").html("Rs." + transamount);
        //$("#trans_status").html(status);
        $("#trans_operator").html(operatorname);
        $("#trans_id").html(transid);
        $("#trans_mode").html(transactionmode);
        $("#trans_date").html(transdatetime);
    }


</script>

</body>
</html>