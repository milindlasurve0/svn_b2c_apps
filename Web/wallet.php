<?php
session_start();
if ($_SESSION["flag"] != "true") {

    header("location:login.php?logout=logout");
}


include 'config.php';
include 'common.php';

if (isset($_POST["lat"]) && $_POST["lat"] != '') {
    $lat = $_POST["lat"];
} elseif (!isset($_SESSION["latitude"]) && $_SESSION["latitude"] == '') {
    $lat = "0";
} else {
    $lat = $_SESSION["latitude"];
}
if (isset($_POST["lon"]) && $_POST["lon"] != '') {
    $lon = $_POST["lon"];
} elseif (!isset($_SESSION["longitude"]) && $_SESSION["longitude"] == '') {
    $lon = "0";
} else {
    $lon = $_SESSION["longitude"];
}

$service_url = PANEL_URL . "/apis/receiveWeb/mindsarray/mindsarray/json?method=getNearByRetailer&lat=$lat&lng=$lon";
$curl_response = getCurlAjaxRespose($service_url);
$curl_response = trim($curl_response);
$curl_response = trim($curl_response, ")");
$curl_response = trim($curl_response, "(");
$curl_response = trim($curl_response, ";");
$curl_response = substr_replace($curl_response, "", -1);
$response = array();
$curl_response = json_decode($curl_response);
$curl_response = objectToArray($curl_response);

foreach ($curl_response as $key => $val) {
    foreach ($val as $k => $v) {
        foreach ($v as $datakey => $dataval) {
            $response[$dataval["id"]] = $dataval;
        }
    }
}
$dealerlocation = json_encode($response);


//if(isset($_POST['shop_id']) && !empty($_POST['shop_id'])){
//    
//                $url = CDEV_URL."/index.php/api_new/action/api/true/actiontype/get_deal_details/?id=$deal_id&latitude=$lat&longitude=$lon";
//                $response = getCurlAjaxRespose($url);
//                $response = trim($response);
//                $response = objectToArray($response);
//                
//}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Wallet || PAY1</title>
        <meta charset="utf-8">  
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,width=device-width,user-scalable=no" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link rel="stylesheet" type="text/css" href="css/mobile-tablet-style.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div>
            <!-- navigation -->
            <div class="mobilemenu">
                <p class="mobiletoggle" data-idd="#mobnav"></p>
                <div class="walletTOP">
                    <ul class="nav nav-pills dealtoplist">
                        <li>
                            <span class="dropdown-toggle" data-toggle="dropdown">
                                <table>
                                    <tr>
                                        <td><span class="deals-balance"></span></td>
                                        <td>Balance<br> <strong>60,000 <span class="caret"></span></strong></td>
                                    </tr>
                                </table>
                            </span>
                            <ul class="dropdown-menu" role="menu">
                                <li>1,00,000</li>
                                <li>2,00,000</li>
                                <li>3,00,000</li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <a href="index1.html" title="Pay1"><img src="images/logo.jpg" align="logo-image"></a>
            </div>
            <div class="opacitybg"></div>
            <?php include 'left.php'; ?>

            <!-- //close navigation -->
            <div class="shiftbox">
                <div class="hutpart">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <?php
                                include 'header.php';
                                include 'analyticstracking.php'
                                ?>

                            </div>
                        </div>
                        <div class="row mT20">
                            <div class="col-sm-3">
                                <ul id="myTab" class="nav nav-tabs1 walletlist" role="tablist">
                                    <li class="active">
                                        <a href="#collapsnav1" data-toggle="collapse" data-parent="#proaccordion1">
                                            <span class="proicon-profile"></span> Top Up Wallet</a>
                                        <ul id="collapsnav1" class="wallseumenu panel-collapse collapse in">
                                            <li><a data-toggle="tab" role="tab" href="#tab1">Dealer Locator</a></li>
                                            <li><a data-toggle="tab" role="tab" href="#tab2">Debit/Credit Card</a></li>
                                            <li><a data-toggle="tab" role="tab" href="#tab3">Coupon Code</a></li>
                                        </ul>
                                    </li>
                                    <li class="">
                                        <a data-toggle="tab" role="tab"  href="#tab4" onclick="getWalletHistory('0');">
                                            <span class="proicon-qpay"></span> Wallet History
                                        </a>
                                    </li>
                                </ul>
                            </div>

                            <div class="col-sm-9">
                                <div id="ListTabContent" class="tab-content clearfix">
                                    <div id="tab1" class="tab-pane fade active in">
                                        <div class="dealfullbox">
                                            <div class="dealboxx">
                                                <div class="dealsbread">
                                                    <span class="proicon-profile proicmbT"></span> <strong>Top Up Wallet</strong>
                                                </div>
                                                <div>
                                                    <h4 class="prohead" style="position:relative;"><?php if($_POST["shop_id"]!=''){ ?>Freebie Locator<?php  } else { ?>Dealer Locator<?php } ?> <span class="querysupport pull-right" data-toggle="collapse" data-parent="#mapaccordion" data-target="#query1"></span>
                                                    </h4>
                                                    <div class="panel-group" id="mapaccordion">
                                                        <div class="panel panel-default">
                                                            <div id="query1" class="panel-collapse collapse in">
                                                                <div class="panel-body" id="map_canvas"  style="height: 250px;">

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- /// -->
                                                    <form method="post" action="">
                                                        <div class="table-responsive" id="dealloactor">

                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>



                                    <div id="tab2" class="tab-pane fade">
                                        <div class="dealfullbox">
                                            <div class="dealboxx">
                                                <div class="dealsbread">
                                                    <span class="proicon-sup proicmbT"></span> <strong>Top Up Wallet</strong>
                                                </div>
                                                <div>
                                                    <h4 class="prohead">Debit/Credit Card</h4>
                                                    <form class="form-horizontal mT20" role="form">
                                                        <div class="form-group">
                                                            <div class="col-sm-7">
                                                                <span>Enter Amount</span>
                                                                <input type="text" name="amount" id="amount" class="form-control mT10">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-sm-8">
                                                                <input type="button" value="Make Payment" onclick="Wallettopup();" class="btn btn-primary">
                                                            </div>
                                                        </div>
                                                    </form>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="loader">
                                        <center>
                                            <img class="loading-image" src="images/ajax-loader.gif" alt="loading..">
                                        </center>
                                    </div>

                                    <div id="tab3" class="tab-pane fade">
                                        <div class="dealfullbox">
                                            <div class="dealboxx">
                                                <div class="dealsbread">
                                                    <span class="proicon-sup proicmbT"></span> <strong>Top Up Wallet</strong>
                                                </div>
                                                <div>
                                                    <span style="color:red" id="couponcode"></span>
                                                    <h4 class="prohead">Coupon Code</h4>
                                                    <form class="form-horizontal mT20" role="form">
                                                        <div class="form-group">
                                                            <div class="col-sm-7">
                                                                <span>Enter Coupon Code</span>
                                                                <input type="text" id="coupon_code" name="coupon_code" class="form-control mT10">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-sm-8">
                                                                <input type="button" value="Reedem" onclick="redeemcode()" class="btn btn-primary">
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!--  -->
                                    <div id="tab4" class="tab-pane fade">
                                        <div class="dealfullbox">
                                            <div class="dealboxx">
                                                <div class="dealsbread">
                                                    <span class="proicon-phistory proicmbT"></span> <strong>Wallet History</strong>
                                                </div>
                                                <div>
                                                    <div class="table-responsive">

                                                        <table class='wallhistorytable mT10' id="wallethistory">
                                                            <thead>
                                                                <tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td><small>Closing Balance</small></td></tr>
                                                            </thead>
                                                            <tbody>

                                                            </tbody>
                                                            <tfoot>

                                                            </tfoot>
                                                        </table>


                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <!-- footer box -->
<?php include 'footer.php'; ?>

            </div><!-- //shiftbox -->
        </div>

        <script>
            function getWalletHistory(count) {
                var url = "<?php echo CDEV_URL; ?>/index.php/api_new/action/api/true/actiontype/wallet_history/?";
                var page = parseInt(count);
                if (page == "0") {
                    $("#wallethistory tbody").html('');
                }
                var j = parseInt(count) * 10;
                $("#wallethistory tfoot").html('');
                var htmlcontent = "";

                var html = "";
                $.ajax({
                    url: url,
                    type: "GET",
                    timeout: 50000,
                    dataType: "jsonp",
                    data: {
                        page: page,
                        res_format: "jsonp"
                    },
                    crossDomain: true,
                    success: function(data) {
                        //console.log(data);
                        if (data.status == "success")
                        {
                            $.each(data.description, function(key, val) {
                                var transactiontype = val.trans_category;
                                transactiontype = transactiontype.indexOf("refund");
                                if (transactiontype == -1) {
                                    var classname = "red";
                                    var transtype = "Debited";
                                }
                                else {
                                    var classname = "green";
                                    var transtype = "Credited";
                                }
                                html += '<tr id=' + j + '><td ><strong>' + val.number + '</strong><span>' + val.trans_datetime + '</span><br/><span>' + val.operator_name + '</span></td><td>Rs.' + val.transaction_amount + '</td><td class=' + classname + '>' + transtype + '</td><td class=' + classname + '>' + val.closing_bal + '</td></tr>'
                                j++;

                            });
                            page++;
                            htmlcontent += '<tr><td>&nbsp;</td><td>&nbsp;</td><td><span id ="load" onclick="getWalletHistory(' + page + ')"  ><b><a href="#">Load more History</a></b></span></td><td>&nbsp;</td><td>&nbsp;</td></tr>';
                            $("#wallethistory tbody").append(html);
                            if (data.description.length > 0 && data.description.length >= 10) {
                                $("#wallethistory tfoot").append(htmlcontent);
                            }

                            //var id =  $("#wallethistory").find("tbody>div:last").attr("id");

                        }
                    },
                    beforeSend: function() {
                        $('.loader').show();
                    },
                    complete: function() {
                        $('.loader').hide();
                    },
                    error: function(xhr, error) {
                        //console.log(error);
                  },
                });

            }

            function Wallettopup()
            {
                var amount = $("#amount").val();
                var paymenturl = "<?php echo "http://" . $_SERVER["SERVER_NAME"] . "/success.php" ?>";
                if (amount == "")
                {
                    alert("Please Enter Amount");
                    $("#amount").focus();
                    return false;
                }
                else
                {
                    var url = "<?php echo CDEV_URL; ?>/index.php/api_new/action/api/true/actiontype/online_walletrefill/?";
                    $.ajax({
                        url: url,
                        type: "GET",
                        data: {
                            amount: amount,
                            return_url: paymenturl,
                            res_format: "jsonp"
                        },
                        timeout: 50000,
                        dataType: "jsonp",
                        jsonpCallback: 'callback',
                        crossDomain: true,
                        success: function(data) {

                            if (data.status == "success") {
                                localStorage.setItem("amount", amount);
                                localStorage.setItem("data", data.description.form_content);
                                var url = "content.php?data=";
                                window.location.href = encodeURI(url);
                            }
                        },
                        beforeSend: function() {
                            $('.loader').show();
                        },
                        complete: function() {
                            $('.loader').hide();
                        }
                    });

                }
            }



            function redeemcode()
            {

                var couponcode = $("#coupon_code").val();
                if (couponcode == "")
                {
                    alert("Please Enter Coupon code");
                    $("#coupon_code").focus();
                    return false;
                }
                else
                {
                    var url = "<?php echo CDEV_URL; ?>/index.php/api_new/action/api/true/actiontype/redeem_voucher/?";
                    $.ajax({
                        url: url,
                        type: "GET",
                        data: {
                            vcode: couponcode,
                            res_format: "jsonp"
                        },
                        timeout: 50000,
                        dataType: "jsonp",
                        jsonpCallback: 'callback',
                        crossDomain: true,
                        success: function(data) {
                            if (data.status == "success") {
                                $("#couponcode").html(data.description);
                                checkbalance();
                            }
                            else
                            {
                                $("#couponcode").html(data.description);
                            }
                        },
                        beforeSend: function() {
                            $('.loader').show();
                        },
                        complete: function() {
                            $('.loader').hide();
                        }
                    });

                }

            }

            function checkbalance()
            {
                var url = "<?php echo CDEV_URL; ?>/index.php/api_new/action/api/true/actiontype/check_bal";
                $.ajax({
                    url: url,
                    type: "GET",
                    data: {
                        res_format: "jsonp"
                    },
                    timeout: 50000,
                    dataType: "jsonp",
                    jsonpCallback: 'callback',
                    crossDomain: true,
                    success: function(data) {
                        if (data.status == "success") {
                            $("#couponcode").html(data.description);
                            $.ajax({
                                url: "info.php",
                                type: "POST",
                                data: {
                                    walletbal: data.description.account_balance,
                                },
                                timeout: 50000,
                                dataType: "json",
                                success: function(data) {
                                    if (data.status == "success")
                                    {
                                        location.reload();
                                    }
                                },
                                error: function(error) {
                                }
                            });

                        }
                        else
                        {
                            $("#couponcode").html(data.description);
                        }
                    }
                });

            }
        </script>

        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false" type="text/javascript"></script>
        <script>
            $(function() {
                var myArray = JSON.parse(<?php echo json_encode($dealerlocation); ?>);
                var directionsDisplay;
                var directionsService = new google.maps.DirectionsService();
                var map;
                var elevator;
                var marker;
                var infowindow = new google.maps.InfoWindow();
                <?php if ($_POST["lat"] == '') { ?>
                                    var lat = "<?php echo $_SESSION["latitude"] ?>";
                <?php } else { ?>
                                    var lat = "<?php echo $_POST["lat"]; ?>";
                <?php } if ($_POST["lon"] == '') { ?>
                                    var long = "<?php echo $_SESSION["longitude"] ?>";
                <?php } else { ?>
                                    var long = "<?php echo $_POST["lon"]; ?>";
                <?php } ?>

                var html = "";
                directionsDisplay = new google.maps.DirectionsRenderer();
                var myOptions = {
                    zoom: 14,
                    center: new google.maps.LatLng(lat, long),
                    mapTypeControl: false,
                    navigationControlOptions: {style: google.maps.NavigationControlStyle.SMALL},
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                };
                map = new google.maps.Map($('#map_canvas')[0], myOptions);
                directionsDisplay.setMap(map);
                var deal_id = "<?php echo $_POST['deal_id'] ?>";
                var shop_id = "<?php echo $_POST['shop_id'] ?>";
                if (deal_id == '' && shop_id=='') {
                    html += "<input type='hidden' name='lat' id='lat' value=''><input type='hidden' name='deal_id' id='deal_id' value=''><input type='hidden' id='long' name='long' value=''><table class='upwallettable'>";
                    $.each(myArray, function(index, element) {
                        var latlng = new google.maps.LatLng(element.latitude, element.longitude);
                        var latitude = "\'" + element.latitude + "\'";
                        var longitude = "\'" + element.longitude + "\'";
                        var id = "\'" + element.id + "\'";
                        html += '<tr><td><strong><a href="#" onclick="getdeals(' + latitude + ',' + longitude + ',' + id + ')">' + element.shopname + '</strong><span>' + element.area_name + '</span></a></td>';
                        if (element.D > 1) {
                            html += '<td><strong>' + Number(element.D).toFixed(2) + 'Km</strong><span></span></td>';
                        } else {
                            html += '<td><strong>' + Number(element.D).toFixed(2) + 'm</strong><span></span></td>';
                        }
                        html += "</tr>"
                        html += '<tr id=' + id + ' style="display:none"><td ><strong>' + element.shopname + '<br/>' + element.address + '<br/>' + element.area_name + '<br/>' + element.city_name + '<br/>' + element.state_name + '<br/>' + element.mobile + '</strong></td><td></td></tr>';
                        marker = new google.maps.Marker({
                            position: latlng,
                            map: map,
                            title: element.address

                        });
                        google.maps.event.addListener(marker, 'click', function() {
                            infowindow.setContent(element.shopname + "<br/>" + element.area_name + "<br/>" + element.city_name);
                            infowindow.open(map, this);
                        });
                    });
                    html += "</table>";
                    $("#dealloactor").append(html);
                }
                else if(shop_id!=''){
                
                var shopurl = "<?php echo CDEV_URL; ?>/index.php/api_new/action/api/true/actiontype/get_deal_details/?";
                var latitude = "<?php echo $_POST['lat'] ?>";
                var longitude = "<?php echo $_POST['long'] ?>";
                var shop_id = "<?php echo $_POST['shop_id'] ?>";
                
                 $.ajax({
                        url: shopurl,
                        type: "GET",
                        timeout: 50000,
                        data:{
                        id: shop_id,  
                        latitude : latitude,
                        longitude: longitude,
                        res_format : "jsonp"
                        },
                        dataType: "jsonp",
                        crossDomain: true,
                        success: function(data) {
                        var dataarray = data.description.location_detail[0];
                        //console.log(dataarray);
                        var latlng = new google.maps.LatLng(dataarray.lat, dataarray.lng);
                        html+="<table class='upwallettable'><tr><td ><strong>"+dataarray.address +"<br/>" + dataarray.city + "<br/>" + dataarray.state + "<br/></strong></td><td></td>";
                        html+= "</tr>"
                        html+="</table>";
                        marker = new google.maps.Marker({
                            position: latlng,
                            map: map,
                            title: dataarray.address

                        });
                        google.maps.event.addListener(marker, 'click', function() {
                            infowindow.setContent(dataarray.address + "<br/>" + dataarray.city + "<br/>" + dataarray.state);
                            infowindow.open(map, this);
                        });
                      $("#dealloactor").append(html);
                      var latilng = localStorage.getItem("latitude") + ',' + localStorage.getItem("longitude");
                       var address = "http://maps.googleapis.com/maps/api/geocode/json?latlng=" + latilng;
                    $.ajax({
                        url: address,
                        type: "GET",
                        timeout: 50000,
                        dataType: "json",
                        crossDomain: true,
                        success: function(data) {
                            var start = data.results[0].formatted_address;
                            var end = dataarray.address + "," + dataarray.state + "," + dataarray.city;
                            var request = {
                                origin: start,
                                destination: end,
                                travelMode: google.maps.TravelMode.DRIVING
                            };
                            directionsService.route(request, function(response, status) {
                                if (status == google.maps.DirectionsStatus.OK) {
                                    directionsDisplay.setDirections(response);
                                }
                            });
                        }
                    });
                      }
                     });
                
                }
                else {
                
                    var dataarray = myArray[deal_id];
                    html += "<input type='hidden' name='lat' id='lat' value=''><input type='hidden' name='deal_id' id='deal_id' value=''><input type='hidden' id='long' name='long' value=''><table class='upwallettable'>";
                    var latlng = new google.maps.LatLng(dataarray.latitude, dataarray.longitude);
                    var latitude = "\'" + dataarray.latitude + "\'";
                    var longitude = "\'" + dataarray.longitude + "\'";
                    var id = "\'" + dataarray.id + "\'";
                    html += '<tr><td><strong><a href="#" onclick="getdeals(' + latitude + ',' + longitude + ',' + id + ')">' + dataarray.shopname + '</strong><span>' + dataarray.area_name + '</span></a></td>';
//                    if (dataarray.D > 1) {
//                        html += '<td><strong>' + Number(dataarray.D).toFixed(2) + 'Km</strong><span></span></td>';
//                    } else {
//                        html += '<td><strong>' + Number(dataarray.D).toFixed(2) + 'm</strong><span></span></td>';
//                    }
                    html += "</tr>"
                    html += '<tr id=' + id + '><td ><strong>' + dataarray.shopname + '<br/>' + dataarray.address + '<br/>' + dataarray.area_name + '<br/>' + dataarray.city_name + '<br/>' + dataarray.state_name + '<br/>' + dataarray.mobile + '</strong></td><td></td></tr>';
                    var latlng = localStorage.getItem("latitude") + ',' + localStorage.getItem("longitude");
                    var address = "http://maps.googleapis.com/maps/api/geocode/json?latlng=" + latlng;
                    $.ajax({
                        url: address,
                        type: "GET",
                        timeout: 50000,
                        dataType: "json",
                        crossDomain: true,
                        success: function(data) {
                            var start = data.results[0].formatted_address;
                            var end = dataarray.address + "," + dataarray.area_name + "," + dataarray.city_name;
                            var request = {
                                origin: start,
                                destination: end,
                                travelMode: google.maps.TravelMode.DRIVING
                            };
                            directionsService.route(request, function(response, status) {
                                if (status == google.maps.DirectionsStatus.OK) {
                                    directionsDisplay.setDirections(response);
                                }
                            });
                        }
                    });

                    html += "</table>";
                    $("#dealloactor").append(html);

                }


                var url = "<?php echo CDEV_URL ?>/index.php/api_new/action/api/true/actiontype/check_bal";
                $.ajax({
                    url: url,
                    type: "GET",
                    data: {
                        res_format: "jsonp"
                    },
                    timeout: 50000,
                    dataType: "jsonp",
                    crossDomain: true,
                    success: function(data) {
                        if (data.status == "success") {
                            var walletbal = data.description.account_balance;
                            var sessionwalletbal = "<?php echo $_SESSION["wallet_balance"]; ?>";
                            if (walletbal != sessionwalletbal) {
                                $.ajax({
                                    url: "info.php",
                                    type: "POST",
                                    data: {
                                        walletbal: walletbal,
                                    },
                                    timeout: 50000,
                                    dataType: "json",
                                    success: function(data) {
                                        if (data.status == "success")
                                        {
                                            location.reload();
                                        }
                                    },
                                    error: function(error) {
                                    }
                                });
                            }
                        }
                        else
                        {
                            window.location = "login.php?logout=";
                        }
                    }
                });
            });

        </script>
        <script type="text/javascript">
            function getdeals(lat, lon, id)
            {
                $("#lat").val(lat);
                $("#deal_id").val(id);
                $("#long").val(lon);
                document.forms[0].action = "wallet.php";
                document.forms[0].submit();
            }
        </script>
    </body>
</html>

<?php
//
// echo "<pre>";
// print_r($_POST);
?>