//
//  BaseViewController.m
//  Pay1
//
//  Created by Annapurna on 04/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "BaseViewController.h"
#import "OfferView.h"
#import "MBProgressHUD.h"
#import "NoNetworkView.h"
#import "CustomAlertView.h"
#import "TransactionHistoryView.h"
#import "DatePickerView.h"
@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    HUD = [[MBProgressHUD alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    HUD.labelText = @"Loading...";
    HUD.dimBackground = YES;
    [self.view addSubview:HUD];
    
    
    /*isOfferButtonClicked = NO;
    
    objOfferView  =  [[[NSBundle mainBundle] loadNibNamed:@"OfferView" owner:self options:nil] objectAtIndex:0]; //[[[NSBundle mainBundle] loadNibNamed:@"OfferView" owner:self options:nil] objectAtIndex:0]; + 50
    objOfferView.frame = CGRectMake(self.view.frame.size.width - 170, self.view.frame.size.height -  (objOfferView.frame.size.height + 50), objOfferView.frame.size.width, objOfferView.frame.size.height);
    
    bgImgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    bgImgView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:bgImgView];*/
    
    
    
}

-(void)setBackgroundImageOnNavigationBar{
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"red_bar.png"] forBarMetrics:UIBarMetricsDefault];
}

-(UIButton *)setImageOnHeader:(NSString *)imageName isRight:(BOOL)isRight {
    UIImage *btnImg = [UIImage imageNamed:imageName];
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, btnImg.size.width, btnImg.size.height)];
    [btn setImage:btnImg forState:UIControlStateNormal];
    btn.backgroundColor = [UIColor clearColor];
    if(isRight)
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btn] ;
    else
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btn] ;
    return btn ;
}


-(void)setPlaceHolderColor:(UITextField *)textField{
    [textField setValue:[UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];

}
-(void)setViewOfButton:(UIButton *)button{
    button.layer.cornerRadius = 5;
    button.layer.borderColor = [UIColor blackColor].CGColor;
    button.layer.borderWidth = 1;
    
}
-(void)hideNavBackButton{
    self.navigationItem.hidesBackButton = YES;
    [self.navigationController setNavigationBarHidden:FALSE];

}

-(void)addOfferButtonOnAllView{
    offerButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    offerButton.frame = CGRectMake(self.view.frame.size.width - 80, self.view.frame.size.height - 80, 70, 70);
    [offerButton addTarget:self action:@selector(offerButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
  //  offerButton.backgroundColor = [UIColor redColor];
    [offerButton setBackgroundImage:[UIImage imageNamed:@"ic_menu.png"] forState:UIControlStateNormal];
    
    [self.view addSubview:offerButton];

}

-(UIButton *) setNaviationButtonWithImage:(NSString *)imageName selector:(SEL) selector isLeft:(BOOL)isLeft {
    UIImage *btnImg = [UIImage imageNamed:imageName];
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, btnImg.size.width, btnImg.size.height)];
    [btn setImage:btnImg forState:UIControlStateNormal];
    //  btn.backgroundColor = [UIColor whiteColor];
    [btn addTarget:self action:selector forControlEvents:UIControlEventTouchUpInside];
    
    if(isLeft)
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btn] ;
    else
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btn] ;
    return btn ;
}

-(UIButton *) setNaviationButtonWithText:(NSString *)text selector:(SEL) selector isLeft:(BOOL)isLeft isReduceSize:(BOOL)isReduceSize isMyFont:(CGFloat)isMyFont{
    NSLog(@"Button Text is :%@",text);
    UIButton *btn;

    if(isReduceSize == TRUE){
        btn =  [[UIButton alloc] initWithFrame:CGRectMake(0, 0,self.navigationController.navigationBar.frame.size.width - 125, 50)];
   
    }
    else{
   btn =  [[UIButton alloc] initWithFrame:CGRectMake(0, 0,self.navigationController.navigationBar.frame.size.width - 50, 50)];

    }
    
    UIImageView *arrowImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 14, 20, 20)];
    arrowImageView.image = [UIImage imageNamed:@"move_back.png"];
    
    UILabel *textLabel = [[UILabel alloc]initWithFrame:CGRectMake(arrowImageView.frame.origin.x + arrowImageView.frame.size.width + 2,12, btn.frame.size.width - 10, 21)];
    textLabel.text = text;
    textLabel.textColor = [UIColor whiteColor];
    textLabel.font = [UIFont fontWithName:@"Open Sans" size:isMyFont];
    [btn addSubview:arrowImageView];
    [btn addSubview:textLabel];
    
    [btn addTarget:self action:selector forControlEvents:UIControlEventTouchUpInside];
    
    if(isLeft)
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btn] ;
    else
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btn] ;
    return btn ;
}

-(void)setTwoNavigationButtonsWithImages:(NSString*)firstImage secondImage:(NSString*)secondImage thirdImage:(NSString*)thirdImage isLeft:(BOOL)isLeft selector1:(SEL) selector1 selector2:(SEL) selector2 selector3:(SEL) selector3 isFromGift:(BOOL)isFromGift{
    
    if(isFromGift == TRUE){
        UIView *walletView = [[UIView alloc]initWithFrame:CGRectMake(400, 0, 150, 40)];
                
        UIButton *searchButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [searchButton setBackgroundImage:[UIImage imageNamed:thirdImage] forState:UIControlStateNormal];
        searchButton.frame = CGRectMake(2, 10, 25, 25);
        [searchButton addTarget:self action:selector3 forControlEvents:UIControlEventTouchUpInside];
        
        
        
        
        UIButton *walletButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [walletButton setImage:[UIImage imageNamed:firstImage] forState:UIControlStateNormal];
        walletButton.frame = CGRectMake(searchButton.frame.origin.x + searchButton.frame.size.width + 30, 15, 35, 20);
        [walletButton addTarget:self action:selector1 forControlEvents:UIControlEventTouchUpInside];
        
        UILabel *walletLabel = [[UILabel alloc]initWithFrame:CGRectMake(searchButton.frame.origin.x + searchButton.frame.size.width + 13, -5, 70, 15)];
        walletLabel.text =  [NSString stringWithFormat:@"Rs.%@",[Constant getUserWalletBalance]]; //[NSString stringWithFormat:@"Rs.%@",[Session shared].objSignInResponse.userWalletBalance]; //@"Wallet";
        walletLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
        walletLabel.textColor = [UIColor whiteColor];
        walletLabel.textAlignment = NSTextAlignmentCenter;

        
        [walletView addSubview:searchButton];
        [walletView addSubview:walletButton];
        [walletView addSubview:walletLabel];
        
        
        UIButton *coinButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [coinButton setImage:[UIImage imageNamed:secondImage] forState:UIControlStateNormal];
        coinButton.frame = CGRectMake(walletButton.frame.origin.x + walletButton.frame.size.width + 26, 10, 30, 30);
        [coinButton addTarget:self action:selector2 forControlEvents:UIControlEventTouchUpInside];
        
        UIButton *coinBGButton = [UIButton buttonWithType:UIButtonTypeCustom];
        coinBGButton.frame = CGRectMake(walletButton.frame.origin.x + walletButton.frame.size.width + 20 ,5 , 35, 35);
        [coinBGButton addTarget:self action:selector2 forControlEvents:UIControlEventTouchUpInside];
        
        
        UILabel *coinLabel = [[UILabel alloc]initWithFrame:CGRectMake(searchButton.frame.origin.x + searchButton.frame.size.width + 65 , -5, 80, 15)];
        coinLabel.text = [Constant getUserCoinBalance]; //[Session shared].objSignInResponse.userLoyaltyPoints; //@"Coin";
        coinLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
        coinLabel.textColor = [UIColor whiteColor];
        coinLabel.textAlignment = NSTextAlignmentCenter;

        
        [walletView addSubview:coinButton];
        [walletView addSubview:coinLabel];
        [walletView addSubview:coinBGButton];
        
        UIBarButtonItem *firstButton = [[UIBarButtonItem alloc]initWithCustomView:walletView];
        
        
        [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:firstButton ,nil]];
    }
    
    
    
    else{
        
        
        NSLog(@"#Loyalty Values :%@ %@",[Session shared].objSignInResponse.userLoyaltyPoints,[Session shared].objSignInResponse.userWalletBalance);
        
        
        UIView *walletContainerView = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width - 200, 0, 100, 40)];
        
        UIView *walletView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 50, 40)];
        
       // walletView.backgroundColor = [UIColor greenColor];
        
      //  walletContainerView.backgroundColor = [UIColor blueColor];

        
        
        UILabel *walletLabel = [[UILabel alloc]initWithFrame:CGRectMake(3, -5, 70, 15)];
        walletLabel.text = [NSString stringWithFormat:@"Rs.%@",[Constant getUserWalletBalance]];//[NSString stringWithFormat:@"Rs.%@",[Session shared].objSignInResponse.userWalletBalance]; //@"Wallet";
        walletLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
        walletLabel.textColor = [UIColor whiteColor];
      //  walletLabel.textAlignment = NSTextAlignmentCenter;

        
        
        UIButton *walletButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [walletButton setImage:[UIImage imageNamed:firstImage] forState:UIControlStateNormal];
        walletButton.frame = CGRectMake(8, walletLabel.frame.origin.y + walletLabel.frame.size.height + 5, 35, 20);
        [walletButton addTarget:self action:selector1 forControlEvents:UIControlEventTouchUpInside];
        
        [walletView addSubview:walletButton];
        [walletView addSubview:walletLabel];
        
        [walletContainerView addSubview:walletView];
        
        
        
        UIButton *coinView = [UIButton buttonWithType:UIButtonTypeCustom];
        
        coinView.frame = CGRectMake(walletView.frame.origin.x + walletView.frame.size.width + 10, 0, 50, 40);
        [coinView addTarget:self action:selector2 forControlEvents:UIControlEventTouchUpInside];

        
        UILabel *coinLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, -5, 50, 15)];
        coinLabel.text = [Constant getUserCoinBalance];//[Session shared].objSignInResponse.userLoyaltyPoints; //@"Coin";
        coinLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
        coinLabel.textColor = [UIColor whiteColor];
      //  coinLabel.textAlignment = NSTextAlignmentCenter;
       
        
        UIButton *coinButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [coinButton setImage:[UIImage imageNamed:secondImage] forState:UIControlStateNormal];
        coinButton.frame = CGRectMake(7 ,coinLabel.frame.origin.y + coinLabel.frame.size.height , 30, 30);
        [coinButton addTarget:self action:selector2 forControlEvents:UIControlEventTouchUpInside];
        
        
        UIButton *coinBGButton = [UIButton buttonWithType:UIButtonTypeCustom];
        coinBGButton.frame = CGRectMake(5 ,coinLabel.frame.origin.y + coinLabel.frame.size.height-5 , 35, 35);
        [coinBGButton addTarget:self action:selector2 forControlEvents:UIControlEventTouchUpInside];

        
        [coinView addSubview:coinButton];
        [coinView addSubview:coinLabel];
        [coinView addSubview:coinBGButton];
        
        [walletContainerView addSubview:coinView];

        
        
        UIBarButtonItem *firstButton = [[UIBarButtonItem alloc]initWithCustomView:walletContainerView];
        
        
        [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:firstButton ,nil]];
    }
    
    

}


-(void)setConfirmVIewOfButton:(UIButton *)button{
[button setBackgroundColor:[UIColor colorWithRed:36/255.0 green:182/255.0 blue:82/255.0 alpha:1.0]];
button.layer.cornerRadius = 5;
}
-(void)showHUDOnView{
    [HUD show:YES];
}
-(void)hideHUDOnView{
    [HUD hide:YES];
}

-(void)showNoNetworkView{
    
    coverBaseView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    coverBaseView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.4];
    coverBaseView.userInteractionEnabled = TRUE;
    
    [self.view addSubview:coverBaseView];
    
    
    objNoNetworkView = [[NoNetworkView alloc]initWithFrame:CGRectMake(20, self.view.frame.origin.y + self.view.frame.size.height / 2 - 100 , [UIScreen mainScreen].bounds.size.width - 50, 150)];
    
    [coverBaseView addSubview:objNoNetworkView];
    
    [objNoNetworkView setOnRetryClick:^(){
        
    if(_onRetryClick){
    _onRetryClick();
        
    [coverBaseView removeFromSuperview];
        
    }
        
    }];
    

}


-(void)setTextOnLabel:(UILabel *)textLabel fontSize:(CGFloat)fontsize{
    textLabel.textColor = [UIColor darkGrayColor];
    textLabel.font = [UIFont fontWithName:@"Open Sans" size:fontsize];
    textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    textLabel.numberOfLines = 0;
    [textLabel sizeToFit];
}



-(void)setNavBarTintColor{
  UINavigationBar *bar = [self.navigationController navigationBar];
  bar.barTintColor = [UIColor redColor];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event]; 
  //  UITouch *touch = [touches anyObject];
    
    [objOfferView removeFromSuperview];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setUIForErrorViews:(UIView *)errorView{
    errorView.layer.borderColor = [UIColor redColor].CGColor;
    errorView.layer.borderWidth = 2;
}
-(void)setNavBarText:(NSString *)text{
    self.navigationController.navigationBar.topItem.title = text;
    [[UIBarButtonItem appearance] setTintColor:[UIColor whiteColor]];


}

-(void)setRoundedCornerForImageView:(UIImageView *)imgView{
    imgView.layer.cornerRadius = imgView.frame.size.height/2;
    imgView.clipsToBounds = YES;
    imgView.layer.masksToBounds = YES;
}

-(NSString *)giveLogoImageAccordingToOperator:(NSString *)opName{
    NSString *imageName;
    
   NSLog(@"#ImageName :%@",opName);
    
    
if ([opName  isEqualToString:@"Airtel"] || [opName  isEqualToString:@"Airtel Postpaid"] || [opName isEqualToString:@"Airtel DTH"] || [opName isEqualToString:@"Airtel Dth"]) {
    imageName = @"m_airtel.png";
}
if ([opName isEqualToString:@"Aircel"] ) {
    imageName = @"m_aircel.png";
}

if ([opName isEqualToString:@"BSNL"] || [opName  isEqualToString:@"Cellone Postpaid"] || [opName isEqualToString:@"Bsnl"]) {
    imageName = @"m_bsnl.png";
}

if ([opName isEqualToString:@"Videocon"] ) {
    imageName = @"m_videocon.png";
}

if ([opName isEqualToString:@"Mtnl"] || [opName isEqualToString:@"MTNL"]) {
    imageName = @"m_mtnl.png";
}

if ([opName isEqualToString:@"MTS"] || [opName isEqualToString:@"Mts"]  || [opName isEqualToString:@"mts"]) {
    imageName = @"m_mts.png";

}

if ([opName isEqualToString:@"Reliance CDMA"] ||[opName isEqualToString:@"Reliance Postpaid"] || [opName isEqualToString:@"Reliance"]) {
    imageName = @"m_reliance_cdma.png";
}

if ([opName isEqualToString:@"Reliance GSM"] ) {
    imageName = @"m_reliance.png";
}

if ([opName isEqualToString:@"Tata Teleservices Postpaid"]|| [opName isEqualToString:@"Tata Indicom"]) {
    imageName = @"m_indicom.png";

}

if ([opName isEqualToString:@"Tata Docomo"] || [opName isEqualToString:@"Docomo Postpaid"]) {
    imageName = @"m_docomo.png";
}

if ([opName isEqualToString:@"Uninor"] ) {
    imageName = @"m_uninor.png";
}

if ([opName isEqualToString:@"Vodafone"] || [opName isEqualToString:@"Vodafone Postpaid"] || [opName isEqualToString:@"vodafone"]) {
    imageName = @"m_vodafone.png";
}

if ([opName isEqualToString:@"Idea"] || [opName isEqualToString:@"IDEA Postpaid"] || [opName isEqualToString:@"Idea Postpaid"]) {
    imageName = @"m_idea.png";
}

if ([opName isEqualToString:@"Loop Mobile PostPaid"]) {
    imageName = @"m_loop.png";

}

   if ([opName isEqualToString:@"Big TV DTH"] || [opName isEqualToString:@"Big Tv Dth"]) {
    imageName = @"d_bigtv.png";

   }


   if ([opName isEqualToString:@"Sun TV DTH"] ) {
    imageName = @"d_sundirect.png";

   }
    
   
    if ([opName isEqualToString:@"Dish TV DTH"] || [opName isEqualToString:@"Dish Tv Dth"] ) {
        imageName = @"dishtv_logo.jpg";
        
    }
    
    if ([opName isEqualToString:@"Tata Sky DTH"] || [opName isEqualToString:@"Tata Sky Dth"]) {
        imageName = @"d_tatasky.png";
        
    }
    
    if ([opName isEqualToString:@"Videocon DTH"] || [opName isEqualToString:@"Videocon Dth"] ) {
        imageName = @"d_videocon.png";
        
    }
    
    
    return imageName;
}

-(UIView *)onAddDimViewOnSuperView:(UIView *)myView {
    coverBaseView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    coverBaseView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.4];
    coverBaseView.userInteractionEnabled = TRUE;
    
    [self.view addSubview:coverBaseView];
    
    return coverBaseView;
    
}




-(void)addSpaceBetweenTextFieldOriginAndText:(UITextField *)textField{
 UIView *spacerTextView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
 [textField setLeftViewMode:UITextFieldViewModeAlways];
 [textField setLeftView:spacerTextView];
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
