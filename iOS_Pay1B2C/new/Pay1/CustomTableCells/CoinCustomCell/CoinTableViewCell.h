//
//  CoinTableViewCell.h
//  Pay1
//
//  Created by webninjaz on 01/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CoinTableViewCell : UITableViewCell{
    

    
}

@property (nonatomic,strong) IBOutlet UIImageView *coinTransactionImageView;
@property (nonatomic,strong) IBOutlet UILabel *coinTransactionLabel;
@property (nonatomic,strong) IBOutlet UILabel *giftTxtLabel;
@property (nonatomic,strong) IBOutlet UILabel *transactionIDLabel;
@property (nonatomic,strong) IBOutlet UILabel *dateTimeLabel;



@end
