
#import "MailCreator.h"


@implementation MailCreator

@synthesize typeOfCall;
@synthesize idOfCall;
@synthesize delegate;
@synthesize recipient;
@synthesize emailSubject;
@synthesize emailBody;
@synthesize arrayForAttachements;
@synthesize emailBodyToHyperLink;

-(void)mail:(id)del{
    
    
	Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
	if (mailClass != nil)
	{
		// We must always check whether the current device is configured for sending emails
		if ([mailClass canSendMail])
		{
            
			[self setDelegate:del];
			[self displayComposerSheet];
		}
		
		else
		{
     
      
			UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Sorry!"
      message:@"Mail client for you phone doesn't seem to be configured." delegate:self
      cancelButtonTitle:@"OK" otherButtonTitles:nil];
			[alert show];
		}
		
	}
}


-(void) showAlertWithTitle:(NSString *) title message:(NSString *) message {
  UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Congratulation!"
  message:@"Mail has been sent successfully." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
  [alert show];
}







-(void)displayComposerSheet{
	if([MFMailComposeViewController canSendMail])
	{
		picker = [[MFMailComposeViewController alloc] init];
		picker.mailComposeDelegate = self;
    
		//picker.navigationBar.tintColor=[UIColor colorWithRed:0.33 green:0.65 blue:0.21 alpha:1.0];
        
    	NSMutableArray *array=[[NSMutableArray alloc]init];
		
        if([recipient length]>0){
            NSLog(@"recipient is :%@",recipient);
			[array addObject:recipient];
        }
		
		if([array count])
			[picker setToRecipients:array];	
        
		if([emailSubject length]<=0)
			emailSubject =(NSMutableString*)@"";
		
		
		[picker setSubject:emailSubject];
		if([emailBody length]<=0)
			emailBody=(NSMutableString*)@"";
        
        
		
		if([emailBodyToHyperLink length]>0)
			emailBody=[NSString stringWithFormat:@"%@ <a href='%@'>%@</a>",emailBody,emailBodyToHyperLink,emailBodyToHyperLink];
        
		
		[picker setMessageBody:emailBody isHTML:YES];
		
		for(NSDictionary *d in arrayForAttachements)
		{
			NSData   *data=[d objectForKey:@"data"];
			NSString *mamType=[d objectForKey:@"mam_type"];
			NSString *fileName=[d objectForKey:@"file_name"];
			[picker addAttachmentData:data mimeType:mamType fileName:fileName];
		}	
		[delegate presentModalViewController:picker animated:YES];
		
		
	}
	else {
    [self showAlertWithTitle:@"Information" message:@"Mail client for you phone doesn't seem to be configured."];
  }
  
}


-(void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
	[delegate dismissModalViewControllerAnimated:YES];
  if (result == MFMailComposeResultCancelled) {
    //NSLog(@"Message cancelled");
  }
  else if (result == MFMailComposeResultSent){
   // NSLog(@"Message sent");
    [self showAlertWithTitle:@"Title Here" message:@"Mail sent"];
  }
 
	
}


@end
