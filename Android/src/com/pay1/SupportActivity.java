package com.pay1;

import java.net.URLEncoder;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.SQLException;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.pay1.constants.Constants;
import com.pay1.customviews.MyProgressDialog;
import com.pay1.databasehandler.SupportDataSource;
import com.pay1.requesthandler.RequestClass;
import com.pay1.utilities.EditTextValidator;
import com.pay1.utilities.Utility;

public class SupportActivity extends AppBaseActivity {

	private static final String SCREEN_LABEL = "Support Screen";
	private EasyTracker easyTracker = null;
	private final String TAG = "Support";

	private TextView textView_Title, textView_Email, textView_Call,
			textView_History, textView_1, textView_2, textView_3;
	private ImageView imageView_Back, imageView_Ok, imageView_Cancel;
	private EditText editText_Query;
	private RelativeLayout relativeLayout_Email, relativeLayout_Call;
	private SupportDataSource supportDataSource;

	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.support_activity);
		try {
			easyTracker = EasyTracker.getInstance(SupportActivity.this);
			easyTracker.set(Fields.SCREEN_NAME, SCREEN_LABEL);
			registerBaseActivityReceiver();

			supportDataSource = new SupportDataSource(SupportActivity.this);

			Typeface Reguler = Typeface.createFromAsset(getAssets(),
					"EncodeSansNormal-400-Regular.ttf");

			Typeface Narrow = Typeface.createFromAsset(getAssets(),
					"EncodeSansNarrow-500-Medium.ttf");

			Typeface Normal = Typeface.createFromAsset(getAssets(),
					"EncodeSansNormal-100-Thin.ttf");

			textView_Title = (TextView) findViewById(R.id.textView_Title);
			textView_Title.setTypeface(Reguler);

			textView_Email = (TextView) findViewById(R.id.textView_Email);
			textView_Email.setTypeface(Narrow);

			textView_Call = (TextView) findViewById(R.id.textView_Call);
			textView_Call.setTypeface(Narrow);
			textView_Call
					.setText("Give us a Missed Call to Raise the Complaint of your last Transaction, on "
							+ Utility.getSupportNumber(SupportActivity.this,
									Constants.SHAREDPREFERENCE_SUPPORT_NUMBER));

			textView_1 = (TextView) findViewById(R.id.textView_1);
			textView_1.setTypeface(Narrow);
			textView_2 = (TextView) findViewById(R.id.textView_2);
			textView_2.setTypeface(Reguler);
			textView_3 = (TextView) findViewById(R.id.textView_3);
			textView_3.setTypeface(Normal);
			textView_History = (TextView) findViewById(R.id.textView_History);
			textView_History.setTypeface(Narrow);
			textView_History.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					startActivity(new Intent(SupportActivity.this,
							SupportHistoryActivity.class));
				}
			});

			imageView_Back = (ImageView) findViewById(R.id.imageView_Back);
			imageView_Back.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onBackPressed();
				}
			});

			editText_Query = (EditText) findViewById(R.id.editText_Query);
			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(editText_Query.getWindowToken(), 0);
			editText_Query.setTypeface(Normal);
			editText_Query.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					// TODO Auto-generated method stub
					if (editText_Query.getText().toString().trim().length() > 0)
						EditTextValidator.hasText(SupportActivity.this,
								editText_Query,
								Constants.ERROR_QUERY_BLANK_FIELD);
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					// TODO Auto-generated method stub

				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

				}
			});

			relativeLayout_Email = (RelativeLayout) findViewById(R.id.relativeLayout_Email);
			relativeLayout_Email.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					Intent emailIntent = new Intent(
							android.content.Intent.ACTION_SEND);

					if (Utility.getEmail(SupportActivity.this,
							Constants.SHAREDPREFERENCE_EMAIL) == null) {
						try {

							// Explicitly only use Gmail to send
							emailIntent
									.setClassName("com.google.android.gm",
											"com.google.android.gm.ComposeActivityGmail");

							emailIntent.setType("text/html");

							// Add the recipients

							emailIntent.putExtra(
									android.content.Intent.EXTRA_EMAIL,
									new String[] { "listen@pay1.in" });
							emailIntent.putExtra(
									android.content.Intent.EXTRA_SUBJECT,
									"Support");

							emailIntent
									.putExtra(
											android.content.Intent.EXTRA_TEXT,
											"Thanks & Regards\n"
													+ Utility
															.getMobileNumber(
																	SupportActivity.this,
																	Constants.SHAREDPREFERENCE_MOBILE));

							// Add the attachment by specifying a reference to
							// our
							// custom
							// ContentProvider
							// and the specific file of interest
							// emailIntent.putExtra(
							// Intent.EXTRA_STREAM,
							// Uri.parse("content://" +
							// CachedFileProvider.AUTHORITY
							// + "/"
							// + fileName));

							startActivity(emailIntent);
						} catch (Exception e) {
							emailIntent.setType("text/html");

							emailIntent.putExtra(
									android.content.Intent.EXTRA_EMAIL,
									new String[] { "listen@pay1.in" });
							emailIntent.putExtra(
									android.content.Intent.EXTRA_SUBJECT,
									"Support");

							emailIntent
									.putExtra(
											android.content.Intent.EXTRA_TEXT,
											"Thanks & Regards\n"
													+ Utility
															.getMobileNumber(
																	SupportActivity.this,
																	Constants.SHAREDPREFERENCE_MOBILE));

							startActivity(emailIntent);
						}
					} else {
						try {

							String[] recipients = new String[] {
									"listen@pay1.in",
									Utility.getEmail(SupportActivity.this,
											Constants.SHAREDPREFERENCE_EMAIL), };
							emailIntent.putExtra(
									android.content.Intent.EXTRA_EMAIL,
									recipients);
							emailIntent.putExtra(
									android.content.Intent.EXTRA_SUBJECT,
									"Support");
							emailIntent
									.putExtra(
											android.content.Intent.EXTRA_TEXT,
											"Thanks & Regards\n"
													+ Utility
															.getMobileNumber(
																	SupportActivity.this,
																	Constants.SHAREDPREFERENCE_MOBILE));
							emailIntent.setType("text/html");
							startActivity(Intent.createChooser(emailIntent,
									"Send mail client :"));
						} catch (Exception e) {

						}
					}

				}
			});

			relativeLayout_Call = (RelativeLayout) findViewById(R.id.relativeLayout_Call);
			relativeLayout_Call.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent callIntent = new Intent(Intent.ACTION_CALL);
					// callIntent.setData(Uri.parse("tel:+912267242266"));
					callIntent.setData(Uri.parse("tel:"
							+ Utility.getSupportNumber(SupportActivity.this,
									Constants.SHAREDPREFERENCE_SUPPORT_NUMBER)));
					startActivity(callIntent);
				}
			});

			imageView_Ok = (ImageView) findViewById(R.id.imageView_Ok);
			imageView_Ok.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					if (EditTextValidator.hasText(SupportActivity.this,
							editText_Query, Constants.ERROR_QUERY_BLANK_FIELD)) {
						new SupportTask().execute(Utility.getMobileNumber(
								SupportActivity.this,
								Constants.SHAREDPREFERENCE_MOBILE),
								editText_Query.getText().toString().trim());
					}

				}
			});
			imageView_Cancel = (ImageView) findViewById(R.id.imageView_Cancel);
			imageView_Cancel.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					editText_Query.setText("");
				}
			});

			// try {
			// supportDataSource.open();
			// List<Support> supports = supportDataSource.getAllSupport();
			// if (supports.size() == 0) {
			// textView_History.setVisibility(View.GONE);
			// } else {
			// textView_History.setVisibility(View.VISIBLE);
			// }
			// } catch (SQLException exception) {
			// } catch (Exception e) {
			// } finally {
			// supportDataSource.close();
			// }
		} catch (Exception e) {
		}

		overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}

	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView().build());
		// EasyTracker.getInstance(this).activityStart(this);
		easyTracker.send(MapBuilder.createAppView().build());
	}

	@Override
	public void onStop() {
		super.onStop();
		// EasyTracker.getInstance(this).activityStop(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (Constants.isExpanded) {
			Constants.collapsView(this);
		} else {
			Constants.SELECTED_MENU_INDEX = Constants.PROFILE_MENU_INDEX;
			Constants.showNavigationBar(SupportActivity.this);
			// overridePendingTransition(R.anim.push_down_in,
			// R.anim.push_down_out);
		}
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unRegisterBaseActivityReceiver();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (Constants.isExpanded) {
			Constants.collapsView(this);
		} else {
			super.onBackPressed();
			overridePendingTransition(R.anim.slide_in_left,
					R.anim.slide_out_right);
		}
	}

	public class SupportTask extends AsyncTask<String, String, String>
			implements OnDismissListener {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			try {
				String response = RequestClass
						.getInstance()
						.readPay1B2CRequest(
								SupportActivity.this,
								Constants.B2C_URL
										+ "support_req/?mobile_number="
										+ params[0] + "&message="
										+ URLEncoder.encode(params[1], "utf-8"));

				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			try {

				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONObject jsonObject = new JSONObject(replaced);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {

						try {
							supportDataSource.open();

							supportDataSource.createSupport(1, editText_Query
									.getText().toString().trim(),
									System.currentTimeMillis());
						} catch (SQLException exception) {
						} catch (Exception e) {
						} finally {
							supportDataSource.close();
						}
						Constants.showOneButtonDialog(SupportActivity.this,
								TAG, "Request sent successfully.",
								Constants.DIALOG_CLOSE);
						editText_Query.setText("");
						textView_History.setVisibility(View.VISIBLE);
					} else {
						Constants.showOneButtonDialog(SupportActivity.this,
								TAG, Constants.checkCode(replaced),
								Constants.DIALOG_CLOSE);
					}
				} else {
					// Constants.showOneButtonDialog(SupportActivity.this, TAG,
					// Constants.ERROR_INTERNET, Constants.DIALOG_CLOSE);
					Intent intent = new Intent(SupportActivity.this,
							ConnectivityErrorActivity.class);
					intent.putExtra(Constants.RECHARGE_FOR,
							Constants.WALLET_PAYMENT);
					startActivity(intent);

				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonDialog(SupportActivity.this, TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonDialog(SupportActivity.this, TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(SupportActivity.this);
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							SupportTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			SupportTask.this.cancel(true);
			dialog.cancel();
		}

	}

}
