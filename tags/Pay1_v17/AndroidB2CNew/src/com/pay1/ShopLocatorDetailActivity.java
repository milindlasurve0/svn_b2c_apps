package com.pay1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.WeakHashMap;

import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SlidingDrawer;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.larvalabs.svgandroid.SVGParser;
import com.pay1.constants.Constants;
import com.pay1.customviews.MyProgressDialog;
import com.pay1.imagehandler.ImageLoader;
import com.pay1.utilities.DirectionsJSONParser;
import com.pay1.utilities.Utility;

public class ShopLocatorDetailActivity extends FragmentActivity implements
		LocationListener {

	private static final String SCREEN_LABEL = "Shop Locator Screen";
	private EasyTracker easyTracker = null;
	// private final String TAG = "Shop Locator";

	// private ListView listView_Dealers;
	private GoogleMap googleMap;
	private TextView textView_Title, textView_Name, textView_Distance,
			textView_Address, textView_FullAddress, textView_Call,
			textView_Locate;
	private ImageView imageView_Back, imageView_Locate, imageView_Call;
	// private ArrayList<WeakHashMap<String, String>> data;

	// private DealerLocatorAdapter adapter;

	// public String NAME = "name";
	// public String DISTANCE = "distance";
	// public String ADDRESS = "address";
	private double latitude, longitude;
	private SlidingDrawer slidingDrawer1;
	private String mobile;
	String url = null;
	private ImageLoader imageLoader;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.shop_locator_detail_activity);
		try {
			easyTracker = EasyTracker
					.getInstance(ShopLocatorDetailActivity.this);
			easyTracker.set(Fields.SCREEN_NAME, SCREEN_LABEL);
			imageLoader = new ImageLoader(ShopLocatorDetailActivity.this);
			Typeface Reguler = Typeface.createFromAsset(getAssets(),
					"EncodeSansNormal-400-Regular.ttf");

			Typeface Medium = Typeface.createFromAsset(getAssets(),
					"EncodeSansNarrow-500-Medium.ttf");
			RelativeLayout back_layout=(RelativeLayout)findViewById(R.id.back_layout);
			back_layout.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onBackPressed();
				}
			});
			textView_Name = (TextView) findViewById(R.id.textView_Name);
			textView_Name.setTypeface(Medium);
			textView_Distance = (TextView) findViewById(R.id.textView_Distance);
			textView_Distance.setTypeface(Medium);
			textView_Address = (TextView) findViewById(R.id.textView_Address);
			textView_Address.setTypeface(Medium);
			textView_FullAddress = (TextView) findViewById(R.id.textView_FullAddress);
			textView_FullAddress.setTypeface(Medium);
			textView_Call = (TextView) findViewById(R.id.textView_Call);
			textView_Call.setTypeface(Medium);
			textView_Locate = (TextView) findViewById(R.id.textView_Locate);
			textView_Locate.setTypeface(Medium);

			try {
				Bundle bundle = getIntent().getExtras();
				if (bundle != null) {
					url = bundle.getString("URL");
					textView_Name.setText(bundle.getString("NAME"));
					textView_Address.setText(bundle.getString("AREA") + ", "
							+ bundle.getString("CITY"));
					mobile = bundle.getString("MOBILE");
					latitude = Double.parseDouble(bundle.getString("LATITUDE"));
					longitude = Double.parseDouble(bundle
							.getString("LONGITUDE"));
					StringBuffer sb = new StringBuffer();
					sb.append("Shop name : ")
							.append(bundle.getString("NAME"))
							.append("\nAddress : ")
							.append(bundle.getString("ADDRESS"))
							.append(",\n")
							.append(bundle.getString("AREA").trim()
									.equalsIgnoreCase("null") ? "" : bundle
									.getString("AREA"))
							.append(", ")
							.append(bundle.getString("CITY").trim()
									.equalsIgnoreCase("null") ? "" : bundle
									.getString("CITY"))
							.append(",\n")
							.append(bundle.getString("STATE").trim()
									.equalsIgnoreCase("null") ? "" : bundle
									.getString("STATE"))
							.append(" ")
							.append(bundle.getString("PIN").trim()
									.equalsIgnoreCase("null") ? "" : bundle
									.getString("PIN")).append(".")
							.append("\nShop number : ").append(mobile);
					textView_FullAddress.setText(sb.toString());

					double dis = 0;
					DecimalFormat decimalFormat = new DecimalFormat("0.0");

					try {
						dis = Double.parseDouble(bundle.getString("DISTANCE"));
					} catch (Exception e) {
						dis = 0;
					}
					if (dis < 1) {
						dis = dis * 1000;
						textView_Distance.setText(decimalFormat.format(dis)
								+ " M");
					} else {
						textView_Distance.setText(decimalFormat.format(dis)
								+ " KM");
					}
					boolean gps_enabled = false;
					boolean network_enabled = false;
					LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
					try {
						gps_enabled = lm
								.isProviderEnabled(LocationManager.GPS_PROVIDER);
					} catch (Exception ex) {
					}

					try {
						network_enabled = lm
								.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
					} catch (Exception ex) {
					}
					// don't start listeners if no provider is
					// enabled
					boolean hasGPS = getPackageManager().hasSystemFeature(
							PackageManager.FEATURE_LOCATION_GPS);
					WifiManager wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);

					if (!(network_enabled && wifi.isWifiEnabled())
							&& (hasGPS && !gps_enabled)) {
						textView_Distance.setText("");
						textView_Distance.setVisibility(View.INVISIBLE);
					}

				}
			} catch (Exception e) {
			}

			textView_Title = (TextView) findViewById(R.id.textView_Title);
			textView_Title.setTypeface(Reguler);

			imageView_Back = (ImageView) findViewById(R.id.imageView_Back);
			imageView_Back.setImageDrawable(SVGParser.getSVGFromResource(
					getResources(), R.raw.ic_back_old).createPictureDrawable());
			if (Build.VERSION.SDK_INT >= 11)
				imageView_Back.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
			imageView_Back.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onBackPressed();
				}
			});
			imageView_Call = (ImageView) findViewById(R.id.imageView_Call);
			imageView_Call.setImageDrawable(SVGParser.getSVGFromResource(
					getResources(), R.raw.ic_call).createPictureDrawable());
			if (Build.VERSION.SDK_INT >= 11)
				imageView_Call.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
			imageView_Call.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent callIntent = new Intent(Intent.ACTION_CALL);
					callIntent.setData(Uri.parse("tel:+91" + mobile));
					startActivity(callIntent);
				}
			});

			imageView_Locate = (ImageView) findViewById(R.id.imageView_Locate);
			imageView_Locate.setImageDrawable(SVGParser.getSVGFromResource(
					getResources(), R.raw.ic_shoplocator)
					.createPictureDrawable());
			if (Build.VERSION.SDK_INT >= 11)
				imageView_Locate.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
			imageView_Locate.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					boolean gps_enabled = false;
					boolean network_enabled = false;
					LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
					try {
						gps_enabled = lm
								.isProviderEnabled(LocationManager.GPS_PROVIDER);
					} catch (Exception ex) {
					}
					try {
						network_enabled = lm
								.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
					} catch (Exception ex) {
					}

					// don't start listeners if no provider is
					// enabled
					boolean hasGPS = getPackageManager().hasSystemFeature(
							PackageManager.FEATURE_LOCATION_GPS);
					WifiManager wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
					if (!(network_enabled && wifi.isWifiEnabled())
							&& (hasGPS && !gps_enabled)) {

						showTwoButtonDialog(
								ShopLocatorDetailActivity.this,
								"Shop Locator",
								"Turn on your location service on the phone to locate nearby Pay1 merchant.",
								Constants.DIALOG_CLOSE_LOCATION);
					} else if (gps_enabled || network_enabled) {
						boolean show_route = false;

						if (wifi.isWifiEnabled())
							show_route = true;
						else if (network_enabled && gps_enabled)
							show_route = true;
						else if (network_enabled)
							show_route = true;

						try {
							slidingDrawer1.close();
							String lat = Utility
									.getCurrentLatitude(
											ShopLocatorDetailActivity.this,
											Constants.SHAREDPREFERENCE_CURRENT_LATITUDE);
							String lng = Utility
									.getCurrentLongitude(
											ShopLocatorDetailActivity.this,
											Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE);
							if (lat != "" && lng != "" && show_route) {
								Location location = new Location("Current");
								location.setLatitude(Double.parseDouble(lat));
								location.setLongitude(Double.parseDouble(lng));
								setUpMap(location);
							} else {
								setUpMap();
							}
						} catch (Exception exception) {
							setUpMap();
						}

					} else {
						showTwoButtonDialog(
								ShopLocatorDetailActivity.this,
								"Shop Locator",
								"Turn on your location service on the phone to locate nearby Pay1 merchant.",
								Constants.DIALOG_CLOSE_LOCATION);
					}

				}
			});
			// data = new ArrayList<WeakHashMap<String, String>>();
			// listView_Dealers = (ListView)
			// findViewById(R.id.listView_Dealers);
			// textView_NoData = (TextView) findViewById(R.id.textView_NoData);
			// listView_Dealers.setEmptyView(textView_NoData);
			// adapter = new
			// DealerLocatorAdapter(DealerLocatorDetailActivity.this, data);
			// listView_Dealers.setAdapter(adapter);

			// new DealorLocatorTask().execute(Utility.getLatitude(
			// DealerLocatorDetailActivity.this,
			// Constants.SHAREDPREFERENCE_LATITUDE), Utility.getLongitude(
			// DealerLocatorDetailActivity.this,
			// Constants.SHAREDPREFERENCE_LONGITUDE));

			if (googleMap == null) {
				// Try to obtain the map from the SupportMapFragment.
				googleMap = ((SupportMapFragment) getSupportFragmentManager()
						.findFragmentById(R.id.map_full)).getMap();
				googleMap
						.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {

							@Override
							public void onMarkerDragStart(Marker marker) {
								// TODO Auto-generated method stub

							}

							@Override
							public void onMarkerDragEnd(Marker marker) {
								// TODO Auto-generated method stub
								// googleMap.clear();
								// LatLng latLng = marker.getPosition();
								// MarkerOptions options = new MarkerOptions();
								// options.position(latLng);
								// options.title("You are here");
								// options.draggable(false);
								// options.icon(BitmapDescriptorFactory
								// .defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
								// //
								//
								// options.icon(BitmapDescriptorFactory
								// .fromResource(R.drawable.close));
								// location = new Location("Test");
								// location.setLatitude(latLng.latitude);
								// location.setLongitude(latLng.longitude);
								// googleMap.addMarker(options);
							}

							@Override
							public void onMarkerDrag(Marker marker) {
								// TODO Auto-generated method stub

							}
						});
				googleMap
						.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

							@Override
							public void onMapClick(LatLng latLng) {
								// TODO Auto-generated method stub
								// googleMap.clear();
								// MarkerOptions options = new MarkerOptions();
								// options.position(latLng);
								// options.title("You are here");
								// options.draggable(false);
								// options.icon(BitmapDescriptorFactory
								// .defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
								// //
								//
								// options.icon(BitmapDescriptorFactory
								// .fromResource(R.drawable.close));
								// location = new Location("Test");
								// location.setLatitude(latLng.latitude);
								// location.setLongitude(latLng.longitude);
								// googleMap.addMarker(options);
							}
						});
				googleMap
						.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {

							@Override
							public void onInfoWindowClick(Marker arg0) {
								// TODO Auto-generated method stub
								slidingDrawer1.open();
							}
						});
				// Check if we were successful in obtaining the map.
				if (googleMap != null) {

					googleMap.setMyLocationEnabled(true);
					googleMap.getUiSettings().setMyLocationButtonEnabled(true);
					googleMap.getUiSettings().setRotateGesturesEnabled(true);
					googleMap.getUiSettings().setCompassEnabled(true);
					// googleMap.setInfoWindowAdapter(new
					// CustomInfoWindowAdapter(
					// ShopLocatorDetailActivity.this));
					googleMap
							.setInfoWindowAdapter(new CustomGiftInfoWindowAdapter());

					LatLng latLng_desti = new LatLng(latitude, longitude);
					MarkerOptions options_desti = new MarkerOptions();
					options_desti.position(latLng_desti);
					options_desti.title(textView_Name.getText().toString());
					options_desti
							.snippet(textView_Address.getText().toString());
					options_desti.draggable(false);
					options_desti.icon(BitmapDescriptorFactory
							.fromResource(R.drawable.icon_retailer));
					Marker marker = googleMap.addMarker(options_desti);
					marker.showInfoWindow();

					CameraPosition camPos = new CameraPosition.Builder()
							.target(latLng_desti).zoom(14).build();

					CameraUpdate camUpd3 = CameraUpdateFactory
							.newCameraPosition(camPos);

					googleMap.animateCamera(camUpd3);

					Location mylocation = googleMap.getMyLocation();
					if (mylocation != null) {

						double latitude = mylocation.getLatitude();

						// Getting longitude of the current location
						double longitude = mylocation.getLongitude();

						Utility.setCurrentLatitude(
								ShopLocatorDetailActivity.this,
								Constants.SHAREDPREFERENCE_CURRENT_LATITUDE,
								String.valueOf(latitude));
						Utility.setCurrentLongitude(
								ShopLocatorDetailActivity.this,
								Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE,
								String.valueOf(longitude));
					}
				}
			}

			slidingDrawer1 = (SlidingDrawer) findViewById(R.id.slidingDrawer1);
			try {
				LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

				// Creating a criteria object to retrieve provider
				Criteria criteria = new Criteria();

				// Getting the name of the best provider
				String provider = locationManager.getBestProvider(criteria,
						true);

				// Getting Current Location
				Location location = locationManager
						.getLastKnownLocation(provider);

				if (location != null) {
					onLocationChanged(location);
				}
				locationManager
						.requestLocationUpdates(provider, 20000, 0, this);
			} catch (Exception e) {
			}
		} catch (Exception exception) {
		}
		overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}

	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView().build());
		// EasyTracker.getInstance(this).activityStart(this);
		easyTracker.send(MapBuilder.createAppView().build());
	}

	@Override
	public void onStop() {
		super.onStop();
		// EasyTracker.getInstance(this).activityStop(this);
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (slidingDrawer1.isOpened())
			slidingDrawer1.close();
		else {
			super.onBackPressed();
			Utility.setBackRequired(ShopLocatorDetailActivity.this,
					Constants.SHAREDPREFERENCE_BACK_LOCATION, false);
			overridePendingTransition(R.anim.slide_in_left,
					R.anim.slide_out_right);
		}
	}

	private void setUpMap() {
		try {
			googleMap.clear();

			LatLng latLng_desti = new LatLng(latitude, longitude);
			MarkerOptions options_desti = new MarkerOptions();
			options_desti.position(latLng_desti);
			options_desti.title(textView_Name.getText().toString());
			options_desti.snippet(textView_Address.getText().toString());
			options_desti.draggable(false);
			options_desti.icon(BitmapDescriptorFactory
					.fromResource(R.drawable.icon_retailer));
			Marker marker = googleMap.addMarker(options_desti);
			marker.showInfoWindow();

			CameraPosition camPos = new CameraPosition.Builder()
					.target(latLng_desti).zoom(14).build();

			CameraUpdate camUpd3 = CameraUpdateFactory
					.newCameraPosition(camPos);

			googleMap.animateCamera(camUpd3);
		} catch (Exception exception) {
		}

	}

	private void setUpMap(Location location) {
		try {
			googleMap.clear();

			double lat = location.getLatitude(), lng = location.getLongitude();
			LatLng latLng_current = new LatLng(lat, lng);
			MarkerOptions options_current = new MarkerOptions();
			options_current.position(latLng_current);
			options_current.title("You are here");
			options_current.draggable(false);
			options_current.icon(BitmapDescriptorFactory
					.fromResource(R.drawable.location));
			googleMap.addMarker(options_current);

			LatLng latLng_desti = new LatLng(latitude, longitude);
			MarkerOptions options_desti = new MarkerOptions();
			options_desti.position(latLng_desti);
			options_desti.title(textView_Name.getText().toString());
			options_desti.snippet(textView_Address.getText().toString());
			options_desti.draggable(false);
			options_desti.icon(BitmapDescriptorFactory
					.fromResource(R.drawable.icon_retailer));
			Marker marker = googleMap.addMarker(options_desti);
			marker.showInfoWindow();

			String url = getDirectionsUrl(latLng_current, latLng_desti);
			// Log.w("URL", url);

			DownloadTask downloadTask = new DownloadTask();

			// Start downloading json data from Google Directions API
			downloadTask.execute(url);
			// Route.paths.add(polyline);
			// CameraPosition camPos = new CameraPosition.Builder()
			// .target(new LatLng(latitude, longitude)).zoom(18)
			// .bearing(location.getBearing()).tilt(70).build();

			CameraPosition camPos = new CameraPosition.Builder()
					.target(latLng_current).zoom(14).build();

			CameraUpdate camUpd3 = CameraUpdateFactory
					.newCameraPosition(camPos);

			googleMap.animateCamera(camUpd3);
		} catch (Exception exception) {
		}

	}

	private String getDirectionsUrl(LatLng origin, LatLng dest) {

		// Origin of route
		String str_origin = "origin=" + origin.latitude + ","
				+ origin.longitude;

		// Destination of route
		String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

		// Sensor enabled
		String sensor = "sensor=false";

		// driving (default) indicates standard driving directions using the
		// road network.
		// walking requests walking directions via pedestrian paths & sidewalks
		// (where available).
		// bicycling requests bicycling directions via bicycle paths & preferred
		// streets (where available).
		// transit requests directions via public transit routes (where
		// available).

		// Travel Modes
		String mode = "mode=walking";

		// Building the parameters to the web service
		String parameters = str_origin + "&" + str_dest + "&" + sensor + "&"
				+ mode;

		// Output format
		String output = "json";

		// Building the url to the web service
		String url = "https://maps.googleapis.com/maps/api/directions/"
				+ output + "?" + parameters;

		return url;
	}

	/** A method to download json data from url */
	private String downloadUrl(String strUrl) throws IOException {
		String data = "";
		InputStream iStream = null;
		HttpURLConnection urlConnection = null;
		try {
			URL url = new URL(strUrl);

			// Creating an http connection to communicate with url
			urlConnection = (HttpURLConnection) url.openConnection();

			// Connecting to url
			urlConnection.connect();

			// Reading data from url
			iStream = urlConnection.getInputStream();

			BufferedReader br = new BufferedReader(new InputStreamReader(
					iStream));

			StringBuffer sb = new StringBuffer();

			String line = "";
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}

			data = sb.toString();

			br.close();

		} catch (Exception e) {
			// Log.d("Exception while downloading url", e.toString());
		} finally {
			iStream.close();
			urlConnection.disconnect();
		}
		return data;
	}

	// Fetches data from url passed
	private class DownloadTask extends AsyncTask<String, Void, String>
			implements OnDismissListener {
		// private MyProgressDialog dialog;
		private MyProgressDialog dialog;

		// Downloading data in non-ui thread
		@Override
		protected String doInBackground(String... url) {

			// For storing data from web service
			String data = "";

			try {
				// Fetching the data from web service
				data = downloadUrl(url[0]);
			} catch (Exception e) {
				// Log.d("Background Task", e.toString());
			}
			return data;
		}

		// Executes in UI thread, after the execution of
		// doInBackground()
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				if (this.dialog.isShowing()) {
					this.dialog.dismiss();
				}
				if (!result.equalsIgnoreCase("")) {
					ParserTask parserTask = new ParserTask();
					// Invokes the thread for parsing the JSON data
					parserTask.execute(result);
				} else {
					setUpMap();
				}
			} catch (Exception e) {
				setUpMap();
			}
		}

		protected void onPreExecute() {
			dialog = new MyProgressDialog(ShopLocatorDetailActivity.this);
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							DownloadTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			DownloadTask.this.cancel(true);
			dialog.cancel();
		}

	}

	/** A class to parse the Google Places in JSON format */
	private class ParserTask extends
			AsyncTask<String, Integer, List<List<WeakHashMap<String, String>>>> {
		// private MyProgressDialog dialog;

		// Parsing the data in non-ui thread
		@Override
		protected List<List<WeakHashMap<String, String>>> doInBackground(
				String... jsonData) {

			JSONObject jObject;
			List<List<WeakHashMap<String, String>>> routes = null;

			try {
				jObject = new JSONObject(jsonData[0]);
				DirectionsJSONParser parser = new DirectionsJSONParser();

				// Starts parsing data
				routes = parser.parse(jObject);
			} catch (Exception e) {
				// e.printStackTrace();
			}
			return routes;
		}

		// Executes in UI thread, after the parsing process
		@Override
		protected void onPostExecute(
				List<List<WeakHashMap<String, String>>> result) {
			super.onPostExecute(result);
			try {
				// if (this.dialog.isShowing()) {
				// this.dialog.dismiss();
				// }
				if (result != null) {
					ArrayList<LatLng> points = null;
					PolylineOptions lineOptions = null;
					// MarkerOptions markerOptions = new MarkerOptions();

					// Traversing through all the routes
					for (int i = 0; i < result.size(); i++) {
						points = new ArrayList<LatLng>();
						lineOptions = new PolylineOptions();

						// Fetching i-th route
						List<WeakHashMap<String, String>> path = result.get(i);

						// Fetching all the points in i-th route
						for (int j = 0; j < path.size(); j++) {
							WeakHashMap<String, String> point = path.get(j);

							double lat = Double.parseDouble(point.get("lat"));
							double lng = Double.parseDouble(point.get("lng"));
							LatLng position = new LatLng(lat, lng);

							points.add(position);
						}

						// Adding all the points in the route to LineOptions
						lineOptions.addAll(points);
						lineOptions.width(6);
						lineOptions.color(getResources().getColor(
								R.color.app_blue_line_map_color));
					}

					// Drawing polyline in the Google Map for the i-th route
					googleMap.addPolyline(lineOptions);
				} else {
					setUpMap();
				}
			} catch (Exception e) {
				setUpMap();
			}
		}

		protected void onPreExecute() {
			// dialog = new MyProgressDialog(ShopLocatorDetailActivity.this);
			// // this.dialog.setMessage("Please wait.....");
			// this.dialog.setCancelable(false);
			// this.dialog
			// .setOnCancelListener(new DialogInterface.OnCancelListener() {
			//
			// @Override
			// public void onCancel(DialogInterface dialog) {
			// // TODO Auto-generated method stub
			// ParserTask.this.cancel(true);
			// }
			// });
			// this.dialog.show();
			super.onPreExecute();
		}

		// @Override
		// public void onDismiss(DialogInterface dialog) {
		// // TODO Auto-generated method stub
		// ParserTask.this.cancel(true);
		// dialog.cancel();
		// }

	}

	private class CustomGiftInfoWindowAdapter implements InfoWindowAdapter {

		private View view;
		private Typeface Reguler, Medium;

		public CustomGiftInfoWindowAdapter() {
			view = getLayoutInflater().inflate(R.layout.custom_info_window,
					null);
			Reguler = Typeface.createFromAsset(getAssets(),
					"EncodeSansNormal-400-Regular.ttf");

			Medium = Typeface.createFromAsset(getAssets(),
					"EncodeSansNarrow-500-Medium.ttf");
		}

		@Override
		public View getInfoContents(Marker marker) {

			if (marker != null && marker.isInfoWindowShown()) {
				marker.hideInfoWindow();
				marker.showInfoWindow();
			}
			return null;
		}

		@SuppressLint("NewApi")
		@Override
		public View getInfoWindow(final Marker marker) {

			final ImageView image = ((ImageView) view
					.findViewById(R.id.imageView_Icon));

			if (url != null && !url.equalsIgnoreCase("null")
					&& !url.equalsIgnoreCase("")) {
				ShopLocatorDetailActivity.this.runOnUiThread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						imageLoader.getDealImage(url, image);
						image.setVisibility(View.VISIBLE);
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						imageLoader.getDealImage(url, image);
						getInfoContents(marker);
					}
				});

			} else {
				image.setImageResource(R.drawable.icon_gift);
				image.setVisibility(View.GONE);
			}

			final String title = marker.getTitle();
			final TextView titleUi = ((TextView) view.findViewById(R.id.title));
			titleUi.setTypeface(Medium);
			if (title != null) {
				titleUi.setText(Html.fromHtml("<font color=\"blue\"><u>"
						+ title + "</u></font>"));

			} else {
				titleUi.setText("");
			}

			final String snippet = marker.getSnippet();
			final TextView snippetUi = ((TextView) view
					.findViewById(R.id.snippet));
			snippetUi.setTypeface(Reguler);
			if (snippet != null) {
				snippetUi.setText(snippet);
			} else {
				snippetUi.setText("");
			}

			return view;
		}
	}

	public void showTwoButtonDialog(final Context context, String Title,
			String message, final int Type) {
		try {

			final Dialog dialog = new Dialog(context);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			// dialog.getWindow().setBackgroundDrawableResource(
			// android.R.color.transparent);
			dialog.setContentView(R.layout.dialog_two_button);
			dialog.setCancelable(false);
			dialog.setCanceledOnTouchOutside(false);
			// dialog.setTitle(null);

			Typeface Reguler = Typeface.createFromAsset(context.getAssets(),
					"EncodeSansNormal-400-Regular.ttf");

			Typeface Medium = Typeface.createFromAsset(context.getAssets(),
					"EncodeSansNarrow-500-Medium.ttf");

			// set the custom dialog components - text, image and button
			TextView textView_Title = (TextView) dialog
					.findViewById(R.id.textView_Title);
			textView_Title.setText(Title);
			textView_Title.setTypeface(Reguler);

			TextView textView_Message = (TextView) dialog
					.findViewById(R.id.textView_Message);
			textView_Message.setText(message);
			textView_Message.setTypeface(Medium);

			Button button_Ok = (Button) dialog.findViewById(R.id.button_Ok);
			button_Ok.setTypeface(Reguler);
			// if button is clicked, close the custom dialog
			button_Ok.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();

					Intent viewIntent = new Intent(
							Settings.ACTION_LOCATION_SOURCE_SETTINGS);
					context.startActivity(viewIntent);
				}
			});

			Button button_Cancel = (Button) dialog
					.findViewById(R.id.button_Cancel);
			button_Cancel.setTypeface(Reguler);
			// if button is clicked, close the custom dialog
			button_Cancel.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
					slidingDrawer1.close();
					boolean gps_enabled = false;
					boolean network_enabled = false;
					LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
					try {
						gps_enabled = lm
								.isProviderEnabled(LocationManager.GPS_PROVIDER);
					} catch (Exception ex) {
					}
					try {
						network_enabled = lm
								.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
					} catch (Exception ex) {
					}

					// don't start listeners if no provider is
					// enabled
					boolean hasGPS = getPackageManager().hasSystemFeature(
							PackageManager.FEATURE_LOCATION_GPS);
					WifiManager wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
					boolean show_route = false;

					if (wifi.isWifiEnabled())
						show_route = true;
					else if (network_enabled && gps_enabled)
						show_route = true;
					else if (network_enabled)
						show_route = true;

					try {
						slidingDrawer1.close();
						String lat = Utility.getCurrentLatitude(
								ShopLocatorDetailActivity.this,
								Constants.SHAREDPREFERENCE_CURRENT_LATITUDE);
						String lng = Utility.getCurrentLongitude(
								ShopLocatorDetailActivity.this,
								Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE);
						if (lat != "" && lng != "" && show_route) {
							Location location = new Location("Current");
							location.setLatitude(Double.parseDouble(lat));
							location.setLongitude(Double.parseDouble(lng));
							setUpMap(location);
						} else {
							setUpMap();
						}
					} catch (Exception exception) {
						setUpMap();
					}
				}
			});

			button_Ok.setText("Ok");
			button_Cancel.setText("Not now");

			dialog.show();

		} catch (Exception exception) {
		}
	}

	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		if (location != null) {

			double latitude = location.getLatitude();

			// Getting longitude of the current location
			double longitude = location.getLongitude();

			// Creating a LatLng object for the current location
			// LatLng latLng = new LatLng(latitude, longitude);

			// Showing the current location in Google Map
			// googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

			// Zoom in the Google Map
			// googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));

			Utility.setCurrentLatitude(ShopLocatorDetailActivity.this,
					Constants.SHAREDPREFERENCE_CURRENT_LATITUDE,
					String.valueOf(latitude));
			Utility.setCurrentLongitude(ShopLocatorDetailActivity.this,
					Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE,
					String.valueOf(longitude));

		}
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}
}
