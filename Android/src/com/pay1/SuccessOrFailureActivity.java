package com.pay1;

import java.util.ArrayList;
import java.util.WeakHashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.pay1.customviews.MyProgressDialog;

import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.pay1.constants.Constants;
import com.pay1.imagehandler.ImageLoader;
import com.pay1.requesthandler.RequestClass;
import com.pay1.utilities.Utility;

public class SuccessOrFailureActivity extends AppBaseActivity {
	private static final String SCREEN_LABEL = "Success Or Failure Screen";
	private EasyTracker easyTracker = null;
	TextView textView_Title, textViewMobileNumber, textViewOperator,
			textViewAmount, textTaxesInfo, textViewType, textView_Deal;
	private Button textView_Call;
	private ImageLoader imageLoader;
	private TextView textView_DealName, textView_Amount, textView_OfferAmount,
			textView_Sold, textView_Discount;
	ImageView imageView_Back, imageView_Pic;
	String operatorName, mobileNumber, rechargeAmount, payOpid,
			specialRecharge, subsId;
	double totalAmount;
	String TAG = "Order Summary";
	int rechargeType;
	boolean isSuccess;
	ArrayList<WeakHashMap<String, String>> data;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.success_failure_activity);
		try {
			easyTracker = EasyTracker
					.getInstance(SuccessOrFailureActivity.this);
			easyTracker.set(Fields.SCREEN_NAME, SCREEN_LABEL);

			findViewById();
			registerBaseActivityReceiver();

			data = new ArrayList<WeakHashMap<String, String>>();
			imageLoader = new ImageLoader(SuccessOrFailureActivity.this);
			Bundle orderBundle = getIntent().getExtras();
			isSuccess = orderBundle.getBoolean(Constants.SUCCESS_OR_FAILURE);

			if (isSuccess) {
				textView_Title.setText("Success!");
				mobileNumber = orderBundle.getString(Constants.MOBILE_NUMBER);
				rechargeAmount = orderBundle
						.getString(Constants.RECHARGE_AMOUNT);
				operatorName = orderBundle.getString(Constants.OPERATOR_NAME);
				rechargeType = orderBundle.getInt(Constants.RECHARGE_FOR);
				payOpid = orderBundle.getString(Constants.OPERATOR_ID);
				specialRecharge = orderBundle.getString(Constants.STV_RECHARGE);
				textViewOperator.setText(operatorName);
				textViewAmount.setText("Rs. " + rechargeAmount);
				if (rechargeType == Constants.RECHARGE_DTH) {
					subsId = orderBundle.getString(Constants.MOBILE_NUMBER);
					textViewMobileNumber.setText(subsId);
					textViewType.setText("DTH recharge");
				} else if (rechargeType == Constants.RECHARGE_MOBILE
						|| rechargeType == Constants.RECHARGE_DATA) {
					textViewMobileNumber.setText(mobileNumber);
					textViewType.setText("Mobile recharge");
				} else if (rechargeType == Constants.BILL_PAYMENT) {
					textViewMobileNumber.setText(mobileNumber);
					textViewType.setText("Bill payment");
					textTaxesInfo.setVisibility(View.VISIBLE);
					textTaxesInfo.setText(orderBundle.getString("TAX"));
				} else if (rechargeType == Constants.SUBSCRIBE_DEAL) {
					textViewMobileNumber.setText(mobileNumber);
					textViewOperator.setVisibility(View.GONE);
					textViewType.setText("Deal purchased");
				} else if (rechargeType == Constants.WALLET_PAYMENT) {
					textViewMobileNumber.setText(mobileNumber);
					textViewType.setText("Wallet topup");
					textViewOperator.setVisibility(View.GONE);
					new CheckBalanceTask().execute();
				}
				textView_Call
						.setText("If your transaction does not succeed, give a misscall on "
								+ Utility
										.getSupportNumber(
												SuccessOrFailureActivity.this,
												Constants.SHAREDPREFERENCE_SUPPORT_NUMBER));
			} else {
				textView_Title.setText("Failure!");

				mobileNumber = orderBundle.getString(Constants.MOBILE_NUMBER);
				rechargeAmount = orderBundle
						.getString(Constants.RECHARGE_AMOUNT);
				operatorName = orderBundle.getString(Constants.OPERATOR_NAME);
				rechargeType = orderBundle.getInt(Constants.RECHARGE_FOR);
				payOpid = orderBundle.getString(Constants.OPERATOR_ID);
				specialRecharge = orderBundle.getString(Constants.STV_RECHARGE);
				textViewOperator.setText(operatorName);
				textViewAmount.setText("Rs. " + rechargeAmount);
				if (rechargeType == Constants.RECHARGE_DTH) {
					subsId = orderBundle.getString(Constants.MOBILE_NUMBER);
					textViewMobileNumber.setText(subsId);
					textViewType.setText(orderBundle
							.getString(Constants.REASON));
				} else if (rechargeType == Constants.RECHARGE_MOBILE
						|| rechargeType == Constants.RECHARGE_DATA) {
					textViewMobileNumber.setText(mobileNumber);
					textViewType.setText(orderBundle
							.getString(Constants.REASON));
				} else if (rechargeType == Constants.BILL_PAYMENT) {
					textViewMobileNumber.setText(mobileNumber);
					textViewType.setText(orderBundle
							.getString(Constants.REASON));
					textTaxesInfo.setVisibility(View.VISIBLE);
					textTaxesInfo.setText(orderBundle.getString("TAX"));
				} else if (rechargeType == Constants.WALLET_PAYMENT) {
					textViewMobileNumber.setText(mobileNumber);
					textViewMobileNumber.setVisibility(View.GONE);
					textViewType.setText(orderBundle
							.getString(Constants.REASON));
					textViewOperator.setVisibility(View.GONE);
				} else if (rechargeType == Constants.SUBSCRIBE_DEAL) {
					textViewMobileNumber.setText(mobileNumber);
					textViewType.setText(orderBundle
							.getString(Constants.REASON));
					textViewOperator.setVisibility(View.GONE);
				}
				// textView_Call
				// .setText("Your recharge request could not be completed due to some technical error. The amount of Rs "
				// + rechargeAmount
				// +
				// " has been credited back to your wallet. Please try after sometime.");

				textView_Call
						.setText("For any other support, you can try our other support options.");
				textView_Call.setCompoundDrawables(null, null, null, null);
			}

			Typeface Normal = Typeface.createFromAsset(getAssets(),
					"EncodeSansNormal-100-Thin.ttf");
			Typeface Reguler = Typeface.createFromAsset(getAssets(),
					"EncodeSansNormal-400-Regular.ttf");
			Typeface Medium = Typeface.createFromAsset(getAssets(),
					"EncodeSansNarrow-500-Medium.ttf");
			Typeface Narrow = Typeface.createFromAsset(getAssets(),
					"EncodeSansNarrow-500-Medium.ttf");
			textViewAmount.setTypeface(Normal);
			textTaxesInfo.setTypeface(Medium);
			textViewMobileNumber.setTypeface(Normal);
			textViewOperator.setTypeface(Normal);
			textViewType.setTypeface(Medium);
			textView_Title.setTypeface(Reguler);
			textView_Deal.setTypeface(Medium);
			textView_Call.setTypeface(Medium);

			try {
				imageView_Pic = (ImageView) findViewById(R.id.imageView_Pic);
				textView_DealName = (TextView) findViewById(R.id.textView_DealName);
				textView_DealName.setTypeface(Reguler);
				textView_Amount = (TextView) findViewById(R.id.textView_Amount);
				textView_Amount.setTypeface(Reguler);
				textView_OfferAmount = (TextView) findViewById(R.id.textView_OfferAmount);
				textView_OfferAmount.setTypeface(Narrow);
				textView_Sold = (TextView) findViewById(R.id.textView_Sold);
				textView_Sold.setTypeface(Narrow);
				textView_Discount = (TextView) findViewById(R.id.textView_Discount);
				textView_Discount.setTypeface(Narrow);
				data = new ArrayList<WeakHashMap<String, String>>(
						(ArrayList<WeakHashMap<String, String>>) getIntent()
								.getSerializableExtra("DATA"));

				WeakHashMap<String, String> map = new WeakHashMap<String, String>(
						data.get(0));

				imageLoader.getDealImage(map.get("URL"), imageView_Pic);
				textView_DealName.setText(map.get("DEAL_NAME"));
				textView_Amount.setText("Rs. " + map.get("ACTUAL_PRICE"));
				textView_Amount.setPaintFlags(textView_Amount.getPaintFlags()
						| Paint.STRIKE_THRU_TEXT_FLAG);
				textView_OfferAmount.setText("Rs. " + map.get("OFFER_PRICE"));
				textView_Sold.setText(map.get("STOCK_SOLD") + " Bought");
				textView_Discount.setText(map.get("DISCOUNT") + "% Discount");

				double actual_amt = 0;
				try {
					actual_amt = Double.parseDouble(map
							.get(DealActivity.ACTUAL_PRICE));
				} catch (Exception e) {
					actual_amt = 0;
				}

				if (actual_amt == 0) {
					textView_Amount.setVisibility(View.GONE);
					textView_Discount.setVisibility(View.GONE);
				} else {
					textView_Amount.setVisibility(View.VISIBLE);
					textView_Discount.setVisibility(View.VISIBLE);
				}

			} catch (Exception e) {
				imageView_Pic.setVisibility(View.GONE);
				textView_DealName.setVisibility(View.GONE);
				textView_Amount.setVisibility(View.GONE);
				textView_OfferAmount.setVisibility(View.GONE);
				textView_Sold.setVisibility(View.GONE);
				textView_Discount.setVisibility(View.GONE);
				textView_Deal.setVisibility(View.GONE);

				new CheckBalanceTask().execute();
			}

			imageView_Back.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onBackPressed();
					// closeAllActivitiesAfterRecharge(rechargeType);
				}
			});

			imageView_Pic = (ImageView) findViewById(R.id.imageView_Pic);
			imageView_Pic.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent intent = new Intent(SuccessOrFailureActivity.this,
							DealActivity.class);
					intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
							| Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(intent);
					finish();
				}
			});

			textView_Call.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (isSuccess) {
						Intent callIntent = new Intent(Intent.ACTION_CALL);
						// callIntent.setData(Uri.parse("tel:+912267242266"));
						callIntent.setData(Uri.parse("tel:"
								+ Utility
										.getSupportNumber(
												SuccessOrFailureActivity.this,
												Constants.SHAREDPREFERENCE_SUPPORT_NUMBER)));
						startActivity(callIntent);
					} else {
						Intent intent = new Intent(
								SuccessOrFailureActivity.this,
								SupportActivity.class);
						startActivity(intent);
					}
				}
			});
		} catch (Exception e) {
		}
		overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}

	private void findViewById() {
		imageView_Back = (ImageView) findViewById(R.id.imageView_Back);
		// TODO Auto-generated method stub
		textView_Title = (TextView) findViewById(R.id.textView_Title);
		textViewMobileNumber = (TextView) findViewById(R.id.textViewMobileNumber);
		textViewAmount = (TextView) findViewById(R.id.textViewAmount);
		textTaxesInfo = (TextView) findViewById(R.id.textTaxesInfo);
		textTaxesInfo.setText("");
		textTaxesInfo.setVisibility(View.GONE);
		textViewType = (TextView) findViewById(R.id.textViewType);
		textView_Deal = (TextView) findViewById(R.id.textView_Deal);
		textView_Call = (Button) findViewById(R.id.textView_Call);
		textViewOperator = (TextView) findViewById(R.id.textViewOperator);
	}

	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView().build());
		// EasyTracker.getInstance(this).activityStart(this);
		easyTracker.send(MapBuilder.createAppView().build());
	}

	@Override
	public void onStop() {
		super.onStop();
		// EasyTracker.getInstance(this).activityStop(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (Constants.isExpanded) {
			Constants.collapsView(this);
		} else {
			if (rechargeType == Constants.RECHARGE_MOBILE
					|| rechargeType == Constants.RECHARGE_DTH
					|| rechargeType == Constants.RECHARGE_DATA) {
				Constants.SELECTED_MENU_INDEX = Constants.RECHARGE_MENU_INDEX;
				if (isSuccess) {
					textView_Title.setTextColor(getResources().getColor(
							R.color.app_green_color));
				} else {
					textView_Title.setTextColor(getResources().getColor(
							R.color.Red));
				}
				imageView_Back
						.setImageResource(R.drawable.button_recharge_back_icon);
			} else if (rechargeType == Constants.SUBSCRIBE_DEAL) {
				Constants.SELECTED_MENU_INDEX = Constants.DEAL_MENU_INDEX;
				if (isSuccess) {
					textView_Title.setTextColor(getResources().getColor(
							R.color.app_orange_color));
				} else {
					textView_Title.setTextColor(getResources().getColor(
							R.color.Red));
				}
				imageView_Back
						.setImageResource(R.drawable.button_deal_back_icon);
			} else if (rechargeType == Constants.BILL_PAYMENT) {
				Constants.SELECTED_MENU_INDEX = Constants.BILL_MENU_INDEX;
				if (isSuccess) {
					textView_Title.setTextColor(getResources().getColor(
							R.color.app_skyblue_color));
				} else {
					textView_Title.setTextColor(getResources().getColor(
							R.color.Red));
				}
				imageView_Back
						.setImageResource(R.drawable.button_bill_back_icon);
			} else if (rechargeType == Constants.WALLET_PAYMENT) {
				Constants.SELECTED_MENU_INDEX = Constants.PROFILE_MENU_INDEX;
				if (isSuccess) {
					textView_Title.setTextColor(getResources().getColor(
							R.color.app_blue_color));
				} else {
					textView_Title.setTextColor(getResources().getColor(
							R.color.Red));
				}
				imageView_Back
						.setImageResource(R.drawable.button_profile_back_icon);
			}
			Constants.showNavigationBar(SuccessOrFailureActivity.this);
		}
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unRegisterBaseActivityReceiver();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (Constants.isExpanded) {
			Constants.collapsView(this);
		} else {
			super.onBackPressed();
			closeAllActivitiesAfterRecharge(rechargeType);
			overridePendingTransition(R.anim.slide_in_left,
					R.anim.slide_out_right);
		}
	}

	public class CheckBalanceTask extends AsyncTask<String, String, String>
			implements OnDismissListener {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			try {
				String response = RequestClass.getInstance()
						.readPay1B2CRequest(SuccessOrFailureActivity.this,
								Constants.B2C_URL + "check_bal");

				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			try {

				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONObject jsonObject = new JSONObject(replaced);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {
						JSONObject jsonObject2 = jsonObject
								.getJSONObject("description");

						Utility.setBalance(SuccessOrFailureActivity.this,
								Constants.SHAREDPREFERENCE_BALANCE, jsonObject2
										.getString("account_balance").trim());
						Constants
								.showNavigationBar(SuccessOrFailureActivity.this);

						try {
							JSONArray jsonArray = jsonObject
									.getJSONArray("deal");
							for (int i = 0; i < jsonArray.length(); i++) {

								imageLoader.getDealImage(jsonArray
										.getJSONObject(i).getString("img_url"),
										imageView_Pic);
								textView_DealName
										.setText(jsonArray.getJSONObject(i)
												.getString("dealname"));
								textView_Amount.setText("Rs. "
										+ jsonArray.getJSONObject(i).getString(
												"actual_price"));
								textView_Amount.setPaintFlags(textView_Amount
										.getPaintFlags()
										| Paint.STRIKE_THRU_TEXT_FLAG);
								textView_OfferAmount.setText("Rs. "
										+ jsonArray.getJSONObject(i).getString(
												"offer_price"));
								textView_Sold.setText(jsonArray
										.getJSONObject(i).getString(
												"stock_sold")
										+ " Bought");
								textView_Discount.setText(jsonArray
										.getJSONObject(i).getString("discount")
										+ "% Discount");

								double actual_amt = 0;
								try {
									actual_amt = Double.parseDouble(jsonArray
											.getJSONObject(i).getString(
													"actual_price"));
								} catch (Exception e) {
									actual_amt = 0;
								}

								if (actual_amt == 0) {
									textView_Amount.setVisibility(View.GONE);
									textView_Discount.setVisibility(View.GONE);
								} else {
									textView_Amount.setVisibility(View.VISIBLE);
									textView_Discount
											.setVisibility(View.VISIBLE);
								}
							}

							imageView_Pic.setVisibility(View.VISIBLE);
							textView_DealName.setVisibility(View.VISIBLE);
							textView_Amount.setVisibility(View.VISIBLE);
							textView_OfferAmount.setVisibility(View.VISIBLE);
							textView_Sold.setVisibility(View.VISIBLE);
							textView_Discount.setVisibility(View.VISIBLE);
							textView_Deal.setVisibility(View.VISIBLE);
						} catch (Exception e) {
							imageView_Pic.setVisibility(View.GONE);
							textView_DealName.setVisibility(View.GONE);
							textView_Amount.setVisibility(View.GONE);
							textView_OfferAmount.setVisibility(View.GONE);
							textView_Sold.setVisibility(View.GONE);
							textView_Discount.setVisibility(View.GONE);
							textView_Deal.setVisibility(View.GONE);
						}

					} else {
						// Constants.showOneButtonDialog(
						// SuccessOrFailureActivity.this, TAG,
						// Constants.checkCode(replaced),
						// Constants.DIALOG_CLOSE);
					}

				} else {
					// Constants.showOneButtonDialog(
					// SuccessOrFailureActivity.this, TAG,
					// Constants.ERROR_INTERNET, Constants.DIALOG_CLOSE);

				}
			} catch (JSONException e) {
				// e.printStackTrace();
				// Constants.showOneButtonDialog(SuccessOrFailureActivity.this,
				// TAG, Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
				// Constants.DIALOG_CLOSE);
			} catch (Exception e) {
				// TODO: handle exception
				// Constants.showOneButtonDialog(SuccessOrFailureActivity.this,
				// TAG, Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
				// Constants.DIALOG_CLOSE);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(SuccessOrFailureActivity.this);
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							CheckBalanceTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			CheckBalanceTask.this.cancel(true);
			dialog.cancel();
		}

	}
}
