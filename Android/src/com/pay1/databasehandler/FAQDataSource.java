package com.pay1.databasehandler;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class FAQDataSource {
	// Database fields
	private SQLiteDatabase database;
	private Pay1SQLiteHelper dbHelper;

	public FAQDataSource(Context context) {
		dbHelper = new Pay1SQLiteHelper(context);
	}

	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
	}

	public void createFAQ(String faqQuestion, String faqAnswer,
			long faqUptateTime) {
		ContentValues values = new ContentValues();
		values.put(Pay1SQLiteHelper.FAQ_QUESTION, faqQuestion);
		values.put(Pay1SQLiteHelper.FAQ_ANSWER, faqAnswer);
		values.put(Pay1SQLiteHelper.FAQ_TIME, faqUptateTime);

		database.insert(Pay1SQLiteHelper.TABLE_FAQ, null, values);
		// Cursor cursor = database.query(Pay1SQLiteHelper.TABLE_FAQ,
		// null, Pay1SQLiteHelper.FAQ_ID + " = " + insertId, null,
		// null, null, null);
		// cursor.moveToFirst();
		// FAQ neFAQ = cursorToFAQ(cursor);
		// cursor.close();
		// return newFAQ;
	}

	public void updateFAQ(String faqQuestion, String faqAnswer,
			long faqUptateTime, int faqID) {

		ContentValues values = new ContentValues();
		values.put(Pay1SQLiteHelper.FAQ_QUESTION, faqQuestion);
		values.put(Pay1SQLiteHelper.FAQ_ANSWER, faqAnswer);
		values.put(Pay1SQLiteHelper.FAQ_TIME, faqUptateTime);
		long updatetId = database.update(Pay1SQLiteHelper.TABLE_FAQ, values,
				Pay1SQLiteHelper.FAQ_ID + " = " + faqID, null);
		if (updatetId > 0) {
		}
		// // Log.i(Tag, "Monitor updated with id: " + monitor_ID);
		else {
		}
	}

	public void deleteFAQ() {
		database.delete(Pay1SQLiteHelper.TABLE_FAQ, null, null);
	}

	public void deleteFAQ(FAQ support) {
		long id = support.getFaqID();
		database.delete(Pay1SQLiteHelper.TABLE_FAQ, Pay1SQLiteHelper.FAQ_ID
				+ " = " + id, null);
	}

	public List<FAQ> getAllFAQ() {
		List<FAQ> faqs = new ArrayList<FAQ>();

		Cursor cursor = database.query(Pay1SQLiteHelper.TABLE_FAQ, null, null,
				null, null, null, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			FAQ faq = cursorToFAQ(cursor);
			faqs.add(faq);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return faqs;
	}

	private FAQ cursorToFAQ(Cursor cursor) {
		FAQ FAQ = new FAQ();
		FAQ.setFaqID(cursor.getInt(0));
		FAQ.setFaqQuestion(cursor.getString(1));
		FAQ.setFaqAnswer(cursor.getString(2));
		FAQ.setFaqUptateTime(cursor.getLong(3));
		return FAQ;
	}
}
