package com.pay1;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.database.SQLException;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.larvalabs.svgandroid.SVGParser;
import com.pay1.adapterhandler.GiftGridAdapter;
import com.pay1.databasehandler.FreeBies;
import com.pay1.databasehandler.FreeBiesDataSource;

public class GiftLikeFragment extends Fragment {

	private GridView gridView_Gifts;
	private GiftGridAdapter adapter;
	private ArrayList<FreeBies> data;
	private FreeBiesDataSource freeBiesDataSource;
	TextView textView_NoData;

	public static GiftLikeFragment newInstance() {
		GiftLikeFragment fragment = new GiftLikeFragment();
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// if ((savedInstanceState != null)
		// && savedInstanceState.containsKey(KEY_CONTENT)) {
		// mContent = savedInstanceState.getString(KEY_CONTENT);
		// }
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		final View view = inflater.inflate(R.layout.gift_grid_fragment,
				container, false);
		try {

			freeBiesDataSource = new FreeBiesDataSource(getActivity());

			data = new ArrayList<FreeBies>();
			gridView_Gifts = (GridView) view.findViewById(R.id.gridView_Gifts);
			textView_NoData = (TextView) view
					.findViewById(R.id.textView_NoData);
			gridView_Gifts.setEmptyView(textView_NoData);

			textView_NoData.setText("Tap the heart on the gifts you like.");

			// String ExternalStorageDirectoryPath = Environment
			// .getExternalStorageDirectory().getAbsolutePath();
			//
			// String targetPath = ExternalStorageDirectoryPath + "/Pay1Cache/";
			//
			// File targetDirector = new File(targetPath);
			//
			// File[] files = targetDirector.listFiles();
			// int i = 1;
			// for (File file : files) {
			// items.add(new GiftItem(i, "Gift " + i, file.getAbsolutePath(),
			// i * 10));
			// i++;
			// }

			try {
				freeBiesDataSource.open();

				List<FreeBies> list = freeBiesDataSource.getAllLikeFreeBies(1);
				int len = list.size();
				if (len != 0) {

					for (FreeBies freebie : list)
						data.add(freebie);

				} else {

					ImageView imageViewMyGift = (ImageView) view
							.findViewById(R.id.imageViewMyGift);
					imageViewMyGift.setImageDrawable(SVGParser
							.getSVGFromResource(getActivity().getResources(),
									R.raw.near_you_blank_screen)
							.createPictureDrawable());
					// TextView
					// textView=(TextView)view.findViewById(R.id.textView_NoData);
					textView_NoData
							.setText(" Tap the heart on the gifts you like. ");
					// new FreeBiesTask()
					// .execute(
					// Utility.getCurrentLatitude(
					// getActivity(),
					// Constants.SHAREDPREFERENCE_CURRENT_LATITUDE),
					// Utility.getCurrentLongitude(
					// getActivity(),
					// Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE));
					// pager.setVisibility(View.GONE);

				}
			} catch (SQLException exception) {
				// TODO: handle exception
				// // // Log.e("Er ", exception.getMessage());
			} catch (Exception e) {
				// TODO: handle exception
				// Log.e("Er ", e.getMessage());
			} finally {
				freeBiesDataSource.close();
			}

			if (data.size() != 0) {

			} else {
				ImageView imageViewMyGift = (ImageView) view
						.findViewById(R.id.imageViewMyGift);
				imageViewMyGift.setImageDrawable(SVGParser.getSVGFromResource(
						getActivity().getResources(), R.raw.like_blank_screen)
						.createPictureDrawable());

				textView_NoData
						.setText(" Tap the heart on the gifts you like. ");
			}

			adapter = new GiftGridAdapter(getActivity(), data, false, false,
					false);
			// items.clear();
			gridView_Gifts.setAdapter(adapter);
			gridView_Gifts
					.setOnItemClickListener(new AdapterView.OnItemClickListener() {

						@Override
						public void onItemClick(AdapterView<?> parent,
								View view, int position, long id) {
							// TODO Auto-generated method stub
							try {
								FreeBies freeBie = data.get(position);
								Intent intent = new Intent(getActivity(),
										MyFreeBiesDetailsActivity.class);
								intent.putExtra("GIFT", freeBie);
								intent.putExtra("IS_BUY", 0);
								startActivity(intent);
							} catch (Exception e) {
							}
						}
					});
			loadData();
		} catch (Exception e) {
		}
		return view;
	}

	public void loadData() {
		try {
			freeBiesDataSource.open();
			data.clear();

			List<FreeBies> list = freeBiesDataSource.getAllLikeFreeBies(1);
			int len = list.size();
			if (len != 0) {

				for (FreeBies freebie : list)
					data.add(freebie);

			} else {
				// new FreeBiesTask()
				// .execute(
				// Utility.getCurrentLatitude(
				// getActivity(),
				// Constants.SHAREDPREFERENCE_CURRENT_LATITUDE),
				// Utility.getCurrentLongitude(
				// getActivity(),
				// Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE));
				// pager.setVisibility(View.GONE);

			}

			getActivity().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					adapter.notifyDataSetChanged();
				}
			});
		} catch (SQLException exception) {
			// TODO: handle exception
			Log.e("Er ", exception.getMessage());
		} catch (Exception e) {
			// TODO: handle exception
			// Log.e("Er ", e.getMessage());
		} finally {
			freeBiesDataSource.close();
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		// outState.putString(KEY_CONTENT, mContent);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.Fragment#onDestroy()
	 */
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}
}
