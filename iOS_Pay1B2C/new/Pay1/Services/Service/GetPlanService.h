//
//  GetPlanService.h
//  Pay1
//
//  Created by Annapurna on 05/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "Service.h"
@class GetPlanResponse;
@class GetMobileResponse;
typedef void (^getMobileResponse)(NSError *error, GetMobileResponse *objMobileResponse);
typedef void (^getPlanResponse)(NSError *error, GetPlanResponse *objGetPlanResponse);
@interface GetPlanService : Service
-(void)callGetPlanService:(getPlanResponse)callback;

-(void)callGetMobileService:(NSString *)systemTime callback:(getMobileResponse)callback;

@end
