package com.pay1;

import java.util.ArrayList;
import java.util.WeakHashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.pay1.adapterhandler.DealAdapter;
import com.pay1.constants.Constants;
import com.pay1.customviews.MyProgressDialog;
import com.pay1.requesthandler.RequestClass;
import com.pay1.utilities.Utility;

public class DealActivity extends AppBaseActivity {

	private static final String SCREEN_LABEL = "Deal Screen";
	private final String TAG = "Deals";

	private ImageView imageView_ChangeLocation, imageView_MyDeals;
	private ListView listView_Deals;
	private DealAdapter adapter;
	private TextView textView_Title, textView_VersionTitle;
	private ArrayList<WeakHashMap<String, String>> data;

	public static final String DEAL_ID = "id";
	public static final String DEAL_NAME = "dealname";
	public static final String OFFER_PRICE = "offer_price";
	public static final String CATAGORY = "category";
	public static final String CATAGORY_ID = "category_id";
	public static final String LATITUDE = "latitude";
	public static final String LONGITUDE = "longitude";
	public static final String DISTANCE = "distance";
	public static final String ACTUAL_PRICE = "actual_price";
	public static final String DISCOUNT = "discount";
	public static final String TOTAL_STOCK = "total_stock";
	public static final String URL = "img_url";
	public static final String STOCK_SOLD = "stock_sold";

	private boolean doubleBackToExitPressedOnce = false;

	private EasyTracker easyTracker = null;

	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.deal_activity);
		try {
			// Initialize GA
			// Pay1GA pay1GA = new Pay1GA(DealActivity.this);
			// Pay1GA.getGaTracker().set(Fields.SCREEN_NAME, SCREEN_LABEL);

			easyTracker = EasyTracker.getInstance(DealActivity.this);
			easyTracker.set(Fields.SCREEN_NAME, SCREEN_LABEL);

			registerBaseActivityReceiver();

			// Typeface Normal = Typeface.createFromAsset(getAssets(),
			// "EncodeSansNormal-100-Thin.ttf");

			try {
				if (getIntent().getExtras().containsKey("bal")) {
					if (getIntent().getExtras().getString("bal")
							.equalsIgnoreCase("1")) {
						Constants.showOneButtonDialog(DealActivity.this,
								"Notification", getIntent().getExtras()
										.getString("desc").trim(),
								Constants.DIALOG_CLOSE);
						new CheckBalanceTask().execute();
					}
				}
			} catch (Exception e) {
			}

			Typeface Reguler = Typeface.createFromAsset(getAssets(),
					"EncodeSansNormal-400-Regular.ttf");

			textView_Title = (TextView) findViewById(R.id.textView_Title);
			textView_Title.setTypeface(Reguler);
			textView_VersionTitle = (TextView) findViewById(R.id.textView_VersionTitle);
			textView_VersionTitle.setTypeface(Reguler);

			imageView_ChangeLocation = (ImageView) findViewById(R.id.imageView_ChangeLocation);
			imageView_ChangeLocation.setVisibility(View.GONE);
			imageView_ChangeLocation
					.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub

						}
					});

			imageView_MyDeals = (ImageView) findViewById(R.id.imageView_MyDeals);
			imageView_MyDeals.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (Utility.getLoginFlag(DealActivity.this,
							Constants.SHAREDPREFERENCE_IS_LOGIN)) {
						Intent intent = new Intent(DealActivity.this,
								MyDealActivity.class);
						startActivity(intent);
					} else {
						Intent intent = new Intent(DealActivity.this,
								LoginForRechargeActivity.class);
						startActivity(intent);
					}
				}
			});

			data = new ArrayList<WeakHashMap<String, String>>();
			listView_Deals = (ListView) findViewById(R.id.listView_Deals);
			adapter = new DealAdapter(DealActivity.this, data);
			listView_Deals.setAdapter(adapter);
			listView_Deals
					.setOnItemClickListener(new AdapterView.OnItemClickListener() {

						@Override
						public void onItemClick(AdapterView<?> parent,
								View view, int position, long id) {
							// TODO Auto-generated method stub
							try {
								WeakHashMap<String, String> map = new WeakHashMap<String, String>();
								map = data.get(position);
								Intent intent = new Intent(DealActivity.this,
										DealDetailsActivity.class);
								intent.putExtra("IS_BUY", 1);
								intent.putExtra("LAT", map.get(LATITUDE));
								intent.putExtra("LNG", map.get(LONGITUDE));
								intent.putExtra("URL", map.get(URL));
								intent.putExtra("DEAL_ID", map.get(DEAL_ID));
								intent.putExtra("DEAL_NAME", map.get(DEAL_NAME));
								intent.putExtra("ACTUAL_PRICE",
										map.get(ACTUAL_PRICE));
								intent.putExtra("OFFER_PRICE",
										map.get(OFFER_PRICE));
								intent.putExtra("STOCK_SOLD",
										map.get(STOCK_SOLD));
								intent.putExtra("DISCOUNT", map.get(DISCOUNT));

								startActivity(intent);
							} catch (Exception e) {
							}
						}
					});

			// try {
			// LocationResult locationResult = new LocationResult() {
			// @Override
			// public void gotLocation(Location location) {
			// // Got the location!
			// try {
			// // //// Log.w("Location", location + "");
			// if (location.getLatitude() != 0
			// && location.getLongitude() != 0) {
			// new DealTask().execute(
			// "" + location.getLatitude(), ""
			// + location.getLongitude());
			// } else {
			// String lat = Utility
			// .getCurrentLatitude(
			// DealActivity.this,
			// Constants.SHAREDPREFERENCE_CURRENT_LATITUDE);
			// String lng = Utility
			// .getCurrentLongitude(
			// DealActivity.this,
			// Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE);
			// if (lat != null && lng != null) {
			// new DealTask().execute(lat, lng);
			// } else {
			// new DealTask()
			// .execute(
			// Utility.getLatitude(
			// DealActivity.this,
			// Constants.SHAREDPREFERENCE_LATITUDE),
			// Utility.getLongitude(
			// DealActivity.this,
			// Constants.SHAREDPREFERENCE_LONGITUDE));
			// }
			// }
			// } catch (Exception exception) {
			// new DealTask().execute(null, null);
			// }
			// }
			// };
			// MyLocation myLocation = new MyLocation();
			// myLocation.getLocation(this, locationResult);
			// } catch (Exception exception) {
			// new DealTask().execute(null, null);
			// }

			try {

				String lat = Utility.getCurrentLatitude(DealActivity.this,
						Constants.SHAREDPREFERENCE_CURRENT_LATITUDE);
				String lng = Utility.getCurrentLongitude(DealActivity.this,
						Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE);
				if (lat != null && lng != null) {
					new DealTask().execute(lat, lng);
				} else {
					new DealTask().execute(Utility.getLatitude(
							DealActivity.this,
							Constants.SHAREDPREFERENCE_LATITUDE), Utility
							.getLongitude(DealActivity.this,
									Constants.SHAREDPREFERENCE_LONGITUDE));

				}
			} catch (Exception exception) {
				new DealTask().execute(null, null);
			}
			// overridePendingTransition(R.anim.translate_activity_down,
			// R.anim.translate_activity_up);
		} catch (Exception e) {
		}

		overridePendingTransition(R.anim.push_up_in, R.anim.fadeout);
	}

	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView().build());
		// EasyTracker.getInstance(this).activityStart(this);
		easyTracker.send(MapBuilder.createAppView().build());
	}

	@Override
	public void onStop() {
		super.onStop();
		// EasyTracker.getInstance(this).activityStop(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (Constants.isExpanded) {
			Constants.collapsView(this);
		} else {
			Constants.SELECTED_MENU_INDEX = Constants.DEAL_MENU_INDEX;
			Constants.showNavigationBar(DealActivity.this);
		}
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unRegisterBaseActivityReceiver();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// if (Constants.isExpanded) {
		// Constants.collapsView(this);
		// } else {
		// super.onBackPressed();
		// overridePendingTransition(R.anim.fadeout, R.anim.push_down_out);
		// }
		if (Constants.isExpanded) {
			Constants.collapsView(this);
		} else {
			if (doubleBackToExitPressedOnce) {
				super.onBackPressed();
				overridePendingTransition(R.anim.fadeout, R.anim.push_down_out);
				closeAllActivitiesInApp();
				return;
			}

			this.doubleBackToExitPressedOnce = true;
			Toast.makeText(this, "Press again to exit.", Toast.LENGTH_SHORT)
					.show();

			new Handler().postDelayed(new Runnable() {

				@Override
				public void run() {
					doubleBackToExitPressedOnce = false;
				}
			}, 10000);
		}
		// if (Constants.isExpanded) {
		// if (doubleBackToExitPressedOnce) {
		// super.onBackPressed();
		// overridePendingTransition(R.anim.fadeout, R.anim.push_down_out);
		// } else {
		// Constants.collapsView(this);
		// }
		// } else if (!Constants.isExpanded) {
		// if (doubleBackToExitPressedOnce) {
		// super.onBackPressed();
		// overridePendingTransition(R.anim.fadeout, R.anim.push_down_out);
		// } else {
		// DisplayMetrics metrics = new DisplayMetrics();
		// getWindowManager().getDefaultDisplay().getMetrics(metrics);
		// int panelWidth = (int) ((metrics.widthPixels) * 0.80);
		// int panelHeight = (int) ((metrics.heightPixels) * 0.80);
		// Constants
		// .expandView(DealActivity.this, panelWidth, panelHeight);
		// }
		// }
		// if (!doubleBackToExitPressedOnce) {
		// this.doubleBackToExitPressedOnce = true;
		// Toast.makeText(this, "Press again to exit.",
		// Toast.LENGTH_SHORT).show();
		// }
		//
		// new Handler().postDelayed(new Runnable() {
		//
		// @Override
		// public void run() {
		// doubleBackToExitPressedOnce = false;
		// }
		// }, 2000);
		// return;
	}

	public class DealTask extends AsyncTask<String, String, String> implements
			OnDismissListener {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			try {
				String response = RequestClass
						.getInstance()
						.readPay1B2CRequest(
								DealActivity.this,
								Constants.B2C_URL + "get_deals/?latitude="
										+ params[0] + "&longitude=" + params[1]);

				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			try {

				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONObject jsonObject = new JSONObject(replaced);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {
						data.clear();

						JSONArray jsonArrayDesc = jsonObject
								.getJSONArray("description");

						for (int i = 0; i < jsonArrayDesc.length(); i++) {
							WeakHashMap<String, String> map = new WeakHashMap<String, String>();
							map.put(DEAL_ID, jsonArrayDesc.getJSONObject(i)
									.getString("id"));
							map.put(DEAL_NAME, jsonArrayDesc.getJSONObject(i)
									.getString("dealname"));
							map.put(OFFER_PRICE, jsonArrayDesc.getJSONObject(i)
									.getString("offer_price"));
							map.put(CATAGORY, jsonArrayDesc.getJSONObject(i)
									.getString("category"));
							map.put(CATAGORY_ID, jsonArrayDesc.getJSONObject(i)
									.getString("category_id"));
							map.put(LATITUDE, jsonArrayDesc.getJSONObject(i)
									.getString("latitude"));
							map.put(LONGITUDE, jsonArrayDesc.getJSONObject(i)
									.getString("longitude"));
							map.put(DISTANCE, jsonArrayDesc.getJSONObject(i)
									.getString("distance"));
							map.put(ACTUAL_PRICE, jsonArrayDesc
									.getJSONObject(i).getString("actual_price"));
							map.put(DISCOUNT, jsonArrayDesc.getJSONObject(i)
									.getString("discount"));
							map.put(TOTAL_STOCK, jsonArrayDesc.getJSONObject(i)
									.getString("total_stock"));
							map.put(URL, jsonArrayDesc.getJSONObject(i)
									.getString("img_url"));
							map.put(STOCK_SOLD, jsonArrayDesc.getJSONObject(i)
									.getString("stock_sold"));

							data.add(map);
						}

						DealActivity.this.runOnUiThread(new Runnable() {

							@Override
							public void run() {
								// TODO Auto-generated method stub
								adapter.notifyDataSetChanged();
							}
						});

					} else {
						Constants.showOneButtonDialog(DealActivity.this, TAG,
								Constants.checkCode(replaced),
								Constants.DIALOG_CLOSE);
					}

				} else {
					// Constants.showOneButtonDialog(DealActivity.this, TAG,
					// Constants.ERROR_INTERNET, Constants.DIALOG_CLOSE);
					Intent intent = new Intent(DealActivity.this,
							ConnectivityErrorActivity.class);
					intent.putExtra(Constants.RECHARGE_FOR,
							Constants.SUBSCRIBE_DEAL);
					startActivity(intent);

				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonDialog(DealActivity.this, TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonDialog(DealActivity.this, TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(DealActivity.this);
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							DealTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			DealTask.this.cancel(true);
			dialog.cancel();
		}

	}

	public class CheckBalanceTask extends AsyncTask<String, String, String> {
		// MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {

			String response = RequestClass.getInstance().readPay1B2CRequest(
					DealActivity.this, Constants.B2C_URL + "check_bal");

			return response;

		}

		@Override
		protected void onPostExecute(String result) {
			// if (this.dialog.isShowing()) {
			// this.dialog.dismiss();
			// }
			super.onPostExecute(result);
			try {
				// JSONArray array = new JSONArray(result);
				JSONObject jsonObject = new JSONObject(result);
				String status = jsonObject.getString("status");
				if (status.equalsIgnoreCase("SUCCESS")) {
					JSONObject description = jsonObject
							.getJSONObject("description");
					String account_balance = description
							.getString("account_balance");
					Utility.setBalance(DealActivity.this,
							Constants.SHAREDPREFERENCE_BALANCE,
							account_balance.trim());
					Constants.SELECTED_MENU_INDEX = Constants.SUBSCRIBE_DEAL;
					Constants.showNavigationBar(DealActivity.this);

				}
			} catch (JSONException e) {
				// e.printStackTrace();
				// Constants.showCustomToast(RechargeMobileActivity.this,
				// result);
			} catch (Exception e) {
				// TODO: handle exception
				// Constants.showCustomToast(RechargeMobileActivity.this,
				// result);
			}

		}

		@Override
		protected void onPreExecute() {
			// dialog = new MyProgressDialog(DealActivity.this);
			// // this.dialog.setMessage("Please wait.....");
			// this.dialog.show();
			super.onPreExecute();

		}

	}

}
