package com.pay1.databasehandler;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Pay1SyncDatabaseHelper extends SQLiteOpenHelper{
	private Context dbContext;
//	public static String dbPath = "/mnt/sdcard/Download";
	
	public Pay1SyncDatabaseHelper(Context context) {
		super(context, Pay1SyncDataHelper.SYNC_DATABASE, null, Pay1SyncDataHelper.DATABASE_VERSION);
		 dbContext = context;
	} 
	
	/*public Pay1SyncDatabaseHelper(Context context, String databaseName) {
		super(context, databaseName, null, Pay1SyncDataHelper.DATABASE_VERSION);
		 dbContext = context;
	} */
	
	public boolean cloneDatabase(String inputDBName, String outputDBName) {
		try{
			File dbFile = dbContext.getDatabasePath(inputDBName);//new File(dbPath + "/" + inputDBName);
			InputStream input = new FileInputStream(dbFile);
			
		    OutputStream output = new FileOutputStream(dbFile.getAbsoluteFile().getParentFile().getAbsolutePath() + "/" + outputDBName);
	
		    byte[] buffer = new byte[1024];
		    int length;
		    while ((length = input.read(buffer)) > 0)
		    {
		        output.write(buffer, 0, length);
		    }
		    
		    output.flush();
		    output.close();
		    input.close();
		    return true;
		}
		catch(IOException e){
			e.printStackTrace();
			return false;
		} 
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		
	}

	
}
