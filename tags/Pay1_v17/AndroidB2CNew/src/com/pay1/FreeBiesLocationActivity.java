package com.pay1;

import java.util.ArrayList;
import java.util.WeakHashMap;

import android.annotation.SuppressLint;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.larvalabs.svgandroid.SVGParser;
import com.pay1.adapterhandler.CustomInfoWindowAdapter;
import com.pay1.adapterhandler.FreebiesLocationAdapter;
import com.pay1.constants.Constants;
import com.pay1.utilities.Utility;

public class FreeBiesLocationActivity extends FragmentActivity {

	private static final String SCREEN_LABEL = "Freebie Location List Screen";
	private EasyTracker easyTracker = null;
	// private final String TAG = "Deal Location";

	private ListView listView_Location;
	private GoogleMap googleMap;
	private TextView textView_Title;
	private ImageView imageView_Back;
	private ArrayList<WeakHashMap<String, String>> data_location;
	private ArrayList<WeakHashMap<String, String>> temp;
	private FreebiesLocationAdapter adapter;

	public static final String DEAL_ID = "id";
	public static final String LATITUDE = "latitude";
	public static final String LONGITUDE = "longitude";
	public static final String ADDRESS = "address";
	public static final String CITY = "city";
	public static final String STATE = "state";

	String deal_name = "";
	boolean show_route = false;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.freebies_location_activity);
		try {
			easyTracker = EasyTracker
					.getInstance(FreeBiesLocationActivity.this);
			easyTracker.set(Fields.SCREEN_NAME, SCREEN_LABEL);

			Typeface Reguler = Typeface.createFromAsset(getAssets(),
					"EncodeSansNormal-400-Regular.ttf");

			Utility.setBackRequired(FreeBiesLocationActivity.this,
					Constants.SHAREDPREFERENCE_BACK_LOCATION, false);

			
			RelativeLayout back_layout=(RelativeLayout)findViewById(R.id.back_layout);
			back_layout.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onBackPressed();
				}
			});
			
			textView_Title = (TextView) findViewById(R.id.textView_Title);
			textView_Title.setTypeface(Reguler);

			imageView_Back = (ImageView) findViewById(R.id.imageView_Back);
			imageView_Back.setImageDrawable(SVGParser.getSVGFromResource(
					getResources(), R.raw.ic_back_old).createPictureDrawable());
			if (Build.VERSION.SDK_INT >= 11)
				imageView_Back.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
			imageView_Back.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onBackPressed();
				}
			});

			try {

				if (getIntent().getBooleanExtra("FROM_DETAILS", true)) {
					data_location = new ArrayList<WeakHashMap<String, String>>(
							(ArrayList<WeakHashMap<String, String>>) getIntent()
									.getSerializableExtra("LOCATIONS"));
				} else {
					temp = new ArrayList<WeakHashMap<String, String>>(
							(ArrayList<WeakHashMap<String, String>>) getIntent()
									.getSerializableExtra("LOCATIONS"));
					data_location = new ArrayList<WeakHashMap<String, String>>();
					for (int j = 0; j < temp.size(); j++) {
						WeakHashMap<String, String> map1 = new WeakHashMap<String, String>();
						WeakHashMap<String, String> map = new WeakHashMap<String, String>(
								temp.get(j));
						if (Integer.parseInt(map.get(DEAL_ID)) == Integer
								.parseInt(getIntent().getStringExtra("ID"))) {
							map1.put(LATITUDE, map.get(LATITUDE));
							map1.put(LONGITUDE, map.get(LONGITUDE));
							map1.put(ADDRESS, map.get(ADDRESS));
							map1.put(CITY, map.get(CITY));
							map1.put(STATE, map.get(STATE));

							data_location.add(map1);
						}
					}
				}
				deal_name = getIntent().getStringExtra("DEAL");
				show_route = getIntent().getBooleanExtra("ROUTE", false);
			} catch (Exception e) {
				data_location = new ArrayList<WeakHashMap<String, String>>();
			}

			textView_Title.setText("Way to " + deal_name);
			listView_Location = (ListView) findViewById(R.id.listView_Location);
			// textView_NoData = (TextView) findViewById(R.id.textView_NoData);
			// listView_Location.setEmptyView(textView_NoData);
			adapter = new FreebiesLocationAdapter(
					FreeBiesLocationActivity.this, show_route, deal_name,
					data_location);
			listView_Location.setAdapter(adapter);
			listView_Location.setClickable(false);
			listView_Location
					.setOnItemClickListener(new AdapterView.OnItemClickListener() {

						@Override
						public void onItemClick(AdapterView<?> parent,
								View view, int position, long id) {
							// TODO Auto-generated method stub
							setUpMap(position);
						}
					});

			if (googleMap == null) {
				// Try to obtain the map from the SupportMapFragment.
				googleMap = ((SupportMapFragment) getSupportFragmentManager()
						.findFragmentById(R.id.map_full)).getMap();
				googleMap
						.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {

							@Override
							public void onMarkerDragStart(Marker marker) {
								// TODO Auto-generated method stub

							}

							@Override
							public void onMarkerDragEnd(Marker marker) {
								// TODO Auto-generated method stub
								// googleMap.clear();
								// LatLng latLng = marker.getPosition();
								// MarkerOptions options = new MarkerOptions();
								// options.position(latLng);
								// options.title("You are here");
								// options.draggable(true);
								// options.icon(BitmapDescriptorFactory
								// .defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
								// //
								// options.icon(BitmapDescriptorFactory.fromResource(R.drawable.close));
								// location = new Location("Test");
								// location.setLatitude(latLng.latitude);
								// location.setLongitude(latLng.longitude);
								// googleMap.addMarker(options);
							}

							@Override
							public void onMarkerDrag(Marker marker) {
								// TODO Auto-generated method stub

							}
						});
				googleMap
						.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

							@Override
							public void onMapClick(LatLng latLng) {
								// TODO Auto-generated method stub
								// googleMap.clear();
								// MarkerOptions options = new MarkerOptions();
								// options.position(latLng);
								// options.title("You are here");
								// options.draggable(true);
								// options.icon(BitmapDescriptorFactory
								// .defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
								// //
								// options.icon(BitmapDescriptorFactory.fromResource(R.drawable.close));
								// location = new Location("Test");
								// location.setLatitude(latLng.latitude);
								// location.setLongitude(latLng.longitude);
								// googleMap.addMarker(options);
							}
						});
				// Check if we were successful in obtaining the map.
				if (googleMap != null) {
					setUpMap(0);
				}
			}

		} catch (Exception exception) {
		}
		overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}

	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView().build());
		// EasyTracker.getInstance(this).activityStart(this);
		easyTracker.send(MapBuilder.createAppView().build());
	}

	@Override
	public void onStop() {
		super.onStop();
		// EasyTracker.getInstance(this).activityStop(this);
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
	}

	private void setUpMap(final int position) {
		try {

			googleMap.setInfoWindowAdapter(new CustomInfoWindowAdapter(
					FreeBiesLocationActivity.this));

			WeakHashMap<String, String> map = new WeakHashMap<String, String>(
					data_location.get(position));
			googleMap.clear();
			StringBuffer sb = new StringBuffer();
			sb.append(map.get(FreeBiesActivity.ADDRESS)).append(",\n")
					.append(map.get(FreeBiesActivity.CITY)).append(", ")
					.append(map.get(FreeBiesActivity.STATE));

			double lat = Double.parseDouble(map.get(FreeBiesActivity.LATITUDE)), lng = Double
					.parseDouble(map.get(FreeBiesActivity.LONGITUDE));
			LatLng latLng = new LatLng(lat, lng);
			MarkerOptions options = new MarkerOptions();
			options.position(latLng);
			options.title("Deal location");
			options.snippet(sb.toString());
			options.draggable(false);
			// options.icon(BitmapDescriptorFactory
			// .defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
			options.icon(BitmapDescriptorFactory
					.fromResource(R.drawable.location));
			googleMap.addMarker(options);

			// CameraPosition camPos = new CameraPosition.Builder()
			// .target(new LatLng(latitude, longitude)).zoom(18)
			// .bearing(location.getBearing()).tilt(70).build();

			CameraPosition camPos = new CameraPosition.Builder().target(latLng)
					.zoom(15).build();

			CameraUpdate camUpd3 = CameraUpdateFactory
					.newCameraPosition(camPos);

			googleMap.animateCamera(camUpd3);
		} catch (Exception exception) {
		}

	}

}