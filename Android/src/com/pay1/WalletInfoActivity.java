package com.pay1;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.pay1.adapterhandler.InfoAdapter;
import com.pay1.constants.Constants;
import com.pay1.requesthandler.RequestClass;
import com.pay1.utilities.Utility;

public class WalletInfoActivity extends AppBaseActivity {

	// private final String TAG = "Info";

	private ListView listView_Info;
	private ArrayList<String> list;
	private InfoAdapter adapter;
	float x1, x2, y1, y2, dx, dy;
	boolean touchActionMoveStatus;

	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.profile_info_activity);
		try {

			registerBaseActivityReceiver();

			listView_Info = (ListView) findViewById(R.id.listView_Info);
			list = new ArrayList<String>();
			list.clear();
			list.add("Top up wallet");
			list.add("Wallet history");
			adapter = new InfoAdapter(WalletInfoActivity.this, list);
			listView_Info.setAdapter(adapter);
			listView_Info
					.setOnItemClickListener(new AdapterView.OnItemClickListener() {

						@Override
						public void onItemClick(AdapterView<?> parent,
								View view, int position, long id) {
							// TODO Auto-generated method stub
							switch (position) {
							case 0:
								startActivity(new Intent(
										WalletInfoActivity.this,
										WalletTopupActivity.class));
								break;
							case 1:
								startActivity(new Intent(
										WalletInfoActivity.this,
										WalletHistoryActivity.class));
								break;
							default:
								break;
							}

						}
					});

			new CheckBalanceTask().execute();
		} catch (Exception e) {
		}
		overridePendingTransition(R.anim.push_up_in, R.anim.fadeout);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (Constants.isExpanded) {
			Constants.collapsView(this);
		} else {
			Constants.SELECTED_MENU_INDEX = Constants.PROFILE_MENU_INDEX;
			Constants.showNavigationBar(WalletInfoActivity.this);
		}

	}

	// @Override
	// public boolean onTouchEvent(MotionEvent event) {
	// // TODO Auto-generated method stub
	// int eventaction = event.getAction();
	//
	// switch (eventaction) {
	//
	// case MotionEvent.ACTION_POINTER_UP:
	// touchActionMoveStatus = false;
	// break;
	// case MotionEvent.ACTION_DOWN: {
	// // finger touches the screen
	// x1 = event.getX();
	// y1 = event.getY();
	// touchActionMoveStatus = true;
	// // DisplayMetrics metrics = new DisplayMetrics();
	// // getWindowManager().getDefaultDisplay().getMetrics(metrics);
	// // int panelWidth = (int) ((metrics.widthPixels) * 0.80);
	// // int panelHeight = (int) ((metrics.heightPixels) * 0.80);
	// //
	// // Constants.expandView(WalletInfoActivity.this, panelWidth,
	// // panelHeight);
	// }
	// break;
	// case MotionEvent.ACTION_MOVE:
	// // finger moves on the screen
	//
	// x2 = event.getX();
	// y2 = event.getY();
	//
	// dx = x2 - x1;
	// dy = y2 - y1;
	// if (touchActionMoveStatus) {
	// if (Math.abs(dx) > Math.abs(dy)) {
	// // if (dx > 0)
	// // directiion = "right";
	// // else
	// // direction = "left";
	// } else {
	// if (dy > 0) {
	// DisplayMetrics metrics = new DisplayMetrics();
	// getWindowManager().getDefaultDisplay().getMetrics(
	// metrics);
	// int panelWidth = (int) ((metrics.widthPixels) * 0.80);
	// int panelHeight = (int) ((metrics.heightPixels) * 0.80);
	//
	// Constants.expandView(WalletInfoActivity.this,
	// panelWidth, panelHeight);
	// touchActionMoveStatus = false;
	// } else {
	// Constants.collapsView(WalletInfoActivity.this);
	// touchActionMoveStatus = false;
	// }
	// }
	//
	// }
	// break;
	// case MotionEvent.ACTION_UP: {
	// // finger leaves the screen
	// // Constants.collapsView(WalletInfoActivity.this);
	//
	// }
	// break;
	// }
	//
	// // tell the system that we handled the event and no further processing
	// // is required
	// return true;
	// }

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unRegisterBaseActivityReceiver();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (Constants.isExpanded) {
			Constants.collapsView(this);
		} else {
			super.onBackPressed();
			overridePendingTransition(R.anim.fadeout, R.anim.push_down_out);
		}
	}

	public class CheckBalanceTask extends AsyncTask<String, String, String> {
		// MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {

			String response = RequestClass.getInstance().readPay1B2CRequest(
					WalletInfoActivity.this, Constants.B2C_URL + "check_bal");
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			// if (this.dialog.isShowing()) {
			// this.dialog.dismiss();
			// }
			super.onPostExecute(result);
			try {
				// JSONArray array = new JSONArray(result);
				JSONObject jsonObject = new JSONObject(result);
				String status = jsonObject.getString("status");
				if (status.equalsIgnoreCase("SUCCESS")) {
					JSONObject description = jsonObject
							.getJSONObject("description");
					String account_balance = description
							.getString("account_balance");
					Utility.setBalance(WalletInfoActivity.this,
							Constants.SHAREDPREFERENCE_BALANCE,
							account_balance.trim());
					Constants.SELECTED_MENU_INDEX = Constants.PROFILE_MENU_INDEX;
					Constants.showNavigationBar(WalletInfoActivity.this);
				}
			} catch (JSONException e) {
				// e.printStackTrace();
				// Constants.showCustomToast(WalletHistoryActivity.this,
				// result);
			} catch (Exception e) {
				// TODO: handle exception
				// Constants.showCustomToast(WalletHistoryActivity.this,
				// result);
			}

		}

		@Override
		protected void onPreExecute() {
			// dialog = new MyProgressDialog(WalletHistoryActivity.this);
			// // this.dialog.setMessage("Please wait.....");
			// this.dialog.show();
			super.onPreExecute();

		}

	}
}