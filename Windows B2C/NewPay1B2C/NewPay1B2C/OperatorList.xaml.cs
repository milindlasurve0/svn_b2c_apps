﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using NewPay1B2C.Model;
using NewPay1B2C.UserControls;
using NewPay1B2C.UtilityClass;
using SQLiteClient;

namespace NewPay1B2C
	{
	public partial class OperatorList : PhoneApplicationPage
		{
		TextBlock textBlock1;
		string prevText = "";
		bool LayoutUpdateFlag = true;
		string flag;
		SQLiteConnection db = null;
		List<OperatorDB> result = null;
		List<Operator> resOperator = null;

		public OperatorList ()
			{
			InitializeComponent ();
			CreateDatabase ();
			GoogleAnalytics.EasyTracker.GetTracker ().SendView ("Search Operator");
			 flag = PhoneApplicationService.Current.State[Constant.OPERATOR_RECHARGE_FLAG].ToString ();
			//flag = "4";
			 resOperator = new List<Operator> ();
			if (flag.Equals ("1"))
				{
				textTitlePath.Text = "Recharge > Mobile > Operator ";
				}
			else if (flag.Equals ("3"))
				{
				textTitlePath.Text = "Recharge > Data card > Operator ";
				}
			else if (flag.Equals ("2"))
				{
				textTitlePath.Text = "Recharge > Dth > Operator ";
				}
			else if (flag.Equals ("4"))
				{
				textTitlePath.Text = "Mobile Bill > Operator ";
				}
			//Listloading();
			}

		private void ListBox_SelectionChanged (object sender, SelectionChangedEventArgs e)
			{
			ListBox listBox = sender as ListBox;

			if (listBox.SelectedIndex != -1)
				{
				var selected = result[listBox.SelectedIndex]; //listBox.SelectedValue as OperatorDB;
				string selectedId = selected.id;
				//NavigationService.Navigate (new Uri ("/RechargeAndBills.xaml?data=1", UriKind.Relative));
				PhoneApplicationService.Current.State["operatorData"] = selected;
				PhoneApplicationService.Current.State["data"] = "1";
				/*  PhoneApplicationService.Current.State["area_name"] = "";
				  PhoneApplicationService.Current.State["area"] = "";*/
				// PhoneApplicationService.Current.State[Constant.SELECTED_OPERATOR] = selected.name;
				NavigationService.GoBack ();

				}
			}

		protected override void OnNavigatedTo (NavigationEventArgs e)
			{
			// flag = PhoneApplicationService.Current.State[Constant.OPERATOR_RECHARGE_FLAG].ToString ();
			Listloading ();
			}

		private void CreateDatabase ()
			{
			if (db == null)
				{
				int rec;
				db = new SQLiteConnection (Constant.DATABASE_NAME);
				db.Open ();

				try
					{
					SQLiteCommand cmd = db.CreateCommand ("Create table OperatorTable(id text,name text,code text,p_id text UNIQUE,stv text,flag text,service_id text,delegateFlag text)");
					int i = cmd.ExecuteNonQuery ();
					}
				catch (SQLiteException ex)
					{
					}
				}
			}
		private void Listloading ()
			{

			try
				{
				if (db != null)
					{
					try
						{
						SQLiteCommand cmd1 = db.CreateCommand ("SELECT * FROM OperatorTable WHERE flag='" + flag + "'");
						var lst = cmd1.ExecuteQuery<OperatorDB> ();
						result = lst.ToList ();
						if (result.Count == 1)
							{
							}
						}
					catch (SQLiteException se)
						{
						string see = se.Message;
						}
					}
				}
			catch (SQLiteException ed)
				{
				MessageBox.Show ("Error");
				}
			//listBoxobj.ItemsSource = result;
			foreach (var item in result)
				{
				//OperatorUserControl oper = new OperatorUserControl ();
				Operator oper = new Operator ();
				oper.name = item.name;

				oper.imagePath = OperatorConstant.getOperatorResource (Convert.ToInt16 (item.p_id));
				//listBoxobj.Items.Add (oper);
				resOperator.Add (oper);
				}
			listBoxobj.ItemsSource = resOperator;
			}





		private void contactFilterString_TextChanged (object sender, TextChangedEventArgs e)
			{

			this.listBoxobj.ItemsSource = resOperator.Where (w => w.name.ToUpper ().Contains (SearchTextBox.Text.ToUpper ()));
			LayoutUpdateFlag = true;

			}

		private void listBoxobj_LayoutUpdated (object sender, EventArgs e)
			{
			if (LayoutUpdateFlag)
				{
				SearchVisualTree (listBoxobj);
				}
			LayoutUpdateFlag = false;
			}

		private void SearchVisualTree (DependencyObject targetElement)
			{

			var count = VisualTreeHelper.GetChildrenCount (targetElement);

			for (int i = 0; i < count; i++)
				{
				var child = VisualTreeHelper.GetChild (targetElement, i);
				if (child is TextBlock)
					{
					textBlock1 = (TextBlock)child;
					prevText = textBlock1.Text;
					if ((textBlock1.Name == "NameTxt" || textBlock1.Name == "AddrTxt") && textBlock1.Text.ToUpper ().Contains (SearchTextBox.Text.ToUpper ()))
						{
						HighlightText ();
						}
					}
				else
					{
					SearchVisualTree (child);
					}
				}
			}

		private void HighlightText ()
			{
			if (textBlock1 != null)
				{
				string text = textBlock1.Text.ToUpper ();
				textBlock1.Text = text;
				textBlock1.Inlines.Clear ();

				int index = text.IndexOf (SearchTextBox.Text.ToUpper ());
				int lenth = SearchTextBox.Text.Length;

				if (!(index < 0))
					{

					Run run = new Run () { Text = prevText.Substring (index, lenth) };
					run.Foreground = new SolidColorBrush (Colors.Orange);
					textBlock1.Inlines.Add (new Run () { Text = prevText.Substring (0, index) });
					textBlock1.Inlines.Add (run);
					textBlock1.Inlines.Add (new Run () { Text = prevText.Substring (index + lenth) });
					}
				}
			}

		private void buttonImage_Click_Back (object sender, RoutedEventArgs e)
			{
			NavigationService.GoBack ();
			}

		private void buttonImage_Click_Clear (object sender, RoutedEventArgs e)
			{
			SearchTextBox.Text = "";
			}
		}
	}