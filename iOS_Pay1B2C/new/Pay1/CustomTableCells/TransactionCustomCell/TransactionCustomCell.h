//
//  TransactionCustomCell.h
//  Pay1
//
//  Created by webninjaz on 09/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface TransactionCustomCell : UITableViewCell{
    

}

@property (nonatomic,strong)  IBOutlet UIImageView *transactionImageView;

@property (nonatomic,strong)  IBOutlet UILabel *transactionPhoneNumberLabel;
@property (nonatomic,strong)  IBOutlet UILabel *transactionTopUpsLabel;
@property (nonatomic,strong)  IBOutlet UILabel *transactionMoneyLabel;
@property (nonatomic,strong)  IBOutlet UIImageView *transactionCorrectImageView;

@property (nonatomic, strong) IBOutlet UIImageView *operatorLogoImageView;



@end
