package com.pay1;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.SQLException;
import android.graphics.Typeface;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.google.android.gcm.GCMRegistrar;
import com.pay1.constants.Constants;
import com.pay1.customviews.MyProgressDialog;
import com.pay1.databasehandler.Operator;
import com.pay1.databasehandler.OperatorDataSource;
import com.pay1.databasehandler.Plan;
import com.pay1.databasehandler.PlanDataSource;
import com.pay1.requesthandler.RequestClass;
import com.pay1.utilities.EditTextValidator;
import com.pay1.utilities.MyLocation;
import com.pay1.utilities.MyLocation.LocationResult;
import com.pay1.utilities.Utility;

public class RechargeMobileActivity extends AppBaseActivity implements
		OnClickListener {
	private static final String SCREEN_LABEL = "Mobile Screen";
	int flag = 1;
	Button btnPlans, btnContacts;// , mBtnRchCancel, mBtnRchNow;
	// ImageView btnContacts;
	ImageView imageView_Ok, imageView_Cancel, imageView_dropdown;
	LinearLayout radioLayout;
	ToggleButton toggleSTV;
	EditText editRechargeMobile, editRechargeAmount;
	private TextView textView_Name;
	/*
	 * RadioGroup radioRchTypeGroup; RadioButton radioTopUp, radioSTV;
	 */
	static int PICK_CONTACT = 100;
	static int PICK_OPERATOR = 111;
	TextView textView_Details, textView_Title, textView_Operator, textViewMob,
			textViewSpinner, textViewAmount, textView_Type;

	String circle_code = null, operatorCode = null, operatorID = null,
			operatorName = null, operatorProductID = null;
	ProgressBar progressBar1;

	Bundle bundle;
	static String isStv = "0";
	// PlanAdapterMobile plansAdapter;
	// ListView listPlan;

	private OperatorDataSource operatorDataSource;
	private PlanDataSource planDataSource;
	// private ScrollView scrollView;
	private boolean doubleBackToExitPressedOnce = false;

	private EasyTracker easyTracker = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.recharge_mobile_activity);
		try {
			// Initialize GA
			// Pay1GA pay1GA = new Pay1GA(RechargeMobileActivity.this);
			// Pay1GA.getGaTracker().set(Fields.SCREEN_NAME, SCREEN_LABEL);
			// try {
			// MemoryManager.clearApplicationData(RechargeMobileActivity.this);
			//
			// // Log.e("Memory ", MemoryManager.getTotalMemory() + "   "
			// // + MemoryManager.getFreeMemory());
			//
			// // MemoryManager.getInstance().clearApplicationData();
			// //
			// // Log.e("Memory ", MemoryManager.getInstance().getTotalMemory()
			// // + "   " + MemoryManager.getInstance().getFreeMemory());
			// } catch (Exception e) {
			// }
			easyTracker = EasyTracker.getInstance(RechargeMobileActivity.this);

			findViewById();
			registerBaseActivityReceiver();

			// scrollView = (ScrollView) findViewById(R.id.scrollView);

			operatorDataSource = new OperatorDataSource(
					RechargeMobileActivity.this);

			try {
				operatorDataSource.open();

				List<Operator> operator = operatorDataSource.getAllOperator();

				if (operator.size() != 0) {
					int days = (int) ((System.currentTimeMillis() - operator
							.get(0).getOperatorUptateTime()) / (1000 * 60 * 60 * 24));
					if (days >= 2) {
						operatorDataSource.deleteOperator();
						new GetOperatorTask().execute();
					}
				} else {
					new GetOperatorTask().execute();
				}

			} catch (SQLException exception) {
				// TODO: handle exception
				// // Log.e("Er ", exception.getMessage());
			} catch (Exception e) {
				// TODO: handle exception
				// // Log.e("Er ", e.getMessage());
			} finally {
				operatorDataSource.close();
			}

			bundle = getIntent().getExtras();
			flag = 1;

			imageView_Ok.setOnClickListener(this);
			imageView_Cancel.setOnClickListener(this);
			btnContacts.setOnClickListener(this);
			btnPlans.setOnClickListener(this);
			textView_Operator.setOnClickListener(this);
			imageView_dropdown.setOnClickListener(this);

			Typeface Normal = Typeface.createFromAsset(getAssets(),
					"EncodeSansNormal-100-Thin.ttf");
			Typeface Reguler = Typeface.createFromAsset(getAssets(),
					"EncodeSansNormal-400-Regular.ttf");
			Typeface Medium = Typeface.createFromAsset(getAssets(),
					"EncodeSansNarrow-500-Medium.ttf");
			// Typeface Awesome = Typeface.createFromAsset(getAssets(),
			// "fontawesome-webfont.ttf");
			//
			// TextView textView_Test = (TextView)
			// findViewById(R.id.textView_Test);
			// textView_Test.setTypeface(Awesome);
			// textView_Test.setText(getResources().getString(
			// R.string.icon_right_arrow)
			// + "   "
			// + getResources().getString(R.string.icon_filled_circle));
			// textView_Test.setTextColor(getResources().getColor(
			// R.color.app_blue_color));
			// textView_Test.setTextSize(25);

			textView_Details.setTypeface(Normal);
			textView_Title.setTypeface(Reguler);
			textViewAmount.setTypeface(Medium);
			textViewMob.setTypeface(Medium);
			textView_Type.setTypeface(Medium);
			textViewSpinner.setTypeface(Medium);
			btnPlans.setTypeface(Medium);
			editRechargeMobile.setTypeface(Normal);
			editRechargeAmount.setTypeface(Normal);
			textView_Operator.setTypeface(Normal);
			planDataSource = new PlanDataSource(RechargeMobileActivity.this);

			toggleSTV.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (toggleSTV.isChecked()) {
						isStv = "1";
					} else {
						isStv = "0";
					}
				}
			});
			editRechargeMobile.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					// TODO Auto-generated method stub

					if (editRechargeMobile.getText().toString().trim().length() > 0) {
						if (EditTextValidator.hasText(
								RechargeMobileActivity.this,
								editRechargeMobile,
								Constants.ERROR_MOBILE_BLANK_FIELD))
							btnContacts.setVisibility(View.VISIBLE);
						else
							btnContacts.setVisibility(View.GONE);

						textView_Operator.setText("");
						btnPlans.setVisibility(View.GONE);
						radioLayout.setVisibility(View.GONE);
						isStv = "0";
					}

					if (editRechargeMobile.length() == 10) {
						if (editRechargeMobile.getText().toString().trim()
								.startsWith("9")
								|| editRechargeMobile.getText().toString()
										.trim().startsWith("8")
								|| editRechargeMobile.getText().toString()
										.trim().startsWith("7")) {
							editRechargeAmount.requestFocus();
							new GetPlansCircletask().execute();
						} else {
							Constants.showOneButtonDialog(
									RechargeMobileActivity.this, "Mobile",
									Constants.ERROR_MOBILE_LENGTH_FIELD,
									Constants.DIALOG_CLOSE);
						}
					}
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					// TODO Auto-generated method stub

				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

				}
			});

			editRechargeMobile
					.setOnEditorActionListener(new TextView.OnEditorActionListener() {

						@Override
						public boolean onEditorAction(TextView v, int actionId,
								KeyEvent event) {
							// TODO Auto-generated method stub
							if (actionId == EditorInfo.IME_ACTION_NEXT) {
								if (EditTextValidator.isValidMobileNumber(
										RechargeMobileActivity.this,
										editRechargeMobile,
										Constants.ERROR_MOBILE_LENGTH_FIELD))
									btnContacts.setVisibility(View.VISIBLE);
								else
									btnContacts.setVisibility(View.GONE);
								return true;
							}
							return false;
						}
					});

			textView_Operator.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					// TODO Auto-generated method stub
					if (textView_Operator.getText().toString().trim().length() > 0)
						if (EditTextValidator.hasText(
								RechargeMobileActivity.this, textView_Operator,
								Constants.ERROR_OPERATOR_BLANK_FIELD))
							imageView_dropdown.setVisibility(View.VISIBLE);
						else
							imageView_dropdown.setVisibility(View.GONE);
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					// TODO Auto-generated method stub

				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

				}
			});

			editRechargeAmount.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					// TODO Auto-generated method stub
					if (editRechargeAmount.getText().toString().trim().length() > 0)
						if (EditTextValidator.hasText(
								RechargeMobileActivity.this,
								editRechargeAmount,
								Constants.ERROR_AMOUNT_BLANK_FIELD))
							btnPlans.setVisibility(View.VISIBLE);
						else
							btnPlans.setVisibility(View.GONE);
					if (editRechargeMobile.getText().toString().trim().length() == 10
							&& editRechargeAmount.getText().toString().trim()
									.length() >= 2) {
						if (circle_code != null) {
							progressBar1.setVisibility(View.VISIBLE);
							// Toast.makeText(RechargeMobileActivity.this,
							// pay1OpId,
							// Toast.LENGTH_SHORT).show();
							getPlan(operatorProductID,
									circle_code,
									Integer.parseInt(editRechargeAmount
											.getText().toString().trim()));
						}
					}

				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					// TODO Auto-generated method stub

				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

				}
			});

			if (bundle.getInt(Constants.RECHARGE_FOR) == Constants.RECHARGE_MOBILE) {
				// Pay1GA.getGaTracker().set(Fields.SCREEN_NAME, SCREEN_LABEL);

				easyTracker.set(Fields.SCREEN_NAME, SCREEN_LABEL);
				textView_Title.setText("Mobile");

				new CheckUpdateTask().execute();
			} else if (bundle.getInt(Constants.RECHARGE_FOR) == Constants.RECHARGE_DATA) {
				// Pay1GA.getGaTracker().set(Fields.SCREEN_NAME,
				// "Data Card Screen");

				easyTracker.set(Fields.SCREEN_NAME, "Data Card Screen");
				textView_Title.setText("Data Card");
			}

			GCMRegistrar.checkDevice(this);
			GCMRegistrar.checkManifest(this);
			checkNotNull(Constants.SENDER_ID, "SENDER_ID");

			final String regId = GCMRegistrar.getRegistrationId(this);
			// // Log.i(TAG, "registration id =====  " + regId);
			Utility.setGCMID(RechargeMobileActivity.this,
					Constants.SHAREDPREFERENCE_GCMID, regId);
			if (regId.equals("")) {
				GCMRegistrar.register(this, Constants.SENDER_ID);
			} else {
				// // Log.v(TAG, "Already registered");
			}

			try {
				LocationResult locationResult = new LocationResult() {
					@Override
					public void gotLocation(Location location) {
						// Got the location!
						try {

							Utility.setCurrentLatitude(
									RechargeMobileActivity.this,
									Constants.SHAREDPREFERENCE_CURRENT_LATITUDE,
									String.valueOf(location.getLatitude()));
							Utility.setCurrentLongitude(
									RechargeMobileActivity.this,
									Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE,
									String.valueOf(location.getLongitude()));

						} catch (Exception exception) {
						}
					}
				};
				MyLocation myLocation = new MyLocation();
				myLocation.getLocation(this, locationResult);
			} catch (Exception exception) {
			}
		} catch (Exception e) {
		}

		// overridePendingTransition(R.anim.overshoot, R.anim.fadeout);
		if (Utility.getOneTime(RechargeMobileActivity.this,
				Constants.SHAREDPREFERENCE_ONE_TIME)) {
			overridePendingTransition(0, 0);
		} else {
			overridePendingTransition(R.anim.push_up_in, R.anim.fadeout);
		}

	}

	protected void checkForSTV(int pay1OpId2) {

		if (pay1OpId2 == 1) {
			radioLayout.setVisibility(View.VISIBLE);
		} else {
			radioLayout.setVisibility(View.GONE);
			isStv = "0";
		}
	}

	private void findViewById() {
		// TODO Auto-generated method stub
		btnPlans = (Button) findViewById(R.id.btnPlans);
		btnPlans.setVisibility(View.GONE);
		textView_Details = (TextView) findViewById(R.id.textView_Details);
		textViewMob = (TextView) findViewById(R.id.textViewMob);
		textView_Type = (TextView) findViewById(R.id.textView_Type);
		textViewSpinner = (TextView) findViewById(R.id.textViewSpinner);
		textViewAmount = (TextView) findViewById(R.id.textViewAmount);
		textView_Title = (TextView) findViewById(R.id.textView_Title);
		radioLayout = (LinearLayout) findViewById(R.id.radioLayout);
		/*
		 * radioRchTypeGroup = (RadioGroup)
		 * findViewById(R.id.radioRechargeType); radioSTV = (RadioButton)
		 * findViewById(R.id.radioSTV); radioTopUp = (RadioButton)
		 * findViewById(R.id.radioTopup);
		 */
		editRechargeMobile = (EditText) findViewById(R.id.editRechargeMobile);
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(editRechargeMobile.getWindowToken(), 0);
		btnContacts = (Button) findViewById(R.id.btnContacts);
		textView_Operator = (TextView) findViewById(R.id.textView_Operator);
		editRechargeAmount = (EditText) findViewById(R.id.editRechargeAmount);
		imageView_Ok = (ImageView) findViewById(R.id.imageView_Ok);
		toggleSTV = (ToggleButton) findViewById(R.id.toggleSTV);

		imageView_Cancel = (ImageView) findViewById(R.id.imageView_Cancel);
		imageView_dropdown = (ImageView) findViewById(R.id.imageView_dropdown);
		// listPlan = (ListView) findViewById(R.id.listPlan);

		progressBar1 = (ProgressBar) findViewById(R.id.progressBar1);
		progressBar1.setVisibility(View.GONE);

		textView_Name = (TextView) findViewById(R.id.textView_Name);
	}

	private boolean checkValidation(Context context) {
		boolean ret = true;

		if (!EditTextValidator.hasText(context, editRechargeMobile,
				Constants.ERROR_MOBILE_BLANK_FIELD)) {
			ret = false;
			editRechargeMobile.requestFocus();
			btnContacts.setVisibility(View.GONE);
			return ret;
		} else if (!EditTextValidator.isValidMobileNumber(context,
				editRechargeMobile, Constants.ERROR_MOBILE_LENGTH_FIELD)) {
			ret = false;
			editRechargeMobile.requestFocus();
			btnContacts.setVisibility(View.GONE);
			return ret;
		} else if (!EditTextValidator.hasText(context, textView_Operator,
				Constants.ERROR_OPERATOR_BLANK_FIELD)) {
			ret = false;
			textView_Operator.requestFocus();
			imageView_dropdown.setVisibility(View.GONE);
			return ret;
		} else if (!EditTextValidator.hasText(context, editRechargeAmount,
				Constants.ERROR_AMOUNT_BLANK_FIELD)) {
			ret = false;
			editRechargeAmount.requestFocus();
			btnPlans.setVisibility(View.GONE);
			return ret;
		} else if (!EditTextValidator.isValidAmount(context,
				editRechargeAmount, Constants.ERROR_AMOUNT_VALID_FIELD)) {
			ret = false;
			editRechargeAmount.requestFocus();
			btnPlans.setVisibility(View.GONE);
			return ret;
		} else
			return ret;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		try {
			if (resultCode == Activity.RESULT_OK) {
				if (requestCode == Constants.PLAN_REQUEST) {
					editRechargeAmount.setText(data.getExtras().getString(
							"result"));
				}
				if (requestCode == PICK_OPERATOR) {
					textView_Operator.setText(data.getExtras().getString(
							"OperatorName"));
					btnPlans.setVisibility(View.VISIBLE);
					operatorCode = data.getExtras().getString("OperatorCode");
					operatorID = data.getExtras().getString("OperatorID");
					operatorProductID = data.getExtras().getString(
							"OperatorProductID");
					int d = Integer.parseInt(data.getExtras().getString("STV"));
					checkForSTV(d);
				}
				if (requestCode == PICK_CONTACT) {
					overridePendingTransition(R.anim.slide_in_left,
							R.anim.slide_out_right);
					Uri contactData = data.getData();
					Cursor contactCursor = getContentResolver().query(
							contactData,
							new String[] { ContactsContract.Contacts._ID },
							null, null, null);
					String id = null;
					if (contactCursor.moveToFirst()) {
						id = contactCursor.getString(contactCursor
								.getColumnIndex(ContactsContract.Contacts._ID));
					}
					contactCursor.close();
					String phoneNumber = null, displayName = null;
					Cursor phoneCursor = getContentResolver()
							.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
									new String[] {
											ContactsContract.CommonDataKinds.Phone.NUMBER,
											ContactsContract.PhoneLookup.DISPLAY_NAME },
									ContactsContract.CommonDataKinds.Phone.CONTACT_ID
											+ "= ? ", new String[] { id }, null);
					if (phoneCursor.moveToFirst()) {
						phoneNumber = phoneCursor
								.getString(phoneCursor
										.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
						displayName = phoneCursor
								.getString(phoneCursor
										.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
						phoneNumber = Normalizer.normalize(phoneNumber,
								Normalizer.Form.NFD).replaceAll("[^a-zA-Z0-9]",
								"");
						if (phoneNumber.length() >= 10) {
							phoneNumber = phoneNumber.substring(
									phoneNumber.length() - 10,
									phoneNumber.length());
							editRechargeMobile.setText(phoneNumber);
							if (displayName.equalsIgnoreCase(phoneNumber))
								textView_Name.setText("");
							else
								textView_Name.setText(displayName);
						} else {
							Toast.makeText(RechargeMobileActivity.this,
									"Not a valid mobile number.",
									Toast.LENGTH_LONG).show();
							phoneNumber = "";
							displayName = "";
							editRechargeMobile.setText("");
							textView_Name.setText("");
						}

					} else {
						Toast.makeText(RechargeMobileActivity.this,
								"Not a valid mobile number.", Toast.LENGTH_LONG)
								.show();
						phoneNumber = "";
						displayName = "";
						editRechargeMobile.setText("");
						textView_Name.setText("");
					}
					phoneCursor.close();
				}

			} else {
				if (requestCode == PICK_CONTACT) {
					overridePendingTransition(R.anim.slide_in_left,
							R.anim.slide_out_right);
				}
			}
		} catch (Exception e) {
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.textView_Operator: {
			Intent intent = new Intent(RechargeMobileActivity.this,
					OperatorActivity.class);
			if (bundle.getInt(Constants.RECHARGE_FOR) == Constants.RECHARGE_MOBILE) {
				intent.putExtra(Constants.RECHARGE_FOR,
						Constants.RECHARGE_MOBILE);
			} else if (bundle.getInt(Constants.RECHARGE_FOR) == Constants.RECHARGE_DATA) {
				intent.putExtra(Constants.RECHARGE_FOR, Constants.RECHARGE_DATA);
			}
			startActivityForResult(intent, PICK_OPERATOR);
		}
			break;
		case R.id.imageView_dropdown: {
			Intent intent = new Intent(RechargeMobileActivity.this,
					OperatorActivity.class);
			if (bundle.getInt(Constants.RECHARGE_FOR) == Constants.RECHARGE_MOBILE) {
				intent.putExtra(Constants.RECHARGE_FOR,
						Constants.RECHARGE_MOBILE);
			} else if (bundle.getInt(Constants.RECHARGE_FOR) == Constants.RECHARGE_DATA) {
				intent.putExtra(Constants.RECHARGE_FOR, Constants.RECHARGE_DATA);
			}
			startActivityForResult(intent, PICK_OPERATOR);
		}
			break;
		case R.id.btnContacts: {
			Intent intent = new Intent(Intent.ACTION_PICK,
					ContactsContract.Contacts.CONTENT_URI);
			startActivityForResult(intent, PICK_CONTACT);
			overridePendingTransition(R.anim.slide_in_right,
					R.anim.slide_out_left);
		}
			break;
		case R.id.imageView_Cancel:
			editRechargeAmount.setText("");
			editRechargeMobile.setText("");
			editRechargeMobile.requestFocus();
			textView_Operator.setText("");
			btnPlans.setVisibility(View.GONE);
			textView_Details.setVisibility(View.GONE);
			progressBar1.setVisibility(View.GONE);
			textView_Name.setText("");
			radioLayout.setVisibility(View.GONE);
			isStv = "0";
			break;
		case R.id.imageView_Ok: {
			if (Utility.getLoginFlag(RechargeMobileActivity.this,
					Constants.SHAREDPREFERENCE_IS_LOGIN)) {

				if (!checkValidation(this)) {

				} else {
					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

					String charges_slab = null;
					double service_charge_amount = 0, service_charge_percent = 0, service_tax_percent = 0, min = 0, max = 0;
					try {
						operatorDataSource.open();

						List<Operator> operators;
						if (bundle.getInt(Constants.RECHARGE_FOR) == Constants.RECHARGE_MOBILE) {
							operators = operatorDataSource.getAllOperator(
									Constants.RECHARGE_MOBILE, operatorCode);
						} else {
							operators = operatorDataSource.getAllOperator(
									Constants.RECHARGE_DATA, operatorCode);
						}
						if (operators.size() != 0) {
							charges_slab = operators.get(0)
									.getOperatorChargeSlab();
							service_charge_amount = operators.get(0)
									.getOperatorServiceChargeAmount();
							service_charge_percent = operators.get(0)
									.getOperatorServiceChargePercent();
							service_tax_percent = operators.get(0)
									.getOperatorServiceTaxPercent();
							min = operators.get(0).getOperatorMinAmount();
							max = operators.get(0).getOperatorMaxAmount();
						}

					} catch (SQLException exception) {
						// TODO: handle exception
						// // Log.e("Er ", exception.getMessage());
					} catch (Exception e) {
						// TODO: handle exception
						// // Log.e("Er ", e.getMessage());
					} finally {
						operatorDataSource.close();
					}

					double rechargeAmount = 0, balance = 0;
					try {
						rechargeAmount = Double.parseDouble(editRechargeAmount
								.getText().toString().trim());
						balance = Double.parseDouble(Utility.getBalance(
								RechargeMobileActivity.this,
								Constants.SHAREDPREFERENCE_BALANCE));
					} catch (Exception e) {
						rechargeAmount = 0;
						balance = 0;
					}
					if (rechargeAmount < min || rechargeAmount > max) {
						EditTextValidator.isValidAmount(
								RechargeMobileActivity.this,
								editRechargeAmount,
								Constants.ERROR_AMOUNT_RANG_FIELD + min
										+ " to " + max, min, max);
						editRechargeAmount.requestFocus();
						return;
					}
					int slab_amount = 0;
					try {
						if (charges_slab.equalsIgnoreCase("null")
								|| charges_slab.equalsIgnoreCase("")) {
							rechargeAmount = (rechargeAmount + (rechargeAmount * service_charge_percent));
							rechargeAmount = (rechargeAmount + (rechargeAmount * service_tax_percent));
						} else {
							String[] comma_seperator = charges_slab.split(",");
							for (String s : comma_seperator) {
								String[] colon_sperator = s.split(":");
								if (rechargeAmount > Double
										.parseDouble(colon_sperator[0])) {
									slab_amount = Integer
											.parseInt(colon_sperator[1]);
								}
							}
						}
					} catch (Exception e) {
					}
					if ((rechargeAmount + slab_amount) > balance) {
						Intent orderIntent = new Intent(
								RechargeMobileActivity.this,
								OrderSummaryForLowBalActivity.class);
						orderIntent.putExtra(Constants.OPERATOR_NAME,
								textView_Operator.getText().toString());
						orderIntent.putExtra(Constants.MOBILE_NUMBER,
								editRechargeMobile.getText().toString().trim());
						orderIntent.putExtra(Constants.RECHARGE_AMOUNT,
								editRechargeAmount.getText().toString().trim());
						orderIntent
								.putExtra(Constants.CHARGE_SLAB, slab_amount);
						orderIntent.putExtra(Constants.CONTACT_NAME,
								textView_Name.getText().toString().trim());
						orderIntent.putExtra(Constants.OPERATOR_ID, operatorID);
						Log.w("STV", isStv);
						orderIntent.putExtra(Constants.STV_RECHARGE, isStv);
						orderIntent.putExtra(Constants.RECHARGE_FOR,
								bundle.getInt(Constants.RECHARGE_FOR));
						startActivity(orderIntent);

					} else {
						Intent orderIntent = new Intent(
								RechargeMobileActivity.this,
								OrderSummaryActivity.class);
						orderIntent.putExtra(Constants.OPERATOR_NAME,
								textView_Operator.getText().toString());
						orderIntent.putExtra(Constants.MOBILE_NUMBER,
								editRechargeMobile.getText().toString().trim());
						orderIntent.putExtra(Constants.RECHARGE_AMOUNT,
								editRechargeAmount.getText().toString().trim());
						orderIntent
								.putExtra(Constants.CHARGE_SLAB, slab_amount);
						orderIntent.putExtra(Constants.CONTACT_NAME,
								textView_Name.getText().toString().trim());
						orderIntent.putExtra(Constants.OPERATOR_ID, operatorID);
						Log.w("STV", isStv);
						orderIntent.putExtra(Constants.STV_RECHARGE, isStv);
						orderIntent.putExtra(Constants.RECHARGE_FOR,
								bundle.getInt(Constants.RECHARGE_FOR));
						startActivity(orderIntent);
					}
				}
			} else {
				// Constants.showOneButtonDialog(RechargeMobileActivity.this,
				// "Recharge ", "Please Login First.",
				// Constants.DIALOG_CLOSE_LOGIN);
				Intent intent = new Intent(RechargeMobileActivity.this,
						LoginForRechargeActivity.class);
				startActivity(intent);
			}
		}
			break;
		case R.id.btnPlans:
			Intent plansIntent = new Intent(RechargeMobileActivity.this,
					PlanFragmentActivity.class);
			if (bundle.getInt(Constants.RECHARGE_FOR) == Constants.RECHARGE_MOBILE) {
				plansIntent.putExtra(Constants.RECHARGE_FOR,
						Constants.RECHARGE_MOBILE);
			} else if (bundle.getInt(Constants.RECHARGE_FOR) == Constants.RECHARGE_DATA) {
				plansIntent.putExtra(Constants.RECHARGE_FOR,
						Constants.RECHARGE_DATA);
			}
			if (editRechargeMobile.getText().toString().trim().length() != 0) {
				plansIntent.putExtra(Constants.PLAN_JSON, operatorProductID);
				plansIntent.putExtra("CIRCLE_CODE", circle_code);
				plansIntent.putExtra("DATA_CARD_PLAN", 0);
			} else {
				plansIntent.putExtra(Constants.PLAN_JSON, operatorProductID);
				plansIntent.putExtra("CIRCLE_CODE", "");
				plansIntent.putExtra("DATA_CARD_PLAN", 0);
			}
			startActivityForResult(plansIntent, Constants.PLAN_REQUEST);
			break;
		default:
			break;
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView().build());
		// EasyTracker.getInstance(this).activityStart(this);
		easyTracker.send(MapBuilder.createAppView().build());
	}

	@Override
	public void onStop() {
		super.onStop();
		// EasyTracker.getInstance(this).activityStop(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (Constants.isExpanded) {
			Constants.collapsView(this);
		} else {
			Constants.SELECTED_MENU_INDEX = Constants.RECHARGE_MENU_INDEX;
			Constants.showNavigationBar(RechargeMobileActivity.this);
			// overridePendingTransition(R.anim.translate_activity_down,
			// R.anim.linear);
			if (Utility.getOneTime(RechargeMobileActivity.this,
					Constants.SHAREDPREFERENCE_ONE_TIME)) {
				Utility.setOneTime(RechargeMobileActivity.this,
						Constants.SHAREDPREFERENCE_ONE_TIME, false);
				DisplayMetrics metrics = new DisplayMetrics();
				getWindowManager().getDefaultDisplay().getMetrics(metrics);
				int panelWidth = (int) ((metrics.widthPixels) * 0.80);
				int panelHeight = (int) ((metrics.heightPixels) * 0.80);

				Constants.expandView(RechargeMobileActivity.this, panelWidth,
						panelHeight);
			}
			flag = 1;
		}
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unRegisterBaseActivityReceiver();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// if (Constants.isExpanded) {
		// Constants.collapsView(this);
		// } else {
		// super.onBackPressed();
		// overridePendingTransition(R.anim.fadeout, R.anim.push_down_out);
		// }
		if (Constants.isExpanded) {
			Constants.collapsView(this);
		} else {
			if (doubleBackToExitPressedOnce) {
				super.onBackPressed();
				overridePendingTransition(R.anim.fadeout, R.anim.push_down_out);
				closeAllActivitiesInApp();
				return;
			}

			this.doubleBackToExitPressedOnce = true;
			Toast.makeText(this, "Press again to exit.", Toast.LENGTH_SHORT)
					.show();

			new Handler().postDelayed(new Runnable() {

				@Override
				public void run() {
					doubleBackToExitPressedOnce = false;
				}
			}, 10000);
		}
		// if (Constants.isExpanded) {
		// if (doubleBackToExitPressedOnce) {
		// super.onBackPressed();
		// overridePendingTransition(R.anim.fadeout, R.anim.push_down_out);
		// } else {
		// Constants.collapsView(this);
		// }
		// } else if (!Constants.isExpanded) {
		// if (doubleBackToExitPressedOnce) {
		// super.onBackPressed();
		// overridePendingTransition(R.anim.fadeout, R.anim.push_down_out);
		// } else {
		// DisplayMetrics metrics = new DisplayMetrics();
		// getWindowManager().getDefaultDisplay().getMetrics(metrics);
		// int panelWidth = (int) ((metrics.widthPixels) * 0.80);
		// int panelHeight = (int) ((metrics.heightPixels) * 0.80);
		// Constants.expandView(RechargeMobileActivity.this, panelWidth,
		// panelHeight);
		// }
		// }
		// if (!doubleBackToExitPressedOnce) {
		// this.doubleBackToExitPressedOnce = true;
		// Toast.makeText(this, "Press again to exit.",
		// Toast.LENGTH_SHORT).show();
		// }
		//
		// new Handler().postDelayed(new Runnable() {
		//
		// @Override
		// public void run() {
		// doubleBackToExitPressedOnce = false;
		// }
		// }, 2000);
		// return;
	}

	private void checkNotNull(Object reference, String name) {
		if (reference == null) {
			throw new NullPointerException("Errror");
		}
	}

	public class GetPlansCircletask extends AsyncTask<String, String, String> {
		MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {

			String response = RequestClass.getInstance().readPay1B2BRequest(
					RechargeMobileActivity.this,
					Constants.B2B_URL + "method=getMobileDetails&mobile="
							+ editRechargeMobile.getText().toString().trim());
			/*
			 * String response = RequestClass.getInstance().readPay1Request(
			 * RechargeMobileActivity.this, Constants.URL, listValuePair);
			 */
			String replaced = response.replace("(", "").replace(")", "")
					.replace(";", "");

			return replaced;

		}

		@Override
		protected void onPostExecute(String result) {
			// if (this.dialog.isShowing()) {
			// this.dialog.dismiss();
			// }
			super.onPostExecute(result);
			try {
				JSONArray array = new JSONArray(result);
				JSONObject jsonObject = array.getJSONObject(0);
				String status = jsonObject.getString("status");
				if (status.equalsIgnoreCase("SUCCESS")) {

					JSONObject jsonObjectdetails = jsonObject
							.getJSONObject("details");
					circle_code = jsonObjectdetails.getString("area")
							.toString().trim();
					operatorCode = jsonObjectdetails.getString("operator")
							.toString().trim();

					try {
						operatorDataSource.open();

						List<Operator> operators;
						if (bundle.getInt(Constants.RECHARGE_FOR) == Constants.RECHARGE_MOBILE) {
							operators = operatorDataSource.getAllOperator(
									Constants.RECHARGE_MOBILE, operatorCode);
						} else {
							operators = operatorDataSource.getAllOperator(
									Constants.RECHARGE_DATA, operatorCode);
						}
						if (operators.size() != 0) {
							operatorName = operators.get(0).getOperatorName();
							operatorProductID = String.valueOf(operators.get(0)
									.getOperatorProductID());
							operatorID = String.valueOf(operators.get(0)
									.getOperatorID());
							checkForSTV(operators.get(0).getOperatorSTV());
						}

					} catch (SQLException exception) {
						// TODO: handle exception
						// // Log.e("Er ", exception.getMessage());
					} catch (Exception e) {
						// TODO: handle exception
						// // Log.e("Er ", e.getMessage());
					} finally {
						operatorDataSource.close();
					}
					textView_Operator.setText(operatorName);
					btnPlans.setVisibility(View.VISIBLE);

					// if (circle_code != null && operatorCode != null) {
					// btnPlans.setVisibility(View.VISIBLE);
					// }
				}
			} catch (JSONException e) {
				// e.printStackTrace();
				// Constants.showCustomToast(RechargeMobileActivity.this,
				// result);
			} catch (Exception e) {
				// TODO: handle exception
				// Constants.showCustomToast(RechargeMobileActivity.this,
				// result);
			}

		}

		@Override
		protected void onPreExecute() {
			// dialog = new MyProgressDialog(PlansActivity.this);
			// // this.dialog.setMessage("Please wait.....");
			// this.dialog.show();
			super.onPreExecute();

		}

	}

	protected void getPlan(String operator, String circle, int amount) {

		try {
			planDataSource.open();

			final List<Plan> plans = planDataSource.getPlanDetails(operator,
					circle, amount, 0);
			List<Plan> data = planDataSource.getAllPlan(operator, circle);
			/*
			 * List<Plan> planUniqList=new ArrayList<Plan>(); Set<Plan>
			 * uniquePlan = new HashSet<Plan>(plans); for (Plan plan :
			 * uniquePlan) { planUniqList.add(plan); }
			 */
			// plansAdapter = new PlanAdapterMobile(RechargeMobileActivity.this,
			// 1, plans);
			// listPlan.setAdapter(plansAdapter);
			// listPlan.setOnItemClickListener(new OnItemClickListener() {
			//
			// @Override
			// public void onItemClick(AdapterView<?> arg0, View arg1,
			// int position, long arg3) {
			// // TODO Auto-generated method stub
			// int amount = plans.get(position).getPlanAmount();
			// editRechargeAmount.setText(amount + "");
			// textView_Details.setVisibility(View.VISIBLE);
			// textView_Details.setText(plans.get(position)
			// .getPlanDescription());
			// }
			// });
			if (plans.size() == 0 && data.size() == 0) {
				new GetPlanstask().execute();
			} else {
				if (plans.size() != 0) {
					String LastUpdateTime = plans.get(0).getPlanUptateTime();
					int days = (int) ((System.currentTimeMillis() - Long
							.parseLong(LastUpdateTime)) / (1000 * 60 * 60 * 24));

					if (days >= 1) {
						planDataSource.deletePlan();
						new GetPlanstask().execute();
					} else {
						textView_Details.setText(plans.get(0)
								.getPlanDescription());
						textView_Details.setVisibility(View.VISIBLE);
						progressBar1.setVisibility(View.GONE);
						textView_Details.setTextColor(getResources().getColor(
								R.color.Black));
					}
				} else {
					textView_Details
							.setText("No such plan exist in our system. Kindly check with operator.");
					textView_Details.setVisibility(View.VISIBLE);
					progressBar1.setVisibility(View.GONE);
					textView_Details.setTextColor(getResources().getColor(
							R.color.Red));
				}
			}

		} catch (SQLException exception) {

			// TODO: handle exception
			// // Log.e("Er ", exception.getMessage());
		} catch (Exception e) {
			// TODO: handle exception
			// // Log.e("Er ", e.getMessage());
		} finally {
			planDataSource.close();
			// progressBar1.setVisibility(View.GONE);
		}

	}

	public class GetPlanstask extends AsyncTask<String, String, String> {
		// MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

			listValuePair
					.add(new BasicNameValuePair("method", "getPlanDetails"));
			listValuePair.add(new BasicNameValuePair("operator",
					operatorProductID));
			listValuePair.add(new BasicNameValuePair("circle", circle_code));

			String response = RequestClass.getInstance().readPay1B2CRequest(
					RechargeMobileActivity.this, Constants.B2B_URL,
					listValuePair);

			String replaced = response.replace("(", "").replace(")", "")
					.replace(";", "");
			savePlansTodataBase(replaced);

			return replaced;

		}

		@Override
		protected void onPostExecute(String result) {
			// if (this.dialog.isShowing()) {
			// this.dialog.dismiss();
			// }

			super.onPostExecute(result);
			try {
				if (result != null && !result.startsWith("Error") && flag != 0) {
					flag--;
					getPlan(operatorProductID,
							circle_code,
							Integer.parseInt(editRechargeAmount.getText()
									.toString().trim()));
				} else {
					textView_Details
							.setText("No such plan exist in our system. Kindly check with operator.");
					textView_Details.setVisibility(View.VISIBLE);
					textView_Details.setTextColor(getResources().getColor(
							R.color.Red));
				}
			} catch (Exception e) {
				// TODO: handle exception
				// Constants.showCustomToast(RechargeMobileActivity.this,
				// result);
			}

			progressBar1.setVisibility(View.GONE);
		}

		@Override
		protected void onPreExecute() {
			// dialog = new MyProgressDialog(RechargeActivity.this);
			// // this.dialog.setMessage("Please wait.....");
			// this.dialog.show();
			progressBar1.setVisibility(View.VISIBLE);
			super.onPreExecute();

		}

	}

	private void savePlansTodataBase(String planJson) {
		try {
			planDataSource.open();
			// ContentValues contentValues = new ContentValues();
			JSONArray array = new JSONArray(planJson);
			JSONObject planJsonObject = array.getJSONObject(0);

			Iterator<String> operatorKey = planJsonObject.keys();

			if (operatorKey.hasNext()) {

				long time = System.currentTimeMillis();

				JSONObject jsonObject2 = planJsonObject
						.getJSONObject(operatorKey.next());

				String opr_name = jsonObject2.getString("opr_name");
				String prod_code_pay1 = jsonObject2.getString("prod_code_pay1");
				// contentValues.put("operator_id", prod_code_pay1);
				// contentValues.put("operator_name", opr_name);
				// contentValues.put("prod_code_pay1", prod_code_pay1);
				JSONObject circleJsonObject = jsonObject2
						.getJSONObject("circles");
				Iterator<String> circleKey = circleJsonObject.keys();
				if (circleKey.hasNext()) {
					JSONObject circleJsonObject2 = circleJsonObject
							.getJSONObject(circleKey.next().toString());
					String circleName = circleJsonObject2
							.getString("circle_name");

					// contentValues.put("circle_name", circleName);

					JSONObject plansJsonObject = circleJsonObject2
							.getJSONObject("plans");
					String circle_id = circleJsonObject2.getString("circle_id");
					Iterator<String> planKey = plansJsonObject.keys();
					Iterator<String> planKeyDB = plansJsonObject.keys();
					// contentValues.put("circle_id", circle_id);

					while (planKey.hasNext()) {
						String planType = planKey.next().toString();
						JSONArray planArray = plansJsonObject
								.getJSONArray(planType);
						// contentValues.put("planType", planType);

						for (int i = 0; i < planArray.length(); i++) {
							JSONObject jsonObject = planArray.getJSONObject(i);
							String plan_desc = jsonObject
									.getString("plan_desc");
							// contentValues.put("plan_desc", plan_desc);
							String plan_amt = jsonObject.getString("plan_amt");
							// contentValues.put("plan_amt", plan_amt);
							String plan_validity = jsonObject
									.getString("plan_validity");
							// contentValues.put("plan_validity",
							// plan_validity);
							// // Log.e("cORRECT", "cORRECT   " + i + "  "
							// + jsonObject.toString());
							// controller.insertPlans(contentValues);
							planDataSource.createPlan(prod_code_pay1, opr_name,
									circle_id, circleName, planType, plan_amt,
									plan_validity, plan_desc, "" + time);
						}

					}

				}

			}
		} catch (JSONException exception) {

		} catch (SQLException exception) {
			// TODO: handle exception
			// // Log.e("Er ", exception.getMessage());
		} catch (Exception e) {
			// TODO: handle exception
			// // Log.e("Er ", e.getMessage());
		} finally {
			planDataSource.close();
		}
	}

	// class RequestTask extends AsyncTask<String, String, String> {
	//
	// @Override
	// protected String doInBackground(String... uri) {
	// HttpClient httpclient = new DefaultHttpClient();
	// HttpResponse response;
	// String responseString = null;
	// try {
	// response = httpclient.execute(new HttpGet(Constants.B2B_URL
	// + "method=getPlanDetails&operator" + pay1OpId
	// + "&circle=all"));
	// StatusLine statusLine = response.getStatusLine();
	// if (statusLine.getStatusCode() == HttpStatus.SC_OK) {
	// ByteArrayOutputStream out = new ByteArrayOutputStream();
	// response.getEntity().writeTo(out);
	// out.close();
	// responseString = out.toString();
	// } else {
	// // Closes the connection.
	// response.getEntity().getContent().close();
	// throw new IOException(statusLine.getReasonPhrase());
	// }
	// } catch (ClientProtocolException e) {
	// // TODO Handle problems..
	// } catch (IOException e) {
	// // TODO Handle problems..
	// e.printStackTrace();
	// }
	// return responseString;
	// }
	//
	// @Override
	// protected void onPostExecute(String result) {
	// super.onPostExecute(result);
	// // Do anything with response..
	// }
	// }

	public class GetOperatorTask extends AsyncTask<String, String, String> {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			try {
				String response = RequestClass.getInstance()
						.readPay1B2CRequest(RechargeMobileActivity.this,
								Constants.B2C_URL + "get_all_operators/?");
				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}

			try {

				long timestamp = System.currentTimeMillis();
				operatorDataSource.open();

				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");
					JSONObject jsonObject = new JSONObject(replaced);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {
						JSONObject description = jsonObject
								.getJSONObject("description");
						JSONArray mobile = description.getJSONArray("mobile");
						JSONArray data = description.getJSONArray("data");
						JSONArray postpaid = description
								.getJSONArray("postpaid");
						JSONArray dth = description.getJSONArray("dth");

						for (int i = 0; i < mobile.length(); i++) {
							JSONObject object = mobile.getJSONObject(i);

							operatorDataSource
									.createOperator(
											Integer.parseInt(object.getString(
													"id").trim()),
											object.getString("opr_code").trim(),
											Integer.parseInt(object.getString(
													"product_id").trim()),
											object.getString("name").trim(),
											Integer.parseInt(object.getString(
													"service_id").trim()),
											Integer.parseInt(object.getString(
													"flag").trim()),
											Integer.parseInt(object.getString(
													"delegate").trim()),
											Integer.parseInt(object.getString(
													"stv").trim()),
											Constants.RECHARGE_MOBILE,
											timestamp,
											Integer.parseInt(object
													.getString("max")),
											Integer.parseInt(object
													.getString("min")),
											Integer.parseInt(object
													.getString("service_charge_amount")),
											Integer.parseInt(object
													.getString("service_tax_percent")),
											Integer.parseInt(object
													.getString("service_charge_percent")),
											object.getString("charges_slab")
													.toString());

						}
						for (int i = 0; i < data.length(); i++) {
							JSONObject object = data.getJSONObject(i);

							operatorDataSource
									.createOperator(
											Integer.parseInt(object.getString(
													"id").trim()),
											object.getString("opr_code").trim(),
											Integer.parseInt(object.getString(
													"product_id").trim()),
											object.getString("name").trim(),
											Integer.parseInt(object.getString(
													"service_id").trim()),
											Integer.parseInt(object.getString(
													"flag").trim()),
											Integer.parseInt(object.getString(
													"delegate").trim()),
											Integer.parseInt(object.getString(
													"stv").trim()),
											Constants.RECHARGE_DATA,
											timestamp,
											Integer.parseInt(object
													.getString("max")),
											Integer.parseInt(object
													.getString("min")),
											Integer.parseInt(object
													.getString("service_charge_amount")),
											Integer.parseInt(object
													.getString("service_tax_percent")),
											Integer.parseInt(object
													.getString("service_charge_percent")),
											object.getString("charges_slab")
													.toString());
						}
						for (int i = 0; i < postpaid.length(); i++) {
							JSONObject object = postpaid.getJSONObject(i);

							operatorDataSource
									.createOperator(
											Integer.parseInt(object.getString(
													"id").trim()),
											object.getString("opr_code").trim(),
											Integer.parseInt(object.getString(
													"product_id").trim()),
											object.getString("name").trim(),
											Integer.parseInt(object.getString(
													"service_id").trim()),
											Integer.parseInt(object.getString(
													"flag").trim()),
											Integer.parseInt(object.getString(
													"delegate").trim()),
											Integer.parseInt(object.getString(
													"stv").trim()),
											Constants.BILL_PAYMENT,
											timestamp,
											Integer.parseInt(object
													.getString("max")),
											Integer.parseInt(object
													.getString("min")),
											Integer.parseInt(object
													.getString("service_charge_amount")),
											Integer.parseInt(object
													.getString("service_tax_percent")),
											Integer.parseInt(object
													.getString("service_charge_percent")),
											object.getString("charges_slab")
													.toString());
						}
						for (int i = 0; i < dth.length(); i++) {

							JSONObject object = dth.getJSONObject(i);

							operatorDataSource
									.createOperator(
											Integer.parseInt(object.getString(
													"id").trim()),
											object.getString("opr_code").trim(),
											Integer.parseInt(object.getString(
													"product_id").trim()),
											object.getString("name").trim(),
											Integer.parseInt(object.getString(
													"service_id").trim()),
											Integer.parseInt(object.getString(
													"flag").trim()),
											Integer.parseInt(object.getString(
													"delegate").trim()),
											Integer.parseInt(object.getString(
													"stv").trim()),
											Constants.RECHARGE_DTH,
											timestamp,
											Integer.parseInt(object
													.getString("max")),
											Integer.parseInt(object
													.getString("min")),
											Integer.parseInt(object
													.getString("service_charge_amount")),
											Integer.parseInt(object
													.getString("service_tax_percent")),
											Integer.parseInt(object
													.getString("service_charge_percent")),
											object.getString("charges_slab")
													.toString());
						}

					}
				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonDialog(RechargeMobileActivity.this,
						"Operator", Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			} catch (SQLException exception) {
				// TODO: handle exception
				// // Log.e("Er ", exception.getMessage());
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonDialog(RechargeMobileActivity.this,
						"Operator", Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			} finally {
				operatorDataSource.close();
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(RechargeMobileActivity.this);
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(false);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							GetOperatorTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}
	}

	public class CheckUpdateTask extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... params) {

			String response = RequestClass
					.getInstance()
					.readPay1B2BAppUpdateRequest(RechargeMobileActivity.this,
							"https://androidquery.appspot.com/api/market?app=com.pay1");
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			try {
				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONObject jsonObject = new JSONObject(replaced);
					String live_version = jsonObject.getString("version");
					// int curVersion = getPackageManager().getPackageInfo(
					// "com.mindsarray.pay1", 0).versionCode;
					String current_version = getPackageManager()
							.getPackageInfo("com.pay1", 0).versionName;

					// Log.w("SDFSD ", curVersion + "  " + current_version);

					if (!live_version.equalsIgnoreCase("null")
							&& !current_version.equalsIgnoreCase(live_version)) {

						try {
							final Dialog dialog = new Dialog(
									RechargeMobileActivity.this);
							dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
							// dialog.getWindow().setBackgroundDrawableResource(
							// android.R.color.transparent);
							dialog.setContentView(R.layout.dialog_success_two_button);
							// dialog.setTitle(null);

							Typeface Reguler = Typeface.createFromAsset(
									getAssets(),
									"EncodeSansNormal-400-Regular.ttf");

							Typeface Narrow = Typeface.createFromAsset(
									getAssets(),
									"EncodeSansNarrow-500-Medium.ttf");

							TextView textView_Title = (TextView) dialog
									.findViewById(R.id.textView_Title);
							textView_Title.setText("Pay1 Update Available");
							textView_Title.setTypeface(Reguler);
							// set the custom dialog components - text, image
							// and button
							TextView textView_Message = (TextView) dialog
									.findViewById(R.id.textView_Message);
							textView_Message
									.setText("There is newer version of Pay1 application available, click OK to upgrade now?");
							textView_Message.setTypeface(Narrow);

							ImageView imageView_Ok = (ImageView) dialog
									.findViewById(R.id.imageView_Ok);
							// if button is clicked, close the custom dialog
							imageView_Ok
									.setOnClickListener(new View.OnClickListener() {
										@Override
										public void onClick(View v) {
											dialog.dismiss();
											Intent intent = new Intent(
													Intent.ACTION_VIEW,
													Uri.parse("market://details?id=com.pay1"));
											startActivity(intent);
										}
									});

							ImageView imageView_Cancel = (ImageView) dialog
									.findViewById(R.id.imageView_Cancel);
							// if button is clicked, close the custom dialog
							imageView_Cancel
									.setOnClickListener(new View.OnClickListener() {
										@Override
										public void onClick(View v) {
											dialog.dismiss();
										}
									});

							dialog.show();
						} catch (Exception exception) {
						}
					}
				} else {

				}
			} catch (JSONException e) {
				// e.printStackTrace();

			} catch (Exception e) {
				// TODO: handle exception

			}
		}

		@Override
		protected void onPreExecute() {

			super.onPreExecute();
		}

	}
}
