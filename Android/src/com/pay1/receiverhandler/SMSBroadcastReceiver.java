package com.pay1.receiverhandler;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;

import com.pay1.LoginConfirmActivity;

public class SMSBroadcastReceiver extends BroadcastReceiver {

	private static final String SMS_RECEIVED = "android.provider.Telephony.SMS_RECEIVED";

	// private static final String TAG = "SMSBroadcastReceiver";

	@Override
	public void onReceive(Context context, Intent intent) {
		// Log.i(TAG, "Intent received: " + intent.getAction());

		if (intent.getAction().equals(SMS_RECEIVED)) {
			Bundle bundle = intent.getExtras();
			if (bundle != null) {
				Object[] pdus = (Object[]) bundle.get("pdus");
				final SmsMessage[] messages = new SmsMessage[pdus.length];
				for (int i = 0; i < pdus.length; i++) {
					messages[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
					if (messages.length > -1) {
						try {
							String FROM = messages[i].getOriginatingAddress();

							if (FROM.contains("PAYONE")) {

								String str = messages[0].getMessageBody()
										.toString().trim();
								if (str.substring(0, str.indexOf(":"))
										.trim()
										.equalsIgnoreCase(
												"Dear Pay1 User, Your One time Password (OTP) for you app registration is")) {

									Intent intent2 = new Intent(context,
											LoginConfirmActivity.class);
									intent2.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
									intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
									intent2.putExtra("OTP",
											str.substring(str.indexOf(":") + 1)
													.trim());
									context.startActivity(intent2);
								}
							}
						} catch (Exception e) {
						}
					}
				}

			}
		}
	}
}