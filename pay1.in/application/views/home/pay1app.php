<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
         <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
        <meta name="author" content="">
    </head>
    <body>
        <?php 
        $app_url="";
        $app_string = $this->agent->agent;
        if(strpos($app_string, 'Windows')){
            $app_url = "https://www.microsoft.com/en-in/store/apps/pay1/9nblggh0dd65";
        }elseif (strpos($app_string, 'Android')) {
            $app_url = "https://play.google.com/store/apps/details?id=com.pay1&hl=en";
        }
        elseif (strpos($app_string, 'iPhone')) {
            $app_url = "";
        }
        ?>
        <div class="container">
            <a href="<?php echo $app_url; ?>"><img alt="Pay1 APP" src="/assets/images/icons/pay1_downloadapp.png"  class="img-responsive" ></a>
        </div>
    </body>
</html>