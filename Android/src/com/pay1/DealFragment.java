package com.pay1;

import java.util.ArrayList;
import java.util.WeakHashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.pay1.customviews.MyProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.pay1.adapterhandler.DealAdapter;
import com.pay1.constants.Constants;
import com.pay1.requesthandler.RequestClass;
import com.pay1.utilities.Utility;

public class DealFragment extends Fragment {

	private final String TAG = "Deals";

	private ImageView imageView_ChangeLocation;
	private ListView listView_Deals;
	private DealAdapter adapter;
	private TextView textView_Title;
	private ArrayList<WeakHashMap<String, String>> data;

	public static final String DEAL_ID = "id";
	public static final String DEAL_NAME = "dealname";
	public static final String OFFER_PRICE = "offer_price";
	public static final String CATAGORY = "category";
	public static final String CATAGORY_ID = "category_id";
	public static final String LATITUDE = "latitude";
	public static final String LONGITUDE = "longitude";
	public static final String DISTANCE = "distance";
	public static final String ACTUAL_PRICE = "actual_price";
	public static final String DISCOUNT = "discount";
	public static final String TOTAL_STOCK = "total_stock";
	public static final String URL = "img_url";
	public static final String STOCK_SOLD = "stock_sold";

	public static DealFragment newInstance() {
		DealFragment fragment = new DealFragment();
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View vi = inflater.inflate(R.layout.deal_fragment, container, false);
		try {

			// Typeface Normal = Typeface.createFromAsset(getActivity()
			// .getAssets(), "EncodeSansNormal-100-Thin.ttf");

			Typeface Reguler = Typeface.createFromAsset(getActivity()
					.getAssets(), "EncodeSansNormal-400-Regular.ttf");

			textView_Title = (TextView) vi.findViewById(R.id.textView_Title);
			textView_Title.setTypeface(Reguler);

			imageView_ChangeLocation = (ImageView) vi
					.findViewById(R.id.imageView_ChangeLocation);
			imageView_ChangeLocation.setVisibility(View.GONE);
			imageView_ChangeLocation
					.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub

						}
					});
			data = new ArrayList<WeakHashMap<String, String>>();
			listView_Deals = (ListView) vi.findViewById(R.id.listView_Deals);
			// textView_NoData = (TextView)
			// vi.findViewById(R.id.textView_NoData);
			// listView_Deals.setEmptyView(textView_NoData);
			adapter = new DealAdapter(getActivity(), data);
			listView_Deals.setAdapter(adapter);
			listView_Deals
					.setOnItemClickListener(new AdapterView.OnItemClickListener() {

						@Override
						public void onItemClick(AdapterView<?> parent,
								View view, int position, long id) {
							// TODO Auto-generated method stub
							try {
								WeakHashMap<String, String> map = new WeakHashMap<String, String>();
								map = data.get(position);
								Intent intent = new Intent(getActivity(),
										DealDetailsActivity.class);
								intent.putExtra("URL", map.get(URL));
								intent.putExtra("DEAL_ID", map.get(DEAL_ID));
								intent.putExtra("DEAL_NAME", map.get(DEAL_NAME));
								intent.putExtra("ACTUAL_PRICE",
										map.get(ACTUAL_PRICE));
								intent.putExtra("OFFER_PRICE",
										map.get(OFFER_PRICE));
								intent.putExtra("STOCK_SOLD",
										map.get(STOCK_SOLD));
								intent.putExtra("DISCOUNT", map.get(DISCOUNT));

								startActivity(intent);
							} catch (Exception e) {
							}
						}
					});

			String lat = Utility.getCurrentLatitude(getActivity(),
					Constants.SHAREDPREFERENCE_CURRENT_LATITUDE);
			String lng = Utility.getCurrentLongitude(getActivity(),
					Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE);
			if (lat != null && lng != null) {
				new DealTask().execute(lat, lng);
			} else {
				new DealTask().execute(Utility.getLatitude(getActivity(),
						Constants.SHAREDPREFERENCE_LATITUDE), Utility
						.getLongitude(getActivity(),
								Constants.SHAREDPREFERENCE_LONGITUDE));
			}

			// overridePendingTransition(R.anim.translate_activity_down,
			// R.anim.translate_activity_up);
		} catch (Exception e) {
		}

		return vi;
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	public class DealTask extends AsyncTask<String, String, String> implements
			OnDismissListener {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			try {
				String response = RequestClass
						.getInstance()
						.readPay1B2CRequest(
								getActivity(),
								Constants.B2C_URL + "get_deals/?latitude="
										+ params[0] + "&longitude=" + params[1]);

				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			try {

				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONObject jsonObject = new JSONObject(replaced);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {
						data.clear();

						JSONArray jsonArrayDesc = jsonObject
								.getJSONArray("description");

						for (int i = 0; i < jsonArrayDesc.length(); i++) {
							WeakHashMap<String, String> map = new WeakHashMap<String, String>();
							map.put(DEAL_ID, jsonArrayDesc.getJSONObject(i)
									.getString("id"));
							map.put(DEAL_NAME, jsonArrayDesc.getJSONObject(i)
									.getString("dealname"));
							map.put(OFFER_PRICE, jsonArrayDesc.getJSONObject(i)
									.getString("offer_price"));
							map.put(CATAGORY, jsonArrayDesc.getJSONObject(i)
									.getString("category"));
							map.put(CATAGORY_ID, jsonArrayDesc.getJSONObject(i)
									.getString("category_id"));
							map.put(LATITUDE, jsonArrayDesc.getJSONObject(i)
									.getString("latitude"));
							map.put(LONGITUDE, jsonArrayDesc.getJSONObject(i)
									.getString("longitude"));
							map.put(DISTANCE, jsonArrayDesc.getJSONObject(i)
									.getString("distance"));
							map.put(ACTUAL_PRICE, jsonArrayDesc
									.getJSONObject(i).getString("actual_price"));
							map.put(DISCOUNT, jsonArrayDesc.getJSONObject(i)
									.getString("discount"));
							map.put(TOTAL_STOCK, jsonArrayDesc.getJSONObject(i)
									.getString("total_stock"));
							map.put(URL, jsonArrayDesc.getJSONObject(i)
									.getString("img_url"));
							map.put(STOCK_SOLD, jsonArrayDesc.getJSONObject(i)
									.getString("stock_sold"));

							data.add(map);
						}

						getActivity().runOnUiThread(new Runnable() {

							@Override
							public void run() {
								// TODO Auto-generated method stub
								adapter.notifyDataSetChanged();
							}
						});

					} else {
						Constants.showOneButtonDialog(getActivity(), TAG,
								Constants.checkCode(replaced),
								Constants.DIALOG_CLOSE);
					}

				} else {
					Constants.showOneButtonDialog(getActivity(), TAG,
							Constants.ERROR_INTERNET, Constants.DIALOG_CLOSE);

				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonDialog(getActivity(), TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonDialog(getActivity(), TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(getActivity());
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							DealTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			DealTask.this.cancel(true);
			dialog.cancel();
		}

	}

}
