<?php $this->load->view('common/doctype_html');  

    $current_page_url =  "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    $meteTagData = getMetaTagDetails();
    foreach ($meteTagData as $row){
      
      if($row->page_url == $current_page_url){
        $new_page_tittle =  $row->page_tittle;
        $new_page_meta_description =  $row->page_meta_description;  
        $new_page_meta_tag =  $row->page_meta_tag;  
      }
      
    }
?>
<meta name="description" content="<?php if($new_page_meta_description){ echo $new_page_meta_description; }else{ ?>My Gift list on Pay1 with all active and expired gifts. Use the gift today before it expires. <?php } ?> ">
<meta name="keywords" content="<?php if($new_page_meta_tag){ echo $new_page_meta_tag; }else{ echo $response['description']['offer_detail'][0]['name']; ?> , My Gifts , Pay1 <?php } ?>" />
<title><?php if($new_page_tittle){ echo $new_page_tittle; }else{ echo $response['description']['offer_detail'][0]['name']; ?> | My Gifts | Pay1  <?php } ?></title>
<?php $this->load->view('common/header');  ?>
<?php 
$userData = getUserDetails();

//      echo '<pre>----';
//      print_r($response);
//      print_r($coupon_details);
//      print_r($Img_url);

  if(isset($coupon_details['description'][0]['pin']) && !empty($coupon_details['description'][0]['pin'])){ 
      $coupon_details['description'][0]['pin']; 
  }
    $pin_set = (isset($coupon_details['description'][0]['pin'])&& !empty($coupon_details['description'][0]['pin']))?'set':'unset'; 
?>
         
      <link rel="stylesheet" href="/assets/css/style.css">
       <!---******************************************** Hidden Elements  ****************************************** -->
    
    
    <input type='hidden' id='user' value='<?php echo $userData['user_id']; ?>'>
    <input type='hidden' id='user_mobile' value="<?php echo $userData['mobile']; ?>">
    <input type='hidden' id='txn_id' value='<?php echo $txn_id; ?>'>
    
    
    <!---******************************************** Hidden Elements  ****************************************** -->

      <div class="clearfix" style="clear:both;"></div>
      <div class="container-fluid">
         <div class="container border-both1 margin-top-50">
            <div class="bg col-md-12"><img src="<?php echo $Img_url['description']['image_list'][0]['L_img'] ; ?>" class="img-responsive margin-top-20" style="width:100%;" alt=""></div>
            <div class="row">
               <div class="col-md-3">
                   <?php if($Img_url['description']['image_list'][0]['logo_url'] == ''){ ?>
                    <img class="logo" src="/public/assets/images/default_img/ic_logo_entertainment.png" alt="">
                    <?php }else{ ?>
                        <img src="<?php echo $Img_url['description']['image_list'][0]['logo_url'] ; ?>" class="img-responsive margin-top-20" alt=""   style="height:170px;width:170px;z-index: 2;border-radius: 85px; border: 1px solid #c7c7c7;">
                    <?php } ?>
               </div>
               <div class="col-md-9">
                  <p class="page_title"><?php echo $response['description']['offer_detail'][0]['offer_desc'] ; ?></p>
                  <div class="">
                     <div class="col-md-7 page_title"><?php if($response['description']['offer_detail'][0]['by_voucher']==1){ ?><i class="fa fa-inr"></i> <?php echo $response['description']['offer_detail'][0]['offer_price'] ; ?> + <?php } ?><img src="/assets/images/gift/gifti.png"><span><?php echo $response['description']['offer_detail'][0]['min_amount'] ; ?></span></div>
<!--                     <div class="col-md-4 page_title">Share <i class="fa fa-share-alt"></i></div>-->
                     <div class="col-md-5 text-right">
                        
                        <!---********************************************SMS model confirm success ****************************************** -->
                        <div class="modal fade" role="dialog" id="confirm_SMS_success" role="dialog" aria-hidden="true">
                           <div class="modal-dialog">
                              <div class="modal-content">
                                 <div class="modal-header1">
                                    <button class="close" data-dismiss="modal">&times; </button>
                                    <div class="text-center"><h4>CONGRATULATIONS</h4></div>
                                 </div>
                                 <div class="modal-body clearfix text-center">
                                    <div class="text-center"><h4>Message sent successfully.</h4></div>
                                    <div class="margin-top-50"></div>
                                    <hr>
                                    <button class="btn btn-default btn-block" data-dismiss="modal">DONE</button>
                                 </div>
                              </div>
                           </div>
                        </div>
                       <!---********************************************model end ****************************************** -->
                       
                        <!---********************************************SMS model error ****************************************** -->
                        <div class="modal fade" role="dialog" id="confirm_SMS_error" role="dialog" aria-hidden="true">
                           <div class="modal-dialog">
                              <div class="modal-content">
                                 <div class="modal-header1">
                                    <button class="close" data-dismiss="modal">&times; </button>
                                    <div class="text-center"><h4>OOps</h4></div>
                                 </div>
                                 <div class="modal-body clearfix text-center">
                                    <div class="text-center"><h4>Something went wrong,<br/> there is problem while sending SMS. Please try again later.</h4></div>
                                    <div class="margin-top-50"></div>
                                    <hr>
                                    <button class="btn btn-default btn-block" data-dismiss="modal">DONE</button>
                                 </div>
                              </div>
                           </div>
                        </div>
                       <!---********************************************SMS model error end ****************************************** -->

                       <div class="loader" style="display:none;">
			<center>
			<img class="loading-image" src="/images/bx_loader.gif" alt="loading..">
			</center>
                    </div>
                       <div class="pull-right">
                           <!-- if gift already grabbed then give option of sms -->
                           <div id="sendsms" style="border-radius:10px" class="btn getitnow btn-save btn-block">Send voucher code <br /> to your mobile</div>
<!--                           <input type="button" style="display: block" value="GET IT NOW" id="confirm" class="btn getitnow btn-save btn-block">-->
                         
                       </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-sm-12 margin-top-20">
               <div class="row">
                   <div class="col-md-3 gift_tel"><?php if(isset($response['description']['offer_detail'][0]['dealer_contact']) && !empty($response['description']['offer_detail'][0]['dealer_contact'])){ ?><span style="color:#17a086;"><i class="fa fa-phone"></i>  </span> tel:<?php echo  $response['description']['offer_detail'][0]['dealer_contact'] ; } ?></div> 
                  <div class="col-md-6"><?php if(isset($coupon_details['description'][0]['area']) && !empty($coupon_details['description'][0]['area'])){ ?><img src="/assets/images/gift/gift1.jpg" alt="">  <span class="gifthed"><?php echo $coupon_details['description'][0]['area'].' : '.ceil($response['description']['location_detail'][0]['distance']).'KM' ; ?></span><?php } ?></div>
                  <div class="clearfix"></div>
                  <div class="row margin-top-20" id='coupon_details' >
                     <div class="col-md-3"></div>
                     <div class="col-md-6">
                          <h4 >Coupon Details:</h4>
                          <table>
                              <tr>
                                  <td>
                                      Mobile Number :
                                  </td>
                                  <td>
                                      xxxxxx<?php echo substr($this->session->userdata['user']['mobile'],6); ?>
                                  </td>
                              </tr>
                              <tr>
                                  <td>
                                      Voucher Code :
                                  </td>
                                  <td>
                                      <span id="voucher" style="color:#ed1c24"><?php if(isset($coupon_details['description'][0]['code']) && !empty($coupon_details['description'][0]['code'])){ echo $coupon_details['description'][0]['code']; } ?></span>
                                  </td>
                              </tr>
                              <tr><?php if($pin_set == 'set'){?>
                                  <td>
                                    Pin :
                                  </td><?php } ?>
                                  <td>
                                      <span id="pin"><?php if(isset($coupon_details['description'][0]['pin']) && !empty($coupon_details['description'][0]['pin'])){ echo $coupon_details['description'][0]['pin']; } ?></span>
                                  </td>
                              </tr>
                              <tr>
                                  <td>
                                      Expiry Date :
                                  </td>
                                  <td>
                                      <span id='expiry_date'><?php if(isset($coupon_details['description'][0]['expiry_date']) && !empty($coupon_details['description'][0]['expiry_date'])){ echo $coupon_details['description'][0]['expiry_date']; } ?></span>
                                  </td>
                              </tr>
                          </table>
                          
                     </div>
                     <div class="col-md-3"></div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="row margin-top-20">
                     <div class="col-md-3"></div>
                     <div class="col-md-6">
                         <span class="gifthed" style="font-size: 21px">Offer Details:</span>
                        <div class="ul_bullet">
                            <?php echo isset($response['description']['offer_info_html']) ? $response['description']['offer_info_html'] : ""; ?>
                        </div>
                        <div class="margin-top-20"></div>
                        <?php if(isset($response['description']['location_detail'][0]['full_address'])) { ?>
                        <span class="gifthed" style="font-size: 21px">Address:</span>
                        <ul class="gift">
                           <li><?php echo isset($response['description']['location_detail'][0]['full_address']) ? $response['description']['location_detail'][0]['full_address'] : ""; ?>
                               <br><?php // echo isset($response['description']['location_detail'][0]['city']) ? $response['description']['location_detail'][0]['city'] : "" ; ?>
                           </li>
                        </ul><?php } ?>
                        <?php if(isset($response['description']['location_detail'][0]['address'])) { ?>
                        <h4>Map:</h4>
                        <div class='col-md-8'>
                            <?php if($response['description']['location_detail'][0]['lat']){  ?><div id="giftmap1">
                            </div><?php } ?> 
                        </div><?php } ?>
                        <div class="margin-top-50"></div>
                         <?php if(isset($response['description']['deal_detail'][0]['content_txt']) && !empty($response['description']['deal_detail'][0]['content_txt'])) { ?>
                           <div class="termcondition" style="clear:both;padding-top:10px">
                              <span class="gifthed" style="font-size: 21px">Terms & Conditions:</span>
                              <div class="ul_bullet">
                                 <?php echo $response['description']['deal_detail'][0]['content_txt']; ?>
                              </div>
                            </div><?php } ?>
                        
                        <div class="text-right col-md-12">
                           <p id="viewmore"><i class="glyphicon glyphicon-chevron-down"></i>
                        </div>
                    
                     <div class="text-right col-md-12" id="viewless"> VIEW LESS &nbsp; &nbsp; <i class="glyphicon glyphicon-chevron-up "> </i></div>
                     </div>
                     <div class="col-md-3">
                        
                     </div>
                     <div class="">
                        
                           
                  </div>
               </div>
            </div>
            
         </div>
      </div>
      </div><div class="margin-top-30 clearfix"></div>
      <div class="clearfix"></div>
      
      <div class="modal fade" role="dialog" id="failure">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="cls" data-dismiss="modal">&times;</button>
                  <h3>Payment Failure</h3>
               </div>
               <div class="modal-body">
                  <p>Sorry! Your previous payment attempt failed.</p>
                  <div class="pull-right">
                     <button class="modal_button" id="try-again">Try Again</button>
                     <button class="modal_button" id="cancel-button"data-dismiss="modal">Cancel</button>
                  </div>
                  <div class="clearfix"></div>
               </div>
            </div>
         </div>
      </div>


      <script type="text/javascript">
         function initialize() {
                var lat = '<?php echo $response['description']['location_detail'][0]['lat'] ;?>';
                var lng = '<?php echo $response['description']['location_detail'][0]['lng'] ;?>';
              var mapOptions = {
                zoom: 12,
                center: new google.maps.LatLng(lat,lng)
              };
              var image2 = '/assets/images/map/0.png';
              var mapDiv = document.getElementById('giftmap1');
              var map = new google.maps.Map(mapDiv, mapOptions);
              var myLatLng = new google.maps.LatLng(lat, lng);
              var beachMarker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                icon: image2
            });
            }
            google.maps.event.addDomListener(window, 'load', initialize);  
         </script>    
        <script type="text/javascript">   
          
         $(document).ready(function(){
            // jQuery.noConflict();
            $(".ul_bullet").children("ul").addClass('list_bullet')
            
             $('body').scrollToTop({skin: 'cycle'});
            
            if($('.termcondition').is(':visible')){
               $('#viewmore').hide();
         
            }else{
               $('#viewless').hide();
            }
            $('#viewmore').click(function(){
               $('.termcondition').toggle('slow');
               $('#viewless').show();
               $(this).hide();
            });
            $('#viewless').click(function(){
               $('.termcondition').toggle('slow');
               $('#viewmore').show();
               $(this).hide();
            });
              
            $('#confirm').click(function () { 
                var amount = $("#amount1").val();
                var by_voucher =  $("#by_voucher").val();
                console.log('amount '+amount+' by_voucher '+by_voucher+' '+$('#loyalty_balance').val());
               if($('#loyalty_balance').val() < amount && by_voucher==0){ // not e-voucher
                    $('#less_giftcoin_error').modal({
                        show: true
                    });
                }
                else{
                    $('#confirm_gift').modal({
                        show: true
                    });
                }
        
                
            });
            
             $("#sendsms").click( function(){
                b2c.core.send_sms();
            });
           
     });
     
      </script>
<?php $this->load->view('common/footer'); ?>
      
