package com.pay1.adapterhandler;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.WeakHashMap;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.LocationManager;
import android.net.wifi.WifiManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.pay1.R;
import com.pay1.ShopLocatorActivity;

public class ShopLocatorAdapter extends BaseAdapter {

	private Context context;
	private ArrayList<WeakHashMap<String, String>> data;
	private Typeface Reguler, Medium;

	public ShopLocatorAdapter(Context context,
			ArrayList<WeakHashMap<String, String>> data) {
		this.context = context;
		this.data = data;

		Reguler = Typeface.createFromAsset(context.getAssets(),
				"Roboto-Regular.ttf");

		Medium = Typeface.createFromAsset(context.getAssets(),
				"Roboto-Regular.ttf");

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder viewHolder;
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.shop_locator_adapter, null);
			viewHolder = new ViewHolder();

			viewHolder.textView_Name = (TextView) convertView
					.findViewById(R.id.textView_Name);
			viewHolder.textView_Name.setTypeface(Medium);
			viewHolder.textView_Distance = (TextView) convertView
					.findViewById(R.id.textView_Distance);
			viewHolder.textView_Distance.setTypeface(Medium);
			viewHolder.textView_Address = (TextView) convertView
					.findViewById(R.id.textView_Address);
			viewHolder.textView_Address.setTypeface(Reguler);
			convertView.setTag(viewHolder);

		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		WeakHashMap<String, String> map = new WeakHashMap<String, String>();
		map = data.get(position);

		viewHolder.textView_Name.setText(map.get(ShopLocatorActivity.SHOP_NAME)
				.toString().trim());

		double dis = 0;
		DecimalFormat decimalFormat = new DecimalFormat("0.0");
		try {
			dis = Double.parseDouble(map.get(ShopLocatorActivity.SHOP_DISTANCE)
					.toString().trim());
			if (dis < 1) {
				dis = dis * 1000;
				viewHolder.textView_Distance.setText(decimalFormat.format(dis)
						+ " M");
			} else {
				viewHolder.textView_Distance.setText(decimalFormat.format(dis)
						+ " KM");
			}

		} catch (Exception e) {
		}

		viewHolder.textView_Address.setText(map
				.get(ShopLocatorActivity.SHOP_AREA)
				+ ", "
				+ map.get(ShopLocatorActivity.SHOP_CITY));

		boolean gps_enabled = false;
		boolean network_enabled = false;
		LocationManager lm = (LocationManager) context
				.getSystemService(Context.LOCATION_SERVICE);
		try {
			gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
		} catch (Exception ex) {
		}

		try {
			network_enabled = lm
					.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
		} catch (Exception ex) {
		}
		// don't start listeners if no provider is
		// enabled
		boolean hasGPS = context.getPackageManager().hasSystemFeature(
				PackageManager.FEATURE_LOCATION_GPS);
		WifiManager wifi = (WifiManager) context
				.getSystemService(Context.WIFI_SERVICE);

		if (!(network_enabled && wifi.isWifiEnabled())
				&& (hasGPS && !gps_enabled)) {
			viewHolder.textView_Distance.setText("");
			viewHolder.textView_Distance.setVisibility(View.INVISIBLE);
		}
		return convertView;
	}

	private class ViewHolder {
		TextView textView_Name, textView_Distance, textView_Address;
	}
}
