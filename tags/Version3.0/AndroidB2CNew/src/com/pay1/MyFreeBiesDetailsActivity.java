package com.pay1;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.WeakHashMap;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.SQLException;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.text.Html;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.larvalabs.svgandroid.SVGParser;
import com.pay1.adapterhandler.CallListAdapter;
import com.pay1.constants.Constants;
import com.pay1.customviews.GiftImageGalleryLayout;
import com.pay1.customviews.MyProgressDialog;
import com.pay1.databasehandler.FreeBies;
import com.pay1.databasehandler.FreeBiesDataSource;
import com.pay1.requesthandler.RequestClass;
import com.pay1.servicehandler.GPSTracker;
import com.pay1.utilities.MyLocation;
import com.pay1.utilities.MyLocation.LocationResult;
import com.pay1.utilities.Utility;
import com.squareup.picasso.Picasso;

public class MyFreeBiesDetailsActivity extends Activity {

	public static final String DEAL_ID = "id";
	public static int thunbleft;
	public static int thunbtop;
	int review = 0;
	public static int arrowleft;
	String dealer_contact;
	public static int arrowtop;
	public int by_voucher = 0;
	RelativeLayout locationLayout;
	private static final String SCREEN_LABEL = "Freebie Details Screen";
	private EasyTracker easyTracker = null;
	ArrayList<HashMap<String, String>> numberWithLocationList;
	private final String TAG = "Deal";
	JSONArray imageArray;
	// private ImageLoader imageLoader;
	private ImageView imageView_Back, imageView_Image, imageView_Locate,
			imageView_Photo, imageView_Like, imageView_Points,
			imageView_Rating, imageView_Love, imageView_LikeIt,
			imageView_JustOk, imageView_Poor, imageView_ItsBad,
			img_right_arrow;
	private TextView textView_Title, textView_OfferName,
			textView_OfferDescription, textView_Mobile, textView_Voucher,
			textView_ExpiryDate, textView_DealName, textView_Address,
			textView_Distance, textView_Points, textView_ShopGallery,
			textView_Brand, textView_BrandDescription, textView_Rating,
			textView_Offer, textView_OfferDetails, textView_More,
			textView_Rate, textView_AddressMore, textView_like,
			textView_GiftCoins, textView_OfferStatus, textView_Pin;
	WebView webView_Offer_Details;
	ImageView location_img;
	// TextView textView_Unit;
	TextView textView_Area;
	LinearLayout layout_like;
	private WebView webView_Details;
	private RelativeLayout relativeLayout_Locate, relativeLayout_Gallery;
	private LinearLayout linearLayout_Main, linearLayout_More;
	private Button button_Call, button_Purchase;
	TextView textViewDistance;
	private View headPhotos, headAbout;
	TextView textViewOffer;
	RelativeLayout visitUrlLayout;
	Button buttonVisitUrl;
	LinearLayout layout_review, view_sep, view_ShopGallery;
	// ImageView img_redeem_now;
	// ImageView frameLayout1;
	Typeface Reguler;
	String company_url;
	// private ProgressBar progressBar1;
	private ArrayList<WeakHashMap<String, String>> data_location;
	String logo_url;
	public static final String CONTENT_TYPE = "type";
	public static final String TITLE = "title";
	public static final String CONTENT = "content";
	public static final String DEAL_NAME = "dealname";
	public static final String OFFER_PRICE = "offer_price";
	public static final String CATAGORY = "category";
	public static final String CATAGORY_ID = "category_id";
	public static final String DISTANCE = "distance";
	public static final String ACTUAL_PRICE = "actual_price";
	public static final String DISCOUNT = "discount";
	public static final String TOTAL_STOCK = "total_stock";
	public static final String URL = "img_url";
	public static final String STOCK_SOLD = "stock_sold";
	public static final String LATITUDE = "latitude";
	public static final String LONGITUDE = "longitude";
	public static final String ADDRESS = "address";
	public static final String CITY = "city";
	public static final String AREA = "area";
	public static final String STATE = "state";
	public static final String FULL_ADDERESS = "full_adderess";
	public static final String GALARY_URL = "gal_image";
	public static final String GALARY_INFO = "image_info";
	public static final String VOUCHER_CODE = "code";
	public static final String NUMBER = "number";
	// Typeface fontAwesome;
	private String deal_ID, mobile = "";

	int offer_ID, amount, price, offer_price;
	String payment_options = "wallet";

	@SuppressLint("NewApi")
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.gift_deatil_activity);
		try {
			Reguler = Typeface.createFromAsset(getAssets(),
					"Roboto-Regular.ttf");
			Typeface Medium = Typeface.createFromAsset(getAssets(),
					"Roboto-Regular.ttf");
			RelativeLayout parent = (RelativeLayout) findViewById(R.id.parent);
			Constants.setFont(MyFreeBiesDetailsActivity.this, parent);

			/*
			 * if (!Utility.showGiftTutorial(MyFreeBiesDetailsActivity.this)) {
			 * // showDailogForFreeBie();
			 * 
			 * Intent intent = new Intent(MyFreeBiesDetailsActivity.this,
			 * GiftTutorialActivity.class); startActivity(intent);
			 * 
			 * }
			 */
			/*
			 * fontAwesome = Typeface.createFromAsset(getAssets(),
			 * "fontawesome-webfont.ttf");
			 */
			// ShowcaseImage();

			/*
			 * frameLayout1 = (ImageView) findViewById(R.id.frameLayout1);
			 * frameLayout1.setImageDrawable(SVGParser.getSVGFromResource(
			 * getResources(), R.raw.ic_topdrop_new).createPictureDrawable());
			 */

			/*
			 * img_redeem_now = (ImageView) findViewById(R.id.img_redeem_now);
			 * img_redeem_now.setImageDrawable(SVGParser.getSVGFromResource(
			 * getResources(), R.raw.icon_redemption) .createPictureDrawable());
			 */
			textViewDistance = (TextView) findViewById(R.id.textView_Distance);

			textView_Area = (TextView) findViewById(R.id.textView_Area);
			layout_review = (LinearLayout) findViewById(R.id.layout_review);
			headAbout = (View) findViewById(R.id.headAbout);
			buttonVisitUrl = (Button) findViewById(R.id.buttonVisitUrl);
			buttonVisitUrl.setTypeface(Reguler);
			buttonVisitUrl.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (company_url.length() > 0 && company_url!=null) {
						if (!company_url.startsWith("http://")
								&& !company_url.startsWith("https://")) {
							company_url = "http://" + company_url;
							Intent browserIntent = new Intent(
									Intent.ACTION_VIEW, Uri.parse(company_url));
							startActivity(browserIntent);
						} else {
							Intent browserIntent = new Intent(
									Intent.ACTION_VIEW, Uri.parse(company_url));
							startActivity(browserIntent);
						}
					}
				}
			});

			visitUrlLayout = (RelativeLayout) findViewById(R.id.visitUrlLayout);
			textViewOffer = (TextView) findViewById(R.id.textViewOffer);
			textViewOffer.setTypeface(Reguler);
			ImageView imageView_location = (ImageView) findViewById(R.id.imageView_location);
			imageView_location.setImageDrawable(SVGParser.getSVGFromResource(
					this.getResources(), R.raw.ic_shoplocator)
					.createPictureDrawable());

			ImageView imageView_Point = (ImageView) findViewById(R.id.imageView_Point);
			imageView_Point.setImageDrawable(SVGParser.getSVGFromResource(
					this.getResources(), R.raw.ic_gift_coin)
					.createPictureDrawable());

			headAbout.setVisibility(View.GONE);
			easyTracker = EasyTracker
					.getInstance(MyFreeBiesDetailsActivity.this);
			easyTracker.set(Fields.SCREEN_NAME, SCREEN_LABEL);
			numberWithLocationList = new ArrayList<HashMap<String, String>>();

			RelativeLayout back_layout = (RelativeLayout) findViewById(R.id.back_layout);
			back_layout.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onBackPressed();
				}
			});
			imageView_Back = (ImageView) findViewById(R.id.imageView_Back);
			imageView_Back.setImageDrawable(SVGParser.getSVGFromResource(
					getResources(), R.raw.ic_back_old).createPictureDrawable());
			if (Build.VERSION.SDK_INT >= 11)
				imageView_Back.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
			imageView_Back.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onBackPressed();
				}
			});

			layout_review.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

				}
			});

			webView_Details = (WebView) findViewById(R.id.webView_Details);
			WebSettings settings = webView_Details.getSettings();
			settings.setJavaScriptEnabled(true);
			webView_Details.setVisibility(View.GONE);
			webView_Details.setLongClickable(false);

			webView_Details.setBackgroundColor(0x00000000);
			if (Build.VERSION.SDK_INT >= 11)
				webView_Details.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);

			imageView_Image = (ImageView) findViewById(R.id.imageView_Image);
			imageView_Photo = (ImageView) findViewById(R.id.imageView_Photo);
			imageView_Like = (ImageView) findViewById(R.id.imageView_Like);
			layout_like = (LinearLayout) findViewById(R.id.layout_like);
			// imageView_Points = (ImageView)
			// findViewById(R.id.imageView_Points);
			final RelativeLayout mainLayout = (RelativeLayout) findViewById(R.id.parent);
			mainLayout.getViewTreeObserver().addOnGlobalLayoutListener(
					new ViewTreeObserver.OnGlobalLayoutListener() {
						public void onGlobalLayout() {
							// Remove the listener before proceeding
							if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
								mainLayout.getViewTreeObserver()
										.removeOnGlobalLayoutListener(this);
							} else {
								mainLayout.getViewTreeObserver()
										.removeGlobalOnLayoutListener(this);
							}
							thunbleft = getRelativeLeft(imageView_Like);
							thunbtop = getRelativeTop(imageView_Like);

							Rect rectf = new Rect();
							imageView_Like.getLocalVisibleRect(rectf);

							Log.d("WIDTH        :",
									String.valueOf(rectf.width()));
							Log.d("HEIGHT       :",
									String.valueOf(rectf.height()));
							Log.d("left         :", String.valueOf(rectf.left));
							Log.d("right        :", String.valueOf(rectf.right));
							Log.d("top          :", String.valueOf(rectf.top));
							Log.d("bottom       :",
									String.valueOf(rectf.bottom));
						}
					});

			imageView_Locate = (ImageView) findViewById(R.id.imageView_Locate);

			textView_OfferStatus = (TextView) findViewById(R.id.textView_OfferStatus);
			textView_OfferStatus.setTypeface(Reguler);
			textView_OfferStatus.setVisibility(View.GONE);
			imageView_Rating = (ImageView) findViewById(R.id.imageView_Rating);
			imageView_Rating.setVisibility(View.GONE);

			imageView_Rating.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					showRatingDialog();
				}
			});

			imageView_Love = (ImageView) findViewById(R.id.imageView_Love);
			imageView_Love.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					try {
						imageView_LikeIt.setTag("0");
						imageView_LikeIt.setImageDrawable(getResources()
								.getDrawable(R.drawable.ic_likeit_unsel));
						imageView_JustOk.setTag("0");
						imageView_JustOk.setImageDrawable(getResources()
								.getDrawable(R.drawable.ic_justok_unsel));
						imageView_Poor.setTag("0");
						imageView_Poor.setImageDrawable(getResources()
								.getDrawable(R.drawable.ic_poor_unsel));
						imageView_ItsBad.setTag("0");
						imageView_ItsBad.setImageDrawable(getResources()
								.getDrawable(R.drawable.ic_itsbad_unsel));
						if (imageView_Love.getTag().equals("1")) {
							imageView_Love.setTag("0");
							imageView_Love.setImageDrawable(getResources()
									.getDrawable(R.drawable.ic_loveit_unsel));
						} else {
							/*
							 * Toast t = Toast.makeText(
							 * MyFreeBiesDetailsActivity.this, "LOVE IT!",
							 * Toast.LENGTH_SHORT); t.setGravity(Gravity.CENTER,
							 * 0, 0); t.show();
							 */

							ShowToastWithImage("LOVE IT!", getResources()
									.getDrawable(R.drawable.ic_loveit_select));

							imageView_Love.setTag("1");
							imageView_Love.setImageDrawable(getResources()
									.getDrawable(R.drawable.ic_loveit_select));
						}
						new RatingTask().execute(String.valueOf(offer_ID), "5");
					} catch (Exception e) {
					}
				}
			});
			imageView_Love
					.setOnLongClickListener(new View.OnLongClickListener() {

						@Override
						public boolean onLongClick(View v) {
							// TODO Auto-generated method stub
							try {
								Toast t = Toast.makeText(
										MyFreeBiesDetailsActivity.this,
										"LOVE IT!", Toast.LENGTH_SHORT);
								t.setGravity(Gravity.CENTER, 0, 0);
								t.show();
							} catch (Exception e) {
							}
							return false;
						}
					});
			imageView_LikeIt = (ImageView) findViewById(R.id.imageView_LikeIt);
			imageView_LikeIt.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					try {
						imageView_Love.setTag("0");
						imageView_Love.setImageDrawable(getResources()
								.getDrawable(R.drawable.ic_loveit_unsel));
						imageView_JustOk.setTag("0");
						imageView_JustOk.setImageDrawable(getResources()
								.getDrawable(R.drawable.ic_justok_unsel));
						imageView_Poor.setTag("0");
						imageView_Poor.setImageDrawable(getResources()
								.getDrawable(R.drawable.ic_poor_unsel));
						imageView_ItsBad.setTag("0");
						imageView_ItsBad.setImageDrawable(getResources()
								.getDrawable(R.drawable.ic_itsbad_unsel));
						if (imageView_LikeIt.getTag().equals("1")) {
							imageView_LikeIt.setTag("0");
							imageView_LikeIt.setImageDrawable(getResources()
									.getDrawable(R.drawable.ic_likeit_unsel));
						} else {
							/*
							 * Toast t = Toast.makeText(
							 * MyFreeBiesDetailsActivity.this, "LIKE IT!",
							 * Toast.LENGTH_SHORT); t.setGravity(Gravity.CENTER,
							 * 0, 0); t.show();
							 */

							ShowToastWithImage("LIKE IT!", getResources()
									.getDrawable(R.drawable.ic_likeit_select));

							imageView_LikeIt.setTag("1");
							imageView_LikeIt.setImageDrawable(getResources()
									.getDrawable(R.drawable.ic_likeit_select));
						}
						new RatingTask().execute(String.valueOf(offer_ID), "4");
					} catch (Exception e) {
					}
				}
			});
			imageView_LikeIt
					.setOnLongClickListener(new View.OnLongClickListener() {

						@Override
						public boolean onLongClick(View v) {
							// TODO Auto-generated method stub
							try {
								Toast t = Toast.makeText(
										MyFreeBiesDetailsActivity.this,
										"LIKE IT!", Toast.LENGTH_SHORT);
								t.setGravity(Gravity.CENTER, 0, 0);
								t.show();
							} catch (Exception e) {
							}
							return false;
						}
					});
			imageView_JustOk = (ImageView) findViewById(R.id.imageView_JustOk);
			imageView_JustOk.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					try {
						imageView_Love.setTag("0");
						imageView_Love.setImageDrawable(getResources()
								.getDrawable(R.drawable.ic_loveit_unsel));
						imageView_LikeIt.setTag("0");
						imageView_LikeIt.setImageDrawable(getResources()
								.getDrawable(R.drawable.ic_likeit_unsel));
						imageView_Poor.setTag("0");
						imageView_Poor.setImageDrawable(getResources()
								.getDrawable(R.drawable.ic_poor_unsel));
						imageView_ItsBad.setTag("0");
						imageView_ItsBad.setImageDrawable(getResources()
								.getDrawable(R.drawable.ic_itsbad_unsel));
						if (imageView_JustOk.getTag().equals("1")) {
							imageView_JustOk.setTag("0");
							imageView_JustOk.setImageDrawable(getResources()
									.getDrawable(R.drawable.ic_justok_unsel));
						} else {
							/*
							 * Toast t = Toast.makeText(
							 * MyFreeBiesDetailsActivity.this, "JUST OK!",
							 * Toast.LENGTH_SHORT); t.setGravity(Gravity.CENTER,
							 * 0, 0); t.show();
							 */

							ShowToastWithImage("JUST OK!", getResources()
									.getDrawable(R.drawable.ic_justok_select));

							imageView_JustOk.setTag("1");
							imageView_JustOk.setImageDrawable(getResources()
									.getDrawable(R.drawable.ic_justok_select));
						}
						new RatingTask().execute(String.valueOf(offer_ID), "3");
					} catch (Exception e) {
					}
				}
			});
			imageView_JustOk
					.setOnLongClickListener(new View.OnLongClickListener() {

						@Override
						public boolean onLongClick(View v) {
							// TODO Auto-generated method stub
							try {
								Toast t = Toast.makeText(
										MyFreeBiesDetailsActivity.this,
										"JUST OK!", Toast.LENGTH_SHORT);
								t.setGravity(Gravity.CENTER, 0, 0);
								t.show();
							} catch (Exception e) {
							}
							return false;
						}
					});
			imageView_Poor = (ImageView) findViewById(R.id.imageView_Poor);
			imageView_Poor.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					try {
						imageView_Love.setTag("0");
						imageView_Love.setImageDrawable(getResources()
								.getDrawable(R.drawable.ic_loveit_unsel));
						imageView_LikeIt.setTag("0");
						imageView_LikeIt.setImageDrawable(getResources()
								.getDrawable(R.drawable.ic_likeit_unsel));
						imageView_JustOk.setTag("0");
						imageView_JustOk.setImageDrawable(getResources()
								.getDrawable(R.drawable.ic_justok_unsel));
						imageView_ItsBad.setTag("0");
						imageView_ItsBad.setImageDrawable(getResources()
								.getDrawable(R.drawable.ic_itsbad_unsel));
						if (imageView_Poor.getTag().equals("1")) {
							imageView_Poor.setTag("0");
							imageView_Poor.setImageDrawable(getResources()
									.getDrawable(R.drawable.ic_poor_unsel));
						} else {
							/*
							 * Toast t = Toast.makeText(
							 * MyFreeBiesDetailsActivity.this, "POOR!",
							 * Toast.LENGTH_SHORT); t.setGravity(Gravity.CENTER,
							 * 0, 0); t.show();
							 */

							ShowToastWithImage("POOR!", getResources()
									.getDrawable(R.drawable.ic_poor_select));
							imageView_Poor.setTag("1");
							imageView_Poor.setImageDrawable(getResources()
									.getDrawable(R.drawable.ic_poor_select));
						}
						new RatingTask().execute(String.valueOf(offer_ID), "2");
					} catch (Exception e) {
					}
				}
			});
			imageView_Poor
					.setOnLongClickListener(new View.OnLongClickListener() {

						@Override
						public boolean onLongClick(View v) {
							// TODO Auto-generated method stub
							try {
								Toast t = Toast.makeText(
										MyFreeBiesDetailsActivity.this,
										"POOR!", Toast.LENGTH_SHORT);
								t.setGravity(Gravity.CENTER, 0, 0);
								t.show();
							} catch (Exception e) {
							}
							return false;
						}
					});
			imageView_ItsBad = (ImageView) findViewById(R.id.imageView_ItsBad);
			imageView_ItsBad.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					try {
						imageView_Love.setTag("0");
						imageView_Love.setImageDrawable(getResources()
								.getDrawable(R.drawable.ic_loveit_unsel));
						imageView_LikeIt.setTag("0");
						imageView_LikeIt.setImageDrawable(getResources()
								.getDrawable(R.drawable.ic_likeit_unsel));
						imageView_JustOk.setTag("0");
						imageView_JustOk.setImageDrawable(getResources()
								.getDrawable(R.drawable.ic_justok_unsel));
						imageView_Poor.setTag("0");
						imageView_Poor.setImageDrawable(getResources()
								.getDrawable(R.drawable.ic_poor_unsel));
						if (imageView_ItsBad.getTag().equals("1")) {
							imageView_ItsBad.setTag("0");
							imageView_ItsBad.setImageDrawable(getResources()
									.getDrawable(R.drawable.ic_itsbad_unsel));
						} else {
							/*
							 * Toast t = Toast.makeText(
							 * MyFreeBiesDetailsActivity.this, "ITS BAD!",
							 * Toast.LENGTH_SHORT); t.setGravity(Gravity.CENTER,
							 * 0, 0); t.show();
							 */

							ShowToastWithImage("ITS BAD!", getResources()
									.getDrawable(R.drawable.ic_itsbad_select));
							imageView_ItsBad.setTag("1");
							imageView_ItsBad.setImageDrawable(getResources()
									.getDrawable(R.drawable.ic_itsbad_select));
						}
						new RatingTask().execute(String.valueOf(offer_ID), "1");
					} catch (Exception e) {
					}
				}
			});
			imageView_ItsBad
					.setOnLongClickListener(new View.OnLongClickListener() {

						@Override
						public boolean onLongClick(View v) {
							// TODO Auto-generated method stub
							try {
								Toast t = Toast.makeText(
										MyFreeBiesDetailsActivity.this,
										"ITS BAD!", Toast.LENGTH_SHORT);
								t.setGravity(Gravity.CENTER, 0, 0);
								t.show();
							} catch (Exception e) {
							}
							return false;
						}
					});
			// imageLoader = new ImageLoader(MyFreeBiesDetailsActivity.this);

			button_Call = (Button) findViewById(R.id.button_Call);
			button_Call.setTypeface(Reguler);

			button_Call.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					// if (!mobile.equalsIgnoreCase("")) {
					/*
					 * Intent callIntent = new Intent(Intent.ACTION_CALL);
					 * callIntent.setData(Uri.parse("tel:" + mobile));
					 * startActivity(callIntent);
					 */
					if (numberWithLocationList.size() >0) {
						// Log.d("Area", "Area");
						showDialogForCallList(numberWithLocationList);
					}else{
						if(dealer_contact.contains(dealer_contact)){
						String[] numberArray = dealer_contact.split(",");
						String firstNumber = numberArray[0].trim();
						Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + firstNumber));
						startActivity(intent);
						}else{
							Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + dealer_contact));
							startActivity(intent);
						}
					}
					// }
				}
			});

			button_Purchase = (Button) findViewById(R.id.button_Purchase);
			button_Purchase.setTypeface(Reguler);
			button_Purchase.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (by_voucher == 0) {
						if (button_Purchase.getText().toString().trim()
								.equalsIgnoreCase("GET IT NOW")) {
							String loyalityPoints = Utility.getBalance(
									MyFreeBiesDetailsActivity.this,
									Constants.SHAREDPREFERENCE_LOYALTY_POINTS);
							String giftPoints = String.valueOf(amount);
							int loyalityPointsInt = Integer
									.parseInt(loyalityPoints);
							int giftPointsInt = Integer.parseInt(giftPoints);
							if (giftPointsInt > loyalityPointsInt) {
								showDailogForCoinsBalance(loyalityPoints,
										giftPoints);
							} else {
								showTwoButtonDialog(
										MyFreeBiesDetailsActivity.this,
										"Confirmation",
										"Do you want to pocket this gift ?", "");
							}
						} else if (button_Purchase.getText().toString().trim()
								.equalsIgnoreCase("REDEEM NOW")) {
							showTwoButtonDialog(MyFreeBiesDetailsActivity.this,
									"Redeem Coupon", "Enter password");
						}
					} else if (by_voucher == 1) {
						if (button_Purchase.getText().toString().trim()
								.equalsIgnoreCase("GET IT NOW")) {
							showPurchaseDialog();
						}
					}
				}
			});

			/*
			 * img_redeem_now.setOnClickListener(new OnClickListener() {
			 * 
			 * @Override public void onClick(View v) { // TODO Auto-generated
			 * method stub showTwoButtonDialog(MyFreeBiesDetailsActivity.this,
			 * "Redeem Coupon", "Enter password"); } });
			 */

			LinearLayout rupeeLayout = (LinearLayout) findViewById(R.id.rupeeLayout);
			// RelativeLayout
			 locationLayout=(RelativeLayout)findViewById(R.id.locationLayout);

			linearLayout_More = (LinearLayout) findViewById(R.id.linearLayout_More);
			linearLayout_More.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (webView_Details.getVisibility() == View.VISIBLE) {
						webView_Details.setVisibility(View.GONE);
						img_right_arrow.setImageDrawable(SVGParser
								.getSVGFromResource(
										MyFreeBiesDetailsActivity.this
												.getResources(),
										R.raw.ic_dwnarrow)
								.createPictureDrawable());
						textView_More.setText("MORE");

					} else {
						img_right_arrow.setImageDrawable(SVGParser
								.getSVGFromResource(
										MyFreeBiesDetailsActivity.this
												.getResources(),
										R.raw.ic_uparrow)
								.createPictureDrawable());
						webView_Details.setVisibility(View.VISIBLE);
						textView_More.setText("LESS");
					}

				}
			});
			img_right_arrow = (ImageView) findViewById(R.id.img_right_arrow);
			img_right_arrow.setImageDrawable(SVGParser.getSVGFromResource(
					this.getResources(), R.raw.ic_dwnarrow)
					.createPictureDrawable());

			view_sep = (LinearLayout) findViewById(R.id.view_sep);

			textView_Rate = (TextView) findViewById(R.id.textView_Rate);
			textView_Rate.setTypeface(Reguler);
			textView_AddressMore = (TextView) findViewById(R.id.textView_AddressMore);
			textView_AddressMore.setTypeface(Reguler);
			Drawable drawableLocation = SVGParser.getSVGFromResource(
					this.getResources(), R.raw.ic_shoplocator)
					.createPictureDrawable();
			location_img = (ImageView) findViewById(R.id.location_img);
			location_img.setImageDrawable(drawableLocation);
			// textView_AddressMore.setCompoundDrawables(drawableLocation, null,
			// null, null);
			// location_img.setAlpha(0.5f);
			// Constants.setAlpha(location_img, 0.6f);
			textView_More = (TextView) findViewById(R.id.textView_More);
			textView_More.setTypeface(Reguler);

			textView_DealName = (TextView) findViewById(R.id.textView_DealName);
			textView_DealName.setTypeface(Reguler);

			textView_OfferName = (TextView) findViewById(R.id.textView_OfferName);
			textView_OfferName.setTypeface(Reguler);

			textView_OfferDescription = (TextView) findViewById(R.id.textView_OfferDescription);
			textView_OfferDescription.setTypeface(Reguler);

			textView_Distance = (TextView) findViewById(R.id.textView_Distance);
			textView_Distance.setTypeface(Reguler);

			/*
			 * textView_Unit = (TextView) findViewById(R.id.textView_Unit);
			 * textView_Unit.setTypeface(Reguler);
			 */

			textView_Address = (TextView) findViewById(R.id.textView_Address);
			textView_Address.setTypeface(Reguler);

			textView_Mobile = (TextView) findViewById(R.id.textView_Mobile);
			textView_Mobile.setTypeface(Reguler);

			textView_Voucher = (TextView) findViewById(R.id.textView_Voucher);
			textView_Voucher.setTypeface(Reguler);

			textView_ExpiryDate = (TextView) findViewById(R.id.textView_ExpiryDate);
			textView_ExpiryDate.setTypeface(Reguler);
			textView_Pin = (TextView) findViewById(R.id.textView_Pin);
			textView_Pin.setTypeface(Reguler);

			/*
			 * textView_Points = (TextView) findViewById(R.id.textView_Points);
			 * textView_Points.setTypeface(Reguler);
			 */
			textView_GiftCoins = (TextView) findViewById(R.id.textView_GiftCoins);
			textView_GiftCoins.setTypeface(Reguler);

			RotateAnimation rotate = (RotateAnimation) AnimationUtils
					.loadAnimation(this, R.anim.rotate_text);
			// textView_GiftCoins.setAnimation(rotate);
			textView_like = (TextView) findViewById(R.id.textView_like);
			textView_like.setTypeface(Reguler);
			textView_ShopGallery = (TextView) findViewById(R.id.textView_ShopGallery);
			textView_ShopGallery.setTypeface(Reguler);
			view_ShopGallery = (LinearLayout) findViewById(R.id.view_ShopGallery);
			headPhotos = (View) findViewById(R.id.headPhotos);

			linearLayout_Main = (LinearLayout) findViewById(R.id.linearLayout_Main);

			relativeLayout_Gallery = (RelativeLayout) findViewById(R.id.relativeLayout_Gallery);

			textView_Brand = (TextView) findViewById(R.id.textView_Brand);
			textView_Brand.setTypeface(Reguler);

			textView_BrandDescription = (TextView) findViewById(R.id.textView_BrandDescription);
			textView_BrandDescription.setTypeface(Reguler);

			textView_Rating = (TextView) findViewById(R.id.textView_Rating);

			textView_Rating.setTypeface(Reguler);
			textView_Rating.setVisibility(View.GONE);

			textView_Offer = (TextView) findViewById(R.id.textView_Offer);
			textView_Offer.setTypeface(Reguler);

			textView_OfferDetails = (TextView) findViewById(R.id.textView_OfferDetails);
			textView_OfferDetails.setTypeface(Reguler);
			webView_Offer_Details = (WebView) findViewById(R.id.webView_Offer_Details);
			relativeLayout_Locate = (RelativeLayout) findViewById(R.id.relativeLayout_Locate);
			relativeLayout_Locate.setVisibility(View.GONE);
			relativeLayout_Locate
					.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub

							boolean gps_enabled = false;
							boolean network_enabled = false;
							LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
							try {
								gps_enabled = lm
										.isProviderEnabled(LocationManager.GPS_PROVIDER);
							} catch (Exception ex) {
							}
							try {
								network_enabled = lm
										.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
							} catch (Exception ex) {
							}

							// don't start listeners if no provider is
							// enabled
							boolean hasGPS = getPackageManager()
									.hasSystemFeature(
											PackageManager.FEATURE_LOCATION_GPS);
							WifiManager wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
							if (!(network_enabled && wifi.isWifiEnabled())
									&& (hasGPS && !gps_enabled)) {

								showTwoButtonDialog(
										MyFreeBiesDetailsActivity.this,
										"Shop Locator",
										"Activate your location services to avail this free gift.",
										Constants.DIALOG_CLOSE_LOCATION);
							} else if (gps_enabled || network_enabled) {
								boolean show_route = false;

								if (wifi.isWifiEnabled())
									show_route = true;
								else if (network_enabled && gps_enabled)
									show_route = true;
								else if (network_enabled)
									show_route = true;
								try {

									ArrayList<WeakHashMap<String, String>> temp = new ArrayList<WeakHashMap<String, String>>();

									for (int j = 0; j < data_location.size(); j++) {
										WeakHashMap<String, String> map1 = new WeakHashMap<String, String>();
										WeakHashMap<String, String> map = new WeakHashMap<String, String>(
												data_location.get(j));
										if (Integer.parseInt(map.get(DEAL_ID)) == Integer
												.parseInt(deal_ID)) {
											map1.put(LATITUDE,
													map.get(LATITUDE));
											map1.put(LONGITUDE,
													map.get(LONGITUDE));
											map1.put(ADDRESS, map.get(ADDRESS));
											map1.put(CITY, map.get(CITY));
											map1.put(AREA, map.get(CITY));
											map1.put(STATE, map.get(STATE));
											map1.put(FULL_ADDERESS,
													map.get(FULL_ADDERESS));
											temp.add(map1);
										}
									}
									if (temp.size() > 1) {
										Intent intent = new Intent(
												MyFreeBiesDetailsActivity.this,
												FreeBiesLocationActivity.class);
										intent.putExtra("ROUTE", show_route);
										intent.putExtra("DEAL",
												textView_DealName.getText()
														.toString().trim());
										intent.putExtra("ID", deal_ID);
										intent.putExtra("LOGO_URL", logo_url);
										intent.putExtra("FROM_DETAILS", true);
										intent.putExtra("LOCATIONS",
												data_location);
										startActivity(intent);
									} else {
										Intent intent = new Intent(
												MyFreeBiesDetailsActivity.this,
												FreeBiesLocatorActivity.class);
										WeakHashMap<String, String> map = new WeakHashMap<String, String>(
												temp.get(0));
										intent.putExtra("ROUTE", show_route);
										intent.putExtra("DEAL",
												textView_DealName.getText()
														.toString().trim());
										intent.putExtra("LATITUDE", map
												.get(FreeBiesActivity.LATITUDE));
										intent.putExtra(
												"LONGITUDE",
												map.get(FreeBiesActivity.LONGITUDE));
										intent.putExtra("ADDRESS", map
												.get(FreeBiesActivity.ADDRESS));
										intent.putExtra("CITY",
												map.get(FreeBiesActivity.CITY));
										intent.putExtra("STATE",
												map.get(FreeBiesActivity.STATE));
										intent.putExtra("FULL_ADDRESS",
												map.get(FULL_ADDERESS));
										startActivity(intent);
									}
								} catch (Exception e) {
									Intent intent = new Intent(
											MyFreeBiesDetailsActivity.this,
											FreeBiesLocationActivity.class);
									intent.putExtra("ROUTE", show_route);
									intent.putExtra("DEAL", textView_DealName
											.getText().toString().trim());
									intent.putExtra("ID", deal_ID);
									intent.putExtra("FROM_DETAILS", true);
									intent.putExtra("LOCATIONS", data_location);
									startActivity(intent);
								}
							} else {
								showTwoButtonDialog(
										MyFreeBiesDetailsActivity.this,
										"Shop Locator",
										"Turn on your location service on the phone to locate "
												+ textView_DealName.getText()
														.toString().trim()
												+ ".",
										Constants.DIALOG_CLOSE_LOCATION);
							}

						}
					});

			data_location = new ArrayList<WeakHashMap<String, String>>();

			textView_Title = (TextView) findViewById(R.id.textView_Title);
			textView_Title.setTypeface(Medium);
			// progressBar1 = (ProgressBar) findViewById(R.id.progressBar1);

			Bundle bundle = getIntent().getExtras();
			if (bundle != null) {
				final FreeBies freeBie = (FreeBies) getIntent()
						.getSerializableExtra("GIFT");
				offer_ID = freeBie.getFreebieOfferID();
				amount = freeBie.getFreebieMinAmount();
				mobile = freeBie.getFreebieDealerMobile();
				price = freeBie.getprice();
				offer_price = freeBie.getFreeBieOfferPrice();
				TextView textView_Rupee_deatils = (TextView) findViewById(R.id.textView_Rupee_deatils);
				textView_Rupee_deatils.setText(offer_price + "");
				textView_Rupee_deatils.setTypeface(Reguler);

				ImageView imageView_Rupee_deatils = (ImageView) findViewById(R.id.imageView_Rupee_deatils);
				imageView_Rupee_deatils
						.setImageDrawable(SVGParser.getSVGFromResource(
								this.getResources(), R.raw.ic_rupee)
								.createPictureDrawable());

				if (mobile.equalsIgnoreCase("")) {
					button_Call.setVisibility(View.INVISIBLE);
					//button_Call.setBackgroundColor(Color.GRAY);
				}
				// imageLoader.getDealImage(bundle.getString("URL"),
				// imageView_Image, progressBar1);
				try {
					Picasso.with(MyFreeBiesDetailsActivity.this)
							.load(freeBie.getFreebieURL())
							.placeholder(R.drawable.deal_default).fit()
							.into(imageView_Image);
				} catch (Exception e) {
				}
				try {
					String url = freeBie.getFreebieLogoURL();
					Picasso.with(MyFreeBiesDetailsActivity.this)
							.load(freeBie.getFreebieLogoURL())
							.placeholder(R.drawable.deal_icon)
							.into(imageView_Photo);
					logo_url = freeBie.getFreebieLogoURL();
				} catch (Exception e) {
				}
				/*
				 * try { Picasso.with(MyFreeBiesDetailsActivity.this)
				 * .load("https://maps.googleapis.com/maps/api/staticmap?center="
				 * + freeBie.getFreebieLat() + "," + freeBie.getFreebieLng() +
				 * "&zoom=13&size=350x200&maptype=roadmap&markers=color:red%7Clabel:A%7C"
				 * + freeBie.getFreebieLat() + "," + freeBie.getFreebieLng() +
				 * "&format=gif") .fit().into(imageView_Locate); } catch
				 * (Exception e) { }
				 */
				if (freeBie.getFreebieLike() == 0)
					imageView_Like.setImageResource(R.drawable.ic_unlike);
				else
					imageView_Like.setImageResource(R.drawable.ic_like);
				imageView_Like.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if (freeBie.getFreebieLike() == 0) {
							imageView_Like.setImageResource(R.drawable.ic_like);
							freeBie.setFreebieLike(1);
						} else {
							imageView_Like
									.setImageResource(R.drawable.ic_unlike);
							freeBie.setFreebieLike(0);
						}
						new LikeTask().execute(offer_ID + "");
					}
				});

				by_voucher = freeBie.getFreeBieByVoucher();
				textView_Title.setText(freeBie.getFreebieDealName());
				textView_OfferName.setText("");
				textView_OfferDescription
						.setText(freeBie.getFreebieShortDesc());
				textView_GiftCoins.setText(String.valueOf(freeBie
						.getFreebieMinAmount()));
				/*
				 * textView_Points.setText(String.valueOf(freeBie
				 * .getFreebieMinAmount())+"\nGift\nCoins");
				 */
				/*
				 * textView_Points.setText(Html.fromHtml("<html><body><font>"+String
				 * .valueOf(freeBie .getFreebieMinAmount())
				 * +"</font><font>Gift\nCoins</font> </body><html>"));
				 */

				String s = String.valueOf(freeBie.getFreebieMinAmount())
						+ "\nGift\nCoins";
				SpannableString ss1 = new SpannableString(s);
				ss1.setSpan(new RelativeSizeSpan(1.3f), 0, 4, 0); // set size
				// ss1.setSpan(new ForegroundColorSpan(Color.RED), 0, 4, 0);//
				// set color
				// textView_Points.setText(ss1);
				RotateAnimation rotate1 = (RotateAnimation) AnimationUtils
						.loadAnimation(this, R.anim.rotate_text);
				// textView_Points.setAnimation(rotate1);

				String message1 = "<font><span style='padding-top:20px'>REDEEM </span></font> <h1>"
						+ String.valueOf(freeBie.getFreebieMinAmount())
						+ "</h1><font> <span style='padding-bottom:20px'>Gift Coins</span></font>";

				// textView_Points.setText(Html.fromHtml(message1));

				if (bundle.getBoolean("MY")) {
					textView_Mobile.setVisibility(View.VISIBLE);
					textView_Voucher.setVisibility(View.VISIBLE);
					textView_ExpiryDate.setVisibility(View.VISIBLE);
					view_sep.setVisibility(View.VISIBLE);
					layout_like.setVisibility(View.GONE);
					imageView_Like.setVisibility(View.GONE);
					textView_like.setVisibility(View.GONE);
					String str = Utility.getMobileNumber(
							MyFreeBiesDetailsActivity.this,
							Constants.SHAREDPREFERENCE_MOBILE);
					try {
						String temp = str.substring(0, 6);
						textView_Mobile.setText("Mobile number: "
								+ str.replaceAll(temp, "XXXXXX"));
					} catch (Exception e) {
						textView_Mobile.setText("Mobile number: " + str);
					}
					textView_Voucher.setText("Voucher code: "
							+ freeBie.getFreebieCode());

					if (freeBie.getPin().equalsIgnoreCase("")) {
						textView_Pin.setVisibility(View.GONE);
					} else {
						textView_Pin.setVisibility(View.VISIBLE);
						textView_Pin.setText("Pin : "+freeBie.getPin());
					}

					textView_ExpiryDate.setText("Expiry date: "
							+ freeBie.getFreebieExpiry());
					// button_Purchase.setText("YAY! It's yours now.\nVisit store & get it");

					button_Purchase.setText("In your pocket");
					// img_redeem_now.setVisibility(View.GONE);

					button_Purchase.setBackground(getResources().getDrawable(
							R.drawable.gray_pocket_selector));
					// button_Purchase.setEnabled(false);
					textView_OfferStatus.setVisibility(View.VISIBLE);
					textView_OfferStatus.setTypeface(Reguler);
					textView_OfferStatus
							.setText(Html
									.fromHtml("<p margin-top: 0px;width: 1271px;height: 6px;'><h1><b>Yay! It's yours now.</b></h1></p><p><h4>Visit  store to get it.</h4></p>"));
					// imageView_Expired.setImageResource(R.drawable.ic_pocketed);
					textView_OfferStatus.setText("");
					if (freeBie.getFreebieCouponStatusID() == 3) {
						textView_OfferStatus.setVisibility(View.VISIBLE);
						// img_redeem_now.setVisibility(View.GONE);
						button_Purchase.setEnabled(false);
						textView_OfferStatus.setText("EXPIRED");
						textView_OfferStatus.setTypeface(Reguler);
						textView_OfferStatus.setTextSize(21);
						button_Purchase.setBackground(this.getResources()
								.getDrawable(R.drawable.gray_pocket_selector));
						/*
						 * imageView_Expired
						 * .setImageResource(R.drawable.ic_expired);
						 */
					} else if (freeBie.getFreebieCouponStatusID() == 2) {
						button_Purchase.setText("REDEEMED");
						button_Purchase.setEnabled(false);
						textView_OfferStatus.setVisibility(View.VISIBLE);
						// img_redeem_now.setVisibility(View.GONE);
						textView_OfferStatus.setText("REDEEMED");
						/*
						 * imageView_Expired
						 * .setImageResource(R.drawable.ic_redeemed);
						 */
					}
				} else {
					textView_Mobile.setVisibility(View.GONE);
					textView_Voucher.setVisibility(View.GONE);
					textView_ExpiryDate.setVisibility(View.GONE);
					view_sep.setVisibility(View.GONE);
					textView_Pin.setVisibility(View.GONE);
					imageView_Like.setVisibility(View.VISIBLE);
					button_Purchase.setText("GET IT NOW");
					/*
					 * button_Purchase.setBackgroundColor(getResources().getColor
					 * ( R.color.blue));
					 */

					button_Purchase.setBackground(getResources().getDrawable(
							R.drawable.button_home_gift_selector));
				}

				textViewOffer.setText(textView_Title.getText().toString());
				if (freeBie.getFreeBieByVoucher() == 1) {

					rupeeLayout.setVisibility(View.VISIBLE);
					// locationLayout.setVisibility(View.GONE);

				} else {

					rupeeLayout.setVisibility(View.GONE);
					// locationLayout.setVisibility(View.VISIBLE);

				}

				deal_ID = String.valueOf(freeBie.getFreebieDealID());
				new DealDetailsTask().execute(deal_ID, Utility
						.getCurrentLatitude(MyFreeBiesDetailsActivity.this,
								Constants.SHAREDPREFERENCE_CURRENT_LATITUDE),
						Utility.getCurrentLongitude(
								MyFreeBiesDetailsActivity.this,
								Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE));

			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}

	protected void ShowToastWithImage(String msg, Drawable drawable) {
		// TODO Auto-generated method stub
		View toastView = getLayoutInflater().inflate(R.layout.toast,
				(ViewGroup) findViewById(R.id.toastLayout));
		ImageView imageView = (ImageView) toastView.findViewById(R.id.image);

		imageView.setImageDrawable(drawable);
		// imageView.setBackgroundDrawable(bitmapDrawable);

		TextView textView = (TextView) toastView.findViewById(R.id.text);

		textView.setText(msg);
		Toast toast = new Toast(MyFreeBiesDetailsActivity.this);

		toast.setGravity(Gravity.CENTER_VERTICAL, 0, -100);
		toast.setDuration(200);
		toast.setView(toastView);

		toast.show();
	}

	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView().build());
		// EasyTracker.getInstance(this).activityStart(this);
		easyTracker.send(MapBuilder.createAppView().build());
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		// check if the request code is same as what is passed here it is 2
		if (requestCode == 2) {
			String dataFromPG = data.getStringExtra("RESULT");
			if (dataFromPG.equalsIgnoreCase("1")) {
				Constants.showOneButtonDialog(MyFreeBiesDetailsActivity.this,
						"Congratulations!",
						"This is yours now. Visit store & get it.",
						Constants.DIALOG_CLOSE_FORGOTPIN);

				String str = Utility.getMobileNumber(
						MyFreeBiesDetailsActivity.this,
						Constants.SHAREDPREFERENCE_MOBILE);
				try {
					String temp = str.substring(0, 6);
					textView_Mobile.setText("Mobile number: "
							+ str.replaceAll(temp, "XXXXXX"));
				} catch (Exception e) {
					textView_Mobile.setText("Mobile number: " + str);
				}
				textView_Voucher.setText("Voucher code: "
						+ data.getStringExtra("deal_coupon_code"));
				try {
					textView_ExpiryDate.setText("Expiry date: "
							+ data.getStringExtra("expiry"));
				} catch (Exception e) {
				}

				textView_Mobile.setVisibility(View.VISIBLE);
				textView_Voucher.setVisibility(View.VISIBLE);
				textView_ExpiryDate.setVisibility(View.VISIBLE);
				view_sep.setVisibility(View.VISIBLE);
				button_Purchase.setText("REDEEM NOW");
				button_Purchase.setBackgroundDrawable(getResources()
						.getDrawable(R.drawable.gray_pocket_selector));

				Utility.setRefreshRequired(MyFreeBiesDetailsActivity.this,
						Constants.SHAREDPREFERENCE_REFESH_MY_GIFT, true);

				/*
				 * button_Purchase.setText("REDEEM NOW"); //
				 * img_redeem_now.setVisibility(View.GONE);
				 * button_Purchase.setBackgroundDrawable(getResources()
				 * .getDrawable(R.drawable.gray_pocket_selector));
				 */

				textView_OfferStatus.setVisibility(View.VISIBLE);
				textView_OfferStatus
						.setText(Html
								.fromHtml("<p margin-top: 0px;width: 1271px;height: 6px;'><h1><b>Yay! It's yours now.</b></h1></p><p><h4>Visit  store to get it.</h4></p>"));

			} else {

				Constants.showOneButtonDialog(MyFreeBiesDetailsActivity.this,
						"Gift", "Transaction Failed. Please try again",
						Constants.DIALOG_CLOSE);
			}
		}
	}

	@Override
	public void onStop() {
		super.onStop();
		// EasyTracker.getInstance(this).activityStop(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		// Constants.showNavigationBar(DealDetailsActivity.this);
		try {
			LocationResult locationResult = new LocationResult() {
				@Override
				public void gotLocation(Location location) {
					// Got the location!
					try {
						// Log.w("Location", location + "");
						if (location != null) {
							location.getLongitude();
							location.getLatitude();

							/*
							 * Utility.setCurrentLatitude(
							 * MyFreeBiesDetailsActivity.this,
							 * Constants.SHAREDPREFERENCE_CURRENT_LATITUDE,
							 * String.valueOf(location.getLatitude()));
							 * Utility.setCurrentLongitude(
							 * MyFreeBiesDetailsActivity.this,
							 * Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE,
							 * String.valueOf(location.getLongitude()));
							 */

							GPSTracker gps = new GPSTracker(
									MyFreeBiesDetailsActivity.this);
							if (gps.canGetLocation()) { // gps enabled} //
														// return boolean
														// true/false

								gps.getLatitude(); // returns latitude
								gps.getLongitude(); // returns longitude

								Utility.setCurrentLatitude(
										MyFreeBiesDetailsActivity.this,
										Constants.SHAREDPREFERENCE_CURRENT_LATITUDE,
										String.valueOf(gps.getLatitude()));
								Utility.setCurrentLongitude(
										MyFreeBiesDetailsActivity.this,
										Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE,
										String.valueOf(gps.getLongitude()));
							}
						}
					} catch (Exception exception) {
					}
				}
			};
			MyLocation myLocation = new MyLocation();
			myLocation.getLocation(this, locationResult);
		} catch (Exception exception) {

		}
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
	}

	public class DealDetailsTask extends AsyncTask<String, String, String>
			implements OnDismissListener {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			try {
				String response = RequestClass.getInstance()
						.readPay1B2CRequest(
								MyFreeBiesDetailsActivity.this,
								Constants.B2C_URL + "get_deal_details/?id="
										+ params[0] + "&latitude=" + params[1]
										+ "&longitude=" + params[2]
										+ "&api_version=3");

				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			try {

				if (!result.startsWith("Error")) {
					String replaced = result;

					JSONObject jsonObject = new JSONObject(replaced);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {

						JSONObject jsonObject_Desc = jsonObject
								.getJSONObject("description");

						JSONArray jsonArrayDetails = jsonObject_Desc
								.getJSONArray("deal_detail");

						for (int i = 0; i < jsonArrayDetails.length(); i++) {
							JSONObject jsonObjectInner = jsonArrayDetails
									.getJSONObject(i);
							// String
							// offer_info_html=jsonObject_Desc.getString("offer_info_html");
							String str = jsonObjectInner.getString(
									"content_txt").trim();
							webView_Details
									.loadDataWithBaseURL(
											null,
											"<html><head><style>@font-face {font-family: Normal;src: url(\"file:///android_asset/Roboto-Regular.ttf\")}"
													+ "@font-face {font-family: Narrow;src: url(\"file:///android_asset/Roboto-Regular.ttf\")}"
													+ "body {margin: 0px 5px;padding:0px;color:#000000;}"
													+ "h1{font-size:16px;font-family:\"Normal\";margin:0;padding:10px 0 5px 0;color:#000000;}"
													+ "h2{font-size:16px;font-family:\"Normal\";margin:0;padding:10px 0 5px 0;color:#000000;}"
													+ "ul{margin:0 0 10px 0;padding-left:20px;}"
													+ "li{font-size:14px;margin:0 0 2px 0;font-family:\"Normal\";}</style></head><body>"
													+ str + "</body></html>",
											"text/html", "UTF-8", null);

						}

						String des = jsonObject_Desc
								.getString("brand_description");
						textView_BrandDescription.setText(des);
						if (des.equalsIgnoreCase("")) {
							textView_Brand.setVisibility(View.GONE);
							textView_BrandDescription.setVisibility(View.GONE);
						}

						data_location.clear();
						numberWithLocationList.clear();
						JSONArray jsonArrayLocation = jsonObject_Desc
								.getJSONArray("location_detail");
						if (jsonArrayLocation.length() > 0) {
							for (int i = 0; i < jsonArrayLocation.length(); i++) {
								JSONObject jsonObjectInner = jsonArrayLocation
										.getJSONObject(i);
								WeakHashMap<String, String> map = new WeakHashMap<String, String>();
								map.put(DEAL_ID,
										jsonObjectInner.getString("deal_id"));
								double latitude = Double
										.parseDouble(jsonObjectInner
												.getString("lat"));
								double longitude = Double
										.parseDouble(jsonObjectInner
												.getString("lng"));

								map.put(LATITUDE, String.valueOf(latitude));
								map.put(LONGITUDE, String.valueOf(longitude));
								map.put(ADDRESS,
										jsonObjectInner.getString("address"));
								map.put(CITY, jsonObjectInner.getString("city"));
								map.put(AREA, jsonObjectInner.getString("area"));
								map.put(STATE,
										jsonObjectInner.getString("state"));
								map.put(FULL_ADDERESS, jsonObjectInner
										.getString("full_address"));
								map.put(NUMBER, jsonObjectInner
										.getString("dealer_contact"));
							//	dealer_contact=jsonObjectInner.getString("dealer_contact");
								HashMap<String, String> numberLocationMap = new HashMap<String, String>();
								numberLocationMap.put("number", jsonObjectInner
										.getString("dealer_contact"));
								numberLocationMap.put("area",
										jsonObjectInner.getString("area"));
								numberLocationMap.put("city",
										jsonObjectInner.getString("city"));
								numberLocationMap.put("address",
										jsonObjectInner.getString("address"));

								if (jsonObjectInner.getString("dealer_contact")
										.isEmpty()
										&& jsonObjectInner.getString(
												"dealer_contact").length() == 0) {

								} else {
									numberWithLocationList
											.add(numberLocationMap);
								}
								if (numberWithLocationList.size() >= 1) {
									button_Call.setVisibility(View.VISIBLE);
									button_Call.setEnabled(true);
								} else {
									button_Call.setVisibility(View.VISIBLE);
									button_Call.setEnabled(false);
								}
								data_location.add(map);

								if (i == 0) {
									textView_DealName
											.setText(MyFreeBiesDetailsActivity.this.textView_Title
													.getText().toString());
									/*
									 * textView_Address .setText(jsonObjectInner
									 * .getString("address") + ", " +
									 * jsonObjectInner .getString("area") + ", "
									 * + jsonObjectInner .getString("city"));
									 */
									textView_Area.setText(jsonObjectInner
											.getString("area"));
									textView_Address.setText(jsonObjectInner
											.getString("full_address"));
									double dis = 0;
									DecimalFormat decimalFormat = new DecimalFormat(
											"0.0");
									try {
										String lat = Utility
												.getCurrentLatitude(
														MyFreeBiesDetailsActivity.this,
														Constants.SHAREDPREFERENCE_CURRENT_LATITUDE);
										String lng = Utility
												.getCurrentLongitude(
														MyFreeBiesDetailsActivity.this,
														Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE);
										String dist = "0";
										if (lat != "" && lng != "")
											dist = String
													.valueOf(Constants.distanceFrom(
															Double.parseDouble(lat),
															Double.parseDouble(lng),
															latitude, longitude));

										dis = Double.parseDouble(dist);

										if (dis < 1) {
											// dis = dis * 1000;
											textView_Distance
													.setText(decimalFormat
															.format(dis));
											// textView_Unit.setText("(M)");
											/*
											 * textViewDistance.setText("< "+
											 * decimalFormat .format(dis) +
											 * "\nkm");
											 */
											textViewDistance
													.setText(" : < 1 km ");
										} else {
											textView_Distance
													.setText(decimalFormat
															.format(dis));
											// textView_Unit.setText("(KM)");
											textViewDistance.setText(" : "
													+ decimalFormat.format(dis)
													+ "km");
										}

									} catch (Exception e) {
									}
									if (jsonArrayLocation.length() > 1) {
										textView_AddressMore
												.setVisibility(View.VISIBLE);
										textView_AddressMore
												.setText((jsonArrayLocation
														.length() - 1 + "  more locations")
														.toUpperCase());
									} else {
										textView_AddressMore
												.setVisibility(View.GONE);
									}

									try {
										Picasso.with(
												MyFreeBiesDetailsActivity.this)
												.load("https://maps.googleapis.com/maps/api/staticmap?center="
														+ latitude
														+ ","
														+ longitude
														+ "&zoom=13&size=350x200&maptype=roadmap&markers=color:red%7Clabel:A%7C"
														+ latitude
														+ ","
														+ longitude
														+ "&format=gif").fit()
												.into(imageView_Locate);
									} catch (Exception e) {
									}

								}
							}

							relativeLayout_Locate.setVisibility(View.VISIBLE);
						}else if(jsonArrayLocation.length()==0){
							locationLayout.setVisibility(View.GONE);
						}

						// mobile = jsonObject_Desc.getString("dealer_contact");
						/*
						 * if (mobile.equalsIgnoreCase("")) {
						 * 
						 * button_Call.setBackgroundColor(Color.GRAY); }
						 */
						JSONArray jsonArrayOfferDeatils = jsonObject_Desc
								.getJSONArray("offer_detail");
						for (int i = 0; i < jsonArrayOfferDeatils.length(); i++) {
							int rating = 0;
							try {
								rating = Integer.parseInt(jsonArrayOfferDeatils
										.getJSONObject(i).getString("off_avg"));
								switch (rating) {
								case 1:
									imageView_Rating
											.setImageResource(R.drawable.ic_itsbad_select);
									break;
								case 2:
									imageView_Rating
											.setImageResource(R.drawable.ic_poor_select);
									break;
								case 3:
									imageView_Rating
											.setImageResource(R.drawable.ic_justok_select);
									break;
								case 4:
									imageView_Rating
											.setImageResource(R.drawable.ic_likeit_select);
									break;
								case 5:
									imageView_Rating
											.setImageResource(R.drawable.ic_loveit_select);
									break;
								case 0:
									imageView_Rating
											.setImageResource(R.drawable.ic_loveit_unsel);
									break;
								default:
									imageView_Rating.setVisibility(View.GONE);
									textView_Rating.setVisibility(View.GONE);
									break;
								}
							} catch (Exception e) {
							}

							try {
							
								review = Integer.parseInt(jsonArrayOfferDeatils
										.getJSONObject(i).getString("review"));
								imageView_ItsBad.setTag("0");
								imageView_Poor.setTag("0");
								imageView_JustOk.setTag("0");
								imageView_LikeIt.setTag("0");
								imageView_Love.setTag("0");
								switch (review) {
								case 1:
									imageView_ItsBad.setTag("1");
									imageView_ItsBad
											.setImageResource(R.drawable.ic_itsbad_select);
									break;
								case 2:
									imageView_Poor.setTag("1");
									imageView_Poor
											.setImageResource(R.drawable.ic_poor_select);
									break;
								case 3:
									imageView_JustOk.setTag("1");
									imageView_JustOk
											.setImageResource(R.drawable.ic_justok_select);
									break;
								case 4:
									imageView_LikeIt.setTag("1");
									imageView_LikeIt
											.setImageResource(R.drawable.ic_likeit_select);
									break;
								case 5:
									imageView_Love.setTag("1");
									imageView_Love
											.setImageResource(R.drawable.ic_loveit_select);
									break;
								default:
									imageView_ItsBad.setTag("0");
									imageView_Poor.setTag("0");
									imageView_JustOk.setTag("0");
									imageView_LikeIt.setTag("0");
									imageView_Love.setTag("0");
									break;
								}
							} catch (Exception e) {
							}
							// if (rating != 0) {
							// textView_Rating.setText(String.valueOf(rating));
							imageView_Rating.setVisibility(View.VISIBLE);
							// textView_Rating.setVisibility(View.VISIBLE);
							// }
						}
						final JSONArray jsonArrayImage = jsonObject_Desc
								.getJSONArray("image_list");
						imageArray = jsonArrayImage;
						if (jsonArrayImage.length() != 0) {
							textView_ShopGallery.setVisibility(View.VISIBLE);
							linearLayout_Main.setVisibility(View.VISIBLE);
							// linearLayout_Main.setBackgroundColor(getResources().getColor(R.color.RosyBrown));
							view_ShopGallery.setVisibility(View.VISIBLE);
							headPhotos.setVisibility(View.VISIBLE);
							HorizontalScrollView hs = new HorizontalScrollView(
									MyFreeBiesDetailsActivity.this);
							GiftImageGalleryLayout giftCell = new GiftImageGalleryLayout(
									MyFreeBiesDetailsActivity.this);

							hs.addView(giftCell);
							linearLayout_Main.addView(hs);

							for (int i = 0; i < jsonArrayImage.length(); i++)
								giftCell.add(jsonArrayImage.getString(i));

						} else {
							textView_ShopGallery.setVisibility(View.GONE);
							linearLayout_Main.setVisibility(View.GONE);
							view_ShopGallery.setVisibility(View.GONE);
							headPhotos.setVisibility(View.GONE);

						}

						JSONObject jsonObjectOfferInfo = jsonObject_Desc
								.getJSONObject("offer_info");
						dealer_contact=jsonObjectOfferInfo
								.getString("dealer_contact");
						if(!dealer_contact.isEmpty()){
							button_Call.setVisibility(View.VISIBLE);
							button_Call.setEnabled(true);
						}
						String pocketGift = jsonObject_Desc
								.getString("pocketGift");

						company_url = jsonObject_Desc.getString("company_url");
						String offer_info_html = jsonObject_Desc
								.getString("offer_info_html");

						webView_Offer_Details
								.loadDataWithBaseURL(
										null,
										"<html><head><style>@font-face {font-family: Normal;src: url(\"file:///android_asset/Roboto-Regular.ttf\")}"
												+ "@font-face {font-family: Narrow;src: url(\"file:///android_asset/Roboto-Regular.ttf\")}"
												+ "body {margin: 0px 5px;padding:0px;color:#000000;}"
												+ "h1{font-size:16px;font-family:\"Normal\";margin:0;padding:10px 0 5px 0;color:#000000;}"
												+ "h2{font-size:16px;font-family:\"Normal\";margin:0;padding:10px 0 5px 0;color:#000000;}"
												+ "ul{margin:0 0 10px 0;padding-left:20px;}"
												+ "li{font-size:14px;margin:0 0 2px 0;font-family:\"Normal\";}</style></head><body>"
												+ offer_info_html
												+ "</body></html>",
										"text/html", "UTF-8", null);
						if (company_url.isEmpty() && company_url.length() == 0) {
							visitUrlLayout.setVisibility(View.GONE);
						} else {
							visitUrlLayout.setVisibility(View.VISIBLE);
						}

						Log.d("pocketGift", "pocketGift " + pocketGift);

						if (pocketGift.equalsIgnoreCase("1")) {
							button_Purchase.setText("REDEEM NOW");
							// img_redeem_now.setVisibility(View.GONE);

							button_Purchase
									.setBackgroundDrawable(MyFreeBiesDetailsActivity.this
											.getResources()
											.getDrawable(
													R.drawable.gray_pocket_selector));
							// button_Purchase.setEnabled(false);
							textView_OfferStatus.setVisibility(View.VISIBLE);

							textView_OfferStatus
									.setText(Html
											.fromHtml("<p margin-top: 0px;width: 1271px;height: 6px;'><h1><b>Yay! It's yours now.</b></h1></p><p><h4>Visit  store to get it.</h4></p>"));
							/*
							 * imageView_Expired
							 * .setImageResource(R.drawable.ic_pocketed);
							 */
						} else

						if (pocketGift.equalsIgnoreCase("3")) {
							// imageView_Expired.setVisibility(View.VISIBLE);
							// img_redeem_now.setVisibility(View.GONE);

							/*
							 * imageView_Expired
							 * .setImageResource(R.drawable.ic_expired);
							 */
							textView_OfferStatus.setText("EXPIRED");
							textView_OfferStatus.setTypeface(Reguler);
							textView_OfferStatus.setTextSize(21);
							button_Purchase.setText("EXPIRED");
							// img_redeem_now.setVisibility(View.GONE);

							button_Purchase
									.setBackgroundDrawable(MyFreeBiesDetailsActivity.this
											.getResources()
											.getDrawable(
													R.drawable.gray_pocket_selector));
							// button_Purchase.setEnabled(false);
							textView_OfferStatus.setVisibility(View.VISIBLE);
						} else if (pocketGift.equalsIgnoreCase("2")) {
							button_Purchase.setText("REDEEMED");
							button_Purchase.setEnabled(false);
							textView_OfferStatus.setVisibility(View.VISIBLE);
							// img_redeem_now.setVisibility(View.GONE);
							/*
							 * imageView_Expired
							 * .setImageResource(R.drawable.ic_redeemed);
							 */
							textView_OfferStatus.setText("REDEEMED");
							button_Purchase
									.setBackgroundDrawable(MyFreeBiesDetailsActivity.this
											.getResources()
											.getDrawable(
													R.drawable.gray_pocket_selector));
						}

						StringBuffer sb = new StringBuffer();
						String bullet = getResources().getString(
								R.string.bullet);
						String time = "", day = "", appointment = "", exclusive_gender = "";
						if (jsonObjectOfferInfo.getString("time").isEmpty()) {

						} else {
							time = bullet + " "
									+ jsonObjectOfferInfo.getString("time");
						}

						if (jsonObjectOfferInfo.getString("day").isEmpty()) {

						} else {
							day = bullet + " "
									+ jsonObjectOfferInfo.getString("day");
						}

						if (jsonObjectOfferInfo.getString("appointment")
								.isEmpty()) {

						} else {
							appointment = bullet
									+ " "
									+ jsonObjectOfferInfo
											.getString("appointment");
						}

						if (jsonObjectOfferInfo.getString("exclusive_gender")
								.isEmpty()) {

						} else {
							exclusive_gender = bullet
									+ " "
									+ jsonObjectOfferInfo
											.getString("exclusive_gender");
						}
						/*
						 * sb.append(time).append("\n").append(day).append("\n")
						 * .append(appointment).append("\n")
						 * .append(exclusive_gender);
						 */

						if (time.isEmpty()) {

						} else {

							sb.append(time);

						}

						if (day.isEmpty()) {

						} else {

							sb.append("\n");
							sb.append(day);

						}

						if (appointment.isEmpty()) {

						} else {

							sb.append("\n");
							sb.append(appointment);

						}

						if (exclusive_gender.isEmpty()) {

						} else {

							sb.append("\n");
							sb.append(exclusive_gender);

						}

						/*
						 * String htmlDesc="<html><body>";
						 * 
						 * htmlDesc+="<ul style='list-style-type: circle'>";
						 * 
						 * htmlDesc+="<ol>";
						 * 
						 * htmlDesc+="hkaHKDHAKSD";
						 * 
						 * htmlDesc+="</ol>";
						 * 
						 * htmlDesc+="</ul>";
						 * 
						 * htmlDesc+="</body></html>";
						 */

						// String.valueOf(sb)
						textView_OfferDetails.setTextSize(14);
						// textView_OfferDetails.setTypeface(fontAwesome);
						textView_OfferDetails.setText("");
						// textView_OfferDetails.setText(Html.fromHtml(offer_info_html));

						boolean gps_enabled = false;
						boolean network_enabled = false;
						LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
						try {
							gps_enabled = lm
									.isProviderEnabled(LocationManager.GPS_PROVIDER);
						} catch (Exception ex) {
						}

						try {
							network_enabled = lm
									.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
						} catch (Exception ex) {
						}
						// don't start listeners if no provider is
						// enabled
						boolean hasGPS = getPackageManager().hasSystemFeature(
								PackageManager.FEATURE_LOCATION_GPS);
						WifiManager wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);

						if (!(network_enabled && wifi.isWifiEnabled())
								&& (hasGPS && !gps_enabled)) {
							textView_Distance.setText("");
							// textView_Unit.setText("");
							textView_Distance.setVisibility(View.INVISIBLE);
							// textView_Unit.setVisibility(View.INVISIBLE);
						}
					} else {
						// Constants.showOneButtonDialog(
						// MyFreeBiesDetailsActivity.this, TAG,
						// Constants.checkCode(replaced),
						// Constants.DIALOG_CLOSE);
					}

				} else {
					// Intent intent = new
					// Intent(MyFreeBiesDetailsActivity.this,
					// ConnectivityErrorActivity.class);
					// intent.putExtra(Constants.RECHARGE_FOR,
					// Constants.SUBSCRIBE_DEAL);
					// startActivity(intent);

				}
			} catch (JSONException e) {
				// e.printStackTrace();
				// Constants.showOneButtonDialog(MyFreeBiesDetailsActivity.this,
				// TAG, Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
				// Constants.DIALOG_CLOSE);
			} catch (Exception e) {
				// TODO: handle exception
				// Constants.showOneButtonDialog(MyFreeBiesDetailsActivity.this,
				// TAG, Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
				// Constants.DIALOG_CLOSE);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(MyFreeBiesDetailsActivity.this);
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(false);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							dialog.dismiss();
							DealDetailsTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			dialog.dismiss();
			DealDetailsTask.this.cancel(true);
		}

	}

	public void editActions(View v) {
		Toast.makeText(this, "Layout", Toast.LENGTH_SHORT).show();
	}

	protected void showPurchaseDialog() {
		// TODO Auto-generated method stub

		final Context mContext = MyFreeBiesDetailsActivity.this;
		final Dialog dialog = new Dialog(mContext);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.getWindow().setBackgroundDrawableResource(
				android.R.color.transparent);
		dialog.setContentView(R.layout.voucher_layout);
		dialog.setCancelable(true);
		dialog.setCanceledOnTouchOutside(false);

		Typeface Reguler = Typeface.createFromAsset(mContext.getAssets(),
				"Roboto-Regular.ttf");

		Typeface Medium = Typeface.createFromAsset(mContext.getAssets(),
				"Roboto-Regular.ttf");

		// set the custom dialog components - text, image and button
		TextView textView_Title = (TextView) dialog
				.findViewById(R.id.textView_Title);

		textView_Title.setTypeface(Reguler);

		TextView textView_Message = (TextView) dialog
				.findViewById(R.id.textView_Message);
		final CheckBox checkBoxUseWallet = (CheckBox) dialog
				.findViewById(R.id.checkBoxUseWallet);
		TextView textView_Rupee = (TextView) dialog
				.findViewById(R.id.textView_Rupee);
		TextView textView_Plus = (TextView) dialog
				.findViewById(R.id.textView_Plus);
		TextView textView_Points = (TextView) dialog
				.findViewById(R.id.textView_Points);
		TextView text_low_balance = (TextView) dialog
				.findViewById(R.id.text_low_balance);

		textView_Message.setTypeface(Medium);
		textView_Rupee.setTypeface(Medium);
		textView_Plus.setTypeface(Medium);
		textView_Points.setTypeface(Medium);
		text_low_balance.setTypeface(Medium);

		textView_Rupee.setText(offer_price + "");
		textView_Points.setText(amount + "");

		ImageView imageView_Photo_Details = (ImageView) dialog
				.findViewById(R.id.imageView_Photo_Details);
		try {
			// String url = freeBie.getFreebieLogoURL();
			Picasso.with(MyFreeBiesDetailsActivity.this).load(logo_url)
					.placeholder(R.drawable.deal_icon)
					.into(imageView_Photo_Details);
			// logo_url = freeBie.getFreebieLogoURL();
		} catch (Exception e) {
		}
		ImageView imageView_Rupee = (ImageView) dialog
				.findViewById(R.id.imageView_Rupee);
		imageView_Rupee.setImageDrawable(SVGParser.getSVGFromResource(
				mContext.getResources(), R.raw.ic_rupee)
				.createPictureDrawable());

		checkBoxUseWallet.setTypeface(Medium);
		final String bal_coin = Utility.getBalance(mContext,
				Constants.SHAREDPREFERENCE_LOYALTY_POINTS);
		final String bal_wallet = Utility.getBalance(mContext,
				Constants.SHAREDPREFERENCE_BALANCE);
		if (Double.parseDouble(bal_wallet) <= 0) {
			checkBoxUseWallet.setVisibility(View.GONE);
			checkBoxUseWallet.setChecked(false);
			payment_options = "online";
		} else {
			checkBoxUseWallet.setVisibility(View.VISIBLE);
			checkBoxUseWallet.setChecked(true);
		}

		if (amount > Double.parseDouble(bal_coin)) {
			text_low_balance.setText("You have " + bal_coin
					+ " gifts Coins. Recharge  to earn " + amount
					+ " Gifts coins\nOR\nCountinue to pay full Rs. " + price);
		} else {

		}

		Button button_Ok = (Button) dialog.findViewById(R.id.button_Ok);
		button_Ok.setTypeface(Reguler);
		button_Ok.setText("CONFIRM");
		Button button_Cancel = (Button) dialog.findViewById(R.id.button_Cancel);
		button_Cancel.setTypeface(Reguler);
		button_Cancel.setText("CANCEL");

		button_Cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
			}
		});
		// if button is clicked, close the custom dialog
		button_Ok.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();

				if (checkBoxUseWallet.isChecked()) {
					payment_options = "wallet";
				} else {
					payment_options = "online";
				}

				if (amount > Double.parseDouble(bal_coin)) {
					new PurchaseDealTask().execute(deal_ID,
							String.valueOf(offer_ID), String.valueOf("0"),
							String.valueOf(price), payment_options);
				} else {
					new PurchaseDealTask().execute(deal_ID,
							String.valueOf(offer_ID), String.valueOf(amount),
							String.valueOf(offer_price), payment_options);
				}

			}
		});
		dialog.show();
	}

	public void showTwoButtonDialog(final Context context, String Title,
			String message, String s) {
		try {

			final Dialog dialog = new Dialog(context);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.getWindow().setBackgroundDrawableResource(
					android.R.color.transparent);
			dialog.setContentView(R.layout.dialog_two_button);
			dialog.setCancelable(false);
			dialog.setCanceledOnTouchOutside(false);
			// dialog.setTitle(null);

			Typeface Reguler = Typeface.createFromAsset(context.getAssets(),
					"Roboto-Regular.ttf");

			Typeface Medium = Typeface.createFromAsset(context.getAssets(),
					"Roboto-Regular.ttf");

			// set the custom dialog components - text, image and button
			TextView textView_Title = (TextView) dialog
					.findViewById(R.id.textView_Title);
			textView_Title.setText(Title);
			textView_Title.setTypeface(Reguler);

			TextView textView_Message = (TextView) dialog
					.findViewById(R.id.textView_Message);
			textView_Message.setText(message);
			textView_Message.setTypeface(Medium);

			Button button_Ok = (Button) dialog.findViewById(R.id.button_Ok);
			button_Ok.setTypeface(Reguler);
			button_Ok.setText("CONFIRM");
			// if button is clicked, close the custom dialog
			button_Ok.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();

					new PurchaseDealTask().execute(deal_ID,
							String.valueOf(offer_ID), String.valueOf(amount),
							String.valueOf(offer_price), payment_options);

				}
			});

			Button button_Cancel = (Button) dialog
					.findViewById(R.id.button_Cancel);
			button_Cancel.setTypeface(Reguler);
			button_Cancel.setText("LATER");
			// if button is clicked, close the custom dialog
			button_Cancel.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
				}
			});

			dialog.show();

		} catch (Exception exception) {
		}
	}

	public void showTwoButtonDialog(final Context context, String Title,
			String message, final int Type) {
		try {

			final Dialog dialog = new Dialog(context);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.getWindow().setBackgroundDrawableResource(
					android.R.color.transparent);
			dialog.setContentView(R.layout.dialog_two_button);
			dialog.setCancelable(false);
			dialog.setCanceledOnTouchOutside(false);
			// dialog.setTitle(null);

			Typeface Reguler = Typeface.createFromAsset(context.getAssets(),
					"Roboto-Regular.ttf");

			Typeface Medium = Typeface.createFromAsset(context.getAssets(),
					"Roboto-Regular.ttf");

			// set the custom dialog components - text, image and button
			TextView textView_Title = (TextView) dialog
					.findViewById(R.id.textView_Title);
			textView_Title.setText(Title);
			textView_Title.setTypeface(Reguler);

			TextView textView_Message = (TextView) dialog
					.findViewById(R.id.textView_Message);
			textView_Message.setText(message);
			textView_Message.setTypeface(Medium);

			Button button_Ok = (Button) dialog.findViewById(R.id.button_Ok);
			button_Ok.setTypeface(Reguler);
			// if button is clicked, close the custom dialog
			button_Ok.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();

					Intent viewIntent = new Intent(
							Settings.ACTION_LOCATION_SOURCE_SETTINGS);
					context.startActivity(viewIntent);
				}
			});

			Button button_Cancel = (Button) dialog
					.findViewById(R.id.button_Cancel);
			button_Cancel.setTypeface(Reguler);
			// if button is clicked, close the custom dialog
			button_Cancel.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {

					dialog.dismiss();

					/*
					 * dialog.dismiss();
					 * 
					 * // don't start listeners if no provider is // enabled
					 * boolean show_route = false; boolean gps_enabled = false;
					 * boolean network_enabled = false; LocationManager lm =
					 * (LocationManager)
					 * getSystemService(Context.LOCATION_SERVICE); try {
					 * gps_enabled = lm
					 * .isProviderEnabled(LocationManager.GPS_PROVIDER); } catch
					 * (Exception ex) { } try { network_enabled = lm
					 * .isProviderEnabled(LocationManager.NETWORK_PROVIDER); }
					 * catch (Exception ex) { } WifiManager wifi = (WifiManager)
					 * getSystemService(Context.WIFI_SERVICE); if
					 * (wifi.isWifiEnabled()) show_route = true; else if
					 * (network_enabled && gps_enabled) show_route = true; else
					 * if (network_enabled) show_route = true; try {
					 * 
					 * ArrayList<WeakHashMap<String, String>> temp = new
					 * ArrayList<WeakHashMap<String, String>>();
					 * 
					 * for (int j = 0; j < data_location.size(); j++) {
					 * WeakHashMap<String, String> map1 = new
					 * WeakHashMap<String, String>(); WeakHashMap<String,
					 * String> map = new WeakHashMap<String, String>(
					 * data_location.get(j)); if
					 * (Integer.parseInt(map.get(DEAL_ID)) == Integer
					 * .parseInt(deal_ID)) { map1.put(LATITUDE,
					 * map.get(LATITUDE)); map1.put(LONGITUDE,
					 * map.get(LONGITUDE)); map1.put(ADDRESS, map.get(ADDRESS));
					 * map1.put(CITY, map.get(CITY)); map1.put(STATE,
					 * map.get(STATE));
					 * 
					 * temp.add(map1); } } if (temp.size() > 1) { Intent intent
					 * = new Intent( MyFreeBiesDetailsActivity.this,
					 * FreeBiesLocationActivity.class); intent.putExtra("ROUTE",
					 * show_route); intent.putExtra("DEAL",
					 * textView_DealName.getText() .toString().trim());
					 * intent.putExtra("ID", deal_ID);
					 * intent.putExtra("FROM_DETAILS", true);
					 * intent.putExtra("LOCATIONS", data_location);
					 * startActivity(intent); } else { Intent intent = new
					 * Intent( MyFreeBiesDetailsActivity.this,
					 * FreeBiesLocatorActivity.class); WeakHashMap<String,
					 * String> map = new WeakHashMap<String, String>(
					 * temp.get(0));
					 * 
					 * intent.putExtra("ROUTE", show_route);
					 * intent.putExtra("DEAL", textView_DealName.getText()
					 * .toString().trim()); intent.putExtra("LATITUDE",
					 * map.get(FreeBiesActivity.LATITUDE));
					 * intent.putExtra("LONGITUDE",
					 * map.get(FreeBiesActivity.LONGITUDE));
					 * intent.putExtra("ADDRESS",
					 * map.get(FreeBiesActivity.ADDRESS));
					 * intent.putExtra("CITY", map.get(FreeBiesActivity.CITY));
					 * intent.putExtra("STATE",
					 * map.get(FreeBiesActivity.STATE)); startActivity(intent);
					 * } } catch (Exception e) { Intent intent = new Intent(
					 * MyFreeBiesDetailsActivity.this,
					 * FreeBiesLocationActivity.class); intent.putExtra("ROUTE",
					 * show_route); intent.putExtra("DEAL",
					 * textView_DealName.getText() .toString().trim());
					 * intent.putExtra("ID", deal_ID);
					 * intent.putExtra("FROM_DETAILS", true);
					 * intent.putExtra("LOCATIONS", data_location);
					 * startActivity(intent); }
					 */}
			});

			button_Ok.setText("Ok");
			button_Cancel.setText("Not now");

			dialog.show();

		} catch (Exception exception) {
		}
	}

	public void showTwoButtonDialog(final Context context, String Title,
			String message) {
		try {

			final Dialog dialog = new Dialog(context);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.getWindow().setBackgroundDrawableResource(
					android.R.color.transparent);
			dialog.setContentView(R.layout.dialog_redeem);
			dialog.setCancelable(false);
			dialog.setCanceledOnTouchOutside(false);
			// dialog.setTitle(null);

			Typeface Reguler = Typeface.createFromAsset(context.getAssets(),
					"Roboto-Regular.ttf");

			Typeface Medium = Typeface.createFromAsset(context.getAssets(),
					"Roboto-Regular.ttf");

			// set the custom dialog components - text, image and button
			TextView textView_Title = (TextView) dialog
					.findViewById(R.id.textView_Title);
			textView_Title.setText(Title);
			textView_Title.setTypeface(Reguler);

			TextView textView_Message = (TextView) dialog
					.findViewById(R.id.textView_Message);
			textView_Message.setText("");
			textView_Message.setTypeface(Medium);

			final EditText editText_Password = (EditText) dialog
					.findViewById(R.id.editText_Password);
			editText_Password.setTypeface(Medium);

			Button button_Ok = (Button) dialog.findViewById(R.id.button_Ok);
			button_Ok.setTypeface(Reguler);
			// if button is clicked, close the custom dialog
			button_Ok.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
					String[] vCode = textView_Voucher.getText().toString()
							.split(":");
					new RedeemGiftTask().execute(vCode[1].trim(), Utility
							.getMobileNumber(MyFreeBiesDetailsActivity.this,
									Constants.SHAREDPREFERENCE_MOBILE),
							deal_ID, editText_Password.getText().toString()
									.trim());
				}
			});

			Button button_Cancel = (Button) dialog
					.findViewById(R.id.button_Cancel);
			button_Cancel.setTypeface(Reguler);
			// if button is clicked, close the custom dialog
			button_Cancel.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
				}
			});

			dialog.show();

		} catch (Exception exception) {
		}
	}

	public class RedeemGiftTask extends AsyncTask<String, String, String>
			implements OnDismissListener {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			try {
				String response = RequestClass.getInstance()
						.readPay1B2CRequest(
								MyFreeBiesDetailsActivity.this,
								Constants.B2C_URL
										+ "Redeem_FreebieByApp/?coupon="
										+ params[0] + "&mobile=" + params[1]
										+ "&dealid=" + params[2] + "&password="
										+ params[3]);

				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				if (this.dialog.isShowing()) {
					this.dialog.dismiss();
				}
			} catch (Exception e) {
			}
			try {

				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONObject jsonObject = new JSONObject(replaced);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {

						// JSONArray jsonArray_Desc = jsonObject
						// .getJSONArray("description");
						button_Purchase.setText("REDEEMED");
						textView_OfferStatus.setVisibility(View.VISIBLE);
						// img_redeem_now.setVisibility(View.GONE);
						/*
						 * imageView_Expired
						 * .setImageResource(R.drawable.ic_redeemed);
						 */
						textView_OfferStatus.setText("REDEEMED");
						button_Purchase.setEnabled(false);
						Utility.setRefreshRequired(
								MyFreeBiesDetailsActivity.this,
								Constants.SHAREDPREFERENCE_REFESH_MY_GIFT, true);
						Utility.setBackRequired(MyFreeBiesDetailsActivity.this,
								Constants.SHAREDPREFERENCE_BACK_LOCATION, true);
						Constants.showOneButtonDialog(
								MyFreeBiesDetailsActivity.this, TAG,
								Constants.checkCode(replaced),
								Constants.DIALOG_CLOSE);

					} else {
						Constants.showOneButtonDialog(
								MyFreeBiesDetailsActivity.this, TAG,
								Constants.checkCode(replaced),
								Constants.DIALOG_CLOSE);
					}

				} else {
					Intent intent = new Intent(MyFreeBiesDetailsActivity.this,
							ConnectivityErrorActivity.class);
					startActivity(intent);
				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonDialog(MyFreeBiesDetailsActivity.this,
						TAG, Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonDialog(MyFreeBiesDetailsActivity.this,
						TAG, Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(MyFreeBiesDetailsActivity.this);
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							dialog.dismiss();
							RedeemGiftTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			dialog.dismiss();
			RedeemGiftTask.this.cancel(true);
		}
	}

	public class LikeTask extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... params) {
			try {

				String response = RequestClass.getInstance()
						.readPay1B2CRequest(MyFreeBiesDetailsActivity.this,
								Constants.B2C_URL + "MyLikes/?id=" + params[0]);

				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {

				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONObject jsonObject = new JSONObject(replaced);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {

						JSONObject jsonObject2 = jsonObject
								.getJSONObject("description");
						FreeBiesDataSource freeBiesDataSource = new FreeBiesDataSource(
								MyFreeBiesDetailsActivity.this);
						try {

							freeBiesDataSource.open();
							freeBiesDataSource.updateFreeBies(Integer
									.parseInt(jsonObject2.getString("status")),
									String.valueOf(offer_ID));
						} catch (SQLException exception) {
						} catch (Exception e) {

						} finally {
							freeBiesDataSource.close();
						}
					}
				}
			} catch (JSONException e) {
			} catch (Exception e) {
			}
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}
	}

	public class RatingTask extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... params) {
			try {

				String response = RequestClass.getInstance()
						.readPay1B2CRequest(
								MyFreeBiesDetailsActivity.this,
								Constants.B2C_URL + "SetOfferReview/?id="
										+ params[0] + "&review=" + params[1]);

				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {

				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONObject jsonObject = new JSONObject(replaced);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {

						JSONObject jsonObject2 = jsonObject
								.getJSONObject("description");
						FreeBiesDataSource freeBiesDataSource = new FreeBiesDataSource(
								MyFreeBiesDetailsActivity.this);
						try {

							freeBiesDataSource.open();
							freeBiesDataSource.updateFreeBies(Integer
									.parseInt(jsonObject2.getString("status")),
									String.valueOf(offer_ID));
						} catch (SQLException exception) {
						} catch (Exception e) {

						} finally {
							freeBiesDataSource.close();
						}
					}
				}
			} catch (JSONException e) {
			} catch (Exception e) {
			}
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}
	}

	public class PurchaseDealTask extends AsyncTask<String, String, String>
			implements OnDismissListener {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {

			String response = RequestClass
					.getInstance()
					.readPay1B2CRequest(
							MyFreeBiesDetailsActivity.this,
							Constants.B2C_URL
									+ "purchase_deal/?deal_id="
									+ params[0]
									+ "&offer_id="
									+ params[1]
									+ "&coupon_id=&paymentopt="
									+ params[4]
									+ "&amount="
									+ params[2]
									+ "&of_price="
									+ params[3]
									+ "&quantity=1&trans_category=deal&freebie=true&partial=1&ref_id=&api_version="
									+ Constants.API_VERSION);

			return response;
		}

		@Override
		protected void onCancelled() {
			// TODO Auto-generated method stub
			super.onCancelled();
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			try {

				if (!result.startsWith("Error")) {

					JSONObject jsonObject = new JSONObject(result);

					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {

						JSONObject description = jsonObject
								.getJSONObject("description");
						if (description.getString("online_form")
								.equalsIgnoreCase("true")) {

							Intent intent = new Intent(
									MyFreeBiesDetailsActivity.this,
									PGActivity.class);
							intent.putExtra("OPERATOR_NAME", "");
							intent.putExtra("online_form", true);
							intent.putExtra("TAX", "");
							intent.putExtra("MOBILE_NUMBER", "");
							intent.putExtra("RECHARGE_AMOUNT", "0");
							intent.putExtra("RECHARGE_FOR",
									Constants.SUBSCRIBE_DEAL);
							intent.putExtra("TRANS_ID",
									description.getString("txnid"));
							intent.putExtra("CONTENT",
									description.getString("form_content")
											.replaceAll("\\\\", ""));

							// startActivity(intent);
							startActivityForResult(intent, 2);

						} else {

							Constants.showOneButtonDialog(
									MyFreeBiesDetailsActivity.this,
									"Congratulations!",
									"This is yours now. Visit store & get it.",
									Constants.DIALOG_CLOSE_FORGOTPIN);

							Utility.setBalance(MyFreeBiesDetailsActivity.this,
									Constants.SHAREDPREFERENCE_LOYALTY_POINTS,
									description.getString("loyalty_balance"));
							Utility.setBalance(MyFreeBiesDetailsActivity.this,
									Constants.SHAREDPREFERENCE_BALANCE,
									description.getString("closing_balance"));
							String str = Utility.getMobileNumber(
									MyFreeBiesDetailsActivity.this,
									Constants.SHAREDPREFERENCE_MOBILE);
							try {
								String temp = str.substring(0, 6);
								textView_Mobile.setText("Mobile number: "
										+ str.replaceAll(temp, "XXXXXX"));
							} catch (Exception e) {
								textView_Mobile
										.setText("Mobile number: " + str);
							}
							textView_Voucher
									.setText("Voucher code: "
											+ description
													.getString("deal_coupon_code"));
							try {
								textView_ExpiryDate.setText("Expiry date: "
										+ description.getString("expiry"));
							} catch (Exception e) {
							}

							textView_Mobile.setVisibility(View.VISIBLE);
							textView_Voucher.setVisibility(View.VISIBLE);
							textView_ExpiryDate.setVisibility(View.VISIBLE);
							view_sep.setVisibility(View.VISIBLE);
							/*button_Purchase.setText("REDEEM NOW");
							button_Purchase
									.setBackgroundDrawable(getResources()
											.getDrawable(
													R.drawable.gray_pocket_selector));*/

							Utility.setRefreshRequired(
									MyFreeBiesDetailsActivity.this,
									Constants.SHAREDPREFERENCE_REFESH_MY_GIFT,
									true);

							button_Purchase.setText("REDEEM NOW");
							// img_redeem_now.setVisibility(View.GONE);
							button_Purchase
									.setBackgroundDrawable(getResources()
											.getDrawable(
													R.drawable.gray_pocket_selector));

							/*
							 * imageView_Expired.setVisibility(View.VISIBLE);
							 * imageView_Expired
							 * .setImageResource(R.drawable.ic_pocketed);
							 */

							textView_OfferStatus.setVisibility(View.VISIBLE);
							textView_OfferStatus
									.setText(Html
											.fromHtml("<p margin-top: 0px;width: 1271px;height: 6px;'><h1><b>Yay! It's yours now.</b></h1></p><p><h4>Visit  store to get it.</h4></p>"));
							// img_redeem_now.setVisibility(View.GONE);
						}

					} else {
						Constants.showOneButtonDialog(
								MyFreeBiesDetailsActivity.this, "Gift",
								Constants.checkCode(result),
								Constants.DIALOG_CLOSE);
					}
				} else {
					Intent intent = new Intent(MyFreeBiesDetailsActivity.this,
							ConnectivityErrorActivity.class);
					startActivity(intent);
				}
			} catch (Exception e) {
				Constants.showOneButtonDialog(MyFreeBiesDetailsActivity.this,
						"Gift", Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(MyFreeBiesDetailsActivity.this);
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(false);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							PurchaseDealTask.this.cancel(true);
							dialog.cancel();
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			PurchaseDealTask.this.cancel(true);
			dialog.cancel();
		}

	}

	public void showDailogForFreeBie() {
		final Dialog dialog = new Dialog(MyFreeBiesDetailsActivity.this);

		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

		dialog.setCancelable(false);
		dialog.setCanceledOnTouchOutside(false);
		dialog.getWindow().setBackgroundDrawableResource(
				android.R.color.transparent);
		dialog.setContentView(R.layout.dialog_freebie_info_layout);

		TextView text = (TextView) dialog.findViewById(R.id.textViewInfo);

		String msg = "1. Please read the offer carefully"
				+ "\n2. Please check the T& C before using the offer"
				+ "\n3. Once you pocket the gift you need to redeem gift"
				+ "\n4. When you redeem the gift ask the merchant to add password";

		text.setText(msg);

		Button dialogButton = (Button) dialog
				.findViewById(R.id.btnDialogContinue);
		// if button is clicked, close the custom dialog
		dialogButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				dialog.dismiss();
			}
		});

		dialog.show();
	}

	public void showDialogForCallList(
			final ArrayList<HashMap<String, String>> numberWithLocationList) {
		try {
			final Dialog dialog = new Dialog(MyFreeBiesDetailsActivity.this);

			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

			dialog.setCancelable(true);
			dialog.setCanceledOnTouchOutside(true);
			dialog.getWindow().setBackgroundDrawableResource(
					android.R.color.transparent);
			dialog.setContentView(R.layout.dialog_call_list);
			ImageView imageView_Close = (ImageView) dialog
					.findViewById(R.id.imageView_Close);
			TextView textView_Title = (TextView) dialog
					.findViewById(R.id.textView_Title);
			textView_Title.setText(this.textView_Title.getText().toString());
			textView_Title.setTypeface(Reguler);
			imageView_Close.setImageDrawable(SVGParser.getSVGFromResource(
					MyFreeBiesDetailsActivity.this.getResources(),
					R.raw.ic_close).createPictureDrawable());
			imageView_Close.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					dialog.dismiss();
				}
			});
			ListView callListView = (ListView) dialog
					.findViewById(R.id.listViewCall);
			CallListAdapter adapter = new CallListAdapter(
					MyFreeBiesDetailsActivity.this, numberWithLocationList);

			callListView.setAdapter(adapter);

			callListView.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {
					// TODO Auto-generated method stub
					dialog.dismiss();
					String number = numberWithLocationList.get(arg2).get(
							"number");
					String[] numberArray = number.split(",");
					String firstNumber = numberArray[0].trim();
					if (!firstNumber.isEmpty()) {
						Intent callIntent = new Intent(Intent.ACTION_CALL);
						callIntent.setData(Uri.parse("tel:" + firstNumber));
						startActivity(callIntent);
					} else {
						Toast.makeText(MyFreeBiesDetailsActivity.this,
								"No number available", Toast.LENGTH_LONG)
								.show();
					}
				}
			});

			dialog.show();
		} catch (Exception exc) {
			Log.d("msg", "Msg");
		}
	}

	public void showDailogForCoinsBalance(String balance, String requiredBalance) {
		try {
			final Dialog dialog = new Dialog(MyFreeBiesDetailsActivity.this);

			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

			dialog.setCancelable(false);
			dialog.setCanceledOnTouchOutside(false);
			dialog.getWindow().setBackgroundDrawableResource(
					android.R.color.transparent);

			Typeface Reguler = Typeface.createFromAsset(getAssets(),
					"Roboto-Regular.ttf");
			Typeface Medium = Typeface.createFromAsset(getAssets(),
					"Roboto-Regular.ttf");

			dialog.setContentView(R.layout.dialog_low_coins);

			TextView textView_Message4 = (TextView) dialog
					.findViewById(R.id.textView_Message4);

			TextView textView_Title = (TextView) dialog
					.findViewById(R.id.textView_Title);

			TextView textView_Message1 = (TextView) dialog
					.findViewById(R.id.textView_Message1);
			TextView textView_Message2 = (TextView) dialog
					.findViewById(R.id.textView_Message2);

			ImageView imgNoCoins = (ImageView) dialog
					.findViewById(R.id.imgNoCoins);

			/*
			 * imgNoCoins.setImageDrawable(SVGParser.getSVGFromResource(
			 * MyFreeBiesDetailsActivity.this.getResources(),
			 * R.raw.ic_not_enough_balance).createPictureDrawable());
			 */

			textView_Title.setText("Oops you need more "
					+ (Integer.parseInt(requiredBalance) - (Integer
							.parseInt(balance))) + " Gift Coins.");
			textView_Message1.setTypeface(Reguler);
			textView_Message2.setTypeface(Reguler);
			textView_Message4.setTypeface(Reguler);
			textView_Title.setTypeface(Reguler);
			// textViewInfo.setTypeface();
			/*
			 * String message1 =
			 * "<font color=#1651ff>Redeeming this gift requires </font> <font color=#E63638 >"
			 * + requiredBalance +
			 * "</font><font color=#1651ff> Gift Coins.</font>"; String message2
			 * = "<font color=#1651ff>You have </font> <font color=#E63638 >" +
			 * balance + "</font><font color=#1651ff> Gift Coins.</font>";
			 */

			String message1 = "You have "
					+ balance
					+ " Gift Coins.\nRecharge now to earn more\ngift coins and get this gift.";
			textView_Message1.setText(Html.fromHtml(message1));
			// textView_Message2.setText(Html.fromHtml(message2));
			Button btnDialogContinue = (Button) dialog
					.findViewById(R.id.btnDialogContinue);
			btnDialogContinue.setTypeface(Reguler);

			btnDialogContinue.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					// Utility.saveshowDialog(MyFreeBiesDetailsActivity.this,
					// true);
					dialog.dismiss();
					/*
					 * Intent i = new Intent(MyFreeBiesDetailsActivity.this,
					 * MainActivity.class); i.putExtra(Constants.RECHARGE_FOR,
					 * Constants.RECHARGE_MOBILE); startActivity(i); finish();
					 */

					Intent intent = new Intent(MyFreeBiesDetailsActivity.this,
							MainActivity.class);
					intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
							| Intent.FLAG_ACTIVITY_NEW_TASK);
					intent.putExtra("FROM_QUICKPAY", true);
					intent.putExtra("IS_GIFT", false);
					MyFreeBiesDetailsActivity.this.startActivity(intent);
					MyFreeBiesDetailsActivity.this.finish();

				}
			});

			Button button_cancel = (Button) dialog
					.findViewById(R.id.button_cancel);
			button_cancel.setTypeface(Reguler);
			button_cancel.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					// Utility.saveshowDialog(MyFreeBiesDetailsActivity.this,
					// true);
					dialog.dismiss();
				}
			});

			dialog.show();
		} catch (Exception exc) {
			Log.d("msg", "Msg");
		}
	}

	private ImageView mOverLayImage;

	public void ShowcaseImage() {
		final Dialog overlayInfo = new Dialog(MyFreeBiesDetailsActivity.this);

		overlayInfo.requestWindowFeature(Window.FEATURE_NO_TITLE);
		overlayInfo.getWindow().setBackgroundDrawable(
				new ColorDrawable(Color.TRANSPARENT));
		overlayInfo.getWindow().clearFlags(
				WindowManager.LayoutParams.FLAG_DIM_BEHIND);
		overlayInfo.setContentView(R.layout.overlay_view);
		overlayInfo.show();

		/*
		 * mOverLayImage = (ImageView) overlayInfo
		 * .findViewById(R.id.over_lay_image);
		 * mOverLayImage.setOnClickListener(new OnClickListener() {
		 * 
		 * @Override public void onClick(View v) { overlayInfo.cancel(); } });
		 */
	}

	private int getRelativeLeft(View myView) {
		if (myView.getParent() == myView.getRootView())
			return myView.getLeft();
		else
			return myView.getLeft()
					+ getRelativeLeft((View) myView.getParent());
	}

	private int getRelativeTop(View myView) {
		if (myView.getParent() == myView.getRootView())
			return myView.getTop();
		else
			return myView.getTop() + getRelativeTop((View) myView.getParent());
	}

	private void showRatingDialog() {

		RadioGroup radioOptions;
		RadioButton radioButton;

		final Dialog dialog = new Dialog(MyFreeBiesDetailsActivity.this);

		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

		dialog.setCancelable(true);
		dialog.setCanceledOnTouchOutside(true);
		dialog.getWindow().setBackgroundDrawableResource(
				android.R.color.transparent);

		Typeface Reguler = Typeface.createFromAsset(getAssets(),
				"Roboto-Regular.ttf");
		Typeface Medium = Typeface.createFromAsset(getAssets(),
				"Roboto-Regular.ttf");

		dialog.setContentView(R.layout.dialog_freebie_rating);

		TextView textView_Title = (TextView) dialog
				.findViewById(R.id.textView_Title);
		textView_Title.setTypeface(Reguler);
		radioOptions = (RadioGroup) dialog.findViewById(R.id.radioOptions);
		ImageView imageViewClose = (ImageView) dialog
				.findViewById(R.id.imageViewClose);
		imageViewClose.setImageDrawable(SVGParser.getSVGFromResource(
				getResources(), R.raw.ic_close).createPictureDrawable());

		try {

			RadioButton radio1 = (RadioButton) dialog.findViewById(R.id.radio1);
			RadioButton radio2 = (RadioButton) dialog.findViewById(R.id.radio2);
			RadioButton radio3 = (RadioButton) dialog.findViewById(R.id.radio3);
			RadioButton radio4 = (RadioButton) dialog.findViewById(R.id.radio4);
			RadioButton radio5 = (RadioButton) dialog.findViewById(R.id.radio5);

			radio1.setTypeface(Reguler);
			radio2.setTypeface(Reguler);
			radio3.setTypeface(Reguler);

			radio4.setTypeface(Reguler);
			radio5.setTypeface(Reguler);

			switch (review) {
			case 1:
				radio1.setChecked(true);
				break;
			case 2:
				radio2.setChecked(true);
				break;
			case 3:
				radio3.setChecked(true);
				break;
			case 4:
				radio4.setChecked(true);
				break;
			case 5:
				radio5.setChecked(true);
				break;
			default:
				radio1.setChecked(true);
				break;
			}
		} catch (Exception e) {
		}

		imageViewClose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
			}
		});
		dialog.show();
		radioOptions.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				switch (checkedId) {

				case R.id.radio1:
					// TODO Something
					new RatingTask().execute(String.valueOf(offer_ID), "1");
					review = 1;
					break;
				case R.id.radio2:
					// TODO Something
					new RatingTask().execute(String.valueOf(offer_ID), "2");
					review = 2;
					break;
				case R.id.radio3:
					// TODO Something
					new RatingTask().execute(String.valueOf(offer_ID), "3");
					review = 3;
					break;
				case R.id.radio4:
					// TODO Something
					new RatingTask().execute(String.valueOf(offer_ID), "4");
					review = 4;
					break;
				case R.id.radio5:
					// TODO Something
					new RatingTask().execute(String.valueOf(offer_ID), "5");
					review = 5;
					break;
				}
				/*
				 * try { Thread.sleep(200); dialog.dismiss(); } catch
				 * (InterruptedException e) { // TODO Auto-generated catch block
				 * e.printStackTrace(); }catch (Exception e) {
				 * 
				 * // TODO: handle exception e.printStackTrace(); }
				 */

				Handler myHandler = new Handler();
				myHandler.postDelayed(new Runnable() {
					@Override
					public void run() {
						// Do something after 100ms
						dialog.dismiss();
					}
				}, 2000);

			}
		});
	}

	private Runnable mMyRunnable = new Runnable() {
		@Override
		public void run() {
			// Change state here
		}
	};

}
