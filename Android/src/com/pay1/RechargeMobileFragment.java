package com.pay1;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import com.pay1.customviews.MyProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.SQLException;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.pay1.adapterhandler.PlanAdapterMobile;
import com.pay1.constants.Constants;
import com.pay1.databasehandler.Operator;
import com.pay1.databasehandler.OperatorDataSource;
import com.pay1.databasehandler.Plan;
import com.pay1.databasehandler.PlanDataSource;
import com.pay1.requesthandler.RequestClass;
import com.pay1.utilities.EditTextValidator;
import com.pay1.utilities.Utility;

public class RechargeMobileFragment extends Fragment implements OnClickListener {

	int flag = 1;
	Button btnPlans, btnContacts;// , mBtnRchCancel, mBtnRchNow;
	// ImageView btnContacts;
	ImageView imageView_Ok, imageView_Cancel, imageView_dropdown;
	LinearLayout radioLayout;
	ToggleButton toggleSTV;
	EditText editRechargeMobile, editRechargeAmount;
	/*
	 * RadioGroup radioRchTypeGroup; RadioButton radioTopUp, radioSTV;
	 */
	static int PICK_CONTACT = 100;
	static int PICK_OPERATOR = 111;
	TextView textView_Details, textView_Title, textView_Operator, textViewMob,
			textViewAmount;

	String circle_code = null, operatorCode = null, operatorID = null,
			operatorName = null, operatorProductID = null;
	ProgressBar progressBar1;

	Bundle bundle;
	String isStv = "0";
	PlanAdapterMobile plansAdapter;
	ListView listPlan;

	private OperatorDataSource operatorDataSource;
	private PlanDataSource planDataSource;

	// private ScrollView scrollView;

	public static RechargeMobileFragment newInstance() {
		RechargeMobileFragment fragment = new RechargeMobileFragment();
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View vi = inflater.inflate(R.layout.recharge_mobile_fragment,
				container, false);

		try {

			flag = 1;
			findViewById(vi);

			// scrollView = (ScrollView) vi.findViewById(R.id.scrollView);

			operatorDataSource = new OperatorDataSource(getActivity());

			try {
				operatorDataSource.open();

				List<Operator> operator = operatorDataSource.getAllOperator();
				if (operator.size() == 0)
					new GetOperatorTask().execute();

			} catch (SQLException exception) {
				// TODO: handle exception
				// // Log.e("Er ", exception.getMessage());
			} catch (Exception e) {
				// TODO: handle exception
				// // Log.e("Er ", e.getMessage());
			} finally {
				operatorDataSource.close();
			}

			bundle = getActivity().getIntent().getExtras();
			flag = 1;

			imageView_Ok.setOnClickListener(this);
			imageView_Cancel.setOnClickListener(this);
			btnContacts.setOnClickListener(this);
			btnPlans.setOnClickListener(this);
			textView_Operator.setOnClickListener(this);
			imageView_dropdown.setOnClickListener(this);

			Typeface Normal = Typeface.createFromAsset(getActivity()
					.getAssets(), "EncodeSansNormal-100-Thin.ttf");
			Typeface Reguler = Typeface.createFromAsset(getActivity()
					.getAssets(), "EncodeSansNormal-400-Regular.ttf");
			Typeface Medium = Typeface.createFromAsset(getActivity()
					.getAssets(), "EncodeSansNarrow-500-Medium.ttf");

			textView_Details.setTypeface(Normal);
			textView_Title.setTypeface(Reguler);
			textViewAmount.setTypeface(Medium);
			textViewMob.setTypeface(Medium);
			btnPlans.setTypeface(Medium);
			editRechargeMobile.setTypeface(Normal);
			editRechargeAmount.setTypeface(Normal);
			textView_Operator.setTypeface(Normal);
			planDataSource = new PlanDataSource(getActivity());

			toggleSTV.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (toggleSTV.isChecked()) {
						isStv = "1";
					} else {
						isStv = "0";
					}
				}
			});
			editRechargeMobile.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					// TODO Auto-generated method stub
					// EditTextValidator.hasText(getActivity(),
					// editRechargeMobile, Constants.ERROR_MOBILE_BLANK_FIELD);
					if (editRechargeMobile.length() == 10) {
						new GetPlansCircletask().execute();
					}
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					// TODO Auto-generated method stub

				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

				}
			});

			editRechargeAmount.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					// TODO Auto-generated method stub
					// EditTextValidator.hasText(getActivity(),
					// editRechargeAmount, Constants.ERROR_AMOUNT_BLANK_FIELD);
					if (editRechargeMobile.getText().toString().trim().length() == 10
							&& editRechargeAmount.getText().toString().trim()
									.length() >= 2) {
						if (circle_code != null) {
							progressBar1.setVisibility(View.VISIBLE);
							// Toast.makeText(getActivity(),
							// pay1OpId,
							// Toast.LENGTH_SHORT).show();
							getPlan(operatorProductID,
									circle_code,
									Integer.parseInt(editRechargeAmount
											.getText().toString().trim()));
						}
					}

				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					// TODO Auto-generated method stub

				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

				}
			});

			if (bundle.getInt(Constants.RECHARGE_FOR) == Constants.RECHARGE_MOBILE) {
				textView_Title.setText("Mobile");
			} else if (bundle.getInt(Constants.RECHARGE_FOR) == Constants.RECHARGE_DATA) {
				textView_Title.setText("Data Card");
			}
		} catch (Exception e) {
		}
		InputMethodManager imm = (InputMethodManager) getActivity()
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(container.getWindowToken(), 0);
		return vi;
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
	}

	protected void checkForSTV(int pay1OpId2) {

		if (pay1OpId2 == 1) {
			radioLayout.setVisibility(View.VISIBLE);
		} else {
			radioLayout.setVisibility(View.GONE);
		}
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	private void findViewById(View vi) {
		// TODO Auto-generated method stub
		btnPlans = (Button) vi.findViewById(R.id.btnPlans);
		btnPlans.setVisibility(View.GONE);
		textView_Details = (TextView) vi.findViewById(R.id.textView_Details);
		textViewMob = (TextView) vi.findViewById(R.id.textViewMob);
		textViewAmount = (TextView) vi.findViewById(R.id.textViewAmount);
		textView_Title = (TextView) vi.findViewById(R.id.textView_Title);
		radioLayout = (LinearLayout) vi.findViewById(R.id.radioLayout);
		/*
		 * radioRchTypeGroup = (RadioGroup)
		 * findViewById(R.id.radioRechargeType); radioSTV = (RadioButton)
		 * findViewById(R.id.radioSTV); radioTopUp = (RadioButton)
		 * findViewById(R.id.radioTopup);
		 */
		editRechargeMobile = (EditText) vi
				.findViewById(R.id.editRechargeMobile);
		InputMethodManager imm = (InputMethodManager) getActivity()
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(editRechargeMobile.getWindowToken(), 0);
		btnContacts = (Button) vi.findViewById(R.id.btnContacts);
		textView_Operator = (TextView) vi.findViewById(R.id.textView_Operator);
		editRechargeAmount = (EditText) vi
				.findViewById(R.id.editRechargeAmount);
		imageView_Ok = (ImageView) vi.findViewById(R.id.imageView_Ok);
		toggleSTV = (ToggleButton) vi.findViewById(R.id.toggleSTV);

		imageView_Cancel = (ImageView) vi.findViewById(R.id.imageView_Cancel);
		imageView_dropdown = (ImageView) vi
				.findViewById(R.id.imageView_dropdown);
		listPlan = (ListView) vi.findViewById(R.id.listPlan);

		progressBar1 = (ProgressBar) vi.findViewById(R.id.progressBar1);
		progressBar1.setVisibility(View.GONE);
	}

	private boolean checkValidation(Context context) {
		boolean ret = true;

		if (!EditTextValidator.hasText(context, editRechargeMobile,
				Constants.ERROR_MOBILE_BLANK_FIELD)) {
			ret = false;
			editRechargeMobile.requestFocus();
			return ret;
		} else if (!EditTextValidator.isValidMobileNumber(context,
				editRechargeMobile, Constants.ERROR_MOBILE_LENGTH_FIELD)) {
			ret = false;
			editRechargeMobile.requestFocus();
			return ret;
		} else if (!EditTextValidator.hasText(context, textView_Operator,
				Constants.ERROR_OPERATOR_BLANK_FIELD)) {
			ret = false;
			textView_Operator.requestFocus();
			return ret;
		} else if (!EditTextValidator.hasText(context, editRechargeAmount,
				Constants.ERROR_AMOUNT_BLANK_FIELD)) {
			ret = false;
			editRechargeAmount.requestFocus();
			return ret;
		} else if (!EditTextValidator.isValidAmount(context,
				editRechargeAmount, Constants.ERROR_AMOUNT_VALID_FIELD)) {
			ret = false;
			editRechargeAmount.requestFocus();
			return ret;
		} else
			return ret;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		try {
			if (resultCode == Activity.RESULT_OK) {
				if (requestCode == Constants.PLAN_REQUEST) {
					editRechargeAmount.setText(data.getExtras().getString(
							"result"));
				}
				if (requestCode == PICK_OPERATOR) {
					textView_Operator.setText(data.getExtras().getString(
							"OperatorName"));
					btnPlans.setVisibility(View.VISIBLE);
					operatorCode = data.getExtras().getString("OperatorCode");
					operatorID = data.getExtras().getString("OperatorID");
					operatorProductID = data.getExtras().getString(
							"OperatorProductID");
					int d = Integer.parseInt(data.getExtras().getString("STV"));
					checkForSTV(d);
				}
				if (requestCode == PICK_CONTACT) {

					Uri contactData = data.getData();
					Cursor contactCursor = getActivity()
							.getContentResolver()
							.query(contactData,
									new String[] { ContactsContract.Contacts._ID },
									null, null, null);
					String id = null;
					if (contactCursor.moveToFirst()) {
						id = contactCursor.getString(contactCursor
								.getColumnIndex(ContactsContract.Contacts._ID));
					}
					contactCursor.close();
					String phoneNumber = null, displayName = null;
					Cursor phoneCursor = getActivity()
							.getContentResolver()
							.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
									new String[] {
											ContactsContract.CommonDataKinds.Phone.NUMBER,
											ContactsContract.PhoneLookup.DISPLAY_NAME },
									ContactsContract.CommonDataKinds.Phone.CONTACT_ID
											+ "= ? ", new String[] { id }, null);
					if (phoneCursor.moveToFirst()) {
						phoneNumber = phoneCursor
								.getString(phoneCursor
										.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
						displayName = phoneCursor
								.getString(phoneCursor
										.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
						phoneNumber = Normalizer.normalize(phoneNumber,
								Normalizer.Form.NFD).replaceAll("[^a-zA-Z0-9]",
								"");
						if (phoneNumber.length() >= 10) {
							phoneNumber = phoneNumber.substring(
									phoneNumber.length() - 10,
									phoneNumber.length());
							editRechargeMobile.setText(phoneNumber);
							btnContacts.setText(displayName);
						} else {
							Toast.makeText(getActivity(),
									"Not a valid mobile number.",
									Toast.LENGTH_LONG).show();
							phoneNumber = "";
							displayName = "";
							editRechargeMobile.setText("");
							btnContacts.setText("");
						}

					} else {
						Toast.makeText(getActivity(),
								"Not a valid mobile number.", Toast.LENGTH_LONG)
								.show();
						phoneNumber = "";
						displayName = "";
						editRechargeMobile.setText("");
						btnContacts.setText("");
					}
					phoneCursor.close();
				}

			}
		} catch (Exception e) {
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.textView_Operator: {
			Intent intent = new Intent(getActivity(), OperatorActivity.class);
			if (bundle.getInt(Constants.RECHARGE_FOR) == Constants.RECHARGE_MOBILE) {
				intent.putExtra(Constants.RECHARGE_FOR,
						Constants.RECHARGE_MOBILE);
			} else if (bundle.getInt(Constants.RECHARGE_FOR) == Constants.RECHARGE_DATA) {
				intent.putExtra(Constants.RECHARGE_FOR, Constants.RECHARGE_DATA);
			}
			startActivityForResult(intent, PICK_OPERATOR);
		}
			break;
		case R.id.imageView_dropdown: {
			Intent intent = new Intent(getActivity(), OperatorActivity.class);
			if (bundle.getInt(Constants.RECHARGE_FOR) == Constants.RECHARGE_MOBILE) {
				intent.putExtra(Constants.RECHARGE_FOR,
						Constants.RECHARGE_MOBILE);
			} else if (bundle.getInt(Constants.RECHARGE_FOR) == Constants.RECHARGE_DATA) {
				intent.putExtra(Constants.RECHARGE_FOR, Constants.RECHARGE_DATA);
			}
			startActivityForResult(intent, PICK_OPERATOR);
		}
			break;
		case R.id.btnContacts: {
			Intent intent = new Intent(Intent.ACTION_PICK,
					ContactsContract.Contacts.CONTENT_URI);
			startActivityForResult(intent, PICK_CONTACT);
		}
			break;
		case R.id.imageView_Cancel:
			editRechargeAmount.setText("");
			editRechargeMobile.setText("");
			editRechargeMobile.requestFocus();
			textView_Operator.setText("");
			btnPlans.setVisibility(View.GONE);
			break;
		case R.id.imageView_Ok:

			if (Utility.getLoginFlag(getActivity(),
					Constants.SHAREDPREFERENCE_IS_LOGIN)) {

				if (!checkValidation(getActivity())) {

				} else {
					InputMethodManager imm = (InputMethodManager) getActivity()
							.getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

					// double rechargeAmount, balance;
					// try {
					// rechargeAmount = Double.parseDouble(editRechargeAmount
					// .getText().toString().trim());
					// balance = Double.parseDouble(Utility.getBalance(
					// getActivity(),
					// Constants.SHAREDPREFERENCE_BALANCE));
					// } catch (Exception e) {
					// rechargeAmount = 0;
					// balance = 0;
					// }
					// if (rechargeAmount > balance) {
					// Intent orderIntent = new Intent(getActivity(),
					// WalletTopupActivity.class);
					// orderIntent.putExtra("AMOUNT", rechargeAmount);
					// startActivity(orderIntent);
					//
					// } else {
					/*
					 * int selectedId = radioRchTypeGroup
					 * .getCheckedRadioButtonId(); radioSTV = (RadioButton)
					 * findViewById(selectedId);
					 */
					Intent orderIntent = new Intent(getActivity(),
							OrderSummaryActivity.class);
					orderIntent.putExtra(Constants.OPERATOR_NAME,
							textView_Operator.getText().toString());
					orderIntent.putExtra(Constants.MOBILE_NUMBER,
							editRechargeMobile.getText().toString().trim());
					orderIntent.putExtra(Constants.RECHARGE_AMOUNT,
							editRechargeAmount.getText().toString().trim());
					orderIntent.putExtra(Constants.CONTACT_NAME, btnContacts
							.getText().toString().trim());
					orderIntent.putExtra(Constants.OPERATOR_ID, operatorID);
					orderIntent.putExtra(Constants.STV_RECHARGE, isStv);
					orderIntent.putExtra(Constants.RECHARGE_FOR,
							bundle.getInt(Constants.RECHARGE_FOR));
					startActivity(orderIntent);
					// }
				}
			} else {
				Constants.showOneButtonDialog(getActivity(), "Recharge ",
						"Please Login First.", Constants.DIALOG_CLOSE_LOGIN);
			}
			break;
		case R.id.btnPlans:
			Intent plansIntent = new Intent(getActivity(),
					PlanFragmentActivity.class);
			if (editRechargeMobile.getText().toString().trim().length() != 0) {
				plansIntent.putExtra(Constants.PLAN_JSON, operatorProductID);
				plansIntent.putExtra("CIRCLE_CODE", circle_code);
				plansIntent.putExtra("DATA_CARD_PLAN", 0);
			} else {
				plansIntent.putExtra(Constants.PLAN_JSON, operatorProductID);
				plansIntent.putExtra("CIRCLE_CODE", "");
				plansIntent.putExtra("DATA_CARD_PLAN", 0);
			}
			startActivityForResult(plansIntent, Constants.PLAN_REQUEST);
			break;
		default:
			break;
		}
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	public class GetPlansCircletask extends AsyncTask<String, String, String> {
		MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {

			String response = RequestClass.getInstance().readPay1B2BRequest(
					getActivity(),
					Constants.B2B_URL + "method=getMobileDetails&mobile="
							+ editRechargeMobile.getText().toString().trim());
			/*
			 * String response = RequestClass.getInstance().readPay1Request(
			 * getActivity(), Constants.URL, listValuePair);
			 */
			String replaced = response.replace("(", "").replace(")", "")
					.replace(";", "");

			return replaced;

		}

		@Override
		protected void onPostExecute(String result) {
			// if (this.dialog.isShowing()) {
			// this.dialog.dismiss();
			// }
			super.onPostExecute(result);
			try {
				JSONArray array = new JSONArray(result);
				JSONObject jsonObject = array.getJSONObject(0);
				String status = jsonObject.getString("status");
				if (status.equalsIgnoreCase("SUCCESS")) {

					JSONObject jsonObjectdetails = jsonObject
							.getJSONObject("details");
					circle_code = jsonObjectdetails.getString("area")
							.toString().trim();
					operatorCode = jsonObjectdetails.getString("operator")
							.toString().trim();

					try {
						operatorDataSource.open();

						List<Operator> operators;
						if (bundle.getInt(Constants.RECHARGE_FOR) == Constants.RECHARGE_MOBILE) {
							operators = operatorDataSource.getAllOperator(
									Constants.RECHARGE_MOBILE, operatorCode);
						} else {
							operators = operatorDataSource.getAllOperator(
									Constants.RECHARGE_DATA, operatorCode);
						}
						if (operators.size() != 0) {
							operatorName = operators.get(0).getOperatorName();
							operatorProductID = String.valueOf(operators.get(0)
									.getOperatorProductID());
							operatorID = String.valueOf(operators.get(0)
									.getOperatorID());
							checkForSTV(operators.get(0).getOperatorSTV());
						}

					} catch (SQLException exception) {
						// TODO: handle exception
						// // Log.e("Er ", exception.getMessage());
					} catch (Exception e) {
						// TODO: handle exception
						// // Log.e("Er ", e.getMessage());
					} finally {
						operatorDataSource.close();
					}
					textView_Operator.setText(operatorName);
					btnPlans.setVisibility(View.VISIBLE);

					// if (circle_code != null && operatorCode != null) {
					// btnPlans.setVisibility(View.VISIBLE);
					// }
				}
			} catch (JSONException e) {
				// e.printStackTrace();
				// Constants.showCustomToast(getActivity(),
				// result);
			} catch (Exception e) {
				// TODO: handle exception
				// Constants.showCustomToast(getActivity(),
				// result);
			}

		}

		@Override
		protected void onPreExecute() {
			// dialog = new MyProgressDialog(PlansActivity.this);
			// // this.dialog.setMessage("Please wait.....");
			// this.dialog.show();
			super.onPreExecute();

		}

	}

	protected void getPlan(String operator, String circle, int amount) {

		try {
			planDataSource.open();

			final List<Plan> plans = planDataSource.getPlanDetails(operator,
					circle, amount, 0);
			List<Plan> data = planDataSource.getAllPlan(operator, circle);
			/*
			 * List<Plan> planUniqList=new ArrayList<Plan>(); Set<Plan>
			 * uniquePlan = new HashSet<Plan>(plans); for (Plan plan :
			 * uniquePlan) { planUniqList.add(plan); }
			 */
			plansAdapter = new PlanAdapterMobile(getActivity(), 1, plans);
			listPlan.setAdapter(plansAdapter);
			listPlan.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int position, long arg3) {
					// TODO Auto-generated method stub
					int amount = plans.get(position).getPlanAmount();
					editRechargeAmount.setText(amount + "");
					textView_Details.setVisibility(View.VISIBLE);
					textView_Details.setText(plans.get(position)
							.getPlanDescription());
				}
			});
			if (plans.size() == 0 && data.size() == 0) {

				new GetPlanstask().execute();
			} else {
				if (plans.size() != 0) {
					String LastUpdateTime = plans.get(0).getPlanUptateTime();
					int days = (int) ((System.currentTimeMillis() - Long
							.parseLong(LastUpdateTime)) / (1000 * 60 * 60 * 24));

					if (days >= 1) {
						planDataSource.deletePlan();
						new GetPlanstask().execute();
					} else {
						textView_Details.setText(plans.get(0)
								.getPlanDescription());
						textView_Details.setVisibility(View.VISIBLE);
						progressBar1.setVisibility(View.GONE);
						textView_Details.setTextColor(getResources().getColor(
								R.color.Black));
					}
				} else {
					textView_Details
							.setText("No such plan exist in our system. Kindly check with operator.");
					textView_Details.setVisibility(View.VISIBLE);
					progressBar1.setVisibility(View.GONE);
					textView_Details.setTextColor(getResources().getColor(
							R.color.Red));
				}
			}

		} catch (SQLException exception) {

			// TODO: handle exception
			// // Log.e("Er ", exception.getMessage());
		} catch (Exception e) {
			// TODO: handle exception
			// // Log.e("Er ", e.getMessage());
		} finally {
			planDataSource.close();
			// progressBar1.setVisibility(View.GONE);
		}

	}

	public class GetPlanstask extends AsyncTask<String, String, String> {
		// MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

			listValuePair
					.add(new BasicNameValuePair("method", "getPlanDetails"));
			listValuePair.add(new BasicNameValuePair("operator", operatorCode));
			listValuePair.add(new BasicNameValuePair("circle", circle_code));

			String response = RequestClass.getInstance().readPay1B2BRequest(
					getActivity(), Constants.B2B_URL, listValuePair);

			String replaced = response.replace("(", "").replace(")", "")
					.replace(";", "");
			savePlansTodataBase(replaced);

			return replaced;

		}

		@Override
		protected void onPostExecute(String result) {
			// if (this.dialog.isShowing()) {
			// this.dialog.dismiss();
			// }

			super.onPostExecute(result);
			try {
				if (result != null && !result.startsWith("Error") && flag != 0) {
					flag--;
					getPlan(operatorCode,
							circle_code,
							Integer.parseInt(editRechargeAmount.getText()
									.toString().trim()));
				} else {
					textView_Details
							.setText("No such plan exist in our system. Kindly check with operator.");
					textView_Details.setVisibility(View.VISIBLE);
					textView_Details.setTextColor(getResources().getColor(
							R.color.Red));
				}
			} catch (Exception e) {
				// TODO: handle exception
				// Constants.showCustomToast(getActivity(),
				// result);
			}

			progressBar1.setVisibility(View.GONE);
		}

		@Override
		protected void onPreExecute() {
			// dialog = new MyProgressDialog(RechargeActivity.this);
			// // this.dialog.setMessage("Please wait.....");
			// this.dialog.show();
			progressBar1.setVisibility(View.VISIBLE);
			super.onPreExecute();

		}

	}

	private void savePlansTodataBase(String planJson) {
		try {
			planDataSource.open();
			// ContentValues contentValues = new ContentValues();
			JSONArray array = new JSONArray(planJson);
			JSONObject planJsonObject = array.getJSONObject(0);

			Iterator<String> operatorKey = planJsonObject.keys();

			if (operatorKey.hasNext()) {

				long time = System.currentTimeMillis();

				JSONObject jsonObject2 = planJsonObject
						.getJSONObject(operatorKey.next());

				String opr_name = jsonObject2.getString("opr_name");
				String prod_code_pay1 = jsonObject2.getString("prod_code_pay1");
				// contentValues.put("operator_id", prod_code_pay1);
				// contentValues.put("operator_name", opr_name);
				// contentValues.put("prod_code_pay1", prod_code_pay1);
				JSONObject circleJsonObject = jsonObject2
						.getJSONObject("circles");
				Iterator<String> circleKey = circleJsonObject.keys();
				if (circleKey.hasNext()) {
					JSONObject circleJsonObject2 = circleJsonObject
							.getJSONObject(circleKey.next().toString());
					String circleName = circleJsonObject2
							.getString("circle_name");

					// contentValues.put("circle_name", circleName);

					JSONObject plansJsonObject = circleJsonObject2
							.getJSONObject("plans");
					String circle_id = circleJsonObject2.getString("circle_id");
					Iterator<String> planKey = plansJsonObject.keys();
					Iterator<String> planKeyDB = plansJsonObject.keys();
					// contentValues.put("circle_id", circle_id);

					while (planKey.hasNext()) {
						String planType = planKey.next().toString();
						JSONArray planArray = plansJsonObject
								.getJSONArray(planType);
						// contentValues.put("planType", planType);

						for (int i = 0; i < planArray.length(); i++) {
							JSONObject jsonObject = planArray.getJSONObject(i);
							String plan_desc = jsonObject
									.getString("plan_desc");
							// contentValues.put("plan_desc", plan_desc);
							String plan_amt = jsonObject.getString("plan_amt");
							// contentValues.put("plan_amt", plan_amt);
							String plan_validity = jsonObject
									.getString("plan_validity");
							// contentValues.put("plan_validity",
							// plan_validity);
							// // Log.e("cORRECT", "cORRECT   " + i + "  "
							// + jsonObject.toString());
							// controller.insertPlans(contentValues);
							planDataSource.createPlan(prod_code_pay1, opr_name,
									circle_id, circleName, planType, plan_amt,
									plan_validity, plan_desc, "" + time);
						}

					}

				}

			}
		} catch (JSONException exception) {

		} catch (SQLException exception) {
			// TODO: handle exception
			// // Log.e("Er ", exception.getMessage());
		} catch (Exception e) {
			// TODO: handle exception
			// // Log.e("Er ", e.getMessage());
		} finally {
			planDataSource.close();
		}
	}

	// class RequestTask extends AsyncTask<String, String, String> {
	//
	// @Override
	// protected String doInBackground(String... uri) {
	// HttpClient httpclient = new DefaultHttpClient();
	// HttpResponse response;
	// String responseString = null;
	// try {
	// response = httpclient.execute(new HttpGet(Constants.B2B_URL
	// + "method=getPlanDetails&operator" + pay1OpId
	// + "&circle=all"));
	// StatusLine statusLine = response.getStatusLine();
	// if (statusLine.getStatusCode() == HttpStatus.SC_OK) {
	// ByteArrayOutputStream out = new ByteArrayOutputStream();
	// response.getEntity().writeTo(out);
	// out.close();
	// responseString = out.toString();
	// } else {
	// // Closes the connection.
	// response.getEntity().getContent().close();
	// throw new IOException(statusLine.getReasonPhrase());
	// }
	// } catch (ClientProtocolException e) {
	// // TODO Handle problems..
	// } catch (IOException e) {
	// // TODO Handle problems..
	// e.printStackTrace();
	// }
	// return responseString;
	// }
	//
	// @Override
	// protected void onPostExecute(String result) {
	// super.onPostExecute(result);
	// // Do anything with response..
	// }
	// }

	public class GetOperatorTask extends AsyncTask<String, String, String> {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			try {
				String response = RequestClass.getInstance()
						.readPay1B2CRequest(getActivity(),
								Constants.B2C_URL + "get_all_operators/?");
				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}

			try {

				long timestamp = System.currentTimeMillis();
				operatorDataSource.open();

				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");
					JSONObject jsonObject = new JSONObject(replaced);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {
						JSONObject description = jsonObject
								.getJSONObject("description");
						JSONArray mobile = description.getJSONArray("mobile");
						JSONArray data = description.getJSONArray("data");
						JSONArray postpaid = description
								.getJSONArray("postpaid");
						JSONArray dth = description.getJSONArray("dth");

						for (int i = 0; i < mobile.length(); i++) {
							JSONObject object = mobile.getJSONObject(i);

							operatorDataSource
									.createOperator(
											Integer.parseInt(object.getString(
													"id").trim()),
											object.getString("opr_code").trim(),
											Integer.parseInt(object.getString(
													"product_id").trim()),
											object.getString("name").trim(),
											Integer.parseInt(object.getString(
													"service_id").trim()),
											Integer.parseInt(object.getString(
													"flag").trim()),
											Integer.parseInt(object.getString(
													"delegate").trim()),
											Integer.parseInt(object.getString(
													"stv").trim()),
											Constants.RECHARGE_MOBILE,
											timestamp,
											Integer.parseInt(object
													.getString("max")),
											Integer.parseInt(object
													.getString("min")),
											Integer.parseInt(object
													.getString("service_charge_amount")),
											Integer.parseInt(object
													.getString("service_tax_percent")),
											Integer.parseInt(object
													.getString("service_charge_percent")),
											object.getString("charges_slab")
													.toString());

						}
						for (int i = 0; i < data.length(); i++) {
							JSONObject object = data.getJSONObject(i);

							operatorDataSource
									.createOperator(
											Integer.parseInt(object.getString(
													"id").trim()),
											object.getString("opr_code").trim(),
											Integer.parseInt(object.getString(
													"product_id").trim()),
											object.getString("name").trim(),
											Integer.parseInt(object.getString(
													"service_id").trim()),
											Integer.parseInt(object.getString(
													"flag").trim()),
											Integer.parseInt(object.getString(
													"delegate").trim()),
											Integer.parseInt(object.getString(
													"stv").trim()),
											Constants.RECHARGE_DATA,
											timestamp,
											Integer.parseInt(object
													.getString("max")),
											Integer.parseInt(object
													.getString("min")),
											Integer.parseInt(object
													.getString("service_charge_amount")),
											Integer.parseInt(object
													.getString("service_tax_percent")),
											Integer.parseInt(object
													.getString("service_charge_percent")),
											object.getString("charges_slab")
													.toString());
						}
						for (int i = 0; i < postpaid.length(); i++) {
							JSONObject object = postpaid.getJSONObject(i);

							operatorDataSource
									.createOperator(
											Integer.parseInt(object.getString(
													"id").trim()),
											object.getString("opr_code").trim(),
											Integer.parseInt(object.getString(
													"product_id").trim()),
											object.getString("name").trim(),
											Integer.parseInt(object.getString(
													"service_id").trim()),
											Integer.parseInt(object.getString(
													"flag").trim()),
											Integer.parseInt(object.getString(
													"delegate").trim()),
											Integer.parseInt(object.getString(
													"stv").trim()),
											Constants.BILL_PAYMENT,
											timestamp,
											Integer.parseInt(object
													.getString("max")),
											Integer.parseInt(object
													.getString("min")),
											Integer.parseInt(object
													.getString("service_charge_amount")),
											Integer.parseInt(object
													.getString("service_tax_percent")),
											Integer.parseInt(object
													.getString("service_charge_percent")),
											object.getString("charges_slab")
													.toString());
						}
						for (int i = 0; i < dth.length(); i++) {

							JSONObject object = dth.getJSONObject(i);

							operatorDataSource
									.createOperator(
											Integer.parseInt(object.getString(
													"id").trim()),
											object.getString("opr_code").trim(),
											Integer.parseInt(object.getString(
													"product_id").trim()),
											object.getString("name").trim(),
											Integer.parseInt(object.getString(
													"service_id").trim()),
											Integer.parseInt(object.getString(
													"flag").trim()),
											Integer.parseInt(object.getString(
													"delegate").trim()),
											Integer.parseInt(object.getString(
													"stv").trim()),
											Constants.RECHARGE_DTH,
											timestamp,
											Integer.parseInt(object
													.getString("max")),
											Integer.parseInt(object
													.getString("min")),
											Integer.parseInt(object
													.getString("service_charge_amount")),
											Integer.parseInt(object
													.getString("service_tax_percent")),
											Integer.parseInt(object
													.getString("service_charge_percent")),
											object.getString("charges_slab")
													.toString());
						}

					}
				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonDialog(getActivity(), "Operator",
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			} catch (SQLException exception) {
				// TODO: handle exception
				// // Log.e("Er ", exception.getMessage());
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonDialog(getActivity(), "Operator",
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			} finally {
				operatorDataSource.close();
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(getActivity());
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(false);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							GetOperatorTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}
	}

}
