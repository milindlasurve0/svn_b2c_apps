//
//  SaveUserReviewService.m
//  Pay1
//
//  Created by Annapurna on 25/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "SaveUserReviewService.h"
#import "SaveUserResponse.h"
@implementation SaveUserReviewService
-(void)callSaveUserReview:(NSString *)offerReviewID reviewPoint:(NSString *)reviewPoint callback:(saveUserResponse)callback{
    NSMutableDictionary *postData = [[NSMutableDictionary alloc]init];
    [postData setObject:offerReviewID forKey:@"id"];
    [postData setObject:reviewPoint forKey:@"review"];
    
    NSLog(@"PostData for MyLike:%@",postData);
    
    RequestData *requestData = [[RequestData alloc]init];
    requestData.baseURL = kBaseURL;
    requestData.webServiceURL = kUserReviewURL;
    requestData.postData = postData;
    requestData.showLoader = FALSE;
    
    requestData.requestSuccessBlock = ^(RequestData *request) {
        
    [self printResponse:request.responseData forRequest:@"MyLike Response"];
    if ([self handleNullResponse:request])
    return;
        
    callback(nil, [self parseUserReviewService:request]);
    };
    
    
    requestData.requestFailureBlock = ^(RequestData *request) {
        [self handleNetworkError:request.responseData];
        callback(request.responseData, nil);
    };
    
    [[ServiceManager sharedInstance] makeRequest:requestData];
    
}

-(SaveUserResponse *)parseUserReviewService:(RequestData *)request{
    SaveUserResponse *objResponse = [[SaveUserResponse alloc]init];
    objResponse.staus = [NSString handleNull:[request.responseData objectForKey:@"status"]];
    objResponse.desc = [NSString handleNull:[request.responseData objectForKey:@"description"]];
    objResponse.errorCode = [NSString handleNull:[request.responseData objectForKey:@"errCode"]];
    
    return objResponse;
    
}

@end
