package com.pay1.servicehandler;

import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.IntentService;
import android.content.Intent;
import android.database.SQLException;
import android.os.AsyncTask;

import com.pay1.constants.Constants;
import com.pay1.databasehandler.FreeBiesDataSource;
import com.pay1.databasehandler.FreeBiesLocationDataSource;
import com.pay1.requesthandler.RequestClass;
import com.pay1.utilities.Utility;

public class FreeBiesUpdateService extends IntentService {

	private FreeBiesDataSource freeBiesDataSource;
	private FreeBiesLocationDataSource freeBiesLocationDataSource;

	public FreeBiesUpdateService() {
		super("FreeBiesUpdateService");
		// TODO Auto-generated constructor stub
		freeBiesDataSource = new FreeBiesDataSource(FreeBiesUpdateService.this);
		freeBiesLocationDataSource = new FreeBiesLocationDataSource(
				FreeBiesUpdateService.this);
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		new FreeBiesTask().execute(Utility.getCurrentLatitude(
				FreeBiesUpdateService.this,
				Constants.SHAREDPREFERENCE_CURRENT_LATITUDE), Utility
				.getCurrentLongitude(FreeBiesUpdateService.this,
						Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE));
	}

	public class FreeBiesTask extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... params) {
			try {

				String time = Utility
						.getSharedPreferences(FreeBiesUpdateService.this,
								Constants.LAST_GIFT_UPDATED) == null ? ""
						: Utility.getSharedPreferences(
								FreeBiesUpdateService.this,
								Constants.LAST_GIFT_UPDATED);

				if (time != "") {
					try {
						DateFormat dateFormat = new SimpleDateFormat(
								"yyyy-MM-dd HH:mm:ss");
						Date date = new Date(Long.parseLong(time));
						time = dateFormat.format(date);
					} catch (Exception e) {
					}
				}
				String response = RequestClass
						.getInstance()
						.readPay1B2CRequest(
								FreeBiesUpdateService.this,
								Constants.B2C_URL
										+ "GetupdatedDeal/?latitude="
										+ params[0]
										+ "&longitude="
										+ params[1]
										+ "&user_mobile="
										+ Utility
												.getMobileNumber(
														FreeBiesUpdateService.this,
														Constants.SHAREDPREFERENCE_MOBILE)
										+ "&updatedTime="
										+ URLEncoder.encode(time, "utf-8")
										+ "&next=-1");

				if (!response.startsWith("Error")) {
					JSONObject jsonObject = new JSONObject(response);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {

						try {

							freeBiesDataSource.open();
							freeBiesLocationDataSource.open();

							JSONObject jsonObjectDesc = jsonObject
									.getJSONObject("description");
							Utility.setOrder(FreeBiesUpdateService.this,
									Constants.SHAREDPREFERENCE_GIFT,
									jsonObjectDesc.getString("claimedGifts"));
							Utility.setOrder(FreeBiesUpdateService.this,
									Constants.SHAREDPREFERENCE_GIFT_EXPIRED,
									jsonObjectDesc.getString("ExpiredOffers"));
							try {
								freeBiesDataSource
										.deleteFreeBies(jsonObjectDesc
												.getString("ExpiredOffers"));
								freeBiesLocationDataSource
										.deleteFreeBiesLocation(jsonObjectDesc
												.getString("ExpiredOffers"));
							} catch (Exception e) {

							}

							JSONArray jsonArrayAlldeals = jsonObjectDesc
									.getJSONArray("Alldeals");
							long timestamp = System.currentTimeMillis();

							freeBiesDataSource.createFreeBies(
									jsonArrayAlldeals, timestamp);
							// for (int i = 0; i < jsonArrayAlldeals.length();
							// i++) {
							//
							// String deal_id = jsonArrayAlldeals
							// .getJSONObject(i).getString("id");
							// try {
							// int id = Integer.parseInt(deal_id);
							// freeBiesDataSource.deleteFreeBies(id);
							// freeBiesLocationDataSource
							// .deleteFreeBiesLocation(id);
							// } catch (Exception e) {
							// continue;
							// }
							//
							// String deal_name = jsonArrayAlldeals
							// .getJSONObject(i).getString("name");
							// String url = jsonArrayAlldeals.getJSONObject(i)
							// .getString("i_url");
							// String catagory = jsonArrayAlldeals
							// .getJSONObject(i).getString("cat");
							// String catagory_id = jsonArrayAlldeals
							// .getJSONObject(i).getString("cat_id");
							// String min_amt = jsonArrayAlldeals
							// .getJSONObject(i).getString("min");
							// String offer_id = jsonArrayAlldeals
							// .getJSONObject(i).getString("of_id");
							// String offer_name = jsonArrayAlldeals
							// .getJSONObject(i).getString("of_name");
							// String validity = jsonArrayAlldeals
							// .getJSONObject(i).getString("valid");
							// String desc = jsonArrayAlldeals
							// .getJSONObject(i).getString("desc");
							// String offer_lat = "0", offer_lng = "0",
							// offer_add = "", offer_area = "", offer_city = "",
							// offer_state = "";
							//
							// JSONArray jsonArrayLocation = jsonArrayAlldeals
							// .getJSONObject(i).getJSONArray("locs");
							//
							// for (int j = 0; j < jsonArrayLocation.length();
							// j++) {
							// JSONObject jsonObjectInner = jsonArrayLocation
							// .getJSONObject(j);
							// WeakHashMap<String, String> map1 = new
							// WeakHashMap<String, String>();
							//
							// String loc_lat = jsonObjectInner
							// .getString("lat");
							// String loc_lng = jsonObjectInner
							// .getString("lng");
							// String loc_add = jsonObjectInner
							// .getString("addr");
							// String loc_area = jsonObjectInner
							// .getString("area");
							// String loc_city = jsonObjectInner
							// .getString("city");
							// String loc_state = jsonObjectInner
							// .getString("state");
							//
							// if (j == 0) {
							// offer_lat = loc_lat;
							// offer_lng = loc_lng;
							// offer_add = loc_add;
							// offer_area = loc_area;
							// offer_city = loc_city;
							// offer_state = loc_state;
							// }
							//
							// freeBiesLocationDataSource
							// .createFreeBiesLocation(
							// Integer.parseInt(offer_id),
							// Double.parseDouble(loc_lat),
							// Double.parseDouble(loc_lng),
							// loc_add, loc_area,
							// loc_city, loc_state, 0, 0,
							// timestamp);
							// }
							// freeBiesDataSource.createFreeBies(
							// Integer.parseInt(deal_id), deal_name,
							// url, catagory,
							// Integer.parseInt(catagory_id),
							// Double.parseDouble(offer_lat),
							// Double.parseDouble(offer_lng),
							// offer_add, offer_area, offer_city,
							// offer_state, 0,
							// jsonArrayLocation.length(),
							// Integer.parseInt(offer_id), offer_name,
							// validity, desc,
							// Integer.parseInt(min_amt), timestamp);
							// }

							Utility.setSharedPreferences(
									FreeBiesUpdateService.this,
									Constants.LAST_GIFT_UPDATED,
									String.valueOf(System.currentTimeMillis()));
						} catch (SQLException exception) {
							// TODO: handle exception
							// Log.e("Er ", exception.getMessage());
						} catch (Exception e) {
							// TODO: handle exception
							// Log.e("Er ", e.getMessage());
						} finally {
							freeBiesDataSource.close();
							freeBiesLocationDataSource.close();
						}

					}
				}
			} catch (Exception e) {

			}
			return "Error";
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

	}
}
