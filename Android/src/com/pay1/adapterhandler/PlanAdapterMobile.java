package com.pay1.adapterhandler;

import java.util.List;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.pay1.R;
import com.pay1.databasehandler.Plan;

public class PlanAdapterMobile extends ArrayAdapter<Plan> {
	Context mContext;
	List<Plan> jsonObject;
	LayoutInflater inflater;
	TextView textPlanAmount, textPlanValidity, textPlanDesc, textView_Validity;
	Typeface Normal, Reguler, Medium;

	public PlanAdapterMobile(Context context, int textViewResourceId,
			List<Plan> objects) {
		super(context, textViewResourceId, objects);
		// TODO Auto-generated constructor stub
		this.mContext = context;
		this.jsonObject = objects;
		inflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		Normal = Typeface.createFromAsset(context.getAssets(),
				"EncodeSansNormal-100-Thin.ttf");
		Reguler = Typeface.createFromAsset(context.getAssets(),
				"EncodeSansNormal-400-Regular.ttf");
		Medium = Typeface.createFromAsset(context.getAssets(),
				"EncodeSansNarrow-500-Medium.ttf");
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View view = convertView;
		if (convertView == null) {

			view = inflater.inflate(R.layout.mobile_plan_adapter, null);
		}

		// if (position % 2 == 1) {
		// view.setBackgroundColor(Color.WHITE);
		// } else {
		//
		// view.setBackgroundColor(mContext.getResources().getColor(
		// R.color.row_light_green));
		// }

		textPlanAmount = (TextView) view.findViewById(R.id.textPlanAmount);
		textPlanValidity = (TextView) view.findViewById(R.id.textPlanValidity);
		textPlanDesc = (TextView) view.findViewById(R.id.textPlanDesc);
		textView_Validity = (TextView) view
				.findViewById(R.id.textView_Validity);
		textView_Validity.setTypeface(Reguler);
		textPlanDesc.setTypeface(Normal);
		textPlanAmount.setTypeface(Normal);
		textPlanValidity.setTypeface(Normal);

		try {
			textPlanAmount.setText(""
					+ jsonObject.get(position).getPlanAmount());
			textPlanValidity
					.setText(jsonObject.get(position).getPlanValidity());
			textPlanDesc.setText(jsonObject.get(position).getPlanDescription());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return view;
	}

}
