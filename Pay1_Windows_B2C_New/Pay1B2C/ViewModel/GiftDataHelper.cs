﻿using Pay1B2C.Model;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pay1B2C.ViewModel
    {
    class GiftDataHelper
        {

        public void InsertGifts(GiftsTable gift)
            {

            using(var dbConn = new SQLiteConnection(App.DB_PATH))
                {
                try
                    {
                    //var existingconact = dbConn.Query<GiftsTable>("select * from GiftsTable where freebieDealID =" + contactid).FirstOrDefault();
                    dbConn.RunInTransaction(() =>
                    {
                        try
                            {
                            int i=  dbConn.Insert(gift);
                            }
                        catch(SQLiteException se)
                            {
                            }
                        catch(Exception e)
                            {
                            }
                    });


                    }
                catch(Exception exc)
                    {
                    }

                }

            }






        public void InsertGiftLocation(LocationTable gift)
            {

            using(var dbConn = new SQLiteConnection(App.DB_PATH))
                {
                try
                    {
                    //var existingconact = dbConn.Query<GiftsTable>("select * from GiftsTable where freebieDealID =" + contactid).FirstOrDefault();
                    dbConn.RunInTransaction(() =>
                    {
                        int i=  dbConn.Insert(gift);
                    });


                    }
                catch(Exception exc)
                    {
                    }

                }

            }


        public void DeleteGifts(int Id)
            {
            using(var dbConn = new SQLiteConnection(App.DB_PATH))
                {
                var existingGift = dbConn.Query<GiftsTable>("select * from GiftsTable where freebieOfferID =" + Id).FirstOrDefault();
                if(existingGift != null)
                    {
                    dbConn.RunInTransaction(() =>
                    {
                        int delCount=   dbConn.Delete(existingGift);
                    });
                    }
                }
            }


        public void DeleteGiftsForLocation(int Id)
            {
            using(var dbConn = new SQLiteConnection(App.DB_PATH))
                {
                var existingGift = dbConn.Query<LocationTable>("select * from LocationTable where locationDealID =" + Id).FirstOrDefault();
                if(existingGift != null)
                    {
                    dbConn.RunInTransaction(() =>
                    {
                        int delCount=   dbConn.Delete(existingGift);
                    });
                    }
                }
            }


        public GiftsTable ReadGiftByDealId(int giftId)
            {
            using(var dbConn = new SQLiteConnection(App.DB_PATH))
                {
                try
                    {
                    var existingconact = dbConn.Query<GiftsTable>("select * from GiftsTable where freebieOfferID =" + giftId).FirstOrDefault();
                    return existingconact;
                    }
                catch(Exception exc)
                    {
                    }
                return new GiftsTable();
                }
            }


        public LocationTable ReadLocationByDealId(int giftId)
            {
            using(var dbConn = new SQLiteConnection(App.DB_PATH))
                {
                try
                    {
                    var existingconact = dbConn.Query<LocationTable>("select * from LocationTable where locationID =" + giftId).FirstOrDefault();
                    return existingconact;
                    }
                catch(Exception exc)
                    {
                    }
                return new LocationTable();
                }
            }
        public List<GiftsTable> ReadNearByGiftByLocation(double curLat, double curLong, int dis)
            {
            List<GiftsTable> freebies = new List<GiftsTable>();
            List<GiftsTable> freebieLocation = new List<GiftsTable>();
            Dictionary<int, Double> hashMap = new Dictionary<int, Double>();
            List<Dictionary<int, Double>> distanceDict = new List<Dictionary<int, Double>>();

            freebieLocation=getAllFreeBies();
            foreach(GiftsTable giftLoc in freebieLocation)
                {
                getNearestLocationFromLocationTable(curLat, curLong, giftLoc.freebieOfferID);
                }





            return freebies;
            }

        public List<GiftsTable> ReadNearByGift(double curLat, double curLong, int dis)
            {

            List<GiftsTable> lst=null;
            List<GiftsTable> freebies = new List<GiftsTable>();
            List<GiftsTable> freebies2 = new List<GiftsTable>();
            List<Dictionary<int, Double>> distanceDict = new List<Dictionary<int, Double>>();

            Dictionary<int, Double> dict=new Dictionary<int, double>();
            using(var dbConn = new SQLiteConnection(App.DB_PATH))
                {
                try
                    {
                    lst= dbConn.Query<GiftsTable>("select * from GiftsTable");
                    // return lst;

                    foreach(GiftsTable gifts in lst)
                        {
                        double distance=  Constant.distance(curLat, curLong, gifts.freebieLat, gifts.freebieLng);
                        if(distance<dis)
                            {

                            dict.Add(gifts.freebieOfferID, distance);
                            distanceDict.Add(dict);
                            }
                        }


                    var list = dict.ToList();
                    //  list.Sort();

                    var sortedDict = from entry in list orderby entry.Value ascending select entry;

                    foreach(var dict1 in sortedDict)
                        {
                        LocationTable locTable=getNearestLocationFromLocationTable(curLat, curLong, dict1.Key);
                        GiftsTable giftsTable=ReadGiftByOfferId(dict1.Key);
                        giftsTable.freebieLat=locTable.locationLat;
                        giftsTable.freebieLng=locTable.locationLng;
                        giftsTable.freebieAddress=locTable.locationAddress;
                        giftsTable.freebieArea=locTable.locationArea;
                        giftsTable.freebieCity=locTable.locationCity;
                        giftsTable.freebieState=locTable.locationState;
                        freebies.Add(giftsTable);
                        }


                    return freebies;
                    }
                catch(Exception exc)
                    {
                    }
                }
            return freebies;
            }

        public LocationTable getNearestLocationFromLocationTable(double curLat, double curLong, int offerId)
            {

            List<LocationTable> lst=null;
            List<LocationTable> freebies = new List<LocationTable>();
            List<LocationTable> freebies2 = new List<LocationTable>();
            List<Dictionary<int, Double>> distanceDict = new List<Dictionary<int, Double>>();
            GiftsTable giftsTable=ReadGiftByOfferId(offerId);
            Dictionary<int, Double> dict=new Dictionary<int, double>();
            using(var dbConn = new SQLiteConnection(App.DB_PATH))
                {
                try
                    {
                    lst= dbConn.Query<LocationTable>("select * from LocationTable where locationDealID = "+giftsTable.freebieDealID);
                    // return lst;
                    }
                catch(Exception exc)
                    {
                    }
                foreach(LocationTable gifts in lst)
                    {
                    double distance=  Constant.distance(curLat, curLong, gifts.locationLat, gifts.locationLng);

                    dict.Add(gifts.locationID, distance);
                    distanceDict.Add(dict);

                    }


                var list = dict.ToList();
                //  list.Sort();

                var sortedDict = from entry in list orderby entry.Value ascending select entry;

                foreach(var dict1 in sortedDict)
                    {

                    freebies.Add(ReadLocationByDealId(dict1.Key));
                    break;
                    }


                return freebies.FirstOrDefault();
                }
            return freebies.FirstOrDefault();
            }

        public List<GiftsTable> getAllFreeBies()
            {
            List<GiftsTable> allFreeBie=null;
             using(var dbConn = new SQLiteConnection(App.DB_PATH))
                {
                try
                    {
                    allFreeBie = dbConn.Query<GiftsTable>("select * from GiftsTable");
                    return allFreeBie;
                    }
                catch(Exception exc)
                    {
                    }
                return allFreeBie;
                }
            }



        public List<GiftsTable> getAllLikedFreeBies()
            {
            List<GiftsTable> allFreeBie=null;
            using(var dbConn = new SQLiteConnection(App.DB_PATH))
                {
                try
                    {
                    allFreeBie = dbConn.Query<GiftsTable>("select * from GiftsTable where freebieLike = 1");
                    return allFreeBie;
                    }
                catch(Exception exc)
                    {
                    }
                return allFreeBie;
                }
            }

        public GiftsTable ReadGiftByOfferId(int contactid)
            {
            using(var dbConn = new SQLiteConnection(App.DB_PATH))
                {
                try
                    {
                    var existingconact = dbConn.Query<GiftsTable>("select * from GiftsTable where freebieOfferID =" + contactid).FirstOrDefault();
                    return existingconact;
                    }
                catch(Exception exc)
                    {
                    }
                return new GiftsTable();
                }
            }


        public List<GiftsTable> ReadGiftByOfferIds(String order)
            {
            List<GiftsTable> lst=null;
            if(order != "")
                {
                String[] orders = order.Split(',');

                StringBuilder sb = new StringBuilder();
                sb.Append(" case ").Append("freebieOfferID")
                        .Append(" when ").Append(orders[0]).Append(" then ")
                        .Append("1");
                for(int i = 0;i < orders.Length;i++)
                    {
                    if(i != 0)
                        sb.Append(" when ").Append(orders[i]).Append(" then ")
                                .Append(i + 1);
                    }
                sb.Append(" end ");


                using(var dbConn = new SQLiteConnection(App.DB_PATH))
                    {
                    try
                        {
                        lst= dbConn.Query<GiftsTable>("select * from GiftsTable where freebieOfferID In ("+order+") group by "+sb);
                        return lst;
                        }
                    catch(Exception exc)
                        {
                        }
                    return lst;
                    }

                }

            return lst;

            }


        public void updateGiftForLikeByOfferId(int offerId, int setLike)
            {
           
            using(var dbConn = new SQLiteConnection(App.DB_PATH))
                {
                try
                    {
                    var existingconact = dbConn.Query<GiftsTable>("UPDATE GiftsTable SET freebieLike = "+setLike+" where freebieOfferID =" + offerId);
                   
                    }
                catch(Exception exc)
                    {
                    }
               
                }
            }


        }
    }
