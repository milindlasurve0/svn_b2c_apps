//
//  AppDelegate.m
//  Pay1
//
//  Created by Annapurna on 02/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//http://174.129.181.27/index.php/api_new/action/api/true/actiontype/signin/api/true/?username=8860675961&password=12345&uuid=hfdjshfdfjksd&latitude=28.8&longitude=77.3&device_type=ios

//http://nguyenmanhit.blogspot.in/2014/01/ios-programming-insertupdatedeleteselec.html

#import "AppDelegate.h"
#import "ViewController.h"
#import "CurrentLocation.h"
#import "Constant.h"
#import "SWRevealViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import <FacebookSDK/FacebookSDK.h>
#import "DBManager.h"


#import "AllDealList.h"
#import "NSString+Utility.h"
#import "GetUpdatedDealResponse.h"
#import "DealLocationList.h"
#import "PlanList.h"
#import "GetPlanResponse.h"
#import "GetMobileDataList.h"
#import "GetMobileResponse.h"
#import "OperatorList.h"
#import "GetOperatorResponse.h"
#import "Session.h"
#import "GetPlanService.h"
#import "SignInCriteria.h"
#import "SignInResponse.h"
#import "SignInService.h"
#import "UIDevice+IdentifierAddition.h"
#import "NSString+MD5Addition.h"
#import "SidebarViewController.h"

#import "HomeViewController.h"
#import "MainViewController.h"
#import "GiftsViewController.h"
#import "WalletMoneyViewController.h"
#import "OptionViewController.h"
#import "ViewController.h"
@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [CurrentLocation sharedLocation];
    [Constant saveQuickPayRecord:FALSE];

    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];

    
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"red_bar.png"]forBarMetrics:UIBarMetricsDefault];
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

    
    HomeViewController *homeVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
    UINavigationController *objNavHome = [[UINavigationController alloc]initWithRootViewController:homeVC];

    objNavHome.tabBarItem.title = @"HOME";
    objNavHome.tabBarItem.image = [UIImage imageNamed:@"tab_Home.png"];

    
    //Recharge controller
    MainViewController *mainVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"mainViewC"];
    UINavigationController *objNavMain = [[UINavigationController alloc]initWithRootViewController:mainVC];

    objNavMain.tabBarItem.title = @"RECHARGE";
    objNavMain.tabBarItem.image = [UIImage imageNamed:@"tab_Recharge.png"];

    
    //Gifts controller
    GiftsViewController *giftVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"giftViewC"];
    UINavigationController *objNavGift = [[UINavigationController alloc]initWithRootViewController:giftVC];

    objNavGift.tabBarItem.title = @"GIFT";
    objNavGift.tabBarItem.image = [UIImage imageNamed:@"tab_Gift.png"];


    
    //Wallet controller
    WalletMoneyViewController *walletVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"walletDetailViewC"];
    UINavigationController *objNavWallet = [[UINavigationController alloc]initWithRootViewController:walletVC];

    objNavWallet.tabBarItem.title = @"ADD MONEY";
    objNavWallet.tabBarItem.image = [UIImage imageNamed:@"tab_add_money.png"];

    
    OptionViewController *optionVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"optionV"];
    UINavigationController *objNavOption = [[UINavigationController alloc]initWithRootViewController:optionVC];
    objNavOption.tabBarItem.image = [UIImage imageNamed:@"tab_More.png"];

    
    objNavOption.tabBarItem.title = @"MORE";

    
    //init the UITabBarController
    
    self.tabBarController = [[UITabBarController alloc]init];
    self.tabBarController.viewControllers = @[objNavHome,objNavMain,objNavGift,objNavWallet,objNavOption];

    [[UITabBarItem appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor] }
                                             forState:UIControlStateNormal];
    
    
    
    [GMSServices provideAPIKey:@"AIzaSyCZXKgUh6MEjedpeam_KFZjYM3HjwIPHoU"];
    
    
    NSLog(@"Current identifier: %@", [[NSBundle mainBundle] bundlePath]);
 
    UIImage *tabBackground = [UIImage imageNamed:@"footerTab.png"];
    [[UITabBar appearance] setBackgroundImage:tabBackground];
   // [[UITabBar appearance] setSelectionIndicatorImage:[UIImage imageNamed:@"selectedTab.png"]];
   // [[UITabBar appearance] setSelectedImageTintColor:[UIColor whiteColor]];
    
    [[UITabBar appearance] setSelectedImageTintColor:[UIColor redColor]];

    
    NSLog(@"#Check User name and password :%@ %@",[Constant getUserPhoneNumber],[Constant getUserPass]);
    
    if(![Constant checkNetworkConnection]){
        
        NSLog(@"#NoNetworkFound");
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"No Internet Connection" message:@"Oops!\nLooks like we lost you" delegate:self cancelButtonTitle:@"Retry" otherButtonTitles:nil, nil];
        
        [alert show];
        
        
        
        
        ViewController *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"viewC"];
        _navController = [[UINavigationController alloc]initWithRootViewController:vc];
        self.window.rootViewController = _navController;
        [self.window makeKeyAndVisible];
    }
    
    
    
    
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"HasLaunchedonce"]){
        NSLog(@"second Time");
        
        [Session shared].isAppLaunched = FALSE;
        
        
        
        
        
        
        
        
        
        if ([Constant getUserPhoneNumber].length == 0 || [Constant getUserPass].length == 0) {
            
            [Session shared].isFromAutoLogin = TRUE;
            
            
            if(![Constant checkNetworkConnection]){
                
                NSLog(@"#NoNetworkFound");
                
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"No Internet Connection" message:@"Oops!\nLooks like we lost you" delegate:self cancelButtonTitle:@"Retry" otherButtonTitles:nil, nil];
                
                [alert show];
                
                
                
                
                return TRUE;
            }
            
            ViewController *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"viewC"];
            _navController = [[UINavigationController alloc]initWithRootViewController:vc];
            self.window.rootViewController = _navController;
            [self.window makeKeyAndVisible];
        }
        
            
            
            else{
                [self.window addSubview:self.tabBarController.view];
                [self.window addSubview:self.navController.view];
                
                [self.window makeKeyAndVisible];
                
                
                
                [self callSignInAPIService];
            }
            
        
        
        
      /*  [self.window addSubview:self.tabBarController.view];
        [self.window addSubview:self.navController.view];
        
        [self.window makeKeyAndVisible];*/
        
    }
    
    
    else {
        [Session shared].isFromAutoLogin = FALSE;
        
        [Session shared].isAppLaunched = TRUE;
        
        NSLog(@"first Time");
        
        [Constant saveLastGiftTime:@"2014-01-01 12:12:12"];
        [Constant saveLastMobilePlanTime:@"2014-01-01 12:12:12"];
        [Constant saveLastServicePlanTime:@"2014-01-01 12:12:12"];
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"HasLaunchedonce"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
        
        ViewController *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"viewC"];
        _navController = [[UINavigationController alloc]initWithRootViewController:vc];
        self.window.rootViewController = _navController;
        [self.window makeKeyAndVisible];
        
        
        // [self.window addSubview:self.tabBarController.view];
        //[self.window addSubview:self.navController.view];
        
        //  [self.window makeKeyAndVisible];
        
    }

    
    
    
   
    
   /* if ([Constant getUserPhoneNumber].length > 0 && [Constant getUserPass].length >0) {
        
        [Session shared].isFromAutoLogin = TRUE;

        
        if(![Constant checkNetworkConnection]){
            
            NSLog(@"#NoNetworkFound");
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"No Internet Connection" message:@"Oops!\nLooks like we lost you" delegate:self cancelButtonTitle:@"Retry" otherButtonTitles:nil, nil];
            
            [alert show];
            
            
            return TRUE;
        }

    
        
        else{
            
        
            
            
    [self callSignInAPIService];
        }
        
    }
    
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"HasLaunchedonce"]){
        NSLog(@"second Time");
        
        [Session shared].isAppLaunched = FALSE;

        
        [self.window addSubview:self.tabBarController.view];
        [self.window addSubview:self.navController.view];
        
        [self.window makeKeyAndVisible];

    }
    
    
    else {
        [Session shared].isFromAutoLogin = FALSE;
        
        [Session shared].isAppLaunched = TRUE;

        NSLog(@"first Time");
        
        [Constant saveLastGiftTime:@"2014-01-01 12:12:12"];
        [Constant saveLastMobilePlanTime:@"2014-01-01 12:12:12"];
        [Constant saveLastServicePlanTime:@"2014-01-01 12:12:12"];
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"HasLaunchedonce"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
        
        ViewController *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"viewC"];
         _navController = [[UINavigationController alloc]initWithRootViewController:vc];
         self.window.rootViewController = _navController;
         [self.window makeKeyAndVisible];
        
        
       

    }
    */
    
   
   

    return YES;
}

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
    
    NSLog(@"hooray this works");
    
}




#pragma mark - SignInAPI

-(void)callSignInAPIService{
    SignInService *signInService = [[SignInService alloc]init];
    [signInService callSignInService:self.criteria callback:^(NSError *error, SignInResponse *objSignInResponse) {
    if(error)return ;
        
    else{
    if([objSignInResponse.status isEqualToString:@"success"]){
        [Constant saveUserCoinBalance:objSignInResponse.userLoyaltyPoints];
        [Constant saveUserWalletBalance:objSignInResponse.userWalletBalance];
    [self loginDone];        
    }
    
    if([objSignInResponse.status isEqualToString:@"failure"]){
   // [self loginDone];
        
   
        
        if([Constant getUserLoginFailedStatus] == FALSE){
            [Constant saveUserLoginFailedStatus:TRUE];
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Pay1" message:objSignInResponse.signinDesc delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            
            [alert show];
        }
        
        [self doLogout];
        
    }
        
    }
    }];
}

-(SignInCriteria *)criteria{
    SignInCriteria *objCriteria = [[SignInCriteria alloc]init];
    objCriteria.userMobNum = [Constant getUserPhoneNumber];
    objCriteria.password = [Constant getUserPass];
    
    
    NSString *strUDID = [[UIDevice currentDevice] uniqueDeviceIdentifier];
    NSLog(@"value is------>%@",strUDID);
    objCriteria.udid = strUDID;
    
    CLLocation *location = [CurrentLocation sharedLocation].currentLocation;
    objCriteria.userLat = [NSString stringWithFormat:@"%f",location.coordinate.latitude];
    objCriteria.userLong = [NSString stringWithFormat:@"%f",location.coordinate.longitude];
    
    NSLog(@"user lat and long :%@ %@",objCriteria.userLat,objCriteria.userLong);
    objCriteria.deviceType = @"iOS";
    
    return objCriteria;
}

-(void)loginDone{
  [self.tabBarController setSelectedIndex:0];
    
    
    [Constant saveHomeRefreshStatus:TRUE];
    
    if(![Session shared].isFromAutoLogin){
    [self.window setRootViewController:self.tabBarController];
    }
    
    else{
    if([self.tabBarController.viewControllers count]>0) {
        UINavigationController *nc = [self.tabBarController.viewControllers objectAtIndex:0];
        if([[nc viewControllers] count]>0) {
            [nc popToRootViewControllerAnimated:NO];
            HomeViewController *mvc = [[nc viewControllers] objectAtIndex:0];
            [mvc loginDone];
        }
    }
    
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    CGRect tmpRect=self.navController.view.frame;
    self.navController.view.frame=CGRectMake(-1*tmpRect.size.width,tmpRect.origin.y, tmpRect.size.width,tmpRect.size.height);
    [UIView commitAnimations];
    }
}

-(void)doLogout{
    [Session shared].objSignInResponse = nil;
    [Session shared].isFromAutoLogin = FALSE;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

    ViewController *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"viewC"];
    UINavigationController * navController = [[UINavigationController alloc]initWithRootViewController:vc];
    self.window.rootViewController = navController;
}


-(BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation{
    
    return [FBAppCall handleOpenURL:url sourceApplication:sourceApplication];
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    
   

}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    NSLog(@"#AppEntersBackGround %@ %@ %@",[Constant getLastTimeServicePlan],[Constant getLastTimeMobilePlan],[Constant currentSystemDateTime]);
    
    
    NSDate* date1 = [Constant convertTheDate:[Constant currentSystemDateTime]];
    NSDate* date2 = [Constant convertTheDate:[Constant getLastTimeServicePlan]];
    NSDate* date3 = [Constant convertTheDate:[Constant getLastTimeMobilePlan]];
    
    NSTimeInterval distanceBetweenMobilePlans = [date1 timeIntervalSinceDate:date2];
    NSTimeInterval distanceBetweenMobileDetail = [date1 timeIntervalSinceDate:date3];
    
    
    NSLog(@"distanceBetweenDates :%f",distanceBetweenMobilePlans);
    
    if(distanceBetweenMobilePlans >= 86400){
   // if(distanceBetweenMobilePlans >= 50){
        [self callTheGetPlanService];
    }
    
    if(distanceBetweenMobileDetail >= 172800){
        [self callTheMobilePlanService];
    }
}

-(void)callTheGetPlanService{
    GetPlanService *objService = [[GetPlanService alloc]init];
    [objService callGetPlanService:^(NSError *error, GetPlanResponse *objGetPlanResponse) {
    if(error)return ;
    else{
    [Constant saveLastServicePlanTime:[Constant currentSystemDateTime]];
    NSLog(@"Plans isPerformed");
    [self emptyFirstPlanDB];
    }
        
    }];
}

-(void)emptyFirstPlanDB{
    
    DBManager *  objDBManager = [[DBManager alloc]initWithDatabaseFilename:@"plansdb.sql"];
    
    NSString *query1 = [NSString stringWithFormat:@"drop table plandatatable"];
    
    
    [objDBManager executeQuery:query1];
    
    if (objDBManager.affectedRows != 0) {
        NSLog(@"Query was executed successfully.");
    }
    else{
        NSLog(@"Could not execute the query.");
    }
    
    NSString *query2 = [NSString stringWithFormat:@"alter table tempplantable rename to plandatatable"];
    [objDBManager executeQuery:query2];
    
    if (objDBManager.affectedRows != 0) {
        NSLog(@"Query was executed successfully.");
    }
    else{
        NSLog(@"Could not execute the query.");
    }
    
    
    
}



-(void)callTheMobilePlanService{
    GetPlanService *objService = [[GetPlanService alloc]init];
    [objService callGetMobileService:[Constant getLastTimeMobilePlan] callback:^(NSError *error, GetMobileResponse *objMobileResponse) {
    if(error)return ;
    else{
   // [self saveMobileDetailsInDB:objMobileResponse];
    [Constant saveLastMobilePlanTime:[Constant currentSystemDateTime]];
    if(objMobileResponse.detailsArray.count > 0){
   // [self saveMobileDetailsInDB:objMobileResponse];
    }
    else{
    NSLog(@"#NoNewDataForMobileDetailInfoIsFound");
    }

    }
    }];
}
/*
-(void)saveMobileDetailsInDB:(GetMobileResponse *)mobResponse{
    DBManager* objMobDBManager = [[DBManager alloc]initWithDatabaseFilename:@"mobiledatadb.sql"];
    NSString *query;
    for(int i=0;i<mobResponse.detailsArray.count;i++){
    GetMobileDataList *objDataList = [mobResponse.detailsArray objectAtIndex:i];
    query = [NSString stringWithFormat:@"insert into NumberAndCircleTable values(null,'%@','%@','%@','%@','%@','%@','%@')",objDataList.area,objDataList.areaName,objDataList.operatorID,objDataList.operatorName,objDataList.productID,objDataList.startNumber,[Constant currentSystemDateTime]];
    [objMobDBManager executeQuery:query];
        
    }
    
    if (objMobDBManager.affectedRows != 0) {
        NSLog(@"Query was executed successfully.");
    }
    else{
        NSLog(@"Could not execute the query.");
    }
}*/

- (void)applicationWillEnterForeground:(UIApplication *)application {
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
