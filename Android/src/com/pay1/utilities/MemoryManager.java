package com.pay1.utilities;

import java.io.File;

import android.content.Context;
import android.os.Environment;
import android.os.StatFs;
import android.util.Log;
import android.widget.Toast;

public class MemoryManager {

	public static void clearApplicationData(Context context) {
		File cache = context.getCacheDir();
		Long total = cache.getTotalSpace();
		Long used = cache.getUsableSpace();
		Log.e("TAG", "Total " + total + "\nUsed  " + used + "\nAvail "
				+ (total - used));

		if (used >= 1000000) {
			// Toast.makeText(context, "Clearing....",
			// Toast.LENGTH_LONG).show();
			File appDir = new File(cache.getParent());
			if (appDir.exists()) {
				String[] children = appDir.list();
				for (String s : children) {
					if (!s.equals("lib") && !s.equals("shared_prefs")
							&& !s.equals("files") && !s.equals("databases")) {
						deleteDir(new File(appDir, s));
						Log.e("TAG",
								"**************** File /data/data/APP_PACKAGE/"
										+ s + " DELETED *******************");
					}
				}
			}
			// Toast.makeText(context, "Done", Toast.LENGTH_LONG).show();
		}
	}

	public static boolean deleteDir(File dir) {
		if (dir != null && dir.isDirectory()) {
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++) {
				boolean success = deleteDir(new File(dir, children[i]));
				if (!success) {
					return false;
				}
			}
		}

		return dir.delete();
	}

	public static long getFreeMemory() {
		File path = Environment.getDataDirectory();
		StatFs stat = new StatFs(path.getPath());
		long blockSize = stat.getBlockSize();
		long availableBlocks = stat.getAvailableBlocks();
		return availableBlocks * blockSize;
	}

	public static long getTotalMemory() {
		File path = Environment.getDataDirectory();
		StatFs stat = new StatFs(path.getPath());
		long blockSize = stat.getBlockSize();
		long availableBlocks = stat.getBlockCount();
		return availableBlocks * blockSize;
	}
}
