package com.pay1.adapterhandler;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.pay1.MainPagerFragment;

public class RegistrationPagerAdapter extends FragmentStatePagerAdapter

{

	public RegistrationPagerAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int position) {
		//
		switch (position) {
		case 0:
			return MainPagerFragment.newInstance("1",
					"");
		case 1:
			return MainPagerFragment
					.newInstance("2",
							"Locate\nPay1 store near you\nfor Cash Topup");
		case 2:
			return MainPagerFragment
					.newInstance("3",
							"Recharge\nPay Bills, DTH\n& Datacards");
			
		case 3:
			return MainPagerFragment
					.newInstance("4",
							"Collect Coins\nafter every Recharge");
			
		case 4:
			return MainPagerFragment
					.newInstance("5",
							"Grab a Gift\nwith your Gift Coins");
		default:
			return MainPagerFragment.newInstance("Simple & Assured Recharges",
					"Recharge\nPay Bills, DTH\n& Datacards");
		}
	}

	@Override
	public int getCount() {
		return 5;
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return null;
	}

	// @Override
	// public int getIconResId(int index) {
	// return ICONS[index % ICONS.length];
	// }

	// public void setCount(int count) {
	// if (count > 0 && count <= 10) {
	// mCount = count;
	// notifyDataSetChanged();
	// }
	// }
}
