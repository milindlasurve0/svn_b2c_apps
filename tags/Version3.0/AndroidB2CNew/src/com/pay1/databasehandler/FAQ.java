package com.pay1.databasehandler;

public class FAQ {

	private int faqID;
	private String faqQuestion;
	private String faqAnswer;
	private long faqUptateTime;

	public int getFaqID() {
		return faqID;
	}

	public void setFaqID(int faqID) {
		this.faqID = faqID;
	}

	public String getFaqQuestion() {
		return faqQuestion;
	}

	public void setFaqQuestion(String faqQuestion) {
		this.faqQuestion = faqQuestion;
	}

	public String getFaqAnswer() {
		return faqAnswer;
	}

	public void setFaqAnswer(String faqAnswer) {
		this.faqAnswer = faqAnswer;
	}

	public long getFaqUptateTime() {
		return faqUptateTime;
	}

	public void setFaqUptateTime(long faqUptateTime) {
		this.faqUptateTime = faqUptateTime;
	}
}
