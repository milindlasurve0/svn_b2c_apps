﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Threading;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using SQLiteClient;

using NewPay1B2C.Model;
using Microsoft.Phone.Info;
using System.ComponentModel;
using System.Collections.ObjectModel;

namespace NewPay1B2C
	{
	public partial class EnterPin : PhoneApplicationPage
		{
		ProgressBarManager ProgressBarManager;
		SQLiteConnection db = null;
		List<OperatorDB> queryResult = null;
		public EnterPin ()
			{
			InitializeComponent ();
			timer1.Interval = new TimeSpan (0, 0, 0, 1);
			timer1.Tick += new EventHandler (timer1_Tick);

			CreateDatabase ();
			GoogleAnalytics.EasyTracker.GetTracker ().SendView ("EnterPin");
			ProgressIndicator prog = new ProgressIndicator ();
			ProgressBarManager = new ProgressBarManager ();
			SystemTray.SetProgressIndicator (this, prog);
			ProgressBarManager.Hook (prog);
			btnOtp.Tap += btnRegister_Click;
			btnResendOtp.Tap += btnResendOtp_Click;
			
			textUserMobileNumber.Text = Constant.getUserNumber ();
			if (Constant.LoadPersistent<Boolean> (Constant.SETTINGS_IS_NEW))
				{
				textOTP.Hint = "OTP";
				Countdown.Visibility = Visibility.Visible;
				timer1.Start ();
				textNewOrOldMsg.Text = "You will receive one time passward (OTP) via SMS. Kindly enter below";
				}
			else
				{
				textOTP.Hint = "Password";
				Countdown.Visibility = Visibility.Collapsed;
				btnResendOtp.Visibility = Visibility.Visible;
				btnResendOtp.Text = "Forgot Password";
				textNewOrOldMsg.Text = "You are a registered user, kindly enter your password below";
				}
			
				StartLoadingData ();

			}


		private void StartLoadingData ()
			{
			BackgroundWorker backroungWorker = new BackgroundWorker ();
			backroungWorker.DoWork += new DoWorkEventHandler (backroungWorker_DoWork);
			//backroungWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(backroungWorker_RunWorkerCompleted);

			backroungWorker.RunWorkerAsync ();
			}

		private void backroungWorker_DoWork (object sender, DoWorkEventArgs e)
			{
			getDealsTask (HomePage.latti,HomePage.longi);
			try
				{

				SQLiteCommand cmd1 = db.CreateCommand ("SELECT *  FROM OperatorTable");
				var lst = cmd1.ExecuteQuery<OperatorDB> ();
				List<OperatorDB> queryResult = lst.ToList ();
				if (queryResult.Count <= 1)
					{
					DateTime currentDateOp = DateTime.Now;//.ToString();
					DateTime updatedDateOp = Constant.LoadPersistent<DateTime> (Constant.OPERATOR_LIST_UPDATE_TIME);
					TimeSpan durationOp = currentDateOp - updatedDateOp;
					if (durationOp.Days >= 1)
						{
						SQLiteCommand cmd2 = db.CreateCommand ("DELETE FROM OperatorTable");
						int i = cmd2.ExecuteNonQuery ();
						GetAndSaveOperator ();
						}
					}
				}
			catch (SQLiteException se)
				{
				string see = se.Message;
				}


			string strSelect = "SELECT * FROM NumberAndCircle";

			ObservableCollection<NumberAndCircle> result1 = (Application.Current as App).dbh.SelectObservableCollection<NumberAndCircle> (strSelect);
			


			/*SQLiteCommand cmd11 = db.CreateCommand ("SELECT * FROM NumberAndCircle");
			var lst1 = cmd11.ExecuteQuery<NumberAndCircle> ();
			List<NumberAndCircle> result1 = lst1.ToList ();	   */
			if (result1.Count >= 1)
				{
				DateTime CurrentTime = DateTime.Now;
				DateTime EndTime = Constant.LoadPersistent<DateTime> (Constant.CIRCLE_INSERT_TIME);
				if (EndTime != null)
					{
					TimeSpan ts = EndTime - CurrentTime;

					if (ts.Days > 1)
						{
						if (Constant.checkInternetConnection ())
							{
							getAllCircles ();
							}
						}
					}
				}
			else
				{
				if (Constant.checkInternetConnection ())
					{
					getAllCircles ();
					}
				}
			}



		private async void getAllCircles ()
			{
			DateTime lastStorageCircle = Constant.LoadPersistent<DateTime> (Constant.CIRCLE_INSERT_TIME);
			// Use current time
			string format = "yyyy-MM-dd HH:mm:ss";    // Use this format
			Console.WriteLine (lastStorageCircle.ToString (format));
			String lastUrl = "https://panel.pay1.in/apis/receiveWeb/mindsarray/mindsarray/json?method=getMobileDetails&mobile=all&timestamp=";// +lastStorageCircle.ToString (format);
			var httpClient = new HttpClient (new HttpClientHandler ());
			HttpResponseMessage response = await httpClient.PostAsync (lastUrl, null);
			response.EnsureSuccessStatusCode ();
			long cl = (long)response.Content.Headers.ContentLength;
			var responseString = await response.Content.ReadAsStringAsync ();
			int count = 0;
			try
				{
				db.Open ();


				string output = responseString.Remove (responseString.Length - 1, 1);
				string output1 = output.Remove (output.Length - 1, 1);
				string output2 = output1.Remove (0, 1);
				string output3 = output2.Remove (0, 1);
				string output4 = output3.Remove (output3.Length - 1, 1);
				NumberAndCircle numberAndCircle = new NumberAndCircle ();
				var dict = (JObject)JsonConvert.DeserializeObject (output4);
				string status = dict["status"].ToString ();
				int inseted = 0;
				var details = dict["details"];
				foreach (var item in details)
					{
					inseted = inseted + 1;
					numberAndCircle.area = item["area"].ToString ();
					numberAndCircle.area_name = item["area_name"].ToString ();
					numberAndCircle.number = item["number"].ToString ();
					numberAndCircle.numberOperator = item["operator"].ToString ();
					numberAndCircle.opr_name = item["opr_name"].ToString ();
					numberAndCircle.product_id = item["product_id"].ToString ();
					try
						{

						string strInsert = " Insert into NumberAndCircle (area_name,area,opr_name,numberOperator,number,product_id) values (@area_name,@area,@opr_name,@numberOperator,@number,@product_id)";
						int rec = (Application.Current as App).dbh.Insert<NumberAndCircle> (numberAndCircle, strInsert);


						//SQLiteCommand cmd = db.CreateCommand ("");
						//cmd.CommandText = " Insert into NumberAndCircle (area_name,area,opr_name,numberOperator,number,product_id) values (@area_name,@area,@opr_name,@numberOperator,@number,@product_id)";
						//int rec = cmd.ExecuteNonQuery (numberAndCircle);
						//System.Diagnostics.Debug.WriteLine("Inserted  Circle  " + inseted);
						}
					catch (Exception ex)
						{
						string ms = ex.Message;
						}
					}

				Constant.SavePersistent (Constant.CIRCLE_INSERT_TIME, DateTime.Now);
				System.Diagnostics.Debug.WriteLine ("Inserted  Circle  " + inseted);

				}
			catch (JsonException je)
				{
				}
			}



		private async void getDealsTask (string lattitude, string longitude)
			{

			var values = new List<KeyValuePair<string, string>> ();
			values.Add (new KeyValuePair<string, string> ("actiontype", "Get_all_deal_data"));
			values.Add (new KeyValuePair<string, string> ("longitude", Constant.LoadPersistent<string>(Constant.SETTINGS_USER_LONGITUDE)));
			values.Add (new KeyValuePair<string, string> ("latitude", Constant.LoadPersistent<string> (Constant.SETTINGS_USER_LATTITUDE)));
			values.Add (new KeyValuePair<string, string> ("mobile", Constant.getUserNumber ()));
			values.Add (new KeyValuePair<string, string> ("api", "true"));
			FormUrlEncodedContent values1 = new FormUrlEncodedContent (values);
			Task<string> response = Constant.postParamsAndgetResponse (Constant.B2C_URL, values1);

			string result = await response;
			if (string.IsNullOrEmpty (result))
				{
				NavigationService.Navigate (new Uri ("/NoConnection.xaml?", UriKind.Relative));
				}
			else
				{
				var dict = (JObject)JsonConvert.DeserializeObject (result);
				string status = dict["status"].ToString ();

				if (status.Equals ("success"))
					{
					if (db != null)
						{
						try
							{
							SQLiteCommand cmd = db.CreateCommand ("");
							SQLiteCommand cmd2 = db.CreateCommand ("DELETE FROM FreeBieDataBase");
							int i = cmd2.ExecuteNonQuery ();
							FreeBieDataBase freebie = new FreeBieDataBase ();
							freebie.freeBie = dict["description"].ToString ();
							cmd.CommandText = "Insert into FreeBieDataBase (freeBie) values (@freeBie)";
							int rec = cmd.ExecuteNonQuery (freebie);
							Constant.SavePersistent (Constant.FREEBIE_LIST_UPDATE_TIME, DateTime.Now);
							}
						catch (SQLiteException exc)
							{
							System.Diagnostics.Debug.WriteLine (exc.Message);
							}
						}
					}
				else
					{
					Constant.checkForErrorCode (result);
					}
				}
			}

		private void CreateDatabase ()
			{
			if (db == null)
				{
				db = new SQLiteConnection (Constant.DATABASE_NAME);
				db.Open ();
				try
					{
					SQLiteCommand cmd = db.CreateCommand ("Create table OperatorTable(id text,name text,code text,p_id text UNIQUE,stv text,flag text,service_id text,delegateFlag text,min text,max text,charges_slab text,service_charge_amount text,service_tax_percent text,service_charge_percent text,prefix text,length text)");
					int i = cmd.ExecuteNonQuery ();
					}
				catch (SQLiteException ex)
					{

					}
				try
					{
					SQLiteCommand cmd1 = db.CreateCommand ("Create table Plans(planAmount int,planOpertorID text,planOperatorName text,planCircleID text,planCircleName text,planType text,planValidity text,planDescription text,planUptateTime datetime)");
					int i1 = cmd1.ExecuteNonQuery ();
					}
				catch (SQLiteException ex)
					{

					}
				try
					{
					SQLiteCommand cmd2 = db.CreateCommand ("Create table DthPlans (dthPlanAmount int,dthPlanOpertorID text,dthPlanOperatorName text,dthPlanCircleID text,dthPlanCircleName text,dthPlanType text,dthPlanValidity text,dthPlanDescription text,dthPlanUptateTime datetime)");
					int i2 = cmd2.ExecuteNonQuery ();
					}
				catch (SQLiteException ex)
					{

					}
				try
					{
					SQLiteCommand cmdFreeBie = db.CreateCommand ("Create table FreeBieDataBase (freeBie text)");
					int i3 = cmdFreeBie.ExecuteNonQuery ();
					}
				catch (SQLiteException ex)
					{

					}


				try
					{
					SQLiteCommand cmd = db.CreateCommand ("Create table NumberAndCircle (area_name text,area text,opr_name text,numberOperator text,number text,product_id text)");
					int i = cmd.ExecuteNonQuery ();
					}
				catch (Exception ee)
					{
					System.Diagnostics.Debug.WriteLine (ee.Message);
					}



				}
			}


		DispatcherTimer timer1 = new DispatcherTimer ();

		int tik = 178;
		void timer1_Tick (object sender, EventArgs e)
			{
			int hours = (int)(tik / 3600);
			int minutes = (int)(tik / 60) % 60;
			int seconds = (int)tik % 60;
			string sec, min;
			if (seconds < 10)
				{
				sec = "0" + seconds;
				}
			else
				{
				sec = "" + seconds;
				}
			if (minutes < 10)
				{
				min = "0" + minutes;
				}
			else
				{
				min = "" + minutes;
				}

			string res = min + ":" + sec;

			Countdown.Text = res + " Seconds Remaining";
			if (tik > 0)
				tik--;
			else
				{

				Countdown.Text = "00:00";
				Countdown.Text = "Looks like there is some problem in sending SMS, try re-sending it.";
				btnResendOtp.Visibility = Visibility.Visible;
				}
			}

		private void btnRegister_Click (object sender, RoutedEventArgs e)
			{
			String strOtp = textOTP.Text;
			String hashNumber = Constant.EncodeString (Constant.getUserNumber ());
			String hashOtp = Constant.EncodeString (strOtp);
			String serverOtp = Constant.getOtpHash ();
			if ((hashNumber + hashOtp).Equals (serverOtp))
				{
				Constant.setOtp (strOtp);
				Constant.SavePersistent (Constant.SETTINGS_IS_FIRST, true);
				if (Constant.LoadPersistent<Boolean> (Constant.SETTINGS_IS_NEW))
					{
					UpdatePasswordTask (strOtp);
					}
				else
					{
					autoLogin (strOtp);
					}

				NavigationService.Navigate (new Uri ("/HomePage.xaml?", UriKind.RelativeOrAbsolute));
				NavigationService.RemoveBackEntry ();
				}
			else
				{
				MessageBox.Show ("failed");
				}
			}

		private async void UpdatePasswordTask (string strOtp)
			{
			var values = new List<KeyValuePair<string, string>>
                        { 
                        new KeyValuePair<string, string>("actiontype", "update_password"),
                        new KeyValuePair<string, string>("mobile_number",Constant.getUserNumber()),
                        new KeyValuePair<string, string>("password", strOtp),
                        new KeyValuePair<string, string>("confirm_password",strOtp),
                        new KeyValuePair<string, string>("code", strOtp),
                        
                       new KeyValuePair<string, string>("api","true"),
                        };

			FormUrlEncodedContent values1 = new FormUrlEncodedContent (values);
			Task<string> response = Constant.postParamsAndgetResponse (Constant.B2C_URL, values1);
			string result = await response;
			if (string.IsNullOrEmpty (result))
				{
				NavigationService.Navigate (new Uri ("/NoConnection.xaml?", UriKind.Relative));
				}
			else
				{
				var dict = (JObject)JsonConvert.DeserializeObject (result);
				string status = dict["status"].ToString ();
				if (status.Equals ("success"))
					{
					autoLogin (strOtp);
					}
				else
					{
					Constant.checkForErrorCode (result);
					}
				} 
			}


		private async void autoLogin (String strOtp)
			{
			ProgressBarManager.Show (11, "Please wait...", false, 10);
			PhoneApplicationService.Current.State["loginCall"] = 1;
			var values = new List<KeyValuePair<string, string>>
                        { 
                        new KeyValuePair<string, string>("actiontype", "signin"),
                        new KeyValuePair<string, string>("username",Constant.getUserNumber()),
                        new KeyValuePair<string, string>("password",strOtp),
                        new KeyValuePair<string, string>("uuid", Constant.getUUID()),
                        new KeyValuePair<string, string>("latitude",Constant.LoadPersistent<string>(Constant.SETTINGS_USER_LATTITUDE)),
                        new KeyValuePair<string, string>("longitude",Constant.LoadPersistent<string>(Constant.SETTINGS_USER_LONGITUDE)),
                        new KeyValuePair<string, string>("device_type","windows"),
                        new KeyValuePair<string, string>("manufacturer",DeviceStatus.DeviceManufacturer),
                        new KeyValuePair<string, string>("os_info",Environment.OSVersion.Version.ToString()),
                        new KeyValuePair<string, string>("gcm_reg_id",Constant.LoadPersistent<string>(Constant.NOTIFICATION_URL)),
                        new KeyValuePair<string, string>("api","true"),
                        };

			FormUrlEncodedContent values1 = new FormUrlEncodedContent (values);
			Task<string> response = Constant.postParamsAndgetResponse (Constant.B2C_URL, values1);

			string result = await response;
			if (string.IsNullOrEmpty (result))
				{
				NavigationService.Navigate (new Uri ("/NoConnection.xaml?", UriKind.Relative));
				}
			else
				{
				var dict = (JObject)JsonConvert.DeserializeObject (result);
				string status = dict["status"].ToString ();
				if (status.Equals ("success"))
					{
					var description = dict["description"];
					Constant.SavePersistent (Constant.SETTINGS_USER_NAME, description["name"].ToString ());
					Constant.SavePersistent (Constant.SETTINGS_USER_PIN, strOtp);
					Constant.setUserNumber (description["mobile"].ToString ());
					Constant.SavePersistent (Constant.SETTINGS_USER_GENDER, description["gender"].ToString ());
					Constant.SavePersistent (Constant.SETTINGS_USER_EMAIL, description["email"].ToString ());
					Constant.SavePersistent (Constant.SETTINGS_USER_ID, description["user_id"].ToString ());
					Constant.SavePersistent (Constant.SETTINGS_USER_DOB, description["date_of_birth"].ToString ());
					Constant.setbalance (description["wallet_balance"].ToString ());
					Constant.SavePersistent (Constant.SETTINGS_USER_LATTITUDE, description["latitude"].ToString ());
					Constant.SavePersistent (Constant.SETTINGS_USER_LONGITUDE, description["longitude"].ToString ());
					Constant.SavePersistent (Constant.SETTINGS_USER_SUPPORT_NUMBER, description["support_number"].ToString ());
					Constant.SavePersistent (Constant.SETTINGS_IS_LOGIN, true);
					//NavigationService.Navigate (new Uri ("/HomePage.xaml?", UriKind.RelativeOrAbsolute));

					//NavigationService.RemoveBackEntry ();
					}
				else
					{
					Constant.checkForErrorCode (result);
					}

				}
			ProgressBarManager.Hide (11);
			}




		private async void RegisterTask ()
			{
			// ProgressBarManager.Show(11, "Please wait...", false, 10);
			var values = new List<KeyValuePair<string, string>>
                        { 
                        new KeyValuePair<string, string>("actiontype", "create_user"),
                        new KeyValuePair<string, string>("mobile_number",Constant.getUserNumber()),
                       new KeyValuePair<string, string>("api","true"),
                        };

			FormUrlEncodedContent values1 = new FormUrlEncodedContent (values);
			Task<string> response = Constant.postParamsAndgetResponse (Constant.B2C_URL, values1);
			string result = await response;
			if (string.IsNullOrEmpty (result))
				{
				NavigationService.Navigate (new Uri ("/NoConnection.xaml?", UriKind.Relative));
				}
			else
				{
				var dict = (JObject)JsonConvert.DeserializeObject (result);
				string status = dict["status"].ToString ();
				if (status.Equals ("success"))
					{
					var description = dict["description"];
					Constant.setOtpHash (description["otp"].ToString ());
					Constant.setUserNumber (Constant.getUserNumber ());
					Constant.setbalance (description["balance"].ToString ());
					NavigationService.Navigate (new Uri ("/EnterPin.xaml?", UriKind.RelativeOrAbsolute));
					NavigationService.RemoveBackEntry ();
					}
				else if (status.Equals ("failure"))
					{
					String errCode = dict["errCode"].ToString ();
					var description = dict["description"];
					if (errCode.Equals ("202"))
						{
						Constant.setOtpHash (description["otp"].ToString ());
						Constant.setUserNumber (Constant.getUserNumber ());
						Constant.setbalance (description["balance"].ToString ());
						NavigationService.Navigate (new Uri ("/EnterPin.xaml?", UriKind.RelativeOrAbsolute));
						NavigationService.RemoveBackEntry ();
						}
					else
						{
						}
					}
				}
			}

		private void btnResendOtp_Click (object sender, RoutedEventArgs e)
			{
			
			if (Constant.LoadPersistent<Boolean> (Constant.SETTINGS_IS_NEW))
				{
				RegisterTask ();
				}
			else
				{
				ForgotPasswordTask ();
				}

			}

		private async void ForgotPasswordTask ()
			{
			var values = new List<KeyValuePair<string, string>>
                        { 
                        new KeyValuePair<string, string>("actiontype", "forgotpwd"),
                        new KeyValuePair<string, string>("user_name",Constant.getUserNumber()),
                        new KeyValuePair<string, string>("vtype", "1"),
						new KeyValuePair<string, string>("api","true"),
                        };

			FormUrlEncodedContent values1 = new FormUrlEncodedContent (values);
			Task<string> response = Constant.postParamsAndgetResponse (Constant.B2C_URL, values1);
			string result = await response;
			if (string.IsNullOrEmpty (result))
				{
				NavigationService.Navigate (new Uri ("/NoConnection.xaml?", UriKind.RelativeOrAbsolute));
				}
			else
				{
				var dict = (JObject)JsonConvert.DeserializeObject (result);
				string status = dict["status"].ToString ();
				if (status.Equals ("success"))
					{
					PhoneApplicationService.Current.State["updateFromLogin"] = 1;
					NavigationService.Navigate (new Uri ("/UpdatePassword.xaml?", UriKind.RelativeOrAbsolute));
					}
				else
					{
					Constant.checkForErrorCode (result);
					}
				}
			}



		private async void GetAndSaveOperator ()
			{
			//ProgressBarManager.Show(10, "Please wait...", false, 10);
			var values = new List<KeyValuePair<string, string>>
                        { 
                        new KeyValuePair<string, string>("actiontype", "get_all_operators"),
                        new KeyValuePair<string, string>("api","true"),
                        };

			FormUrlEncodedContent values1 = new FormUrlEncodedContent (values);
			Task<string> response = Constant.postParamsAndgetResponse (Constant.B2C_URL, values1);

			string result = await response;
			var dict = (JObject)JsonConvert.DeserializeObject (result);
			string status = dict["status"].ToString ();
			if (status.Equals ("success"))
				{
				Constant.SavePersistent (Constant.OPERATOR_LIST_UPDATE_TIME, DateTime.Now);
				var description = dict["description"];
				var mobile = description["mobile"];
				foreach (var item in mobile)
					{
					OperatorDB opeDb = new OperatorDB ();
					opeDb.id = item["id"].ToString ();
					opeDb.code = item["opr_code"].ToString ();
					opeDb.delegateFlag = item["delegate"].ToString ();
					opeDb.flag = item["flag"].ToString ();
					opeDb.name = item["name"].ToString ();
					opeDb.p_id = item["product_id"].ToString ();
					opeDb.service_id = item["service_id"].ToString ();
					opeDb.stv = item["stv"].ToString ();
					opeDb.prefix = item["prefix"].ToString ();
					opeDb.length = item["length"].ToString ();
					opeDb.min = item["min"].ToString ();
					opeDb.max = item["max"].ToString ();
					opeDb.charges_slab = item["charges_slab"].ToString ();
					opeDb.service_charge_amount = item["service_charge_amount"].ToString ();
					opeDb.service_tax_percent = item["service_tax_percent"].ToString ();
					opeDb.service_charge_percent = item["service_charge_percent"].ToString ();
					insertOperatorToDb (opeDb);
					}
				var data = description["data"];
				foreach (var item in data)
					{
					OperatorDB opeDb = new OperatorDB ();
					opeDb.id = item["id"].ToString ();
					opeDb.code = item["opr_code"].ToString ();
					opeDb.delegateFlag = item["delegate"].ToString ();
					opeDb.flag = item["flag"].ToString ();
					opeDb.name = item["name"].ToString ();
					opeDb.p_id = item["product_id"].ToString ();
					opeDb.service_id = item["service_id"].ToString ();
					opeDb.stv = item["stv"].ToString ();
					opeDb.prefix = item["prefix"].ToString ();
					opeDb.length = item["length"].ToString ();
					opeDb.min = item["min"].ToString ();
					opeDb.max = item["max"].ToString ();
					opeDb.charges_slab = item["charges_slab"].ToString ();
					opeDb.service_charge_amount = item["service_charge_amount"].ToString ();
					opeDb.service_tax_percent = item["service_tax_percent"].ToString ();
					opeDb.service_charge_percent = item["service_charge_percent"].ToString ();
					insertOperatorToDb (opeDb);
					}
				var postpaid = description["postpaid"];
				foreach (var item in postpaid)
					{
					OperatorDB opeDb = new OperatorDB ();
					opeDb.id = item["id"].ToString ();
					opeDb.code = item["opr_code"].ToString ();
					opeDb.delegateFlag = item["delegate"].ToString ();
					opeDb.flag = item["flag"].ToString ();
					opeDb.name = item["name"].ToString ();
					opeDb.p_id = item["product_id"].ToString ();
					opeDb.service_id = item["service_id"].ToString ();
					opeDb.stv = item["stv"].ToString ();
					opeDb.prefix = item["prefix"].ToString ();
					opeDb.length = item["length"].ToString ();
					opeDb.min = item["min"].ToString ();
					opeDb.max = item["max"].ToString ();
					opeDb.charges_slab = item["charges_slab"].ToString ();
					opeDb.service_charge_amount = item["service_charge_amount"].ToString ();
					opeDb.service_tax_percent = item["service_tax_percent"].ToString ();
					opeDb.service_charge_percent = item["service_charge_percent"].ToString ();
					insertOperatorToDb (opeDb);
					}
				var dth = description["dth"];
				foreach (var item in dth)
					{
					OperatorDB opeDb = new OperatorDB ();
					opeDb.id = item["id"].ToString ();
					opeDb.code = item["opr_code"].ToString ();
					opeDb.delegateFlag = item["delegate"].ToString ();
					opeDb.flag = item["flag"].ToString ();
					opeDb.name = item["name"].ToString ();
					opeDb.p_id = item["product_id"].ToString ();
					opeDb.service_id = item["service_id"].ToString ();
					opeDb.stv = item["stv"].ToString ();
					opeDb.prefix = item["prefix"].ToString ();
					opeDb.length = item["length"].ToString ();
					opeDb.min = item["min"].ToString ();
					opeDb.max = item["max"].ToString ();
					opeDb.charges_slab = item["charges_slab"].ToString ();
					opeDb.service_charge_amount = item["service_charge_amount"].ToString ();
					opeDb.service_tax_percent = item["service_tax_percent"].ToString ();
					opeDb.service_charge_percent = item["service_charge_percent"].ToString ();
					insertOperatorToDb (opeDb);
					}
				}
			// ProgressBarManager.Hide(10);
			}

		public void insertOperatorToDb (OperatorDB opeDb)
			{
			if (db != null)
				{
				try
					{
					SQLiteCommand cmd = db.CreateCommand ("");

					cmd.CommandText = "Insert into OperatorTable (id,name,code,p_id,stv,flag,service_id,delegateFlag,min,max,charges_slab,service_charge_amount,service_tax_percent,service_charge_percent,prefix,length) values (@id,@name,@code,@p_id,@stv,@flag,@service_id,@delegateFlag,@min,@max,@charges_slab,@service_charge_amount,@service_tax_percent,@service_charge_percent,@prefix,@length)";
					int rec = cmd.ExecuteNonQuery (opeDb);
					}
				catch (SQLiteException exc)
					{
					System.Diagnostics.Debug.WriteLine (exc.Message);
					}
				}
			}

		private void editEmailClick (object sender, RoutedEventArgs e)
			{
			NavigationService.Navigate (new Uri ("/RegistrationLayout.xaml?", UriKind.RelativeOrAbsolute));
			NavigationService.RemoveBackEntry ();
			}

		}
	}