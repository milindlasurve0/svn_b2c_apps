﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Media;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using NewPay1B2C.UserControls;
using NewPay1B2C.UtilityClass;

namespace NewPay1B2C
{
    public partial class MyTransaction : PhoneApplicationPage
    {
        List<string> purchaseList;
        ProgressBarManager ProgressBarManager;
        public MyTransaction()
        {
            InitializeComponent();
            GoogleAnalytics.EasyTracker.GetTracker ().SendView ("My Transactions");
			
            Constant.laodNavigationBar(TitlePanel, LayoutRoot, canvas,"My Transactions");
            purchaseList = new List<string>();
            Constant.getAllContacts ();
            ProgressIndicator prog = new ProgressIndicator();
            ProgressBarManager = new ProgressBarManager();
            SystemTray.SetProgressIndicator(this, prog);
            ProgressBarManager.Hook(prog);
            getPurchaseTask(0);


        }
		protected override void OnBackKeyPress (System.ComponentModel.CancelEventArgs e)
			{
			//Do your work here
			base.OnBackKeyPress (e);
			NavigationService.Navigate (new Uri ("/HomePage.xaml?", UriKind.RelativeOrAbsolute));
			while (this.NavigationService.BackStack.Any ())
				{
				this.NavigationService.RemoveBackEntry ();
				}
			}
        private async void getPurchaseTask(int page)
        {
            ProgressBarManager.Show(19, "Please wait...", false, 10);
            if (purchaseListBox.Items.Count > 0)
            {
                purchaseListBox.Items.RemoveAt(purchaseListBox.Items.Count - 1);
            }

            var values = new List<KeyValuePair<string, string>>();
            values.Add(new KeyValuePair<string, string>("actiontype", "purchase_history"));
            values.Add(new KeyValuePair<string, string>("page", Convert.ToString(page)));
            values.Add(new KeyValuePair<string, string>("limit", "10"));
            values.Add(new KeyValuePair<string, string>("api", "true"));
            FormUrlEncodedContent values1 = new FormUrlEncodedContent(values);
            Task<string> response = Constant.postParamsAndgetResponse(Constant.B2C_URL, values1);

            string result = await response;
            if (string.IsNullOrEmpty(result))
            {
                NavigationService.Navigate(new Uri("/NoConnection.xaml?", UriKind.Relative));
            }
            else
            {
                var dict = (JObject)JsonConvert.DeserializeObject(result);
                string status = dict["status"].ToString();

                if (status.Equals("success"))
                {
                    var description = dict["description"];

                    foreach (var item in description)
                    {
                        purchaseList.Add(item.ToString());
                        PurchaseHistoryControl purchaseControl = new PurchaseHistoryControl();
                        string service_id = item["service_id"].ToString();
                        string transStatus = item["status"].ToString();
                        string recharge_flag = item["recharge_flag"].ToString();
                        purchaseControl.textAmount.Text = "Rs. " + item["transaction_amount"].ToString();
                        purchaseControl.textContactNumber.Text = item["number"].ToString();
                        purchaseControl.textContactName.Text = Constant.getContactName(item["number"].ToString());


                     //  String operatorImageName= OperatorConstant.getOperatorResource (Convert.ToInt16 (item["product_id"]));

                      /*  if (recharge_flag.Equals("0"))
                        {

                        }
                        else if (recharge_flag.Equals("1"))
                        {
                            purchaseControl.image_recharge_via.Source = new BitmapImage(new Uri("Images/rupee.png", UriKind.Relative));
                        }
                        else if (recharge_flag.Equals("2"))
                        {
                            purchaseControl.image_recharge_via.Source = new BitmapImage(new Uri("/Images/misscallcolor.png", UriKind.Relative));
                        }
                        */

                        if (transStatus.Equals("1"))
                        {
                            purchaseControl.imageStatus.Source = new BitmapImage(new Uri("/Pay1Images/inprocess.png", UriKind.Relative));
                        }
                        else if (transStatus.Equals("2"))
                        {
                        purchaseControl.imageStatus.Source = new BitmapImage (new Uri ("/Pay1Images/ok.png", UriKind.Relative));
                        }
                        else if (transStatus.Equals("3"))
                        {
                        purchaseControl.imageStatus.Source = new BitmapImage (new Uri ("/Pay1Images/cancel.png", UriKind.Relative));
                        }
                        else if (transStatus.Equals("4"))
                        {
                        purchaseControl.imageStatus.Source = new BitmapImage (new Uri ("/Pay1Images/cancel.png", UriKind.Relative));
                        }


                        if (service_id.Equals("99"))
                        {
                           purchaseControl.textOperator.Text = "DTH Recharge";
                        }
                        else if (service_id.Equals("1"))
                        {
                            purchaseControl.textOperator.Text = "Prepaid Recharge";
                        }
                        else if (service_id.Equals("2"))
                        {
                            purchaseControl.textOperator.Text = "DTH Recharge";
                        }
                        else if (service_id.Equals("3"))
                        {
                           
                        }
                        else if (service_id.Equals("4"))
                        {
                            purchaseControl.textOperator.Text = "Postpaid Bill";
                        }
                        else if (service_id.Equals("5"))
                        {
                            purchaseControl.textOperator.Text = "Datacard Recharge";
                        }
                        else if (service_id.Equals("98"))
                        {
                           purchaseControl.textOperator.Text = "Wallet TopUp";
                        
                        }
                        else if (service_id.Equals("0"))
                        {
                            purchaseControl.textOperator.Text = "Wallet TopUp";
                        }
                        //purchaseControl.imageOperator.Source = new BitmapImage (new Uri ("/Pay1Images/" + operatorImageName + ".png", UriKind.RelativeOrAbsolute));
                        purchaseControl.imageOperator.Source = OperatorConstant.getOperatorResource (Convert.ToInt16 (item["product_id"]));
                        purchaseListBox.Items.Add(purchaseControl);
                    }

                    if (purchaseListBox.Items.Count == 0)
                    {
                        emptyViewPanel.Visibility = Visibility.Visible;
                        purchaseListBox.Visibility = Visibility.Collapsed;
                    }
                    else
                    {
                        emptyViewPanel.Visibility = Visibility.Collapsed;
                        purchaseListBox.Visibility = Visibility.Visible;
                    }

                    Button bt = new Button();
                    bt.Content = "Load more history";
                    bt.Foreground = new SolidColorBrush(Colors.White);
                    bt.BorderBrush = new SolidColorBrush(Constant.ConvertStringToColor("#503D97"));
                    bt.Background = new SolidColorBrush(Constant.ConvertStringToColor("#503D97"));
                    bt.Width = 480;
                    int countNumber = description.Count();
                    if (countNumber == 10)
                    {
                        if ((purchaseListBox.Items.Count % 10) == 0)
                        {
                            bt.Click += (s, e) =>
                            {

                                getPurchaseTask(page + 1);
                            };

                            purchaseListBox.Items.Add(bt);
                        }
                    }
                }
                else
					{
					Constant.checkForErrorCode (result);
                }
            }
            ProgressBarManager.Hide(19);
        }
      
        private void purchaseListBox_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (purchaseListBox.SelectedIndex == -1) return;
            NavigationService.Navigate(new Uri("/PurchaseDetails.xaml?", UriKind.Relative));
            PhoneApplicationService.Current.State["PurchaseData"] = purchaseList[purchaseListBox.SelectedIndex];
            //NavigationService.RemoveBackEntry();

            purchaseListBox.SelectedIndex = -1;
        }

    }
}