package com.pay1.adapterhandler;

import java.io.File;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.accounts.Account;
import android.annotation.SuppressLint;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.Context;
import android.content.SyncResult;
import android.database.SQLException;
import android.os.Bundle;
import android.util.Log;

import com.pay1.constants.Constants;
import com.pay1.databasehandler.Pay1SyncDataHelper;
import com.pay1.databasehandler.SyncDatabase;
import com.pay1.databasehandler.SyncDatabaseForFreeGifts;
import com.pay1.requesthandler.RequestClass;
import com.pay1.utilities.Utility;

/**
 * https://developer.android.com/training/sync-adapters/creating-sync-adapter.
 * html
 **/

public class SyncAdapter extends AbstractThreadedSyncAdapter {

	private Context syncContext;
	public static final long TIME_INTERVAL = 24;

	public SyncAdapter(Context context, boolean autoInitialize) {
		super(context, autoInitialize);

		syncContext = context;
	}

	@SuppressLint("NewApi")
	public SyncAdapter(Context context, boolean autoInitialize,
			boolean allowParallelSyncs) {
		super(context, autoInitialize, allowParallelSyncs);

		syncContext = context;
	}

	@Override
	public void onPerformSync(Account account, Bundle extras, String authority,
			ContentProviderClient provider, SyncResult syncResult) {

		long saveTimeLong = Long
				.parseLong(Utility.getLastSyncTime(syncContext));
		long currentTIme = System.currentTimeMillis();
		Log.d("Current Time Interval", "Time Interval " + currentTIme + "");
		Log.d("SSaved Time Interval", "Time Interval " + saveTimeLong + "");
		long difference = currentTIme - saveTimeLong;
		long timeDifHours = difference / (60 * 60 * 1000);
		Log.d("Time Interval", "Time Interval " + timeDifHours + "");
		
		updateFreeGiftData();
		if (timeDifHours > TIME_INTERVAL) {
			//updatePlanAndCircleData();

			Log.d("Inside Time Interval", "Inside Time Interval "
					+ timeDifHours + "");
		} else {
			// updateFreeGiftData();
			Log.d("Outside Time Interval", "Outside Time Interval "
					+ timeDifHours + "");
		}
		
	}

	void updatePlanAndCircleData() {

		long itime = System.currentTimeMillis();
		String iDateTime = Utility.convertMillisToDateTime(
				String.valueOf(itime), "yyyy-MM-dd hh:mm:ss");
		Log.e("Sync Adapter", "starting sync at " + iDateTime);
		boolean dbCloned = false;

		File dbSync = syncContext
				.getDatabasePath(Pay1SyncDataHelper.SYNC_DATABASE);
		if (!dbSync.exists()) {
			SyncDatabase syncDBFirst = new SyncDatabase(syncContext);
			try {
				dbCloned = syncDBFirst.cloneDatabase(
						Pay1SyncDataHelper.DATABASE_NAME,
						Pay1SyncDataHelper.SYNC_DATABASE);
			} catch (Exception ee) {

				ee.printStackTrace();
				Log.d("run", "run");
			}
			try {
				syncDBFirst.open();
			} catch (Exception ee) {

				ee.printStackTrace();
				Log.d("run2", "run2");
			}
			try {
				syncDBFirst.close();
			} catch (Exception ee) {

				ee.printStackTrace();
				Log.d("run3", "run3");
			}
			Log.e("Sync Adapter", "Sync DB Created");
		}
		SyncDatabase syncDB = new SyncDatabase(syncContext);
		SyncDatabaseForFreeGifts syncDBGifts = new SyncDatabaseForFreeGifts(
				syncContext);

		boolean arePlansSaved = false;
		boolean areCirclesSaved = false;

		String lastSyncTime = Utility.getLastSyncTime(syncContext);
		String lastSyncDateTime = Utility.convertMillisToDateTime(lastSyncTime,
				"yyyy-MM-dd hh:mm:ss");

		ArrayList<NameValuePair> plansUpdateParams = new ArrayList<NameValuePair>();
		plansUpdateParams
				.add(new BasicNameValuePair("method", "getPlanDetails"));
		plansUpdateParams.add(new BasicNameValuePair("operator", "all"));
		plansUpdateParams.add(new BasicNameValuePair("circle", "all"));
		plansUpdateParams.add(new BasicNameValuePair("timestamp",
				lastSyncDateTime));

		ArrayList<NameValuePair> circleUpdateParams = new ArrayList<NameValuePair>();
		circleUpdateParams.add(new BasicNameValuePair("method",
				"getMobileDetails"));
		circleUpdateParams.add(new BasicNameValuePair("mobile", "all"));
		circleUpdateParams.add(new BasicNameValuePair("timestamp",
				lastSyncDateTime));
		Log.e("Sync Adapter: Last Sync Time", lastSyncDateTime);
		try {
			Log.d("Sync Adapter Params Sent", "Params Sent "
					+ (Constants.B2B_URL + plansUpdateParams));
			String plansResponseString = RequestClass.getInstance()
					.readPay1B2BRequest(syncContext, Constants.B2B_URL,
							plansUpdateParams);
			String circleResponseString = RequestClass.getInstance()
					.readPay1B2BRequest(syncContext, Constants.B2B_URL,
							circleUpdateParams);
			Log.e("Sync Adapter", "jsons fetched");
			Log.e("Sync Adapter: Plans", plansResponseString);
			Log.e("Sync Adapter: Circle", circleResponseString);
			if (!plansResponseString.startsWith("Error")) {
				String replaced = plansResponseString.replace("(", "")
						.replace(")", "").replace(";", "");
				if (replaced.length() >= 1) {
					replaced = replaced.substring(1, replaced.length() - 1);
					if (replaced.contains("No plans found")) {

					} else {
						if (!dbCloned)
							dbCloned = syncDB.cloneDatabase(
									Pay1SyncDataHelper.DATABASE_NAME,
									Pay1SyncDataHelper.SYNC_DATABASE);
						arePlansSaved = syncDB.savePlansTodataBase(replaced);
						Log.d("replaced", "replaced " + replaced);
						// Gson gson = new Gson();
						// String result = gson.fromJson(responseJson,
						// String.class);

						Log.e("Sync Adapter: Plans Saved",
								String.valueOf(arePlansSaved));
					}
				}
			}

			if (!circleResponseString.startsWith("Error")) {
				String replaced = circleResponseString.replace("(", "")
						.replace(")", "").replace(";", "");
				replaced = replaced.substring(1, replaced.length() - 1);
				JSONObject jsonObject = new JSONObject(replaced);
				String status = jsonObject.getString("status");

				if (status.equalsIgnoreCase("success")) {
					String details = jsonObject.getString("details");
					if (details.length() > 2) {
						if (!dbCloned)
							syncDB.cloneDatabase(
									Pay1SyncDataHelper.DATABASE_NAME,
									Pay1SyncDataHelper.SYNC_DATABASE);
						areCirclesSaved = syncDB
								.saveCircleNumberToDatabase(jsonObject);
						Log.e("Sync Adapter: Circles Saved",
								String.valueOf(areCirclesSaved));
					}
				}
			}

			if (arePlansSaved || areCirclesSaved) {
				Utility.setLastSyncTime(syncContext, String.valueOf(itime));
				syncDB.cloneDatabase(Pay1SyncDataHelper.SYNC_DATABASE,
						Pay1SyncDataHelper.DATABASE_NAME);
				Log.e("Sync Adapter", "Database cloned back");
			}

			long etime = System.currentTimeMillis();
			String currentDateTime = Utility.convertMillisToDateTime(
					String.valueOf(etime), "yyyy-MM-dd hh:mm:ss");
			Log.e("Sync Adapter: Sync Time",
					String.valueOf((etime - itime) / 1000) + " secs at "
							+ currentDateTime);

			// Constants.writeToSDCard(syncContext,
			// "\n"+"Sync Adapter: Sync Time" + String.valueOf((etime -
			// itime)/1000) + " secs at " + currentDateTime+"\n");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void updateFreeGiftData() {
		// TODO Auto-generated method stub
		try {

			boolean dbCloned = false;

			File dbSync = syncContext
					.getDatabasePath(Pay1SyncDataHelper.SYNC_DATABASE_GIFT);
			
		//	if (!dbSync.exists()) {
				SyncDatabaseForFreeGifts syncDBFirst = new SyncDatabaseForFreeGifts(
						syncContext);
				try {
					dbCloned = syncDBFirst.cloneDatabase(
							Pay1SyncDataHelper.DATABASE_NAME_GIFT,
							Pay1SyncDataHelper.SYNC_DATABASE_GIFT);
				} catch (Exception ee) {

					ee.printStackTrace();
					Log.d("run1", "run1");
				}
				try {
					syncDBFirst.open();
				} catch (Exception ee) {

					ee.printStackTrace();
					Log.d("run12", "run12");
				}
				try {
					syncDBFirst.close();
				} catch (Exception ee) {

					ee.printStackTrace();
					Log.d("run13", "run13");
				}
				Log.e("Sync Adapter", "Sync DB Created");
		//}

			Log.d("Inside updateFreeGiftData", "updateFreeGiftData");

			String time = Utility.getSharedPreferences(syncContext,
					Constants.LAST_GIFT_UPDATED) == null ? "" : Utility
					.getSharedPreferences(syncContext,
							Constants.LAST_GIFT_UPDATED);

			if (time != "") {
				try {
					DateFormat dateFormat = new SimpleDateFormat(
							"yyyy-MM-dd HH:mm:ss");
					Date date = new Date(Long.parseLong(time));
					time = dateFormat.format(date);
				} catch (Exception e) {
				}
			}

			String latitude = Utility.getCurrentLatitude(syncContext,
					Constants.SHAREDPREFERENCE_CURRENT_LATITUDE);
			String longitude = Utility.getCurrentLatitude(syncContext,
					Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE);

			String response = RequestClass.getInstance().readPay1B2CRequest(
					syncContext,
					Constants.B2C_URL
							+ "GetupdatedDeal/?latitude="
							+ latitude
							+ "&longitude="
							+ longitude
							+ "&user_mobile="
							+ Utility.getMobileNumber(syncContext,
									Constants.SHAREDPREFERENCE_MOBILE)
							+ "&updatedTime="
							+ URLEncoder.encode(time, "utf-8")
							+ "&next=-1&api_version=3");
			Log.d("Inside updateFreeGiftData", "updateFreeGiftData " + response);
			if (!response.startsWith("Error")) {
				JSONObject jsonObject = new JSONObject(response);
				String status = jsonObject.getString("status");
				if (status.equalsIgnoreCase("success")) {
					SyncDatabaseForFreeGifts syncDBGifts = new SyncDatabaseForFreeGifts(
							syncContext);
					try {

						syncDBGifts.open();
						syncDBGifts.open();

						JSONObject jsonObjectDesc = jsonObject
								.getJSONObject("description");
						Utility.setOrder(syncContext,
								Constants.SHAREDPREFERENCE_GIFT,
								jsonObjectDesc.getString("claimedGifts"));
						Utility.setSharedPreferences(syncContext,
								"claimedGifts",
								jsonObjectDesc.getString("claimedGifts"));
						Utility.setOrder(syncContext,
								Constants.SHAREDPREFERENCE_GIFT_EXPIRED,
								jsonObjectDesc.getString("ExpiredOffers"));
						try {
							syncDBGifts.deleteFreeBies(jsonObjectDesc
									.getString("ExpiredOffers"));
							syncDBGifts.deleteFreeBiesLocation(jsonObjectDesc
									.getString("ExpiredOffers"));
						} catch (Exception e) {

						}

						JSONArray jsonArrayAlldeals = jsonObjectDesc
								.getJSONArray("Alldeals");
						long timestamp = System.currentTimeMillis();

						syncDBGifts
								.createFreeBies(jsonArrayAlldeals, timestamp);

						Utility.setSharedPreferences(syncContext,
								Constants.LAST_GIFT_UPDATED,
								String.valueOf(System.currentTimeMillis()));

						try {
							SyncDatabaseForFreeGifts synDAtaGifts = new SyncDatabaseForFreeGifts(
									syncContext);
							dbCloned = synDAtaGifts.cloneDatabase(
									Pay1SyncDataHelper.SYNC_DATABASE_GIFT,
									Pay1SyncDataHelper.DATABASE_NAME_GIFT);
						} catch (Exception ee) {

							ee.printStackTrace();
							Log.d("run1", "run1");
						}
					} catch (SQLException exception) {
						// TODO: handle exception
						// Log.e("Er ", exception.getMessage());
					} catch (Exception e) {
						// TODO: handle exception
						// Log.e("Er ", e.getMessage());
					} finally {
						syncDBGifts.close();
						syncDBGifts.close();
					}

				}
			}
		} catch (SQLException se) {
			Log.d("SQLException Some Error", "" + se.getMessage());
		} catch (Exception e) {
			// TODO: handle exception
			Log.d("Exception Some Error", "" + e.getMessage());
		}

	}

	@Override
	public void onSyncCanceled() {
		long etime = System.currentTimeMillis();
		String currentDateTime = Utility.convertMillisToDateTime(
				String.valueOf(etime), "yyyy-MM-dd HH:mm:ss");
		Log.e("sync cancelled at", currentDateTime);
	}
}