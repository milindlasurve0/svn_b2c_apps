package com.pay1.databasehandler;

public class FreeBiesLocation {

	private int locationID;
	private int locationDealID;
	private double locationLat;
	private double locationLng;
	private String locationAddress;
	private String locationArea;
	private String locationCity;
	private String locationState;
	private int locationPin;
	private double locationDistance;
	private long locationUpdatedTime;

	/**
	 * @return the locationID
	 */
	public int getLocationID() {
		return locationID;
	}

	/**
	 * @param locationID
	 *            the locationID to set
	 */
	public void setLocationID(int locationID) {
		this.locationID = locationID;
	}

	/**
	 * @return the locationDealID
	 */
	public int getLocationDealID() {
		return locationDealID;
	}

	/**
	 * @param locationDealID
	 *            the locationDealID to set
	 */
	public void setLocationDealID(int locationDealID) {
		this.locationDealID = locationDealID;
	}

	/**
	 * @return the locationLat
	 */
	public double getLocationLat() {
		return locationLat;
	}

	/**
	 * @param locationLat
	 *            the locationLat to set
	 */
	public void setLocationLat(double locationLat) {
		this.locationLat = locationLat;
	}

	/**
	 * @return the locationLng
	 */
	public double getLocationLng() {
		return locationLng;
	}

	/**
	 * @param locationLng
	 *            the locationLng to set
	 */
	public void setLocationLng(double locationLng) {
		this.locationLng = locationLng;
	}

	/**
	 * @return the locationAddress
	 */
	public String getLocationAddress() {
		return locationAddress;
	}

	/**
	 * @param locationAddress
	 *            the locationAddress to set
	 */
	public void setLocationAddress(String locationAddress) {
		this.locationAddress = locationAddress;
	}

	/**
	 * @return the locationArea
	 */
	public String getLocationArea() {
		return locationArea;
	}

	/**
	 * @param locationArea
	 *            the locationArea to set
	 */
	public void setLocationArea(String locationArea) {
		this.locationArea = locationArea;
	}

	/**
	 * @return the locationCity
	 */
	public String getLocationCity() {
		return locationCity;
	}

	/**
	 * @param locationCity
	 *            the locationCity to set
	 */
	public void setLocationCity(String locationCity) {
		this.locationCity = locationCity;
	}

	/**
	 * @return the locationState
	 */
	public String getLocationState() {
		return locationState;
	}

	/**
	 * @param locationState
	 *            the locationState to set
	 */
	public void setLocationState(String locationState) {
		this.locationState = locationState;
	}

	/**
	 * @return the locationPin
	 */
	public int getLocationPin() {
		return locationPin;
	}

	/**
	 * @param locationPin
	 *            the locationPin to set
	 */
	public void setLocationPin(int locationPin) {
		this.locationPin = locationPin;
	}

	/**
	 * @return the locationDistance
	 */
	public double getLocationDistance() {
		return locationDistance;
	}

	/**
	 * @param locationDistance
	 *            the locationDistance to set
	 */
	public void setLocationDistance(double locationDistance) {
		this.locationDistance = locationDistance;
	}

	/**
	 * @return the locationUpdatedTime
	 */
	public long getLocationUpdatedTime() {
		return locationUpdatedTime;
	}

	/**
	 * @param locationUpdatedTime
	 *            the locationUpdatedTime to set
	 */
	public void setLocationUpdatedTime(long locationUpdatedTime) {
		this.locationUpdatedTime = locationUpdatedTime;
	}

}
