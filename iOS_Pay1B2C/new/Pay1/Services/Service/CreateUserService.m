//
//  CreateUserService.m
//  Pay1
//
//  Created by Annapurna on 11/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "CreateUserService.h"
#import "CreateUserResponse.h"
@implementation CreateUserService

-(void)callCreateUserService:(NSString *)mobileNumber callback:(createUserResponse)callback{
    NSMutableDictionary *postData = [[NSMutableDictionary alloc]init];
    [postData setObject:mobileNumber forKey:@"mobile_number"];
    
    NSLog(@"Post data :%@",postData);
    
    RequestData *requestData = [[RequestData alloc]init];
    requestData.baseURL = kBaseURL;
    requestData.webServiceURL = kCreateUserURL;
    requestData.postData = postData;
    requestData.showLoader = FALSE;
    
    requestData.requestSuccessBlock = ^(RequestData *request) {
        
    [self printResponse:request.responseData forRequest:@"Create User Response"];
    if ([self handleNullResponse:request])
    return;
        
    callback(nil, [self parseCreateUserResponse:request]);
    };
    
    
    requestData.requestFailureBlock = ^(RequestData *request) {
    [self handleNetworkError:request.responseData];
    callback(request.responseData, nil);
    };
    
    [[ServiceManager sharedInstance] makeRequest:requestData];
}

-(CreateUserResponse *)parseCreateUserResponse:(RequestData *)request{
    CreateUserResponse *userResponse = [[CreateUserResponse alloc]init];
    userResponse.status = [request.responseData objectForKey:@"status"];
    userResponse.errorCode = [request.responseData objectForKey:@"errCode"];
    
    NSMutableDictionary *descriptionDic = [request.responseData objectForKey:@"description"];
    userResponse.otp = [descriptionDic objectForKey:@"otp"];
    userResponse.balance = [descriptionDic objectForKey:@"balance"];
    userResponse.recentTransArrayList = [descriptionDic objectForKey:@"recent_pay1_transactions"];
    userResponse.supportNumber = [descriptionDic objectForKey:@"support_number"];
    
    NSMutableDictionary *errorDic = [request.responseData objectForKey:@"ErrorMsg"];
    userResponse.billPaymentFailure = [errorDic objectForKey:@""];
    userResponse.rechargeFailure = [errorDic objectForKey:@""];
    userResponse.walletRefilledRetailer = [errorDic objectForKey:@"WALLET_REFILLED_RETAILER"];
    
    userResponse.dealsArrayList = [descriptionDic objectForKey:@"Deals_offer"];
    
    NSMutableDictionary *offerDic = [descriptionDic objectForKey:@"OfferReview"];
    userResponse.offerFirstStr = [offerDic objectForKey:@"1"];
    userResponse.offerSecondStr = [offerDic objectForKey:@"2"];
    userResponse.offerThirdStr = [offerDic objectForKey:@"3"];
    userResponse.offerfourthStr = [offerDic objectForKey:@"4"];
    userResponse.offerFifthStr = [offerDic objectForKey:@"5"];

    
    
    
    return userResponse;
}

@end
