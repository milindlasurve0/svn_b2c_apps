﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Pay1B2C.UserControls;

namespace Pay1B2C.Views
    {
    public partial class SelectCircle: PhoneApplicationPage
        {
        JArray dict;
        public static string jsonCircle;
        string area_name, op_name;
        string op_id;
        bool isFromRecharge=false;
        string code;
        public SelectCircle()
            {
            InitializeComponent();




            jsonCircle = @"[
    {
        'type': 'States',
        'id': 1,
        'name': 'Andhra Pradesh',
        'code': 'AP'
    },
    {
        'type': 'States',
        'id': 2,
        'name': 'Assam',
        'code': 'AS'
    },
    {
        'type': 'States',
        'id': 3,
        'name': 'Jharkhand',
        'code': 'BR'
    },
    {
        'type': 'Metros',
        'id': 5,
        'name': 'Delhi NCR',
        'code': 'DL'
    },
    {
        'type': 'States',
        'id': 6,
        'name': 'Gujarat',
        'code': 'GJ'
    },
    {
        'type': 'States',
        'id': 7,
        'name': 'Haryana',
        'code': 'NE'
    },
    {
        'type': 'States',
        'id': 8,
        'name': 'Himachal Pradesh',
        'code': 'HP'
    },
    {
        'type': 'States',
        'id': 9,
        'name': 'Jammu & Kashmir',
        'code': 'JK'
    },
    {
        'type': 'States',
        'id': 10,
        'name': 'Karnataka',
        'code': 'KA'
    },
    {
        'type': 'States',
        'id': 11,
        'name': 'Kerala',
        'code': 'KL'
    },
    {
        'type': 'Metros',
        'id': 12,
        'name': 'Kolkata',
        'code': 'KO'
    },
    {
        'type': 'States',
        'id': 13,
        'name': 'Maharashtra',
        'code': 'MH'
    },
    {
        'type': 'States',
        'id': 14,
        'name': 'Madhya Pradesh',
        'code': 'MP'
    },
    {
        'type': 'Metros',
        'id': 15,
        'name': 'Mumbai',
        'code': 'MU'
    },
    {
        'type': 'States',
        'id': 16,
        'name': 'Tripura',
        'code': 'NE'
    },
    {
        'type': 'States',
        'id': 17,
        'name': 'Orissa',
        'code': 'OR'
    },
    {
        'type': 'States',
        'id': 18,
        'name': 'Punjab',
        'code': 'PB'
    },
    {
        'type': 'States',
        'id': 19,
        'name': 'Rajasthan',
        'code': 'RJ'
    },
    {
        'type': 'States',
        'id': 20,
        'name': 'Tamil Nadu',
        'code': 'TN'
    },
    {
        'type': 'States',
        'id': 21,
        'name': 'Uttar Pradesh (East)',
        'code': 'UE'
    },
    {
        'type': 'States',
        'id': 22,
        'name': 'Uttar Pradesh (West)',
        'code': 'UW'
    },{
        'type': 'States',
        'id': 22,
        'name': 'Uttarakhand',
        'code': 'UW'
    },
    {
        'type': 'States',
        'id': 23,
        'name': 'West Bengal',
        'code': 'WB'
    }
]";
            jsonCircle = jsonCircle.Replace("\r", "");
            jsonCircle = jsonCircle.Replace("\n", "");



            dict = (JArray)JsonConvert.DeserializeObject (jsonCircle);
					
						foreach (var item1 in dict)
							{
							
								var name = item1["name"];

                                CircleListControl circleListCtrl = new CircleListControl ();
								circleListCtrl.CircleName.Text = name.ToString ();
                                circleList.Items.Add(circleListCtrl);
								
							}


            }

        protected override void OnNavigatedTo(NavigationEventArgs e)
            {
            if(NavigationContext.QueryString.ContainsKey("op_id") && NavigationContext.QueryString.ContainsKey("code") && NavigationContext.QueryString.ContainsKey("op_name") && NavigationContext.QueryString.ContainsKey("cir_name"))
                {
                op_id = NavigationContext.QueryString["op_id"];
                code = NavigationContext.QueryString["code"];
                area_name = NavigationContext.QueryString["cir_name"];
                op_name=NavigationContext.QueryString["op_name"];

                if(string.IsNullOrEmpty(area_name))
                    {
                    isFromRecharge=true;
                    }

                }
            }

        private void listBox1_SelectionChanged(object sender, SelectionChangedEventArgs e)
            {
            string codeId = dict[circleList.SelectedIndex]["code"].ToString();
        area_name=    dict[circleList.SelectedIndex]["name"].ToString();
        NavigationService.Navigate(new Uri("/Views/PlansActivity.xaml?code=" + codeId + "&op_id=" + op_id + "&cir_name=" + area_name + "&op_name="+op_name, UriKind.RelativeOrAbsolute));
        if(isFromRecharge)
            {
            NavigationService.RemoveBackEntry();
            }
        else
            {
            }
            //NavigationService.Navigate(new Uri("/Views/PlansActivity.xaml?code=" + codeId + "&cir_name=" + dict[circleList.SelectedIndex]["name"].ToString()+"&fromCircle=1", UriKind.RelativeOrAbsolute));
            }
        }
    }