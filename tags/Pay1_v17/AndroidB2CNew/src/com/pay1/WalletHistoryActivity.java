package com.pay1;

import java.util.ArrayList;
import java.util.WeakHashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.larvalabs.svgandroid.SVGParser;
import com.pay1.adapterhandler.WalletHistoryAdapter;
import com.pay1.constants.Constants;
import com.pay1.customviews.MyProgressDialog;
import com.pay1.requesthandler.RequestClass;
import com.pay1.utilities.Utility;

public class WalletHistoryActivity extends Activity {

	private static final String SCREEN_LABEL = "Wallet History Screen";
	private EasyTracker easyTracker = null;
	private final String TAG = "Wallet History";
	int page = 0;
	private Button button_LoadMore;
	private ListView listView_WalletHistory;
	TextView textView_NoData, textView_Shop;
	private RelativeLayout relativeLayout_EmptyView;
	private WalletHistoryAdapter adapter;
	private ArrayList<WeakHashMap<String, String>> data;

	public static final String CATAGORY = "trans_category";
	public static final String TIME = "trans_datetime";
	public static final String AMOUNT = "transaction_amount";
	public static final String BALANCE = "closing_bal";
	public static final String TRANS_TYPE = "trans_type";
	public static final String SERVICE_ID = "service_id";
	public static final String NUMBER = "number";
	public static final String OPERATOR = "operator_name";
	public static final String PRODUCT = "product_id";

	private ImageView imageView_Back;
	private TextView textView_Title;

	@SuppressLint("NewApi")
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.wallet_history_activity);
		try {
			easyTracker = EasyTracker.getInstance(WalletHistoryActivity.this);
			easyTracker.set(Fields.SCREEN_NAME, SCREEN_LABEL);

			// timestamp = System.currentTimeMillis();

			// faqDataSource = new FAQDataSource(WalletHistoryActivity.this);

			imageView_Back = (ImageView) findViewById(R.id.imageView_Back);
			imageView_Back.setImageDrawable(SVGParser.getSVGFromResource(
					getResources(), R.raw.ic_back_old).createPictureDrawable());
			if (Build.VERSION.SDK_INT >= 11)
				imageView_Back.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
			imageView_Back.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onBackPressed();
				}
			});
			
			RelativeLayout back_layout=(RelativeLayout)findViewById(R.id.back_layout);
			back_layout.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onBackPressed();
				}
			});

			Typeface Reguler = Typeface.createFromAsset(getAssets(),
					"EncodeSansNormal-400-Regular.ttf");
			textView_Title = (TextView) findViewById(R.id.textView_Title);
			textView_Title.setTypeface(Reguler);

			textView_NoData = (TextView) findViewById(R.id.textView_NoData);
			textView_NoData.setTypeface(Reguler);
			textView_Shop = (TextView) findViewById(R.id.textView_Shop);
			textView_Shop.setTypeface(Reguler);
			textView_Shop.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent intent = new Intent(WalletHistoryActivity.this,
							MainActivity.class);
					intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
							| Intent.FLAG_ACTIVITY_NEW_TASK);
					intent.putExtra("FROM_QUICKPAY", true);
					intent.putExtra("IS_GIFT", false);
					WalletHistoryActivity.this.startActivity(intent);
					WalletHistoryActivity.this.finish();
				}
			});

			data = new ArrayList<WeakHashMap<String, String>>();
			data.clear();
			listView_WalletHistory = (ListView) findViewById(R.id.listView_WalletHistory);
			relativeLayout_EmptyView = (RelativeLayout) findViewById(R.id.relativeLayout_EmptyView);
			ImageView imageViewHistory=(ImageView)findViewById(R.id.imageViewHistory);
			imageViewHistory.setImageDrawable(SVGParser.getSVGFromResource(
					getResources(), R.raw.ic_wallet_history).createPictureDrawable());
			// listView_WalletHistory.setEmptyView(relativeLayout_EmptyView);
			relativeLayout_EmptyView.setVisibility(View.GONE);

			adapter = new WalletHistoryAdapter(WalletHistoryActivity.this, data);

			button_LoadMore = new Button(WalletHistoryActivity.this);
			button_LoadMore.setHeight(90);
			button_LoadMore
					.setBackgroundResource(R.drawable.button_background_selector_primary);
			button_LoadMore.setText("Load more.....");
			button_LoadMore
					.setTextColor(getResources().getColor(R.color.White));
			button_LoadMore.setTypeface(Reguler);
			button_LoadMore.setTextSize(15f);
			button_LoadMore.setVisibility(View.GONE);
			button_LoadMore.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					// // // Log.e("Last post ID ", "" + lastPostID);
					button_LoadMore.setText("Loading.....");
					page++;
					new MoreWalletHistoryTask().execute("" + page);
				}
			});

			listView_WalletHistory.addFooterView(button_LoadMore);
			listView_WalletHistory.setAdapter(adapter);

			try {

				if (Utility.getSharedPreferences(WalletHistoryActivity.this,
						Constants.SHAREDPREFERENCE_WALLET_DATA) != null) {
					JSONObject jsonObject = new JSONObject(
							Utility.getSharedPreferences(
									WalletHistoryActivity.this,
									Constants.SHAREDPREFERENCE_WALLET_DATA));
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {

						JSONArray jsonArrayDesc = jsonObject
								.getJSONArray("description");
						if (jsonArrayDesc.length() != 0) {
							data.clear();
							int len = 0;
							for (int i = 0; i < jsonArrayDesc.length(); i++) {
								WeakHashMap<String, String> map = new WeakHashMap<String, String>();
								map.put(CATAGORY,
										jsonArrayDesc.getJSONObject(i)
												.getString("trans_category"));
								map.put(TIME, jsonArrayDesc.getJSONObject(i)
										.getString("trans_datetime"));
								map.put(AMOUNT, jsonArrayDesc.getJSONObject(i)
										.getString("transaction_amount"));
								map.put(BALANCE, jsonArrayDesc.getJSONObject(i)
										.getString("closing_bal"));
								map.put(TRANS_TYPE,
										jsonArrayDesc.getJSONObject(i)
												.getString("trans_type"));
								map.put(SERVICE_ID,
										jsonArrayDesc.getJSONObject(i)
												.getString("service_id"));
								if (jsonArrayDesc.getJSONObject(i)
										.getString("trans_category").toString()
										.trim().equalsIgnoreCase("deal")) {
									map.put(NUMBER,
											jsonArrayDesc.getJSONObject(i)
													.getString("offer_name"));
									map.put(OPERATOR,
											jsonArrayDesc.getJSONObject(i)
													.getString("deal_name"));
								} else {
									map.put(NUMBER,
											jsonArrayDesc.getJSONObject(i)
													.getString("number"));
									map.put(OPERATOR,
											jsonArrayDesc.getJSONObject(i)
													.getString("operator_name"));
								}

								map.put(PRODUCT, jsonArrayDesc.getJSONObject(i)
										.getString("product_id"));

								data.add(map);
								len++;
							}

							if (len < 10) {
								button_LoadMore.setVisibility(View.GONE);
							} else {
								button_LoadMore.setText("Load more history");
								button_LoadMore.setVisibility(View.VISIBLE);
							}

							relativeLayout_EmptyView.setVisibility(View.GONE);
							listView_WalletHistory.setVisibility(View.VISIBLE);
						}else{
							relativeLayout_EmptyView.setVisibility(View.VISIBLE);
							listView_WalletHistory.setVisibility(View.GONE);
						}
					}
				}
				WalletHistoryActivity.this.runOnUiThread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						adapter.notifyDataSetChanged();
					}
				});

			} catch (Exception e) {
				// TODO: handle exception
				// // // Log.e("Er ", e.getMessage());
			}

			if (Utility.getRefreshRequired(WalletHistoryActivity.this,
					Constants.SHAREDPREFERENCE_REFESH_WALLET))
				new WalletHistoryTask().execute("" + page);

			// if
			// (WalletHistoryActivity.this.getIntent().getExtras().containsKey("bal"))
			// {
			// if
			// (WalletHistoryActivity.this.getIntent().getExtras().getString("bal")
			// .equalsIgnoreCase("1")) {
			// Constants.showOneButtonDialog(WalletHistoryActivity.this,
			// "Notification", WalletHistoryActivity.this.getIntent()
			// .getExtras().getString("desc").trim(),
			// Constants.DIALOG_CLOSE);
			// new CheckBalanceTask().execute();
			// }
			//
			// }

		} catch (Exception e) {
		}
		overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}

	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView().build());
		// EasyTracker.getInstance(this).activityStart(this);
		easyTracker.send(MapBuilder.createAppView().build());
	}

	@Override
	public void onStop() {
		super.onStop();
		// EasyTracker.getInstance(this).activityStop(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
	}

	public class WalletHistoryTask extends AsyncTask<String, String, String>
			implements OnDismissListener {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			try {
				String response = RequestClass.getInstance()
						.readPay1B2CRequest(
								WalletHistoryActivity.this,
								Constants.B2C_URL + "wallet_history/?page="
										+ params[0] + "&limit=10");

				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				if (this.dialog.isShowing()) {
					this.dialog.dismiss();
				}
			} catch (Exception e) {
			}
			try {
				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONObject jsonObject = new JSONObject(replaced);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {

						if (page == 0) {
							try {

								Utility.setSharedPreferences(
										WalletHistoryActivity.this,
										Constants.SHAREDPREFERENCE_WALLET_DATA,
										replaced);

							} catch (Exception e) {
								// TODO: handle exception
								// // // Log.e("Er ", e.getMessage());
							}
						}

						JSONArray jsonArrayDesc = jsonObject
								.getJSONArray("description");

						if (jsonArrayDesc.length() != 0) {
							if (page == 0)
								data.clear();
							int len = 0;
							for (int i = 0; i < jsonArrayDesc.length(); i++) {
								WeakHashMap<String, String> map = new WeakHashMap<String, String>();
								map.put(CATAGORY,
										jsonArrayDesc.getJSONObject(i)
												.getString("trans_category"));
								map.put(TIME, jsonArrayDesc.getJSONObject(i)
										.getString("trans_datetime"));
								map.put(AMOUNT, jsonArrayDesc.getJSONObject(i)
										.getString("transaction_amount"));
								map.put(BALANCE, jsonArrayDesc.getJSONObject(i)
										.getString("closing_bal"));
								map.put(TRANS_TYPE,
										jsonArrayDesc.getJSONObject(i)
												.getString("trans_type"));
								map.put(SERVICE_ID,
										jsonArrayDesc.getJSONObject(i)
												.getString("service_id"));
								if (jsonArrayDesc.getJSONObject(i)
										.getString("trans_category").toString()
										.trim().equalsIgnoreCase("deal")) {
									map.put(NUMBER,
											jsonArrayDesc.getJSONObject(i)
													.getString("offer_name"));
									map.put(OPERATOR,
											jsonArrayDesc.getJSONObject(i)
													.getString("deal_name"));
								} else {
									map.put(NUMBER,
											jsonArrayDesc.getJSONObject(i)
													.getString("number"));
									map.put(OPERATOR,
											jsonArrayDesc.getJSONObject(i)
													.getString("operator_name"));
								}

								map.put(PRODUCT, jsonArrayDesc.getJSONObject(i)
										.getString("product_id"));

								data.add(map);
								len++;
							}

							if (len < 10) {
								button_LoadMore.setVisibility(View.GONE);
							} else {
								button_LoadMore.setText("Load more history");
								button_LoadMore.setVisibility(View.VISIBLE);
							}
							WalletHistoryActivity.this
									.runOnUiThread(new Runnable() {

										@Override
										public void run() {
											// TODO Auto-generated method stub
											adapter.notifyDataSetChanged();
										}
									});
							relativeLayout_EmptyView.setVisibility(View.GONE);
							listView_WalletHistory.setVisibility(View.VISIBLE);
						} else {
							relativeLayout_EmptyView
									.setVisibility(View.VISIBLE);
							listView_WalletHistory.setVisibility(View.GONE);
						}

						Utility.setRefreshRequired(WalletHistoryActivity.this,
								Constants.SHAREDPREFERENCE_REFESH_WALLET, false);
					} else {
						Constants.showOneButtonDialog(
								WalletHistoryActivity.this, TAG,
								Constants.checkCode(replaced),
								Constants.DIALOG_CLOSE);
					}

				} else {
					Intent intent = new Intent(WalletHistoryActivity.this,
							ConnectivityErrorActivity.class);
					startActivity(intent);
				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonDialog(WalletHistoryActivity.this, TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
				relativeLayout_EmptyView.setVisibility(View.VISIBLE);
				listView_WalletHistory.setVisibility(View.GONE);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonDialog(WalletHistoryActivity.this, TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
				relativeLayout_EmptyView.setVisibility(View.VISIBLE);
				listView_WalletHistory.setVisibility(View.GONE);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(WalletHistoryActivity.this);
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							dialog.dismiss();
							WalletHistoryTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			dialog.dismiss();
			WalletHistoryTask.this.cancel(true);
		}

	}

	public class MoreWalletHistoryTask extends
			AsyncTask<String, String, String> implements OnDismissListener {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			try {
				String response = RequestClass.getInstance()
						.readPay1B2CRequest(
								WalletHistoryActivity.this,
								Constants.B2C_URL + "wallet_history/?page="
										+ params[0] + "&limit=10");

				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				if (this.dialog.isShowing()) {
					this.dialog.dismiss();
				}
			} catch (Exception e) {
			}
			try {
				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONObject jsonObject = new JSONObject(replaced);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {

						JSONArray jsonArrayDesc = jsonObject
								.getJSONArray("description");
						int len = 0;
						for (int i = 0; i < jsonArrayDesc.length(); i++) {
							WeakHashMap<String, String> map = new WeakHashMap<String, String>();
							map.put(CATAGORY, jsonArrayDesc.getJSONObject(i)
									.getString("trans_category"));
							map.put(TIME, jsonArrayDesc.getJSONObject(i)
									.getString("trans_datetime"));
							map.put(AMOUNT, jsonArrayDesc.getJSONObject(i)
									.getString("transaction_amount"));
							map.put(BALANCE, jsonArrayDesc.getJSONObject(i)
									.getString("closing_bal"));
							map.put(TRANS_TYPE, jsonArrayDesc.getJSONObject(i)
									.getString("trans_type"));
							map.put(SERVICE_ID, jsonArrayDesc.getJSONObject(i)
									.getString("service_id"));
							if (jsonArrayDesc.getJSONObject(i)
									.getString("trans_category").toString()
									.trim().equalsIgnoreCase("deal")) {
								map.put(NUMBER, jsonArrayDesc.getJSONObject(i)
										.getString("offer_name"));
								map.put(OPERATOR, jsonArrayDesc
										.getJSONObject(i)
										.getString("deal_name"));
							} else {
								map.put(NUMBER, jsonArrayDesc.getJSONObject(i)
										.getString("number"));
								map.put(OPERATOR,
										jsonArrayDesc.getJSONObject(i)
												.getString("operator_name"));
							}

							map.put(PRODUCT, jsonArrayDesc.getJSONObject(i)
									.getString("product_id"));

							data.add(map);
							len++;
						}

						if (len < 10) {
							button_LoadMore.setVisibility(View.GONE);
						} else {
							button_LoadMore.setText("Load more history");
							button_LoadMore.setVisibility(View.VISIBLE);
						}

						WalletHistoryActivity.this
								.runOnUiThread(new Runnable() {

									@Override
									public void run() {
										// TODO Auto-generated method stub
										adapter.notifyDataSetChanged();
									}
								});

					} else {
						Constants.showOneButtonDialog(
								WalletHistoryActivity.this, TAG,
								Constants.checkCode(replaced),
								Constants.DIALOG_CLOSE);
					}

				} else {
					Intent intent = new Intent(WalletHistoryActivity.this,
							ConnectivityErrorActivity.class);
					startActivity(intent);
				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonDialog(WalletHistoryActivity.this, TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonDialog(WalletHistoryActivity.this, TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(WalletHistoryActivity.this);
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							dialog.dismiss();
							MoreWalletHistoryTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			dialog.dismiss();
			MoreWalletHistoryTask.this.cancel(true);
		}

	}

	public class CheckBalanceTask extends AsyncTask<String, String, String> {
		// MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {

			String response = RequestClass.getInstance()
					.readPay1B2CRequest(WalletHistoryActivity.this,
							Constants.B2C_URL + "check_bal");
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			// if (this.dialog.isShowing()) {
			// this.dialog.dismiss();
			// }
			super.onPostExecute(result);
			try {
				// JSONArray array = new JSONArray(result);
				JSONObject jsonObject = new JSONObject(result);
				String status = jsonObject.getString("status");
				if (status.equalsIgnoreCase("SUCCESS")) {
					JSONObject description = jsonObject
							.getJSONObject("description");
					String account_balance = description
							.getString("account_balance");
					Utility.setBalance(WalletHistoryActivity.this,
							Constants.SHAREDPREFERENCE_BALANCE,
							account_balance.trim());
				}
			} catch (JSONException e) {
				// e.printStackTrace();
				// Constants.showCustomToast(WalletHistoryActivity.this,
				// result);
			} catch (Exception e) {
				// TODO: handle exception
				// Constants.showCustomToast(WalletHistoryActivity.this,
				// result);
			}

		}

		@Override
		protected void onPreExecute() {
			// dialog = new MyProgressDialog(WalletHistoryActivity.this);
			// // this.dialog.setMessage("Please wait.....");
			// this.dialog.show();
			super.onPreExecute();

		}

	}
}
