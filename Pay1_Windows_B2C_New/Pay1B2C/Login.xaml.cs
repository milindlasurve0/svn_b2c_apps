﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.IO.IsolatedStorage;
using System.IO;
using System.Windows.Input;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using SQLite;
using System.Windows.Threading;
using System.Device.Location;

namespace Pay1B2C
    {
    public partial class Login: PhoneApplicationPage
        {
        ProgressBarManager ProgressBarManager;
        GeoCoordinateWatcher watcher;
        public static string latti, longi;
        // public static string currentAddress;
        SQLiteConnection db = null;
        DispatcherTimer DelayedTimer;
        public Login()
            {
            InitializeComponent();
            GoogleAnalytics.EasyTracker.GetTracker().SendView("Registration Layout");
            ProgressIndicator prog = new ProgressIndicator();
            ProgressBarManager = new ProgressBarManager();
            SystemTray.SetProgressIndicator(this, prog);
            ProgressBarManager.Hook(prog);
            getPushUrl();
            }

        private void getPushUrl()
            {
           
            }


        private void CreateDatabase()
            {
          
                try
                    {
                    if(db == null)
                        {
                        db = new SQLiteConnection(Constant.DATABASE_NAME);

                       // db.Open();
                        }
                    SQLiteCommand cmd = db.CreateCommand("Create table QuickPayAllDataTable(id text primary key not null,flag text,amount text,users_id text,stv text,operator_id text,product text,number text,delegate_flag text,missed_number text,confirmation text,operator_name text,transaction_flag text,operator_code text,datetime text,name text)");
                    int i = cmd.ExecuteNonQuery();
                    }
                catch(SQLiteException ex)
                    {

                    }



                try
                    {
                    SQLiteCommand cmd = db.CreateCommand("Create table OperatorTable(id text,name text,code text,p_id text UNIQUE,stv text,flag text,service_id text,delegateFlag text,min text,max text,charges_slab text,service_charge_amount text,service_tax_percent text,service_charge_percent text,prefix text,length text)");
                    int i = cmd.ExecuteNonQuery();
                    }
                catch(SQLiteException ex)
                    {

                    }
                try
                    {
                    SQLiteCommand cmd1 = db.CreateCommand("Create table Plans(planAmount int,planOpertorID text,planOperatorName text,planCircleID text,planCircleName text,planType text,planValidity text,planDescription text,planUptateTime datetime,show_flag text)");
                    int i1 = cmd1.ExecuteNonQuery();
                    }
                catch(SQLiteException ex)
                    {

                    }
                try
                    {
                    SQLiteCommand cmd2 = db.CreateCommand("Create table DthPlans (dthPlanAmount int,dthPlanOpertorID text,dthPlanOperatorName text,dthPlanCircleID text,dthPlanCircleName text,dthPlanType text,dthPlanValidity text,dthPlanDescription text,dthPlanUptateTime datetime)");
                    int i2 = cmd2.ExecuteNonQuery();
                    }
                catch(SQLiteException ex)
                    {

                    }

                try
                    {
                    SQLiteCommand cmdFreeBie = db.CreateCommand("Create table NewFreeBieAllData (dealname text,img_url text,category text,category_id text,min_amount text,offer_id text unique,deal_id text,name text,validity text,short_desc text,location_detail text,freeBieUpdateTime text)");
                    int i3 = cmdFreeBie.ExecuteNonQuery();
                    }
                catch(Exception ee)
                    {
                    System.Diagnostics.Debug.WriteLine(ee.Message);
                    }
                try
                    {
                    SQLiteCommand cmd = db.CreateCommand("Create table NumberAndCircle (area_name text,area text,opr_name text,numberOperator text,number text,product_id text)");
                    int i = cmd.ExecuteNonQuery();
                    }
                catch(Exception ee)
                    {
                    System.Diagnostics.Debug.WriteLine(ee.Message);
                    }


                try
                    {
                    SQLiteCommand cmdPlans = db.CreateCommand("Create table AllPlans (allPlanOpertorID text,allPlanOperatorName text,allPlanCircleID text,allPlanCircleName text,allCirclePlans text)");
                    int j = cmdPlans.ExecuteNonQuery();
                    }
                catch(Exception ee)
                    {
                    System.Diagnostics.Debug.WriteLine(ee.Message);
                    }

                
            }






        private void button1_Click(object sender, RoutedEventArgs e)
            {
           
            }

        void textMouseEnter(object sender, MouseEventArgs e)
            {

            NavigationService.Navigate(new Uri("/TermsAndCondition.xaml?", UriKind.RelativeOrAbsolute));
            PhoneApplicationService.Current.State["isTermsAndCondition"] = "1";
            PhoneApplicationService.Current.State[Constant.IS_PROFILE] = true;
            }


        void textPrivacyMouseEnter(object sender, MouseEventArgs e)
            {
            NavigationService.Navigate(new Uri("/TermsAndCondition.xaml?", UriKind.RelativeOrAbsolute));
            PhoneApplicationService.Current.State["isTermsAndCondition"] = "2";
            PhoneApplicationService.Current.State[Constant.IS_PROFILE] = true;
            }

        private void InitializeWebRequestClientStackForURI()
            {

            // Create a HttpWebRequest.
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(new Uri(Constant.B2C_URL));

            //Create the cookie container and add a cookie.
            request.CookieContainer = new CookieContainer();


            // This example shows manually adding a cookie, but you would most
            // likely read the cookies from isolated storage.
            request.CookieContainer.Add(new Uri(Constant.B2C_URL),
                new Cookie("id", "1234"));

            // Send the request.
            request.BeginGetResponse(new AsyncCallback(ReadCallback), request);
            }

        // Get the response and write cookies to isolated storage.
        private void ReadCallback(IAsyncResult asynchronousResult)
            {
            HttpWebRequest request = (HttpWebRequest)asynchronousResult.AsyncState;
            HttpWebResponse response = (HttpWebResponse)
                request.EndGetResponse(asynchronousResult);
            using(IsolatedStorageFile isf =
                IsolatedStorageFile.GetUserStoreForApplication())
                {
                using(IsolatedStorageFileStream isfs = isf.OpenFile("CookieExCookies",
                    FileMode.OpenOrCreate, FileAccess.Write))
                    {
                    using(StreamWriter sw = new StreamWriter(isfs))
                        {
                        foreach(Cookie cookieValue in response.Cookies)
                            {
                            sw.WriteLine("Cookie: " + cookieValue.ToString());
                            }
                        sw.Close();
                        }
                    }

                }
            }
        private void ReadFromIsolatedStorage()
            {
            using(IsolatedStorageFile isf = IsolatedStorageFile.GetUserStoreForApplication())
                {
                using(IsolatedStorageFileStream isfs =
                   isf.OpenFile("CookieExCookies", FileMode.OpenOrCreate))
                    {
                    using(StreamReader sr = new StreamReader(isfs))
                        {
                       String st = sr.ReadToEnd();
                        sr.Close();
                        }
                    }

                }
            }

        private void btnRegister_Click(object sender, RoutedEventArgs e)
            {
            if(textMobileNumber.Text.Length==10 && (textMobileNumber.Text.StartsWith("7")||textMobileNumber.Text.StartsWith("8")||textMobileNumber.Text.StartsWith("9")))
                {
                registerUser(textMobileNumber.Text);
                }
            else
                {
                MessageBox.Show("");
                }
           
            }

        private async void registerUser(String number)
            {
            //ProgressBarManager.Show(1, "Please wait...", false, 10);
            var values = new List<KeyValuePair<string, string>>
                        { 
                        new KeyValuePair<string, string>("actiontype", "create_user"),
                        new KeyValuePair<string, string>("mobile_number",number),
                       new KeyValuePair<string, string>("api","true"),
                       new KeyValuePair<string, string>("api_version",Constant.API_VERSION)
                        };

            FormUrlEncodedContent values1 = new FormUrlEncodedContent(values);
            Task<string> response = Constant.postParamsAndGetResponse(values1);
            string result = await response;
            if(string.IsNullOrEmpty(result))
                {
                NavigationService.Navigate(new Uri("/Views/NoConnection.xaml?", UriKind.Relative));
                }
            else
                {
                var dict = (JObject)JsonConvert.DeserializeObject(result);
                var status = dict["status"];
              
                if(status.ToString() == "success")
                    {
                    Constant.SavePersistent(Constant.SETTINGS_IS_NEW, true);
                    var description = dict["description"];
                    Constant.setOtpHash(description["otp"].ToString());
                    Constant.setUserNumber(number);
                    Constant.setLastTransaction(description["recent_pay1_transactions"].ToString());
                    Constant.setbalance(description["balance"].ToString());
                    Constant.SavePersistent("session_id", description["session_id"].ToString());
                    Constant.SavePersistent("session_name", description["session_name"].ToString());
                    NavigationService.Navigate(new Uri("/EnterPin.xaml?", UriKind.RelativeOrAbsolute));
                    NavigationService.RemoveBackEntry();
                    }
                else
                    {
                    String errCode=dict["errCode"].ToString();
                    if(errCode.Equals("202"))
                        {
                        Constant.SavePersistent(Constant.SETTINGS_IS_NEW, false);
                        var description = dict["description"];
                        Constant.setOtpHash(description["otp"].ToString());
                        Constant.setUserNumber(number);
                        Constant.setLastTransaction(description["recent_pay1_transactions"].ToString());
                        Constant.setbalance(description["balance"].ToString());
                        Constant.SavePersistent("session_id", description["session_id"].ToString());
                        Constant.SavePersistent("session_name", description["session_name"].ToString());
                        NavigationService.Navigate(new Uri("/EnterPin.xaml?", UriKind.RelativeOrAbsolute));
                        NavigationService.RemoveBackEntry();
                        }
                    else
                        {
                        MessageBox.Show("Something went wrong");
                        }
                    }
                }
            }


        }
    }