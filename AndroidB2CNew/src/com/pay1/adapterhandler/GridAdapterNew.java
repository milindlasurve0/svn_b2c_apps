package com.pay1.adapterhandler;

import java.text.DecimalFormat;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.database.SQLException;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.larvalabs.svgandroid.SVGParser;
import com.pay1.R;
import com.pay1.constants.Constants;
import com.pay1.databasehandler.FreeBies;
import com.pay1.databasehandler.FreeBiesDataSource;
import com.pay1.requesthandler.RequestClass;
import com.pay1.utilities.RoundedImageView;
import com.pay1.utilities.RoundedTransformation;
import com.pay1.utilities.Utility;
import com.squareup.picasso.Picasso;

public class GridAdapterNew extends ArrayAdapter<FreeBies> {
	Context context;
	ArrayList<FreeBies> data;
	private int o_id = 0;
	private boolean isMyLike, isNear, isMyGift;
	Typeface Reguler;

	
	public GridAdapterNew(Context context, ArrayList<FreeBies> freeBies) {
	       super(context,0, freeBies);
	    }
	
	public GridAdapterNew(Context context, ArrayList<FreeBies> data,
			boolean isMyLike, boolean isNear, boolean isMyGift) {
		super(context,0);
		this.context = context;
		this.data = data;
		Reguler = Typeface.createFromAsset(this.context.getAssets(),
				"Roboto-Regular.ttf");
		this.isMyLike = isMyLike;
		this.isNear = isNear;
		this.isMyGift = isMyGift;
	
	}
	
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		try {
			RecordHolder holder = null;

			if (row == null) {
				LayoutInflater inflater = ((Activity) context)
						.getLayoutInflater();
				row = inflater.inflate(R.layout.gift_grid_cell_adapter, null);

				holder = new RecordHolder();
				holder.textView_dealName1=(TextView)row.findViewById(R.id.textView_dealName1);
				holder.imageView_Photo = (ImageView) row
						.findViewById(R.id.imageView_Photo);
				holder.textView_OfferStatus = (TextView) row
						.findViewById(R.id.textView_OfferStatus);
				holder.imageView_Logo = (RoundedImageView) row
						.findViewById(R.id.imageView_Logo);
				holder.imageView_Like = (ImageView) row
						.findViewById(R.id.imageView_Like);
				holder.textView_OfferName = (TextView) row
						.findViewById(R.id.textView_OfferName);
				holder.textView_OfferName.setTypeface(Reguler);
				holder.textView_Points = (TextView) row
						.findViewById(R.id.textView_Points);
				holder.textView_Points.setTypeface(Reguler);
				holder.textView_Area = (TextView) row
						.findViewById(R.id.textView_Area);
				holder.textView_Area.setTypeface(Reguler);
				holder.textView_Distance = (TextView) row
						.findViewById(R.id.textView_Distance);
				holder.textView_Distance.setTypeface(Reguler);
				holder.locationLayout = (RelativeLayout) row
						.findViewById(R.id.locationLayout);
				holder.imageView_Point=(ImageView)row.findViewById(R.id.imageView_Point);
				holder.imageView_location=(ImageView)row.findViewById(R.id.imageView_location);

				row.setTag(holder);
			} else {
				holder = (RecordHolder) row.getTag();
			}

			final FreeBies freeBie = data.get(position);
			holder.textView_OfferName.setText(freeBie.getFreebieShortDesc());
			
			holder.imageView_location.setImageDrawable(SVGParser.getSVGFromResource(
					context.getResources(), R.raw.ic_shoplocator)
					.createPictureDrawable());
			
			holder.imageView_Point.setImageDrawable(SVGParser.getSVGFromResource(
					context.getResources(), R.raw.ic_gift_coin)
					.createPictureDrawable());
			holder.textView_dealName1.setText(freeBie.getFreebieDealName());
			holder.textView_dealName1.setTypeface(Reguler);
			// holder.imageView_Photo.setImageResource(freeBie.getUrl());
			try {
				Picasso.with(context).load(freeBie.getFreebieURL())
						.transform(new RoundedTransformation(0, 0))
						.placeholder(R.drawable.deal_default)
						.into(holder.imageView_Photo);
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			try {
				Picasso.with(context).load(freeBie.getFreebieLogoURL())
						.placeholder(R.drawable.deal_default)
						.transform(new RoundedTransformation(0, 0))
						.into(holder.imageView_Logo);
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			// Bitmap bm = decodeSampledBitmapFromUri(freeBie.getFreebieURL(),
			// 150,
			// 150);
			// holder.imageView_Photo.setImageBitmap(bm);
			holder.textView_Points.setText(freeBie.getFreebieMinAmount() + "");
			holder.textView_Area.setText(freeBie.getFreebieArea() + "");

			try {
				String lat = Utility.getCurrentLatitude(context,
						Constants.SHAREDPREFERENCE_CURRENT_LATITUDE);
				String lng = Utility.getCurrentLongitude(context,
						Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE);
				String d = "0";
				if (lat != "" && lng != "")
					d = String.valueOf(Constants.distanceFrom(
							Double.parseDouble(lat), Double.parseDouble(lng),
							freeBie.getFreebieLat(), freeBie.getFreebieLng()));
				Double dis = Double.parseDouble(d);
				DecimalFormat decimalFormat = new DecimalFormat("0.0");
				if (dis == 0) {
					holder.textView_Distance.setVisibility(View.INVISIBLE);
				} else if (dis < 1) {
					dis = dis * 1000;
					/*holder.textView_Distance.setText(decimalFormat.format(dis)
							+ " M");*/
					holder.textView_Distance.setText(" : < 1KM ");
					holder.textView_Distance.setVisibility(View.VISIBLE);
				} else {
					holder.textView_Distance.setText(" : "+decimalFormat.format(dis)
							+ " KM");
					holder.textView_Distance.setVisibility(View.VISIBLE);
				}
			} catch (Exception e) {
				holder.textView_Distance.setVisibility(View.INVISIBLE);
			}
			if (freeBie.getFreebieLike() == 0)
				holder.imageView_Like.setImageResource(R.drawable.ic_unlike);
			else
				holder.imageView_Like.setImageResource(R.drawable.ic_like);
			holder.imageView_Like
					.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							if (freeBie.getFreebieLike() == 0)
								freeBie.setFreebieLike(1);
							else
								freeBie.setFreebieLike(0);
							notifyDataSetChanged();
							o_id = freeBie.getFreebieOfferID();

							new LikeTask().execute(o_id + "");

							// notifyDataSetChanged();
						}
					});

			if (isMyLike)
				holder.imageView_Like.setVisibility(View.GONE);
			else
				holder.imageView_Like.setVisibility(View.VISIBLE);

			if (isNear) {
				holder.textView_Distance.setVisibility(View.VISIBLE);
				holder.textView_Area.setVisibility(View.VISIBLE);
			} else {
				holder.textView_Distance.setVisibility(View.GONE);
				holder.locationLayout.setVisibility(View.GONE);
			}

			if (isMyGift) {
				if (freeBie.getFreebieCouponStatusID() == 3) {
					holder.textView_OfferStatus.setVisibility(View.VISIBLE);
					holder.textView_OfferStatus.setText("EXPIRED");
							//.setImageResource(R.drawable.ic_expired);
				} else if (freeBie.getFreebieCouponStatusID() == 2) {
					holder.textView_OfferStatus.setVisibility(View.VISIBLE);
					holder.textView_OfferStatus.setText("REDEEMED");
				} else {
					holder.textView_OfferStatus.setVisibility(View.GONE);
				}
			} else
				holder.textView_OfferStatus.setVisibility(View.GONE);
			
			
			
			LinearLayout rupeeLayout=(LinearLayout)row.findViewById(R.id.rupeeLayout);
			//RelativeLayout locationLayout=(RelativeLayout)row.findViewById(R.id.locationLayout);
			ImageView imageView_Rupee = (ImageView) row
					.findViewById(R.id.imageView_Rupee);
			imageView_Rupee.setImageDrawable(SVGParser.getSVGFromResource(
					context.getResources(), R.raw.ic_rupee)
					.createPictureDrawable());
			
			TextView textView_Rupee=(TextView)row.findViewById(R.id.textView_Rupee);
			textView_Rupee.setText(freeBie.getFreeBieOfferPrice()+ "");
			textView_Rupee.setTypeface(Reguler);
			
			
			if(freeBie.getFreeBieByVoucher()==1){
				rupeeLayout.setVisibility(View.VISIBLE);
			}else{
								rupeeLayout.setVisibility(View.GONE);
				
			}

			
		} catch (Exception e) {
		}
		return row;

	}

	class RecordHolder {
		TextView textView_OfferName, textView_Points, textView_Area,
				textView_Distance,textView_OfferStatus,textView_dealName1;
		ImageView imageView_Photo, imageView_Like,
				imageView_Point,imageView_location;
		RoundedImageView imageView_Logo;
		RelativeLayout locationLayout;

	}

	public Bitmap decodeSampledBitmapFromUri(String path, int reqWidth,
			int reqHeight) {
		Bitmap bm = null;

		// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(path, options);

		// Calculate inSampleSize
		options.inSampleSize = calculateInSampleSize(options, reqWidth,
				reqHeight);

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;
		bm = BitmapFactory.decodeFile(path, options);

		return bm;
	}

	public int calculateInSampleSize(

	BitmapFactory.Options options, int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {
			if (width > height) {
				inSampleSize = Math.round((float) height / (float) reqHeight);
			} else {
				inSampleSize = Math.round((float) width / (float) reqWidth);
			}
		}

		return inSampleSize;
	}

	public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, int pixels) {
		Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
				bitmap.getHeight(), Config.ARGB_8888);
		Canvas canvas = new Canvas(output);

		final int color = 0xff424242;
		final Paint paint = new Paint();
		final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
		final RectF rectF = new RectF(rect);
		final float roundPx = pixels;

		paint.setAntiAlias(true);
		canvas.drawARGB(0, 0, 0, 0);
		paint.setColor(color);
		canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

		paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
		canvas.drawBitmap(bitmap, rect, rect, paint);

		return output;
	}

	public class LikeTask extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... params) {
			try {

				String response = RequestClass.getInstance()
						.readPay1B2CRequest(context,
								Constants.B2C_URL + "MyLikes/?id=" + params[0]);

				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {

				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONObject jsonObject = new JSONObject(replaced);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {

						JSONObject jsonObject2 = jsonObject
								.getJSONObject("description");
						FreeBiesDataSource freeBiesDataSource = new FreeBiesDataSource(
								context);
						try {

							freeBiesDataSource.open();
							freeBiesDataSource.updateFreeBies(Integer
									.parseInt(jsonObject2.getString("status")),
									String.valueOf(o_id));
						} catch (SQLException exception) {
						} catch (Exception e) {

						} finally {
							freeBiesDataSource.close();
						}
					}
				}
			} catch (JSONException e) {
			} catch (Exception e) {
			}
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}
	}

	/* (non-Javadoc)
	 * @see android.widget.BaseAdapter#notifyDataSetChanged()
	 */
	@Override
	public void notifyDataSetChanged() {
		// TODO Auto-generated method stub
		super.notifyDataSetChanged();
	}
	
	
	
}
