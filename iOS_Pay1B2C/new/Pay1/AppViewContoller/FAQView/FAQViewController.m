//
//  FAQViewController.m
//  Pay1
//
//  Created by webninjaz on 16/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "FAQViewController.h"
#import "FAQService.h"
#import "FAQCriteria.h"
#import "FAQResponse.h"
#import "UIAlertView+BlockAddition.h"
#import "FAQDataList.h"
#import "ASIHTTPRequest.h"
#import "SBJsonParser.h"

@interface FAQViewController ()

@end

@implementation FAQViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self showHUDOnView];
    [self hideNavBackButton];
    
    [self setNaviationButtonWithText:@"BACK" selector:@selector(backClicked:) isLeft:TRUE isReduceSize:TRUE isMyFont:18.0];
    [self callFaqService];

}

-(void)viewWillAppear:(BOOL)animated{
}


- (void)requestFailed:(ASIHTTPRequest *)request
{
    [self hideHUDOnView];

    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Pay1" message:@"Connection Lost" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    
    [alert show];
}


-(void)callFaqService{
    url = [NSString stringWithFormat:@"http://cdev.pay1.in/index.php/api_new/action/api/true/actiontype/get_faq/?Param=Description&req=faq"];
    NSLog(@"The url is%@",url);
    
    url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSURL *surl = [NSURL URLWithString:url];
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:surl];
    request.shouldAttemptPersistentConnection = NO;
    request.timeOutSeconds = 120;
    [request setDelegate:self];
    [request startAsynchronous];
}


- (void)requestFinished:(ASIHTTPRequest *)request
{
    [self hideHUDOnView];
    NSLog(@"request response :%@",request.responseString);
    NSString *newString = [request.responseString substringToIndex:[request.responseString length]-18];
    NSLog(@"newString:%@",newString);
    newStr =  [newString substringFromIndex:90];
    NSLog(@"newStr:%@",newStr);
    
    
    NSString *htmlString = [NSString stringWithFormat:@"<font face='Open Sans' size='2'>%@", newStr];
   
   // NSString *htmlString = [NSString stringWithFormat:@"<div style=text-align:justify; text-justify:inter-word; font-family='Open Sans';>%@</div>",newStr];
    NSLog(@"htmlString :%@",htmlString);
    
   // [webView loadHTMLString:htmlString baseURL:nil];
    
    NSString* removedString=[htmlString stringByReplacingOccurrencesOfString:@"\\" withString:@""];
    
    NSLog(@"#Removed :%@",removedString);
    
    [webView loadHTMLString:removedString baseURL:nil];
    
    
   // [webView loadHTMLString:newStr baseURL:nil];
}
-(BOOL) webView:(UIWebView *)inWeb shouldStartLoadWithRequest:(NSURLRequest *)inRequest navigationType:(UIWebViewNavigationType)inType {
    if ( inType == UIWebViewNavigationTypeLinkClicked ) {
    [[UIApplication sharedApplication] openURL:[inRequest URL]];
    return NO;
    }
    
    return YES;
}

-(void)backClicked:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
