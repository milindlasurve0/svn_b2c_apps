package com.pay1;

import java.util.ArrayList;
import java.util.WeakHashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.pay1.customviews.MyProgressDialog;

import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.pay1.adapterhandler.MyDealAdapter;
import com.pay1.constants.Constants;
import com.pay1.requesthandler.RequestClass;

public class MyDealActivity extends AppBaseActivity {

	private static final String SCREEN_LABEL = "My Deals Screen";
	private EasyTracker easyTracker = null;
	private final String TAG = "My Deal";

	private ImageView imageView_Back;
	private TextView textView_Title, textView_NoData, textView_Shop;
	private ListView listView_MyDeals;
	private ArrayList<WeakHashMap<String, String>> data;
	private MyDealAdapter adapter;
	private LinearLayout relativeLayout_EmptyView;

	public static final String ID = "id";
	public static final String TRANSACTION_ID = "transaction_id";
	public static final String USER_ID = "users_id";
	public static final String DEAL_ID = "deal_id";
	public static final String OFFER_ID = "offer_id";
	public static final String OFFER_PRICE = "offer_price";
	public static final String QUANTITY = "quantity";
	public static final String AMOUNT = "amount";
	public static final String LATITUDE = "latitude";
	public static final String LONGITUDE = "longitude";
	public static final String DISTANCE = "distance";
	public static final String ACTUAL_PRICE = "actual_price";
	public static final String TOTAL_STOCK = "total_stock";
	public static final String DEAL_NAME = "deal_name";
	public static final String OFFER_NAME = "offer_name";
	public static final String DISCOUNT = "discount";
	public static final String TRANSATION_DATE = "trans_datetime";
	public static final String REANSACTION_MODE = "transaction_mode";
	public static final String SHORT_DESC = "short_desc";
	public static final String STOCK_SOLD = "stock_sold";
	public static final String LONG_DESC = "long_desc";
	public static final String EXPIRY = "expiry";
	public static final String COUPON_STATUS = "coupon_status";
	public static final String URL = "img_url";
	public static final String VOUCHER_CODE = "code";

	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.my_deal_activity);
		try {
			easyTracker = EasyTracker.getInstance(MyDealActivity.this);
			easyTracker.set(Fields.SCREEN_NAME, SCREEN_LABEL);
			registerBaseActivityReceiver();

			Typeface Reguler = Typeface.createFromAsset(getAssets(),
					"EncodeSansNormal-400-Regular.ttf");

			textView_Title = (TextView) findViewById(R.id.textView_Title);
			textView_Title.setTypeface(Reguler);

			textView_NoData = (TextView) findViewById(R.id.textView_NoData);
			textView_NoData.setTypeface(Reguler);
			textView_Shop = (TextView) findViewById(R.id.textView_Shop);
			textView_Shop.setTypeface(Reguler);
			textView_Shop.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onBackPressed();
				}
			});

			imageView_Back = (ImageView) findViewById(R.id.imageView_Back);
			imageView_Back.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onBackPressed();
				}
			});

			data = new ArrayList<WeakHashMap<String, String>>();
			listView_MyDeals = (ListView) findViewById(R.id.listView_MyDeals);

			adapter = new MyDealAdapter(MyDealActivity.this, data);
			relativeLayout_EmptyView = (LinearLayout) findViewById(R.id.relativeLayout_EmptyView);
			listView_MyDeals.setEmptyView(relativeLayout_EmptyView);
			listView_MyDeals.setAdapter(adapter);
			listView_MyDeals.setClickable(false);

			new MyDealTask().execute();
		} catch (Exception e) {
		}
		overridePendingTransition(R.anim.push_up_in, R.anim.fadeout);
	}

	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView().build());
		// EasyTracker.getInstance(this).activityStart(this);
		easyTracker.send(MapBuilder.createAppView().build());
	}

	@Override
	public void onStop() {
		super.onStop();
		// EasyTracker.getInstance(this).activityStop(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		// Constants.showNavigationBar(MyDealActivity.this);
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unRegisterBaseActivityReceiver();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (Constants.isExpanded) {
			Constants.collapsView(this);
		} else {
			super.onBackPressed();
			overridePendingTransition(R.anim.fadeout, R.anim.push_down_out);
		}
	}

	public class MyDealTask extends AsyncTask<String, String, String> implements
			OnDismissListener {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			try {
				String response = RequestClass.getInstance()
						.readPay1B2CRequest(MyDealActivity.this,
								Constants.B2C_URL + "get_my_deal");

				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			try {

				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONObject jsonObject = new JSONObject(replaced);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {
						data.clear();
						JSONArray jsonArray_Desc = jsonObject
								.getJSONArray("description");

						for (int i = 0; i < jsonArray_Desc.length(); i++) {
							JSONObject jsonObjectInner = jsonArray_Desc
									.getJSONObject(i);
							WeakHashMap<String, String> map = new WeakHashMap<String, String>();
							map.put(DEAL_ID,
									jsonObjectInner.getString("deal_id").trim());
							map.put(DEAL_NAME,
									jsonObjectInner.getString("deal_name")
											.trim());
							map.put(OFFER_NAME,
									jsonObjectInner.getString("short_desc")
											.trim());
							map.put(COUPON_STATUS,
									jsonObjectInner.getString("coupon_status")
											.trim());
							map.put(EXPIRY, jsonObjectInner.getString("expiry")
									.trim());
							map.put(URL, jsonObjectInner.getString("img_url")
									.trim());
							map.put(AMOUNT, jsonObjectInner.getString("amount")
									.trim());
							map.put(DISCOUNT,
									jsonObjectInner.getString("discount")
											.trim());
							map.put(ACTUAL_PRICE,
									jsonObjectInner.getString("actual_price")
											.trim());
							map.put(OFFER_PRICE,
									jsonObjectInner.getString("offer_price")
											.trim());
							map.put(TOTAL_STOCK,
									jsonObjectInner.getString("total_stock")
											.trim());
							map.put(STOCK_SOLD,
									jsonObjectInner.getString("stock_sold")
											.trim());
							map.put(VOUCHER_CODE,
									jsonObjectInner.getString("code").trim());
							map.put(TRANSACTION_ID,
									jsonObjectInner.getString("transaction_id")
											.trim());
							map.put(QUANTITY,
									jsonObjectInner.getString("quantity")
											.trim());
							map.put(VOUCHER_CODE,
									jsonObjectInner.getString("code").trim());
							data.add(map);
						}

						MyDealActivity.this.runOnUiThread(new Runnable() {

							@Override
							public void run() {
								// TODO Auto-generated method stub
								adapter.notifyDataSetChanged();
							}
						});

					} else {
						Constants.showOneButtonDialog(MyDealActivity.this, TAG,
								Constants.checkCode(replaced),
								Constants.DIALOG_CLOSE);
					}

				} else {
					// Constants.showOneButtonDialog(MyDealActivity.this,
					// TAG, Constants.ERROR_INTERNET,
					// Constants.DIALOG_CLOSE);
					Intent intent = new Intent(MyDealActivity.this,
							ConnectivityErrorActivity.class);
					intent.putExtra(Constants.RECHARGE_FOR,
							Constants.SUBSCRIBE_DEAL);
					startActivity(intent);

				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonDialog(MyDealActivity.this, TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonDialog(MyDealActivity.this, TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(MyDealActivity.this);
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							MyDealTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			MyDealTask.this.cancel(true);
			dialog.cancel();
		}

	}
}
