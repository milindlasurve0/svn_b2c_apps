package com.pay1;

import java.net.URLEncoder;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.google.android.gcm.GCMRegistrar;
import com.pay1.constants.Constants;
import com.pay1.customviews.MyProgressDialog;
import com.pay1.requesthandler.RequestClass;
import com.pay1.utilities.ArrayHelper;
import com.pay1.utilities.EditTextValidator;
import com.pay1.utilities.MyLocation;
import com.pay1.utilities.MyLocation.LocationResult;
import com.pay1.utilities.Utility;

public class LoginActivity extends Activity {

	private static final String SCREEN_LABEL = "Login Screen";
	private EasyTracker easyTracker = null;

	private final String TAG = "Login";

	private TextView textView_Title, textView_ForgotPin, textView_OR,
			textView_Skip, textView_TC, textView_PP, textView_main,
			textView_and;
	private EditText editText_MobileNumber, editText_Pin;
	private Button button_Login, button_SignUp;
	private Double longitude, latitude;
	private String uuid, version, manufacturer;

	// private LinearLayout linearLayout_Parent;

	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_activity);
		try {
			easyTracker = EasyTracker.getInstance(LoginActivity.this);
			easyTracker.set(Fields.SCREEN_NAME, SCREEN_LABEL);

			Typeface Normal = Typeface.createFromAsset(getAssets(),
					"EncodeSansNormal-100-Thin.ttf");

			if (Utility.getIsNew(LoginActivity.this,
					Constants.SHAREDPREFERENCE_IS_NEW)) {
				Utility.setIsNew(LoginActivity.this,
						Constants.SHAREDPREFERENCE_IS_NEW, false);
				startActivity(new Intent(LoginActivity.this,
						SignUpActivity.class));
				finish();
			} else if (Utility.getLoginFlag(LoginActivity.this,
					Constants.SHAREDPREFERENCE_IS_LOGIN)) {
				startActivity(new Intent(LoginActivity.this,
						SplashScreenActivity.class));
				finish();

			}
			// else if (Utility.getMobileNumber(LoginActivity.this,
			// Constants.SHAREDPREFERENCE_MOBILE) != null
			// && Utility.getPin(LoginActivity.this,
			// Constants.SHAREDPREFERENCE_PIN) != null) {
			//
			// try {
			// TelephonyManager tManager = (TelephonyManager)
			// getSystemService(Context.TELEPHONY_SERVICE);
			// uuid = tManager.getDeviceId();
			// if (uuid == null) {
			// uuid = Secure.getString(
			// LoginActivity.this.getContentResolver(),
			// Secure.ANDROID_ID);
			// }
			// } catch (Exception exception) {
			// }
			// version = android.os.Build.VERSION.RELEASE;
			// manufacturer = android.os.Build.MANUFACTURER;
			//
			// new LoginTask().execute(Utility.getMobileNumber(
			// LoginActivity.this, Constants.SHAREDPREFERENCE_MOBILE),
			// Utility.getPin(LoginActivity.this,
			// Constants.SHAREDPREFERENCE_PIN), uuid, String
			// .valueOf(latitude), String.valueOf(longitude),
			// manufacturer, version);
			// }
			else {

				textView_Title = (TextView) findViewById(R.id.textView_Title);
				textView_Title.setTypeface(Normal);
				textView_OR = (TextView) findViewById(R.id.textView_OR);
				textView_OR.setTypeface(Normal);
				textView_ForgotPin = (TextView) findViewById(R.id.textView_ForgotPin);
				textView_ForgotPin.setTypeface(Normal);
				textView_ForgotPin
						.setOnClickListener(new View.OnClickListener() {

							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								Intent intent = new Intent(LoginActivity.this,
										ForgotPasswordActivity.class);
								startActivity(intent);
							}
						});

				textView_main = (TextView) findViewById(R.id.textView_main);
				textView_main.setTypeface(Normal);
				textView_and = (TextView) findViewById(R.id.textView_and);
				textView_and.setTypeface(Normal);
				textView_TC = (TextView) findViewById(R.id.textView_TC);
				textView_TC.setTypeface(Normal);
				textView_TC.setPaintFlags(textView_TC.getPaintFlags()
						| Paint.UNDERLINE_TEXT_FLAG);
				textView_TC.setText("Terms & Conditions");
				textView_TC.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						Intent intent = new Intent(LoginActivity.this,
								TCActivity.class);
						startActivity(intent);
					}
				});

				textView_PP = (TextView) findViewById(R.id.textView_PP);
				textView_PP.setTypeface(Normal);
				textView_PP.setPaintFlags(textView_TC.getPaintFlags()
						| Paint.UNDERLINE_TEXT_FLAG);
				textView_PP.setText("Privacy Policy.");
				textView_PP.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						Intent intent = new Intent(LoginActivity.this,
								PPActivity.class);
						startActivity(intent);
					}
				});

				final Animation animationFadeIn = AnimationUtils.loadAnimation(
						this, R.anim.fadein);
				textView_Skip = (TextView) findViewById(R.id.textView_Skip);
				textView_Skip.setTypeface(Normal);
				textView_Skip.startAnimation(animationFadeIn);
				textView_Skip.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

						// if (Utility.isFirstTime(LoginActivity.this)) {
						// Intent i = new Intent(LoginActivity.this,
						// TourActivity.class);
						// startActivity(i);
						// } else {
						Intent i = new Intent(LoginActivity.this,
								RechargeMobileActivity.class);
						i.putExtra(Constants.RECHARGE_FOR,
								Constants.RECHARGE_MOBILE);
						startActivity(i);
						// }
						finish();
					}
				});

				editText_MobileNumber = (EditText) findViewById(R.id.editText_MobileNumber);
				InputMethodManager imm = (InputMethodManager) LoginActivity.this
						.getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(
						editText_MobileNumber.getWindowToken(),
						InputMethodManager.HIDE_IMPLICIT_ONLY);
				editText_MobileNumber.setTypeface(Normal);
				editText_MobileNumber.addTextChangedListener(new TextWatcher() {

					@Override
					public void onTextChanged(CharSequence s, int start,
							int before, int count) {
						// TODO Auto-generated method stub
						EditTextValidator.hasTextLogin(LoginActivity.this,
								editText_MobileNumber,
								Constants.ERROR_MOBILE_BLANK_FIELD);
						if (editText_MobileNumber.length() == 10) {
							editText_Pin.requestFocus();
						}
					}

					@Override
					public void beforeTextChanged(CharSequence s, int start,
							int count, int after) {
						// TODO Auto-generated method stub

					}

					@Override
					public void afterTextChanged(Editable s) {
						// TODO Auto-generated method stub

					}
				});
				editText_Pin = (EditText) findViewById(R.id.editText_Pin);
				editText_Pin.setTypeface(Normal);
				editText_Pin.addTextChangedListener(new TextWatcher() {

					@Override
					public void onTextChanged(CharSequence s, int start,
							int before, int count) {
						// TODO Auto-generated method stub
						EditTextValidator.hasTextLogin(LoginActivity.this,
								editText_Pin,
								Constants.ERROR_LOGIN_PIN_BLANK_FIELD);

					}

					@Override
					public void beforeTextChanged(CharSequence s, int start,
							int count, int after) {
						// TODO Auto-generated method stub

					}

					@Override
					public void afterTextChanged(Editable s) {
						// TODO Auto-generated method stub

					}
				});

				editText_Pin
						.setOnEditorActionListener(new TextView.OnEditorActionListener() {

							@Override
							public boolean onEditorAction(TextView v,
									int actionId, KeyEvent event) {
								// TODO Auto-generated method stub
								if (actionId == EditorInfo.IME_ACTION_GO) {
									try {
										if (checkValidation(LoginActivity.this)) {

											InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
											imm.hideSoftInputFromWindow(
													v.getWindowToken(), 0);
											new LoginTask()
													.execute(
															editText_MobileNumber
																	.getText()
																	.toString()
																	.trim(),
															editText_Pin
																	.getText()
																	.toString()
																	.trim(),
															Utility.getUUID(
																	LoginActivity.this,
																	Constants.SHAREDPREFERENCE_UUID),
															String.valueOf(latitude),
															String.valueOf(longitude),
															manufacturer,
															version,
															Utility.getGCMID(
																	LoginActivity.this,
																	Constants.SHAREDPREFERENCE_GCMID));
										}
									} catch (Exception e) {
										// e.printStackTrace();
									}
									return true;
								}
								return false;
							}
						});

				try {
					TelephonyManager tManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
					uuid = tManager.getDeviceId();
					if (uuid == null) {
						uuid = Secure.getString(
								LoginActivity.this.getContentResolver(),
								Secure.ANDROID_ID);
					}
					if (Utility.getUUID(LoginActivity.this,
							Constants.SHAREDPREFERENCE_UUID) == null)
						Utility.setUUID(LoginActivity.this,
								Constants.SHAREDPREFERENCE_UUID, uuid);
				} catch (Exception exception) {
				}
				version = android.os.Build.VERSION.RELEASE;
				manufacturer = android.os.Build.MANUFACTURER;

				try {
					LocationResult locationResult = new LocationResult() {
						@Override
						public void gotLocation(Location location) {
							// Got the location!
							try {
								// //// Log.w("Location", location + "");
								longitude = location.getLongitude();
								latitude = location.getLatitude();

								Utility.setCurrentLatitude(
										LoginActivity.this,
										Constants.SHAREDPREFERENCE_CURRENT_LATITUDE,
										String.valueOf(location.getLatitude()));
								Utility.setCurrentLongitude(
										LoginActivity.this,
										Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE,
										String.valueOf(location.getLongitude()));

								// Geocoder geocoder;
								// List<Address> addresses;
								// geocoder = new Geocoder(LoginActivity.this,
								// Locale.getDefault());
								// addresses =
								// geocoder.getFromLocation(lattitude,
								// longitude, 1);

								// String address =
								// addresses.get(0).getAddressLine(0);
								// String city =
								// addresses.get(0).getAddressLine(1);
								// String country =
								// addresses.get(0).getAddressLine(2);

								// // Log.w("Address: ", "Add : " + address +
								// " City :"
								// + city + " Country :" + country);
							} catch (Exception exception) {
							}
						}
					};
					MyLocation myLocation = new MyLocation();
					myLocation.getLocation(this, locationResult);
				} catch (Exception exception) {
				}

				button_SignUp = (Button) findViewById(R.id.button_SignUp);
				button_SignUp.setTypeface(Normal);
				button_SignUp.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						startActivity(new Intent(LoginActivity.this,
								SignUpActivity.class));
					}
				});

				button_Login = (Button) findViewById(R.id.button_Login);
				button_Login.setTypeface(Normal);
				button_Login.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if (checkValidation(LoginActivity.this)) {
							InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
							imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

							/*
							 * ArrayHelper arrayHelper=new
							 * ArrayHelper(LoginActivity.this);
							 * arrayHelper.saveArray(Constants.CONATACT_JSON,
							 * Constants.getAllContacts(LoginActivity.this));
							 */
							new LoginTask().execute(editText_MobileNumber
									.getText().toString().trim(), editText_Pin
									.getText().toString().trim(), Utility
									.getUUID(LoginActivity.this,
											Constants.SHAREDPREFERENCE_UUID),
									String.valueOf(latitude), String
											.valueOf(longitude), manufacturer,
									version, Utility.getGCMID(
											LoginActivity.this,
											Constants.SHAREDPREFERENCE_GCMID));
						}
					}
				});

				GCMRegistrar.checkDevice(this);
				GCMRegistrar.checkManifest(this);
				checkNotNull(Constants.SENDER_ID, "SENDER_ID");

				final String regId = GCMRegistrar.getRegistrationId(this);
				// // Log.i(TAG, "registration id =====  " + regId);
				Utility.setGCMID(LoginActivity.this,
						Constants.SHAREDPREFERENCE_GCMID, regId);
				if (regId.equals("")) {
					GCMRegistrar.register(this, Constants.SENDER_ID);
				} else {
					// // Log.v(TAG, "Already registered");
				}

				// linearLayout_Parent = (LinearLayout)
				// findViewById(R.id.linearLayout_Parent);

				// final View activityRootView =
				// findViewById(R.id.linearLayout_Parent);
				// activityRootView.getViewTreeObserver()
				// .addOnGlobalLayoutListener(
				// new OnGlobalLayoutListener() {
				// @Override
				// public void onGlobalLayout() {
				// int heightDiff = activityRootView
				// .getRootView().getHeight()
				// - activityRootView.getHeight();
				// if (heightDiff > 100) { // if more than
				// // 100 pixels,
				// // its probably
				// // a keyboard...
				// int[] viewLocation = new int[2];
				// editText_MobileNumber
				// .getLocationInWindow(viewLocation);
				//
				// int x = editText_MobileNumber
				// .getLeft();
				// int y = editText_MobileNumber
				// .getTop();
				// Log.w("Coordi ", x
				// + "             " + y);
				// linearLayout_Parent.setPadding(0,
				// -y, 0, 0);
				// } else {
				// linearLayout_Parent.setPadding(0,
				// 0, 0, 0);
				// }
				// }
				// });
			}
		} catch (Exception e) {
		}
	}

	private void checkNotNull(Object reference, String name) {
		if (reference == null) {
			throw new NullPointerException("Errror");
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView().build());
		// EasyTracker.getInstance(this).activityStart(this);
		easyTracker.send(MapBuilder.createAppView().build());
	}

	@Override
	public void onStop() {
		super.onStop();
		// EasyTracker.getInstance(this).activityStop(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
	}

	private boolean checkValidation(Context context) {
		boolean ret = true;

		if (!EditTextValidator.hasTextLogin(context, editText_MobileNumber,
				Constants.ERROR_MOBILE_BLANK_FIELD)) {
			ret = false;
			editText_MobileNumber.requestFocus();
			return ret;
		} else if (!EditTextValidator.isValidMobileNumberLogin(context,
				editText_MobileNumber, Constants.ERROR_MOBILE_LENGTH_FIELD)) {
			ret = false;
			editText_MobileNumber.requestFocus();
			return ret;
		} else if (!EditTextValidator.hasTextLogin(context, editText_Pin,
				Constants.ERROR_LOGIN_PIN_BLANK_FIELD)) {
			ret = false;
			editText_Pin.requestFocus();
			return ret;
		} else if (!EditTextValidator.isValidPinLogin(context, editText_Pin,
				Constants.ERROR_LOGIN_PIN_VALID_FIELD)) {
			ret = false;
			editText_Pin.requestFocus();
			return ret;
		} else
			return ret;
	}

	public class LoginTask extends AsyncTask<String, String, String> implements
			OnDismissListener {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			try {
				try {
					long LastUpdateTime = Utility.getLongSharedPreferences(
							LoginActivity.this, Constants.CONTACT_UPDATE_TIME);

					int days = (int) ((System.currentTimeMillis() - LastUpdateTime) / (1000 * 60 * 60 * 24));

					if (days >= 2) {
						ArrayHelper arrayHelper = new ArrayHelper(
								LoginActivity.this);
						arrayHelper.clearArray(Constants.CONATACT_JSON);
						arrayHelper.saveArray(Constants.CONATACT_JSON,
								Constants.getAllContacts(LoginActivity.this));
					}
				} catch (Exception e) {
				}
				String response = RequestClass.getInstance()
						.readPay1B2CRequest(
								LoginActivity.this,
								Constants.B2C_URL + "signin/?username="
										+ URLEncoder.encode(params[0], "utf-8")
										+ "&password="
										+ URLEncoder.encode(params[1], "utf-8")
										+ "&uuid=" + params[2] + "&latitude="
										+ params[3] + "&longitude=" + params[4]
										+ "&manufacturer=" + params[5]
										+ "&os_info=" + params[6]
										+ "&gcm_reg_id=" + params[7]
										+ "&device_type=android");

				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			try {

				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONObject jsonObject = new JSONObject(replaced);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {
						Utility.setLoginFlag(LoginActivity.this,
								Constants.SHAREDPREFERENCE_IS_LOGIN, true);
						JSONObject jsonObject2 = jsonObject
								.getJSONObject("description");

						Utility.setUserName(LoginActivity.this,
								Constants.SHAREDPREFERENCE_NAME, jsonObject2
										.getString("name").trim());
						Utility.setMobileNumber(LoginActivity.this,
								Constants.SHAREDPREFERENCE_MOBILE, jsonObject2
										.getString("mobile").trim());
						Utility.setEmail(LoginActivity.this,
								Constants.SHAREDPREFERENCE_EMAIL, jsonObject2
										.getString("email").trim());
						Utility.setGender(LoginActivity.this,
								Constants.SHAREDPREFERENCE_GENDER, jsonObject2
										.getString("gender").trim());
						Utility.setUserID(LoginActivity.this,
								Constants.SHAREDPREFERENCE_USER_ID, jsonObject2
										.getString("user_id").trim());
						Utility.setBalance(LoginActivity.this,
								Constants.SHAREDPREFERENCE_BALANCE, jsonObject2
										.getString("wallet_balance").trim());
						Utility.setPin(LoginActivity.this,
								Constants.SHAREDPREFERENCE_PIN, editText_Pin
										.getText().toString().trim());
						Utility.setDOB(LoginActivity.this,
								Constants.SHAREDPREFERENCE_DOB, jsonObject2
										.getString("date_of_birth").trim());
						Utility.setLatitude(LoginActivity.this,
								Constants.SHAREDPREFERENCE_LATITUDE,
								jsonObject2.getString("latitude").trim());
						Utility.setLongitude(LoginActivity.this,
								Constants.SHAREDPREFERENCE_LONGITUDE,
								jsonObject2.getString("longitude").trim());
						Utility.setSupportNumber(LoginActivity.this,
								Constants.SHAREDPREFERENCE_SUPPORT_NUMBER,
								jsonObject2.getString("support_number").trim());

						Utility.setSharedPreference(
								LoginActivity.this,
								Constants.SHAREDPREFERENCE_SERVICE_CHARGE_PERCENT,
								jsonObject2.getString("service_charge_percent")
										.trim());

						Utility.setSharedPreference(LoginActivity.this,
								Constants.SHAREDPREFERENCE_SERVICE_TAX_PERCENT,
								jsonObject2.getString("service_tax_percent")
										.trim());
						// if (Utility.isFirstTime(LoginActivity.this)) {
						// Intent i = new Intent(LoginActivity.this,
						// TourActivity.class);
						//
						// startActivity(i);
						// } else {
						Intent i = new Intent(LoginActivity.this,
								RechargeMobileActivity.class);
						i.putExtra(Constants.RECHARGE_FOR,
								Constants.RECHARGE_MOBILE);
						startActivity(i);
						// }

						finish();

					} else {
						Constants.showOneButtonDialog(LoginActivity.this, TAG,
								Constants.checkCode(replaced),
								Constants.DIALOG_CLOSE);
					}

				} else {
					// Constants.showOneButtonDialog(LoginActivity.this, TAG,
					// Constants.ERROR_INTERNET, Constants.DIALOG_CLOSE);
					Intent intent = new Intent(LoginActivity.this,
							ConnectivityErrorActivity.class);
					intent.putExtra(Constants.RECHARGE_FOR,
							Constants.WALLET_PAYMENT);
					startActivity(intent);

				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonDialog(LoginActivity.this, TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonDialog(LoginActivity.this, TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(LoginActivity.this);
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							LoginTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			LoginTask.this.cancel(true);
			dialog.cancel();
		}

	}

}