﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

using System.Diagnostics;
using System.Windows.Threading;
using System;
using Pay1B2C.Model;

using Pay1B2C;
using System.Windows.Media;
using Pay1B2C.UserControls;
using System.Collections.ObjectModel;
using Pay1B2C.Model;
using SQLite;
using Pay1B2C.ViewModel;

namespace Pay1B2C.Views
    {
    public partial class PlansActivity: PhoneApplicationPage
        {
        Plans CirclePlans = new Plans();
        //SQLiteAsyncConnection conn;
        PivotItem pvt;
        SQLiteConnection db = null;
       
        int i = 0;
        string[] codeArray;
        ProgressBarManager ProgressBarManager;
        DispatcherTimer newTimer;
        string[] circleArray;
        string area_name,op_name;
        string op_id;
        string code;
        public PlansActivity()
            {
            InitializeComponent();
            CreateDatabase();
            SlideTransition transition = new SlideTransition();
            transition.Mode = SlideTransitionMode.SlideRightFadeIn;
            PhoneApplicationPage phonePage = (PhoneApplicationPage)(((PhoneApplicationFrame)Application.Current.RootVisual)).Content;

            ITransition trans = transition.GetTransition(phonePage);
            trans.Completed += delegate { trans.Stop(); };
            trans.Begin();
            GoogleAnalytics.EasyTracker.GetTracker().SendView("PlansActivity");
            ProgressIndicator prog = new ProgressIndicator();
            ProgressBarManager = new ProgressBarManager();
            SystemTray.SetProgressIndicator(this, prog);
            ProgressBarManager.Hook(prog);
            string flag = PhoneApplicationService.Current.State[Constant.OPERATOR_RECHARGE_FLAG].ToString();
           
            call_event.Tap += (s, e) =>
            {

              //  NavigationService.Navigate(new Uri("/Views/SelectCircle.xaml?op_id=" + op_id + "&op_name=", UriKind.RelativeOrAbsolute));
            NavigationService.Navigate(new Uri("/Views/SelectCircle.xaml?code=" + code + "&op_id=" + op_id + "&cir_name=" + area_name + "&op_name="+op_name, UriKind.RelativeOrAbsolute));
                //NavigationService.RemoveBackEntry();
            };

          /*  if(PhoneApplicationService.Current.State.ContainsKey("area"))
                {
                string area = PhoneApplicationService.Current.State["area"].ToString();
                if(string.IsNullOrEmpty(area))
                    {
                    textSupportMsg.Text = "Select Circle";
                    }
                else
                    {
                    textSupportMsg.Text = getAreaName(area);
                    }

                }
            else
                {
                textSupportMsg.Text = "Select Circle";
                }*/
            }

        private string getAreaName(string area_name1)
            {
            string areaName = "Select Circle";
            try
                {
                var dict = (JArray)JsonConvert.DeserializeObject(Constant.jsonCircle);

                foreach(var item in dict)
                    {
                    var code = item["code"];
                    var name = item["name"];
                    var id = item["id"];
                    if(area_name1.Equals(code.ToString()))
                        {
                        areaName = name.ToString();
                        }
                    else
                        {
                        }
                    }

                }
            catch(Exception e)
                {
                Console.WriteLine(e.StackTrace);
                }

            return areaName;
            }



        protected override void OnNavigatedTo(NavigationEventArgs e)
            {
            /*
            if(NavigationContext.QueryString.ContainsKey("fromCircle"))
                {
                code = NavigationContext.QueryString["code"];
                string area_name = NavigationContext.QueryString["cir_name"];

                if(string.IsNullOrEmpty(area_name))
                    {
                    textSupportMsg.Text = "Select Circle";
                    }
                else
                    {
                    textSupportMsg.Text = area_name;// getAreaName(area);
                    }

                getData();

                }
            else
                {


            */
               


            if(NavigationContext.QueryString.ContainsKey("op_id") && NavigationContext.QueryString.ContainsKey("code") && NavigationContext.QueryString.ContainsKey("op_name") && NavigationContext.QueryString.ContainsKey("cir_name"))
                {
                op_id = NavigationContext.QueryString["op_id"];
                code = NavigationContext.QueryString["code"];
                area_name = NavigationContext.QueryString["cir_name"];
                op_name=NavigationContext.QueryString["op_name"];
                opeNcircle.Text = op_name+ "(" + area_name + ")";

                
                    textTitlePath.Text = ""+NavigationContext.QueryString["op_name"];
                 
                getData();



                if(string.IsNullOrEmpty(area_name))
                        {
                        textSupportMsg.Text = "Select Circle";
                        }
                    else
                        {
                        textSupportMsg.Text = area_name;
                        }

                }
           // }
            base.OnNavigatedTo(e);
            }

        public async Task<string> MakeWebRequest(string apiUrl)
            {
            //Constant.SetProgressIndicator(true);

            ProgressBarManager.Show(16, "Please wait...", false, 10);
            String result = "";
            var httpClient = new HttpClient(new HttpClientHandler());

            HttpResponseMessage response = await httpClient.PostAsync(apiUrl, null);
            response.EnsureSuccessStatusCode();
            var responseString = await response.Content.ReadAsStringAsync();
            int count = 0;
            try
                {
                if(string.IsNullOrEmpty(responseString))
                    {
                    NavigationService.Navigate(new Uri("/Views/NoConnection.xaml?", UriKind.Relative));
                    }
                else
                    {
                    string output = responseString.Remove(responseString.Length - 1, 1);
                    string output1 = output.Remove(output.Length - 1, 1);
                    string output2 = output1.Remove(0, 1);
                    string output3 = output2.Remove(0, 1);
                    string output4 = output3.Remove(output3.Length - 1, 1);
                    var dict = (JObject)JsonConvert.DeserializeObject(output4);
                    var desc = dict[op_id];


                    var prod_code_pay1 = desc["prod_code_pay1"];
                    CirclePlans.planOpertorID = prod_code_pay1.ToString();

                    var opr_name = desc["opr_name"];
                    CirclePlans.planOperatorName = opr_name.ToString();

                    var circles = desc["circles"];



                    var BR = circles[code];
                    var circle_id = BR["circle_id"];
                    CirclePlans.planCircleID = circle_id.ToString();
                    var circle_name = BR["circle_name"];
                    CirclePlans.planCircleName = circle_name.ToString();
                    var plans = BR["plans"];
                    JObject jObject = (JObject)JsonConvert.DeserializeObject(plans.ToString());
                    Dictionary<string, string> res = new Dictionary<string, string>(jObject.Count);
                    foreach(var kvp in jObject)
                        {
                        var planType = kvp.Key;
                        var planDetails = kvp.Value;
                        CirclePlans.planType = planType.ToString();
                        foreach(var plansDetails in planDetails)
                            {
                            count++;
                            var plan_amt = plansDetails["plan_amt"];
                            CirclePlans.planAmount = Convert.ToInt32(plansDetails["plan_amt"].ToString());
                            var plan_validity = plansDetails["plan_validity"];
                            CirclePlans.planValidity = plan_validity.ToString();
                            var plan_desc = plansDetails["plan_desc"];
                            CirclePlans.planDescription = plan_desc.ToString();
                            CirclePlans.planUptateTime = DateTime.Now;//.ToString();
                            // await conn.InsertAsync(CirclePlans);

                         /*   SQLiteCommand cmd = db.CreateCommand("");

                            cmd.CommandText = " Insert into Plans (planAmount,planOpertorID,planOperatorName,planCircleID,planCircleName,planType,planValidity,planDescription,planUptateTime) values (@planAmount,@planOpertorID,@planOperatorName,@planCircleID,@planCircleName,@planType,@planValidity,@planDescription,@planUptateTime)";
                            try
                                {
                                int rec = cmd.ExecuteNonQuery(CirclePlans);
                                }
                            catch(SQLiteException sed)
                                {
                                string mms = sed.Message;
                                }*/

                            PlanDataHelper plansdataHelper=new PlanDataHelper();
                            plansdataHelper.Insert(CirclePlans);


                            }
                        }

                    }
                }
            catch(Exception e)
                {
                Console.WriteLine("");
                }

            if(count == 0 && count == 1)
                {
                MessageBox.Show("No Plans Available.");
                NavigationService.GoBack();
                }
            else
                {
                getData();
                }

            ProgressBarManager.Hide(16);
            //Constant.SetProgressIndicator(false);
            return result;

            }

        private void CreateDatabase()
            {
            if(db == null)
                {
                db = new SQLiteConnection(Constant.DATABASE_NAME);
               // db.Open();
                try
                    {
                    SQLiteCommand cmd = db.CreateCommand("Create table Plans (planAmount int,planOpertorID text,planOperatorName text,planCircleID text,planCircleName text,planType text,planValidity text,planDescription text,planUptateTime datetime)");
                    int i = cmd.ExecuteNonQuery();

                    }
                catch(SQLiteException ex)
                    {

                    }
                }
            }



        public void getData()
            {
            ProgressBarManager.Show(16, "Please wait...", false, 10);
            string planStr = "SELECT * FROM Plans WHERE planCircleID='" + code + "' AND show_flag='1' AND planOpertorID='" + op_id + "' ORDER BY planType DESC,planAmount ASC";
           /* ObservableCollection<Plans> PlanResult = (Application.Current as App).dbh.SelectObservableCollection<Plans>(planStr);
            if(PlanResult.Count < 1)
                {
                string str_select = "SELECT * FROM AllPlans WHERE allPlanCircleID='" + code + "' AND allPlanOpertorID='" + op_id + "'";

                ObservableCollection<AllPlans> allResult = (Application.Current as App).dbh.SelectObservableCollection<AllPlans>(str_select);
                if(allResult.Count != 0)
                    {
                    insertIntoPlansDB(allResult);
                    }
                }
            */

            List<Plans> result = null;
            try
                {
                if(db != null)
                    {
                    try
                        {
                       /* SQLiteCommand cmd1 = db.CreateCommand("SELECT * FROM Plans WHERE planCircleID='" + code + "' AND planOpertorID='" + op_id + "' ORDER BY planType DESC,planAmount ASC");
                        var lst = cmd1.ExecuteQuery<Plans>();
                        result = lst.ToList();
                        if(result.Count == 1)
                            {
                            }*/


                        PlanDataHelper planDataHelper=new PlanDataHelper();
                        
                        var lst=planDataHelper.ReadPlans(op_id, code);
                        result = lst.ToList();
                        }
                    catch(SQLiteException se)
                        {
                        string see = se.Message;
                        }
                    }
                if(result.Count == 0)
                    {

                    Task<string> abc = MakeWebRequest(Constant.B2B_URL + "method=getPlanDetails&operator=" + op_id + "&circle=" + code);

                    }
                else
                    {
                    var item1 = result[0];
                    DateTime currentDate = DateTime.Now;//.ToString();
                    DateTime updatedDate = item1.planUptateTime;
                    TimeSpan duration = currentDate - updatedDate;
                    if(duration.Days >= 2)
                        {
                        SQLiteCommand cmd2 = db.CreateCommand("DELETE FROM Plans WHERE planCircleID='" + code + "' AND planOpertorID='" + op_id + "'");
                        int i = cmd2.ExecuteNonQuery();
                        Task<string> abc = MakeWebRequest(Constant.B2B_URL + "method=getPlanDetails&operator=" + op_id + "&circle=" + code);
                        }
                    else
                        {
                        List<PlanUserControl>[] a = new List<PlanUserControl>[25];
                        List<String> ptype = new List<String>();
                        foreach(var item in result)
                            {
                            PlanUserControl row = new PlanUserControl();
                            row.Validity.Text = item.planValidity;

                            row.amount.Text = "" + item.planAmount;
                            row.PlanDesc.Text = item.planDescription;
                            string typr = item.planType;
                            //L1.FindName(item.planType);
                            int x = 0;

                            if(!ptype.Contains(typr))//ptype does not contain itrType
                                {
                                ptype.Add(typr);
                                x = ptype.IndexOf(typr);
                                a[x] = new List<PlanUserControl>();

                                }
                            else
                                {
                                x = ptype.IndexOf(typr);
                                }
                            a[x].Add(row);

                            i++;

                            //L1.Items.Add(row);
                            }
                        //Pivot myPivot = new Pivot();
                        System.Diagnostics.Debug.WriteLine(a);
                        PivotItem myNewPivotItem;
                        for(int j = 0;j < a.Count();j++)
                            {
                            if(a[j] != null)
                                {

                                TextBlock tb = new TextBlock();
                                tb.Text = ptype[j];
                                tb.Foreground = new SolidColorBrush(Colors.Blue);
                                tb.FontSize = 60;
                                pvt = new PivotItem();
                                pvt.Header = ptype[j];


                                var stack = new StackPanel();
                                pvt.Content = stack;

                                Grid Gird_Root = new Grid();

                                PivotItem PI = new PivotItem() { Content = Gird_Root };

                                PI.Header = ptype[j];




                                Grid myNewGrid = new Grid();
                                myNewGrid.Margin = new Thickness(0, 0, 0, 0);
                                ListBox listPlan = new ListBox();
                                //listPlan.Height = 730;
                                listPlan.ItemsSource = a[j];
                                myNewGrid.Children.Add(listPlan);

                                PI.Content = myNewGrid;
                                myPivot.Items.Add(PI);
                                pvt = null;
                                listPlan.SelectionChanged += new SelectionChangedEventHandler(listBox1_SelectedIndexChanged);

                                }
                            }
                        }
                    }
                }
            catch(Exception e2)
                {
                //Debug.WriteLine(e2.StackTrace);
                /* MessageBox.Show("No Plans Availables");*/
                /*NavigationService.Navigate(new Uri("/RechargeMobile.xaml?id="+op_id, UriKind.Relative));
                NavigationService.RemoveBackEntry();*/
                }


            if(i == 1)
                {
                MessageBox.Show("No Plans Available.");


                NavigationService.GoBack();

                }
            else
                {
                }
            ProgressBarManager.Hide(16);
            }

        private void insertIntoPlansDB(ObservableCollection<AllPlans> allResult)
            {

            var prod_code_pay1 = allResult.First().allPlanOpertorID;
            CirclePlans.planOpertorID = prod_code_pay1.ToString();

            var opr_name =  allResult.First().allPlanOperatorName;
            CirclePlans.planOperatorName = opr_name.ToString();

            CirclePlans.planCircleID = allResult.First().allPlanCircleID;

            CirclePlans.planCircleName = allResult.First().allPlanCircleName;

            var plans=allResult.First().allCirclePlans;
            JObject jObject = (JObject)JsonConvert.DeserializeObject(plans.ToString());
            Dictionary<string, string> res = new Dictionary<string, string>(jObject.Count);
            foreach(var kvp in jObject)
                {
                var planType = kvp.Key;
                var planDetails = kvp.Value;
                CirclePlans.planType = planType.ToString();
                foreach(var plansDetails in planDetails)
                    {
                    //count++;
                    var plan_amt = plansDetails["plan_amt"];
                    CirclePlans.planAmount = Convert.ToInt32(plansDetails["plan_amt"].ToString());
                    var plan_validity = plansDetails["plan_validity"];
                    CirclePlans.planValidity = plan_validity.ToString();
                    var plan_desc = plansDetails["plan_desc"];
                    CirclePlans.planDescription = plan_desc.ToString();
                    CirclePlans.planUptateTime = DateTime.Now;//.ToString();
                    // await conn.InsertAsync(CirclePlans);
                    CirclePlans.show_flag=plansDetails["show_flag"].ToString();
                    SQLiteCommand cmd = db.CreateCommand("");

                  
                    try
                        {
                      //  int rec = cmd.ExecuteNonQuery(CirclePlans);
                        }
                    catch(SQLiteException sed)
                        {
                        string mms = sed.Message;
                        }

                    }
                }

            }

        private void listBox1_SelectedIndexChanged(object sender, SelectionChangedEventArgs e)
            {
            PlanUserControl control = (sender as ListBox).SelectedItem as PlanUserControl;
            string validity = control.Validity.Text;
            string desc = control.PlanDesc.Text;
            string amt = control.amount.Text;
            //NavigationService.Navigate (new Uri ("/RechargeAndBills.xaml?amt=" + amt + "&validity=" + validity + "&desc=" + desc + "&op_id=" + op_id + "&data=1", UriKind.RelativeOrAbsolute));
            //NavigationService.RemoveBackEntry ();
            NavigationService.GoBack();
            PhoneApplicationService.Current.State["recAmt"] = amt;

            }

        private void buttonImage_Click_back(object sender, RoutedEventArgs e)
            {
            NavigationService.GoBack();
            }
        }
    }