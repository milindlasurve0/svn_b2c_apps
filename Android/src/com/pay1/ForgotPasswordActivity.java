package com.pay1;

import java.net.URLEncoder;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.pay1.customviews.MyProgressDialog;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;

import com.pay1.constants.Constants;
import com.pay1.requesthandler.RequestClass;
import com.pay1.utilities.EditTextValidator;

public class ForgotPasswordActivity extends AppBaseActivity {

	private static final String SCREEN_LABEL = "Forgot Pin Screen";
	private EasyTracker easyTracker = null;
	private final String TAG = "Forgot Pin";
	private ImageView imageView_Back, imageView_Ok, imageView_Cancel;
	private EditText editText_Mobile;

	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.forgot_password_activity);
		try {
			easyTracker = EasyTracker.getInstance(ForgotPasswordActivity.this);
			easyTracker.set(Fields.SCREEN_NAME, SCREEN_LABEL);
			registerBaseActivityReceiver();

			Typeface Normal = Typeface.createFromAsset(getAssets(),
					"EncodeSansNormal-100-Thin.ttf");

			editText_Mobile = (EditText) findViewById(R.id.editText_Mobile);
			editText_Mobile.setTypeface(Normal);
			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(editText_Mobile.getWindowToken(), 0);
			editText_Mobile.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					// TODO Auto-generated method stub
					if (editText_Mobile.getText().toString().trim().length() > 0)
						EditTextValidator.hasText(ForgotPasswordActivity.this,
								editText_Mobile,
								Constants.ERROR_PIN_BLANK_FIELD);
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					// TODO Auto-generated method stub

				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

				}
			});

			imageView_Back = (ImageView) findViewById(R.id.imageView_Back);
			imageView_Back.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onBackPressed();
				}
			});

			imageView_Ok = (ImageView) findViewById(R.id.imageView_Ok);
			imageView_Ok.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (checkValidation(ForgotPasswordActivity.this)) {
						new ForgotPasswordTask().execute(editText_Mobile
								.getText().toString().trim());
					}
				}
			});

			imageView_Cancel = (ImageView) findViewById(R.id.imageView_Cancel);
			imageView_Cancel.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					editText_Mobile.setText("");
				}
			});

		} catch (Exception e) {
		}

		overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}

	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView().build());
		// EasyTracker.getInstance(this).activityStart(this);
		easyTracker.send(MapBuilder.createAppView().build());
	}

	@Override
	public void onStop() {
		super.onStop();
		// EasyTracker.getInstance(this).activityStop(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unRegisterBaseActivityReceiver();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (Constants.isExpanded) {
			Constants.collapsView(this);
		} else {
			super.onBackPressed();
			overridePendingTransition(R.anim.slide_in_left,
					R.anim.slide_out_right);
		}
	}

	private boolean checkValidation(Context context) {
		boolean ret = true;

		if (!EditTextValidator.hasText(context, editText_Mobile,
				Constants.ERROR_MOBILE_BLANK_FIELD)) {
			ret = false;
			editText_Mobile.requestFocus();
			return ret;
		} else if (!EditTextValidator.isValidMobileNumber(context,
				editText_Mobile, Constants.ERROR_MOBILE_LENGTH_FIELD)) {
			ret = false;
			editText_Mobile.requestFocus();
			return ret;
		} else
			return ret;
	}

	public class ForgotPasswordTask extends AsyncTask<String, String, String>
			implements OnDismissListener {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			try {

				String response = RequestClass
						.getInstance()
						.readPay1B2CRequest(
								ForgotPasswordActivity.this,
								Constants.B2C_URL + "forgotpwd/?user_name="
										+ URLEncoder.encode(params[0], "utf-8"));

				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			try {

				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONObject jsonObject = new JSONObject(replaced);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {

						Constants.showOneButtonDialog(
								ForgotPasswordActivity.this, TAG,
								jsonObject.getString("description"),
								Constants.DIALOG_CLOSE_CONFIRM);

					} else {
						Constants.showOneButtonDialog(
								ForgotPasswordActivity.this, TAG,
								Constants.checkCode(replaced),
								Constants.DIALOG_CLOSE_FORGOTPIN);
					}

				} else {
					// Constants.showOneButtonDialog(ForgotPasswordActivity.this,
					// TAG,
					// Constants.ERROR_INTERNET, Constants.DIALOG_CLOSE);
					Intent intent = new Intent(ForgotPasswordActivity.this,
							ConnectivityErrorActivity.class);
					intent.putExtra(Constants.RECHARGE_FOR,
							Constants.WALLET_PAYMENT);
					startActivity(intent);

				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonDialog(ForgotPasswordActivity.this, TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonDialog(ForgotPasswordActivity.this, TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(ForgotPasswordActivity.this);
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							ForgotPasswordTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			ForgotPasswordTask.this.cancel(true);
			dialog.cancel();
		}

	}

}
