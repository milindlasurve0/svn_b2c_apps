package com.pay1.adapterhandler;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.larvalabs.svgandroid.SVGParser;
import com.pay1.R;
import com.pay1.constants.Constants;

public class InfoAdapter extends BaseAdapter {

	private Context context;
	private ArrayList<String> list;
	private Typeface Reguler;
	private int flag; // 1 = Topup, 2= History

	public InfoAdapter(Context context, int flag, ArrayList<String> list) {
		this.context = context;
		this.list = list;
		this.flag = flag;

		Reguler = Typeface.createFromAsset(context.getAssets(),
				"EncodeSansNormal-400-Regular.ttf");

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@SuppressLint("NewApi")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		
		
		
		
		if (flag == 1) {

			if (convertView == null) {
				LayoutInflater infalInflater = (LayoutInflater) this.context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = infalInflater
						.inflate(R.layout.wallet_topup_adapter, null);
			}

			TextView textView_Text = (TextView) convertView
					.findViewById(R.id.textView_Text);
			textView_Text.setTypeface(Reguler);

			ImageView imageView_Arrow = (ImageView) convertView
					.findViewById(R.id.imageView_Arrow);
			ImageView imageView_Icon = (ImageView) convertView
					.findViewById(R.id.imageView_Icon);

			imageView_Arrow.setImageDrawable(SVGParser.getSVGFromResource(
					context.getResources(), R.raw.ic_next)
					.createPictureDrawable());

			switch (position) {
			case 0:
				imageView_Icon.setImageDrawable(SVGParser.getSVGFromResource(
						context.getResources(),
						R.raw.icon_locate_shop)
						.createPictureDrawable());
				break;
			case 1:
				imageView_Icon.setImageDrawable(SVGParser.getSVGFromResource(
						context.getResources(), R.raw.icon_online_card_payment)
						.createPictureDrawable());
				break;
			case 2:
				imageView_Icon.setImageDrawable(SVGParser.getSVGFromResource(
						context.getResources(), R.raw.icon_redeem_coupon)
						.createPictureDrawable());
				break;
			default:
				break;
			}

			if (Build.VERSION.SDK_INT >= 11) {
				
				imageView_Icon.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
			}

			textView_Text.setText(list.get(position));

		} else if (flag == 2) {
			if (convertView == null) {
				LayoutInflater infalInflater = (LayoutInflater) this.context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = infalInflater
						.inflate(R.layout.info_adapter, null);
			}

			TextView textView_Name = (TextView) convertView
					.findViewById(R.id.textView_Name);
			textView_Name.setTypeface(Reguler);

			ImageView imageView_Arrow = (ImageView) convertView
					.findViewById(R.id.imageView_Arrow);
			ImageView imageView_Icon = (ImageView) convertView
					.findViewById(R.id.imageView_Icon);

			imageView_Arrow.setImageDrawable(SVGParser.getSVGFromResource(
					context.getResources(), R.raw.ic_next)
					.createPictureDrawable());

			switch (position) {
			case 0:
				imageView_Icon.setImageDrawable(SVGParser.getSVGFromResource(
						context.getResources(), R.raw.ic_transaction_history)
						.createPictureDrawable());
				break;
			case 1:
				imageView_Icon.setImageDrawable(SVGParser.getSVGFromResource(
						context.getResources(), R.raw.ic_wallet_history)
						.createPictureDrawable());
				break;
			case 2:
				imageView_Icon.setImageDrawable(SVGParser.getSVGFromResource(
						context.getResources(), R.raw.ic_coin_history)
						.createPictureDrawable());
				break;
			default:
				break;
			}

			if (Build.VERSION.SDK_INT >= 11) {
				imageView_Arrow.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
				imageView_Icon.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
			}

			textView_Name.setText(list.get(position));
		}
		Constants.setFont(context, (ViewGroup)convertView);
		return convertView;
	}

}
