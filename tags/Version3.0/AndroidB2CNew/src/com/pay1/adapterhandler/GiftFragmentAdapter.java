package com.pay1.adapterhandler;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

import com.pay1.GiftCategoryFragment;
import com.pay1.GiftFragment;
import com.pay1.GiftLikeFragment;
import com.pay1.GiftNearFragment;
import com.pay1.MyFreeBiesFragment;

public class GiftFragmentAdapter extends FragmentStatePagerAdapter
// implements IconPagerAdapter
{
	protected static final String[] CONTENT = new String[] { "Gifts",
			"Near You", "Categories", "My Gifts", "My Likes" };
	
	private int mCount = CONTENT.length;

	private GiftLikeFragment like_frag = null;

	public GiftFragmentAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int position) {
		//
		switch (position) {
		case 0:
			return GiftFragment.newInstance();
		case 1:
			return GiftNearFragment.newInstance();
		case 2:
			return GiftCategoryFragment.newInstance();
		case 3:
			return MyFreeBiesFragment.newInstance();
		case 4:
			like_frag = GiftLikeFragment.newInstance();
			return like_frag;

		default:
			return GiftFragment.newInstance();
		}
	}

	@Override
	public int getCount() {
		return mCount;
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return GiftFragmentAdapter.CONTENT[position % CONTENT.length];
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * android.support.v4.app.FragmentStatePagerAdapter#instantiateItem(android
	 * .view.ViewGroup, int)
	 */
	@Override
	public Object instantiateItem(ViewGroup arg0, int arg1) {
		// TODO Auto-generated method stub
		try {
			if (like_frag != null)
				like_frag.loadData();
		} catch (Exception e) {
		}
		return super.instantiateItem(arg0, arg1);
	}
	// @Override
	// public int getIconResId(int index) {
	// return ICONS[index % ICONS.length];
	// }

	// public void setCount(int count) {
	// if (count > 0 && count <= 10) {
	// mCount = count;
	// notifyDataSetChanged();
	// }
	// }
}