//
//  GiftLocationList.h
//  Pay1
//
//  Created by Annapurna on 19/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GiftLocationList : NSObject

@property (nonatomic, strong) NSString *dealerContract;
@property (nonatomic, strong) NSString *dealID;
@property (nonatomic, strong) NSString *locationLat;
@property (nonatomic, strong) NSString *locationLong;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSString *state;
@property (nonatomic, strong) NSString *area;
@property (nonatomic, strong) NSString *fullAddress;
@property (nonatomic, strong) NSString *dealDistance;



@end
