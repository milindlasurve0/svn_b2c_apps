﻿using Pay1B2C.Model;
using SQLite;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pay1B2C.ViewModel
    {
    class DthPlanDataHelper
        {
        SQLiteConnection dbConn;


        public async Task<bool> onCreate(string DB_PATH)
            {
            try
                {
                if(!CheckFileExists(App.PAY1_DB_PATH).Result)
                    {
                    using(dbConn = new SQLiteConnection(App.PAY1_DB_PATH))
                        {
                        dbConn.CreateTable<DthPlanDB>();
                        }
                    }
                return true;
                }
            catch
                {
                return false;
                }
            }
        private async Task<bool> CheckFileExists(string fileName)
            {
            try
                {
                var store = await Windows.Storage.ApplicationData.Current.LocalFolder.GetFileAsync(fileName);
                return true;
                }
            catch
                {
                return false;
                }
            }


        public DthPlanDB ReadDthPlanDBByFlag(String operator1, String circle)
            {
            using(var dbConn = new SQLiteConnection(App.PAY1_DB_PATH))
                {
                var existingconact = dbConn.Query<DthPlanDB>("SELECT * FROM DthPlanDB WHERE dthPlanOpertorID='" + operator1 + "' ORDER BY dthPlanType DESC,dthPlanAmount ASC").FirstOrDefault();
                return existingconact;
                }
            }

        public ObservableCollection<DthPlanDB> ReadDthPlanDB(String op_code)
            {

            using(var dbConn = new SQLiteConnection(App.PAY1_DB_PATH))
                {

                List<DthPlanDB> myCollection =  dbConn.Query<DthPlanDB>("SELECT * FROM DthPlanDB WHERE dthPlanOpertorID='" + op_code + "' ORDER BY dthPlanType DESC,dthPlanAmount ASC").ToList();

               
                ObservableCollection<DthPlanDB> DthPlanDBList = new ObservableCollection<DthPlanDB>(myCollection.Distinct());
                return DthPlanDBList;
                }
            }


       
        public void Insert(DthPlanDB newcontact)
            {
            using(var dbConn = new SQLiteConnection(App.PAY1_DB_PATH))
                {
                dbConn.RunInTransaction(() =>
                {
                    dbConn.Insert(newcontact);
                });
                }
            }


        public void DeleteDthPlans(String op_id, String code)
            {
            using(var dbConn = new SQLiteConnection(App.PAY1_DB_PATH))
                {
                var existingconact = dbConn.Query<DthPlanDB>("DELETE FROM DthPlanDB WHERE dthPlanCircleID='"+code+"' AND dthPlanOpertorID='" + op_id + "'").FirstOrDefault();
                if(existingconact != null)
                    {
                    dbConn.RunInTransaction(() =>
                    {
                        dbConn.Delete(existingconact);
                    });
                    }
                }
            }

        public void DeleteAllMissCall()
            {
            using(var dbConn = new SQLiteConnection(App.PAY1_DB_PATH))
                {

                dbConn.DropTable<DthPlanDB>();
                dbConn.CreateTable<DthPlanDB>();
                dbConn.Dispose();
                dbConn.Close();

                }
            }
        }

    }
