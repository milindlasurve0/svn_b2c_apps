package com.pay1;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;

import com.pay1.constants.Constants;
import com.pay1.requesthandler.RequestClass;
import com.pay1.utilities.Utility;

public class UpdateBalanceActivity extends Activity {

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		new CheckBalanceTask().execute();
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
		finish();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		finish();
	}

	public class CheckBalanceTask extends AsyncTask<String, String, String> {
		// MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {

			String response = RequestClass.getInstance()
					.readPay1B2CRequest(UpdateBalanceActivity.this,
							Constants.B2C_URL + "check_bal");
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			// if (this.dialog.isShowing()) {
			// this.dialog.dismiss();
			// }
			super.onPostExecute(result);
			try {
				// JSONArray array = new JSONArray(result);
				JSONObject jsonObject = new JSONObject(result);
				String status = jsonObject.getString("status");
				if (status.equalsIgnoreCase("SUCCESS")) {
					JSONObject description = jsonObject
							.getJSONObject("description");
					String account_balance = description
							.getString("account_balance");
					Utility.setBalance(UpdateBalanceActivity.this,
							Constants.SHAREDPREFERENCE_BALANCE,
							account_balance.trim());
				}
			} catch (JSONException e) {
				// e.printStackTrace();
				// Constants.showCustomToast(getActivity(),
				// result);
			} catch (Exception e) {
				// TODO: handle exception
				// Constants.showCustomToast(getActivity(),
				// result);
			}
			finish();
		}

		@Override
		protected void onPreExecute() {
			// dialog = new MyProgressDialog(getActivity());
			// // this.dialog.setMessage("Please wait.....");
			// this.dialog.show();
			super.onPreExecute();

		}

	}
}
