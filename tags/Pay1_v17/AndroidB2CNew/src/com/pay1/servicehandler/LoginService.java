package com.pay1.servicehandler;

import java.net.URLEncoder;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.IntentService;
import android.content.Intent;
import android.os.AsyncTask;

import com.pay1.RegistrationActivity;
import com.pay1.constants.Constants;
import com.pay1.requesthandler.RequestClass;
import com.pay1.utilities.Utility;

public class LoginService extends IntentService {

	public LoginService() {
		super("LoginService");
		// TODO Auto-generated constructor stub

	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		new LoginTask().execute(Utility.getMobileNumber(LoginService.this,
				Constants.SHAREDPREFERENCE_MOBILE), Utility.getPin(
				LoginService.this, Constants.SHAREDPREFERENCE_PIN), Utility
				.getUUID(LoginService.this, Constants.SHAREDPREFERENCE_UUID),
				Utility.getCurrentLatitude(LoginService.this,
						Constants.SHAREDPREFERENCE_CURRENT_LATITUDE), Utility
						.getCurrentLongitude(LoginService.this,
								Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE),
				Utility.getOSManufacturer(LoginService.this,
						Constants.SHAREDPREFERENCE_OS_MANUFACTURER), Utility
						.getOSVersion(LoginService.this,
								Constants.SHAREDPREFERENCE_OS_VERSION), Utility
						.getGCMID(LoginService.this,
								Constants.SHAREDPREFERENCE_GCMID));
	}

	public class LoginTask extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... params) {
			try {

				String response = RequestClass.getInstance()
						.readPay1B2CRequest(
								LoginService.this,
								Constants.B2C_URL + "signin/?username="
										+ URLEncoder.encode(params[0], "utf-8")
										+ "&password="
										+ URLEncoder.encode(params[1], "utf-8")
										+ "&uuid=" + params[2] + "&latitude="
										+ params[3] + "&longitude=" + params[4]
										+ "&manufacturer=" + params[5]
										+ "&os_info=" + params[6]
										+ "&gcm_reg_id=" + params[7]
										+ "&device_type=android");

				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			// Log.e("Login", result);
			try {
				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONObject jsonObject = new JSONObject(replaced);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {
						Utility.setLoginFlag(LoginService.this,
								Constants.SHAREDPREFERENCE_IS_LOGIN, true);
						JSONObject jsonObject2 = jsonObject
								.getJSONObject("description");

						Utility.setUserName(LoginService.this,
								Constants.SHAREDPREFERENCE_NAME, jsonObject2
										.getString("name").trim());
						Utility.setMobileNumber(LoginService.this,
								Constants.SHAREDPREFERENCE_MOBILE, jsonObject2
										.getString("mobile").trim());
						Utility.setEmail(LoginService.this,
								Constants.SHAREDPREFERENCE_EMAIL, jsonObject2
										.getString("email").trim());
						Utility.setGender(LoginService.this,
								Constants.SHAREDPREFERENCE_GENDER, jsonObject2
										.getString("gender").trim());
						Utility.setUserID(LoginService.this,
								Constants.SHAREDPREFERENCE_USER_ID, jsonObject2
										.getString("user_id").trim());
						Utility.setBalance(LoginService.this,
								Constants.SHAREDPREFERENCE_BALANCE, jsonObject2
										.getString("wallet_balance").trim());
						Utility.setDOB(LoginService.this,
								Constants.SHAREDPREFERENCE_DOB, jsonObject2
										.getString("date_of_birth").trim());
						Utility.setLatitude(LoginService.this,
								Constants.SHAREDPREFERENCE_LATITUDE,
								jsonObject2.getString("latitude").trim());
						Utility.setLongitude(LoginService.this,
								Constants.SHAREDPREFERENCE_LONGITUDE,
								jsonObject2.getString("longitude").trim());
						Utility.setSupportNumber(LoginService.this,
								Constants.SHAREDPREFERENCE_SUPPORT_NUMBER,
								jsonObject2.getString("support_number").trim());
						Utility.setCookieName(LoginService.this,
								Constants.SHAREDPREFERENCE_COOKIE_NAME,
								jsonObject2.getString("session_name"));
						Utility.setCookieValue(LoginService.this,
								Constants.SHAREDPREFERENCE_COOKIE_VALUE,
								jsonObject2.getString("session_id"));
						Utility.setMyTotalGifts(LoginService.this,
								jsonObject2.getString("total_gifts"));
						
						Utility.setSharedPreferences(LoginService.this,
								"Deals_offer",
								jsonObject2.getString("Deals_offer"));
						 Utility.setBalance(LoginService.this,
								 Constants.SHAREDPREFERENCE_LOYALTY_POINTS,
								 jsonObject2.getString("loyalty_points"));
						Utility.setMyLikes(LoginService.this,
								jsonObject2.getString("total_mylikes"));

						Utility.setMyTotalRedeem(LoginService.this,
								jsonObject2.getString("total_redeem"));

						Utility.setMyReviews(LoginService.this,
								jsonObject2.getString("total_myreviews"));
						JSONObject jsonObjectTemplate = jsonObject2
								.getJSONObject("ErrorMsg");
						Utility.setTemplate(
								LoginService.this,
								Constants.SHAREDPREFERENCE_SMS_BILLPAYMENT_FAILURE,
								jsonObjectTemplate
										.getString("BILLPAYMENT_FAILURE"));
						Utility.setTemplate(
								LoginService.this,
								Constants.SHAREDPREFERENCE_SMS_RECHARGE_FAILURE,
								jsonObjectTemplate
										.getString("RECHARGE_FAILURE"));
						Utility.setTemplate(
								LoginService.this,
								Constants.SHAREDPREFERENCE_SMS_WALLET_REFILLED_RETAILER,
								jsonObjectTemplate
										.getString("WALLET_REFILLED_RETAILER"));

						Utility.setSharedPreferences(
								LoginService.this,
								Constants.SHAREDPREFERENCE_SERVICE_CHARGE_PERCENT,
								jsonObject2.getString("service_charge_percent")
										.trim());

						Utility.setSharedPreferences(LoginService.this,
								Constants.SHAREDPREFERENCE_SERVICE_TAX_PERCENT,
								jsonObject2.getString("service_tax_percent")
										.trim());

					} else {

					}

				} else {
				}
			} catch (JSONException e) {
				// e.printStackTrace();

			} catch (Exception e) {
				// TODO: handle exception

			}
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}
	}

}
