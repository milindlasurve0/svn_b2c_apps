package com.pay1;

import java.text.Normalizer;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.pay1.constants.Constants;
import com.pay1.customviews.MyProgressDialog;
import com.pay1.databasehandler.Operator;
import com.pay1.databasehandler.OperatorDataSource;
import com.pay1.requesthandler.RequestClass;
import com.pay1.utilities.EditTextValidator;
import com.pay1.utilities.Utility;

public class BillPaymentActivity extends AppBaseActivity implements
		OnClickListener {
	private static final String SCREEN_LABEL = "Mobile Bill Screen";
	private EasyTracker easyTracker = null;

	int flag = 1;
	private Button btnContacts;// , mBtnRchCancel, mBtnRchNow;
	private ImageView imageView_Ok, imageView_Cancel, imageView_dropdown;
	private EditText editRechargeMobile, editRechargeAmount;

	int PICK_CONTACT = 100;
	static int PICK_OPERATOR = 111;
	private TextView textView_Details, textView_Title, textView_Operator, textViewMob,
			textViewSpinner, textViewAmount, textView_Name;
	int rechargeType;
	int rechAmount;
	String circle_code = null, operatorCode = null, operatorID = null,
			operatorName = null, operatorProductID = null;
	Bundle bundle;
	private OperatorDataSource operatorDataSource;
	private boolean doubleBackToExitPressedOnce = false;
	String recharge_flag = "0";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.bill_payment_activity);
		try {
			// Initialize GA
			// Pay1GA pay1GA = new Pay1GA(BillPaymentActivity.this);
			// Pay1GA.getGaTracker().set(Fields.SCREEN_NAME, SCREEN_LABEL);

			easyTracker = EasyTracker.getInstance(BillPaymentActivity.this);
			easyTracker.set(Fields.SCREEN_NAME, SCREEN_LABEL);

			findViewById();

			registerBaseActivityReceiver();

			operatorDataSource = new OperatorDataSource(
					BillPaymentActivity.this);

			bundle = getIntent().getExtras();
			flag = 1;

			try {
				operatorDataSource.open();

				List<Operator> operator = operatorDataSource.getAllOperator();
				if (operator.size() == 0)
					new GetOperatorTask().execute();

			} catch (SQLException exception) {
				// TODO: handle exception
				// // Log.e("Er ", exception.getMessage());
			} catch (Exception e) {
				// TODO: handle exception
				// // Log.e("Er ", e.getMessage());
			} finally {
				operatorDataSource.close();
			}

			imageView_Ok.setOnClickListener(this);
			imageView_Cancel.setOnClickListener(this);
			btnContacts.setOnClickListener(this);
			textView_Operator.setOnClickListener(this);
			imageView_dropdown.setOnClickListener(this);

			Typeface Normal = Typeface.createFromAsset(getAssets(),
					"EncodeSansNormal-100-Thin.ttf");
			Typeface Reguler = Typeface.createFromAsset(getAssets(),
					"EncodeSansNormal-400-Regular.ttf");
			Typeface Medium = Typeface.createFromAsset(getAssets(),
					"EncodeSansNarrow-500-Medium.ttf");

			textView_Details.setTypeface(Normal);
			textView_Title.setTypeface(Reguler);
			textViewAmount.setTypeface(Medium);
			textViewMob.setTypeface(Medium);
			textViewSpinner.setTypeface(Medium);
			editRechargeMobile.setTypeface(Normal);
			editRechargeAmount.setTypeface(Normal);
			textView_Operator.setTypeface(Normal);
			rechargeType = bundle.getInt(Constants.RECHARGE_FOR);

			editRechargeMobile.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					// TODO Auto-generated method stub
					if (editRechargeMobile.getText().toString().trim().length() > 0) {
						if (EditTextValidator.hasText(BillPaymentActivity.this,
								editRechargeMobile,
								Constants.ERROR_MOBILE_BLANK_FIELD))
							btnContacts.setVisibility(View.VISIBLE);
						else
							btnContacts.setVisibility(View.GONE);

						textView_Operator.setText("");
					}

					if ((rechargeType != Constants.QUICK_PAY)
							&& editRechargeMobile.length() == 10) {
						if (editRechargeMobile.getText().toString().trim()
								.startsWith("9")
								|| editRechargeMobile.getText().toString()
										.trim().startsWith("8")
								|| editRechargeMobile.getText().toString()
										.trim().startsWith("7")) {
							editRechargeAmount.requestFocus();
							new GetPlansCircletask().execute();
						} else {
							Constants.showOneButtonDialog(
									BillPaymentActivity.this, "Mobile bill",
									Constants.ERROR_MOBILE_LENGTH_FIELD,
									Constants.DIALOG_CLOSE);
						}
					}
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					// TODO Auto-generated method stub

				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

				}
			});

			editRechargeMobile
					.setOnEditorActionListener(new TextView.OnEditorActionListener() {

						@Override
						public boolean onEditorAction(TextView v, int actionId,
								KeyEvent event) {
							// TODO Auto-generated method stub
							if (actionId == EditorInfo.IME_ACTION_NEXT) {
								if (EditTextValidator.isValidMobileNumber(
										BillPaymentActivity.this,
										editRechargeMobile,
										Constants.ERROR_MOBILE_LENGTH_FIELD))
									btnContacts.setVisibility(View.VISIBLE);
								else
									btnContacts.setVisibility(View.GONE);
								return true;
							}
							return false;
						}
					});

			textView_Operator.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					// TODO Auto-generated method stub
					if (textView_Operator.getText().toString().trim().length() > 0)
						if ((rechargeType != Constants.QUICK_PAY)
								&& EditTextValidator.hasText(
										BillPaymentActivity.this,
										textView_Operator,
										Constants.ERROR_OPERATOR_BLANK_FIELD))
							imageView_dropdown.setVisibility(View.VISIBLE);
						else
							imageView_dropdown.setVisibility(View.GONE);
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					// TODO Auto-generated method stub

				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

				}
			});

			editRechargeAmount.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					// TODO Auto-generated method stub
					if (editRechargeAmount.getText().toString().trim().length() > 0)
						EditTextValidator.hasText(BillPaymentActivity.this,
								editRechargeAmount,
								Constants.ERROR_AMOUNT_BLANK_FIELD);
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					// TODO Auto-generated method stub

				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

				}
			});

			if (rechargeType == Constants.QUICK_PAY) {
				String quickPaydata = bundle.getString(Constants.QUICKPAY_DATA);
				recharge_flag = "1";
				try {
					JSONObject quickJsonObject = new JSONObject(quickPaydata);
					// String flag = quickJsonObject.getString("flag");
					// String id = quickJsonObject.getString("id");
					// String conatctName = quickJsonObject.getString("name");
					// String amount = quickJsonObject.getString("amount");
					String operator_id = quickJsonObject
							.getString("operator_id");
					String name = quickJsonObject.getString("name");
					// String confirmation = quickJsonObject
					// .getString("confirmation");
					String number = quickJsonObject.getString("number");
					textView_Name.setText(name);
					operatorID = operator_id;
					operatorCode = quickJsonObject.getString("operator_code");
					editRechargeAmount.setText("");
					editRechargeMobile.setText(number);
					editRechargeMobile.setEnabled(false);
					btnContacts.setVisibility(View.GONE);
					textView_Operator.setText(quickJsonObject
							.getString("operator_name"));
					textView_Operator.setEnabled(false);
					imageView_dropdown.setVisibility(View.GONE);
					// rechargeType = Constants.BILL_PAYMENT;

				} catch (JSONException je) {

				}
			}
		} catch (Exception e) {
		}
		overridePendingTransition(R.anim.push_up_in, R.anim.fadeout);
	}

	private void findViewById() {
		// TODO Auto-generated method stub
		textView_Details = (TextView) findViewById(R.id.textView_Details);
		textViewMob = (TextView) findViewById(R.id.textViewMob);
		textViewSpinner = (TextView) findViewById(R.id.textViewSpinner);
		textViewAmount = (TextView) findViewById(R.id.textViewAmount);
		textView_Title = (TextView) findViewById(R.id.textView_Title);

		editRechargeMobile = (EditText) findViewById(R.id.editRechargeMobile);
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(editRechargeMobile.getWindowToken(), 0);
		btnContacts = (Button) findViewById(R.id.btnContacts);
		textView_Operator = (TextView) findViewById(R.id.textView_Operator);
		editRechargeAmount = (EditText) findViewById(R.id.editRechargeAmount);
		imageView_Ok = (ImageView) findViewById(R.id.imageView_Ok);

		imageView_Cancel = (ImageView) findViewById(R.id.imageView_Cancel);
		imageView_dropdown = (ImageView) findViewById(R.id.imageView_dropdown);
		textView_Name = (TextView) findViewById(R.id.textView_Name);
	}

	private boolean checkValidation(Context context) {
		boolean ret = true;

		if (!EditTextValidator.hasText(context, editRechargeMobile,
				Constants.ERROR_MOBILE_BLANK_FIELD)) {
			ret = false;
			editRechargeMobile.requestFocus();
			btnContacts.setVisibility(View.GONE);
			return ret;
		} else if (!EditTextValidator.isValidMobileNumber(context,
				editRechargeMobile, Constants.ERROR_MOBILE_LENGTH_FIELD)) {
			ret = false;
			editRechargeMobile.requestFocus();
			btnContacts.setVisibility(View.GONE);
			return ret;
		} else if (!EditTextValidator.hasText(context, textView_Operator,
				Constants.ERROR_OPERATOR_BLANK_FIELD)) {
			ret = false;
			textView_Operator.requestFocus();
			imageView_dropdown.setVisibility(View.GONE);
			return ret;
		} else if (!EditTextValidator.hasText(context, editRechargeAmount,
				Constants.ERROR_AMOUNT_BLANK_FIELD)) {
			ret = false;
			editRechargeAmount.requestFocus();
			return ret;
		} else if (!EditTextValidator.isValidAmount(context,
				editRechargeAmount, Constants.ERROR_AMOUNT_VALID_FIELD)) {
			ret = false;
			editRechargeAmount.requestFocus();
			return ret;
		} else
			return ret;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		try {
			if (resultCode == Activity.RESULT_OK) {
				if (requestCode == Constants.PLAN_REQUEST) {
					editRechargeAmount.setText(data.getExtras().getString(
							"result"));
				}
				if (requestCode == PICK_OPERATOR) {
					textView_Operator.setText(data.getExtras().getString(
							"OperatorName"));
					operatorCode = data.getExtras().getString("OperatorCode");
					operatorID = data.getExtras().getString("OperatorID");
					operatorProductID = data.getExtras().getString(
							"OperatorProductID");
				}
				if (requestCode == PICK_CONTACT) {
					overridePendingTransition(R.anim.slide_in_left,
							R.anim.slide_out_right);
					Uri contactData = data.getData();
					Cursor contactCursor = getContentResolver().query(
							contactData,
							new String[] { ContactsContract.Contacts._ID },
							null, null, null);
					String id = null;
					if (contactCursor.moveToFirst()) {
						id = contactCursor.getString(contactCursor
								.getColumnIndex(ContactsContract.Contacts._ID));
					}
					contactCursor.close();
					String phoneNumber = null, displayName = null;
					Cursor phoneCursor = getContentResolver()
							.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
									new String[] {
											ContactsContract.CommonDataKinds.Phone.NUMBER,
											ContactsContract.PhoneLookup.DISPLAY_NAME },
									ContactsContract.CommonDataKinds.Phone.CONTACT_ID
											+ "= ? ", new String[] { id }, null);
					if (phoneCursor.moveToFirst()) {
						phoneNumber = phoneCursor
								.getString(phoneCursor
										.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
						displayName = phoneCursor
								.getString(phoneCursor
										.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
						phoneNumber = Normalizer.normalize(phoneNumber,
								Normalizer.Form.NFD).replaceAll("[^a-zA-Z0-9]",
								"");
						if (phoneNumber.length() >= 10) {
							phoneNumber = phoneNumber.substring(
									phoneNumber.length() - 10,
									phoneNumber.length());
							editRechargeMobile.setText(phoneNumber);
							if (displayName.equalsIgnoreCase(phoneNumber))
								textView_Name.setText("");
							else
								textView_Name.setText(displayName);
						} else {
							Toast.makeText(BillPaymentActivity.this,
									"Not a valid mobile number.",
									Toast.LENGTH_LONG).show();
							phoneNumber = "";
							displayName = "";
							editRechargeMobile.setText("");
							textView_Name.setText("");
						}

					} else {
						Toast.makeText(BillPaymentActivity.this,
								"Not a valid mobile number.", Toast.LENGTH_LONG)
								.show();
						phoneNumber = "";
						displayName = "";
						editRechargeMobile.setText("");
						textView_Name.setText("");
					}
					phoneCursor.close();
				}
			} else {
				if (requestCode == PICK_CONTACT) {
					overridePendingTransition(R.anim.slide_in_left,
							R.anim.slide_out_right);
				}
			}

		} catch (Exception e) {
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		switch (v.getId()) {
		case R.id.textView_Operator: {
			Intent intent = new Intent(BillPaymentActivity.this,
					OperatorActivity.class);
			intent.putExtra(Constants.RECHARGE_FOR, Constants.BILL_PAYMENT);
			startActivityForResult(intent, PICK_OPERATOR);
		}
			break;
		case R.id.imageView_dropdown: {
			Intent intent = new Intent(BillPaymentActivity.this,
					OperatorActivity.class);
			intent.putExtra(Constants.RECHARGE_FOR, Constants.BILL_PAYMENT);
			startActivityForResult(intent, PICK_OPERATOR);
		}
			break;
		case R.id.btnContacts: {
			Intent intent = new Intent(Intent.ACTION_PICK,
					ContactsContract.Contacts.CONTENT_URI);
			startActivityForResult(intent, PICK_CONTACT);
			overridePendingTransition(R.anim.slide_in_right,
					R.anim.slide_out_left);
		}
			break;
		case R.id.imageView_Cancel:
			editRechargeAmount.setText("");
			editRechargeMobile.setText("");
			editRechargeMobile.requestFocus();
			textView_Operator.setText("");
			textView_Name.setText("");
			editRechargeMobile.setEnabled(true);
			btnContacts.setVisibility(View.VISIBLE);
			textView_Operator.setEnabled(true);
			imageView_dropdown.setVisibility(View.VISIBLE);
			rechargeType = Constants.BILL_PAYMENT;
			break;
		case R.id.imageView_Ok: {
			if (Utility.getLoginFlag(BillPaymentActivity.this,
					Constants.SHAREDPREFERENCE_IS_LOGIN)) {
				if (!checkValidation(this)) {

				} else {

					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

					String charges_slab = null;
					double service_charge_amount = 0, service_charge_percent = 0, service_tax_percent = 0, min = 0, max = 0;
					try {
						operatorDataSource.open();

						List<Operator> operators = operatorDataSource
								.getAllOperator(Constants.BILL_PAYMENT,
										operatorCode);

						if (operators.size() != 0) {
							charges_slab = operators.get(0)
									.getOperatorChargeSlab();
							service_charge_amount = operators.get(0)
									.getOperatorServiceChargeAmount();
							service_charge_percent = operators.get(0)
									.getOperatorServiceChargePercent();
							service_tax_percent = operators.get(0)
									.getOperatorServiceTaxPercent();
							min = operators.get(0).getOperatorMinAmount();
							max = operators.get(0).getOperatorMaxAmount();
						}

					} catch (SQLException exception) {
						// TODO: handle exception
						// // Log.e("Er ", exception.getMessage());
					} catch (Exception e) {
						// TODO: handle exception
						// // Log.e("Er ", e.getMessage());
					} finally {
						operatorDataSource.close();
					}

					double rechargeAmount = 0, balance = 0;
					try {
						rechargeAmount = Double.parseDouble(editRechargeAmount
								.getText().toString().trim());
						balance = Double.parseDouble(Utility.getBalance(
								BillPaymentActivity.this,
								Constants.SHAREDPREFERENCE_BALANCE));
					} catch (Exception e) {
						rechargeAmount = 0;
						balance = 0;
					}
					if (rechargeAmount < min || rechargeAmount > max) {
						EditTextValidator.isValidAmount(
								BillPaymentActivity.this, editRechargeAmount,
								Constants.ERROR_AMOUNT_RANG_FIELD + min
										+ " to " + max, min, max);
						editRechargeAmount.requestFocus();
						return;
					}
					int slab_amount = 0;
					try {
						if (charges_slab.equalsIgnoreCase("null")
								|| charges_slab.equalsIgnoreCase("")) {
							rechargeAmount = (rechargeAmount + (rechargeAmount * service_charge_percent));
							rechargeAmount = (rechargeAmount + (rechargeAmount * service_tax_percent));
						} else {
							String[] comma_seperator = charges_slab.split(",");
							for (String s : comma_seperator) {
								String[] colon_sperator = s.split(":");
								if (rechargeAmount > Double
										.parseDouble(colon_sperator[0])) {
									slab_amount = Integer
											.parseInt(colon_sperator[1]);
								}
							}
						}
					} catch (Exception e) {
					}
					if ((rechargeAmount + slab_amount) > balance) {
						Intent orderIntent = new Intent(
								BillPaymentActivity.this,
								OrderSummaryForLowBalActivity.class);
						orderIntent.putExtra(Constants.OPERATOR_NAME,
								textView_Operator.getText().toString());
						orderIntent.putExtra("RECHARGE_FLAG", recharge_flag);
						orderIntent.putExtra(Constants.MOBILE_NUMBER,
								editRechargeMobile.getText().toString().trim());
						orderIntent.putExtra(Constants.CONTACT_NAME,
								textView_Name.getText().toString().trim());
						orderIntent.putExtra(Constants.RECHARGE_AMOUNT,
								editRechargeAmount.getText().toString().trim());
						orderIntent
								.putExtra(Constants.CHARGE_SLAB, slab_amount);
						orderIntent.putExtra(Constants.OPERATOR_ID, operatorID);
						orderIntent.putExtra(Constants.STV_RECHARGE, "");
						orderIntent.putExtra(Constants.RECHARGE_FOR,
								Constants.BILL_PAYMENT);
						startActivity(orderIntent);

					} else {

						Intent orderIntent = new Intent(
								BillPaymentActivity.this,
								OrderSummaryActivity.class);
						orderIntent.putExtra(Constants.OPERATOR_NAME,
								textView_Operator.getText().toString());
						orderIntent.putExtra("RECHARGE_FLAG", recharge_flag);
						orderIntent.putExtra(Constants.MOBILE_NUMBER,
								editRechargeMobile.getText().toString().trim());
						orderIntent.putExtra(Constants.CONTACT_NAME,
								textView_Name.getText().toString().trim());
						orderIntent.putExtra(Constants.RECHARGE_AMOUNT,
								editRechargeAmount.getText().toString().trim());
						orderIntent
								.putExtra(Constants.CHARGE_SLAB, slab_amount);
						orderIntent.putExtra(Constants.OPERATOR_ID, operatorID);
						orderIntent.putExtra(Constants.STV_RECHARGE, "");
						orderIntent.putExtra(Constants.RECHARGE_FOR,
								Constants.BILL_PAYMENT);
						startActivity(orderIntent);
					}
				}
			} else {
				// Constants.showOneButtonDialog(RechargeMobileActivity.this,
				// "Recharge ", "Please Login First.",
				// Constants.DIALOG_CLOSE_LOGIN);
				Intent intent = new Intent(BillPaymentActivity.this,
						LoginForRechargeActivity.class);
				startActivity(intent);
			}
		}
			break;
		case R.id.btnPlans:
			Intent plansIntent = new Intent(BillPaymentActivity.this,
					PlanFragmentActivity.class);
			if (editRechargeMobile.getText().toString().trim().length() != 0) {
				plansIntent.putExtra(Constants.PLAN_JSON, operatorProductID);
				plansIntent.putExtra("CIRCLE_CODE", circle_code);
				plansIntent.putExtra("DATA_CARD_PLAN", 0);
			} else {
				plansIntent.putExtra(Constants.PLAN_JSON, operatorProductID);
				plansIntent.putExtra("CIRCLE_CODE", "");
				plansIntent.putExtra("DATA_CARD_PLAN", 0);
			}
			startActivityForResult(plansIntent, Constants.PLAN_REQUEST);
			break;
		default:
			break;
		}

	}

	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView().build());
		// EasyTracker.getInstance(this).activityStart(this);
		easyTracker.send(MapBuilder.createAppView().build());
	}

	@Override
	public void onStop() {
		super.onStop();
		// EasyTracker.getInstance(this).activityStop(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (Constants.isExpanded) {
			Constants.collapsView(this);
		} else {
			Constants.SELECTED_MENU_INDEX = Constants.BILL_MENU_INDEX;
			Constants.showNavigationBar(BillPaymentActivity.this);
			// overridePendingTransition(R.anim.translate_activity_down,
			// R.anim.translate_activity_up);
			flag = 1;
		}
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unRegisterBaseActivityReceiver();
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// if (Constants.isExpanded) {
		// Constants.collapsView(this);
		// } else {
		// super.onBackPressed();
		// overridePendingTransition(R.anim.fadeout, R.anim.push_down_out);
		// }
		if (Constants.isExpanded) {
			Constants.collapsView(this);
		} else {
			if (rechargeType != Constants.QUICK_PAY) {
				if (doubleBackToExitPressedOnce) {
					super.onBackPressed();
					overridePendingTransition(R.anim.fadeout,
							R.anim.push_down_out);
					closeAllActivitiesInApp();
					return;
				}

				this.doubleBackToExitPressedOnce = true;
				Toast.makeText(this, "Press again to exit.", Toast.LENGTH_SHORT)
						.show();

				new Handler().postDelayed(new Runnable() {

					@Override
					public void run() {
						doubleBackToExitPressedOnce = false;
					}
				}, 10000);
			} else {
				finish();
			}
		}

		// if (Constants.isExpanded) {
		// if (doubleBackToExitPressedOnce) {
		// super.onBackPressed();
		// overridePendingTransition(R.anim.fadeout, R.anim.push_down_out);
		// } else {
		// Constants.collapsView(this);
		// }
		// } else if (!Constants.isExpanded) {
		// if (doubleBackToExitPressedOnce) {
		// super.onBackPressed();
		// overridePendingTransition(R.anim.fadeout, R.anim.push_down_out);
		// } else {
		// DisplayMetrics metrics = new DisplayMetrics();
		// getWindowManager().getDefaultDisplay().getMetrics(metrics);
		// int panelWidth = (int) ((metrics.widthPixels) * 0.80);
		// int panelHeight = (int) ((metrics.heightPixels) * 0.80);
		// Constants.expandView(BillPaymentActivity.this, panelWidth,
		// panelHeight);
		// }
		// }
		// if (!doubleBackToExitPressedOnce) {
		// this.doubleBackToExitPressedOnce = true;
		// Toast.makeText(this, "Press again to exit.",
		// Toast.LENGTH_SHORT).show();
		// }
		//
		// new Handler().postDelayed(new Runnable() {
		//
		// @Override
		// public void run() {
		// doubleBackToExitPressedOnce = false;
		// }
		// }, 2000);
		// return;
	}

	public class GetPlansCircletask extends AsyncTask<String, String, String> {
		MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			String response = RequestClass.getInstance().readPay1B2BRequest(
					BillPaymentActivity.this,
					Constants.B2B_URL + "method=getMobileDetails&mobile="
							+ editRechargeMobile.getText().toString().trim());
			String replaced = response.replace("(", "").replace(")", "")
					.replace(";", "");

			return replaced;

		}

		@Override
		protected void onPostExecute(String result) {
			// if (this.dialog.isShowing()) {
			// this.dialog.dismiss();
			// }
			super.onPostExecute(result);
			try {
				JSONArray array = new JSONArray(result);
				JSONObject jsonObject = array.getJSONObject(0);
				String status = jsonObject.getString("status");
				if (status.equalsIgnoreCase("SUCCESS")) {

					JSONObject jsonObjectdetails = jsonObject
							.getJSONObject("details");
					circle_code = jsonObjectdetails.getString("area")
							.toString().trim();
					operatorCode = jsonObjectdetails.getString("operator")
							.toString().trim();

					try {
						operatorDataSource.open();

						List<Operator> operators = operatorDataSource
								.getAllOperator(Constants.BILL_PAYMENT,
										operatorCode);
						if (operators.size() != 0) {
							operatorName = operators.get(0).getOperatorName();
							operatorProductID = String.valueOf(operators.get(0)
									.getOperatorProductID());
							operatorID = String.valueOf(operators.get(0)
									.getOperatorID());
						}

					} catch (SQLException exception) {
						// TODO: handle exception
						// // Log.e("Er ", exception.getMessage());
					} catch (Exception e) {
						// TODO: handle exception
						// // Log.e("Er ", e.getMessage());
					} finally {
						operatorDataSource.close();
					}
					textView_Operator.setText(operatorName);
				}
			} catch (JSONException e) {
				// e.printStackTrace();
				// Constants.showCustomToast(RechargeMobileActivity.this,
				// result);
			} catch (Exception e) {
				// TODO: handle exception
				// Constants.showCustomToast(RechargeMobileActivity.this,
				// result);
			}

		}

		@Override
		protected void onPreExecute() {
			// dialog = new MyProgressDialog(PlansActivity.this);
			// // this.dialog.setMessage("Please wait.....");
			// this.dialog.show();
			super.onPreExecute();

		}

	}

	public class GetOperatorTask extends AsyncTask<String, String, String> {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			try {
				String response = RequestClass.getInstance()
						.readPay1B2CRequest(BillPaymentActivity.this,
								Constants.B2C_URL + "get_all_operators/?");
				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}

			try {

				long timestamp = System.currentTimeMillis();
				operatorDataSource.open();

				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");
					JSONObject jsonObject = new JSONObject(replaced);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {
						JSONObject description = jsonObject
								.getJSONObject("description");
						JSONArray mobile = description.getJSONArray("mobile");
						JSONArray data = description.getJSONArray("data");
						JSONArray postpaid = description
								.getJSONArray("postpaid");
						JSONArray dth = description.getJSONArray("dth");

						for (int i = 0; i < mobile.length(); i++) {
							JSONObject object = mobile.getJSONObject(i);

							operatorDataSource
									.createOperator(
											Integer.parseInt(object.getString(
													"id").trim()),
											object.getString("opr_code").trim(),
											Integer.parseInt(object.getString(
													"product_id").trim()),
											object.getString("name").trim(),
											Integer.parseInt(object.getString(
													"service_id").trim()),
											Integer.parseInt(object.getString(
													"flag").trim()),
											Integer.parseInt(object.getString(
													"delegate").trim()),
											Integer.parseInt(object.getString(
													"stv").trim()),
											Constants.RECHARGE_MOBILE,
											timestamp,
											Integer.parseInt(object
													.getString("max")),
											Integer.parseInt(object
													.getString("min")),
											Integer.parseInt(object
													.getString("service_charge_amount")),
											Integer.parseInt(object
													.getString("service_tax_percent")),
											Integer.parseInt(object
													.getString("service_charge_percent")),
											object.getString("charges_slab")
													.toString());

						}
						for (int i = 0; i < data.length(); i++) {
							JSONObject object = data.getJSONObject(i);

							operatorDataSource
									.createOperator(
											Integer.parseInt(object.getString(
													"id").trim()),
											object.getString("opr_code").trim(),
											Integer.parseInt(object.getString(
													"product_id").trim()),
											object.getString("name").trim(),
											Integer.parseInt(object.getString(
													"service_id").trim()),
											Integer.parseInt(object.getString(
													"flag").trim()),
											Integer.parseInt(object.getString(
													"delegate").trim()),
											Integer.parseInt(object.getString(
													"stv").trim()),
											Constants.RECHARGE_DATA,
											timestamp,
											Integer.parseInt(object
													.getString("max")),
											Integer.parseInt(object
													.getString("min")),
											Integer.parseInt(object
													.getString("service_charge_amount")),
											Integer.parseInt(object
													.getString("service_tax_percent")),
											Integer.parseInt(object
													.getString("service_charge_percent")),
											object.getString("charges_slab")
													.toString());
						}
						for (int i = 0; i < postpaid.length(); i++) {
							JSONObject object = postpaid.getJSONObject(i);

							operatorDataSource
									.createOperator(
											Integer.parseInt(object.getString(
													"id").trim()),
											object.getString("opr_code").trim(),
											Integer.parseInt(object.getString(
													"product_id").trim()),
											object.getString("name").trim(),
											Integer.parseInt(object.getString(
													"service_id").trim()),
											Integer.parseInt(object.getString(
													"flag").trim()),
											Integer.parseInt(object.getString(
													"delegate").trim()),
											Integer.parseInt(object.getString(
													"stv").trim()),
											Constants.BILL_PAYMENT,
											timestamp,
											Integer.parseInt(object
													.getString("max")),
											Integer.parseInt(object
													.getString("min")),
											Integer.parseInt(object
													.getString("service_charge_amount")),
											Integer.parseInt(object
													.getString("service_tax_percent")),
											Integer.parseInt(object
													.getString("service_charge_percent")),
											object.getString("charges_slab")
													.toString());
						}
						for (int i = 0; i < dth.length(); i++) {

							JSONObject object = dth.getJSONObject(i);

							operatorDataSource
									.createOperator(
											Integer.parseInt(object.getString(
													"id").trim()),
											object.getString("opr_code").trim(),
											Integer.parseInt(object.getString(
													"product_id").trim()),
											object.getString("name").trim(),
											Integer.parseInt(object.getString(
													"service_id").trim()),
											Integer.parseInt(object.getString(
													"flag").trim()),
											Integer.parseInt(object.getString(
													"delegate").trim()),
											Integer.parseInt(object.getString(
													"stv").trim()),
											Constants.RECHARGE_DTH,
											timestamp,
											Integer.parseInt(object
													.getString("max")),
											Integer.parseInt(object
													.getString("min")),
											Integer.parseInt(object
													.getString("service_charge_amount")),
											Integer.parseInt(object
													.getString("service_tax_percent")),
											Integer.parseInt(object
													.getString("service_charge_percent")),
											object.getString("charges_slab")
													.toString());
						}

					}
				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonDialog(BillPaymentActivity.this,
						"Operator", Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			} catch (SQLException exception) {
				// TODO: handle exception
				// // Log.e("Er ", exception.getMessage());
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonDialog(BillPaymentActivity.this,
						"Operator", Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			} finally {
				operatorDataSource.close();
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(BillPaymentActivity.this);
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(false);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							GetOperatorTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}
	}
}
