<?php $this->load->view('common/doctype_html'); 
    $current_page_url =  "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    $meteTagData = getMetaTagDetails();
    foreach ($meteTagData as $row){
      
      if($row->page_url == $current_page_url){
        $new_page_tittle =  $row->page_tittle;
        $new_page_meta_description =  $row->page_meta_description;  
        $new_page_meta_tag =  $row->page_meta_tag;  
      }
      
    }
?>
<meta name="description" content="<?php if($new_page_meta_description){ echo $new_page_meta_description; }else{ ?>Your privacy policy rights. Pay1 Privacy Policy describes the privacy practices of Pay1 websites, products, online services, and apps. <?php } ?> ">
<meta name="keywords" content="<?php if($new_page_meta_tag){ echo $new_page_meta_tag; }else{ ?>Privacy Policy , Pay1 - Mobile Recharges & Bill Payments <?php } ?>" />
<title><?php if($new_page_tittle){ echo $new_page_tittle; }else{ ?>Privacy Policy | Pay1 - Mobile Recharges & Bill Payments <?php } ?></title>
<?php $this->load->view('common/header');  ?>
  <link rel="stylesheet" href="/assets/css/style.css">
    <div class="clearfix" style="clear:both;"></div>
     <!-- ======================== Header Image ========================== -->
      <input type="hidden" id="privacy_img_hdn" value="/assets/images/background/privacy-policy.jpg">
      <div class="career_page1 margin-top-50">
         <img src="/assets/images/default_img/pay1_loader.jpg" class="img-responsive" alt="" id="privacy_img">
      </div>
      <div class="container-fluid">
         <div class="container border-both">
            <div class="row">
               <div class="col-md-3 col-sm-3 ">
                  <div class=''>
                     <div id="contact_navigation">
                        <ul class="tabs">
                            <li><a href="/about-us">About us</a></li>
                            <li><a href="/contact-us">Contact Us</a></li>
                            <li><a href="/terms-and-conditions">Terms & Conditions</a></li>
                           <!--<li><a href="#security" data-toggle="tab">Security Policy</a></li>-->
                           <li><a href="/faqs">FAQs</a></li>
                           <li class="active"><a href="#privacy" data-toggle="tab">Privacy Policy</a></li>
                           <li><a href="/careers">Careers</a></li>
                           <li><a target="_blank" href="/partners-blog/" >Blog</a></li>
                           <li><a href="/sitemap">Sitemap</a></li>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="col-sm-9">
                  <div class='padding-both margin-top-30'>
                     <h1><b>Privacy Policy</b></h1>
                                  
                        
                        <p>PAY1 recognizes the expectations of its customers with regard to privacy, confidentiality and security of their personal information that resides with PAY1.</p>
                        <p>This Privacy Policy provides you with details about the manner in which your data is collected, stored & used by us. You are advised to read this Privacy Policy carefully. By visiting PAY1 website/WAP site/applications you expressly give us consent to use & disclose your personal information in accordance with this Privacy Policy. If you do not agree to the terms of the policy, please do not use or access PAY1 website, WAP site or mobile applications.</p>
                        <p>Note: Our privacy policy may change at any time without prior notification. To make sure that you are aware of any changes, kindly review the policy periodically. This Privacy Policy shall apply uniformly to PAY1 desktop website, PAY1 mobile WAP site & PAY1 mobile applications.
</p>
                        
                         <h2>General</h2>
                        <p>We will not sell, share or rent your personal information to any 3rd party or use your email address/mobile number for unsolicited emails and/or SMS. Any emails and/or SMS sent by PAY1 will only be in connection with the provision of agreed services & products and this Privacy Policy.</p>
                        <p>Periodically, we may reveal general statistical information about PAY1 & its users, such as number of visitors, number and type of goods and services purchased, etc.</p>
                        <p>We reserve the right to communicate your personal information to any third party that makes a legally-compliant request for its disclosure.</p>

						<h2>Personal Information</h2>
                        <p>Personal Information means and includes all information that can be linked to a specific individual or to identify any individual, such as name, address, mailing address, telephone number, email ID, credit card number, cardholder name, card expiration date, information about your mobile phone, DTH service, data card, electricity connection, Smart Tags and any details that may have been voluntarily provide by the user in connection with availing any of the services on PAY1.</p>
                        <p>When you browse through PAY1, we may collect information regarding the domain and host from which you access the internet, the Internet Protocol [IP] address of the computer or Internet service provider [ISP] you are using, and anonymous site statistical data.</p>

						<h2>Use of Personal Information</h2>
                        <p>We use personal information to provide you with services & products you explicitly requested for, to resolve disputes, troubleshoot concerns, help promote safe services, collect money, measure consumer interest in our services, inform you about offers, products, services, updates, customize your experience, detect & protect us against error, fraud and other criminal activity, enforce our terms and conditions, etc. We also use your contact information to send you offers based on your previous orders and interests. We may occasionally ask you to complete optional online surveys. These surveys may ask you for contact information and demographic information (like zip code, age, gender, etc.). We use this data to customize your experience at PAY1, providing you with content that we think you might be interested in and to display content according to your preferences.</p>

						<h2>Cookies</h2>
                        <p>A "cookie" is a small piece of information stored by a web server on a web browser so it can be later read back from that browser. PAY1 uses cookie and tracking technology depending on the features offered. No personal information will be collected via cookies and other tracking technology; however, if you previously provided personally identifiable information, cookies may be tied to such information. Aggregate cookie and tracking information may be shared with third parties.</p>
                        
                        <h2>Links to Other Sites</h2>
                        <p>Our site links to other websites that may collect personally identifiable information about you. PAY1 is not responsible for the privacy practices or the content of those linked websites.</p>
                        
                        <h2>Security</h2>
                        <p>PAY1 has stringent security measures in place to protect the loss, misuse, and alteration of the information under our control. Whenever you change or access your account information, we offer the use of a secure server. Once your information is in our possession we adhere to strict security guidelines, protecting it against unauthorized access.</p>
                        
                        <h2>Consent</h2>
                        <p>By using PAY1 and/or by providing your information, you consent to the collection and use of the information you disclose on PAY1 in accordance with this Privacy Policy, including but not limited to your consent for sharing your information as per this privacy policy.</p>
                        
                        <h2>Communication</h2>
                        <p>For any kind of communication, please write to listen@pay1.in</p>
                        
                         <h2>PAY1 is headquartered at:</h2>
                        <p>MindsArray Technologies Private Limited
726, Raheja's Metroplex (IJIMIMA), Link Road, Malad West, Mumbai - 400064</p>


                  </div>
                 
 				  <div class="margin-top-30 clearfix"></div>
               </div>


            </div>
         </div>
      </div>
      <div class="clearfix"></div>
      <div class="modal fade" role="dialog" id="failure">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="cls" data-dismiss="modal">&times;</button>
                  <h3>Payment Failure</h3>
               </div>
               <div class="modal-body">
                  <p>Sorry! Your previous payment attempt failed.</p>
                  <div class="pull-right">
                     <button class="modal_button" id="try-again">Try Again</button>
                     <button class="modal_button"id="cancel-button"data-dismiss="modal">Cancel</button>
                  </div>
                  <div class="clearfix"></div>
               </div>
            </div>
         </div>
      </div>
      <script type="text/javascript">
         $(document).ready(function(){
             $('[data-toggle="popover"]').popover();   
             $('body').scrollToTop({skin: 'cycle'});
             $("#privacy_img").attr("src",$("#privacy_img_hdn").val());
         });
      </script>
     <script src="/assets/js/bootstrap.min.js"></script>
      <script src="<?php echo base_url('/assets/js/bootstrap.min.js'); ?>"></script>  
      <script src="/assets/js/jquery-scrollToTop.js"></script>
      <?php $this->load->view('common/footer'); ?>