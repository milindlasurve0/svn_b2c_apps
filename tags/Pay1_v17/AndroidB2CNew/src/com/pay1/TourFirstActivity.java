package com.pay1;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.pay1.adapterhandler.RegistrationPagerAdapter;
import com.pay1.customviews.CirclePageIndicator;

public class TourFirstActivity extends FragmentActivity {

	private ViewPager pager;
	private RegistrationPagerAdapter adapter;
	private final int SPLASH_DISPLAY_LENGTH = 4000;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tour_first_activity);

		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
		// Button buttonContinue = (Button) findViewById(R.id.buttonContinue);
		Typeface Reguler = Typeface.createFromAsset(getAssets(),
				"EncodeSansNormal-400-Regular.ttf");
		// buttonContinue.setTypeface(Reguler);

		pager = (ViewPager) findViewById(R.id.pager);

		adapter = new RegistrationPagerAdapter(this.getSupportFragmentManager());
		pager.setAdapter(adapter);
		CirclePageIndicator indicator = (CirclePageIndicator) findViewById(R.id.indicator);
		indicator.setViewPager(pager);

		runnable(5);

		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				Intent intent = new Intent(TourFirstActivity.this,
						MainActivity.class);
				intent.putExtra("IS_FLAG",
						getIntent().getExtras().getBoolean("IS_FLAG"));
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
						| Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
				finish();
			}
		}, SPLASH_DISPLAY_LENGTH);

		/*
		 * buttonContinue.setOnClickListener(new OnClickListener() {
		 * 
		 * @Override public void onClick(View v) { // TODO Auto-generated method
		 * stub Intent intent = new Intent(TourFirstActivity.this,
		 * MainActivity.class); intent.putExtra("IS_FLAG",
		 * getIntent().getExtras().getBoolean("IS_FLAG"));
		 * intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
		 * Intent.FLAG_ACTIVITY_NEW_TASK); startActivity(intent); finish(); }
		 * });
		 */

	}

	public void runnable(final int size) {

		pager.setCurrentItem(size - 1);
		new CountDownTimer(180000, 3000) {

			public void onTick(long millisUntilFinished) {

				if (pager.getCurrentItem() == size - 1) {
					pager.setCurrentItem(0);
				} else {
					pager.setCurrentItem(pager.getCurrentItem() + 1, true);
				}
			}

			public void onFinish() {

			}
		}.start();
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// super.onBackPressed();

	}
}
