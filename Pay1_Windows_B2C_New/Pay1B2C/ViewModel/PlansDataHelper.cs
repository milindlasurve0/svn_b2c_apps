﻿using Pay1B2C.Model;
using SQLite;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pay1B2C.ViewModel
    {
    class PlanDataHelper
        {
        SQLiteConnection dbConn;


        public async Task<bool> onCreate(string DB_PATH)
            {
            try
                {
                if(!CheckFileExists(App.PAY1_DB_PATH).Result)
                    {
                    using(dbConn = new SQLiteConnection(App.PAY1_DB_PATH))
                        {
                        dbConn.CreateTable<Plans>();
                        }
                    }
                return true;
                }
            catch
                {
                return false;
                }
            }
        private async Task<bool> CheckFileExists(string fileName)
            {
            try
                {
                var store = await Windows.Storage.ApplicationData.Current.LocalFolder.GetFileAsync(fileName);
                return true;
                }
            catch
                {
                return false;
                }
            }


        public Plans ReadPlansByFlag(String operator1,String circle)
            {
            using(var dbConn = new SQLiteConnection(App.PAY1_DB_PATH))
                {
                var existingconact = dbConn.Query<Plans>("SELECT * from Plans where operator = '"+operator1+"'").FirstOrDefault();
                return existingconact;
                }
            }

        public ObservableCollection<Plans> ReadPlans(String op_code,String c_id)
            {
           
            using(var dbConn = new SQLiteConnection(App.PAY1_DB_PATH))
                {
                //List<Plans> myCollection  = dbConn.Table<Plans>().Where(k => k.planOpertorID==op_code && k.planCircleID==c_id).OrderBy(l => l.planAmount).GroupBy(j=>j.planAmount).ToList<Plans>();

                List<Plans> myCollection =  dbConn.Query<Plans>("SELECT * FROM Plans WHERE planCircleID='" + c_id + "' AND planOpertorID='" + op_code + "' GROUP BY planAmount ORDER BY planType DESC,planAmount ASC").ToList();

             //   var result = myCollection.GroupBy(test => test.planAmount).ToList();
                ObservableCollection<Plans> PlansList = new ObservableCollection<Plans>(myCollection.Distinct());
                return PlansList;
                }
            }


        public void UpdateContact(Plans contact)
            {
            using(var dbConn = new SQLiteConnection(App.DB_PATH))
                {
                var existingconact = dbConn.Query<Plans>("select * from Plans where Id =" + contact.planCircleID).FirstOrDefault();
                if(existingconact != null)
                    {
                    //existingconact.Name = contact.Name;
                    //existingconact.PhoneNumber = contact.PhoneNumber;
                    //existingconact.CreationDate = contact.CreationDate;
                    dbConn.RunInTransaction(() =>
                    {
                        dbConn.Update(existingconact);
                    });
                    }
                }
            }

        public void Insert(Plans newcontact)
            {
            using(var dbConn = new SQLiteConnection(App.PAY1_DB_PATH))
                {
                dbConn.RunInTransaction(() =>
                {
                    dbConn.Insert(newcontact);
                });
                }
            }


        public void DeleteMisscallById(String Id)
            {
            using(var dbConn = new SQLiteConnection(App.PAY1_DB_PATH))
                {
                var existingconact = dbConn.Query<Plans>("select * from Plans where id =" + Id).FirstOrDefault();
                if(existingconact != null)
                    {
                    dbConn.RunInTransaction(() =>
                    {
                        dbConn.Delete(existingconact);
                    });
                    }
                }
            }

        public void DeleteAllMissCall()
            {
            using(var dbConn = new SQLiteConnection(App.PAY1_DB_PATH))
                {

                dbConn.DropTable<Plans>();
                dbConn.CreateTable<Plans>();
                dbConn.Dispose();
                dbConn.Close();

                }
            }
        }

    }
