//
//  GiftOfferDetailList.h
//  Pay1
//
//  Created by Annapurna on 15/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GiftOfferDetailList : NSObject

@property (nonatomic, strong) NSString *actualPrice;
@property (nonatomic, strong) NSString *couponExpiryDate;
@property (nonatomic, strong) NSString *created;
@property (nonatomic, strong) NSString *dealID;
@property (nonatomic, strong) NSString *dealName;
@property (nonatomic, strong) NSString *dealerContact;
@property (nonatomic, strong) NSString *discount;
@property (nonatomic, strong) NSString *globalFlag;
@property (nonatomic, strong) NSString *giftOfferID;
@property (nonatomic, strong) NSString *imageURL;
@property (nonatomic, strong) NSString *longDesc;
@property (nonatomic, strong) NSString *mailStatus;
@property (nonatomic, strong) NSString *maxLimitPerUser;
@property (nonatomic, strong) NSString *maxQuantity;
@property (nonatomic, strong) NSString *minAmount;
@property (nonatomic, strong) NSString *myLikes;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *offerAverage;
@property (nonatomic, strong) NSString *offerDesc;
@property (nonatomic, strong) NSString *offerPrice;
@property (nonatomic, strong) NSString *orderFlag;
@property (nonatomic, strong) NSString *review;
@property (nonatomic, strong) NSString *sequence;
@property (nonatomic, strong) NSString *shortDesc;
@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) NSString *stockSold;
@property (nonatomic, strong) NSString *totalStock;
@property (nonatomic, strong) NSString *updated;
@property (nonatomic, strong) NSString *validity;
@property (nonatomic, strong) NSString *validityMode;
@property (nonatomic, strong) NSString *byVouchers;



@end
