package com.pay1;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.database.SQLException;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.pay1.constants.Constants;
import com.pay1.customviews.GiftCellLayout;
import com.pay1.customviews.MyProgressDialog;
import com.pay1.databasehandler.FreeBies;
import com.pay1.databasehandler.FreeBiesDataSource;
import com.pay1.requesthandler.RequestClass;

public class GiftCategoryFragment extends Fragment {
	// private MyHorizontalLayout myHorizontalLayout;
	private LinearLayout linearLayout_Main;
	private FreeBiesDataSource freeBiesDataSource;
	private Typeface Reguler;Typeface Medium;

	public static GiftCategoryFragment newInstance() {
		GiftCategoryFragment fragment = new GiftCategoryFragment();
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// if ((savedInstanceState != null)
		// && savedInstanceState.containsKey(KEY_CONTENT)) {
		// mContent = savedInstanceState.getString(KEY_CONTENT);
		// }
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		final View view = inflater.inflate(R.layout.gift_category_fragment,
				container, false);
		try {

			freeBiesDataSource = new FreeBiesDataSource(getActivity());
			Reguler = Typeface.createFromAsset(getActivity().getAssets(),
					"EncodeSansNormal-400-Regular.ttf");
			
			 Medium = Typeface.createFromAsset(getActivity()
						.getAssets(), "EncodeSansNarrow-500-Medium.ttf");
			linearLayout_Main = (LinearLayout) view
					.findViewById(R.id.linearLayout_Main);

			new GiftTask().execute();

		} catch (Exception e) {
		}
		return view;
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		// outState.putString(KEY_CONTENT, mContent);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.Fragment#onDestroy()
	 */
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	public class GiftTask extends AsyncTask<String, String, String> implements
			OnDismissListener {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			try {

				String response = RequestClass.getInstance()
						.readPay1B2CRequest(
								getActivity(),
								Constants.B2C_URL
										+ "GetAllOfferCategory/?type=&next=0");

				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				if (this.dialog.isShowing()) {
					this.dialog.dismiss();
				}
			} catch (Exception e) {
			}
			try {
				if (!result.startsWith("Error")) {

					JSONObject jsonObject = new JSONObject(result);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {

						try {

							freeBiesDataSource.open();

							final JSONArray jsonArrayDesc = jsonObject
									.getJSONArray("description");

							for (int i = 0; i < jsonArrayDesc.length(); i++) {
								final String catName = jsonArrayDesc
										.getJSONObject(i).getString("name");
								final String catID = jsonArrayDesc
										.getJSONObject(i).getString("id");

								RelativeLayout relativeLayout = new RelativeLayout(
										getActivity());
								relativeLayout.setPadding(15, 5, 15, 5);
								relativeLayout.setGravity(Gravity.BOTTOM);
								{
									RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
											LayoutParams.WRAP_CONTENT,
											LayoutParams.WRAP_CONTENT);
									// Align bottom-right, and add
									// bottom-margin
									params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);

									TextView textView_Catagory = new TextView(
											getActivity());
									params.setMargins(0, 10, 0, 0);
									textView_Catagory
											.setGravity(Gravity.BOTTOM);
									textView_Catagory.setTypeface(Medium);
									textView_Catagory.setText(catName);
									textView_Catagory.setTextSize(15);

									textView_Catagory.setLayoutParams(params);
									relativeLayout.addView(textView_Catagory);
								}
								{
									RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
											LayoutParams.WRAP_CONTENT,
											LayoutParams.WRAP_CONTENT);
									// Align bottom-right, and add
									// bottom-margin
									params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
									params.addRule(RelativeLayout.ALIGN_BOTTOM);
									TextView textView = new TextView(
											getActivity());
									params.setMargins(0, 15, 0, 0);
									textView.setGravity(Gravity.BOTTOM);
									textView.setTypeface(Medium);
									textView.setText("See all ");
									textView.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_next,0);
									textView.setTextSize(12);

									textView.setLayoutParams(params);

									relativeLayout.addView(textView);
								}
								relativeLayout
										.setOnClickListener(new View.OnClickListener() {

											@Override
											public void onClick(View v) {
												// TODO Auto-generated
												// method stub
												Intent intent = new Intent(
														getActivity(),
														GiftGridActivity.class);
												intent.putExtra("NAME", catName);
												intent.putExtra("TYPE", catID);
												intent.putExtra("FROM", 1);
												/** 0 for GIFT 1 for Category **/
												startActivity(intent);
											}
										});
								linearLayout_Main.addView(relativeLayout);

								HorizontalScrollView hs = new HorizontalScrollView(
										getActivity());
								GiftCellLayout giftCell = new GiftCellLayout(
										getActivity());
								hs.addView(giftCell);
								linearLayout_Main.addView(hs);

								View v = new View(getActivity());
								LayoutParams layoutParams = new LayoutParams(
										LayoutParams.FILL_PARENT, 1);
								v.setLayoutParams(layoutParams);
								v.setBackgroundColor(getResources().getColor(
										R.color.LightGrey));
								v.setPadding(15, 0, 15, 0);
								linearLayout_Main.addView(v);

								// myHorizontalLayout = (MyHorizontalLayout)
								// view
								// .findViewById(R.id.mygallery);

								String str = jsonArrayDesc.getJSONObject(i)
										.getString("details");
								List<FreeBies> list = freeBiesDataSource
										.getAllFreeBies(str);
								int len = list.size();
								if (len != 0) {
									for (FreeBies freebie : list)
										giftCell.add(freebie);
								} else {
									// new FreeBiesTask()
									// .execute(
									// Utility.getCurrentLatitude(
									// getActivity(),
									// Constants.SHAREDPREFERENCE_CURRENT_LATITUDE),
									// Utility.getCurrentLongitude(
									// getActivity(),
									// Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE));
									// pager.setVisibility(View.GONE);

								}

							}
						} catch (SQLException exception) {
							// TODO: handle exception
							// Log.e("Er ", exception.getMessage());
						} catch (Exception e) {
							// TODO: handle exception
							// Log.e("Er ", e.getMessage());
						} finally {
							freeBiesDataSource.close();
						}
					}
				}

			} catch (JSONException e) {
				// e.printStackTrace();
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(getActivity());
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							dialog.dismiss();
							GiftTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			dialog.dismiss();
			GiftTask.this.cancel(true);
		}
	}
}
