package com.pay1;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pay1.adapterhandler.GiftFragmentAdapter;
import com.pay1.customviews.TabPageIndicator;

public class GiftMainActivity extends Fragment {

	int selected_index = 0;

	public static GiftMainActivity newInstance(int selected_index) {
		GiftMainActivity fragment = new GiftMainActivity();
		fragment.selected_index = selected_index;
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if ((savedInstanceState != null)
				&& savedInstanceState.containsKey("INDEX")) {
			selected_index = savedInstanceState.getInt("INDEX");
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		final Context contextThemeWrapper = new ContextThemeWrapper(
				getActivity(), R.style.StyledIndicators);
		inflater = inflater.cloneInContext(contextThemeWrapper);
		View view = inflater.inflate(R.layout.recharge_main_activity,
				container, false);
		try {

			GiftFragmentAdapter adapter = new GiftFragmentAdapter(getActivity()
					.getSupportFragmentManager());

			ViewPager pager = (ViewPager) view.findViewById(R.id.pager);
			pager.setAdapter(adapter);

			TabPageIndicator indicator = (TabPageIndicator) view
					.findViewById(R.id.indicator);
			indicator
					.setBackgroundColor(getResources().getColor(R.color.White));

			indicator.setViewPager(pager);
			pager.setCurrentItem(selected_index);

		} catch (Exception e) {
		}
		return view;
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt("INDEX", selected_index);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.Fragment#onDestroy()
	 */
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		// Intent intent = new Intent(getActivity(), MainActivity.class);
		// intent.putExtra("FROM_QUICKPAY", false);
		// intent.putExtra("IS_GIFT", false);
		// intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
		// | Intent.FLAG_ACTIVITY_NEW_TASK);
		// getActivity().startActivity(intent);
		// getActivity().finish();
	}
}