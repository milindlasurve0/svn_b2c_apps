package com.pay1.adapterhandler;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.larvalabs.svgandroid.SVGParser;
import com.pay1.R;

public class CallListAdapter extends BaseAdapter {
	private Context context;
	private ArrayList<HashMap<String, String>> data;
	LayoutInflater inflater;
	Typeface Reguler, Medium;

	public CallListAdapter(Context context,
			ArrayList<HashMap<String, String>> data) {
		this.context = context;
		this.data = data;
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		Reguler = Typeface.createFromAsset(context.getAssets(),
				"EncodeSansNormal-400-Regular.ttf");
		Medium = Typeface.createFromAsset(context.getAssets(),
				"EncodeSansNarrow-500-Medium.ttf");
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		// if (convertView == null) {

		convertView = inflater.inflate(R.layout.call_adapter, null);
		final ViewHolder viewHolder = new ViewHolder();
		viewHolder.text_adderess = (TextView) convertView
				.findViewById(R.id.text_adderess);
		viewHolder.text_area = (TextView) convertView
				.findViewById(R.id.text_area);
		viewHolder.imageCall=(ImageView)convertView.findViewById(R.id.imageCall);
		viewHolder.imageCall.setImageDrawable(SVGParser.getSVGFromResource(
				context.getResources(), R.raw.contact_list_icon)
				.createPictureDrawable());

		viewHolder.text_area.setText(data.get(position).get("area") + ","
				+ data.get(position).get("city"));
		viewHolder.text_adderess.setText(data.get(position).get("address"));
		// }
		return convertView;
	}

	private class ViewHolder {
		ImageView imageCall;
		TextView text_area, text_adderess;
	}
}
