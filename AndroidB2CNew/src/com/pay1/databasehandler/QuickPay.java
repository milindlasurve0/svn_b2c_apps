package com.pay1.databasehandler;

public class QuickPay {
	private int quickPayID;
	private int quickPayFlag;
	private String quickPayName;
	private int quickPayAmount;
	private int quickPaySTV;
	private int quickPayOperatorID;
	private int quickPayProductID;
	private String quickPayMobile;
	private int quickPayDelegateFlag;
	private String quickPayMissCallNumber;
	private String quickPayOperatorName;
	private String quickPayOperatorCode;
	private long quickPayUptateTime;
	private String quickPayTransactionDate;
	private int quickPayTransactionFlag;

	/**
	 * @return the quickPayID
	 */
	public int getQuickPayID() {
		return quickPayID;
	}

	/**
	 * @param quickPayID
	 *            the quickPayID to set
	 */
	public void setQuickPayID(int quickPayID) {
		this.quickPayID = quickPayID;
	}

	/**
	 * @return the quickPayFlag
	 */
	public int getQuickPayFlag() {
		return quickPayFlag;
	}

	/**
	 * @param quickPayFlag
	 *            the quickPayFlag to set
	 */
	public void setQuickPayFlag(int quickPayFlag) {
		this.quickPayFlag = quickPayFlag;
	}

	/**
	 * @return the quickPayName
	 */
	public String getQuickPayName() {
		return quickPayName;
	}

	/**
	 * @param quickPayName
	 *            the quickPayName to set
	 */
	public void setQuickPayName(String quickPayName) {
		this.quickPayName = quickPayName;
	}

	/**
	 * @return the quickPayAmount
	 */
	public int getQuickPayAmount() {
		return quickPayAmount;
	}

	/**
	 * @param quickPayAmount
	 *            the quickPayAmount to set
	 */
	public void setQuickPayAmount(int quickPayAmount) {
		this.quickPayAmount = quickPayAmount;
	}

	/**
	 * @return the quickPaySTV
	 */
	public int getQuickPaySTV() {
		return quickPaySTV;
	}

	/**
	 * @param quickPaySTV
	 *            the quickPaySTV to set
	 */
	public void setQuickPaySTV(int quickPaySTV) {
		this.quickPaySTV = quickPaySTV;
	}

	/**
	 * @return the quickPayOperatorID
	 */
	public int getQuickPayOperatorID() {
		return quickPayOperatorID;
	}

	/**
	 * @param quickPayOperatorID
	 *            the quickPayOperatorID to set
	 */
	public void setQuickPayOperatorID(int quickPayOperatorID) {
		this.quickPayOperatorID = quickPayOperatorID;
	}

	/**
	 * @return the quickPayProductID
	 */
	public int getQuickPayProductID() {
		return quickPayProductID;
	}

	/**
	 * @param quickPayProductID
	 *            the quickPayProductID to set
	 */
	public void setQuickPayProductID(int quickPayProductID) {
		this.quickPayProductID = quickPayProductID;
	}

	/**
	 * @return the quickPayMobile
	 */
	public String getQuickPayMobile() {
		return quickPayMobile;
	}

	/**
	 * @param quickPayMobile
	 *            the quickPayMobile to set
	 */
	public void setQuickPayMobile(String quickPayMobile) {
		this.quickPayMobile = quickPayMobile;
	}

	/**
	 * @return the quickPayDelegateFlag
	 */
	public int getQuickPayDelegateFlag() {
		return quickPayDelegateFlag;
	}

	/**
	 * @param quickPayDelegateFlag
	 *            the quickPayDelegateFlag to set
	 */
	public void setQuickPayDelegateFlag(int quickPayDelegateFlag) {
		this.quickPayDelegateFlag = quickPayDelegateFlag;
	}

	/**
	 * @return the quickPayMissCallNumber
	 */
	public String getQuickPayMissCallNumber() {
		return quickPayMissCallNumber;
	}

	/**
	 * @param quickPayMissCallNumber
	 *            the quickPayMissCallNumber to set
	 */
	public void setQuickPayMissCallNumber(String quickPayMissCallNumber) {
		this.quickPayMissCallNumber = quickPayMissCallNumber;
	}

	/**
	 * @return the quickPayOperatorName
	 */
	public String getQuickPayOperatorName() {
		return quickPayOperatorName;
	}

	/**
	 * @param quickPayOperatorName
	 *            the quickPayOperatorName to set
	 */
	public void setQuickPayOperatorName(String quickPayOperatorName) {
		this.quickPayOperatorName = quickPayOperatorName;
	}

	/**
	 * @return the quickPayOperatorCode
	 */
	public String getQuickPayOperatorCode() {
		return quickPayOperatorCode;
	}

	/**
	 * @param quickPayOperatorCode
	 *            the quickPayOperatorCode to set
	 */
	public void setQuickPayOperatorCode(String quickPayOperatorCode) {
		this.quickPayOperatorCode = quickPayOperatorCode;
	}

	/**
	 * @return the quickPayUptateTime
	 */
	public long getQuickPayUptateTime() {
		return quickPayUptateTime;
	}

	/**
	 * @param quickPayUptateTime
	 *            the quickPayUptateTime to set
	 */
	public void setQuickPayUptateTime(long quickPayUptateTime) {
		this.quickPayUptateTime = quickPayUptateTime;
	}

	/**
	 * @return the quickPayTransactionDate
	 */
	public String getQuickPayTransactionDate() {
		return quickPayTransactionDate;
	}

	/**
	 * @param quickPayTransactionDate
	 *            the quickPayTransactionDate to set
	 */
	public void setQuickPayTransactionDate(String quickPayTransactionDate) {
		this.quickPayTransactionDate = quickPayTransactionDate;
	}

	/**
	 * @return the quickPayTransactionFlag
	 */
	public int getQuickPayTransactionFlag() {
		return quickPayTransactionFlag;
	}

	/**
	 * @param quickPayTransactionFlag
	 *            the quickPayTransactionFlag to set
	 */
	public void setQuickPayTransactionFlag(int quickPayTransactionFlag) {
		this.quickPayTransactionFlag = quickPayTransactionFlag;
	}

}
