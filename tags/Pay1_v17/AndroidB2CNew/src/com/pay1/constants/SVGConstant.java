package com.pay1.constants;

import com.pay1.R;

public class SVGConstant {

	public static int getSVGResource(int index) {

		switch (index) {
		case 1:
			return R.raw.ic_home;
		case 2:
			return R.raw.ic_recharges;
		case 3:
			return R.raw.ic_gifts;
//		case 4:
//			return R.raw.ic_mygift;
		case 4:
			return R.raw.ic_wallethistory;
		case 5:
			return R.raw.ic_missedcallrecharge;
		case 6:
			return R.raw.ic_myprofile;
		case 7:
			return R.raw.ic_logout;
		case 8:
			return R.raw.ic_home_line;
		case 9:
			return R.raw.ic_recharge_line;
		case 10:
			return R.raw.ic_gifts_line;
		case 11:
			return R.raw.ic_add_money_line;
		default:
			return R.raw.ic_myrecharges;
		}

	}
}
