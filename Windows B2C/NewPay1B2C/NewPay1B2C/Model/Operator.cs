﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Imaging;

namespace NewPay1B2C.Model
	{
	class Operator
		{
		public Operator ()
			{
			}
		public Operator (string name1,BitmapImage imagePath1)
			{
			name = name1;
			imagePath = imagePath1;
			}

		string _name;
		BitmapImage _imagePath;

		public string name
			{
			get { return _name; }
			set { _name = value; }
			}
		public BitmapImage imagePath
			{
			get { return _imagePath; }
			set { _imagePath = value; }
			}


		}
	}
