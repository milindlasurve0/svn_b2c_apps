//
//  CoinHistoryService.m
//  Pay1
//
//  Created by webninjaz on 09/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "CoinHistoryService.h"
#import "CoinHistoryCriteria.h"
#import "CoinHistoryResponse.h"
#import "CoinHistoryList.h"

@implementation CoinHistoryService

//-(void)callCoinHistoryService:(CoinHistoryCriteria *)criteria callback:(coinHistoryResponse)callback{
-(void)callCoinHistoryService:(coinHistoryResponse)callback{
    NSMutableDictionary *postData = [[NSMutableDictionary alloc]init];
   // [postData setObject:criteria.coinHistoryParamCriteria forKey:@"Param"];
   // [postData setObject:criteria.coinHistoryPageCriteria forKey:@"page"];
   // [postData setObject:criteria.coinHistoryLimitCriteria forKey:@"limit"];
    
    NSLog(@"PostData for Update Password :%@",postData);
    
    RequestData *requestData = [[RequestData alloc]init];
    requestData.baseURL = kBaseURL;
    requestData.webServiceURL = kCoinHistoryURL;
    requestData.postData = postData;
    requestData.showLoader = FALSE;
    
    requestData.requestSuccessBlock = ^(RequestData *request) {
    [self printResponse:request.responseData forRequest:@"Update Coin History Response"];
    if ([self handleNullResponse:request])
    return;
        
    callback(nil, [self parseCoinHistoryResponse:request]);
    };
    
    requestData.requestFailureBlock = ^(RequestData *request) {
        [self handleNetworkError:request.responseData];
        callback(request.responseData, nil);
    };
    
    [[ServiceManager sharedInstance] makeRequest:requestData];
}

-(CoinHistoryResponse *)parseCoinHistoryResponse:(RequestData *)request{
    CoinHistoryResponse *objCoinHistoryResponse = [[CoinHistoryResponse alloc]init];
    objCoinHistoryResponse.statusCoinHistoryResponse = [request.responseData objectForKey:@"status"];
    objCoinHistoryResponse.errorCodeCoinHistoryResponse = [request.responseData objectForKey:@"errCode"];
    NSLog(@"The code wallet response is%@",objCoinHistoryResponse.errorCodeCoinHistoryResponse);
    
    if ([objCoinHistoryResponse.statusCoinHistoryResponse isEqualToString:@"failure"]) {
        return objCoinHistoryResponse;
    }
    
    NSMutableArray *objCoinResponseArray = [request.responseData valueForKey:@"description"];
    NSLog(@"The object wallet response is%@",objCoinResponseArray);
    
    for(NSMutableDictionary *dic in objCoinResponseArray){
        CoinHistoryList *coinHistoryList = [[CoinHistoryList alloc]init];
        coinHistoryList.coinName = [dic objectForKey:@"Name"];
        coinHistoryList.coinCoinsValue = [dic objectForKey:@"coins"];
        coinHistoryList.coinOperator = [dic objectForKey:@"msg"];
        coinHistoryList.coinRechargeType = [dic objectForKey:@"rechargeType"];
        coinHistoryList.coinRedeemStatus = [dic objectForKey:@"redeem_status"];
        coinHistoryList.coinTransactionCategory = [dic objectForKey:@"trans_category"];
        coinHistoryList.coinTransactionDateTime = [dic objectForKey:@"trans_datetime"];
      //  NSLog(@"The transaction date time is%@",coinHistoryList.coinTransactionDateTime);
        coinHistoryList.coinTransactionID = [dic objectForKey:@"trans_id"];
        coinHistoryList.coinTransactionAmount = [dic objectForKey:@"transaction_amount"];
        [objCoinHistoryResponse.coinHistoryResponse addObject:coinHistoryList];
    }
    
    return objCoinHistoryResponse;
}

@end
