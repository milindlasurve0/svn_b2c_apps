//
//  WalletHistoryResponse.h
//  Pay1
//
//  Created by webninjaz on 19/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WalletHistoryResponse : NSObject

@property (nonatomic, strong) NSString *errorCode;
@property (nonatomic, strong) NSString *walletStatus;

@property (nonatomic, strong) NSMutableArray *walletDataArray;



@end
