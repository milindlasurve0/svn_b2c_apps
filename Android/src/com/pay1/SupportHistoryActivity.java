package com.pay1;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.WeakHashMap;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.pay1.customviews.MyProgressDialog;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnDismissListener;
import android.content.res.Configuration;
import android.database.DataSetObserver;
import android.database.SQLException;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.pay1.adapterhandler.SupportHistoryAdapter;
import com.pay1.constants.Constants;
import com.pay1.databasehandler.Support;
import com.pay1.databasehandler.SupportDataSource;
import com.pay1.requesthandler.RequestClass;
import com.pay1.utilities.EditTextValidator;
import com.pay1.utilities.Utility;

public class SupportHistoryActivity extends AppBaseActivity {

	private static final String SCREEN_LABEL = "Suppot History Screen";
	private EasyTracker easyTracker = null;
	private final String TAG = "Support";

	private ImageView imageView_Back, imageView_Ok, imageView_Cancel;
	private EditText editText_Query;
	private ListView listView_SupportHistory;
	private SupportHistoryAdapter adapter;
	private TextView textView_Title;
	private ArrayList<WeakHashMap<String, String>> data;
	private SupportDataSource supportDataSource;

	public static final String TITLE = "title";
	public static final String TIME = "time";
	public static final String QUERY = "query";

	// private long timestamp = 0;

	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.support_history_activity);
		try {
			easyTracker = EasyTracker.getInstance(SupportHistoryActivity.this);
			easyTracker.set(Fields.SCREEN_NAME, SCREEN_LABEL);
			registerBaseActivityReceiver();

			// timestamp = System.currentTimeMillis();

			supportDataSource = new SupportDataSource(
					SupportHistoryActivity.this);

			Typeface Reguler = Typeface.createFromAsset(getAssets(),
					"EncodeSansNormal-400-Regular.ttf");

			Typeface Normal = Typeface.createFromAsset(getAssets(),
					"EncodeSansNormal-100-Thin.ttf");

			textView_Title = (TextView) findViewById(R.id.textView_Title);
			textView_Title.setTypeface(Reguler);

			imageView_Back = (ImageView) findViewById(R.id.imageView_Back);
			imageView_Back.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onBackPressed();
				}
			});

			data = new ArrayList<WeakHashMap<String, String>>();
			listView_SupportHistory = (ListView) findViewById(R.id.listView_SupportHistory);
			// textView_NoData = (TextView) findViewById(R.id.textView_NoData);
			// listView_SupportHistory.setEmptyView(textView_NoData);
			adapter = new SupportHistoryAdapter(SupportHistoryActivity.this,
					data);
			listView_SupportHistory.setAdapter(adapter);
			adapter.registerDataSetObserver(new DataSetObserver() {
				@Override
				public void onChanged() {
					super.onChanged();
					listView_SupportHistory.setSelection(adapter.getCount() - 1);
				}
			});

			try {
				supportDataSource.open();
				data.clear();
				List<Support> support = supportDataSource.getAllSupport();
				for (int i = 0; i < support.size(); i++) {
					WeakHashMap<String, String> map = new WeakHashMap<String, String>();
					map.put(TITLE, support.get(i).getSupportSent() == 1 ? "Me"
							: "Pay1");
					map.put(TIME, String.valueOf(support.get(i)
							.getSupportUptateTime()));
					map.put(QUERY, support.get(i).getSupportQuery());
					data.add(map);
				}

				SupportHistoryActivity.this.runOnUiThread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						adapter.notifyDataSetChanged();
					}
				});
			} catch (SQLException exception) {
				// TODO: handle exception
				// // Log.e("Er ", exception.getMessage());
			} catch (Exception e) {
				// TODO: handle exception
				// // Log.e("Er ", e.getMessage());
			} finally {
				supportDataSource.close();
			}

			editText_Query = (EditText) findViewById(R.id.editText_Query);
			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(editText_Query.getWindowToken(), 0);
			editText_Query.setTypeface(Normal);
			editText_Query.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					// TODO Auto-generated method stub
					if (editText_Query.getText().toString().trim().length() > 0)
						EditTextValidator.hasText(SupportHistoryActivity.this,
								editText_Query,
								Constants.ERROR_QUERY_BLANK_FIELD);
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					// TODO Auto-generated method stub

				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

				}
			});

			imageView_Ok = (ImageView) findViewById(R.id.imageView_Ok);
			imageView_Ok.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					if (EditTextValidator.hasText(SupportHistoryActivity.this,
							editText_Query, Constants.ERROR_QUERY_BLANK_FIELD)) {
						new SupportTask().execute(Utility.getMobileNumber(
								SupportHistoryActivity.this,
								Constants.SHAREDPREFERENCE_MOBILE),
								editText_Query.getText().toString().trim());
					}

				}
			});
			imageView_Cancel = (ImageView) findViewById(R.id.imageView_Cancel);
			imageView_Cancel.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					editText_Query.setText("");
				}
			});

		} catch (Exception e) {
		}
		overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}

	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView().build());
		// EasyTracker.getInstance(this).activityStart(this);
		easyTracker.send(MapBuilder.createAppView().build());
	}

	@Override
	public void onStop() {
		super.onStop();
		// EasyTracker.getInstance(this).activityStop(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (Constants.isExpanded) {
			Constants.collapsView(this);
		} else {
			Constants.SELECTED_MENU_INDEX = Constants.PROFILE_MENU_INDEX;
			Constants.showNavigationBar(SupportHistoryActivity.this);
		}
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unRegisterBaseActivityReceiver();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (Constants.isExpanded) {
			Constants.collapsView(this);
		} else {
			super.onBackPressed();
			overridePendingTransition(R.anim.slide_in_left,
					R.anim.slide_out_right);
		}
	}

	public class SupportTask extends AsyncTask<String, String, String>
			implements OnDismissListener {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			try {
				String response = RequestClass
						.getInstance()
						.readPay1B2CRequest(
								SupportHistoryActivity.this,
								Constants.B2C_URL
										+ "support_req/?mobile_number="
										+ params[0] + "&message="
										+ URLEncoder.encode(params[1], "utf-8"));

				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			try {

				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONObject jsonObject = new JSONObject(replaced);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {

						try {
							supportDataSource.open();

							supportDataSource.createSupport(1, editText_Query
									.getText().toString().trim(),
									System.currentTimeMillis());
						} catch (SQLException exception) {
						} catch (Exception e) {
						} finally {
							supportDataSource.close();
						}
						Constants.showOneButtonDialog(
								SupportHistoryActivity.this, TAG,
								"Request sent successfully.",
								Constants.DIALOG_CLOSE);
						editText_Query.setText("");

						try {
							supportDataSource.open();
							data.clear();
							List<Support> support = supportDataSource
									.getAllSupport();
							for (int i = 0; i < support.size(); i++) {
								WeakHashMap<String, String> map = new WeakHashMap<String, String>();
								map.put(TITLE,
										support.get(i).getSupportSent() == 1 ? "Me"
												: "Pay1");
								map.put(TIME, String.valueOf(support.get(i)
										.getSupportUptateTime()));
								map.put(QUERY, support.get(i).getSupportQuery());
								data.add(map);
							}

							SupportHistoryActivity.this
									.runOnUiThread(new Runnable() {

										@Override
										public void run() {
											// TODO Auto-generated method stub
											adapter.notifyDataSetChanged();
										}
									});
						} catch (SQLException exception) {
							// TODO: handle exception
							// // Log.e("Er ", exception.getMessage());
						} catch (Exception e) {
							// TODO: handle exception
							// // Log.e("Er ", e.getMessage());
						} finally {
							supportDataSource.close();
						}

					} else {
						Constants.showOneButtonDialog(
								SupportHistoryActivity.this, TAG,
								Constants.checkCode(replaced),
								Constants.DIALOG_CLOSE);
					}
				} else {
					// Constants.showOneButtonDialog(SupportHistoryActivity.this,
					// TAG, Constants.ERROR_INTERNET,
					// Constants.DIALOG_CLOSE);
					Intent intent = new Intent(SupportHistoryActivity.this,
							ConnectivityErrorActivity.class);
					intent.putExtra(Constants.RECHARGE_FOR,
							Constants.WALLET_PAYMENT);
					startActivity(intent);

				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonDialog(SupportHistoryActivity.this, TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonDialog(SupportHistoryActivity.this, TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(SupportHistoryActivity.this);
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							SupportTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			SupportTask.this.cancel(true);
			dialog.cancel();
		}

	}
}