package com.pay1.adapterhandler;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.larvalabs.svgandroid.SVGParser;
import com.pay1.R;
import com.pay1.constants.Constants;
import com.pay1.constants.OperatorConstant;

public class PurchaseAdapter extends BaseAdapter {

	private Context context;
	private ArrayList<JSONObject> data;
	LayoutInflater inflater;
	Typeface Reguler, Medium;

	public PurchaseAdapter(Context context, ArrayList<JSONObject> data) {
		this.context = context;
		this.data = data;
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		Reguler = Typeface.createFromAsset(context.getAssets(),
				"Roboto-Regular.ttf");
		Medium = Typeface.createFromAsset(context.getAssets(),
				"Roboto-Regular.ttf");
	}

	@SuppressLint("NewApi")
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final ViewHolder viewHolder;
		// if (convertView == null) {

		convertView = inflater.inflate(R.layout.purchase_adapter, null);
		// }
		viewHolder = new ViewHolder();
		viewHolder.textContactName = (TextView) convertView
				.findViewById(R.id.textContactName);
		viewHolder.textViewNumber = (TextView) convertView
				.findViewById(R.id.textViewNumber);
		viewHolder.textOperator = (TextView) convertView
				.findViewById(R.id.textOperator);
		viewHolder.textViewAmount = (TextView) convertView
				.findViewById(R.id.textViewAmount);
		viewHolder.textViewAmount.setTypeface(Medium);
		viewHolder.textContactName.setTypeface(Reguler);
		viewHolder.textViewNumber.setTypeface(Reguler);
		viewHolder.textOperator.setTypeface(Medium);
		viewHolder.imageView_Image = (ImageView) convertView
				.findViewById(R.id.imageView_Image);
		viewHolder.linearLayout_Image = (LinearLayout) convertView
				.findViewById(R.id.linearLayout_Image);
		viewHolder.imageView_Misscall = (ImageView) convertView
				.findViewById(R.id.imageView_Misscall);
		viewHolder.imageStatusSuccess = (ImageView) convertView
				.findViewById(R.id.imageStatusSuccess);
		viewHolder.linearLayout = (LinearLayout) convertView
				.findViewById(R.id.linearLayout);

		convertView.setTag(viewHolder);
		try {

			if (data.get(position).getString("trans_category")
					.equalsIgnoreCase("deal")) {
				viewHolder.textViewNumber.setText(data.get(position).getString(
						"offer_name"));
				viewHolder.textOperator.setText(data.get(position).getString(
						"deal_name"));
				viewHolder.imageView_Misscall.setVisibility(View.GONE);
			} else {
				viewHolder.textViewNumber.setText(data.get(position).getString(
						"number"));

				viewHolder.imageView_Misscall.setVisibility(View.GONE);
			}
			viewHolder.textViewAmount.setText("Rs. "
					+ data.get(position).getString("transaction_amount"));

			int statusId = 0;
			try {
				statusId = Integer.parseInt(data.get(position).getString(
						"status"));
			} catch (Exception e) {
				statusId = 0;
			}
			// Bitmap bitmap=Constants.getBitmapById(statusId,context);
			switch (statusId) {
			case 0:
			case 1:
				// viewHolder.imageStatusSuccess
				// .setImageResource(R.drawable.nav_inprocess);
				viewHolder.imageStatusSuccess.setImageDrawable(SVGParser
						.getSVGFromResource(context.getResources(),
								R.raw.ic_inprocess).createPictureDrawable());
				if (Build.VERSION.SDK_INT >= 11)
					viewHolder.imageStatusSuccess.setLayerType(
							View.LAYER_TYPE_SOFTWARE, null);
				break;
			case 2:
				viewHolder.imageStatusSuccess.setImageDrawable(SVGParser
						.getSVGFromResource(context.getResources(),
								R.raw.successful).createPictureDrawable());
				if (Build.VERSION.SDK_INT >= 11)
					viewHolder.imageStatusSuccess.setLayerType(
							View.LAYER_TYPE_SOFTWARE, null);
				// viewHolder.imageStatusSuccess
				// .setImageResource(R.drawable.nav_ok);
				break;
			case 3:
			case 4:
				viewHolder.imageStatusSuccess.setImageDrawable(SVGParser
						.getSVGFromResource(context.getResources(),
								R.raw.unsuccessful_tran).createPictureDrawable());
				if (Build.VERSION.SDK_INT >= 11)
					viewHolder.imageStatusSuccess.setLayerType(
							View.LAYER_TYPE_SOFTWARE, null);
				// viewHolder.imageStatusSuccess
				// .setImageResource(R.drawable.nav_cancel);
				break;
			// case 3:
			// viewHolder.imageStatusSuccess
			// .setImageResource(R.drawable.nav_ok_b);
			}

			// viewHolder.textContactName.setText(Constants.getContactName(context,
			// data.get(position).getString("number")));

			int service_id = 1;
			try {
				service_id = Integer.parseInt(data.get(position).getString(
						"service_id"));
			} catch (Exception e) {
				service_id = 1;
			}

			// Bitmap bitmap=Constants.getBitmapById(statusId,context);

			switch (service_id) {
			case 1:

				viewHolder.imageView_Image.setImageResource(OperatorConstant
						.getOperatorResource(Integer.parseInt(data
								.get(position).getString("product_id"))));
				viewHolder.textOperator.setText("Prepaid recharge");
				viewHolder.textOperator.setVisibility(View.VISIBLE);
				break;
			case 2:

				viewHolder.imageView_Image.setImageResource(OperatorConstant
						.getOperatorResource(Integer.parseInt(data
								.get(position).getString("product_id"))));
				viewHolder.textOperator.setText("DTH recharge");
				viewHolder.textOperator.setVisibility(View.VISIBLE);
				break;
			case 4:

				viewHolder.imageView_Image.setImageResource(OperatorConstant
						.getOperatorResource(Integer.parseInt(data
								.get(position).getString("product_id"))));
				viewHolder.textOperator.setText("Postpaid bill");
				viewHolder.textOperator.setVisibility(View.VISIBLE);
				break;
			case 5:

				viewHolder.imageView_Image.setImageResource(OperatorConstant
						.getOperatorResource(Integer.parseInt(data
								.get(position).getString("product_id"))));
				viewHolder.textOperator.setText("Datacard recharge");
				viewHolder.textOperator.setVisibility(View.VISIBLE);
				break;
			case 98:
				/*
				 * viewHolder.linearLayout_Image.setBackgroundColor(context
				 * .getResources().getColor(R.color.app_blue_color));
				 * viewHolder.imageView_Image.setImageDrawable(SVGParser
				 * .getSVGFromResource(context.getResources(),
				 * R.raw.ic_wallet).createPictureDrawable());
				 */

				viewHolder.linearLayout_Image.setBackgroundColor(context
						.getResources().getColor(R.color.white));
			/*	viewHolder.imageView_Image.setImageDrawable(context
						.getResources().getDrawable(
								R.raw.ic_profile_wallet));*/
				viewHolder.imageView_Image.setImageDrawable(SVGParser
						.getSVGFromResource(context.getResources(),
								R.raw.ic_profile_wallet).createPictureDrawable());
				viewHolder.linearLayout.setBackground(context.getResources()
						.getDrawable(R.drawable.wallet_circle_view));

				if (Build.VERSION.SDK_INT >= 11)
					viewHolder.imageView_Image.setLayerType(
							View.LAYER_TYPE_SOFTWARE, null);
				viewHolder.textOperator.setText("Wallet topup");
				viewHolder.imageView_Misscall.setVisibility(View.GONE);
				viewHolder.textOperator.setVisibility(View.VISIBLE);
				break;
			case 99:
				/*
				 * viewHolder.linearLayout_Image.setBackgroundColor(context
				 * .getResources().getColor(R.color.app_blue_color));
				 * viewHolder.imageView_Image.setImageDrawable(SVGParser
				 * .getSVGFromResource(context.getResources(),
				 * R.raw.ic_gifts).createPictureDrawable());
				 */

				viewHolder.linearLayout_Image.setBackgroundColor(context
						.getResources().getColor(R.color.white));
				/*viewHolder.imageView_Image.setImageDrawable(context
						.getResources().getDrawable(
								R.raw.ic_profile_wallet));*/
				viewHolder.imageView_Image.setImageDrawable(SVGParser
						.getSVGFromResource(context.getResources(),
								R.raw.ic_giftcard).createPictureDrawable());
				viewHolder.linearLayout.setBackground(context.getResources()
						.getDrawable(R.drawable.wallet_circle_view));

				if (Build.VERSION.SDK_INT >= 11)
					viewHolder.imageView_Image.setLayerType(
							View.LAYER_TYPE_SOFTWARE, null);
				viewHolder.imageView_Misscall.setVisibility(View.GONE);
				viewHolder.textOperator.setVisibility(View.VISIBLE);
				break;
			}

			// viewHolder.textContactName.setText(Constants
			// .getContactNameFromList(context, data.get(position)
			// .getString("number")));

			if (viewHolder.textContactName.getText().toString().trim()
					.equalsIgnoreCase(""))
				viewHolder.textContactName.setText("Unknown");

			viewHolder.textContactName.setVisibility(View.GONE);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
		Constants.setFont(context, (ViewGroup)convertView);
		return convertView;
	}

	private class ViewHolder {
		ImageView imageStatusSuccess, imageView_Image, imageView_Misscall;
		TextView textViewAmount, textOperator, textViewNumber, textContactName;
		LinearLayout linearLayout_Image, linearLayout;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}
}
