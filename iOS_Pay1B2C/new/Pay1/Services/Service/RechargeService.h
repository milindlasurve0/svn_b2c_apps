//
//  RechargeService.h
//  Pay1
//
//  Created by Annapurna on 15/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "Service.h"
@class RechargeCriteria;
@class RechargeResponse;
typedef void (^rechargeResponse)(NSError *error, RechargeResponse *objRechargeResponse);
typedef void (^onlineRefillResponse)(NSError *error, RechargeResponse *objOnlineRefillResponse);
typedef void (^couponRedeemResponse)(NSError *error, RechargeResponse *objOnlineRefillResponse);

@interface RechargeService : Service

-(void)callRechargeService:(RechargeCriteria *)criteria callback:(rechargeResponse)callback;
-(void)callOnlineWalletRefillService:(NSString *)amountToRefill callback:(onlineRefillResponse)callback;
-(void)callCouponRedeemService:(NSString *)voucherCode callback:(couponRedeemResponse)callback;


@end
