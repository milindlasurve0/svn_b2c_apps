package com.pay1.databasehandler;

public class Support {

	private int supportID;
	private int supportSent;
	private String supportQuery;
	private long supportUptateTime;

	public int getSupportID() {
		return supportID;
	}

	public void setSupportID(int supportID) {
		this.supportID = supportID;
	}

	public int getSupportSent() {
		return supportSent;
	}

	public void setSupportSent(int supportSent) {
		this.supportSent = supportSent;
	}

	public String getSupportQuery() {
		return supportQuery;
	}

	public void setSupportQuery(String supportQuery) {
		this.supportQuery = supportQuery;
	}

	public long getSupportUptateTime() {
		return supportUptateTime;
	}

	public void setSupportUptateTime(long supportUptateTime) {
		this.supportUptateTime = supportUptateTime;
	}

}
