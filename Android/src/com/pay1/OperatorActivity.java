package com.pay1;

import java.util.ArrayList;
import java.util.WeakHashMap;
import java.util.List;
import java.util.Map;

import android.content.Intent;
import android.database.SQLException;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.pay1.adapterhandler.OperatorAdapter;
import com.pay1.constants.Constants;
import com.pay1.databasehandler.Operator;
import com.pay1.databasehandler.OperatorDataSource;

public class OperatorActivity extends AppBaseActivity {
	private static final String SCREEN_LABEL = "Operator Screen";
	private EasyTracker easyTracker = null;
	private EditText editText_Operator;
	private ListView listView_Operators;
	private ImageView imageView_Clear, imageView_Back;
	private TextView textView_Back, textView_Title;

	public static final String OPERATOR_NAME = "name";
	public static final String OPERATOR_CODE = "code";
	public static final String OPERATOR_ID = "id";
	public static final String OPERATOR_PRODUCT_ID = "p_id";
	public static final String OPERATOR_STV = "stv";

	private OperatorAdapter adapter;
	ArrayList<WeakHashMap<String, String>> operator_list = new ArrayList<WeakHashMap<String, String>>();
	ArrayList<WeakHashMap<String, String>> operator_list_temp = new ArrayList<WeakHashMap<String, String>>();

	private OperatorDataSource operatorDataSource;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.operator_activity);

		try {

			easyTracker = EasyTracker.getInstance(OperatorActivity.this);
			easyTracker.set(Fields.SCREEN_NAME, SCREEN_LABEL);
			registerBaseActivityReceiver();

			operatorDataSource = new OperatorDataSource(OperatorActivity.this);

			Typeface Reguler = Typeface.createFromAsset(getAssets(),
					"EncodeSansNormal-400-Regular.ttf");

			textView_Back = (TextView) findViewById(R.id.textView_Back);
			textView_Back.setTypeface(Reguler);
			textView_Title = (TextView) findViewById(R.id.textView_Title);
			textView_Title.setTypeface(Reguler);

			imageView_Back = (ImageView) findViewById(R.id.imageView_Back);
			imageView_Back.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onBackPressed();
				}
			});

			imageView_Clear = (ImageView) findViewById(R.id.imageView_Clear);
			imageView_Clear.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					editText_Operator.setText("");
					editText_Operator.requestFocus();
				}
			});

			editText_Operator = (EditText) findViewById(R.id.editText_Operator);
			editText_Operator.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					// TODO Auto-generated method stub
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					// TODO Auto-generated method stub

				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub
					try {
						String keyToSearch = editText_Operator.getText()
								.toString().trim();

						if (!keyToSearch.equalsIgnoreCase("")) {
							operator_list_temp.clear();
							for (Map<String, String> map : operator_list) {

								if (map.get(OPERATOR_NAME).toLowerCase()
										.startsWith(keyToSearch.toLowerCase())) {
									// // Log.e("VAl ", "Found : " + key
									// + " / value : " + map.values());

									// WeakHashMap
									WeakHashMap<String, String> map1 = new WeakHashMap<String, String>();

									map1.put(OPERATOR_NAME,
											map.get(OPERATOR_NAME));
									map1.put(OPERATOR_CODE,
											map.get(OPERATOR_CODE));
									map1.put(OPERATOR_ID, map.get(OPERATOR_ID));
									map1.put(OPERATOR_PRODUCT_ID,
											map.get(OPERATOR_PRODUCT_ID));
									map1.put(OPERATOR_STV,
											map.get(OPERATOR_STV));
									operator_list_temp.add(map1);
								}
							}
							OperatorActivity.this.runOnUiThread(new Runnable() {

								public void run() {
									adapter.notifyDataSetChanged();
								}
							});
						} else {
							OperatorActivity.this.runOnUiThread(new Runnable() {

								public void run() {
									operator_list_temp.clear();
									operator_list_temp.addAll(operator_list);
									adapter.notifyDataSetChanged();
								}
							});
						}
					} catch (Exception e) {
						// TODO: handle exception
					}
				}
			});

			listView_Operators = (ListView) findViewById(R.id.listView_Operators);
			adapter = new OperatorAdapter(OperatorActivity.this,
					operator_list_temp);

			// Set adapter to AutoCompleteTextView
			listView_Operators.setAdapter(adapter);
			listView_Operators
					.setOnItemClickListener(new AdapterView.OnItemClickListener() {

						@Override
						public void onItemClick(AdapterView<?> parent,
								View view, int position, long id) {
							Intent returnIntent = new Intent();
							WeakHashMap<String, String> map = new WeakHashMap<String, String>();
							map = operator_list_temp.get(position);
							returnIntent.putExtra("OperatorName",
									"" + map.get(OPERATOR_NAME));
							returnIntent.putExtra("OperatorCode",
									"" + map.get(OPERATOR_CODE));
							returnIntent.putExtra("OperatorID",
									"" + map.get(OPERATOR_ID));
							returnIntent.putExtra("OperatorProductID",
									"" + map.get(OPERATOR_PRODUCT_ID));
							returnIntent.putExtra("STV",
									"" + map.get(OPERATOR_STV));
							setResult(RESULT_OK, returnIntent);
							onBackPressed();
						}
					});

			try {
				operatorDataSource.open();

				List<Operator> operators = operatorDataSource
						.getAllOperator(getIntent().getExtras().getInt(
								Constants.RECHARGE_FOR));

				operator_list.clear();

				for (int i = 0; i < operators.size(); i++) {
					WeakHashMap<String, String> map = new WeakHashMap<String, String>();
					map.put(OPERATOR_NAME, operators.get(i).getOperatorName());
					map.put(OPERATOR_CODE, operators.get(i).getOperatorCode());
					map.put(OPERATOR_ID, "" + operators.get(i).getOperatorID());
					map.put(OPERATOR_PRODUCT_ID, ""
							+ operators.get(i).getOperatorProductID());
					map.put(OPERATOR_STV, ""
							+ operators.get(i).getOperatorSTV());

					operator_list.add(map);
				}

				OperatorActivity.this.runOnUiThread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						operator_list_temp.clear();
						operator_list_temp.addAll(operator_list);
						adapter.notifyDataSetChanged();
					}
				});

			} catch (SQLException exception) {
				// TODO: handle exception
				// // Log.e("Er ", exception.getMessage());
			} catch (Exception e) {
				// TODO: handle exception
				// // Log.e("Er ", e.getMessage());
			} finally {
				operatorDataSource.close();
			}
		} catch (Exception exception) {

		}
		overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}

	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView().build());
		// EasyTracker.getInstance(this).activityStart(this);
		easyTracker.send(MapBuilder.createAppView().build());
	}

	@Override
	public void onStop() {
		super.onStop();
		// EasyTracker.getInstance(this).activityStop(this);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (Constants.isExpanded) {
			Constants.collapsView(this);
		} else {
			super.onBackPressed();
			overridePendingTransition(R.anim.slide_in_left,
					R.anim.slide_out_right);
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		int rechargeType = getIntent().getExtras().getInt(
				Constants.RECHARGE_FOR);
		if (rechargeType == Constants.RECHARGE_MOBILE
				|| rechargeType == Constants.RECHARGE_DTH
				|| rechargeType == Constants.RECHARGE_DATA) {
			textView_Back.setTextColor(getResources().getColor(
					R.color.app_green_color));
			imageView_Back
					.setImageResource(R.drawable.button_recharge_back_icon);
			if (rechargeType == Constants.RECHARGE_MOBILE)
				textView_Back.setText("Recharge > Mobile > Operator");
			else if (rechargeType == Constants.RECHARGE_DTH)
				textView_Back.setText("Recharge > DTH > Back");
			else if (rechargeType == Constants.RECHARGE_DATA)
				textView_Back.setText("Recharge > Data card > Operator");
		} else if (rechargeType == Constants.SUBSCRIBE_DEAL) {
			textView_Back.setTextColor(getResources().getColor(
					R.color.app_orange_color));
			imageView_Back.setImageResource(R.drawable.button_deal_back_icon);
			textView_Back.setText("Deal > Offer > Operator");
		} else if (rechargeType == Constants.BILL_PAYMENT) {
			textView_Back.setTextColor(getResources().getColor(
					R.color.app_skyblue_color));
			imageView_Back.setImageResource(R.drawable.button_bill_back_icon);
			textView_Back.setText("Bills > Mobile bill > Operator");
		}
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unRegisterBaseActivityReceiver();
	}

}
