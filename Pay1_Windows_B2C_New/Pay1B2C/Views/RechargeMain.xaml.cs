﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Pay1B2C.ViewModel;
using System.Collections.ObjectModel;
using Pay1B2C.Model;
using System.Windows.Media;
using Pay1B2C.Utilities;
using Pay1B2C.UserControls;
using SQLite;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Windows.Controls.Primitives;
using Pay1B2C.PopUp;
using Microsoft.Phone.Tasks;
using System.Globalization;

namespace Pay1B2C.Views
    {
    public partial class RechargeMain: PhoneApplicationPage
        {
        OperatorDB oper;
        Popup popup;
        NumberAndCircle number;
        public static String flag;
        TextBox quickTextBox;
        string isStv="1";
        bool isQuickPay=false;
        ProgressBarManager ProgressBarManager;
        PhoneNumberChooserTask phoneNumberChooserTask;
        public RechargeMain()
            {
            InitializeComponent();
            Constant.laodNavigationBar(TitlePanel, LayoutRoot, canvas, "Recharge Main Page");
            GoogleAnalytics.EasyTracker.GetTracker().SendView("RechargeMainPage");
            ProgressIndicator prog = new ProgressIndicator();
            ProgressBarManager = new ProgressBarManager();
            SystemTray.SetProgressIndicator(this, prog);
            ProgressBarManager.Hook(prog);
            textMobOperatorPanel.Tap+=(a, b) =>
            {
            NavigationService.Navigate(new Uri("/Views/OperatorList.xaml?", UriKind.RelativeOrAbsolute));
            };

            textDthOperatorPanel.Tap+=(a, b) =>
            {
                NavigationService.Navigate(new Uri("/Views/OperatorList.xaml?", UriKind.RelativeOrAbsolute));
            };

            textDataOperatorPanel.Tap+=(a, b) =>
            {
                NavigationService.Navigate(new Uri("/Views/OperatorList.xaml?", UriKind.RelativeOrAbsolute));
            };

            }

        private void textPlansEvent_Tap(object sender, System.Windows.Input.GestureEventArgs e)
            {
            if(!flag.Equals("2"))
                {
                if(oper!=null && number!=null)
                    {
                    PhoneApplicationService.Current.State[Constant.OPERATOR_RECHARGE_FLAG]=flag;
                    NavigationService.Navigate(new Uri("/Views/PlansActivity.xaml?code=" + number.area + "&op_id=" + oper.p_id + "&cir_name=" + number.area_name + "&op_name="+oper.name, UriKind.RelativeOrAbsolute));
                    }
                else
                    {
                    if(number==null)
                        PhoneApplicationService.Current.State[Constant.OPERATOR_RECHARGE_FLAG]=flag;
                    NavigationService.Navigate(new Uri("/Views/SelectCircle.xaml?code=&op_id=" + oper.p_id + "&cir_name=&op_name="+oper.name, UriKind.RelativeOrAbsolute));
                    }
                }
            else
                {
                NavigationService.Navigate(new Uri("/DthPlans.xaml?op_id=" + oper.p_id, UriKind.Relative));
                }
            //NavigationService.Navigate(new Uri("/Views/PlansActivity.xaml?code=MU&op_id=4&cir_name=MU&op_name=AAA", UriKind.RelativeOrAbsolute));
                //StartTransition ();
               
            }


        void toggle_Unchecked(object sender, RoutedEventArgs e)
            {
            this.AdsToggleSwitch.Content = "Special";
            isStv="1";
            this.AdsToggleSwitch.SwitchForeground = new SolidColorBrush(Colors.Red);
            }

        void toggle_Checked(object sender, RoutedEventArgs e)
            {
            this.AdsToggleSwitch.Content = "Normal";
            isStv="0";
            this.AdsToggleSwitch.SwitchForeground = new SolidColorBrush(Colors.Green);
            }


        private void home_Click(object sender, EventArgs e)
            {
            NavigationService.Navigate(new Uri("/HomePage.xaml?", UriKind.RelativeOrAbsolute));
            }

        private void recharges_Click(object sender, EventArgs e)
            {
            NavigationService.Navigate(new Uri("/Views/RechargeMain.xaml?", UriKind.RelativeOrAbsolute));
            }

        private void gift_Click(object sender, EventArgs e)
            {
            NavigationService.Navigate(new Uri("/GiftMainPivot.xaml?", UriKind.RelativeOrAbsolute));
            }

        private void money_Click(object sender, EventArgs e)
            {
            NavigationService.Navigate(new Uri("/Views/TopupWalletLayout.xaml?", UriKind.RelativeOrAbsolute));
            }

        private void mainGiftPivot_Loaded(object sender, RoutedEventArgs e)
            {

            }

        private void mainRechargePivot_Loaded(object sender, RoutedEventArgs e)
            {
            switch(mainRechargePivot.SelectedIndex)
                {
                case 0: LoadMissCallData();
                    break;
                case 1:

                    flag="1";
                    break;
                case 2: flag="2";
                    break;
                case 3:
                    flag="3";
                    break;

                }
            }

        protected override void OnNavigatedTo(NavigationEventArgs e)
            {
             if(PhoneApplicationService.Current.State.ContainsKey("operatorData"))
                {
                oper= (OperatorDB)PhoneApplicationService.Current.State["operatorData"];
            setOperator(oper);
                 }
             if(PhoneApplicationService.Current.State.ContainsKey("dthAmt"))
                 {
                 string dthAmt=PhoneApplicationService.Current.State["dthAmt"].ToString();
                 if(flag.Equals("2"))
                     {
                     textDthaAmount.Text=dthAmt;
                     PhoneApplicationService.Current.State["dthAmt"]="";
                     }
                 }

             if(PhoneApplicationService.Current.State.ContainsKey("recAmt"))
                 {
                 string recAmt=PhoneApplicationService.Current.State["recAmt"].ToString();
                 if(string.IsNullOrEmpty(recAmt))
                     {
                     }
                 else
                     {
                     if(flag.Equals("1"))
                         {
                         textAmount.Text=recAmt;
                         }
                     else if(flag.Equals("3"))
                         {
                         textAmount.Text=recAmt;
                         }
                     
                     }

                 PhoneApplicationService.Current.State["recAmt"]="";
                 }
           

            }

        private void radioButtons_CheckedChanged(object sender, EventArgs e)
            {


            RadioButton radioButton = sender as RadioButton;
            OperatorHelper operatorHelper=new OperatorHelper();
            if(prepaidRadioButton.IsChecked==true)
                {
                flag="1";

                }
            else if(postpaidRadioButton.IsChecked==true)
                {
                flag="4";

                }
            if(oper!=null)
                {
                OperatorDB opDb1= operatorHelper.ReadOperatorDB(flag, oper.code);
                setOperator(opDb1);
                }
        
            if(textMobileNumber.Text.StartsWith("7")||textMobileNumber.Text.StartsWith("8")||textMobileNumber.Text.StartsWith("9"))
                {
              
                if(!string.IsNullOrEmpty(textMobileNumber.Text))
                    {

                    NumberCircleHelper numberHelper=new NumberCircleHelper();
                    NumberAndCircle number= numberHelper.ReadNumberAndCircleByFlag(textMobileNumber.Text);
                   
                    OperatorDB opDb= operatorHelper.ReadOperatorDB(flag, number.numberOperator);
                    setOperator(opDb);
                    }
                }
            else
                {
                if(!string.IsNullOrEmpty(textMobileNumber.Text))
                    {
                    MessageBox.Show("Enter correct number");
                    }
                }
            }

        private void setOperator(OperatorDB operDb)
            {
            if(operDb!=null)
                {
                oper=operDb;
                if(operDb.flag.Equals("1")||operDb.flag.Equals("4"))
                    {
                    textMobOperator.Text=operDb.name;
                    textMobOperator.Opacity=1.0;
                    if(operDb.flag.Equals("4"))
                        {
                        textPlans.Visibility=Visibility.Collapsed;
                        }
                    else
                        {
                        textPlans.Visibility=Visibility.Visible;
                        }

                    if(operDb.stv.Equals("1"))
                        {
                        AdsToggleSwitch.Visibility=Visibility.Visible;
                        }
                    else
                        {
                        AdsToggleSwitch.Visibility=Visibility.Collapsed;
                        }




                    }
                else if(operDb.flag.Equals("2"))
                    {
                    textDthOperator.Text=operDb.name;
                    textDthOperator.Opacity=1.0;
                    textDthPlans.Visibility=Visibility.Visible;
                    }
                else if(operDb.flag.Equals("3"))
                    {
                    textDataOperator.Text=operDb.name;
                    textDataOperator.Opacity=1.0;
                    }
                }
               
                }
                        
            


        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
            {
            //Do your work here
            ContentOverlay.Visibility=Visibility.Collapsed;
            if(popup!=null)
            popup.IsOpen = false;

            PhoneApplicationService.Current.State.Remove("operatorData");
            base.OnBackKeyPress(e);
            }

        private async void LoadMissCallData()
            {


            await Task.Run(() => getQuickPayTask());


            MisscallDataHelper misscallHelper=new MisscallDataHelper();
            ObservableCollection<QuickPayAllDataTable> quickPayList=misscallHelper.ReadQuickPayAllDataTableForRecharge();
            mytransactionPanel.Children.Clear();
            int totalTransaction = quickPayList.Count;
            //JObject jObj = (JObject)myLastArray[j - 1];
            int noOfRows = 0;
            if((totalTransaction % 2) == 0)
                {
                noOfRows = totalTransaction / 2;
                }
            else
                {
                noOfRows = totalTransaction / 2;
                noOfRows = noOfRows + 1;
                }
            int j = 0;
            for(var rows = 0;rows < noOfRows;rows++)
                {
                StackPanel panel = new StackPanel();
                panel.Orientation = System.Windows.Controls.Orientation.Horizontal;
                int i;
                for(i = 0;i < 2;i++)
                    {
                    j = j + 1;
                    if(j <= totalTransaction)
                        {
                        MyOldTransactionControl MyOldTransactionControl = new MyOldTransactionControl();
                        //JObject jObj = quickList[j - 1];
                        if(string.IsNullOrEmpty(quickPayList[j-1].product.ToString())){
                            }else{
                        MyOldTransactionControl.imageOperator.Source = OperatorConstant.getOperatorResource(Convert.ToInt16(quickPayList[j-1].product));
                            }
                       
                        MyOldTransactionControl.textPopupNumber.Text = quickPayList[j-1].number;
                        string delegateFlag = quickPayList[j - 1].delegate_flag;
                        string missed_number = quickPayList[j-1].missed_number;
                        string flag = quickPayList[j-1].flag;
                        if(flag.Equals("4"))
                            {
                            DateTime time = DateTime.Parse(quickPayList[j-1].datetime);
                            string sampleTime = time.ToString("dd-MMM", CultureInfo.InvariantCulture);
                            MyOldTransactionControl.textPopupAmount.Text=sampleTime;// quickPayList[j-1].datetime;
                            }
                        else
                            {
                            MyOldTransactionControl.textPopupAmount.Text = "Rs. " + quickPayList[j-1].amount;
                            }
                        MyOldTransactionControl.Tag = j;

                      

                        MyOldTransactionControl.Tap += (s, e) =>
                        {
                           // MessageBox.Show (MyOldTransactionControl.Tag.ToString ());
                            string recFlag = quickPayList[Convert.ToInt16(MyOldTransactionControl.Tag.ToString())-1].flag;
                            if(recFlag.Equals("1")||recFlag.Equals("2")||recFlag.Equals("3"))
                                {

                                Popup popup = new Popup();
                                QuickPayRechargePrePaidControl recControl = new QuickPayRechargePrePaidControl();
                                ContentOverlay.Visibility = Visibility.Visible;
                                popup.VerticalOffset = 200;
                                popup.IsOpen = true;
                                popup.Child = recControl;

                                    NumberCircleHelper numberHelper=new NumberCircleHelper();
                                    number= numberHelper.ReadNumberAndCircleByFlag(quickPayList[Convert.ToInt16(MyOldTransactionControl.Tag.ToString())-1].number);
                                    if(number!=null)
                                        {
                                        OperatorHelper operatorHelper=new OperatorHelper();
                                        OperatorDB opDb= operatorHelper.ReadOperatorDB(recFlag, number.numberOperator);
                                        if(opDb!=null)
                                            {
                                          //  setOperator(opDb);
                                            oper=opDb;
                                            }
                                        }
                                    System.Diagnostics.Debug.WriteLine("ok");
                                    


                                    


                                bool isViaWallet = false;
                                recControl.imgClose.Tap += (a, b) =>
                                {
                                    popup.IsOpen = false;
                                    ContentOverlay.Visibility = Visibility.Collapsed;
                                };
                                recControl.textMessage.Text = "Recharge the transaction of Rs. " + quickPayList[Convert.ToInt16(MyOldTransactionControl.Tag.ToString())-1].amount + " on " + quickPayList[Convert.ToInt16(MyOldTransactionControl.Tag.ToString())-1].number + ".";
                                recControl.postPaidPanel.Visibility=Visibility.Collapsed;

                                int balanceWallet=Constant.getbalanceInInt();
                                if(balanceWallet<=0)
                                    {
                                    recControl.Chkcategories.Visibility=Visibility.Collapsed;
                                    recControl.Chkcategories.IsChecked=false;
                                    isViaWallet=false;
                                    }
                                else
                                    {
                                    recControl.Chkcategories.Visibility=Visibility.Visible;
                                    recControl.Chkcategories.IsChecked=true;
                                    isViaWallet=true;
                                    }

                                recControl.buttonImageOk.Click += (ss, args) =>
                                {
                                    ContentOverlay.Visibility=Visibility.Collapsed;
                                    popup.IsOpen = false;

                                    if(recControl.Chkcategories.IsChecked==true)
                                        {
                                        isViaWallet = true;
                                        }
                                    else
                                        {
                                        isViaWallet = false;
                                        }

                                    if(isViaWallet)
                                        {
                                        rechargeMobile(Convert.ToInt16(quickPayList[Convert.ToInt16(MyOldTransactionControl.Tag.ToString())-1].amount), quickPayList[Convert.ToInt16(MyOldTransactionControl.Tag.ToString())-1].number, "wallet");
                                        }
                                    else
                                        {
                                        rechargeMobile(Convert.ToInt16(quickPayList[Convert.ToInt16(MyOldTransactionControl.Tag.ToString())-1].amount), quickPayList[Convert.ToInt16(MyOldTransactionControl.Tag.ToString())-1].number, "online");

                                        }
                                };

                                recControl.buttonImageCancel.Click += (ss, args) =>
                                {
                                    ContentOverlay.Visibility=Visibility.Collapsed;
                                    popup.IsOpen = false;
                                };


                                }
                            else
                                {

                                Popup popup = new Popup();
                                QuickPayRechargePrePaidControl recControl = new QuickPayRechargePrePaidControl();
                                ContentOverlay.Visibility = Visibility.Visible;
                                popup.VerticalOffset = 200;
                                popup.IsOpen = true;
                                popup.Child = recControl;

                                isQuickPay=true;
                                NumberCircleHelper numberHelper=new NumberCircleHelper();
                                number= numberHelper.ReadNumberAndCircleByFlag(quickPayList[Convert.ToInt16(MyOldTransactionControl.Tag.ToString())-1].number);
                                if(number!=null)
                                    {
                                    OperatorHelper operatorHelper=new OperatorHelper();
                                    OperatorDB opDb= operatorHelper.ReadOperatorDB(recFlag, number.numberOperator);
                                    if(opDb!=null)
                                        {
                                        //  setOperator(opDb);
                                        oper=opDb;
                                        }
                                    }

                                quickTextBox=recControl.textAmount;
                                recControl.textAmount.TextChanged+=(a, b) =>
                                {
                                CalculateTotalAmount(recControl.textAmount, recControl.textService);
                               
                                };
                                bool isViaWallet = false;
                                recControl.imgClose.Tap += (a, b) =>
                                {
                                    popup.IsOpen = false;
                                    ContentOverlay.Visibility = Visibility.Collapsed;
                                };
                                recControl.textMessage.Text = "Recharge the transaction of Rs. " + quickPayList[Convert.ToInt16(MyOldTransactionControl.Tag.ToString())-1].amount + " on " + quickPayList[Convert.ToInt16(MyOldTransactionControl.Tag.ToString())-1].number + ".";
                                recControl.postPaidPanel.Visibility=Visibility.Visible;

                                int balanceWallet=Constant.getbalanceInInt();
                                if(balanceWallet<=0)
                                    {
                                    recControl.Chkcategories.Visibility=Visibility.Collapsed;
                                    recControl.Chkcategories.IsChecked=false;
                                    isViaWallet=false;
                                    }
                                else
                                    {
                                    recControl.Chkcategories.Visibility=Visibility.Visible;
                                    recControl.Chkcategories.IsChecked=true;
                                    isViaWallet=true;
                                    }

                                recControl.buttonImageOk.Click += (ss, args) =>
                                {
                                    ContentOverlay.Visibility=Visibility.Collapsed;
                                    popup.IsOpen = false;

                                    if(recControl.Chkcategories.IsChecked==true)
                                        {
                                        isViaWallet = true;
                                        }
                                    else
                                        {
                                        isViaWallet = false;
                                        }

                                    if(isViaWallet)
                                        {
                                        rechargeMobile(Convert.ToInt16(quickPayList[Convert.ToInt16(MyOldTransactionControl.Tag.ToString())-1].amount), quickPayList[Convert.ToInt16(MyOldTransactionControl.Tag.ToString())-1].number, "wallet");
                                        }
                                    else
                                        {
                                        rechargeMobile(Convert.ToInt16(quickPayList[Convert.ToInt16(MyOldTransactionControl.Tag.ToString())-1].amount), quickPayList[Convert.ToInt16(MyOldTransactionControl.Tag.ToString())-1].number, "online");
                                       
                                        }
                                };

                                recControl.buttonImageCancel.Click += (ss, args) =>
                                {
                                    ContentOverlay.Visibility=Visibility.Collapsed;
                                    popup.IsOpen = false;
                                };


                                }
                        };
                       /* if(flag.Equals("2"))
                            {

                            }
                        else
                            {
                            panel.Children.Add(MyOldTransactionControl);
                            }
                        */

                        panel.Children.Add(MyOldTransactionControl);
                        }
                    else
                        {
                        break;
                        }
                    }
                mytransactionPanel.Children.Add(panel);

                }
            }

        private void buttonImage_Click_Mobile(object sender, RoutedEventArgs e)
            {
            if(textMobileNumber.Text.StartsWith("7")||textMobileNumber.Text.StartsWith("8")||textMobileNumber.Text.StartsWith("9"))
                {
                if(flag.Equals("1")||flag.Equals("3")||flag.Equals("4"))
                    {
                    if(string.IsNullOrEmpty(textMobileNumber.Text))
                        {
                        MessageBox.Show("Please enter Mobile number.");
                        return;
                        }
                    if(string.IsNullOrEmpty(textAmount.Text))
                        {
                        MessageBox.Show("Please enter amount.");
                        return;
                        }
                    if(oper!=null)
                        {

                        }
                    else
                        {
                        MessageBox.Show("Please select operator.");
                        return;
                        }


                    if(textMobileNumber.Text.Length != 0)
                        {
                        if(textMobileNumber.Text.Length ==10)
                            {
                            if(textAmount.Text.Length != 0)
                                {
                                int recAmount = Convert.ToInt32(textAmount.Text);
                                if(recAmount >= Convert.ToInt32(oper.min))
                                    {
                                    if(recAmount <= Convert.ToInt32(oper.max))
                                        {

                                        // rechargeMobile(recAmount, mobileNumber.Text);
                                        /* if(flag.Equals("4"))
                                             {
                                             //rechargeMobile(recAmount, textMobileNumber.Text,"wallet");
                                             showConfirmationDialog(recAmount, textMobileNumber.Text);
                                             }
                                         else
                                             {

                                             showConfirmationDialog(recAmount, textMobileNumber.Text);
                                             }*/

                                        showConfirmationDialog(recAmount, textMobileNumber.Text);
                                        }
                                    else
                                        {
                                        MessageBox.Show("maximum amount should be " + oper.max);
                                        }
                                    }
                                else
                                    {
                                    MessageBox.Show("minimum amount should be " + oper.min);
                                    }
                                }
                            else
                                {
                                MessageBox.Show("Please enter amount");
                                }
                            }
                        else
                            {
                            MessageBox.Show("Please enter correct number");
                            }
                        }
                    else
                        {
                        MessageBox.Show("Please enter correct number");
                        }


                    }
                }
            else
                {
                MessageBox.Show("Please enter correct number");
                }
          
            }



        private async void rechargeMobile(int amount, String userMobileNumber, String paymentopt)
            {
            System.Diagnostics.Debug.WriteLine(DateTime.Now + " start  time");
            ProgressBarManager.Show(1, "Please Wait", false, 10);
            var values = new List<KeyValuePair<string, string>>();

            values.Add(new KeyValuePair<string, string>("paymentopt", paymentopt));
            //if(amount<Constant.getbalanceInInt())
            //    {
            //    values.Add(new KeyValuePair<string, string>("paymentopt", "wallet"));
            //    }
            //else
            //    {
            //    values.Add(new KeyValuePair<string, string>("paymentopt", "online"));
            //    }
            values.Add(new KeyValuePair<string, string>("payment", "1"));
            values.Add(new KeyValuePair<string, string>("api", "true"));
            values.Add(new KeyValuePair<string, string>("name", ""));
            values.Add(new KeyValuePair<string, string>("operator", oper.id));
            values.Add(new KeyValuePair<string, string>("flag", oper.flag));
            values.Add(new KeyValuePair<string, string>("recharge_flag", "1"));
            values.Add(new KeyValuePair<string, string>("partial", "1"));
            values.Add(new KeyValuePair<string, string>("api_version", Constant.API_VERSION));

            values.Add(new KeyValuePair<string, string>("longitude", Constant.LoadPersistent<string>(Constant.SETTINGS_USER_LATTITUDE)));
            values.Add(new KeyValuePair<string, string>("latitude", Constant.LoadPersistent<string>(Constant.SETTINGS_USER_LONGITUDE)));
            values.Add(new KeyValuePair<string, string>("updatedTime", Constant.LoadPersistent<DateTime>(Constant.FREEBIE_LIST_UPDATE_TIME).ToString("yyyy-MM-dd HH:mm:ss")));


            if(oper.flag.Equals("1") || oper.flag.Equals("3"))
                {
                values.Add(new KeyValuePair<string, string>("actiontype", "recharge"));
                values.Add(new KeyValuePair<string, string>("amount", amount+""));
                values.Add(new KeyValuePair<string, string>("mobile_number", userMobileNumber));
                values.Add(new KeyValuePair<string, string>("stv", isStv));
                values.Add(new KeyValuePair<string, string>("recharge", "1"));
                }
            else if(oper.flag.Equals("2"))
                {
                values.Add(new KeyValuePair<string, string>("actiontype", "recharge"));
                values.Add(new KeyValuePair<string, string>("amount",Convert.ToString(amount)));
                values.Add(new KeyValuePair<string, string>("subscriber_id", userMobileNumber));
                values.Add(new KeyValuePair<string, string>("recharge", "1"));
                }
            else if(oper.flag.Equals("4"))
                {

                values.Add(new KeyValuePair<string, string>("actiontype", "billpayment"));
                if(isQuickPay==false)
                    {
                    values.Add(new KeyValuePair<string, string>("amount", CalculateTotalAmount(textAmount, textInfo)));
                    values.Add(new KeyValuePair<string, string>("base_amount", textAmount.Text));
                    }
                else
                    {
                    values.Add(new KeyValuePair<string, string>("amount", CalculateTotalAmount(quickTextBox, textInfo)));
                    values.Add(new KeyValuePair<string, string>("base_amount", quickTextBox.Text));
                    }
                values.Add(new KeyValuePair<string, string>("payment_flag", "1"));
                values.Add(new KeyValuePair<string, string>("billpayment", "1"));
                
                values.Add(new KeyValuePair<string, string>("service_charge", "" + charges_slab));
                values.Add(new KeyValuePair<string, string>("service_tax", "" + service_tax_percent));
                values.Add(new KeyValuePair<string, string>("mobile_number", userMobileNumber));
                }

            try
                {
                FormUrlEncodedContent values1 = new FormUrlEncodedContent(values);
                Task<string> response = Constant.postParamsAndgetResponse(Constant.B2C_URL, values1);

                string result = await response;
                if(string.IsNullOrEmpty(result))
                    {
                    NavigationService.Navigate(new Uri("/Views/NoConnection.xaml?", UriKind.Relative));
                    }
                else
                    {
                    var dict = (JObject)JsonConvert.DeserializeObject(result);
                    string status = dict["status"].ToString();
                    System.Diagnostics.Debug.WriteLine(DateTime.Now+" End time");
                    //PhoneApplicationService.Current.State[dealData] = result;
                    PhoneApplicationService.Current.State[Constant.RECHARGE_OPERATOR] = oper.id;
                    if(paymentopt=="wallet"/*amount<Constant.getbalanceInInt()*/)
                
                        {
                        if(status.Equals("success"))
                            {

                            var description = dict["description"];
                           
                            bool online_form=(Boolean)description["online_form"];
                            if(online_form==false)
                                {
                                Constant.setbalance(description["closing_balance"].ToString());
                                Constant.setLoyalityPoints(description["loyalty_balance"].ToString());
                                PhoneApplicationService.Current.State["rechargeAmount"] = amount;
                                Constant.ShowSuccessPopUp(ContentOverlay, oper.flag.ToString(), userMobileNumber, amount.ToString(), oper.p_id.ToString(), true, "", description["loyalty_points"].ToString());
                                }
                            else if(online_form==true)
                                {
                               // var description = dict["description"];
                                string form_content = description["form_content"].ToString();
                                PhoneApplicationService.Current.State[Constant.PAYMENT_GATEWAY_HTML] = form_content;
                                PhoneApplicationService.Current.State[Constant.RECHARGE_AMOUNT] = amount;
                                PhoneApplicationService.Current.State[Constant.OPERATOR_RECHARGE_FLAG]=oper.flag;
                                PhoneApplicationService.Current.State["txn_id"]=description["txnid"].ToString();
                              
                                NavigationService.Navigate(new Uri("/Views/PaymentGatewayLayout.xaml?page=HomePage", UriKind.RelativeOrAbsolute));
                                }
                            
                        
                            }
                        else
                            {
                           
                            string errCode = dict["errCode"].ToString();
                            if(errCode.Equals("201"))
                                {
                                (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri("/Login.xaml?no-cache=" + Guid.NewGuid(), UriKind.Relative));

                                (Application.Current.RootVisual as PhoneApplicationFrame).RemoveBackEntry();

                              
                                }
                            else
                                {
                                Constant.ShowSuccessPopUp(ContentOverlay, oper.flag.ToString(), userMobileNumber, amount.ToString(), oper.p_id.ToString(), false, dict["description"].ToString(),"");
                                }

                            //Constant.checkForErrorCode (result);
                            }
                        }
                    else
                        {
                        if(status.Equals("success"))
                            {
                            var description = dict["description"];
                            string form_content = description["form_content"].ToString();
                            PhoneApplicationService.Current.State[Constant.OPERATOR_RECHARGE_FLAG]=oper.flag;
                            PhoneApplicationService.Current.State[Constant.PAYMENT_GATEWAY_HTML] = form_content;
                            PhoneApplicationService.Current.State[Constant.RECHARGE_AMOUNT] = amount;
                            string updateDealData = dict["dealDetails"].ToString();
                            PhoneApplicationService.Current.State["txn_id"]=description["txnid"].ToString();
                            NavigationService.Navigate(new Uri("/Views/PaymentGatewayLayout.xaml?page=HomePage", UriKind.RelativeOrAbsolute));
                            
                            }
                        else
                            {
                           
                            string errCode = dict["errCode"].ToString();
                            if(errCode.Equals("201"))
                                {
                                (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri("/Login.xaml?no-cache=" + Guid.NewGuid(), UriKind.Relative));

                                (Application.Current.RootVisual as PhoneApplicationFrame).RemoveBackEntry();

                                //return responseString.ToString();	  
                                }
                            else
                                {
                                //Constant.ShowSuccessPopUp(ContentOverlay, rechargeFlag, number, amount, op_id, pay1_pro_id, false);
                                }

                            //Constant.checkForErrorCode (result);
                            }
                        }

                    }

                }
            catch(WebException we)
                {
                }
            catch(Exception e)
                {
                }
            //Constant.SetProgressIndicator(false);
            ProgressBarManager.Hide(1);
            }

        private void buttonImage_Click_Dth(object sender, RoutedEventArgs e)
            {
            if(flag.Equals("2"))
                {
                if(string.IsNullOrEmpty(DthcardNumber.Text))
                    {
                    MessageBox.Show("Please enter correct DTH Id.");
                    return;
                    }
                if(string.IsNullOrEmpty(textDthaAmount.Text))
                    {
                    MessageBox.Show("Please enter amount.");
                    return;
                    }
                if(oper!=null)
                    {

                    }
                else
                    {
                    MessageBox.Show("Please select operator.");
                    return;
                    }


                if(DthcardNumber.Text.Length != 0)
                    {
                   
                        if(textDthaAmount.Text.Length != 0)
                            {
                            int recAmount = Convert.ToInt32(textDthaAmount.Text);
                            if(recAmount >= Convert.ToInt32(oper.min))
                                {
                                if(recAmount <= Convert.ToInt32(oper.max))
                                    {
                                    if(Convert.ToInt32(oper.length)==DthcardNumber.Text.Length)
                                        {
                                        // rechargeMobile(recAmount, DthcardNumber.Text,"wallet");
                                        // showConfirmationDialog(recAmount, DthcardNumber.Text);
                                        if(checkPrefix(DthcardNumber.Text))
                                            {
                                            //rechargeMobile(recAmount, datacardNumber.Text,"wallet");
                                            showConfirmationDialog(recAmount, DthcardNumber.Text);

                                            }
                                       
                                        }
                                    else
                                        {
                                        MessageBox.Show(DthOperatorDetails(oper.p_id));
                                        }

                                    }
                                else
                                    {
                                    MessageBox.Show("maximum amount should be " + oper.max);
                                    }
                                }
                            else
                                {
                                MessageBox.Show("minimum amount should be " + oper.min);
                                }
                            }
                        else
                            {
                            MessageBox.Show("Please enter amount");
                            }
                        
                    }
                else
                    {
                    }


                }
            }

        private void buttonImage_Click_Data(object sender, RoutedEventArgs e)
            {
            if(datacardNumber.Text.StartsWith("8")||datacardNumber.Text.StartsWith("9")||datacardNumber.Text.StartsWith("7"))
                {
            if(flag.Equals("1")||flag.Equals("3"))
                {
                if(string.IsNullOrEmpty(datacardNumber.Text))
                    {
                    MessageBox.Show("Please enter Datacard number.");
                    return;
                    }
                if(string.IsNullOrEmpty(textDataaAmount.Text))
                    {
                    MessageBox.Show("Please enter amount.");
                    return;
                    }
                if(oper!=null)
                    {

                    }
                else
                    {
                    MessageBox.Show("Please select operator.");
                    return;
                    }


                if(datacardNumber.Text.Length != 0)
                    {
                    if(datacardNumber.Text.Length ==10)
                        {
                        if(textDataaAmount.Text.Length != 0)
                            {
                            int recAmount = Convert.ToInt32(textDataaAmount.Text);
                            if(recAmount >= Convert.ToInt32(oper.min))
                                {
                                if(recAmount <= Convert.ToInt32(oper.max))
                                    {

                                    showConfirmationDialog(recAmount, datacardNumber.Text);


                                    }
                                else
                                    {
                                    MessageBox.Show("maximum amount should be " + oper.max);
                                    }
                                }
                            else
                                {
                                MessageBox.Show("Minimum amount should be " + oper.min);
                                }
                            }
                        else
                            {
                            MessageBox.Show("Please enter amount");
                            }
                        }
                    else
                        {
                        MessageBox.Show("Please enter correct datacard number");
                        }
                    }
                else
                    {
                    MessageBox.Show("Please enter correct datacard number");
                    }


                }
            else
                {
                if(flag.Equals("4"))
                    {
                    }
                }
                }
            else
                {
                MessageBox.Show("Please enter correct datacard number.");
                }
            
            }



        private bool checkPrefix(string userNumber)
            {
            bool checkNumber = false;

            if(flag.Equals("2"))
                {

                string[] pipe_seperator = oper.prefix.Split(',');
                foreach(String s1 in pipe_seperator)
                    {
                    if(userNumber.StartsWith(s1))
                        {
                        checkNumber = true;
                        break;

                        }
                    else
                        {
                        checkNumber = false;
                        MessageBox.Show(DthOperatorDetails(oper.p_id));
                        }

                    }

                }
            else
                {
                checkNumber = true;
                //break;
                }



            return checkNumber;
            }


        string DthOperatorDetails(string p_Id)
            {
            int pay1Id = Convert.ToInt16(p_Id);
            string operatorMsg = "";
            switch(pay1Id)
                {
                case 16:
                        {
                        operatorMsg = "Customer ID " + "starts with " + oper.prefix + " and is " + oper.length + " digits long.";
                        }
                    break;
                case 17:
                        {
                        operatorMsg = "Smart card number " + "starts with " + oper.prefix + " and is " + oper.length + " digits long.";
                        }
                    break;
                case 18:
                        {
                        operatorMsg = "Your viewing card (VC) number " + "starts with 0 and is " + oper.length + " digits long.";
                        }
                    break;
                case 19:
                        {
                        operatorMsg = "Smart card number " + "starts with " + oper.prefix + " and is " + oper.length + " digits long.";
                        }
                    break;
                case 20:
                        {

                        operatorMsg = "Subscriber ID " + "starts with " + oper.prefix + " and is " + oper.length + " digits long.";
                        }
                    break;
                case 21:
                        {
                        operatorMsg = "SMS ID to 921-201-2299 from your registered mobile to retrieve your Customer ID.";
                        }
                    break;

                default:
                    break;
                }

            return operatorMsg;
            }


        String charges_slab = null;
        string prefix, length;
        double service_charge_amount = 0, service_charge_percent = 0, service_tax_percent = 0, dblMin = 0, dblMax = 0, dblAmount = 0;

        double tot;
        private string CalculateTotalAmount(TextBox textCalAmount, TextBlock textService)
            {
            string amount=textCalAmount.Text;
            int slab_amount = 0;
            try
                {
                dblAmount = Convert.ToDouble(amount);


                charges_slab = oper.charges_slab;
                service_charge_amount = Convert.ToDouble(oper.service_charge_amount);
                service_charge_percent = Convert.ToDouble(oper.service_charge_percent);
                service_tax_percent = Convert.ToDouble(oper.service_tax_percent);
                dblMin = Convert.ToDouble(oper.min);
                dblMax = Convert.ToDouble(oper.max);




                if(string.IsNullOrEmpty(charges_slab) == true)
                    {
                    dblAmount = (dblAmount + (dblAmount * service_charge_percent));
                    dblAmount = (dblAmount + (dblAmount * service_tax_percent));
                    }
                else
                    {
                    String[] comma_seperator = charges_slab.Split(',');

                    foreach(String s in comma_seperator)
                        {
                        String[] colon_sperator = s.Split(':');
                        if(dblAmount >= Convert.ToDouble(colon_sperator[0]))
                            {
                            slab_amount = Convert.ToInt32(colon_sperator[1]);
                            }
                        }
                    }
                 tot = dblAmount + slab_amount;
                textService.Text = "( " + tot + " = " + amount + " + " + slab_amount + " service charge inclusive of all taxes.)";
                if(amount.Length == 0)
                    {
                    textService.Text = "";
                    }

                }
            catch(SQLiteException se)
                {
                string see = se.Message;
                }
            return Convert.ToString(dblAmount + slab_amount);




            }

        public void showConfirmationDialog(int recAmount, string userNumber)
            {
            bool isViaWallet = false;
            ContentOverlay.Visibility=Visibility.Visible;
            popup = new Popup();
            popup.Height = 300;
            popup.Width = 400;
            popup.VerticalOffset=100;
            popup.HorizontalOffset=20;
            RechargePopup control = new RechargePopup();
            popup.Child = control;
            popup.IsOpen = true;
            control.textPopupNumber.Text=userNumber.ToString();
            

            if(flag.Equals("1"))
                {
                control.textPopupAmount.Text="Rs. "+recAmount.ToString();
                }else  if(flag.Equals("4"))
                {
                control.textPopupAmount.Text="Rs. "+tot.ToString();
                    }
            control.imageOperator.Source=OperatorConstant.getOperatorResource(Convert.ToInt16(oper.p_id));
           

            int balanceWallet=Constant.getbalanceInInt();
            if(balanceWallet<=0){
                control.Chkcategories.Visibility=Visibility.Collapsed;
                control.Chkcategories.IsChecked=false;
                isViaWallet=false;
                }else{
                control.Chkcategories.Visibility=Visibility.Visible;
                control.Chkcategories.IsChecked=true;
                isViaWallet=true;
                }

            control.buttonImageOk.Click += (s, args) =>
            {
                ContentOverlay.Visibility=Visibility.Collapsed;
                popup.IsOpen = false;

                if(control.Chkcategories.IsChecked==true)
                    {
                    isViaWallet = true;
                    }
                else
                    {
                    isViaWallet = false;
                    }

                if(isViaWallet)
                    {
                    rechargeMobile(recAmount, userNumber, "wallet");
                    }
                else
                    {
                    rechargeMobile(recAmount, userNumber, "online");
                    }
            };

            control.buttonImageCancel.Click += (s, args) =>
            {
                ContentOverlay.Visibility=Visibility.Collapsed;
                popup.IsOpen = false;
            };
            }

        private void mobileNumber_TextChanged(object sender, TextChangedEventArgs e)
            {
            setOperatorInText();
            }

        private void setOperatorInText()
            {
            try
                {
                if(!flag.Equals("3"))
                    {
                    if(textMobileNumber.Text.Length>=4)
                        {
                        NumberCircleHelper numberHelper=new NumberCircleHelper();
                        number= numberHelper.ReadNumberAndCircleByFlag(textMobileNumber.Text);
                        if(number!=null)
                            {
                            OperatorHelper operatorHelper=new OperatorHelper();
                            OperatorDB opDb= operatorHelper.ReadOperatorDB(flag, number.numberOperator);
                            if(opDb!=null)
                                {
                                setOperator(opDb);
                                }
                            }
                        System.Diagnostics.Debug.WriteLine("ok");
                        }
                    else
                        {
                        }
                    }
                else if(flag.Equals("3"))
                    {
                    if(datacardNumber.Text.Length>=4)
                        {
                        NumberCircleHelper numberHelper=new NumberCircleHelper();
                        NumberAndCircle number= numberHelper.ReadNumberAndCircleByFlag(datacardNumber.Text);
                        if(number!=null)
                            {
                            OperatorHelper operatorHelper=new OperatorHelper();
                            OperatorDB opDb= operatorHelper.ReadOperatorDB(flag, number.numberOperator);
                            if(opDb!=null)
                                {
                                setOperator(opDb);
                                }
                            }
                        System.Diagnostics.Debug.WriteLine("ok");
                        }
                    else
                        {
                        }
                    }
                }
            catch(Exception exc)
                {
                }
            }

        private void textAmount_TextChanged(object sender, TextChangedEventArgs e)
            {
            if(flag.Equals("4"))
                {
                textInfo.Visibility=Visibility.Visible;
                if(textAmount.Text.Length>=1)
                    CalculateTotalAmount(textAmount,textInfo);
                }
            else
                {
                textInfo.Visibility=Visibility.Collapsed;
                }

            }

        private void imageContact_Tap(object sender, System.Windows.Input.GestureEventArgs e)
            {

            Dispatcher.BeginInvoke(delegate()
            {
            try
                {
                phoneNumberChooserTask = new PhoneNumberChooserTask();
                phoneNumberChooserTask.Completed += new EventHandler<PhoneNumberResult>(phoneNumberChooserTask_Completed);
                phoneNumberChooserTask.Show();
                }
            catch(Exception exc)
                {
                }
            });
            }
        string mobileNo;
        private void phoneNumberChooserTask_Completed(object sender, PhoneNumberResult e)
            {
            try
                {
                if(e.TaskResult == TaskResult.OK)
                    {

                        try
                            {
                            mobileNo = Constant.RemoveSpecialCharacters(e.PhoneNumber);
                            mobileNo = Constant.GetLast(mobileNo, 10);
                            string contactName = Constant.RemoveSpecialCharacters(e.DisplayName);

                            if(mobileNo.Length >= 10)
                                {
                                if(!flag.Equals("3"))
                                    {
                                    textMobileNumber.Text=mobileNo;
                                    }
                                else
                                    {
                                    datacardNumber.Text=mobileNo;
                                    }
                                setOperatorInText();
                                }

                            else
                                {
                                MessageBox.Show("Please select correct mobile number.");
                                }
                            }
                        catch(Exception er)
                            {
                            }
                      
                    }

                }
            catch(Exception exc)
                {
                }
            }
        
      

            //TiltEffect.SetIsTiltEnabled(checkBox, true);

            //CustomMessageBox messageBox = new CustomMessageBox()
            //{
            //    Caption = "CONFIRMATION",
            //    Message =
            //    "If you want to continue listen music while doing other stuff, please use Home key instead of Back key",
            //    Content = checkBox,
            //    LeftButtonContent = "Exit",
            //    RightButtonContent = "Cancel",
            //};

            //messageBox.Dismissed += (s1, e1) =>
            //{
            //    switch(e1.Result)
            //        {
            //        case CustomMessageBoxResult.LeftButton: //Exit
            //            return;// What to do here??
            //        case CustomMessageBoxResult.RightButton: //Cancel
            //        case CustomMessageBoxResult.None:
            //            break;
            //        default:
            //            break;
            //        }
            //};
            //messageBox.Show();
            //}



        private async void getQuickPayTask()
            {
            //ProgressBarManager.Show(40, "Please wait...", false, 10);
            var values = new List<KeyValuePair<string, string>>();
            values.Add(new KeyValuePair<string, string>("actiontype", "get_quickpaylist"));
            values.Add(new KeyValuePair<string, string>("api", "true"));
            values.Add(new KeyValuePair<string, string>("api_version", Constant.API_VERSION));
            FormUrlEncodedContent values1 = new FormUrlEncodedContent(values);
            Task<string> response = Constant.postParamsAndgetResponse(Constant.B2C_URL, values1);

            string result = await response;
            if(string.IsNullOrEmpty(result))
                {
                NavigationService.Navigate(new Uri("/Views/NoConnection.xaml?", UriKind.RelativeOrAbsolute));
                }
            else
                {
                var dict = (JObject)JsonConvert.DeserializeObject(result);
                string status = dict["status"].ToString();

                if(status.Equals("success"))
                    {

                    try
                        {

                        int ii = 0;
                        var description = dict["description"];
                        foreach(var item in description)
                            {
                            if(!string.IsNullOrEmpty(item["product"].ToString()) && !item["product"].ToString().Equals("0"))
                                {
                                QuickPayAllDataTable quickPayAllDataTable = new QuickPayAllDataTable();
                                quickPayAllDataTable.id = item["id"].ToString();
                                quickPayAllDataTable.missed_number = item["missed_number"].ToString();
                                quickPayAllDataTable.name = item["name"].ToString();
                                quickPayAllDataTable.number = item["number"].ToString();
                                quickPayAllDataTable.operator_code = item["operator_code"].ToString();
                                quickPayAllDataTable.operator_id = item["operator_id"].ToString();
                                quickPayAllDataTable.operator_name = item["operator_name"].ToString();
                                quickPayAllDataTable.product = item["product"].ToString();
                                quickPayAllDataTable.stv = item["stv"].ToString();
                                quickPayAllDataTable.transaction_flag = item["transaction_flag"].ToString();
                                quickPayAllDataTable.users_id = item["users_id"].ToString();
                                quickPayAllDataTable.amount = item["amount"].ToString();
                                quickPayAllDataTable.confirmation = item["confirmation"].ToString();
                                quickPayAllDataTable.datetime = item["datetime"].ToString();
                                quickPayAllDataTable.delegate_flag = item["delegate_flag"].ToString();
                                quickPayAllDataTable.flag = item["flag"].ToString();





                                try
                                    {
                                    MisscallDataHelper misscallHelper=new MisscallDataHelper();
                                    misscallHelper.DeleteMisscallById(item["id"].ToString());

                                    misscallHelper.Insert(quickPayAllDataTable);
                                    }
                                catch(SQLiteException swe)
                                    {
                                    }
                                catch(Exception es)
                                    {
                                    }
                                }
                            }
                        }
                    catch(SQLiteException see)
                        {
                        }

                    }

                else
                    {
                    Constant.checkForErrorCode(result);
                    }
                }
            Dispatcher.BeginInvoke(delegate()
            {
                //chaekcForMissCallData();
                //LoadMissCallData (dict["description"].ToString ());

            });
            // ProgressBarManager.Hide(40);
            }



        }
    }