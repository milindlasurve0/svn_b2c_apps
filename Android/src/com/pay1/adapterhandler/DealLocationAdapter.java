package com.pay1.adapterhandler;

import java.util.ArrayList;
import java.util.WeakHashMap;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.pay1.DealDetailsActivity;
import com.pay1.DealLocationActivity;
import com.pay1.DealLocatorActivity;
import com.pay1.R;

public class DealLocationAdapter extends BaseAdapter {

	private Context context;
	private ArrayList<WeakHashMap<String, String>> data;
	private Typeface Reguler, Narrow, Narrow_thin;

	public DealLocationAdapter(Context context,
			ArrayList<WeakHashMap<String, String>> data) {
		this.context = context;
		this.data = data;
		Reguler = Typeface.createFromAsset(context.getAssets(),
				"EncodeSansNormal-400-Regular.ttf");

		Narrow = Typeface.createFromAsset(context.getAssets(),
				"EncodeSansNarrow-500-Medium.ttf");

		Narrow_thin = Typeface.createFromAsset(context.getAssets(),
				"EncodeSansNarrow-100-Thin.ttf");
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder viewHolder;
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater
					.inflate(R.layout.deal_location_adapter, null);
			viewHolder = new ViewHolder();

			viewHolder.imageView_Locate = (ImageView) convertView
					.findViewById(R.id.imageView_Locate);
			viewHolder.imageView_Locate
					.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							Intent intent = new Intent(context,
									DealLocatorActivity.class);
							WeakHashMap<String, String> map = new WeakHashMap<String, String>(
									data.get(position));

							intent.putExtra("LATITUDE",
									map.get(DealLocationActivity.LATITUDE));
							intent.putExtra("LONGITUDE",
									map.get(DealLocationActivity.LONGITUDE));
							intent.putExtra("ADDRESS",
									map.get(DealLocationActivity.ADDRESS));
							intent.putExtra("CITY",
									map.get(DealLocationActivity.CITY));
							intent.putExtra("STATE",
									map.get(DealLocationActivity.STATE));
							context.startActivity(intent);
						}
					});
			viewHolder.textView_Address = (TextView) convertView
					.findViewById(R.id.textView_Address);
			viewHolder.textView_Address.setTypeface(Narrow);
			viewHolder.textView_City = (TextView) convertView
					.findViewById(R.id.textView_City);
			viewHolder.textView_City.setTypeface(Narrow_thin);
			viewHolder.textView_State = (TextView) convertView
					.findViewById(R.id.textView_State);
			viewHolder.textView_State.setTypeface(Narrow_thin);
			convertView.setTag(viewHolder);

		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		try {
			WeakHashMap<String, String> map = new WeakHashMap<String, String>(
					data.get(position));

			viewHolder.textView_Address.setText(map
					.get(DealDetailsActivity.ADDRESS));
			viewHolder.textView_City.setText(map.get(DealDetailsActivity.CITY));
			viewHolder.textView_State.setText(map
					.get(DealDetailsActivity.STATE));
		} catch (Exception e) {
		}
		return convertView;
	}

	private class ViewHolder {
		TextView textView_Address, textView_City, textView_State;
		ImageView imageView_Locate;
	}
}
