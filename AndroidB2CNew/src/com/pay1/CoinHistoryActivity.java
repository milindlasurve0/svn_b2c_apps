package com.pay1;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.larvalabs.svgandroid.SVGParser;
import com.pay1.adapterhandler.CoinAdapter;
import com.pay1.constants.Constants;
import com.pay1.customviews.MyProgressDialog;
import com.pay1.requesthandler.RequestClass;
import com.pay1.utilities.Utility;

public class CoinHistoryActivity extends Activity {

	private static final String SCREEN_LABEL = "Purchase History Screen";
	private EasyTracker easyTracker = null;
	public static final String TAG = "My Transactions";
	int page = 0;
	ArrayList<JSONObject> arrayList;
	ListView listViewCoinHistory;
	CoinAdapter coinAdapter;
	ImageView imageViewHistory;
	TextView textView_NoData, textView_Shop;
	private RelativeLayout back_layout;
	LinearLayout relativeLayout_EmptyView;
	private ImageView imageView_Back;
	private TextView textView_Title;

	@SuppressLint("NewApi")
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.coin_history);
		try {
			easyTracker = EasyTracker.getInstance(CoinHistoryActivity.this);
			easyTracker.set(Fields.SCREEN_NAME, SCREEN_LABEL);

			// timestamp = System.currentTimeMillis();

			// faqDataSource = new FAQDataSource(PurchaseHistoryActivity.this);

			imageView_Back = (ImageView) findViewById(R.id.imageView_Back);
			imageView_Back.setImageDrawable(SVGParser.getSVGFromResource(
					getResources(), R.raw.ic_back_old).createPictureDrawable());
			if (Build.VERSION.SDK_INT >= 11)
				imageView_Back.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
			
			
			back_layout=(RelativeLayout)findViewById(R.id.back_layout);
			back_layout.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onBackPressed();
				}
			});

			imageViewHistory=(ImageView)findViewById(R.id.imageViewHistory);
			imageViewHistory.setImageDrawable(SVGParser.getSVGFromResource(
					getResources(), R.raw.ic_gift_coin).createPictureDrawable());
			Typeface Reguler = Typeface.createFromAsset(getAssets(),
					"Roboto-Regular.ttf");
			textView_Title = (TextView) findViewById(R.id.textView_Title);
			textView_Title.setTypeface(Reguler);

			textView_NoData = (TextView) findViewById(R.id.textView_NoData);
			textView_NoData.setTypeface(Reguler);
			textView_Shop = (TextView) findViewById(R.id.textView_Shop);
			textView_Shop.setTypeface(Reguler);
			textView_Shop.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent intent = new Intent(CoinHistoryActivity.this,
							MainActivity.class);
					intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
							| Intent.FLAG_ACTIVITY_NEW_TASK);
					intent.putExtra("FROM_QUICKPAY", true);
					intent.putExtra("IS_GIFT", false);
					CoinHistoryActivity.this.startActivity(intent);
					CoinHistoryActivity.this.finish();
				}
			});

			arrayList = new ArrayList<JSONObject>();
			listViewCoinHistory = (ListView) findViewById(R.id.listViewCoinHistory);
			coinAdapter = new CoinAdapter(CoinHistoryActivity.this,
					arrayList);
			relativeLayout_EmptyView = (LinearLayout) findViewById(R.id.relativeLayout_EmptyView);
			// listViewCoinHistory.setEmptyView(relativeLayout_EmptyView);
			relativeLayout_EmptyView.setVisibility(View.VISIBLE);

			
			listViewCoinHistory.setAdapter(coinAdapter);

			/*try {

				if (Utility.getSharedPreferences(CoinHistoryActivity.this,
						Constants.SHAREDPREFERENCE_COIN_DATA) != null) {
					JSONObject jsonObject = new JSONObject(
							Utility.getSharedPreferences(
									CoinHistoryActivity.this,
									Constants.SHAREDPREFERENCE_COIN_DATA));
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {
						JSONArray description = jsonObject
								.getJSONArray("description");

						if (description.length() != 0) {
							arrayList.clear();
							int len = 0;
							for (int j = 0; j < description.length(); j++) {
								JSONObject jObject = description
										.getJSONObject(j);
								arrayList.add(jObject);
								len++;
							}
							
							relativeLayout_EmptyView.setVisibility(View.GONE);
							listViewCoinHistory.setVisibility(View.VISIBLE);
						}
					}
				}
				CoinHistoryActivity.this.runOnUiThread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						coinAdapter.notifyDataSetChanged();
					}
				});
			} catch (Exception e) {
				// TODO: handle exception
				// // // Log.e("Er ", e.getMessage());
			}
*/
		
				new PurchaseHistoryTask().execute();

		} catch (Exception e) {
		}
		overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}

	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView().build());
		// EasyTracker.getInstance(this).activityStart(this);
		easyTracker.send(MapBuilder.createAppView().build());
	}

	@Override
	public void onStop() {
		super.onStop();
		// EasyTracker.getInstance(this).activityStop(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
	}

	public class PurchaseHistoryTask extends AsyncTask<String, String, String>
			implements OnDismissListener {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String url = Constants.B2C_URL + "LoyaltyPointsHistory/?";
			String response = RequestClass.getInstance().readPay1B2CRequest(
					CoinHistoryActivity.this, url);
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			try {
				if (this.dialog.isShowing()) {
					this.dialog.dismiss();
				}
			} catch (Exception e) {
			}
			try {
				if (!result.startsWith("Error")) {
					/*String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");
*/
					JSONObject jsonObject = new JSONObject(result);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {

						
							try {

								Utility.setSharedPreferences(
										CoinHistoryActivity.this,
										Constants.SHAREDPREFERENCE_COIN_DATA,
										result);

							} catch (Exception e) {
								// TODO: handle exception
								// // // Log.e("Er ", e.getMessage());
							}
						
						JSONArray description = jsonObject
								.getJSONArray("description");

						if (description.length() > 0) {
						
								arrayList.clear();
							int len = 0;
							for (int j = 0; j < description.length(); j++) {
								JSONObject jObject = description
										.getJSONObject(j);
								arrayList.add(jObject);
								len++;
							}
							
							CoinHistoryActivity.this
									.runOnUiThread(new Runnable() {

										@Override
										public void run() {
											// TODO Auto-generated method stub
											coinAdapter
													.notifyDataSetChanged();
										}
									});
							relativeLayout_EmptyView.setVisibility(View.GONE);
							listViewCoinHistory.setVisibility(View.VISIBLE);
						} else {
							relativeLayout_EmptyView
									.setVisibility(View.VISIBLE);
							listViewCoinHistory.setVisibility(View.GONE);
						}

						Utility.setRefreshRequired(
								CoinHistoryActivity.this,
								Constants.SHAREDPREFERENCE_REFESH_TRANSATION,
								false);
					} else {
						Constants.showOneButtonDialog(
								CoinHistoryActivity.this, TAG,
								Constants.checkCode(result),
								Constants.DIALOG_CLOSE);
					}

				} else {
					Intent intent = new Intent(CoinHistoryActivity.this,
							ConnectivityErrorActivity.class);
					startActivity(intent);
				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonDialog(CoinHistoryActivity.this,
						TAG, Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
				relativeLayout_EmptyView.setVisibility(View.VISIBLE);
				listViewCoinHistory.setVisibility(View.GONE);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonDialog(CoinHistoryActivity.this,
						TAG, Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
				relativeLayout_EmptyView.setVisibility(View.VISIBLE);
				listViewCoinHistory.setVisibility(View.GONE);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(CoinHistoryActivity.this);
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							dialog.dismiss();
							PurchaseHistoryTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			dialog.dismiss();
			PurchaseHistoryTask.this.cancel(true);
		}
	}

	
}
