package com.pay1.databasehandler;

public class FreeBiesOrder {

	private int orderID;
	private String orderData;

	/**
	 * @return the orderID
	 */
	public final int getOrderID() {
		return orderID;
	}

	/**
	 * @param orderID
	 *            the orderID to set
	 */
	public final void setOrderID(int orderID) {
		this.orderID = orderID;
	}

	/**
	 * @return the orderData
	 */
	public final String getOrderData() {
		return orderData;
	}

	/**
	 * @param orderData
	 *            the orderData to set
	 */
	public final void setOrderData(String orderData) {
		this.orderData = orderData;
	}

}