package com.pay1;

import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.WeakHashMap;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.database.SQLException;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.larvalabs.svgandroid.SVG;
import com.larvalabs.svgandroid.SVGParser;
import com.pay1.adapterhandler.QuickPayAdapter;
import com.pay1.constants.Constants;
import com.pay1.customviews.GiftCellLayout;
import com.pay1.customviews.GiftImageSpecialOffersLayout;
import com.pay1.customviews.MyProgressDialog;
import com.pay1.databasehandler.FreeBies;
import com.pay1.databasehandler.FreeBiesDataSource;
import com.pay1.databasehandler.FreeBiesLocationDataSource;
import com.pay1.databasehandler.FreeBiesOrderDataSource;
import com.pay1.databasehandler.Operator;
import com.pay1.databasehandler.OperatorDataSource;
import com.pay1.databasehandler.QuickPay;
import com.pay1.databasehandler.QuickPayDataSource;
import com.pay1.requesthandler.RequestClass;
import com.pay1.utilities.EditTextValidator;
import com.pay1.utilities.Utility;

public class QuickPayFragment extends Fragment {

	private static final String SCREEN_LABEL = "Quick Pay Screen";
	private EasyTracker easyTracker = null;
	private TextView textView_Title, textView_Recharge, textView_Map,
			textView_Gifts, textView_NoData, textView_LoadMore;
	private LinearLayout linearLayout_Recharge, linearLayout_Map,
			linearLayout_Gifts, loadMoreLayout, one_touch_empty_laayout;
	private ImageView imageView_Recharge, imageView_Map, imageView_Gifts,
			loadMoreIcon;
	private GridView gridView_Recharges;
	private RelativeLayout relativeLayout_EmptyView, layoutOneTouch;
	private QuickPayAdapter adapter;
	private ArrayList<WeakHashMap<String, String>> data;
	LinearLayout linearLayout_Main;
	public static final String ID = "id";
	public static final String OPERATOR = "operator";
	public static final String MOBILE = "mobile";
	public static final String AMOUNT = "amount";
	public static final String FLAG = "flag";
	Button button_top_recharge, button_pocket_gift;
	int rechargeType, rechAmount, service_charge;
	String ed_mobile, ed_amount, ed_OperatorCode, ed_OperatorID,
			ed_operatorProductID, ed_stv;
	Typeface Medium;
	private OperatorDataSource operatorDataSource;
	private QuickPayDataSource quickPayDataSource;
	private FreeBiesDataSource freeBiesDataSource;
	private FreeBiesLocationDataSource freeBiesLocationDataSource;
	private FreeBiesOrderDataSource freeBiesOrderDataSource;

	public static QuickPayFragment newInstance() {
		QuickPayFragment fragment = new QuickPayFragment();
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	@SuppressLint("NewApi")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.quickpay_fragment, container,
				false);
		try {

			easyTracker = EasyTracker.getInstance(getActivity());
			easyTracker.set(Fields.SCREEN_NAME, SCREEN_LABEL);

			quickPayDataSource = new QuickPayDataSource(getActivity());
			operatorDataSource = new OperatorDataSource(getActivity());
			freeBiesDataSource = new FreeBiesDataSource(getActivity());
			freeBiesLocationDataSource = new FreeBiesLocationDataSource(
					getActivity());
			freeBiesOrderDataSource = new FreeBiesOrderDataSource(getActivity());
			button_top_recharge = (Button) view
					.findViewById(R.id.button_top_recharge);
			button_pocket_gift = (Button) view
					.findViewById(R.id.button_pocket_gift);
			
			button_pocket_gift.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Fragment fragment =GiftMainActivity.newInstance(0);
					FragmentManager fragmentManager =getActivity().getSupportFragmentManager();
					FragmentTransaction fragmentTransaction = fragmentManager
							.beginTransaction();
					fragmentTransaction.replace(R.id.frame_container, fragment);
					fragmentTransaction.addToBackStack(null);
					fragmentTransaction.commit();
				}
			});
			button_top_recharge.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Fragment fragment =  RechargeMainActivity.newInstance(1);
					FragmentManager fragmentManager =getActivity().getSupportFragmentManager();
					FragmentTransaction fragmentTransaction = fragmentManager
							.beginTransaction();
					fragmentTransaction.replace(R.id.frame_container, fragment);
					fragmentTransaction.addToBackStack(null);
					fragmentTransaction.commit();
				}
			});
			layoutOneTouch = (RelativeLayout) view
					.findViewById(R.id.layoutOneTouch);
			one_touch_empty_laayout = (LinearLayout) view
					.findViewById(R.id.one_touch_empty_laayout);

			Typeface Reguler = Typeface.createFromAsset(getActivity()
					.getAssets(), "Roboto-Regular.ttf");
			Medium = Typeface.createFromAsset(getActivity().getAssets(),
					"Roboto-Regular.ttf");

			if (Utility.showDialog(getActivity())) {
				showDailogForFreeBie();
			}

			textView_Title = (TextView) view.findViewById(R.id.textView_Title);
			textView_Title.setTypeface(Reguler);
			textView_Title.setVisibility(View.GONE);
			linearLayout_Main = (LinearLayout) view
					.findViewById(R.id.gift_layout);
			textView_NoData = (TextView) view
					.findViewById(R.id.textView_NoData);
			textView_NoData.setTypeface(Reguler);

			textView_LoadMore = (TextView) view
					.findViewById(R.id.textView_LoadMore);
			textView_LoadMore.setTypeface(Reguler);
			textView_LoadMore.setVisibility(View.GONE);
			loadMoreIcon = (ImageView) view.findViewById(R.id.loadMoreIcon);
			loadMoreLayout = (LinearLayout) view
					.findViewById(R.id.loadMoreLayout);
			loadMoreIcon.setVisibility(View.GONE);

			showFreeBies();

			loadMoreLayout.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					startActivity(new Intent(getActivity(),
							QuickPayActivity.class));
				}
			});

			textView_Recharge = (TextView) view
					.findViewById(R.id.textView_Recharge);
			textView_Recharge.setTypeface(Reguler);
			textView_Map = (TextView) view.findViewById(R.id.textView_Map);
			textView_Map.setTypeface(Reguler);
			textView_Gifts = (TextView) view.findViewById(R.id.textView_Gifts);
			textView_Gifts.setTypeface(Reguler);

			linearLayout_Recharge = (LinearLayout) view
					.findViewById(R.id.linearLayout_Recharge);
			linearLayout_Map = (LinearLayout) view
					.findViewById(R.id.linearLayout_Map);
			linearLayout_Gifts = (LinearLayout) view
					.findViewById(R.id.linearLayout_Gifts);

			imageView_Recharge = (ImageView) view
					.findViewById(R.id.imageView_Recharge);
			SVG svg = SVGParser.getSVGFromResource(getResources(),
					R.raw.icon_recharge);
			imageView_Recharge.setImageDrawable(svg.createPictureDrawable());
			if (Build.VERSION.SDK_INT >= 11)
				imageView_Recharge.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
			linearLayout_Recharge
					.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							Intent intent = new Intent(getActivity(),
									MainActivity.class);
							intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
									| Intent.FLAG_ACTIVITY_NEW_TASK);
							intent.putExtra("FROM_QUICKPAY", true);
							intent.putExtra("IS_GIFT", false);
							getActivity().startActivity(intent);
						}
					});

			imageView_Map = (ImageView) view.findViewById(R.id.imageView_Map);
			imageView_Map.setImageDrawable(SVGParser.getSVGFromResource(
					getResources(), R.raw.near_you_blank_screen)
					.createPictureDrawable());
			if (Build.VERSION.SDK_INT >= 11)
				imageView_Map.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
			linearLayout_Map.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent intent = new Intent(getActivity(), MapActivity.class);
					intent.putExtra("SHOW_GIFT", true);
					intent.putExtra("SHOW_RETAILER", true);
					getActivity().startActivity(intent);
				}
			});

			imageView_Gifts = (ImageView) view
					.findViewById(R.id.imageView_Gifts);
			imageView_Gifts.setImageDrawable(SVGParser.getSVGFromResource(
					getResources(), R.raw.my_gifts_blank_screen)
					.createPictureDrawable());
			if (Build.VERSION.SDK_INT >= 11)
				imageView_Gifts.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
			linearLayout_Gifts.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent intent = new Intent(getActivity(),
							MainActivity.class);
					intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
							| Intent.FLAG_ACTIVITY_NEW_TASK);
					intent.putExtra("FROM_QUICKPAY", true);
					intent.putExtra("IS_GIFT", true);
					getActivity().startActivity(intent);
				}
			});

			data = new ArrayList<WeakHashMap<String, String>>();

			try {
				quickPayDataSource.open();

				List<QuickPay> quickpays = quickPayDataSource
						.getQuickPayByLimit(2);
				int size = quickpays.size();
				if (size != 0) {
					textView_Title.setVisibility(View.VISIBLE);
					if (size >= 2) {
						one_touch_empty_laayout.setVisibility(View.GONE);
						layoutOneTouch.setVisibility(View.VISIBLE);
						textView_LoadMore.setVisibility(View.VISIBLE);
						loadMoreIcon.setImageDrawable(SVGParser
								.getSVGFromResource(
										getActivity().getResources(),
										R.raw.ic_dwnarrow)
								.createPictureDrawable());

						loadMoreIcon.setVisibility(View.VISIBLE);
					} else {
						textView_LoadMore.setVisibility(View.GONE);
						layoutOneTouch.setVisibility(View.GONE);
						one_touch_empty_laayout.setVisibility(View.VISIBLE);
					}
					int i = 0;
					for (QuickPay quickpay : quickpays) {
						if (i == 2)
							break;
						i++;
						WeakHashMap<String, String> map = new WeakHashMap<String, String>();
						map.put(OPERATOR, "" + quickpay.getQuickPayProductID());
						map.put(MOBILE, quickpay.getQuickPayMobile());
						map.put(FLAG, "" + quickpay.getQuickPayFlag());
						if (quickpay.getQuickPayFlag() == Constants.BILL_PAYMENT) {

							SimpleDateFormat dateFormat = new SimpleDateFormat(
									"dd-MMM");
							String dt = "";
							try {
								String str = quickpay
										.getQuickPayTransactionDate()
										.replaceAll("-", "/");
								Date date = new Date(str);

								dt = dateFormat.format(date);
							} catch (Exception e) {
								// System.out.println(e.getMessage());
							}
							map.put(AMOUNT, dt);
						} else
							map.put(AMOUNT, "" + quickpay.getQuickPayAmount());
						map.put(ID, "" + quickpay.getQuickPayID());
						data.add(map);
					}
				}else {
					textView_LoadMore.setVisibility(View.GONE);
					layoutOneTouch.setVisibility(View.GONE);
					one_touch_empty_laayout.setVisibility(View.VISIBLE);
				}

			} catch (SQLException exception) {
				// TODO: handle exception
				// // // Log.e("Er ", exception.getMessage());
			} catch (Exception e) {
				// TODO: handle exception
				// // // Log.e("Er ", e.getMessage());
			} finally {
				quickPayDataSource.close();
			}

			gridView_Recharges = (GridView) view
					.findViewById(R.id.gridView_Recharges);
			relativeLayout_EmptyView = (RelativeLayout) view
					.findViewById(R.id.relativeLayout_EmptyView);
			// gridView_Recharges.setEmptyView(relativeLayout_EmptyView);
			adapter = new QuickPayAdapter(getActivity(), data);
			gridView_Recharges.setAdapter(adapter);
			gridView_Recharges
					.setOnItemClickListener(new AdapterView.OnItemClickListener() {

						@Override
						public void onItemClick(AdapterView<?> arg0, View arg1,
								int pos, long arg3) {
							// TODO Auto-generated method stub

							try {
								quickPayDataSource.open();

								QuickPay quickPay = quickPayDataSource
										.getQuickPay(Integer.parseInt(data.get(
												pos).get(ID)));

								if (quickPay != null) {
									ed_mobile = quickPay.getQuickPayMobile();
									ed_amount = String.valueOf(quickPay
											.getQuickPayAmount());
									ed_OperatorCode = quickPay
											.getQuickPayOperatorCode();
									ed_OperatorID = String.valueOf(quickPay
											.getQuickPayOperatorID());
									ed_operatorProductID = String
											.valueOf(quickPay
													.getQuickPayProductID());
									ed_stv = String.valueOf(quickPay
											.getQuickPaySTV());
									rechargeType = quickPay.getQuickPayFlag();
									showConfirmationDialog(getActivity(),
											ed_mobile, ed_amount, rechargeType,
											ed_OperatorCode);
								}

							} catch (SQLException exception) {
								// TODO: handle exception
								// // // Log.e("Er ", exception.getMessage());
							} catch (Exception e) {
								// TODO: handle exception
								// Log.e("Er ", e.getMessage());
							} finally {
								quickPayDataSource.close();
							}
						}
					});

			if (Utility.getRefreshRequired(getActivity(),
					Constants.SHAREDPREFERENCE_REFESH_QUICKPAY))
				new GetQuickPayList().execute();

		} catch (Exception e) {

		}
		return view;
	}

	private void showFreeBies() {
		String offerJson = Utility.getSharedPreferences(getActivity(),
				"Deals_offer");

		try {
			JSONObject jsonObjectDescription = new JSONObject(offerJson);

			final JSONArray jsonArrayOffer = jsonObjectDescription
					.getJSONArray("offer_details");

			for (int i = 0; i < jsonArrayOffer.length(); i++) {
				final String catName = jsonArrayOffer.getJSONObject(i)
						.getString("name");
				final String catID = jsonArrayOffer.getJSONObject(i).getString(
						"id");

				final String layoutType = jsonArrayOffer.getJSONObject(i)
						.getString("featured");

				if (layoutType.equalsIgnoreCase("1")) {
					RelativeLayout relativeLayout = new RelativeLayout(
							getActivity());
					relativeLayout.setPadding(15, 15, 15, 5);
					{
						RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
								LayoutParams.WRAP_CONTENT,
								LayoutParams.WRAP_CONTENT);
						// Align bottom-right, and add
						// bottom-margin
						params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);

						TextView textView_Catagory = new TextView(getActivity());
						textView_Catagory.setText(catName);
						textView_Catagory.setLayoutParams(params);
						relativeLayout.addView(textView_Catagory);
					}
					{
						RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
								LayoutParams.WRAP_CONTENT,
								LayoutParams.WRAP_CONTENT);
						// Align bottom-right, and add
						// bottom-margin
						params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
						TextView textView = new TextView(getActivity());
						textView.setText("See all ");
						textView.setCompoundDrawablesWithIntrinsicBounds(0, 0,
								R.drawable.ic_next, 0);
						textView.setTextSize(12);
						textView.setTypeface(Medium);
						textView.setLayoutParams(params);

						relativeLayout.addView(textView);
					}
					relativeLayout
							.setOnClickListener(new View.OnClickListener() {

								@Override
								public void onClick(View v) {
									// TODO Auto-generated
									// method stub
									Intent intent = new Intent(getActivity(),
											GiftGridActivity.class);
									intent.putExtra("NAME", catName);
									intent.putExtra("TYPE", catID);
									startActivity(intent);
								}
							});
					linearLayout_Main.addView(relativeLayout);

					HorizontalScrollView hs = new HorizontalScrollView(
							getActivity());
					GiftImageSpecialOffersLayout giftCell = new GiftImageSpecialOffersLayout(
							getActivity());
					hs.addView(giftCell);
					linearLayout_Main.addView(hs);

					View v = new View(getActivity());
					LayoutParams layoutParams = new LayoutParams(
							LayoutParams.FILL_PARENT, 1);
					v.setLayoutParams(layoutParams);
					v.setBackgroundColor(getResources().getColor(
							R.color.LightGrey));
					v.setPadding(15, 0, 15, 0);
					linearLayout_Main.addView(v);

					// myHorizontalLayout = (MyHorizontalLayout)
					// view
					// .findViewById(R.id.mygallery);

					try {
						freeBiesDataSource.open();

						String str = jsonArrayOffer.getJSONObject(i).getString(
								"details");
						List<FreeBies> list = freeBiesDataSource
								.getAllFreeBies(str);
						int len = list.size();
						if (len != 0) {
							for (FreeBies freebie : list) {
								giftCell.add(freebie);
							}
							Log.d("Aftre for", "Aftre for");

						} else {
							// new FreeBiesTask()
							// .execute(
							// Utility.getCurrentLatitude(
							// getActivity(),
							// Constants.SHAREDPREFERENCE_CURRENT_LATITUDE),
							// Utility.getCurrentLongitude(
							// getActivity(),
							// Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE));
							// pager.setVisibility(View.GONE);

						}
					} catch (SQLException exception) {
						// TODO: handle exception
						// // // Log.e("Er ",
						// exception.getMessage());
					} catch (Exception e) {
						// TODO: handle exception
						// Log.e("Er ", e.getMessage());
					} finally {
						freeBiesDataSource.close();
					}
				} else {
					RelativeLayout relativeLayout = new RelativeLayout(
							getActivity());
					relativeLayout.setPadding(15, 5, 15, 5);
					{
						RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
								LayoutParams.WRAP_CONTENT,
								LayoutParams.WRAP_CONTENT);
						// Align bottom-right, and add
						// bottom-margin
						params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
						params.setMargins(0, 15, 0, 0);
						TextView textView_Catagory = new TextView(getActivity());
						textView_Catagory.setText(catName);
						textView_Catagory.setTypeface(Medium);
						textView_Catagory.setLayoutParams(params);
						relativeLayout.addView(textView_Catagory);
					}
					{
						RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
								LayoutParams.WRAP_CONTENT,
								LayoutParams.WRAP_CONTENT);
						// Align bottom-right, and add
						// bottom-margin
						params.setMargins(0, 15, 0, 0);
						params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
						TextView textView = new TextView(getActivity());
						textView.setText("See all ");
						textView.setCompoundDrawablesWithIntrinsicBounds(0, 0,
								R.drawable.ic_next, 0);
						textView.setTextSize(12);
						textView.setTypeface(Medium);
						textView.setLayoutParams(params);

						relativeLayout.addView(textView);
					}
					relativeLayout
							.setOnClickListener(new View.OnClickListener() {

								@Override
								public void onClick(View v) {
									// TODO Auto-generated
									// method stub
									Intent intent = new Intent(getActivity(),
											GiftGridActivity.class);
									intent.putExtra("NAME", catName);
									intent.putExtra("TYPE", catID);
									startActivity(intent);
								}
							});
					linearLayout_Main.addView(relativeLayout);

					HorizontalScrollView hs = new HorizontalScrollView(
							getActivity());
					GiftCellLayout giftCell = new GiftCellLayout(getActivity());
					hs.addView(giftCell);
					linearLayout_Main.addView(hs);

					View v = new View(getActivity());
					LayoutParams layoutParams = new LayoutParams(
							LayoutParams.FILL_PARENT, 1);
					v.setLayoutParams(layoutParams);
					v.setBackgroundColor(getResources().getColor(
							R.color.LightGrey));
					v.setPadding(15, 0, 15, 0);
					linearLayout_Main.addView(v);

					// myHorizontalLayout = (MyHorizontalLayout)
					// view
					// .findViewById(R.id.mygallery);

					try {
						freeBiesDataSource.open();

						String str = jsonArrayOffer.getJSONObject(i).getString(
								"details");
						List<FreeBies> list = freeBiesDataSource
								.getAllFreeBies(str);
						int len = list.size();
						if (len != 0) {
							for (FreeBies freebie : list) {
								giftCell.add(freebie);
							}
							Log.d("Aftre for", "Aftre for");

						} else {
							// new FreeBiesTask()
							// .execute(
							// Utility.getCurrentLatitude(
							// getActivity(),
							// Constants.SHAREDPREFERENCE_CURRENT_LATITUDE),
							// Utility.getCurrentLongitude(
							// getActivity(),
							// Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE));
							// pager.setVisibility(View.GONE);

						}
					} catch (SQLException exception) {
						// TODO: handle exception
						// // // Log.e("Er ",
						// exception.getMessage());
					} catch (Exception e) {
						// TODO: handle exception
						// Log.e("Er ", e.getMessage());
					} finally {
						freeBiesDataSource.close();
					}
				}

				/*
				 * RelativeLayout relativeLayout = new RelativeLayout(
				 * getActivity()); relativeLayout.setPadding(15, 5, 15, 5); {
				 * RelativeLayout.LayoutParams params = new
				 * RelativeLayout.LayoutParams( LayoutParams.WRAP_CONTENT,
				 * LayoutParams.WRAP_CONTENT); // Align bottom-right, and add //
				 * bottom-margin
				 * params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
				 * 
				 * TextView textView_Catagory = new TextView(getActivity());
				 * textView_Catagory.setText(catName);
				 * textView_Catagory.setLayoutParams(params);
				 * relativeLayout.addView(textView_Catagory); } {
				 * RelativeLayout.LayoutParams params = new
				 * RelativeLayout.LayoutParams( LayoutParams.WRAP_CONTENT,
				 * LayoutParams.WRAP_CONTENT); // Align bottom-right, and add //
				 * bottom-margin
				 * params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT); TextView
				 * textView = new TextView(getActivity());
				 * textView.setText("See all >");
				 * textView.setLayoutParams(params);
				 * 
				 * relativeLayout.addView(textView); }
				 * relativeLayout.setOnClickListener(new View.OnClickListener()
				 * {
				 * 
				 * @Override public void onClick(View v) { // TODO
				 * Auto-generated // method stub Intent intent = new
				 * Intent(getActivity(), GiftGridActivity.class);
				 * intent.putExtra("NAME", catName); intent.putExtra("TYPE",
				 * catID); startActivity(intent); } });
				 * linearLayout_Main.addView(relativeLayout);
				 * 
				 * HorizontalScrollView hs = new HorizontalScrollView(
				 * getActivity()); GiftCellLayout giftCell = new
				 * GiftCellLayout(getActivity()); hs.addView(giftCell);
				 * linearLayout_Main.addView(hs);
				 * 
				 * View v = new View(getActivity()); LayoutParams layoutParams =
				 * new LayoutParams( LayoutParams.FILL_PARENT, 1);
				 * v.setLayoutParams(layoutParams);
				 * v.setBackgroundColor(getResources
				 * ().getColor(R.color.LightGrey)); v.setPadding(15, 0, 15, 0);
				 * linearLayout_Main.addView(v);
				 * 
				 * // myHorizontalLayout = (MyHorizontalLayout) // view //
				 * .findViewById(R.id.mygallery);
				 * 
				 * try { freeBiesDataSource.open();
				 * 
				 * String str = jsonArrayOffer.getJSONObject(i).getString(
				 * "details"); List<FreeBies> list = freeBiesDataSource
				 * .getAllFreeBies(str); int len = list.size(); if (len != 0) {
				 * for (FreeBies freebie : list) { giftCell.add(freebie); }
				 * Log.d("Aftre for", "Aftre for");
				 * 
				 * } else { // new FreeBiesTask() // .execute( //
				 * Utility.getCurrentLatitude( // getActivity(), //
				 * Constants.SHAREDPREFERENCE_CURRENT_LATITUDE), //
				 * Utility.getCurrentLongitude( // getActivity(), //
				 * Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE)); //
				 * pager.setVisibility(View.GONE);
				 * 
				 * } } catch (SQLException exception) { // TODO: handle
				 * exception // // // Log.e("Er ", // exception.getMessage()); }
				 * catch (Exception e) { // TODO: handle exception //
				 * Log.e("Er ", e.getMessage()); } finally {
				 * freeBiesDataSource.close(); }
				 */
			}
		} catch (SQLException se) {

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView().build());
		// EasyTracker.getInstance(this).activityStart(this);
		easyTracker.send(MapBuilder.createAppView().build());
	}

	@Override
	public void onStop() {
		super.onStop();
		// EasyTracker.getInstance(this).activityStop(this);
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		// outState.putString(KEY_CONTENT, mContent);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.Fragment#onDestroy()
	 */
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		// Intent intent = new Intent(getActivity(), MainActivity.class);
		// intent.putExtra("FROM_QUICKPAY", false);
		// intent.putExtra("IS_GIFT", false);
		// intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
		// | Intent.FLAG_ACTIVITY_NEW_TASK);
		// getActivity().startActivity(intent);
		// getActivity().finish();
	}

	@SuppressLint("NewApi")
	public void showConfirmationDialog(final Context context,
			final String mobile, final String amount, final int rechargeType,
			final String operatorCode) {
		try {

			final Dialog dialog = new Dialog(context);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.getWindow().setBackgroundDrawableResource(
					android.R.color.transparent);
			dialog.setContentView(R.layout.dialog_confirmation_quickpay);
			dialog.setCancelable(false);
			dialog.setCanceledOnTouchOutside(false);
			// dialog.setTitle(null);

			Typeface Reguler = Typeface.createFromAsset(context.getAssets(),
					"Roboto-Regular.ttf");

			Typeface Medium = Typeface.createFromAsset(context.getAssets(),
					"Roboto-Regular.ttf");

			final EditText editText_Amount = (EditText) dialog
					.findViewById(R.id.editText_Amount);
			editText_Amount.setTypeface(Reguler);

			// set the custom dialog components - text, image and button
			TextView textView_Title = (TextView) dialog
					.findViewById(R.id.textView_Title);
			textView_Title.setTypeface(Reguler);
			textView_Title.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					dialog.dismiss();
				}
			});

			TextView textView_Message = (TextView) dialog
					.findViewById(R.id.textView_Message);
			textView_Message.setTypeface(Medium);

			TextView textView_Msg = (TextView) dialog
					.findViewById(R.id.textView_Msg);
			textView_Msg.setText("Complete transaction using");
			textView_Msg.setTypeface(Medium);

			FrameLayout frame_amount = (FrameLayout) dialog
					.findViewById(R.id.frame_amount);
			if (rechargeType == Constants.BILL_PAYMENT)
				frame_amount.setVisibility(View.VISIBLE);
			else
				frame_amount.setVisibility(View.GONE);

			final TextView textView_TitleAmount = (TextView) dialog
					.findViewById(R.id.textView_TitleAmount);
			textView_TitleAmount.setTypeface(Medium);
			textView_TitleAmount.setVisibility(View.GONE);

			final TextView textView_Details = (TextView) dialog
					.findViewById(R.id.textView_Details);
			textView_Details.setTypeface(Reguler);
			textView_Details.setVisibility(View.GONE);

			final Button button_Submit = (Button) dialog
					.findViewById(R.id.button_Submit);
			button_Submit.setTypeface(Reguler);
			// if button is clicked, close the custom dialog
			button_Submit.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {

					if (ed_amount != null && !ed_amount.equalsIgnoreCase("0")
							&& !ed_amount.equalsIgnoreCase("")) {

						String charges_slab = null;
						double service_charge_amount = 0, service_charge_percent = 0, service_tax_percent = 0, min = 0, max = 0;
						try {
							operatorDataSource.open();

							Operator operator;
							if (rechargeType == Constants.RECHARGE_MOBILE) {
								operator = operatorDataSource
										.getOperator(Constants.RECHARGE_MOBILE,
												operatorCode);
							} else if (rechargeType == Constants.RECHARGE_DATA) {
								operator = operatorDataSource.getOperator(
										Constants.RECHARGE_DATA, operatorCode);
							} else if (rechargeType == Constants.RECHARGE_DTH) {
								operator = operatorDataSource.getOperator(
										Constants.RECHARGE_DTH, operatorCode);
							} else {
								operator = operatorDataSource.getOperator(
										Constants.BILL_PAYMENT, operatorCode);
							}

							if (operator != null) {
								charges_slab = operator.getOperatorChargeSlab();
								service_charge_amount = operator
										.getOperatorServiceChargeAmount();
								service_charge_percent = operator
										.getOperatorServiceChargePercent();
								service_tax_percent = operator
										.getOperatorServiceTaxPercent();
								min = operator.getOperatorMinAmount();
								max = operator.getOperatorMaxAmount();
							}

						} catch (SQLException exception) {
							// TODO: handle exception
							// // // Log.e("Er ", exception.getMessage());
						} catch (Exception e) {
							// TODO: handle exception
							// // // Log.e("Er ", e.getMessage());
						} finally {
							operatorDataSource.close();
						}

						double rechargeAmount = 0, balance = 0;
						try {
							rechargeAmount = Double.parseDouble(ed_amount);
							balance = Double.parseDouble(Utility.getBalance(
									getActivity(),
									Constants.SHAREDPREFERENCE_BALANCE));
						} catch (Exception e) {
							rechargeAmount = 0;
							balance = 0;
						}
						if (rechargeAmount < min || rechargeAmount > max) {
							EditTextValidator.isValidFragmentAmount(
									getActivity(), editText_Amount,
									Constants.ERROR_AMOUNT_RANG_FIELD + min
											+ " to " + max, min, max);
							editText_Amount.requestFocus();
							return;
						}

						dialog.dismiss();
						if (button_Submit
								.getText()
								.toString()
								.trim()
								.equalsIgnoreCase(
										getActivity().getResources().getString(
												R.string.wallet_payment))) {

							new RechargeViaWalletTask().execute(ed_mobile,
									ed_OperatorID,
									String.valueOf(rechargeType), ed_amount,
									ed_stv);
						} else {
							new RechargeViaPGTask().execute(ed_mobile,
									ed_OperatorID,
									String.valueOf(rechargeType), ed_amount,
									ed_stv);
						}
					} else {
						EditTextValidator.hasFragmentText(getActivity(),
								editText_Amount,
								Constants.ERROR_AMOUNT_BLANK_FIELD);
					}
				}

			});

			final TextView textViewPayBy = (TextView) dialog
					.findViewById(R.id.textViewPayBy);
			textViewPayBy.setTypeface(Reguler);
			// if button is clicked, close the custom dialog
			textViewPayBy.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (button_Submit
							.getText()
							.toString()
							.trim()
							.equalsIgnoreCase(
									getActivity().getResources().getString(
											R.string.wallet_payment))) {
						if (ed_amount != null
								&& !ed_amount.equalsIgnoreCase("0")
								&& !ed_amount.equalsIgnoreCase("")) {

							String charges_slab = null;
							double service_charge_amount = 0, service_charge_percent = 0, service_tax_percent = 0, min = 0, max = 0;
							try {
								operatorDataSource.open();

								Operator operator;
								if (rechargeType == Constants.RECHARGE_MOBILE) {
									operator = operatorDataSource.getOperator(
											Constants.RECHARGE_MOBILE,
											operatorCode);
								} else if (rechargeType == Constants.RECHARGE_DATA) {
									operator = operatorDataSource.getOperator(
											Constants.RECHARGE_DATA,
											operatorCode);
								} else if (rechargeType == Constants.RECHARGE_DTH) {
									operator = operatorDataSource.getOperator(
											Constants.RECHARGE_DTH,
											operatorCode);
								} else {
									operator = operatorDataSource.getOperator(
											Constants.BILL_PAYMENT,
											operatorCode);
								}

								if (operator != null) {
									charges_slab = operator
											.getOperatorChargeSlab();
									service_charge_amount = operator
											.getOperatorServiceChargeAmount();
									service_charge_percent = operator
											.getOperatorServiceChargePercent();
									service_tax_percent = operator
											.getOperatorServiceTaxPercent();
									min = operator.getOperatorMinAmount();
									max = operator.getOperatorMaxAmount();
								}

							} catch (SQLException exception) {
								// TODO: handle exception
								// // // Log.e("Er ", exception.getMessage());
							} catch (Exception e) {
								// TODO: handle exception
								// // // Log.e("Er ", e.getMessage());
							} finally {
								operatorDataSource.close();
							}

							double rechargeAmount = 0, balance = 0;
							try {
								rechargeAmount = Double.parseDouble(ed_amount);
								balance = Double.parseDouble(Utility
										.getBalance(
												getActivity(),
												Constants.SHAREDPREFERENCE_BALANCE));
							} catch (Exception e) {
								rechargeAmount = 0;
								balance = 0;
							}
							if (rechargeAmount < min || rechargeAmount > max) {
								EditTextValidator.isValidFragmentAmount(
										getActivity(), editText_Amount,
										Constants.ERROR_AMOUNT_RANG_FIELD + min
												+ " to " + max, min, max);
								editText_Amount.requestFocus();
								return;
							}

							dialog.dismiss();

							new RechargeViaPGTask().execute(ed_mobile,
									ed_OperatorID,
									String.valueOf(rechargeType), ed_amount,
									ed_stv);

						} else {
							EditTextValidator.hasFragmentText(getActivity(),
									editText_Amount,
									Constants.ERROR_AMOUNT_BLANK_FIELD);
						}
					} else {
						Intent intent = new Intent(getActivity(),
								MapActivity.class);
						intent.putExtra("SHOW_GIFT", false);
						intent.putExtra("SHOW_RETAILER", true);
						getActivity().startActivity(intent);
					}
				}
			});

			ImageView imageView_Close = (ImageView) dialog
					.findViewById(R.id.imageView_Close);
			imageView_Close.setImageDrawable(SVGParser.getSVGFromResource(
					context.getResources(), R.raw.ic_close)
					.createPictureDrawable());
			if (Build.VERSION.SDK_INT >= 11)
				imageView_Close.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
			imageView_Close.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					dialog.dismiss();
				}
			});

			String charges_slab = null;
			double service_charge_amount = 0, service_charge_percent = 0, service_tax_percent = 0, min = 0, max = 0;
			try {
				operatorDataSource.open();

				Operator operator;
				if (rechargeType == Constants.RECHARGE_MOBILE) {
					operator = operatorDataSource.getOperator(
							Constants.RECHARGE_MOBILE, operatorCode);
				} else if (rechargeType == Constants.RECHARGE_DATA) {
					operator = operatorDataSource.getOperator(
							Constants.RECHARGE_DATA, operatorCode);
				} else if (rechargeType == Constants.RECHARGE_DTH) {
					operator = operatorDataSource.getOperator(
							Constants.RECHARGE_DTH, operatorCode);
				} else {
					operator = operatorDataSource.getOperator(
							Constants.BILL_PAYMENT, operatorCode);
				}
				if (operator != null) {
					charges_slab = operator.getOperatorChargeSlab();
					service_charge_amount = operator
							.getOperatorServiceChargeAmount();
					service_charge_percent = operator
							.getOperatorServiceChargePercent();
					service_tax_percent = operator
							.getOperatorServiceTaxPercent();
					min = operator.getOperatorMinAmount();
					max = operator.getOperatorMaxAmount();
				}

			} catch (SQLException exception) {
				// TODO: handle exception
				// // // Log.e("Er ", exception.getMessage());
			} catch (Exception e) {
				// TODO: handle exception
				// // // Log.e("Er ", e.getMessage());
			} finally {
				operatorDataSource.close();
			}

			double rechargeAmount = 0, balance = 0;
			try {
				rechargeAmount = Double.parseDouble(amount);
				balance = Double.parseDouble(Utility.getBalance(getActivity(),
						Constants.SHAREDPREFERENCE_BALANCE));
			} catch (Exception e) {
				rechargeAmount = 0;
				balance = 0;
			}

			int slab_amount = 0;
			try {
				if (charges_slab.equalsIgnoreCase("null")
						|| charges_slab.equalsIgnoreCase("")) {
					rechargeAmount = (rechargeAmount + (rechargeAmount * service_charge_percent));
					rechargeAmount = (rechargeAmount + (rechargeAmount * service_tax_percent));
				} else {
					String[] comma_seperator = charges_slab.split(",");
					for (String s1 : comma_seperator) {
						String[] colon_sperator = s1.split(":");
						if (rechargeAmount > Double
								.parseDouble(colon_sperator[0])) {
							slab_amount = Integer.parseInt(colon_sperator[1]);
						}
					}
				}
			} catch (Exception e) {
			}
			if (rechargeType == Constants.BILL_PAYMENT) {
				textView_Details.setText("("
						+ ((int) rechargeAmount + (int) slab_amount) + " = "
						+ (int) rechargeAmount + " + " + slab_amount
						+ " service charge inclusive of all taxes.)");
				textView_Details.setVisibility(View.VISIBLE);
			} else {
				textView_Details.setVisibility(View.GONE);
			}

			if ((rechargeAmount + slab_amount) > balance) {
				button_Submit.setText(getActivity().getResources().getString(
						R.string.online_payment));
				textViewPayBy.setText(getActivity().getResources().getString(
						R.string.offline_payment));
				textView_Title.setText("Confirmation");
				textView_Message.setText("Recharge the transaction of Rs. "
						+ amount + " on " + mobile + " .");
				textView_Msg.setVisibility(View.VISIBLE);
			} else {
				button_Submit.setText(getActivity().getResources().getString(
						R.string.wallet_payment));
				textViewPayBy.setText(getActivity().getResources().getString(
						R.string.online_payment));

				if (rechargeType != Constants.BILL_PAYMENT) {
					textView_Title.setText("Confirmation");
					textView_Message.setText("Recharge the transaction of Rs. "
							+ amount + " on " + mobile + ".");
				} else {
					textView_Title.setText("Enter Amount");
					textView_Message
							.setText("Make payment of your mobile bill "
									+ mobile + " .");
				}
				textView_Msg.setVisibility(View.GONE);
			}
			service_charge = slab_amount;

			editText_Amount.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					// TODO Auto-generated method stub
					if (editText_Amount.getText().toString().trim().length() != 0) {
						textView_TitleAmount.setVisibility(View.GONE);
						textView_Details.setVisibility(View.VISIBLE);
					} else {
						textView_TitleAmount.setVisibility(View.GONE);
						textView_Details.setVisibility(View.GONE);
					}

					if (editText_Amount.getText().toString().trim().length() > 0) {
						if (!EditTextValidator.hasFragmentText(getActivity(),
								editText_Amount,
								Constants.ERROR_AMOUNT_BLANK_FIELD))
							return;

						String charges_slab = null;
						double service_charge_amount = 0, service_charge_percent = 0, service_tax_percent = 0, min = 0, max = 0;
						try {
							operatorDataSource.open();

							Operator operator = operatorDataSource.getOperator(
									Constants.BILL_PAYMENT, operatorCode);

							if (operator != null) {
								charges_slab = operator.getOperatorChargeSlab();
								service_charge_amount = operator
										.getOperatorServiceChargeAmount();
								service_charge_percent = operator
										.getOperatorServiceChargePercent();
								service_tax_percent = operator
										.getOperatorServiceTaxPercent();
								min = operator.getOperatorMinAmount();
								max = operator.getOperatorMaxAmount();
							}

						} catch (SQLException exception) {
							// TODO: handle exception
							// // // Log.e("Er ", exception.getMessage());
						} catch (Exception e) {
							// TODO: handle exception
							// // // Log.e("Er ", e.getMessage());
						} finally {
							operatorDataSource.close();
						}

						double rechargeAmount = 0, balance = 0;
						try {
							rechargeAmount = Double.parseDouble(editText_Amount
									.getText().toString().trim());
							balance = Double.parseDouble(Utility.getBalance(
									getActivity(),
									Constants.SHAREDPREFERENCE_BALANCE));
						} catch (Exception e) {
							rechargeAmount = 0;
							balance = 0;
						}
						// if (rechargeAmount < min || rechargeAmount > max) {
						// EditTextValidator.isValidFragmentAmount(
						// getActivity(), editText_Amount,
						// Constants.ERROR_AMOUNT_RANG_FIELD + min
						// + " to " + max, min, max);
						// editText_Amount.requestFocus();
						// return;
						// }

						int slab_amount = 0;
						try {
							if (charges_slab.equalsIgnoreCase("null")
									|| charges_slab.equalsIgnoreCase("")) {
								rechargeAmount = (rechargeAmount + (rechargeAmount * service_charge_percent));
								rechargeAmount = (rechargeAmount + (rechargeAmount * service_tax_percent));
							} else {
								String[] comma_seperator = charges_slab
										.split(",");
								for (String s1 : comma_seperator) {
									String[] colon_sperator = s1.split(":");
									if (rechargeAmount > Double
											.parseDouble(colon_sperator[0])) {
										slab_amount = Integer
												.parseInt(colon_sperator[1]);
									}
								}
							}
						} catch (Exception e) {
						}
						if (rechargeType == Constants.BILL_PAYMENT) {
							textView_Details
									.setText("("
											+ ((int) rechargeAmount + (int) slab_amount)
											+ " = "
											+ (int) rechargeAmount
											+ " + "
											+ slab_amount
											+ " service charge inclusive of all taxes.)");
							textView_Details.setVisibility(View.VISIBLE);
						} else {
							textView_Details.setVisibility(View.GONE);
						}

						if ((rechargeAmount + slab_amount) > balance) {
							button_Submit.setText(getActivity().getResources()
									.getString(R.string.online_payment));
							textViewPayBy.setText(getActivity().getResources()
									.getString(R.string.offline_payment));
						} else {
							button_Submit.setText(getActivity().getResources()
									.getString(R.string.wallet_payment));
							textViewPayBy.setText(getActivity().getResources()
									.getString(R.string.online_payment));
						}

						service_charge = slab_amount;
						ed_amount = editText_Amount.getText().toString().trim();
					}
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					// TODO Auto-generated method stub

				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

				}
			});

			dialog.show();

		} catch (Exception exception) {
		}
	}

	public class GetQuickPayList extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String response = RequestClass.getInstance().readPay1B2CRequest(
					getActivity(), Constants.B2C_URL + "get_quickpaylist/?");
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			if (!result.startsWith("Error")) {

				String status;
				try {
					JSONObject jsonObject = new JSONObject(result);
					status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {

						try {
							quickPayDataSource.open();

							JSONArray description = jsonObject
									.getJSONArray("description");

							if (description.length() != 0) {
								quickPayDataSource.deleteQuickPay();
								textView_Title.setVisibility(View.VISIBLE);
								long timestamp = System.currentTimeMillis();
								quickPayDataSource.createQuickPay(description,
										timestamp);

								List<QuickPay> quickpays = quickPayDataSource
										.getQuickPayByLimit(7);
								int size = quickpays.size();
								if (size != 0) {
									data.clear();
									textView_Title.setVisibility(View.VISIBLE);
									if (size > 6)
										textView_LoadMore
												.setVisibility(View.VISIBLE);
									else
										textView_LoadMore
												.setVisibility(View.GONE);
									int i = 0;
									for (QuickPay quickpay : quickpays) {
										if (i == 6)
											break;
										i++;
										WeakHashMap<String, String> map = new WeakHashMap<String, String>();
										map.put(OPERATOR,
												""
														+ quickpay
																.getQuickPayProductID());
										map.put(MOBILE,
												quickpay.getQuickPayMobile());
										map.put(FLAG,
												"" + quickpay.getQuickPayFlag());
										if (quickpay.getQuickPayFlag() == Constants.BILL_PAYMENT) {

											SimpleDateFormat dateFormat = new SimpleDateFormat(
													"dd-MMM");
											String dt = "";
											try {
												String str = quickpay
														.getQuickPayTransactionDate()
														.replaceAll("-", "/");
												Date date = new Date(str);

												dt = dateFormat.format(date);
											} catch (Exception e) {
											}
											map.put(AMOUNT, dt);
										} else
											map.put(AMOUNT,
													""
															+ quickpay
																	.getQuickPayAmount());
										map.put(ID,
												"" + quickpay.getQuickPayID());
										data.add(map);
									}
								}
							}
						} catch (SQLException exception) {
							// TODO: handle exception
							// // // Log.e("Er ", exception.getMessage());
						} catch (Exception e) {
							// TODO: handle exception
							// // // Log.e("Er ", e.getMessage());
						} finally {
							quickPayDataSource.close();
						}
						adapter.notifyDataSetChanged();
						try {
							Utility.setRefreshRequired(getActivity(),
									Constants.SHAREDPREFERENCE_REFESH_QUICKPAY,
									false);
						} catch (Exception e) {
						}
					} else {
					}
				} catch (JSONException e) {
				}
			}
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}
	}

	public class RechargeViaWalletTask extends
			AsyncTask<String, String, String> implements OnDismissListener {
		private MyProgressDialog dialog;
		String url;

		@Override
		protected String doInBackground(String... params) {
			ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

			listValuePair.add(new BasicNameValuePair("operator", params[1]));
			listValuePair.add(new BasicNameValuePair("flag", params[2]));
			listValuePair.add(new BasicNameValuePair("paymentopt", "wallet"));
			listValuePair.add(new BasicNameValuePair("payment", "1"));
			listValuePair.add(new BasicNameValuePair("name", ""));
			listValuePair.add(new BasicNameValuePair("recharge_flag", "1"));
			listValuePair.add(new BasicNameValuePair("latitude", Utility
					.getCurrentLatitude(getActivity(),
							Constants.SHAREDPREFERENCE_CURRENT_LATITUDE)));
			listValuePair.add(new BasicNameValuePair("longitude", Utility
					.getCurrentLongitude(getActivity(),
							Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE)));
			String time = Utility.getSharedPreferences(getActivity(),
					Constants.LAST_GIFT_UPDATED) == null ? "" : Utility
					.getSharedPreferences(getActivity(),
							Constants.LAST_GIFT_UPDATED);

			if (time != "") {
				try {
					DateFormat dateFormat = new SimpleDateFormat(
							"yyyy-MM-dd HH:mm:ss");
					Date date = new Date(Long.parseLong(time));
					time = dateFormat.format(date);
				} catch (Exception e) {
				}
			}
			try {
				listValuePair.add(new BasicNameValuePair("updatedTime",
						URLEncoder.encode(time, "utf-8")));
			} catch (Exception e) {
			}
			String method = "";
			if (rechargeType == Constants.RECHARGE_MOBILE
					|| rechargeType == Constants.RECHARGE_DATA) {
				listValuePair.add(new BasicNameValuePair("mobile_number",
						params[0]));
				listValuePair.add(new BasicNameValuePair("recharge", "1"));
				listValuePair.add(new BasicNameValuePair("amount", params[3]));
				listValuePair.add(new BasicNameValuePair("stv", params[4]));

				method = "recharge";
			} else if (rechargeType == Constants.BILL_PAYMENT) {
				listValuePair.add(new BasicNameValuePair("mobile_number",
						params[0]));
				listValuePair.add(new BasicNameValuePair("billpayment", "1"));
				listValuePair.add(new BasicNameValuePair("payment_flag", "1"));
				listValuePair.add(new BasicNameValuePair("base_amount",
						params[3]));
				listValuePair.add(new BasicNameValuePair("amount", (Integer
						.parseInt(params[3]) + service_charge) + ""));
				listValuePair.add(new BasicNameValuePair("service_charge", ""
						+ service_charge));
				listValuePair.add(new BasicNameValuePair("service_tax", ""
						+ service_charge));

				method = "billpayment";
			} else if (rechargeType == Constants.RECHARGE_DTH) {
				listValuePair.add(new BasicNameValuePair("subscriber_id",
						params[0]));
				listValuePair.add(new BasicNameValuePair("recharge", "1"));
				listValuePair.add(new BasicNameValuePair("amount", params[3]));

				method = "recharge";
			}
			listValuePair.add(new BasicNameValuePair("api_version",
					Constants.API_VERSION));
			String response = RequestClass.getInstance().readPay1B2CRequest(
					getActivity(), Constants.B2C_URL + method, listValuePair);
			listValuePair.clear();
			return response;
		}

		@Override
		protected void onCancelled() {
			// TODO Auto-generated method stub
			super.onCancelled();
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			try {

				if (!result.startsWith("Error")) {

					JSONObject jsonObject = new JSONObject(result);

					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {

						JSONObject jsonObjectGift = jsonObject
								.getJSONObject("dealDetails");
						try {

							freeBiesDataSource.open();
							freeBiesLocationDataSource.open();

							JSONArray jsonArrayAlldeals = jsonObjectGift
									.getJSONArray("Alldeals");
							long timestamp = System.currentTimeMillis();

							freeBiesDataSource.createFreeBies(
									jsonArrayAlldeals, timestamp);
							// for (int i = 0; i < jsonArrayAlldeals.length();
							// i++) {
							//
							// String deal_id = jsonArrayAlldeals
							// .getJSONObject(i).getString("id");
							// String offer_id = jsonArrayAlldeals
							// .getJSONObject(i).getString("of_id");
							// try {
							// freeBiesDataSource.deleteFreeBies(Integer
							// .parseInt(deal_id));
							// freeBiesLocationDataSource
							// .deleteFreeBiesLocation(Integer
							// .parseInt(offer_id));
							// } catch (Exception e) {
							// continue;
							// }
							//
							// String deal_name = jsonArrayAlldeals
							// .getJSONObject(i).getString("name");
							// String url = jsonArrayAlldeals.getJSONObject(i)
							// .getString("i_url");
							// String catagory = jsonArrayAlldeals
							// .getJSONObject(i).getString("cat");
							// String catagory_id = jsonArrayAlldeals
							// .getJSONObject(i).getString("cat_id");
							// String min_amt = jsonArrayAlldeals
							// .getJSONObject(i).getString("min");
							// String offer_name = jsonArrayAlldeals
							// .getJSONObject(i).getString("of_name");
							// String validity = jsonArrayAlldeals
							// .getJSONObject(i).getString("valid");
							// String desc = jsonArrayAlldeals
							// .getJSONObject(i).getString("desc");
							// String offer_lat = "0", offer_lng = "0",
							// offer_add = "", offer_area = "", offer_city = "",
							// offer_state = "";
							//
							// JSONArray jsonArrayLocation = jsonArrayAlldeals
							// .getJSONObject(i).getJSONArray("locs");
							//
							// for (int j = 0; j < jsonArrayLocation.length();
							// j++) {
							// JSONObject jsonObjectInner = jsonArrayLocation
							// .getJSONObject(j);
							//
							// String loc_lat = jsonObjectInner
							// .getString("lat");
							// String loc_lng = jsonObjectInner
							// .getString("lng");
							// String loc_add = jsonObjectInner
							// .getString("addr");
							// String loc_area = jsonObjectInner
							// .getString("area");
							// String loc_city = jsonObjectInner
							// .getString("city");
							// String loc_state = jsonObjectInner
							// .getString("state");
							//
							// if (j == 0) {
							// offer_lat = loc_lat;
							// offer_lng = loc_lng;
							// offer_add = loc_add;
							// offer_area = loc_area;
							// offer_city = loc_city;
							// offer_state = loc_state;
							// }
							//
							// freeBiesLocationDataSource
							// .createFreeBiesLocation(
							// Integer.parseInt(offer_id),
							// Double.parseDouble(loc_lat),
							// Double.parseDouble(loc_lng),
							// loc_add, loc_area,
							// loc_city, loc_state, 0, 0,
							// timestamp);
							// }
							// freeBiesDataSource.createFreeBies(
							// Integer.parseInt(deal_id), deal_name,
							// url, catagory,
							// Integer.parseInt(catagory_id),
							// Double.parseDouble(offer_lat),
							// Double.parseDouble(offer_lng),
							// offer_add, offer_area, offer_city,
							// offer_state, 0,
							// jsonArrayLocation.length(),
							// Integer.parseInt(offer_id), offer_name,
							// validity, desc,
							// Integer.parseInt(min_amt), timestamp);
							// }

							// Utility.setSharedPreferences(getActivity(),
							// Constants.LAST_GIFT_UPDATED,
							// String.valueOf(System.currentTimeMillis()));
						} catch (SQLException exception) {
							// TODO: handle exception
							// Log.e("Er ", exception.getMessage());
						} catch (Exception e) {
							// TODO: handle exception
							// Log.e("Er ", e.getMessage());
						} finally {
							freeBiesDataSource.close();
							freeBiesLocationDataSource.close();
						}
						try {
							freeBiesOrderDataSource.open();
							freeBiesOrderDataSource.deleteFreeBiesOrder();
							freeBiesOrderDataSource.createFreeBiesOrder(1,
									jsonObjectGift.getString("offer_details"));
						} catch (SQLException exception) {
							// TODO: handle exception
							// Log.e("Er ", exception.getMessage());
						} catch (Exception e) {
							// TODO: handle exception
							// Log.e("Er ", e.getMessage());
						} finally {
							freeBiesOrderDataSource.close();
						}
						Utility.setOrder(getActivity(),
								Constants.SHAREDPREFERENCE_GIFT,
								jsonObjectGift.getString("claimedGifts"));

						JSONObject description = jsonObject
								.getJSONObject("description");
						Utility.setBalance(getActivity(),
								Constants.SHAREDPREFERENCE_BALANCE,
								description.getString("closing_balance"));
						Utility.setBalance(getActivity(),
								Constants.SHAREDPREFERENCE_LOYALTY_POINTS,
								description.getString("loyalty_balance"));
						String loyalty_points = description
								.getString("loyalty_points");
						Constants.showSuccessFailureDialog(getActivity(),
								"Success", rechargeType, ed_mobile,
								ed_operatorProductID, ed_amount, "", true, "",
								description.getString("transaction_id"),
								loyalty_points);
					} else {
						Constants.showSuccessFailureDialog(getActivity(),
								"Failure", rechargeType, ed_mobile,
								ed_operatorProductID, ed_amount, "", false,
								jsonObject.getString("description").toString(),
								"", "");
					}
				} else {
					Intent intent = new Intent(getActivity(),
							ConnectivityErrorActivity.class);
					startActivity(intent);
				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonDialog(getActivity(), "Quick Recharge",
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonDialog(getActivity(), "Quick Recharge",
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(getActivity());
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(false);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							RechargeViaWalletTask.this.cancel(true);
							dialog.cancel();
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			RechargeViaWalletTask.this.cancel(true);
			dialog.cancel();
		}

	}

	public class RechargeViaPGTask extends AsyncTask<String, String, String>
			implements OnDismissListener {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

			listValuePair.add(new BasicNameValuePair("operator", params[1]));
			listValuePair.add(new BasicNameValuePair("flag", params[2]));
			listValuePair.add(new BasicNameValuePair("paymentopt", "online"));
			listValuePair.add(new BasicNameValuePair("payment", "1"));
			listValuePair.add(new BasicNameValuePair("name", ""));
			listValuePair.add(new BasicNameValuePair("recharge_flag", "1"));
			listValuePair.add(new BasicNameValuePair("latitude", Utility
					.getCurrentLatitude(getActivity(),
							Constants.SHAREDPREFERENCE_CURRENT_LATITUDE)));
			listValuePair.add(new BasicNameValuePair("longitude", Utility
					.getCurrentLongitude(getActivity(),
							Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE)));
			String time = Utility.getSharedPreferences(getActivity(),
					Constants.LAST_GIFT_UPDATED) == null ? "" : Utility
					.getSharedPreferences(getActivity(),
							Constants.LAST_GIFT_UPDATED);

			if (time != "") {
				try {
					DateFormat dateFormat = new SimpleDateFormat(
							"yyyy-MM-dd HH:mm:ss");
					Date date = new Date(Long.parseLong(time));
					time = dateFormat.format(date);
				} catch (Exception e) {
				}
			}
			try {
				listValuePair.add(new BasicNameValuePair("updatedTime",
						URLEncoder.encode(time, "utf-8")));
			} catch (Exception e) {
			}
			String method = "";
			if (rechargeType == Constants.RECHARGE_MOBILE
					|| rechargeType == Constants.RECHARGE_DATA) {
				listValuePair.add(new BasicNameValuePair("mobile_number",
						params[0]));
				listValuePair.add(new BasicNameValuePair("recharge", "1"));
				listValuePair.add(new BasicNameValuePair("amount", params[3]));
				listValuePair.add(new BasicNameValuePair("stv", params[4]));

				method = "recharge";
			} else if (rechargeType == Constants.BILL_PAYMENT) {
				listValuePair.add(new BasicNameValuePair("mobile_number",
						params[0]));
				listValuePair.add(new BasicNameValuePair("billpayment", "1"));
				listValuePair.add(new BasicNameValuePair("payment_flag", "1"));
				listValuePair.add(new BasicNameValuePair("base_amount",
						params[3]));
				listValuePair.add(new BasicNameValuePair("amount", (Integer
						.parseInt(params[3]) + service_charge) + ""));
				listValuePair.add(new BasicNameValuePair("service_charge", ""
						+ service_charge));
				listValuePair.add(new BasicNameValuePair("service_tax", ""
						+ service_charge));

				method = "billpayment";
			} else if (rechargeType == Constants.RECHARGE_DTH) {
				listValuePair.add(new BasicNameValuePair("subscriber_id",
						params[0]));
				listValuePair.add(new BasicNameValuePair("recharge", "1"));
				listValuePair.add(new BasicNameValuePair("amount", params[3]));

				method = "recharge";
			}
			listValuePair.add(new BasicNameValuePair("api_version",
					Constants.API_VERSION));
			String response = RequestClass.getInstance().readPay1B2CRequest(
					getActivity(), Constants.B2C_URL + method, listValuePair);
			listValuePair.clear();
			return response;

		}

		@Override
		protected void onCancelled() {
			// TODO Auto-generated method stub
			super.onCancelled();
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			try {

				if (!result.startsWith("Error")) {
					// result = result.replaceAll("\\\\", "");
					JSONObject jsonObject = new JSONObject(result);

					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {

						JSONObject jsonObjectGift = jsonObject
								.getJSONObject("dealDetails");
						try {

							freeBiesDataSource.open();
							freeBiesLocationDataSource.open();

							JSONArray jsonArrayAlldeals = jsonObjectGift
									.getJSONArray("Alldeals");
							long timestamp = System.currentTimeMillis();

							freeBiesDataSource.createFreeBies(
									jsonArrayAlldeals, timestamp);
							// for (int i = 0; i < jsonArrayAlldeals.length();
							// i++) {
							//
							// String deal_id = jsonArrayAlldeals
							// .getJSONObject(i).getString("id");
							// String offer_id = jsonArrayAlldeals
							// .getJSONObject(i).getString("of_id");
							// try {
							// freeBiesDataSource.deleteFreeBies(Integer
							// .parseInt(deal_id));
							// freeBiesLocationDataSource
							// .deleteFreeBiesLocation(Integer
							// .parseInt(offer_id));
							// } catch (Exception e) {
							// continue;
							// }
							//
							// String deal_name = jsonArrayAlldeals
							// .getJSONObject(i).getString("name");
							// String url = jsonArrayAlldeals.getJSONObject(i)
							// .getString("i_url");
							// String catagory = jsonArrayAlldeals
							// .getJSONObject(i).getString("cat");
							// String catagory_id = jsonArrayAlldeals
							// .getJSONObject(i).getString("cat_id");
							// String min_amt = jsonArrayAlldeals
							// .getJSONObject(i).getString("min");
							// String offer_name = jsonArrayAlldeals
							// .getJSONObject(i).getString("of_name");
							// String validity = jsonArrayAlldeals
							// .getJSONObject(i).getString("valid");
							// String desc = jsonArrayAlldeals
							// .getJSONObject(i).getString("desc");
							// String offer_lat = "0", offer_lng = "0",
							// offer_add = "", offer_area = "", offer_city = "",
							// offer_state = "";
							//
							// JSONArray jsonArrayLocation = jsonArrayAlldeals
							// .getJSONObject(i).getJSONArray("locs");
							//
							// for (int j = 0; j < jsonArrayLocation.length();
							// j++) {
							// JSONObject jsonObjectInner = jsonArrayLocation
							// .getJSONObject(j);
							// WeakHashMap<String, String> map1 = new
							// WeakHashMap<String, String>();
							//
							// String loc_lat = jsonObjectInner
							// .getString("lat");
							// String loc_lng = jsonObjectInner
							// .getString("lng");
							// String loc_add = jsonObjectInner
							// .getString("addr");
							// String loc_area = jsonObjectInner
							// .getString("area");
							// String loc_city = jsonObjectInner
							// .getString("city");
							// String loc_state = jsonObjectInner
							// .getString("state");
							//
							// if (j == 0) {
							// offer_lat = loc_lat;
							// offer_lng = loc_lng;
							// offer_add = loc_add;
							// offer_area = loc_area;
							// offer_city = loc_city;
							// offer_state = loc_state;
							// }
							//
							// freeBiesLocationDataSource
							// .createFreeBiesLocation(
							// Integer.parseInt(offer_id),
							// Double.parseDouble(loc_lat),
							// Double.parseDouble(loc_lng),
							// loc_add, loc_area,
							// loc_city, loc_state, 0, 0,
							// timestamp);
							// }
							// freeBiesDataSource.createFreeBies(
							// Integer.parseInt(deal_id), deal_name,
							// url, catagory,
							// Integer.parseInt(catagory_id),
							// Double.parseDouble(offer_lat),
							// Double.parseDouble(offer_lng),
							// offer_add, offer_area, offer_city,
							// offer_state, 0,
							// jsonArrayLocation.length(),
							// Integer.parseInt(offer_id), offer_name,
							// validity, desc,
							// Integer.parseInt(min_amt), timestamp);
							// }

							// Utility.setSharedPreferences(getActivity(),
							// Constants.LAST_GIFT_UPDATED,
							// String.valueOf(System.currentTimeMillis()));
						} catch (SQLException exception) {
							// TODO: handle exception
							// Log.e("Er ", exception.getMessage());
						} catch (Exception e) {
							// TODO: handle exception
							// Log.e("Er ", e.getMessage());
						} finally {
							freeBiesDataSource.close();
							freeBiesLocationDataSource.close();
						}

						try {
							freeBiesOrderDataSource.open();
							freeBiesOrderDataSource.deleteFreeBiesOrder();
							freeBiesOrderDataSource.createFreeBiesOrder(1,
									jsonObjectGift.getString("offer_details"));
						} catch (SQLException exception) {
							// TODO: handle exception
							// Log.e("Er ", exception.getMessage());
						} catch (Exception e) {
							// TODO: handle exception
							// Log.e("Er ", e.getMessage());
						} finally {
							freeBiesOrderDataSource.close();
						}
						Utility.setOrder(getActivity(),
								Constants.SHAREDPREFERENCE_GIFT,
								jsonObjectGift.getString("claimedGifts"));

						JSONObject description = jsonObject
								.getJSONObject("description");
						Intent intent = new Intent(getActivity(),
								PGActivity.class);
						intent.putExtra("online_form", false);
						intent.putExtra("OPERATOR_NAME", ed_operatorProductID);
						intent.putExtra("TAX", "");
						intent.putExtra("MOBILE_NUMBER", ed_mobile);
						intent.putExtra("RECHARGE_AMOUNT", ed_amount);
						intent.putExtra("RECHARGE_FOR", rechargeType);
						intent.putExtra("TRANS_ID",
								description.getString("txnid"));
						intent.putExtra("CONTENT",
								description.getString("form_content")
										.replaceAll("\\\\", ""));

						startActivity(intent);

					} else {
						Constants.showSuccessFailureDialog(getActivity(),
								"Failure", rechargeType, ed_mobile,
								ed_operatorProductID, ed_amount, "", false,
								jsonObject.getString("description").toString(),
								"", "");
					}
				} else {
					Intent intent = new Intent(getActivity(),
							ConnectivityErrorActivity.class);
					startActivity(intent);
				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonDialog(getActivity(), "Quick Recharge",
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonDialog(getActivity(), "Quick Recharge",
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(getActivity());
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(false);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							RechargeViaPGTask.this.cancel(true);
							dialog.cancel();
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			RechargeViaPGTask.this.cancel(true);
			dialog.cancel();
		}

	}

	public void showDailogForFreeBie() {
		try {
			final Dialog dialog = new Dialog(getActivity());

			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

			dialog.setCancelable(false);
			dialog.setCanceledOnTouchOutside(false);
			dialog.getWindow().setBackgroundDrawableResource(
					android.R.color.transparent);
			dialog.setContentView(R.layout.dialog_freebie_info_layout);
			
			TextView textViewInfo = (TextView) dialog
					.findViewById(R.id.textViewInfo);
			TextView textView_Title = (TextView) dialog
					.findViewById(R.id.textView_Title);
			textViewInfo.setTypeface(Medium);
			String msg_body =Utility.getSharedPreferences(getActivity(), "msg_body");//"<font color=#1651ff>You've earned </font> <font color=#E63638 >100</font><font color=#1651ff> Gift Coins for signing up with Pay1</font>";
			String msg_title =Utility.getSharedPreferences(getActivity(), "msg_title");
			textViewInfo.setText(msg_body);
			textView_Title.setText(msg_title);
			Button btnDialogContinue = (Button) dialog
					.findViewById(R.id.btnDialogContinue);
			btnDialogContinue.setText("DONE");
			btnDialogContinue.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Utility.saveshowDialog(getActivity(), false);
					dialog.dismiss();
				}
			});
			/*
			 * Button dialogButton = (Button)
			 * dialog.findViewById(R.id.dialogButtonOK);
			 * dialogButton.setText("AMAZING, LET'S GET STARTED");
			 * dialogButton.setTypeface(Medium); // if button is clicked, close
			 * the custom dialog dialogButton.setOnClickListener(new
			 * OnClickListener() {
			 * 
			 * @Override public void onClick(View v) {
			 * Utility.saveshowDialog(getActivity(), true); dialog.dismiss(); }
			 * });
			 */

			dialog.show();
		} catch (Exception exc) {
			Log.d("msg", "Msg");
		}
	}

}