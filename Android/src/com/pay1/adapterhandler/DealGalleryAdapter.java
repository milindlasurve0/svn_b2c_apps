package com.pay1.adapterhandler;

import java.util.ArrayList;
import java.util.WeakHashMap;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.pay1.DealDetailsActivity;
import com.pay1.R;
import com.pay1.imagehandler.ImageLoader;

public class DealGalleryAdapter extends BaseAdapter {

	private Context context;
	private ArrayList<WeakHashMap<String, String>> data;
	private Typeface Reguler, Narrow, Narrow_thin;
	private ImageLoader imageLoader;

	public DealGalleryAdapter(Context context,
			ArrayList<WeakHashMap<String, String>> data) {
		this.context = context;
		this.data = data;
		Reguler = Typeface.createFromAsset(context.getAssets(),
				"EncodeSansNormal-400-Regular.ttf");

		Narrow = Typeface.createFromAsset(context.getAssets(),
				"EncodeSansNarrow-500-Medium.ttf");

		Narrow_thin = Typeface.createFromAsset(context.getAssets(),
				"EncodeSansNarrow-100-Thin.ttf");
		imageLoader = new ImageLoader(context);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder viewHolder;
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.deal_gallery_adapter, null);
			viewHolder = new ViewHolder();

			viewHolder.imageView_Gallery = (ImageView) convertView
					.findViewById(R.id.imageView_Gallery);
			viewHolder.textView_Info = (TextView) convertView
					.findViewById(R.id.textView_Info);
			viewHolder.textView_Info.setTypeface(Reguler);
			convertView.setTag(viewHolder);

		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		try {
			WeakHashMap<String, String> map = new WeakHashMap<String, String>(
					data.get(position));

			imageLoader.getDealImage(map.get(DealDetailsActivity.GALARY_URL),
					viewHolder.imageView_Gallery);
			viewHolder.textView_Info.setText(map
					.get(DealDetailsActivity.GALARY_INFO));

		} catch (Exception e) {
		}

		// Animation animation = AnimationUtils.loadAnimation(context,
		// R.anim.accelerate);
		// convertView.startAnimation(animation);
		return convertView;
	}

	private class ViewHolder {
		ImageView imageView_Gallery;
		TextView textView_Info;
	}
}
