package com.pay1;

import android.annotation.SuppressLint;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.pay1.customviews.MyProgressDialog;

import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.pay1.constants.Constants;

public class PGActivity extends AppBaseActivity {

	private static final String SCREEN_LABEL = "PG Screen";
	private EasyTracker easyTracker = null;
	private ImageView imageView_Back;
	private TextView textView_Title;
	private WebView webView_PG;
	private String html = "";
	int rechargeType;
	String operatorName, mobileNumber, rechargeAmount, payOpid,
			specialRecharge, subsId, tax = "";

	@SuppressLint("NewApi")
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pg_activity);
		try {
			easyTracker = EasyTracker.getInstance(PGActivity.this);
			easyTracker.set(Fields.SCREEN_NAME, SCREEN_LABEL);
			registerBaseActivityReceiver();

			Bundle bundle = getIntent().getExtras();
			if (bundle != null) {
				html = bundle.getString("CONTENT");
				mobileNumber = bundle.getString(Constants.MOBILE_NUMBER);
				rechargeAmount = bundle.getString(Constants.RECHARGE_AMOUNT);
				rechargeType = bundle.getInt(Constants.RECHARGE_FOR);
				operatorName = bundle.getString(Constants.OPERATOR_NAME);
				tax = bundle.getString("TAX");
				if (rechargeType == Constants.RECHARGE_DTH)
					subsId = bundle.getString(Constants.SUBSCRIBER_ID);

			}

			imageView_Back = (ImageView) findViewById(R.id.imageView_Back);
			imageView_Back.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onBackPressed();
				}
			});

			Typeface Reguler = Typeface.createFromAsset(getAssets(),
					"EncodeSansNormal-400-Regular.ttf");

			textView_Title = (TextView) findViewById(R.id.textView_Title);
			textView_Title.setTypeface(Reguler);

			webView_PG = (WebView) findViewById(R.id.webView_PG);
			webView_PG.setOnLongClickListener(new View.OnLongClickListener() {

				@Override
				public boolean onLongClick(View v) {
					// TODO Auto-generated method stub
					return true;
				}
			});
			webView_PG.setLongClickable(false);
			webView_PG.setBackgroundColor(0x00000000);
			if (Build.VERSION.SDK_INT >= 11)
				webView_PG.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);
			webView_PG.getSettings().setJavaScriptEnabled(true);
			webView_PG.getSettings().setDomStorageEnabled(true);
			webView_PG.getSettings().setDatabaseEnabled(true);
			webView_PG.setWebViewClient(new MyWebClient());
			webView_PG.loadDataWithBaseURL(null, html, "text/html", "UTF-8",
					null);

			overridePendingTransition(R.anim.slide_in_right,
					R.anim.slide_out_left);

		} catch (Exception e) {
		}

	}

	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView().build());
		// EasyTracker.getInstance(this).activityStart(this);
		easyTracker.send(MapBuilder.createAppView().build());
	}

	@Override
	public void onStop() {
		super.onStop();
		// EasyTracker.getInstance(this).activityStop(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		try {
			if (rechargeType == Constants.RECHARGE_MOBILE
					|| rechargeType == Constants.RECHARGE_DTH
					|| rechargeType == Constants.RECHARGE_DATA) {
				textView_Title.setTextColor(getResources().getColor(
						R.color.app_green_color));
				imageView_Back
						.setImageResource(R.drawable.button_recharge_back_icon);
				if (rechargeType == Constants.RECHARGE_MOBILE)
					textView_Title
							.setText("Recharge > Mobile > Order summary > Payment");
				else if (rechargeType == Constants.RECHARGE_DTH)
					textView_Title
							.setText("Recharge > DTH > Order summary > Payment");
				else if (rechargeType == Constants.RECHARGE_DATA)
					textView_Title
							.setText("Recharge > Data card > Order summary > Payment");
			} else if (rechargeType == Constants.SUBSCRIBE_DEAL) {
				textView_Title.setTextColor(getResources().getColor(
						R.color.app_orange_color));
				imageView_Back
						.setImageResource(R.drawable.button_deal_back_icon);
				textView_Title
						.setText("Deal > Offer > Order summary > Payment");
			} else if (rechargeType == Constants.BILL_PAYMENT) {
				textView_Title.setTextColor(getResources().getColor(
						R.color.app_skyblue_color));
				imageView_Back
						.setImageResource(R.drawable.button_bill_back_icon);
				textView_Title
						.setText("Bills > Mobile bill > Order summary > Payment");
			}
			super.onResume();
		} catch (Exception e) {
		}
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unRegisterBaseActivityReceiver();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
	}

	class MyWebClient extends WebViewClient implements OnDismissListener {
		private MyProgressDialog dialog = new MyProgressDialog(PGActivity.this);

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * android.webkit.WebViewClient#onReceivedSslError(android.webkit.WebView
		 * , android.webkit.SslErrorHandler, android.net.http.SslError)
		 */
		@Override
		public void onReceivedSslError(WebView view, SslErrorHandler handler,
				SslError error) {
			handler.proceed(); // Ignore SSL certificate errors
		}

		public void onReceivedError(WebView view, int errorCode,
				String description, String failingUrl) {
			Log.e("Load Signup page", description);
			Toast.makeText(
					PGActivity.this,
					"Problem loading. Make sure internet connection is available.",
					Toast.LENGTH_SHORT).show();
		}

		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			// TODO Auto-generated method stub
			super.onPageStarted(view, url, favicon);
			// if (this.this.dialog.isShowing())
			// this.this.dialog.dismiss();
			// dialog = new MyProgressDialog(PGActivity.this);
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(false);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							dialog.dismiss();
							finish();
						}
					});

			this.dialog.show();
		}

		@Override
		public void onPageFinished(WebView view, String url) {
			// String str=url.substring(url.lastIndexOf("/"));
			if (this.dialog.isShowing())
				this.dialog.dismiss();

			Log.w("PG", url);
			view.loadUrl("javascript:console.log(document.body.getElementsByTagName('pre')[0].innerHTML);");
			if (url.contains("payu_update")) {
				String str = url.substring(url.lastIndexOf("/") + 1);
				Log.w("OUT", str);

				Intent intent = new Intent(PGActivity.this,
						SuccessOrFailureActivity.class);
				intent.putExtra("DATA", "");
				intent.putExtra("TAX", tax);
				if (str.equalsIgnoreCase("success")) {
					intent.putExtra(Constants.SUCCESS_OR_FAILURE, true);
					intent.putExtra(Constants.REASON,
							"Transaction completed successfully.");
				} else if (str.equalsIgnoreCase("failure")) {
					intent.putExtra(Constants.SUCCESS_OR_FAILURE, false);
					intent.putExtra(Constants.REASON, "Transaction failed.");
				} else if (str.equalsIgnoreCase("cancel")) {
					intent.putExtra(Constants.SUCCESS_OR_FAILURE, false);
					intent.putExtra(Constants.REASON, "Transaction cancelled.");
				} else if (str.equalsIgnoreCase("timeout")) {
					intent.putExtra(Constants.SUCCESS_OR_FAILURE, false);
					intent.putExtra(Constants.REASON,
							"Transaction request timeout.");
				}
				if (rechargeType == Constants.RECHARGE_DTH) {
					intent.putExtra(Constants.OPERATOR_NAME, operatorName);
					intent.putExtra(Constants.MOBILE_NUMBER, subsId);
					intent.putExtra(Constants.RECHARGE_AMOUNT, rechargeAmount);
				} else if (rechargeType == Constants.RECHARGE_MOBILE
						|| rechargeType == Constants.RECHARGE_DATA) {
					intent.putExtra(Constants.OPERATOR_NAME, operatorName);
					intent.putExtra(Constants.MOBILE_NUMBER, mobileNumber);
					intent.putExtra(Constants.RECHARGE_AMOUNT, rechargeAmount);
				} else if (rechargeType == Constants.BILL_PAYMENT) {
					intent.putExtra(Constants.OPERATOR_NAME, operatorName);
					intent.putExtra(Constants.MOBILE_NUMBER, mobileNumber);
					intent.putExtra(Constants.RECHARGE_AMOUNT, rechargeAmount);
				} else if (rechargeType == Constants.WALLET_PAYMENT) {
					intent.putExtra(Constants.MOBILE_NUMBER,
							"Your wallet refilled successfully.");
					intent.putExtra(Constants.RECHARGE_AMOUNT, rechargeAmount);
				} else if (rechargeType == Constants.SUBSCRIBE_DEAL) {
					intent.putExtra(Constants.MOBILE_NUMBER, mobileNumber);
					intent.putExtra(Constants.RECHARGE_AMOUNT, rechargeAmount);
				}

				intent.putExtra(Constants.RECHARGE_FOR, rechargeType);
				startActivity(intent);
				finish();
			}

		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			this.dialog.dismiss();
			finish();
		}
	}
}
