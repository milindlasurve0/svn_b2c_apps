//
//  ServiceLoaderView.m
//  ASIHTTPDemo
//
//  Created by Dharmender Yadav on 03/11/12.
//
//

#import "ServiceLoaderView.h"

#import <QuartzCore/QuartzCore.h>



@interface ServiceLoaderView ()
{
	IBOutlet UIView *backView;
	IBOutlet UILabel *titleLabel;
}
- (id)initWithFrame:(CGRect)frame title:(NSString *) title;
@end

@implementation ServiceLoaderView


+(ServiceLoaderView *)initializeLoader {
	return [[[self alloc] initWithFrame:[[UIScreen mainScreen] bounds] title:@"Loading..."] autorelease];
    
    
}




- (id)initWithFrame:(CGRect)frame title:(NSString *) title
{
	self = [super initWithFrame:frame];
	if (self) {
		// Initialization code
		UIView *view = [[[NSBundle mainBundle] loadNibNamed:@"ServiceLoaderView" owner:self options:nil] objectAtIndex:0];
		[view setFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.2];
		[self addSubview:view];
			
			
		backView.layer.cornerRadius = 8;
		titleLabel.text = title;
		
		return self;
		
		[self orientationChanged];
		[[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
		[[NSNotificationCenter defaultCenter] addObserver:self
																							 selector:@selector(orientationChanged)
																									 name:UIDeviceOrientationDidChangeNotification
																								 object:nil];
    }
    return self;
}

-(void)orientationChanged
{
	
	UIDeviceOrientation o = [UIDevice currentDevice].orientation;
	NSLog(@"orientationChanged:%@",NSStringFromCGRect(self.frame));

	CGFloat angle = 0;
	if ( o == UIDeviceOrientationLandscapeLeft ) angle = M_PI_2;
	else if ( o == UIDeviceOrientationLandscapeRight ) angle = -M_PI_2;
	else if ( o == UIDeviceOrientationPortraitUpsideDown ) return;// angle = M_PI;
	
	[UIView beginAnimations:@"rotate" context:nil];
	[UIView setAnimationDuration:0.4];
	backView.transform = CGAffineTransformRotate(CGAffineTransformMakeTranslation(0,0),angle);
	[UIView commitAnimations];
}

-(void) removeDeviceOrientaionNotification {
	[[UIDevice currentDevice] endGeneratingDeviceOrientationNotifications];
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)dealloc {

	[self removeDeviceOrientaionNotification];
	[super dealloc];
}

@end
