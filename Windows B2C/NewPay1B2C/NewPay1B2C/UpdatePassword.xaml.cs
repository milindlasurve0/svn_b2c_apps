﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using NewPay1B2C;

namespace NewPay1B2C
	{
	public partial class UpdatePassword : PhoneApplicationPage
		{
		ProgressBarManager ProgressBarManager;
		public UpdatePassword ()
			{
			InitializeComponent ();
			GoogleAnalytics.EasyTracker.GetTracker ().SendView ("UpdatePassword");
			ProgressIndicator prog = new ProgressIndicator ();
			ProgressBarManager = new ProgressBarManager ();
			SystemTray.SetProgressIndicator (this, prog);
			ProgressBarManager.Hook (prog);
			}

		private void buttonImage_Click (object sender, RoutedEventArgs e)
			{
			if (!(textOldPin.Password.Length < 4))
				{

				if (!(textPin.Password.Length < 4))
					{
					if (!(textConfirmPin.Password.Length < 4))
						{
						if (textPin.Password.Equals (textConfirmPin.Password))
							{
							pinUpdateTask (textOldPin.Password, textPin.Password);
							}
						else
							{
							MessageBox.Show ("New  Password and Retype Password must be same.");
							}
						}
					else
						{
						MessageBox.Show ("Please enter correct confirm pin.");
						}
					}
				else
					{
					MessageBox.Show ("Please enter correct pin.");
					}
				}
			else
				{
				MessageBox.Show ("Please enter otp.");
				}

			}

		private async void pinUpdateTask (string oldPin, string password)
			{
			PhoneApplicationService.Current.State["loginCall"] = 1;
			ProgressBarManager.Show (25, "Please wait...", false, 10);
			var values = new List<KeyValuePair<string, string>> ();
			values.Add (new KeyValuePair<string, string> ("actiontype", "update_password"));
			values.Add (new KeyValuePair<string, string> ("mobile_number", Constant.getUserNumber()));
			values.Add (new KeyValuePair<string, string> ("vtype", "1"));
			values.Add (new KeyValuePair<string, string> ("password", password));
			values.Add (new KeyValuePair<string, string> ("confirm_password", password));
			values.Add (new KeyValuePair<string, string> ("code", oldPin));
			values.Add (new KeyValuePair<string, string> ("api", "true"));
			FormUrlEncodedContent values1 = new FormUrlEncodedContent (values);
			Task<string> response = Constant.postParamsAndgetResponse (Constant.B2C_URL, values1);

			string result = await response;
			if (string.IsNullOrEmpty (result))
				{
				NavigationService.Navigate (new Uri ("/NoConnection.xaml?", UriKind.RelativeOrAbsolute));
				}
			else
				{
				var dict = (JObject)JsonConvert.DeserializeObject (result);
				string status = dict["status"].ToString ();
				var description = dict["description"];
				if (status.Equals ("success"))
					{
					//MessageBox.Show (description.ToString ());
				    Constant.SavePersistent (Constant.SETTINGS_USER_NAME, description["name"].ToString ());
					Constant.SavePersistent (Constant.SETTINGS_USER_PIN, password);
					Constant.setUserNumber (description["mobile"].ToString ());
					Constant.SavePersistent (Constant.SETTINGS_USER_GENDER, description["gender"].ToString ());
					Constant.SavePersistent (Constant.SETTINGS_USER_EMAIL, description["email"].ToString ());
					Constant.SavePersistent (Constant.SETTINGS_USER_ID, description["user_id"].ToString ());
					Constant.SavePersistent (Constant.SETTINGS_USER_DOB, description["date_of_birth"].ToString ());
					Constant.setbalance (description["wallet_balance"].ToString ());
					Constant.SavePersistent (Constant.SETTINGS_USER_LATTITUDE, description["latitude"].ToString ());
					Constant.SavePersistent (Constant.SETTINGS_USER_LONGITUDE, description["longitude"].ToString ());
					Constant.SavePersistent (Constant.SETTINGS_USER_SUPPORT_NUMBER, description["support_number"].ToString ());
					Constant.SavePersistent (Constant.SETTINGS_IS_LOGIN, true);
					if ((int)PhoneApplicationService.Current.State["updateFromLogin"] == 1)
						{
						NavigationService.Navigate (new Uri ("/HomePage.xaml?", UriKind.RelativeOrAbsolute));
						NavigationService.RemoveBackEntry ();
						}
					else
						{
						NavigationService.Navigate (new Uri ("/Profile.xaml?", UriKind.RelativeOrAbsolute));
						NavigationService.RemoveBackEntry ();
						}
					}
				else if (status.Equals ("failure"))
					{
					Constant.checkForErrorCode (result); //MessageBox.Show (description.ToString ());
					}
				}
			ProgressBarManager.Hide (25);

			}

		private void buttonImage_Click_Cancel (object sender, RoutedEventArgs e)
			{
			textConfirmPin.Password = "";
			textPin.Password = "";
			textOldPin.Password = "";
			}

		private void buttonImage_Click_Back (object sender, RoutedEventArgs e)
			{
			NavigationService.GoBack ();
			}
		}
	}