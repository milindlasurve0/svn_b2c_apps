package com.pay1;

import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.WeakHashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.SQLException;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationManager;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.GoogleMap.OnMyLocationButtonClickListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.VisibleRegion;
import com.larvalabs.svgandroid.SVGParser;
import com.pay1.constants.Constants;
import com.pay1.customviews.MyProgressDialog;
import com.pay1.customviews.MySupportMapFragment;
import com.pay1.customviews.TouchableWrapper;
import com.pay1.databasehandler.FreeBies;
import com.pay1.databasehandler.FreeBiesDataSource;
import com.pay1.databasehandler.FreeBiesLocation;
import com.pay1.databasehandler.FreeBiesLocationDataSource;
import com.pay1.requesthandler.RequestClass;
import com.pay1.servicehandler.GPSTracker;
import com.pay1.utilities.RoundedTransformation;
import com.pay1.utilities.Utility;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

public class MapActivity extends FragmentActivity implements OnMapReadyCallback {

	private static final String SCREEN_LABEL = "Map Screen";
	private EasyTracker easyTracker = null;
	private final String TAG = "Locator";

	private GoogleMap googleMap;
	private TextView textView_Title;
	private ImageView imageView_Back;
	private FreeBiesDataSource freeBiesDataSource;
	private FreeBiesLocationDataSource freeBiesLocationDataSource;
	private Hashtable<String, String> markers;
	private Hashtable<String, Integer> deal_id;
	private Hashtable<String, Long> shop_id;
	// private Marker marker;
	//private ImageLoader imageLoader;
	boolean isFirstTme = true;
	private ArrayList<WeakHashMap<String, String>> data;
	private float previousZoomLevel = -1.0f;
	private boolean isZooming = false;
	public static final String SHOP_USER_ID = "user_id";
	public static final String SHOP_MOBILE = "mobile";
	public static final String SHOP_NAME = "shopname";
	public static final String SHOP_SALE = "sale";
	public static final String SHOP_LATITUDE = "latitude";
	public static final String SHOP_LONGITUDE = "logitude";
	public static final String SHOP_ADDRESS = "address";
	public static final String SHOP_PIN = "pin";
	public static final String SHOP_AREA = "area";
	public static final String SHOP_CITY = "city";
	public static final String SHOP_STATE = "state";
	public static final String SHOP_DISTANCE = "distance";
	public static final String SHOP_IMAGE = "imagepath";
	Handler handler = new Handler();
	private boolean isInfoWindowClicked = false;
	public static boolean isMoving = false;
	ProgressBar progressBar1;
	DealorLocatorTaskNew dealorLocatorTaskNew = null;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.map_activity);
		try {

			if (Utility.showAgain(MapActivity.this)) {
				showNearShopDialog(MapActivity.this);
			}
		
			
			progressBar1=(ProgressBar)findViewById(R.id.progressBar1);
			easyTracker = EasyTracker.getInstance(MapActivity.this);
			easyTracker.set(Fields.SCREEN_NAME, SCREEN_LABEL);
			freeBiesDataSource = new FreeBiesDataSource(MapActivity.this);
			freeBiesLocationDataSource = new FreeBiesLocationDataSource(
					MapActivity.this);

			Typeface Reguler = Typeface.createFromAsset(getAssets(),
					"Roboto-Regular.ttf");
			// Typeface Medium = Typeface.createFromAsset(getAssets(),
			// "Roboto-Regular.ttf");
			RelativeLayout back_layout = (RelativeLayout) findViewById(R.id.back_layout);
			back_layout.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onBackPressed();
				}
			});
			data = new ArrayList<WeakHashMap<String, String>>();

			markers = new Hashtable<String, String>();
			deal_id = new Hashtable<String, Integer>();
			shop_id = new Hashtable<String, Long>();
		//	imageLoader = new ImageLoader(MapActivity.this);

			textView_Title = (TextView) findViewById(R.id.textView_Title);
			textView_Title.setTypeface(Reguler);

			imageView_Back = (ImageView) findViewById(R.id.imageView_Back);
			imageView_Back.setImageDrawable(SVGParser.getSVGFromResource(
					getResources(), R.raw.ic_back_old).createPictureDrawable());
			if (Build.VERSION.SDK_INT >= 11)
				imageView_Back.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
			imageView_Back.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onBackPressed();
				}
			});

			
			
			
			boolean gift = getIntent().getBooleanExtra("SHOW_GIFT", false);
			final boolean retailer = getIntent().getBooleanExtra(
					"SHOW_RETAILER", false);

			// data = new ArrayList<WeakHashMap<String, String>>();
			if (googleMap == null) {

				MySupportMapFragment customMapFragment = ((MySupportMapFragment) getSupportFragmentManager()
						.findFragmentById(R.id.map_full));
				customMapFragment
						.setOnDragListener(new TouchableWrapper.OnDragListener() {
							@Override
							public void onDrag(MotionEvent motionEvent) {
								switch (motionEvent.getAction()) {
								case MotionEvent.ACTION_DOWN:
									MapActivity.isMoving = false;
									handler.removeCallbacks(runnable, null);
									break;
								case MotionEvent.ACTION_MOVE:
									handler.removeCallbacks(runnable, null);

									break;
								case MotionEvent.ACTION_UP:
									MapActivity.isMoving = false;
									isZooming = false;
									handler.postDelayed(runnable, 2000);
									break;
								}
							}
						});

				// Try to obtain the map from the SupportMapFragment.
				googleMap = ((SupportMapFragment) getSupportFragmentManager()
						.findFragmentById(R.id.map_full)).getMap();

				googleMap.setMyLocationEnabled(true);
				googleMap.getUiSettings().setMyLocationButtonEnabled(true);
				googleMap.getUiSettings().setRotateGesturesEnabled(true);
				googleMap.getUiSettings().setCompassEnabled(true);
				googleMap.getUiSettings().setZoomGesturesEnabled(true);
				googleMap.getUiSettings().setZoomControlsEnabled(true);
				CameraUpdate center = CameraUpdateFactory.newLatLng(new LatLng(
						21.0000, 78.0000));
				googleMap.moveCamera(center);
				googleMap
						.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

							@Override
							public void onMapClick(LatLng arg0) {
								// TODO Auto-generated method stub

								isInfoWindowClicked = false;
							}
						});
				googleMap
						.setInfoWindowAdapter(new CustomGiftInfoWindowAdapter());
				googleMap
						.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {

							@Override
							public void onInfoWindowClick(Marker marker) {
								// TODO Auto-generated method stub
								if (marker.getId() != null && deal_id != null
										&& deal_id.size() > 0) {
									if (deal_id.get(marker.getId()) != null) {
										Intent intent = new Intent(
												MapActivity.this,
												FreeBiesDetailActivity.class);
										intent.putExtra("ID",
												deal_id.get(marker.getId()));
										startActivity(intent);
									}
								}
								if (marker.getId() != null && shop_id != null
										&& shop_id.size() > 0) {
									for (int i = 0; i < data.size(); i++) {
										WeakHashMap<String, String> map = data
												.get(i);
										if (map.get(MapActivity.SHOP_USER_ID)
												.equalsIgnoreCase(
														String.valueOf(shop_id.get(marker
																.getId())))) {

											Intent intent = new Intent(
													MapActivity.this,
													ShopLocatorDetailActivity.class);

											intent.putExtra(
													"MOBILE",
													map.get(MapActivity.SHOP_MOBILE));
											intent.putExtra("NAME", map
													.get(MapActivity.SHOP_NAME));
											intent.putExtra("SALE", map
													.get(MapActivity.SHOP_SALE));
											intent.putExtra(
													"LATITUDE",
													map.get(MapActivity.SHOP_LATITUDE));
											intent.putExtra(
													"LONGITUDE",
													map.get(MapActivity.SHOP_LONGITUDE));
											intent.putExtra(
													"ADDRESS",
													map.get(MapActivity.SHOP_ADDRESS));
											intent.putExtra("PIN", map
													.get(MapActivity.SHOP_PIN));
											intent.putExtra("AREA", map
													.get(MapActivity.SHOP_AREA));
											intent.putExtra("CITY", map
													.get(MapActivity.SHOP_CITY));
											intent.putExtra(
													"STATE",
													map.get(MapActivity.SHOP_STATE));
											intent.putExtra(
													"DISTANCE",
													map.get(MapActivity.SHOP_DISTANCE));
											intent.putExtra(
													"URL",
													map.get(MapActivity.SHOP_IMAGE));
											startActivity(intent);
											break;
										}
									}
								}
							}
						});
				googleMap
						.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {

							@Override
							public void onCameraChange(
									CameraPosition cameraPosition) {
								/*
								 * System.out.println("DISTANCE " + isMoving);
								 * 
								 * 
								 * if (isMoving) {
								 * handler.removeCallbacks(runnable,null);
								 * //handler.postDelayed(runnable, 2000); } else
								 * { handler.postDelayed(runnable, 2000); }
								 */

								Log.d("Zoom", "Zoom: " + cameraPosition.zoom);
								if (!isFirstTme) {
									isFirstTme = false;
									if (cameraPosition.zoom > previousZoomLevel) {
										if (cameraPosition.zoom
												- previousZoomLevel >= 1) {
											isZooming = true;
											handler.postDelayed(runnable, 2000);
										} else {

										}

									} else {
										if (previousZoomLevel
												- cameraPosition.zoom >= 1) {
											isZooming = true;
											handler.postDelayed(runnable, 2000);
										} else {

										}
									}

									previousZoomLevel = cameraPosition.zoom;
								} else {
									isFirstTme = false;
								}
							}
						});

				// Check if we were successful in obtaining the map.
				if (googleMap != null) {
					try {
						Location mylocation = googleMap.getMyLocation();
						if (mylocation != null) {

							double latitude = mylocation.getLatitude();

							// Getting longitude of the current location
							double longitude = mylocation.getLongitude();

							Utility.setCurrentLatitude(
									MapActivity.this,
									Constants.SHAREDPREFERENCE_CURRENT_LATITUDE,
									String.valueOf(latitude));
							Utility.setCurrentLongitude(
									MapActivity.this,
									Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE,
									String.valueOf(longitude));
							
							
							Utility.setCurrentLatitude(MapActivity.this,
									Constants.SHAREDPREFERENCE_LAST_LATITUDE,
									String.valueOf(latitude));
							Utility.setCurrentLongitude(MapActivity.this,
									Constants.SHAREDPREFERENCE_LAST_LONGITUDE,
									String.valueOf(longitude));

							CameraPosition camPos = new CameraPosition.Builder()
									.target(new LatLng(latitude, longitude))
									.zoom(14).build();

							CameraUpdate camUpd3 = CameraUpdateFactory
									.newCameraPosition(camPos);

							googleMap.animateCamera(camUpd3);
							// Circle circle = googleMap
							// .addCircle(new CircleOptions()
							// .center(new LatLng(latitude,
							// longitude))
							// .radius(1000)
							// .strokeColor(
							// R.color.app_login_button_color)
							// .fillColor(
							// R.color.app_skyblue_color));
						} else {
							String lat = Utility
									.getCurrentLatitude(
											MapActivity.this,
											Constants.SHAREDPREFERENCE_CURRENT_LATITUDE);
							String lng = Utility
									.getCurrentLongitude(
											MapActivity.this,
											Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE);

							Utility.setCurrentLatitude(MapActivity.this,
									Constants.SHAREDPREFERENCE_LAST_LATITUDE,
									String.valueOf(lat));
							Utility.setCurrentLongitude(MapActivity.this,
									Constants.SHAREDPREFERENCE_LAST_LONGITUDE,
									String.valueOf(lng));

							if (lat != "" && lng != "") {
								CameraPosition camPos = new CameraPosition.Builder()
										.target(new LatLng(Double
												.parseDouble(lat), Double
												.parseDouble(lng))).zoom(14)
										.build();

								CameraUpdate camUpd3 = CameraUpdateFactory
										.newCameraPosition(camPos);

								googleMap.animateCamera(camUpd3);
							}
						}

					} catch (Exception e) {
					}

					boolean gps_enabled = false;
					boolean network_enabled = false;
					LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
					try {
						gps_enabled = lm
								.isProviderEnabled(LocationManager.GPS_PROVIDER);
					} catch (Exception ex) {
					}
					try {
						network_enabled = lm
								.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
					} catch (Exception ex) {
					}

					// don't start listeners if no provider is
					// enabled
					boolean hasGPS = getPackageManager().hasSystemFeature(
							PackageManager.FEATURE_LOCATION_GPS);
					WifiManager wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
					if (!(network_enabled && wifi.isWifiEnabled())
							&& (hasGPS && !gps_enabled)) {

						showTwoButtonDialog(
								MapActivity.this,
								"Shop Locator",
								"Turn on your location service on the phone to locate nearby Pay1 merchant.",
								Constants.DIALOG_CLOSE_LOCATION);
					} else if (gps_enabled || network_enabled) {
						textView_Title.setText("Locate cash topup store");
						if (gift && retailer) {
							showGift();
							VisibleRegion visibleRegion = googleMap
									.getProjection().getVisibleRegion();

							int r = (int) Constants
									.distanceFrom(
											visibleRegion.latLngBounds.northeast.latitude,
											visibleRegion.latLngBounds.northeast.longitude,
											visibleRegion.latLngBounds
													.getCenter().latitude,
											visibleRegion.latLngBounds
													.getCenter().longitude);

							new DealorLocatorTask()
									.execute(
											Utility.getCurrentLatitude(
													MapActivity.this,
													Constants.SHAREDPREFERENCE_CURRENT_LATITUDE),
											Utility.getCurrentLongitude(
													MapActivity.this,
													Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE),
											Utility.getMobileNumber(
													MapActivity.this,
													Constants.SHAREDPREFERENCE_MOBILE),
											String.valueOf(r));
							new FreeBiesTask()
									.execute(
											Utility.getCurrentLatitude(
													MapActivity.this,
													Constants.SHAREDPREFERENCE_CURRENT_LATITUDE),
											Utility.getCurrentLongitude(
													MapActivity.this,
													Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE));
						} else if (gift) {
							textView_Title.setText("Free Gifts");
							showGift();
							new FreeBiesTask()
									.execute(
											Utility.getCurrentLatitude(
													MapActivity.this,
													Constants.SHAREDPREFERENCE_CURRENT_LATITUDE),
											Utility.getCurrentLongitude(
													MapActivity.this,
													Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE));
						} else if (retailer) {
							textView_Title.setText("Locate cash topup store");
							VisibleRegion visibleRegion = googleMap
									.getProjection().getVisibleRegion();

							int r = (int) Constants
									.distanceFrom(
											visibleRegion.latLngBounds.northeast.latitude,
											visibleRegion.latLngBounds.northeast.longitude,
											visibleRegion.latLngBounds
													.getCenter().latitude,
											visibleRegion.latLngBounds
													.getCenter().longitude);

							/*
							 * new DealorLocatorTask() .execute(
							 * Utility.getCurrentLatitude( MapActivity.this,
							 * Constants.SHAREDPREFERENCE_CURRENT_LATITUDE),
							 * Utility.getCurrentLongitude( MapActivity.this,
							 * Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE),
							 * Utility.getMobileNumber( MapActivity.this,
							 * Constants
							 * .SHAREDPREFERENCE_MOBILE),String.valueOf(r));
							 */
						}
					} else {
						showTwoButtonDialog(
								MapActivity.this,
								"Shop Locator",
								"Turn on your location service on the phone to locate nearby Pay1 merchant.",
								Constants.DIALOG_CLOSE_LOCATION);
					}

				}
			}
			

			googleMap.setOnMyLocationButtonClickListener(new OnMyLocationButtonClickListener() {
                @Override
                public boolean onMyLocationButtonClick() {
                	showTwoButtonDialog(
							MapActivity.this,
							"Shop Locator",
							"Turn on your location service on the phone to locate nearby Pay1 merchant.",
							Constants.DIALOG_CLOSE_LOCATION);
                    return true;
                }
            });
			googleMap
					.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {

						Marker currentShown;

						public boolean onMarkerClick(Marker marker) {
							if (marker.equals(currentShown)) {
								marker.hideInfoWindow();
								currentShown = null;
							} else {
								marker.showInfoWindow();
								currentShown = marker;
							}
							return true;
						}
					});
			googleMap
					.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
						@Override
						public void onMapLoaded() {
							GPSTracker gps = new GPSTracker(MapActivity.this);
							//if (gps.canGetLocation()) { 
						
							// Log.e("TAG",
							// googleMap.getCameraPosition().target.toString());
							VisibleRegion visibleRegion = googleMap
									.getProjection().getVisibleRegion();
						/*	double lat = googleMap.getMyLocation()
									.getLatitude();
							double longi = googleMap.getMyLocation()
									.getLongitude();*/
							int r = (int) Constants
									.distanceFrom(
											visibleRegion.latLngBounds.northeast.latitude,
											visibleRegion.latLngBounds.northeast.longitude,
											visibleRegion.latLngBounds
													.getCenter().latitude,
											visibleRegion.latLngBounds
													.getCenter().longitude);

						// gps enabled} // return boolean true/false

								/*gps.getLatitude(); // returns latitude
								gps.getLongitude(); // returns longitude

								Utility.setCurrentLatitude(MapActivity.this,
										Constants.SHAREDPREFERENCE_CURRENT_LATITUDE,
										String.valueOf(gps.getLatitude()));
								Utility.setCurrentLongitude(MapActivity.this,
										Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE,
										String.valueOf(gps.getLongitude()));
								*/
							

							new DealorLocatorTask().execute(
									Utility.getCurrentLatitude(
											MapActivity.this,
											Constants.SHAREDPREFERENCE_CURRENT_LATITUDE),
									Utility.getCurrentLongitude(
											MapActivity.this,
											Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE),
									Utility.getMobileNumber(MapActivity.this,
											Constants.SHAREDPREFERENCE_MOBILE),
									String.valueOf(r));
							/*}else{
								Toast.makeText(MapActivity.this, "Please make sure your settings are on.", Toast.LENGTH_LONG).show();
							}*/
						}
					});

		} catch (Exception exception) {
		}
		overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}

	private void showNearShopDialog(final Context context) {
		// TODO Auto-generated method stub
		final Dialog dialog = new Dialog(MapActivity.this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.getWindow().setBackgroundDrawableResource(
				android.R.color.transparent);
		dialog.setContentView(R.layout.dialog_for_cash_topup);
		dialog.setCancelable(false);
		dialog.setCanceledOnTouchOutside(false);
		// dialog.setTitle(null);

		Typeface Reguler = Typeface.createFromAsset(context.getAssets(),
				"Roboto-Regular.ttf");

		Typeface Medium = Typeface.createFromAsset(context.getAssets(),
				"Roboto-Regular.ttf");

		// set the custom dialog components - text, image and button
		TextView textView_Title = (TextView) dialog
				.findViewById(R.id.textView_Title);
		textView_Title.setText("Recharge with Cash Near you");
		textView_Title.setTypeface(Reguler);

		TextView textView_Message = (TextView) dialog
				.findViewById(R.id.textView_Message);
		textView_Message.setTypeface(Reguler);
		textView_Title.setTypeface(Reguler);
		Button button_Ok = (Button) dialog.findViewById(R.id.button_Ok);
		button_Ok.setTypeface(Reguler);
		// if button is clicked, close the custom dialog
		final CheckBox checkBoxDonnShow = (CheckBox) dialog
				.findViewById(R.id.checkBoxDonnShow);
		button_Ok.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				if (checkBoxDonnShow.isChecked() == true) {
					Utility.saveshowAgain(context, false);
				}
				dialog.dismiss();
			}
		});

		dialog.show();
	}

	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView().build());
		// EasyTracker.getInstance(this).activityStart(this);
		easyTracker.send(MapBuilder.createAppView().build());
	}

	@Override
	public void onStop() {
		super.onStop();
		// EasyTracker.getInstance(this).activityStop(this);
	}

	@Override
	protected void onResume() {
		super.onResume();
		GPSTracker gps = new GPSTracker(MapActivity.this);
		if (gps.canGetLocation()) { 
		
		}else{
			//Toast.makeText(MapActivity.this, "Please make sure your settings are on.", Toast.LENGTH_LONG).show();
		}
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		googleMap.clear();
		markers.clear();
		deal_id.clear();
		shop_id.clear();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
	}

	private void showGift() {
		try {
			freeBiesDataSource.open();
			freeBiesLocationDataSource.open();

			List<FreeBies> freebies = freeBiesDataSource.getAllFreeBies();
			int len = freebies.size();
			if (len != 0) {

				for (FreeBies freebie : freebies) {

					if (!deal_id.containsValue(freebie.getFreebieDealID())) {

						List<FreeBiesLocation> locations = freeBiesLocationDataSource
								.getAllFreeBiesLocation(freebie
										.getFreebieOfferID());

						for (FreeBiesLocation location : locations) {

							StringBuffer sb = new StringBuffer();
							sb.append(location.getLocationAddress())
									.append(",\n")
									// .append("")
									// .append(", ")
									.append(location.getLocationCity())
									.append(", ")
									.append(location.getLocationState());
							// .append(" - ")
							// .append(location
							// .getLocationPin());

							double lat = location.getLocationLat(), lng = location
									.getLocationLng();
							LatLng latLng = new LatLng(lat, lng);
							MarkerOptions options = new MarkerOptions();
							options.position(latLng);
							options.title(freebie.getFreebieOfferName());
							options.snippet(freebie.getFreebieDealName() + "\n"
									+ sb.toString());
							options.draggable(false);
							// options.icon(BitmapDescriptorFactory
							// .defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
							options.icon(BitmapDescriptorFactory
									.fromResource(R.drawable.ic_shop_pin));

							/*
							 * SVG svg = SVGParser.getSVGFromResource(
							 * MapActivity.this.getResources(),
							 * R.raw.ic_shoplocator); // Drawable ic_rechargenow
							 * = // svg.createPictureDrawable(); PictureDrawable
							 * pictureDrawable = svg .createPictureDrawable();
							 * Bitmap bitmap = Bitmap.createBitmap(
							 * pictureDrawable.getIntrinsicWidth(),
							 * pictureDrawable.getIntrinsicHeight(),
							 * Config.ARGB_8888); Canvas canvas = new
							 * Canvas(bitmap);
							 * canvas.drawPicture(pictureDrawable.getPicture());
							 * Bitmap currentBitmap = bitmap;
							 * options.icon(BitmapDescriptorFactory
							 * .fromBitmap(currentBitmap));
							 */

							Marker marker = googleMap.addMarker(options);
							markers.put(marker.getId(), freebie.getFreebieURL());
							deal_id.put(marker.getId(),
									freebie.getFreebieDealID());

						}
					}
				}
			}
		} catch (SQLException exception) {
			// TODO: handle exception
			// Log.e("Er ", exception.getMessage());
		} catch (Exception e) {
			// TODO: handle exception
			// Log.e("Er ", e.getMessage());
		} finally {
			freeBiesDataSource.close();
			freeBiesLocationDataSource.close();
		}
	}

	private void showRetailer() {
		try {
			for (int i = 0; i < data.size(); i++) {
				WeakHashMap<String, String> map = new WeakHashMap<String, String>();
				map = data.get(i);

				String user_id = map.get(MapActivity.SHOP_USER_ID);
				if (!shop_id.containsValue(user_id)) {
					// String shop_mobile = map.get(MapActivity.SHOP_MOBILE);
					String shop_name = map.get(MapActivity.SHOP_NAME);
					// String shop_sale = map.get(MapActivity.SHOP_SALE);
					String latitude = map.get(MapActivity.SHOP_LATITUDE);
					String longitude = map.get(MapActivity.SHOP_LONGITUDE);

					String address = map.get(MapActivity.SHOP_ADDRESS);
					String pin = map.get(MapActivity.SHOP_PIN);
					String area_name = map.get(MapActivity.SHOP_AREA);
					String city_name = map.get(MapActivity.SHOP_CITY);
					String state_name = map.get(MapActivity.SHOP_STATE);
					String url = map.get(MapActivity.SHOP_IMAGE);
					// String distance = map.get(MapActivity.SHOP_DISTANCE);

					StringBuffer sb = new StringBuffer();
					sb.append(address).append(",\n").append(area_name)
							.append(", ").append(city_name).append(",\n")
							.append(state_name).append(" - ").append(pin);

					double lat = Double.parseDouble(latitude), lng = Double
							.parseDouble(longitude);
					LatLng latLng = new LatLng(lat, lng);
					MarkerOptions options = new MarkerOptions();
					options.position(latLng);
					options.title(shop_name);
					options.snippet(sb.toString());
					options.draggable(false);
					// options.icon(BitmapDescriptorFactory
					// .defaultMarker(BitmapDescriptorFactory.HUE_BLUE));

					options.icon(BitmapDescriptorFactory
							.fromResource(R.drawable.ic_shop_pin));

					/*
					 * SVG svg = SVGParser.getSVGFromResource(
					 * MapActivity.this.getResources(), R.raw.ic_shoplocator);
					 * // Drawable ic_rechargenow = //
					 * svg.createPictureDrawable(); PictureDrawable
					 * pictureDrawable = svg .createPictureDrawable(); Bitmap
					 * bitmap = Bitmap.createBitmap(
					 * pictureDrawable.getIntrinsicWidth(),
					 * pictureDrawable.getIntrinsicHeight(), Config.ARGB_8888);
					 * Canvas canvas = new Canvas(bitmap);
					 * canvas.drawPicture(pictureDrawable.getPicture()); Bitmap
					 * currentBitmap = bitmap;
					 * options.icon(BitmapDescriptorFactory
					 * .fromBitmap(currentBitmap));
					 */
					Marker marker = googleMap.addMarker(options);
					markers.put(marker.getId(), url);
					shop_id.put(marker.getId(), Long.parseLong(user_id));
				}
			}
		} catch (Exception e) {
		}
	}

	public class DealorLocatorTask extends AsyncTask<String, String, String>
			implements OnDismissListener {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			try {
				String response = RequestClass.getInstance()
						.readPay1B2BRequest(
								MapActivity.this,
								Constants.B2B_URL
										+ "method=getNearByRetailer&lat="
										+ params[0] + "&lng=" + params[1]
										+ "&mobile=" + params[2] + "&distance="
										+ params[3]);

				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			progressBar1.setVisibility(View.GONE);
			/*
			 * if (this.dialog.isShowing()) { this.dialog.dismiss(); }
			 */
			try {

				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONArray jsonArray = new JSONArray(replaced);
					JSONArray jsonArrayIn = jsonArray.getJSONArray(0);
					int len = jsonArrayIn.length();

					data.clear();
					
					//if(len>1){
					for (int i = 0; i < len; i++) {
						JSONObject jsonObject = jsonArrayIn.getJSONObject(i);

						String user_id = jsonObject.getJSONObject("t")
								.getString("user_id");
						if (!shop_id.containsValue(user_id)) {
							String shop_mobile = jsonObject.getJSONObject("t")
									.getString("mobile").toString().trim();
							String shop_name = jsonObject.getJSONObject("t")
									.getString("shopname").toString().trim();
							String shop_sale = jsonObject.getJSONObject("t")
									.getString("sale").toString().trim();
							String latitude = jsonObject.getJSONObject("t")
									.getString("latitude").toString().trim();
							String longitude = jsonObject.getJSONObject("t")
									.getString("longitude").toString().trim();

							String address = jsonObject.getJSONObject("t")
									.getString("address").toString().trim();
							String pin = jsonObject.getJSONObject("t")
									.getString("pin").toString().trim();
							String area_name = jsonObject.getJSONObject("t")
									.getString("area_name").toString().trim();
							String city_name = jsonObject.getJSONObject("t")
									.getString("city_name").toString().trim();
							String state_name = jsonObject.getJSONObject("t")
									.getString("state_name").toString().trim();
							String distance = jsonObject.getJSONObject("t")
									.getString("D").toString().trim();
							String url = jsonObject.getJSONObject("t")
									.getString("imagepath").toString().trim();

							StringBuffer sb = new StringBuffer();
							sb.append(address).append(",\n").append(area_name)
									.append(", ").append(city_name)
									.append(",\n").append(state_name)
									.append(" - ").append(pin);

							WeakHashMap<String, String> map = new WeakHashMap<String, String>();
							map.put(SHOP_USER_ID, user_id);
							map.put(SHOP_MOBILE, shop_mobile);
							map.put(SHOP_NAME, shop_name);
							map.put(SHOP_SALE, shop_sale);
							map.put(SHOP_LATITUDE, latitude);
							map.put(SHOP_LONGITUDE, longitude);
							map.put(SHOP_ADDRESS, address);
							map.put(SHOP_PIN, pin);
							map.put(SHOP_AREA, area_name);
							map.put(SHOP_CITY, city_name);
							map.put(SHOP_STATE, state_name);
							map.put(SHOP_DISTANCE, distance);
							map.put(SHOP_IMAGE, url);
							data.add(map);

							double lat = Double.parseDouble(latitude), lng = Double
									.parseDouble(longitude);
							LatLng latLng = new LatLng(lat, lng);
							MarkerOptions options = new MarkerOptions();
							options.position(latLng);
							options.title(shop_name);
							options.snippet(sb.toString());
							options.draggable(false);
							// options.icon(BitmapDescriptorFactory
							// .defaultMarker(BitmapDescriptorFactory.HUE_BLUE));

							options.icon(BitmapDescriptorFactory
									.fromResource(R.drawable.ic_shop_pin));

							/*
							 * SVG svg = SVGParser.getSVGFromResource(
							 * MapActivity.this.getResources(),
							 * R.raw.ic_shoplocator); // Drawable ic_rechargenow
							 * = // svg.createPictureDrawable(); PictureDrawable
							 * pictureDrawable = svg .createPictureDrawable();
							 * Bitmap bitmap = Bitmap.createBitmap(
							 * pictureDrawable.getIntrinsicWidth(),
							 * pictureDrawable.getIntrinsicHeight(),
							 * Config.ARGB_8888); Canvas canvas = new
							 * Canvas(bitmap);
							 * canvas.drawPicture(pictureDrawable.getPicture());
							 * Bitmap currentBitmap = bitmap;
							 * options.icon(BitmapDescriptorFactory
							 * .fromBitmap(currentBitmap));
							 */

							/*
							 * options.icon(BitmapDescriptorFactory
							 * .fromResource(R.drawable.icon_retailer));
							 */
							Marker marker = googleMap.addMarker(options);
							markers.put(marker.getId(), url);
							shop_id.put(marker.getId(), Long.parseLong(user_id));
						}
					}
				/*}else{
					Constants.showOneButtonDialog(MapActivity.this, "Pay1 Store", "No ", Constants.DIALOG_CLOSE_MAP_STORE);
				}*/
				} else {
					Intent intent = new Intent(MapActivity.this,
							ConnectivityErrorActivity.class);
					startActivity(intent);
				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonDialog(MapActivity.this, TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonDialog(MapActivity.this, TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			}
			isMoving = false;
		}

		@Override
		protected void onPreExecute() {
			progressBar1.setVisibility(View.VISIBLE);
			dialog = new MyProgressDialog(MapActivity.this);
			// // this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							DealorLocatorTask.this.cancel(true);
						}
					});
			// this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			progressBar1.setVisibility(View.GONE);
			// TODO Auto-generated method stub
			DealorLocatorTask.this.cancel(true);
			dialog.cancel();
		}

	}

	public class DealorLocatorTaskNew extends AsyncTask<String, String, String>
			implements OnDismissListener {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			try {
				String response = RequestClass.getInstance()
						.readPay1B2BRequest(
								MapActivity.this,
								Constants.B2B_URL
										+ "method=getNearByRetailer&lat="
										+ params[0] + "&lng=" + params[1]
										+ "&mobile=" + params[2] + "&distance="
										+ params[3]);

				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			progressBar1.setVisibility(View.GONE);
			/*
			 * if (this.dialog.isShowing()) { this.dialog.dismiss(); }
			 */
			try {

				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONArray jsonArray = new JSONArray(replaced);
					JSONArray jsonArrayIn = jsonArray.getJSONArray(0);
					int len = jsonArrayIn.length();

					if (len > 0) {
						data.clear();
						googleMap.clear();
						markers.clear();
						deal_id.clear();
						shop_id.clear();
					}
					for (int i = 0; i < len; i++) {
						JSONObject jsonObject = jsonArrayIn.getJSONObject(i);

						String user_id = jsonObject.getJSONObject("t")
								.getString("user_id");
						if (!shop_id.containsValue(user_id)) {
							String shop_mobile = jsonObject.getJSONObject("t")
									.getString("mobile").toString().trim();
							String shop_name = jsonObject.getJSONObject("t")
									.getString("shopname").toString().trim();
							String shop_sale = jsonObject.getJSONObject("t")
									.getString("sale").toString().trim();
							String latitude = jsonObject.getJSONObject("t")
									.getString("latitude").toString().trim();
							String longitude = jsonObject.getJSONObject("t")
									.getString("longitude").toString().trim();

							String address = jsonObject.getJSONObject("t")
									.getString("address").toString().trim();
							String pin = jsonObject.getJSONObject("t")
									.getString("pin").toString().trim();
							String area_name = jsonObject.getJSONObject("t")
									.getString("area_name").toString().trim();
							String city_name = jsonObject.getJSONObject("t")
									.getString("city_name").toString().trim();
							String state_name = jsonObject.getJSONObject("t")
									.getString("state_name").toString().trim();
							String distance = jsonObject.getJSONObject("t")
									.getString("D").toString().trim();
							String url = jsonObject.getJSONObject("t")
									.getString("imagepath").toString().trim();

							StringBuffer sb = new StringBuffer();
							sb.append(address).append(",\n").append(area_name)
									.append(", ").append(city_name)
									.append(",\n").append(state_name)
									.append(" - ").append(pin);

							WeakHashMap<String, String> map = new WeakHashMap<String, String>();
							map.put(SHOP_USER_ID, user_id);
							map.put(SHOP_MOBILE, shop_mobile);
							map.put(SHOP_NAME, shop_name);
							map.put(SHOP_SALE, shop_sale);
							map.put(SHOP_LATITUDE, latitude);
							map.put(SHOP_LONGITUDE, longitude);
							map.put(SHOP_ADDRESS, address);
							map.put(SHOP_PIN, pin);
							map.put(SHOP_AREA, area_name);
							map.put(SHOP_CITY, city_name);
							map.put(SHOP_STATE, state_name);
							map.put(SHOP_DISTANCE, distance);
							map.put(SHOP_IMAGE, url);
							data.add(map);

							double lat = Double.parseDouble(latitude), lng = Double
									.parseDouble(longitude);
							LatLng latLng = new LatLng(lat, lng);
							MarkerOptions options = new MarkerOptions();
							options.position(latLng);
							options.title(shop_name);
							options.snippet(sb.toString());
							options.draggable(false);
							// options.icon(BitmapDescriptorFactory
							// .defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
							options.icon(BitmapDescriptorFactory
									.fromResource(R.drawable.ic_shop_pin));

							/*
							 * SVG svg = SVGParser.getSVGFromResource(
							 * MapActivity.this.getResources(),
							 * R.raw.ic_shoplocator); // Drawable ic_rechargenow
							 * = // svg.createPictureDrawable(); PictureDrawable
							 * pictureDrawable = svg .createPictureDrawable();
							 * Bitmap bitmap = Bitmap.createBitmap(
							 * pictureDrawable.getIntrinsicWidth(),
							 * pictureDrawable.getIntrinsicHeight(),
							 * Config.ARGB_8888); Canvas canvas = new
							 * Canvas(bitmap);
							 * canvas.drawPicture(pictureDrawable.getPicture());
							 * Bitmap currentBitmap = bitmap;
							 * options.icon(BitmapDescriptorFactory
							 * .fromBitmap(currentBitmap));
							 */

							/*
							 * options.icon(BitmapDescriptorFactory
							 * .fromResource(R.drawable.icon_retailer));
							 */

							Marker marker = googleMap.addMarker(options);
							markers.put(marker.getId(), url);
							shop_id.put(marker.getId(), Long.parseLong(user_id));
						}
					}

				} else {
					Intent intent = new Intent(MapActivity.this,
							ConnectivityErrorActivity.class);
					startActivity(intent);
				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonDialog(MapActivity.this, TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonDialog(MapActivity.this, TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			}
			isMoving = false;
		}

		@Override
		protected void onPreExecute() { progressBar1.setVisibility(View.VISIBLE);
			dialog = new MyProgressDialog(MapActivity.this);
			// // this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							DealorLocatorTaskNew.this.cancel(true);
						}
					});
			// this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub 
			progressBar1.setVisibility(View.GONE);
			DealorLocatorTaskNew.this.cancel(true);
			dialog.cancel();
		}

	}

	public void showTwoButtonDialog(final Context context, String Title,
			String message, final int Type) {
		try {

			final Dialog dialog = new Dialog(context);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			// dialog.getWindow().setBackgroundDrawableResource(
			// android.R.color.transparent);
			dialog.setContentView(R.layout.dialog_two_button);
			dialog.setCancelable(false);
			dialog.setCanceledOnTouchOutside(false);
			// dialog.setTitle(null);

			Typeface Reguler = Typeface.createFromAsset(context.getAssets(),
					"Roboto-Regular.ttf");

			Typeface Medium = Typeface.createFromAsset(context.getAssets(),
					"Roboto-Regular.ttf");

			// set the custom dialog components - text, image and button
			TextView textView_Title = (TextView) dialog
					.findViewById(R.id.textView_Title);
			textView_Title.setText(Title);
			textView_Title.setTypeface(Reguler);

			TextView textView_Message = (TextView) dialog
					.findViewById(R.id.textView_Message);
			textView_Message.setText(message);
			textView_Message.setTypeface(Medium);

			Button button_Ok = (Button) dialog.findViewById(R.id.button_Ok);
			button_Ok.setTypeface(Reguler);
			// if button is clicked, close the custom dialog
			button_Ok.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();

					Intent viewIntent = new Intent(
							Settings.ACTION_LOCATION_SOURCE_SETTINGS);
					context.startActivity(viewIntent);
				}
			});

			Button button_Cancel = (Button) dialog
					.findViewById(R.id.button_Cancel);
			button_Cancel.setTypeface(Reguler);
			// if button is clicked, close the custom dialog
			button_Cancel.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					
					dialog.dismiss();
					/*
					dialog.dismiss();

					boolean gift = getIntent().getBooleanExtra("SHOW_GIFT",
							false);
					boolean retailer = getIntent().getBooleanExtra(
							"SHOW_RETAILER", false);

					if (gift && retailer) {
						showGift();
						new DealorLocatorTask().execute(
								Utility.getCurrentLatitude(
										MapActivity.this,
										Constants.SHAREDPREFERENCE_CURRENT_LATITUDE),
								Utility.getCurrentLongitude(
										MapActivity.this,
										Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE),
								Utility.getMobileNumber(MapActivity.this,
										Constants.SHAREDPREFERENCE_MOBILE));
					} else if (gift) {
						showGift();
					} else if (retailer) {
						new DealorLocatorTask().execute(
								Utility.getCurrentLatitude(
										MapActivity.this,
										Constants.SHAREDPREFERENCE_CURRENT_LATITUDE),
								Utility.getCurrentLongitude(
										MapActivity.this,
										Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE),
								Utility.getMobileNumber(MapActivity.this,
										Constants.SHAREDPREFERENCE_MOBILE));
					}
				*/}
			});

			button_Ok.setText("Ok");
			button_Cancel.setText("Not now");

			dialog.show();

		} catch (Exception exception) {
		}
	}

	private class CustomGiftInfoWindowAdapter implements InfoWindowAdapter {

		private View view;
		String url = null;
		private Typeface Reguler, Medium;

		public CustomGiftInfoWindowAdapter() {

			view = getLayoutInflater().inflate(R.layout.custom_info_window,
					null);
			Reguler = Typeface.createFromAsset(getAssets(),
					"Roboto-Regular.ttf");

			Medium = Typeface
					.createFromAsset(getAssets(), "Roboto-Regular.ttf");
		}

		@Override
		public View getInfoContents(Marker marker) {

			if (marker != null && marker.isInfoWindowShown()) {
				marker.hideInfoWindow();
				marker.showInfoWindow();
			}
			return null;
		}

		@SuppressLint("NewApi")
		@Override
		public View getInfoWindow(final Marker marker) {
			url = null;
			if (marker.getId() != null && markers != null && markers.size() > 0) {
				if (markers.get(marker.getId()) != null) {
					url = markers.get(marker.getId());
				}
			}
			final ImageView image = ((ImageView) view
					.findViewById(R.id.imageView_Icon));

			if (url != null && !url.equalsIgnoreCase("null")
					&& !url.equalsIgnoreCase("")) {
				MapActivity.this.runOnUiThread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						//imageLoader.getDealImage(url, image);
						image.setVisibility(View.VISIBLE);
					/*	try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}*/
						//imageLoader.getDealImage(url, image);
						
						/*ImageLoader imageLoader = new ImageLoader(MapActivity.this);
						imageLoader.getDealImage(url, image);*/
						
						try {
							Picasso.with(MapActivity.this)
									.load(url)
									.placeholder(
											R.drawable.deal_default)
									.transform(
											new RoundedTransformation(
													0, 0))
									.into(image, new MarkerCallback(marker));
						} catch (Exception e) {
							System.out.println(e.getMessage());
						}
						
						getInfoContents(marker);
					}
				});

			} else {

				image.setImageDrawable(SVGParser.getSVGFromResource(
						getResources(), R.raw.ic_gift_coin)
						.createPictureDrawable());
				image.setVisibility(View.GONE);
			}

			final String title = marker.getTitle();
			final TextView titleUi = ((TextView) view.findViewById(R.id.title));
			titleUi.setTypeface(Medium);
			if (title != null) {
				titleUi.setText(Html.fromHtml("<font color='1651ff'><u>"
						+ title + "</u></font>"));

			} else {
				titleUi.setText("");
			}

			final String snippet = marker.getSnippet();
			final TextView snippetUi = ((TextView) view
					.findViewById(R.id.snippet));
			snippetUi.setTypeface(Reguler);
			if (snippet != null) {
				snippetUi.setText(snippet);
			} else {
				snippetUi.setText("");
			}

			isInfoWindowClicked = true;
			return view;
		}
	}

	public class FreeBiesTask extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... params) {
			try {

				String time = Utility.getSharedPreferences(MapActivity.this,
						Constants.LAST_GIFT_UPDATED) == null ? "" : Utility
						.getSharedPreferences(MapActivity.this,
								Constants.LAST_GIFT_UPDATED);

				if (time != "") {
					try {
						DateFormat dateFormat = new SimpleDateFormat(
								"yyyy-MM-dd HH:mm:ss");
						Date date = new Date(Long.parseLong(time));
						time = dateFormat.format(date);
					} catch (Exception e) {
					}
				}
				String response = RequestClass
						.getInstance()
						.readPay1B2CRequest(
								MapActivity.this,
								Constants.B2C_URL
										+ "GetupdatedDeal/?latitude="
										+ params[0]
										+ "&longitude="
										+ params[1]
										+ "&user_mobile="
										+ Utility
												.getMobileNumber(
														MapActivity.this,
														Constants.SHAREDPREFERENCE_MOBILE)
										+ "&updatedTime="
										+ URLEncoder.encode(time, "utf-8")
										+ "&api_version=3&next=-1");

				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			try {
				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");
					System.out.println(replaced);
					JSONObject jsonObject = new JSONObject(replaced);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {

						try {

							freeBiesDataSource.open();
							freeBiesLocationDataSource.open();

							JSONObject jsonObjectDesc = jsonObject
									.getJSONObject("description");
							Utility.setOrder(MapActivity.this,
									Constants.SHAREDPREFERENCE_GIFT,
									jsonObjectDesc.getString("claimedGifts"));
							Utility.setOrder(MapActivity.this,
									Constants.SHAREDPREFERENCE_GIFT_EXPIRED,
									jsonObjectDesc.getString("ExpiredOffers"));
							try {
								freeBiesDataSource
										.deleteFreeBies(jsonObjectDesc
												.getString("ExpiredOffers"));
								freeBiesLocationDataSource
										.deleteFreeBiesLocation(jsonObjectDesc
												.getString("ExpiredOffers"));
							} catch (Exception e) {

							}

							JSONArray jsonArrayAlldeals = jsonObjectDesc
									.getJSONArray("Alldeals");
							long timestamp = System.currentTimeMillis();

							freeBiesDataSource.createFreeBies(
									jsonArrayAlldeals, timestamp);
							// for (int i = 0; i < jsonArrayAlldeals.length();
							// i++) {
							//
							// String deal_id = jsonArrayAlldeals
							// .getJSONObject(i).getString("id");
							// String offer_id = jsonArrayAlldeals
							// .getJSONObject(i).getString("of_id");
							// try {
							// freeBiesDataSource.deleteFreeBies(Integer
							// .parseInt(deal_id));
							// freeBiesLocationDataSource
							// .deleteFreeBiesLocation(Integer
							// .parseInt(offer_id));
							// } catch (Exception e) {
							// continue;
							// }
							//
							// String deal_name = jsonArrayAlldeals
							// .getJSONObject(i).getString("name");
							// String url = jsonArrayAlldeals.getJSONObject(i)
							// .getString("i_url");
							// String catagory = jsonArrayAlldeals
							// .getJSONObject(i).getString("cat");
							// String catagory_id = jsonArrayAlldeals
							// .getJSONObject(i).getString("cat_id");
							// String min_amt = jsonArrayAlldeals
							// .getJSONObject(i).getString("min");
							// String offer_name = jsonArrayAlldeals
							// .getJSONObject(i).getString("of_name");
							// String validity = jsonArrayAlldeals
							// .getJSONObject(i).getString("valid");
							// String desc = jsonArrayAlldeals
							// .getJSONObject(i).getString("desc");
							// String offer_lat = "0", offer_lng = "0",
							// offer_add = "", offer_area = "", offer_city = "",
							// offer_state = "";
							//
							// JSONArray jsonArrayLocation = jsonArrayAlldeals
							// .getJSONObject(i).getJSONArray("locs");
							//
							// for (int j = 0; j < jsonArrayLocation.length();
							// j++) {
							// JSONObject jsonObjectInner = jsonArrayLocation
							// .getJSONObject(j);
							// WeakHashMap<String, String> map1 = new
							// WeakHashMap<String, String>();
							//
							// String loc_lat = jsonObjectInner
							// .getString("lat");
							// String loc_lng = jsonObjectInner
							// .getString("lng");
							// String loc_add = jsonObjectInner
							// .getString("addr");
							// String loc_area = jsonObjectInner
							// .getString("area");
							// String loc_city = jsonObjectInner
							// .getString("city");
							// String loc_state = jsonObjectInner
							// .getString("state");
							//
							// if (j == 0) {
							// offer_lat = loc_lat;
							// offer_lng = loc_lng;
							// offer_add = loc_add;
							// offer_area = loc_area;
							// offer_city = loc_city;
							// offer_state = loc_state;
							// }
							//
							// freeBiesLocationDataSource
							// .createFreeBiesLocation(
							// Integer.parseInt(offer_id),
							// Double.parseDouble(loc_lat),
							// Double.parseDouble(loc_lng),
							// loc_add, loc_area,
							// loc_city, loc_state, 0, 0,
							// timestamp);
							// }
							// freeBiesDataSource.createFreeBies(
							// Integer.parseInt(deal_id), deal_name,
							// url, catagory,
							// Integer.parseInt(catagory_id),
							// Double.parseDouble(offer_lat),
							// Double.parseDouble(offer_lng),
							// offer_add, offer_area, offer_city,
							// offer_state, 0,
							// jsonArrayLocation.length(),
							// Integer.parseInt(offer_id), offer_name,
							// validity, desc,
							// Integer.parseInt(min_amt), timestamp);
							// }

							Utility.setSharedPreferences(MapActivity.this,
									Constants.LAST_GIFT_UPDATED,
									String.valueOf(System.currentTimeMillis()));
						} catch (SQLException exception) {
							// TODO: handle exception
							// Log.e("Er ", exception.getMessage());
						} catch (Exception e) {
							// TODO: handle exception
							// Log.e("Er ", e.getMessage());
						} finally {
							freeBiesDataSource.close();
							freeBiesLocationDataSource.close();
						}

						googleMap.clear();
						deal_id.clear();
						shop_id.clear();
						markers.clear();
						showGift();
						showRetailer();
					}
				}
			} catch (JSONException e) {
				// e.printStackTrace();

			} catch (Exception e) {
				// TODO: handle exception
			}
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

	}

	@Override
	public void onMapReady(GoogleMap arg0) {
		// TODO Auto-generated method stub
		VisibleRegion visibleRegion = arg0.getProjection().getVisibleRegion();

		int r = (int) Constants.distanceFrom(
				visibleRegion.latLngBounds.northeast.latitude,
				visibleRegion.latLngBounds.northeast.longitude,
				visibleRegion.latLngBounds.getCenter().latitude,
				visibleRegion.latLngBounds.getCenter().longitude);

		new DealorLocatorTask().execute(Utility.getCurrentLatitude(
				MapActivity.this, Constants.SHAREDPREFERENCE_CURRENT_LATITUDE),
				Utility.getCurrentLongitude(MapActivity.this,
						Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE), Utility
						.getMobileNumber(MapActivity.this,
								Constants.SHAREDPREFERENCE_MOBILE), String
						.valueOf(r));
	}

	Runnable runnable = new Runnable() {
		public void run() {
			if (dealorLocatorTaskNew != null) {

				if (dealorLocatorTaskNew.getStatus() == AsyncTask.Status.PENDING) {
					// My AsyncTask has not
					// started yet
					isMoving = true;
				}

				if (dealorLocatorTaskNew.getStatus() == AsyncTask.Status.RUNNING) {
					// My AsyncTask is
					// currently doing work
					// in doInBackground()
					isMoving = true;
				}

				if (dealorLocatorTaskNew.getStatus() == AsyncTask.Status.FINISHED) {
					// My AsyncTask is done
					// and onPostExecute was
					// called
					isMoving = false;
				}
			}
			if (!isMoving)
				if (!isInfoWindowClicked)

					isMoving = true;
			// Listener of zooming;
			// int zoomLevel = (int)
			// cameraPosition.zoom;
			VisibleRegion visibleRegion = googleMap.getProjection()
					.getVisibleRegion();
			// LatLng nearLeft =
			// visibleRegion.nearLeft;
			// LatLng nearRight =
			// visibleRegion.nearRight;
			LatLng farLeft = visibleRegion.farLeft;
			// LatLng farRight =
			// visibleRegion.farRight;
			// double dist_w =
			// Constants.distanceFrom(
			// nearLeft.latitude,
			// nearLeft.longitude,
			// nearRight.latitude,
			// nearRight.longitude);
			// double dist_h =
			// Constants.distanceFrom(
			// farLeft.latitude,
			// farLeft.longitude,
			// farRight.latitude,
			// farRight.longitude);
			// System.out.println("DISTANCE: "
			// + "DISTANCE WIDTH: " +
			// dist_w
			// + " DISTANCE HEIGHT: " +
			// dist_h);
			// System.out.println("DISTANCE cal width "
			// + (256 * Math.pow(2,
			// zoomLevel)));

			String lat = Utility.getCurrentLatitude(MapActivity.this,
					Constants.SHAREDPREFERENCE_LAST_LATITUDE);
			String lng = Utility.getCurrentLongitude(MapActivity.this,
					Constants.SHAREDPREFERENCE_LAST_LONGITUDE);

			double c_lat = visibleRegion.latLngBounds.getCenter().latitude;
			double c_lng = visibleRegion.latLngBounds.getCenter().longitude;
			Utility.setCurrentLatitude(MapActivity.this,
					Constants.SHAREDPREFERENCE_LAST_LATITUDE,
					String.valueOf(c_lat));
			Utility.setCurrentLongitude(MapActivity.this,
					Constants.SHAREDPREFERENCE_LAST_LONGITUDE,
					String.valueOf(c_lng));

			if (lat != "" && lng != "") {

				float d = (float) Constants.distanceFrom(
						Double.parseDouble(lat), Double.parseDouble(lng),
						visibleRegion.latLngBounds.getCenter().latitude,
						visibleRegion.latLngBounds.getCenter().longitude);
				System.out.println("DISTANCE " + d + " Cam Pos "
						+ googleMap.getCameraPosition());

				float r = (float) Constants.distanceFrom(
						visibleRegion.latLngBounds.northeast.latitude,
						visibleRegion.latLngBounds.northeast.longitude,
						visibleRegion.latLngBounds.getCenter().latitude,
						visibleRegion.latLngBounds.getCenter().longitude);
				String rad = String.valueOf(r);
				if (((d > (r / 2)) && (r > 0)) || isZooming) {
					dealorLocatorTaskNew = new DealorLocatorTaskNew();
					dealorLocatorTaskNew.execute(String.valueOf(c_lat), String
							.valueOf(c_lng), Utility
							.getMobileNumber(MapActivity.this,
									Constants.SHAREDPREFERENCE_MOBILE), String
							.valueOf(rad));

				} else
					isMoving = false;
			}

		}
	};
	
	
	public class MarkerCallback implements Callback {
	    Marker marker=null;

	    MarkerCallback(Marker marker) {
	      this.marker=marker;

	    }

	    @Override
	    public void onError() {
	      Log.e(getClass().getSimpleName(), "Error loading thumbnail!");
	    }

	    @Override
	    public void onSuccess() {
	      if (marker != null && marker.isInfoWindowShown()) {
	        marker.hideInfoWindow();
	        marker.showInfoWindow();
	      }
	    }
	}

}