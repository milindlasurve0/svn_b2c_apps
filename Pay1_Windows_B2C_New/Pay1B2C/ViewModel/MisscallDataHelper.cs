﻿using Pay1B2C.Model;
using SQLite;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pay1B2C.ViewModel
    {
     class MisscallDataHelper
        {
        SQLiteConnection dbConn;

        //Create Tabble 
        public async Task<bool> onCreate(string DB_PATH)
            {
            try
                {
                if(!CheckFileExists(App.PAY1_DB_PATH).Result)
                    {
                    using(dbConn = new SQLiteConnection(App.PAY1_DB_PATH))
                        {
                        dbConn.CreateTable<QuickPayAllDataTable>();
                        }
                    }
                return true;
                }
            catch
                {
                return false;
                }
            }
        private async Task<bool> CheckFileExists(string fileName)
            {
            try
                {
                var store = await Windows.Storage.ApplicationData.Current.LocalFolder.GetFileAsync(fileName);
                return true;
                }
            catch
                {
                return false;
                }
            }

        // Retrieve the specific contact from the database. 
        public QuickPayAllDataTable ReadMisscall()
            {
            using(var dbConn = new SQLiteConnection(App.PAY1_DB_PATH))
                {
                var existingconact = dbConn.Query<QuickPayAllDataTable>("SELECT * from QuickPayAllDataTable where flag = 1 or flag = 3 and transaction_flag='1'").FirstOrDefault();
                return existingconact;
                }
            }
        // Retrieve the all contact list from the database. 
        public ObservableCollection<QuickPayAllDataTable> ReadQuickPayAllDataTable()
            {
            using(var dbConn = new SQLiteConnection(App.PAY1_DB_PATH))
                {
                List<QuickPayAllDataTable> myCollection = dbConn.Query<QuickPayAllDataTable>("SELECT * from QuickPayAllDataTable where flag = 1 AND transaction_flag='1' ORDER BY datetime DESC");
                //List<QuickPayAllDataTable> myCollection  = dbConn.Table<QuickPayAllDataTable>().ToList<QuickPayAllDataTable>();
                ObservableCollection<QuickPayAllDataTable> QuickPayAllDataTableList = new ObservableCollection<QuickPayAllDataTable>(myCollection);
                return QuickPayAllDataTableList;
                }
            }


        public ObservableCollection<QuickPayAllDataTable> ReadQuickPayAllDataTableForRecharge()
            {
            using(var dbConn = new SQLiteConnection(App.PAY1_DB_PATH))
                {
                List<QuickPayAllDataTable> myCollection  = dbConn.Table<QuickPayAllDataTable>().ToList<QuickPayAllDataTable>();
                ObservableCollection<QuickPayAllDataTable> QuickPayAllDataTableList = new ObservableCollection<QuickPayAllDataTable>(myCollection);
                return QuickPayAllDataTableList;
                }
            }

        //Update existing conatct 
      /*  public void UpdateMissCall(QuickPayAllDataTable contact)
            {
           using(var dbConn = new SQLiteConnection(App.DB_PATH))
                {
                var existingconact = dbConn.Query<QuickPayAllDataTable>("select * from QuickPayAllDataTable where Id =" + contact.Id).FirstOrDefault();
                if(existingconact != null)
                    {
                    existingconact.Name = contact.Name;
                    existingconact.PhoneNumber = contact.PhoneNumber;
                    existingconact.CreationDate = contact.CreationDate;
                    dbConn.RunInTransaction(() =>
                    {
                        dbConn.Update(existingconact);
                    });
                    }
                }
            }
         */


        public void UpdateMissCall(String valueFlag, String id,String phoneNumber)
            {
            using(var dbConn = new SQLiteConnection(App.PAY1_DB_PATH))
                {
                try
                    {
                    var existingconact = dbConn.Query<QuickPayAllDataTable>("UPDATE QuickPayAllDataTable SET delegate_flag='"+valueFlag+"',missed_number='"+phoneNumber+"' where id='"+id+"'");
                    System.Diagnostics.Debug.WriteLine(existingconact+"   1122344545");
                    }
                catch(SQLiteException seq)
                    {
                    }
                }
            }


        // Insert the new contact in the QuickPayAllDataTable table. 
        public void Insert(QuickPayAllDataTable newcontact)
            {
            using(var dbConn = new SQLiteConnection(App.PAY1_DB_PATH))
                {
                dbConn.RunInTransaction(() =>
                {
                    dbConn.Insert(newcontact);
                });
                }
            }

        //Delete specific contact 
        public void DeleteMisscallById(String Id)
            {
            using(var dbConn = new SQLiteConnection(App.PAY1_DB_PATH))
                {
                var existingconact = dbConn.Query<QuickPayAllDataTable>("select * from QuickPayAllDataTable where id =" + Id).FirstOrDefault();
                if(existingconact != null)
                    {
                    dbConn.RunInTransaction(() =>
                    {
                        dbConn.Delete(existingconact);
                    });
                    }
                }
            }
        //Delete all contactlist or delete QuickPayAllDataTable table 
        public void DeleteAllMissCall()
            {
            using(var dbConn = new SQLiteConnection(App.PAY1_DB_PATH))
                {
                //dbConn.RunInTransaction(() => 
                //   { 
                dbConn.DropTable<QuickPayAllDataTable>();
                dbConn.CreateTable<QuickPayAllDataTable>();
                dbConn.Dispose();
                dbConn.Close();
                //}); 
                }
            }
        }

    }
