﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NewPay1B2C.Model
{
    class Plans
    {
        public Plans()
        {
        }
        public Plans(int planAmount1, string planOpertorID1, string planOperatorName1, string planCircleID1, string planCircleName1, string planType1, string planValidity1, string planDescription1, DateTime planUptateTime1)
        {


            planAmount = planAmount1;
            planOpertorID = planOpertorID1;
            planOperatorName = planOperatorName1;
            planCircleID = planCircleID1;
            planCircleName = planCircleName1;
            planType = planType1;
            planValidity = planValidity1;
            planDescription = planDescription1;
            planUptateTime = planUptateTime1;

        }



        int _planAmount;
        string _planOpertorID;
        string _planOperatorName;
        string _planCircleID;
        string _planCircleName;
        string _planType;
        string _planValidity;
        string _planDescription;
        DateTime _planUptateTime;



        public int planAmount
        {
            get { return _planAmount; }
            set { _planAmount = value; }
        }

        public string planOpertorID
        {
            get { return _planOpertorID; }
            set { _planOpertorID = value; }
        }

        public string planOperatorName
        {
            get { return _planOperatorName; }
            set { _planOperatorName = value; }
        }
        public string planCircleID
        {
            get { return _planCircleID; }
            set { _planCircleID = value; }
        }

        public string planCircleName
        {
            get { return _planCircleName; }
            set { _planCircleName = value; }
        }
        public string planType
        {
            get { return _planType; }
            set { _planType = value; }
        }
        public string planValidity
        {
            get { return _planValidity; }
            set { _planValidity = value; }
        }

        public string planDescription
        {
            get { return _planDescription; }
            set { _planDescription = value; }
        }
        public DateTime planUptateTime
        {
            get { return _planUptateTime; }
            set { _planUptateTime = value; }
        }


    }
}
