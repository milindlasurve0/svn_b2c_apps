//
//  GiftDetailCommonView.m
//  Pay1
//
//  Created by Annapurna on 23/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "GiftDetailCommonView.h"
#import "GiftLocationList.h"
#import "Constant.h"
@implementation GiftDetailCommonView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (id)initWithFrame:(CGRect)frame
{
    
    dataArray = [[NSMutableArray alloc]init];
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self awakeFromNib];
    }
    return self;
}

-(void)awakeFromNib {
    
    tv.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

    
    UIView* myView = [[[NSBundle mainBundle] loadNibNamed:@"GiftDetailCommonView" owner:self options:nil] objectAtIndex:0];
    myView.backgroundColor = [UIColor colorWithRed:239/255.0 green:239/255.0 blue:239/255.0 alpha:1];
    [self addSubview:myView];
    
}

-(IBAction)crossClicked:(id)sender{
    if(_onCrossClick)
        _onCrossClick();
}
-(void)showCommonViewForGiftDetails:(NSMutableArray *)array fromView:(BOOL)fromView rowID:(NSInteger)rowID shopName:(NSString *)shopName{
  //  NSLog(@"#array :%@",array);

 
    headerLabel.text = shopName;
    isShow = fromView;
    
    if(isShow == FALSE){
    GiftLocationList *locList = [array objectAtIndex:rowID];
    NSArray *subStrings = [locList.dealerContract componentsSeparatedByString:@","]; //or rather @" - "
    for(int i=0;i<subStrings.count;i++){
    [dataArray addObject:[subStrings objectAtIndex:i]];
    }
        
      
        
    NSLog(@"#DataArray :%@",dataArray);
    }
    else{
    dataArray = array;
    }
    
    
    
    [tv reloadData];
}

#pragma mark - TableView Methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:simpleTableIdentifier];
    }
    
    
    if(isShow == TRUE){
        GiftLocationList *locList = [dataArray objectAtIndex:indexPath.row];

        
    NSArray *subStrings = [locList.fullAddress componentsSeparatedByString:@","]; //or rather @" - "
 
    cell.textLabel.text = [NSString stringWithFormat:@"%@,%@",locList.area,[subStrings lastObject]];
    cell.detailTextLabel.text = locList.address;
        
    }
    if(isShow == FALSE){
        cell.textLabel.text = [dataArray objectAtIndex:indexPath.row];
  
    }
    cell.separatorInset = UIEdgeInsetsZero;

    
    cell.imageView.image = [UIImage imageNamed:@"contact_list_icon.png"];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 80;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
    if(isShow == TRUE){
    GiftLocationList *locList = [dataArray objectAtIndex:indexPath.row];
    NSArray *subStrings = [locList.dealerContract componentsSeparatedByString:@","];
    numberStr = [subStrings objectAtIndex:0]; //[NSString stringWithFormat:@"tel:%@",[subStrings objectAtIndex:0]];
    }
    if(isShow == FALSE){
    numberStr = [dataArray objectAtIndex:indexPath.row]; //[NSString stringWithFormat:@"tel:%@",[dataArray objectAtIndex:indexPath.row]];
        
    }

    [self numberClicked:numberStr];
}


-(IBAction)numberClicked:(id)sender{
    NSLog(@"NumberStr :%@",numberStr);
   // [[UIApplication sharedApplication] openURL:[NSURL URLWithString:numberStr]];
    
    
    
    [Constant makePhoneCall:[Constant decodeNumber:numberStr]];

}

@end
