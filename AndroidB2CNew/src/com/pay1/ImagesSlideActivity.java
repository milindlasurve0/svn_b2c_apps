package com.pay1;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

import com.larvalabs.svgandroid.SVG;
import com.larvalabs.svgandroid.SVGParser;
import com.pay1.adapterhandler.ImageAdapter;

public class ImagesSlideActivity extends Activity {
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.image_slide_activity);
		Bundle b = getIntent().getExtras();
		int position = b.getInt("position");
		ArrayList<String> Array = b.getStringArrayList("Array");
		try {
			// JSONArray jsonArray = new JSONArray(Array);
			ViewPager viewPager = (ViewPager) findViewById(R.id.view_pager);
			ImageAdapter adapter = new ImageAdapter(this, Array);

			viewPager.setAdapter(adapter);

			viewPager.setCurrentItem(position, true);
		} catch (Exception exc) {

		}

		ImageView imageViewClose = (ImageView) findViewById(R.id.imageViewClose);
		SVG svgLikeImage = SVGParser.getSVGFromResource(this.getResources(),
				R.raw.ic_close);
		imageViewClose.setImageDrawable(svgLikeImage.createPictureDrawable());
		imageViewClose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

	}

}
