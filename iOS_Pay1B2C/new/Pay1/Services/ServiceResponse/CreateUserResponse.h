//
//  CreateUserResponse.h
//  Pay1
//
//  Created by Annapurna on 11/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CreateUserResponse : NSObject

@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) NSString *errorCode;
@property (nonatomic, strong) NSString * otp;
@property (nonatomic, strong) NSString *balance;
@property (nonatomic, strong) NSString *loyaltyPoints;
@property (nonatomic, strong) NSString *supportNumber;

@property (nonatomic, strong) NSMutableArray *recentTransArrayList;

@property (nonatomic, strong) NSString *billPaymentFailure;
@property (nonatomic, strong) NSString *rechargeFailure;
@property (nonatomic, strong) NSString *walletRefilledRetailer;

@property (nonatomic, strong) NSMutableArray *dealsArrayList;

@property (nonatomic, strong) NSString *offerFirstStr;
@property (nonatomic, strong) NSString *offerSecondStr;
@property (nonatomic, strong) NSString *offerThirdStr;
@property (nonatomic, strong) NSString *offerfourthStr;
@property (nonatomic, strong) NSString *offerFifthStr;




@end
