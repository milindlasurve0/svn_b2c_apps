



#import <UIKit/UIKit.h>

typedef void (^DismissBlock)(int buttonIndex);
typedef void (^CancelBlock)();

@interface UIAlertView (BlockAddition)



+ (UIAlertView*) alertViewWithTitle:(NSString*) title
                      message:(NSString*) message
                      cancelButtonTitle:(NSString*) cancelButtonTitle
                      otherButtonTitles:(NSArray*) otherButtons
                      onDismiss:(DismissBlock) dismissed
                      onCancel:(CancelBlock) cancelled;

+ (UIAlertView*) alertViewWithDefaultTitle:(NSString*) message
                      cancelButtonTitle:(NSString*) cancelButtonTitle
                      otherButtonTitles:(NSArray*)
                      otherButtons onDismiss:(DismissBlock) dismissed
                      onCancel:(CancelBlock) cancelled;

+ (UIAlertView*) alertViewWithDefaultTitle:(NSString*) message onCancel:(CancelBlock) cancelled;
+ (UIAlertView*) alertViewWithTitle:(NSString*) title message:(NSString*) message onCancel:(CancelBlock) cancelled;

+ (UIAlertView*) alertViewWithDefaultTitle:(NSString*) message onDismiss:(DismissBlock)onDismiss  onCancel:(CancelBlock) cancelled;
+ (UIAlertView*) alertViewWithTitle:(NSString*) title message:(NSString*) message onDismiss:(DismissBlock)onDismiss  onCancel:(CancelBlock) cancelled;

@property (nonatomic, copy) DismissBlock dismissBlock;
@property (nonatomic, copy) CancelBlock cancelBlock;

@end
