package com.pay1.model;

import com.pay1.databasehandler.FreeBies;

import android.text.SpannableStringBuilder;

public class GiftGridItem {
	private int offerID;
	private String offerName;
	private String dealName;
	private String logoImageUrl;
	private String dealImageUrl;
	private String dealDistanceArea;
	private FreeBies freeBieItem;
	private int dealLikeUnlike;
	private SpannableStringBuilder loyaltyPoints;
	private int layoutBackGround;
	private String offerStatus;
	
	public String getOfferStatus() {
		return offerStatus;
	}

	public void setOfferStatus(String offerStatus) {
		this.offerStatus = offerStatus;
	}

	public int getLayoutBackGround() {
		return layoutBackGround;
	}

	public void setLayoutBackGround(int layoutBackGround) {
		this.layoutBackGround = layoutBackGround;
	}

	public GiftGridItem() {
	}

	public FreeBies getFreeBieItem() {
		return freeBieItem;
	}


	public void setFreeBieItem(FreeBies freeBieItem) {
		this.freeBieItem = freeBieItem;
	}
	
	
	public int getOfferID() {
		return offerID;
	}
	public void setOfferID(int offerID) {
		this.offerID = offerID;
	}
	public String getOfferName() {
		return offerName;
	}
	public void setOfferName(String offerName) {
		this.offerName = offerName;
	}
	public String getDealName() {
		return dealName;
	}
	public void setDealName(String dealName) {
		this.dealName = dealName;
	}
	public String getLogoImageUrl() {
		return logoImageUrl;
	}
	public void setLogoImageUrl(String logoImageUrl) {
		this.logoImageUrl = logoImageUrl;
	}
	public String getDealImageUrl() {
		return dealImageUrl;
	}
	public void setDealImageUrl(String dealImageUrl) {
		this.dealImageUrl = dealImageUrl;
	}
	public String getDealDistanceArea() {
		return dealDistanceArea;
	}
	public void setDealDistanceArea(String dealDistanceArea) {
		this.dealDistanceArea = dealDistanceArea;
	}
	public int getDealLikeUnlike() {
		return dealLikeUnlike;
	}
	public void setDealLikeUnlike(int dealLikeUnlike) {
		this.dealLikeUnlike = dealLikeUnlike;
	}
	public SpannableStringBuilder getLoyaltyPoints() {
		return loyaltyPoints;
	}
	public void setLoyaltyPoints(SpannableStringBuilder loyaltyPoints) {
		this.loyaltyPoints = loyaltyPoints;
	}
	
	
	
}
