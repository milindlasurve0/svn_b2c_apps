package com.pay1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.WeakHashMap;

import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.graphics.Bitmap.Config;
import android.graphics.drawable.PictureDrawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.larvalabs.svgandroid.SVG;
import com.larvalabs.svgandroid.SVGParser;
import com.pay1.adapterhandler.CustomInfoWindowAdapter;
import com.pay1.constants.Constants;
import com.pay1.customviews.MyProgressDialog;
import com.pay1.servicehandler.GPSTracker;
import com.pay1.utilities.DirectionsJSONParser;
import com.pay1.utilities.Utility;

public class FreeBiesLocatorActivity extends FragmentActivity implements
		LocationListener {

	private static final String SCREEN_LABEL = "Freebie Locator Screen";
	private EasyTracker easyTracker = null;
	// private final String TAG = "Deal Locator";

	private GoogleMap googleMap;
	private TextView textView_Title;
	private ImageView imageView_Back;
	private double latitude, longitude;
	private String ADDRESS = "", CITY = "", STATE = "", DEAL = "";
	private boolean show_route = false;
	RelativeLayout back_layout;
	String full_adderess;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.freebies_locator_activity);
		try {
			easyTracker = EasyTracker.getInstance(FreeBiesLocatorActivity.this);
			easyTracker.set(Fields.SCREEN_NAME, SCREEN_LABEL);

			Typeface Reguler = Typeface.createFromAsset(getAssets(),
					"Roboto-Regular.ttf");

			Utility.setBackRequired(FreeBiesLocatorActivity.this,
					Constants.SHAREDPREFERENCE_BACK_LOCATION, false);
			// Typeface Narrow = Typeface.createFromAsset(getAssets(),
			// "Roboto-Regular.ttf");
			//
			// Typeface Narrow_thin = Typeface.createFromAsset(getAssets(),
			// "EncodeSansNarrow-100-Thin.ttf");

			try {
				Bundle bundle = getIntent().getExtras();
				if (bundle != null) {
					show_route = bundle.getBoolean("ROUTE", false);
					DEAL = bundle.getString("DEAL");
					latitude = Double.parseDouble(bundle.getString("LATITUDE"));
					longitude = Double.parseDouble(bundle
							.getString("LONGITUDE"));
					ADDRESS = bundle.getString("ADDRESS");
					CITY = bundle.getString("CITY");
					STATE = bundle.getString("STATE");
					full_adderess=bundle.getString("FULL_ADDRESS");
				}
			} catch (Exception e) {
			}

			textView_Title = (TextView) findViewById(R.id.textView_Title);
			textView_Title.setTypeface(Reguler);
			textView_Title.setText("Way to " + DEAL);
			back_layout = (RelativeLayout) findViewById(R.id.back_layout);
			imageView_Back = (ImageView) findViewById(R.id.imageView_Back);
			imageView_Back.setImageDrawable(SVGParser.getSVGFromResource(
					getResources(), R.raw.ic_back_old).createPictureDrawable());

			if (Build.VERSION.SDK_INT >= 11)
				imageView_Back.setLayerType(View.LAYER_TYPE_SOFTWARE, null);

			back_layout.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onBackPressed();
				}
			});

			// data = new ArrayList<WeakHashMap<String, String>>();
			// listView_Dealers = (ListView)
			// findViewById(R.id.listView_Dealers);
			// textView_NoData = (TextView) findViewById(R.id.textView_NoData);
			// listView_Dealers.setEmptyView(textView_NoData);
			// adapter = new
			// DealerLocatorAdapter(DealerLocatorDetailActivity.this, data);
			// listView_Dealers.setAdapter(adapter);

			// new DealorLocatorTask().execute(Utility.getLatitude(
			// DealerLocatorDetailActivity.this,
			// Constants.SHAREDPREFERENCE_LATITUDE), Utility.getLongitude(
			// DealerLocatorDetailActivity.this,
			// Constants.SHAREDPREFERENCE_LONGITUDE));

			if (googleMap == null) {
				// Try to obtain the map from the SupportMapFragment.
				googleMap = ((SupportMapFragment) getSupportFragmentManager()
						.findFragmentById(R.id.map_full)).getMap();
				googleMap.setMyLocationEnabled(true);
				googleMap.getUiSettings().setMyLocationButtonEnabled(true);
				googleMap.getUiSettings().setRotateGesturesEnabled(true);
				googleMap.getUiSettings().setCompassEnabled(true);
				googleMap.setInfoWindowAdapter(new CustomInfoWindowAdapter(
						FreeBiesLocatorActivity.this));

				if (googleMap != null) {

					if (show_route) {
						try {
							Location mylocation = googleMap.getMyLocation();
							if (mylocation != null) {

								double latitude = mylocation.getLatitude();

								// Getting longitude of the current location
								double longitude = mylocation.getLongitude();

								/*Utility.setCurrentLatitude(
										FreeBiesLocatorActivity.this,
										Constants.SHAREDPREFERENCE_CURRENT_LATITUDE,
										String.valueOf(latitude));
								Utility.setCurrentLongitude(
										FreeBiesLocatorActivity.this,
										Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE,
										String.valueOf(longitude));
*/
								
								GPSTracker gps = new GPSTracker(this);
								if (gps.canGetLocation()) { // gps enabled} // return boolean true/false

									gps.getLatitude(); // returns latitude
									gps.getLongitude(); // returns longitude

									Utility.setCurrentLatitude(FreeBiesLocatorActivity.this,
											Constants.SHAREDPREFERENCE_CURRENT_LATITUDE,
											String.valueOf(gps.getLatitude()));
									Utility.setCurrentLongitude(FreeBiesLocatorActivity.this,
											Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE,
											String.valueOf(gps.getLongitude()));
								}
								
								
							}
							Location location1 = new Location("Current");
							location1
									.setLatitude(Double.parseDouble(Utility
											.getCurrentLatitude(
													FreeBiesLocatorActivity.this,
													Constants.SHAREDPREFERENCE_CURRENT_LATITUDE)));
							location1
									.setLongitude(Double.parseDouble(Utility
											.getCurrentLongitude(
													FreeBiesLocatorActivity.this,
													Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE)));
							setUpMap(location1);
						} catch (Exception e) {
							setUpMap();
						}

					} else {
						setUpMap();
					}
				}
			}
			try {
				LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

				// Creating a criteria object to retrieve provider
				Criteria criteria = new Criteria();

				// Getting the name of the best provider
				String provider = locationManager.getBestProvider(criteria,
						true);

				// Getting Current Location
				Location location = locationManager
						.getLastKnownLocation(provider);

				if (location != null) {
					onLocationChanged(location);
				}
				locationManager
						.requestLocationUpdates(provider, 20000, 0, this);
			} catch (Exception e) {
			}
		} catch (Exception exception) {
		}
		overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}

	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView().build());
		// EasyTracker.getInstance(this).activityStart(this);
		easyTracker.send(MapBuilder.createAppView().build());
	}

	@Override
	public void onStop() {
		super.onStop();
		// EasyTracker.getInstance(this).activityStop(this);
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
	}

	private void setUpMap() {
		try {
			googleMap.clear();
			googleMap.setMyLocationEnabled(true);
			googleMap.getUiSettings().setMyLocationButtonEnabled(true);
			googleMap.getUiSettings().setRotateGesturesEnabled(true);
			googleMap.getUiSettings().setCompassEnabled(true);
			googleMap.setInfoWindowAdapter(new CustomInfoWindowAdapter(
					FreeBiesLocatorActivity.this));

			//StringBuffer sb = new StringBuffer();
			/*sb.append(ADDRESS).append(",\n").append(CITY).append(", ")
					.append(STATE);*/

			LatLng latLng_desti = new LatLng(latitude, longitude);
			MarkerOptions options_desti = new MarkerOptions();
			options_desti.position(latLng_desti);
			options_desti.title(DEAL);
			options_desti.snippet(full_adderess.toString());
			options_desti.draggable(false);
			/*options_desti.icon(BitmapDescriptorFactory
					.fromResource(R.drawable.icon_gift));*/
			
			SVG svg = SVGParser.getSVGFromResource(
					FreeBiesLocatorActivity.this.getResources(),
					R.raw.ic_gift_coin);
			// Drawable ic_rechargenow =
			// svg.createPictureDrawable();
			PictureDrawable pictureDrawable = svg
					.createPictureDrawable();
			Bitmap bitmap = Bitmap.createBitmap(
					pictureDrawable.getIntrinsicWidth(),
					pictureDrawable.getIntrinsicHeight(),
					Config.ARGB_8888);
			Canvas canvas = new Canvas(bitmap);
			canvas.drawPicture(pictureDrawable.getPicture());
			Bitmap currentBitmap = bitmap;
			options_desti.icon(BitmapDescriptorFactory
					.fromBitmap(currentBitmap));
			
			
			Marker marker = googleMap.addMarker(options_desti);
			marker.showInfoWindow();

			CameraPosition camPos = new CameraPosition.Builder()
					.target(latLng_desti).zoom(15).build();

			CameraUpdate camUpd3 = CameraUpdateFactory
					.newCameraPosition(camPos);

			googleMap.animateCamera(camUpd3);
		} catch (Exception exception) {
		}

	}

	private void setUpMap(Location location) {
		try {
			googleMap.clear();
			googleMap.setMyLocationEnabled(true);
			googleMap.getUiSettings().setMyLocationButtonEnabled(true);
			googleMap.getUiSettings().setRotateGesturesEnabled(true);
			googleMap.getUiSettings().setCompassEnabled(true);
			googleMap.setInfoWindowAdapter(new CustomInfoWindowAdapter(
					FreeBiesLocatorActivity.this));

			double lat = location.getLatitude(), lng = location.getLongitude();
			LatLng latLng_current = new LatLng(lat, lng);
			MarkerOptions options_current = new MarkerOptions();
			options_current.position(latLng_current);
			options_current.title("You are here");
			options_current.draggable(false);
			options_current.icon(BitmapDescriptorFactory
					.fromResource(R.drawable.ic_default_pin));
			
		/*	SVG svg = SVGParser.getSVGFromResource(
					MapActivity.this.getResources(),
					R.raw.ic_shoplocator);
			// Drawable ic_rechargenow =
			// svg.createPictureDrawable();
			PictureDrawable pictureDrawable = svg
					.createPictureDrawable();
			Bitmap bitmap = Bitmap.createBitmap(
					pictureDrawable.getIntrinsicWidth(),
					pictureDrawable.getIntrinsicHeight(),
					Config.ARGB_8888);
			Canvas canvas = new Canvas(bitmap);
			canvas.drawPicture(pictureDrawable.getPicture());
			Bitmap currentBitmap = bitmap;
			options.icon(BitmapDescriptorFactory
					.fromBitmap(currentBitmap));
			*/
			
			
			googleMap.addMarker(options_current);

			//StringBuffer sb = new StringBuffer();
			/*sb.append(ADDRESS).append(",\n").append(CITY).append(", ")
					.append(STATE);*/

			LatLng latLng_desti = new LatLng(latitude, longitude);
			MarkerOptions options_desti = new MarkerOptions();
			options_desti.position(latLng_desti);
			options_desti.title(DEAL);
			options_desti.snippet(full_adderess.toString());
			options_desti.draggable(false);
			options_desti.icon(BitmapDescriptorFactory
					.fromResource(R.drawable.ic_gift_pin));
			
		/*	SVG svg = SVGParser.getSVGFromResource(
					MapActivity.this.getResources(),
					R.raw.ic_shoplocator);
			// Drawable ic_rechargenow =
			// svg.createPictureDrawable();
			PictureDrawable pictureDrawable = svg
					.createPictureDrawable();
			Bitmap bitmap = Bitmap.createBitmap(
					pictureDrawable.getIntrinsicWidth(),
					pictureDrawable.getIntrinsicHeight(),
					Config.ARGB_8888);
			Canvas canvas = new Canvas(bitmap);
			canvas.drawPicture(pictureDrawable.getPicture());
			Bitmap currentBitmap = bitmap;
			options.icon(BitmapDescriptorFactory
					.fromBitmap(currentBitmap));
			*/
			
			
			Marker marker = googleMap.addMarker(options_desti);
			marker.showInfoWindow();

			String url = getDirectionsUrl(latLng_current, latLng_desti);
			// Log.w("URL", url);

			DownloadTask downloadTask = new DownloadTask();

			// Start downloading json data from Google Directions API
			downloadTask.execute(url);
			// Route.paths.add(polyline);
			// CameraPosition camPos = new CameraPosition.Builder()
			// .target(new LatLng(latitude, longitude)).zoom(18)
			// .bearing(location.getBearing()).tilt(70).build();

			CameraPosition camPos = new CameraPosition.Builder()
					.target(latLng_desti).zoom(15).build();

			CameraUpdate camUpd3 = CameraUpdateFactory
					.newCameraPosition(camPos);

			googleMap.animateCamera(camUpd3);
		} catch (Exception exception) {
		}

	}

	private String getDirectionsUrl(LatLng origin, LatLng dest) {

		// Origin of route
		String str_origin = "origin=" + origin.latitude + ","
				+ origin.longitude;

		// Destination of route
		String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

		// Sensor enabled
		String sensor = "sensor=false";

		// driving (default) indicates standard driving directions using the
		// road network.
		// walking requests walking directions via pedestrian paths & sidewalks
		// (where available).
		// bicycling requests bicycling directions via bicycle paths & preferred
		// streets (where available).
		// transit requests directions via public transit routes (where
		// available).

		// Travel Modes
		String mode = "mode=walking";

		// Building the parameters to the web service
		String parameters = str_origin + "&" + str_dest + "&" + sensor + "&"
				+ mode;

		// Output format
		String output = "json";

		// Building the url to the web service
		String url = "https://maps.googleapis.com/maps/api/directions/"
				+ output + "?" + parameters;

		return url;
	}

	/** A method to download json data from url */
	private String downloadUrl(String strUrl) throws IOException {
		String data = "";
		InputStream iStream = null;
		HttpURLConnection urlConnection = null;
		try {
			URL url = new URL(strUrl);

			// Creating an http connection to communicate with url
			urlConnection = (HttpURLConnection) url.openConnection();

			// Connecting to url
			urlConnection.connect();

			// Reading data from url
			iStream = urlConnection.getInputStream();

			BufferedReader br = new BufferedReader(new InputStreamReader(
					iStream));

			StringBuffer sb = new StringBuffer();

			String line = "";
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}

			data = sb.toString();

			br.close();

		} catch (Exception e) {
			// Log.d("Exception while downloading url", e.toString());
		} finally {
			iStream.close();
			urlConnection.disconnect();
		}
		return data;
	}

	// Fetches data from url passed
	private class DownloadTask extends AsyncTask<String, Void, String>
			implements OnDismissListener {
		private MyProgressDialog dialog;

		// Downloading data in non-ui thread
		@Override
		protected String doInBackground(String... url) {

			// For storing data from web service
			String data = "";
			try {
				// Fetching the data from web service
				data = downloadUrl(url[0]);
			} catch (Exception e) {
				// Log.d("Background Task", e.toString());
			}
			return data;
		}

		// Executes in UI thread, after the execution of
		// doInBackground()
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				if (this.dialog.isShowing()) {
					this.dialog.dismiss();
				}
				if (!result.equalsIgnoreCase("")) {
					ParserTask parserTask = new ParserTask();
					// Invokes the thread for parsing the JSON data
					parserTask.execute(result);
				} else {
					setUpMap();
				}
			} catch (Exception e) {
				setUpMap();
			}
		}

		protected void onPreExecute() {
			dialog = new MyProgressDialog(FreeBiesLocatorActivity.this);
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							DownloadTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			DownloadTask.this.cancel(true);
			dialog.cancel();
		}

	}

	/** A class to parse the Google Places in JSON format */
	private class ParserTask extends
			AsyncTask<String, Integer, List<List<WeakHashMap<String, String>>>> {
		// private MyProgressDialog dialog;

		// Parsing the data in non-ui thread
		@Override
		protected List<List<WeakHashMap<String, String>>> doInBackground(
				String... jsonData) {

			JSONObject jObject;
			List<List<WeakHashMap<String, String>>> routes = null;

			try {
				jObject = new JSONObject(jsonData[0]);
				DirectionsJSONParser parser = new DirectionsJSONParser();

				// Starts parsing data
				routes = parser.parse(jObject);
			} catch (Exception e) {
				// e.printStackTrace();
			}
			return routes;
		}

		// Executes in UI thread, after the parsing process
		@Override
		protected void onPostExecute(
				List<List<WeakHashMap<String, String>>> result) {
			super.onPostExecute(result);
			try {
				// if (this.dialog.isShowing()) {
				// this.dialog.dismiss();
				// }
				if (result != null) {
					ArrayList<LatLng> points = null;
					PolylineOptions lineOptions = null;
					// MarkerOptions markerOptions = new MarkerOptions();

					// Traversing through all the routes
					for (int i = 0; i < result.size(); i++) {
						points = new ArrayList<LatLng>();
						lineOptions = new PolylineOptions();

						// Fetching i-th route
						List<WeakHashMap<String, String>> path = result.get(i);

						// Fetching all the points in i-th route
						for (int j = 0; j < path.size(); j++) {
							WeakHashMap<String, String> point = path.get(j);

							double lat = Double.parseDouble(point.get("lat"));
							double lng = Double.parseDouble(point.get("lng"));
							LatLng position = new LatLng(lat, lng);

							points.add(position);
						}

						// Adding all the points in the route to LineOptions
						lineOptions.addAll(points);
						lineOptions.width(6);
						lineOptions.color(getResources().getColor(
								R.color.app_blue_line_map_color));
					}

					// Drawing polyline in the Google Map for the i-th route
					googleMap.addPolyline(lineOptions);
				} else {
					setUpMap();
				}
			} catch (Exception e) {
				setUpMap();
			}
		}

		protected void onPreExecute() {
			// dialog = new MyProgressDialog(DealLocatorActivity.this);
			// // this.dialog.setMessage("Please wait.....");
			// this.dialog.setCancelable(false);
			// this.dialog
			// .setOnCancelListener(new DialogInterface.OnCancelListener() {
			//
			// @Override
			// public void onCancel(DialogInterface dialog) {
			// // TODO Auto-generated method stub
			// ParserTask.this.cancel(true);
			// }
			// });
			// this.dialog.show();
			super.onPreExecute();
		}

		// @Override
		// public void onDismiss(DialogInterface dialog) {
		// // TODO Auto-generated method stub
		// ParserTask.this.cancel(true);
		// dialog.cancel();
		// }

	}

	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		if (location != null) {

			double latitude = location.getLatitude();

			// Getting longitude of the current location
			double longitude = location.getLongitude();

			// Creating a LatLng object for the current location
			// LatLng latLng = new LatLng(latitude, longitude);

			// Showing the current location in Google Map
			// googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

			// Zoom in the Google Map
			// googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));

			/*Utility.setCurrentLatitude(FreeBiesLocatorActivity.this,
					Constants.SHAREDPREFERENCE_CURRENT_LATITUDE,
					String.valueOf(latitude));
			Utility.setCurrentLongitude(FreeBiesLocatorActivity.this,
					Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE,
					String.valueOf(longitude));*/
			
			GPSTracker gps = new GPSTracker(this);
			if (gps.canGetLocation()) { // gps enabled} // return boolean true/false

				gps.getLatitude(); // returns latitude
				gps.getLongitude(); // returns longitude

				Utility.setCurrentLatitude(FreeBiesLocatorActivity.this,
						Constants.SHAREDPREFERENCE_CURRENT_LATITUDE,
						String.valueOf(gps.getLatitude()));
				Utility.setCurrentLongitude(FreeBiesLocatorActivity.this,
						Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE,
						String.valueOf(gps.getLongitude()));
			}

		}
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}
}