﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Pay1B2C.ViewModel;
using System.Collections.ObjectModel;
using Pay1B2C.Model;
using Pay1B2C.UserControls;
using Pay1B2C.Utilities;
using System.Windows.Media.Imaging;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using SQLite;

namespace Pay1B2C.Views
    {
    public partial class MisscallRecharge: PhoneApplicationPage
        {
        ProgressBarManager ProgressBarManager;
        public MisscallRecharge()
            {
            InitializeComponent();
            Constant.laodNavigationBar(TitlePanel, LayoutRoot, canvas, "Missed Call Recharge");
            LoadMissCallData();
            ProgressIndicator prog = new ProgressIndicator();
            ProgressBarManager = new ProgressBarManager();
            SystemTray.SetProgressIndicator(this, prog);
            ProgressBarManager.Hook(prog);

            this.Loaded+=(s, e) =>
            {
             Task.Run(() => getQuickPayTask());
            }; ;
            }


        private async void getQuickPayTask()
            {
            ProgressBarManager.Show(40, "Please wait...", false, 10);
            var values = new List<KeyValuePair<string, string>>();
            values.Add(new KeyValuePair<string, string>("actiontype", "get_quickpaylist"));
            values.Add(new KeyValuePair<string, string>("api", "true"));
            values.Add(new KeyValuePair<string, string>("api_version", Constant.API_VERSION));
            FormUrlEncodedContent values1 = new FormUrlEncodedContent(values);
            Task<string> response = Constant.postParamsAndgetResponse(Constant.B2C_URL, values1);

            string result = await response;
            if(string.IsNullOrEmpty(result))
                {
                NavigationService.Navigate(new Uri("/Views/NoConnection.xaml?", UriKind.RelativeOrAbsolute));
                }
            else
                {
                var dict = (JObject)JsonConvert.DeserializeObject(result);
                string status = dict["status"].ToString();

                if(status.Equals("success"))
                    {

                    try
                        {

                        int ii = 0;
                        var description = dict["description"];
                        foreach(var item in description)
                            {
                            if(!string.IsNullOrEmpty(item["product"].ToString()) && !item["product"].ToString().Equals("0"))
                                {
                                QuickPayAllDataTable quickPayAllDataTable = new QuickPayAllDataTable();
                                quickPayAllDataTable.id = item["id"].ToString();
                                quickPayAllDataTable.missed_number = item["missed_number"].ToString();
                                quickPayAllDataTable.name = item["name"].ToString();
                                quickPayAllDataTable.number = item["number"].ToString();
                                quickPayAllDataTable.operator_code = item["operator_code"].ToString();
                                quickPayAllDataTable.operator_id = item["operator_id"].ToString();
                                quickPayAllDataTable.operator_name = item["operator_name"].ToString();
                                quickPayAllDataTable.product = item["product"].ToString();
                                quickPayAllDataTable.stv = item["stv"].ToString();
                                quickPayAllDataTable.transaction_flag = item["transaction_flag"].ToString();
                                quickPayAllDataTable.users_id = item["users_id"].ToString();
                                quickPayAllDataTable.amount = item["amount"].ToString();
                                quickPayAllDataTable.confirmation = item["confirmation"].ToString();
                                quickPayAllDataTable.datetime = item["datetime"].ToString();
                                quickPayAllDataTable.delegate_flag = item["delegate_flag"].ToString();
                                quickPayAllDataTable.flag = item["flag"].ToString();

                                try
                                    {
                                    MisscallDataHelper misscallHelper=new MisscallDataHelper();
                                    misscallHelper.DeleteMisscallById(item["id"].ToString());

                                    misscallHelper.Insert(quickPayAllDataTable);
                                    }
                                catch(SQLiteException swe)
                                    {
                                    }
                                catch(Exception es)
                                    {
                                    }
                                }
                            }

                        try
                            {
                            LoadMissCallData();
                            }
                        catch(Exception exc)
                            {
                            }
                        }
                    catch(SQLiteException see)
                        {
                        }

                    }

                else
                    {
                    Constant.checkForErrorCode(result);
                    }
                }
            Dispatcher.BeginInvoke(delegate()
            {
                //chaekcForMissCallData();
                //LoadMissCallData (dict["description"].ToString ());

            });
             ProgressBarManager.Hide(40);
            }






        protected override void OnNavigatedTo(NavigationEventArgs e)
            {
          
            Constant.laodNavigationBar(TitlePanel, LayoutRoot, canvas, "Missed Call Recharge");
            }


        private void Button_Click(object sender, RoutedEventArgs e)
            {

            }

        private void home_Click(object sender, EventArgs e)
            {
            NavigationService.Navigate(new Uri("/HomePage.xaml?", UriKind.RelativeOrAbsolute));
            }

        private void recharges_Click(object sender, EventArgs e)
            {
            NavigationService.Navigate(new Uri("/Views/RechargeMain.xaml?", UriKind.RelativeOrAbsolute));

            }

        private void gift_Click(object sender, EventArgs e)
            {
            NavigationService.Navigate(new Uri("/GiftMainPivot.xaml?", UriKind.RelativeOrAbsolute));
            }

        private void money_Click(object sender, EventArgs e)
            {
            NavigationService.Navigate(new Uri("/AddMoneyPage.xaml?", UriKind.RelativeOrAbsolute));
            }


        private void LoadMissCallData()
            {
            MisscallDataHelper misscallHelper=new MisscallDataHelper();
            ObservableCollection<QuickPayAllDataTable> quickPayList=misscallHelper.ReadQuickPayAllDataTable();
            missCallList.Items.Clear();


            foreach(QuickPayAllDataTable quickPayItem in quickPayList)
                {

               
                MissedCallControl panel =new MissedCallControl();
                panel.textAmount.Text="Rs. "+quickPayItem.amount.ToString();
                panel.textMobileNumber.Text=quickPayItem.number;
                if(string.IsNullOrEmpty(quickPayItem.missed_number.ToString()))
                    {
                    panel.Chkcategories.IsChecked=false;
                    panel.textMissedCallNumber.Text="";
                    }
                else
                    {
                   
                    panel.textMissedCallNumber.Text="Missed Call: "+quickPayItem.missed_number.ToString();
                    panel.Chkcategories.IsChecked=true;
                    }


              
                panel.Chkcategories.Checked+=(s, e) =>
                {
                CheckBox cb = s as CheckBox;
                if(cb.IsChecked==true)
                    {
                    setMissedCallTask("1", quickPayItem);
                    };
                };

                 panel.Chkcategories.Unchecked+=(s, e) =>
                { CheckBox cb = s as CheckBox;
                    if(cb.IsChecked==false)
                        {
                        setMissedCallTask("0", quickPayItem);
                        };
                };

                if(string.IsNullOrEmpty(quickPayItem.product.ToString()))
                    {
                    panel.imageOperator.Source = new BitmapImage(new Uri("/Images/ic_wallet.png", UriKind.RelativeOrAbsolute));
                    }
                else
                    {
                    panel.imageOperator.Source = OperatorConstant.getOperatorResource(Convert.ToInt16(quickPayItem.product));
                    }

                missCallList.Items.Add(panel);
                }

             

                
            }



        private async void setMissedCallTask(string delegate_flag, QuickPayAllDataTable quickPayAllDataTable)
            {
            ProgressBarManager.Show(20, "Please wait...", false, 10);

            var values = new List<KeyValuePair<string, string>>();
            values.Add(new KeyValuePair<string, string>("actiontype", "edit_quickpay"));
            values.Add(new KeyValuePair<string, string>("name", quickPayAllDataTable.name));
            values.Add(new KeyValuePair<string, string>("number", quickPayAllDataTable.number));
            values.Add(new KeyValuePair<string, string>("flag", quickPayAllDataTable.flag));
            values.Add(new KeyValuePair<string, string>("amount", quickPayAllDataTable.amount));
            values.Add(new KeyValuePair<string, string>("operator_id", quickPayAllDataTable.operator_id));
            values.Add(new KeyValuePair<string, string>("confirmation", quickPayAllDataTable.confirmation));
            values.Add(new KeyValuePair<string, string>("id", quickPayAllDataTable.id));
            values.Add(new KeyValuePair<string, string>("stv", quickPayAllDataTable.stv));
            values.Add(new KeyValuePair<string, string>("delegate_flag", delegate_flag));

            values.Add(new KeyValuePair<string, string>("api", "true"));
            FormUrlEncodedContent values1 = new FormUrlEncodedContent(values);
            Task<string> response = Constant.postParamsAndgetResponse(Constant.B2C_URL, values1);

            string result = await response;
            if(string.IsNullOrEmpty(result))
                {
                NavigationService.Navigate(new Uri("/NoConnection.xaml?", UriKind.Relative));
                }
            else
                {
                var dict = (JObject)JsonConvert.DeserializeObject(result);
                string status = dict["status"].ToString();

                var description = dict["description"];
                if(status.Equals("success"))
                    {
                    try
                        {
                        MisscallDataHelper misscallHelper=new MisscallDataHelper();
                        if(string.IsNullOrEmpty(description["missed_number"].ToString()))
                            {
                            misscallHelper.UpdateMissCall(delegate_flag, quickPayAllDataTable.id,"");
                            }
                        else
                            {

                            misscallHelper.UpdateMissCall(delegate_flag, quickPayAllDataTable.id, description["missed_number"].ToString());
                            
                            }
                      
                        }
                    catch(JsonException je)
                        {
                        }
                    }
                else
                    {
                    LoadMissCallData();
                    ProgressBarManager.Hide(20);
                    MessageBox.Show(dict["description"].ToString());
                    }
                }

            ProgressBarManager.Hide(20);
            }




        }
    }