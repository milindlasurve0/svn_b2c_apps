<?php

function seoUrl($string)
{
       
    $string = trim($string);
    
    $string = html_entity_decode($string);
    
    $string = strip_tags($string);
    
    $string = strtolower($string);
    
    $string = preg_replace('~[^ a-z0-9_.]~', ' ', $string);
    
    $string = preg_replace('~ ~', '-', $string);
    
    $string = preg_replace('~-+~', '-', $string);
        
    return $string;
}

