//
//  HomeViewController.m
//  Pay1
//
//  Created by Annapurna on 21/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "HomeViewController.h"
#import "DBManager.h"
#import "OfferDetailList.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#import "UIImageView+WebCache.h"
#import "UIImage+Utility.h"
#import "MainViewController.h"
#import "GiftsViewController.h"
#import "TopOfferViewController.h"
#import "StoreLocationViewController.h"
#import "GiftDetailViewController.h"
#import "GetUpdatedDealResponse.h"
#import "GetUpdatedDealService.h"
#import "GetUpdateDealCriteria.h"
#import "CurrentLocation.h"
#import "DBManager.h"
#import "AllDealList.h"
#import "DealLocationList.h"
#import "GetPlanService.h"
#import "GetOperatorResponse.h"
#import "GetMobileDataList.h"
#import "GetMobileResponse.h"
#import "GetOperatorService.h"
#import "CoinViewController.h"

@interface HomeViewController ()

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
   
    
    }

-(void)viewWillAppear:(BOOL)animated{
    
    [self setTwoNavigationButtonsWithImages:@"money_sliding_menu.png" secondImage:@"coin_sling_menu.png" thirdImage:@"magnifying_glass.png" isLeft:FALSE selector1:@selector(moneyButtonClicked:) selector2:@selector(coinButtonClicked:) selector3:@selector(searchBtnClicked:) isFromGift:FALSE];
    
    [self setNavBarTintColor];
    
    [self setViewAccrodingToFrame];
    
   
    if([Constant getHomeRefreshStatus]){
        
        [Constant saveHomeRefreshStatus:FALSE];
    
    
    
    UITapGestureRecognizer *tapGestureStore = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(storeLocationClicked:)];
    tapGestureStore.numberOfTapsRequired = 1;
    [mapButtonView addGestureRecognizer:tapGestureStore];
    
    
    callGetDealValue = 0;
    mapButtonView.backgroundColor = [UIColor colorWithRed:244/255.0 green:244/255.0 blue:244/255.0 alpha:1];
    
    allOfferArrayList = [[NSMutableArray alloc]init];
    array = [[NSMutableArray alloc]init];
    offerTextArray = [[NSMutableArray alloc]init];
    dealArray = [[NSMutableArray alloc]init];
    shopNameArray = [[NSMutableArray alloc]init];
    
   
    
    imageY = 370;
    
    
    
    
    NSLog(@"AllOfferValues :%@",[Session shared].objSignInResponse.offerDetailArray);
    
    for(int i=0;i<[Session shared].objSignInResponse.offerDetailArray.count;i++){
        OfferDetailList *objOfferList = [[Session shared].objSignInResponse.offerDetailArray objectAtIndex:i];
        offerNameStr = objOfferList.name;
        offerIDStr = objOfferList.idVal;
        offerIDS = objOfferList.details;
        
        NSLog(@"Values :%@",objOfferList.details);
        offerIDArray= [objOfferList.details componentsSeparatedByString:@","];
    }
    
    
    
    
    NSLog(@"FrameWidth :%f",self.view.frame.size.width);
    
    scrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height + 100);
    
    
    
    [self fetchGiftDataFromDB];
    
    
        
    [self callGetUpdatedDetailServiceBLock];
        
    
    }
    
    else{
        NSLog(@"Constat is false");
    }
   

}

-(void)loginDone{
    [self viewWillAppear:NO];

}

-(void)callMobileServicePlanBlockInBackground{    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    dispatch_async(dispatch_get_main_queue(), ^{

    [self callGetMobileDetailService];
    [self callOperatorListService];

    });
    });
    
    
    
}


-(void)callGetPlanAPIService{
    
   // [Session shared].isPlansFilled = FALSE;
    
    GetPlanService *service = [[GetPlanService alloc]init];
    [service callGetPlanService:^(NSError *error, GetPlanResponse *objGetPlanResponse) {
    if(error)return ;
    else{
    NSLog(@"PlansResponse ");
   // [Session shared].isPlansFilled = TRUE;
    [Constant saveLastServicePlanTime:[Constant currentSystemDateTime]];
        
    [self emptyFirstPlanDB];
        
    }
    }];
}

-(void)emptyFirstPlanDB{
    
     DBManager *  objDBManager = [[DBManager alloc]initWithDatabaseFilename:@"plansdb.sql"];
    
    NSString *query1 = [NSString stringWithFormat:@"drop table plandatatable"];
    
    
    [objDBManager executeQuery:query1];
    
    if (objDBManager.affectedRows != 0) {
        NSLog(@"Query was executed successfully.");
    }
    else{
        NSLog(@"Could not execute the query.");
    }
    
    NSString *query2 = [NSString stringWithFormat:@"alter table tempplantable rename to plandatatable"];
    [objDBManager executeQuery:query2];
    
    if (objDBManager.affectedRows != 0) {
        NSLog(@"Query was executed successfully.");
    }
    else{
        NSLog(@"Could not execute the query.");
    }
    
    [self callMobileServicePlanBlockInBackground];
}

#pragma mark - Mobile Details

-(void)callMobileDetailAPIServiceInBackground{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
    [self callGetMobileDetailService];
    });
}


-(void)callGetMobileDetailService{
   // [self showHUDOnView];
    GetPlanService *objService = [[GetPlanService alloc]init];
    [objService callGetMobileService:[Constant getLastTimeMobilePlan] callback:^(NSError *error, GetMobileResponse *objMobileResponse) {
    if(error)return ;
    else{
    [Constant saveLastMobilePlanTime:[Constant currentSystemDateTime]];

    }
    }];
}

-(void)saveMobileDetailsInDB:(GetMobileResponse *)mobResponse{
    DBManager* objMobDBManager = [[DBManager alloc]initWithDatabaseFilename:@"mobiledatadb.sql"];
    NSString *query;
    for(int i=0;i<mobResponse.detailsArray.count;i++){
    GetMobileDataList *objDataList = [mobResponse.detailsArray objectAtIndex:i];
    query = [NSString stringWithFormat:@"insert into NumberAndCircleTable values(null,'%@','%@','%@','%@','%@','%@','%@')",objDataList.area,objDataList.areaName,objDataList.operatorID,objDataList.operatorName,objDataList.productID,objDataList.startNumber,[Constant currentSystemDateTime]];
        [objMobDBManager executeQuery:query];
        
    }
    
    if (objMobDBManager.affectedRows != 0) {
        NSLog(@"Query was executed successfully.");
    }
    else{
        NSLog(@"Could not execute the query.");
    }
    
  //  [self callOperatorListService];
}

#pragma mark - Operator Services

-(void)callOperatorListService{
    GetOperatorService *objOperatorService = [[GetOperatorService alloc]init];
    [objOperatorService callGetOperatorService:^(NSError *error, GetOperatorResponse *objGetOperatorResponse) {
    if (error)
    return ;
    else{
   // [self callGetUpdatedGiftDetailServiceBLockInBackGround];
    }
    }];
}


#pragma mark - Gifts Details

-(void)callGetUpdatedGiftDetailServiceBLockInBackGround{
    [self callGetUpdatedDetailServiceBLock];
}

-(void)callGetUpdatedDetailServiceBLock{
    if([Session shared].isFromAutoLogin){
        [self showHUDOnView];
    }
    
    
    GetUpdatedDealService *dealService = [[GetUpdatedDealService alloc]init];
    [dealService callUpdatedDealService:self.dealCriteria callback:^(NSError *error, GetUpdatedDealResponse *updateDealResponse) {
    if(error)return ;
    else{
        [self hideHUDOnView];
  //  [Session shared].isAppLaunched = FALSE;
    [Constant saveLastGiftTime:[Constant currentSystemDateTime]];
    NSLog(@"updateDealResponse :%@ %lu",updateDealResponse,(unsigned long)updateDealResponse.allDealsArray.count);
        
   // dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
    [self saveGiftsDataInSQLite:updateDealResponse];
   // });
 
    }
    }];
}

-(GetUpdateDealCriteria *)dealCriteria{
    GetUpdateDealCriteria *objDealCriteria = [[GetUpdateDealCriteria alloc]init];
    objDealCriteria.userMobileNum = [Constant getUserPhoneNumber]; //@"9975629244";
    CLLocation *location = [CurrentLocation sharedLocation].currentLocation;
    objDealCriteria.userLat = [NSString stringWithFormat:@"%f",location.coordinate.latitude];
    objDealCriteria.userLong = [NSString stringWithFormat:@"%f",location.coordinate.longitude];
    objDealCriteria.nextValue = [NSString stringWithFormat:@"%d",callGetDealValue];
    objDealCriteria.updatedTime = [Constant getLastTimeGiftPlan]; //@"2015-08-11 14:53:37"; //[Constant getLastTimeGiftPlan];
    NSLog(@"objDealCriteria :%@ %@ %@ %@ %@",objDealCriteria.userMobileNum,objDealCriteria.userLat,objDealCriteria.userLong,objDealCriteria.nextValue,objDealCriteria.updatedTime);
    return objDealCriteria;
}


-(void)saveGiftsDataInSQLite:(GetUpdatedDealResponse *)giftDealResponse{
    NSLog(@"#SaveDataIsCalled");
    
    [array removeAllObjects];
    
    
    DBManager *dbManager = [[DBManager alloc]initWithDatabaseFilename:@"dbgiftdata.sql"];
    NSString*  query = [NSString stringWithFormat:@"select * from GIFTTABLE"];
    
    
    NSArray *theArray = [[NSArray alloc] initWithArray:[dbManager loadDataFromDB:query]];
    
    
   // NSLog(@"#TheArray :%@",theArray);
    
    
    for(int i=0;i<theArray.count;i++){
    [array addObject: [[theArray objectAtIndex:i] objectAtIndex:14]];
    }
    
    
    NSMutableArray *expiredIDArray = [[NSMutableArray alloc]init];
    [expiredIDArray addObject:[giftDealResponse.expiredOffers componentsSeparatedByString:@","]];
    
    NSMutableArray *delArray = [expiredIDArray objectAtIndex:0];
    
    for(int i=0;i<array.count;i++){
        
    if(i<delArray.count){
    if([array containsObject:[[expiredIDArray objectAtIndex:0]objectAtIndex:i]]){
     NSLog(@"RemoveDataFromDB");
     NSString * exQuery = [NSString stringWithFormat:@"delete  from GIFTTABLE WHERE FREEBIEDEALID=\"%@\"",[[expiredIDArray objectAtIndex:0] objectAtIndex:i]];
    [dbManager executeQuery:exQuery];
        
    if (dbManager.affectedRows != 0) {
    NSLog(@"Query was executed successfully.");
    }
    else{
     NSLog(@"Could not execute the query.");
    }

    }
    }
    else{
    }
    }
    
   

    
    NSString *insQuery;
    AllDealList *objAllDealList;
    if(giftDealResponse.allDealsArray.count == 0){
       NSLog(@"#Don't insert any value");
    }
    else{
    for(int i=0;i<giftDealResponse.allDealsArray.count;i++){
    objAllDealList = [giftDealResponse.allDealsArray objectAtIndex:i];
    NSLog(@"#DealOfferID :%d",objAllDealList.dealOfferID);
        
    if([array containsObject:[NSString stringWithFormat:@"%d",objAllDealList.dealOfferID]]){
        NSLog(@"#Don't insert any value");
    }
     
    else{
        
    if(objAllDealList.locationArrayList.count > 0){
    DealLocationList *objDealList = [objAllDealList.locationArrayList objectAtIndex:0];
        
  //  NSLog(@"Location Array count :%lu",objAllDealList.locationArrayList.count);
        
    NSUInteger intVal = objAllDealList.locationArrayList.count;
        int iInt1 = (int)intVal;
            
    NSLog(@"Values of addess :%d %@ %@ %@ %d %@ %@ %@ %@ %@ %@ %@ %d %d %@ %@ %@ %d %@ %@ %@ %@",objAllDealList.dealID,objAllDealList.dealName,objAllDealList.dealImgURL,objAllDealList.dealCat,objAllDealList.dealCatID,objDealList.dealLat,objDealList.dealLong,objDealList.dealAddress,objDealList.dealArea,objDealList.dealCity,objDealList.dealState,@"0",iInt1,objAllDealList.dealOfferID,objAllDealList.dealOfferName,objAllDealList.dealValid,objAllDealList.dealOfferDescription,objAllDealList.dealMin,[Constant getLastTimeGiftPlan],@"0",objAllDealList.dealLogoURL,objAllDealList.dealerContact);
    
  
            
    insQuery = [NSString stringWithFormat:@"insert into GIFTTABLE values(null,'%d','%@','%@','%@','%d','%@','%@','%@','%@','%@','%@','%d','%d','%d','%@','%@','%@','%d','%@','%d','%@','%@','%@','%@')",objAllDealList.dealID,objAllDealList.dealName,objAllDealList.dealImgURL,objAllDealList.dealCat,objAllDealList.dealCatID,objDealList.dealLat,objDealList.dealLong,objDealList.dealAddress,objDealList.dealArea,objDealList.dealCity,objDealList.dealState,'0',iInt1,objAllDealList.dealOfferID,objAllDealList.dealOfferName,objAllDealList.dealValid,objAllDealList.dealOfferDescription,objAllDealList.dealMin,[Constant getLastTimeGiftPlan],'0',objAllDealList.dealLogoURL,objAllDealList.dealerContact,objAllDealList.dealByVoucher,objAllDealList.dealOfferPrice];
            
  
            
    [dbManager executeQuery:insQuery];
            
    }
        
        
    else{
    NSUInteger intVal = objAllDealList.locationArrayList.count;
    int iInt1 = (int)intVal;
            
    NSLog(@"#Insert in DB");
           
            
    insQuery = [NSString stringWithFormat:@"insert into GIFTTABLE values(null,'%d','%@','%@','%@','%d','%@','%@','%@','%@','%@','%@','%d','%d','%d','%@','%@','%@','%d','%@','%d','%@','%@','%@','%@')",objAllDealList.dealID,objAllDealList.dealName,objAllDealList.dealImgURL,objAllDealList.dealCat,objAllDealList.dealCatID,@"",@"",@"",@"",@"",@"",'0',iInt1,objAllDealList.dealOfferID,objAllDealList.dealOfferName,objAllDealList.dealValid,objAllDealList.dealOfferDescription,objAllDealList.dealMin,[Constant getLastTimeGiftPlan],'0',objAllDealList.dealLogoURL,objAllDealList.dealerContact,objAllDealList.dealByVoucher,objAllDealList.dealOfferPrice];
            
    NSLog(@"Queryis :%@",query);
            
    [dbManager executeQuery:insQuery];
    }
        
    }
    }
    
    
    if (dbManager.affectedRows != 0) {
        NSLog(@"Query was executed successfully.");
    }
    else{
        NSLog(@"Could not execute the query.");
    }
    
    }
    
    if([Session shared].isAppLaunched == TRUE){
        [Session shared].isAppLaunched = FALSE;
    [self callGetPlanAPIService];
    }

   
    
}




#pragma mark - Display Home View Data

-(void)fetchGiftDataFromDB{

    NSString *query;
    NSMutableArray *offerArrayList = [[NSArray arrayWithArray:offerIDArray] mutableCopy];
    
    
    DBManager *dbManager = [[DBManager alloc]initWithDatabaseFilename:@"dbgiftdata.sql"];
    for(int i=0;i<offerArrayList.count;i++){
    NSString *offserValue = [offerArrayList objectAtIndex:i];
        
    query = [NSString stringWithFormat:@"select * from GIFTTABLE WHERE FREEBIEOFFERID=\"%@\"",offserValue];
   // NSLog(@"query :%@",query);
    NSArray *theArray = [[NSArray alloc] initWithArray:[dbManager loadDataFromDB:query]];
        
   // NSLog(@"#TheArray :%@",theArray);
        
    NSInteger indexOfOfferURL = [dbManager.arrColumnNames indexOfObject:@"FREEBIEURL"];
    for(int i=0;i<theArray.count;i++){
    [array addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfOfferURL]];
  //  NSLog(@"#Array Of Image URL:%@",array);
    }
        
    NSInteger indexOfOfferName = [dbManager.arrColumnNames indexOfObject:@"FREEBIESHORTDESC"];
    for(int i=0;i<theArray.count;i++){
    [offerTextArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfOfferName]];
    }
    
    NSInteger indexOfDeal = [dbManager.arrColumnNames indexOfObject:@"FREEBIEDEALID"];
    for(int i=0;i<theArray.count;i++){
    NSLog(@"Deal%@", [[theArray objectAtIndex:i] objectAtIndex:indexOfDeal]);
    [dealArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfDeal]];
    }
        
    NSInteger indexOfDealName = [dbManager.arrColumnNames indexOfObject:@"FREEBIEDEALNAME"];
    for(int i=0;i<theArray.count;i++){
    [shopNameArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfDealName]];
    }
        
        
    [allOfferArrayList addObject:theArray];
    }
    
    
    [self showOfferOnHomeView:array];
}
-(void)showOfferOnHomeView:(NSArray *)offerArray{
    
    for(int i=0;i<offerArray.count;i++){
    offerImageView = [[UIImageView alloc]initWithFrame:CGRectMake(10, imageY, self.view.frame.size.width - 16, 150)];
    offerCoverView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, offerImageView.frame.size.width, offerImageView.frame.size.height)];
    offerCoverView.tag = i;
    offerCoverView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.2];
        
        
    offerTextLabel = [[UILabel alloc]initWithFrame:CGRectMake(5, 100, offerImageView.frame.size.width - 10, 21)];
    offerTextLabel.text = [offerTextArray objectAtIndex:i];
    offerTextLabel.textColor = [UIColor whiteColor];
    offerTextLabel.numberOfLines = 2;
    [offerTextLabel sizeToFit];

        
    
    [offerImageView setImageWithURL:[NSURL URLWithString:[offerArray objectAtIndex:i]] placeholderImage:[UIImage imageNamed:@"deal_default.png"]completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
    UIImage *newImage = [UIImage imageWithImage:image  scaledToSize:CGSizeMake(320,212)];
    offerImageView.image = newImage;
    }usingActivityIndicatorStyle:(UIActivityIndicatorViewStyle)UIActivityIndicatorViewStyleGray];
        
        
    offerImageView.userInteractionEnabled = TRUE;
        
        
        
    UITapGestureRecognizer *gesRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(imageTapped:)];
    gesRecognizer.delegate = self;
        gesRecognizer.view.tag = i;
    [offerCoverView addGestureRecognizer:gesRecognizer];
     
    [offerCoverView addSubview:offerTextLabel];
    [offerImageView addSubview:offerCoverView];
    [scrollView addSubview:offerImageView];
    imageY+=offerImageView.frame.size.height + 20;
    }
    
    if([UIScreen mainScreen].bounds.size.width == 320){
    scrollView.contentSize = CGSizeMake(self.view.frame.size.width , imageY + 80);

    }
    else
    scrollView.contentSize = CGSizeMake(self.view.frame.size.width , imageY);
}

-(void)moneyButtonClicked:(id)sender{
    NSLog(@"#MoneyClicked");
    NSLog(@"#TheArrayValue is :%@",allOfferArrayList);
    
    AppDelegate *appDelegate =  (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    [appDelegate.tabBarController setSelectedIndex:3];
}
-(void)coinButtonClicked:(id)sender{
    NSLog(@"#CoinClicked");
    CoinViewController *coinVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CoinView"];
    //[Session shared].isFromSlide = FALSE;
    coinVC.hidesBottomBarWhenPushed = TRUE;
    [self.navigationController pushViewController:coinVC animated:YES];
}

-(IBAction)rechargeClicked:(id)sender{
    AppDelegate *appDelegate =  (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [appDelegate.tabBarController setSelectedIndex:1];
    
   // MainViewController *mainVC = [self.storyboard instantiateViewControllerWithIdentifier:@"mainViewC"];
    //[self.navigationController pushViewController:mainVC animated:YES];
}
-(IBAction)giftsCLicked:(id)sender{
    
    AppDelegate *appDelegate =  (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [appDelegate.tabBarController setSelectedIndex:2];
    
 //   GiftsViewController *giftVC = [self.storyboard instantiateViewControllerWithIdentifier:@"giftViewC"];
   // [self.navigationController pushViewController:giftVC animated:YES];
}

-(void)seleAllClicked:(id)sender{
    NSLog(@"allOfferArrayList :%@",offerIDS);
    TopOfferViewController *topOfferVC = [self.storyboard instantiateViewControllerWithIdentifier:@"topOfferViewC"];
    topOfferVC.giftOfferID = offerIDS;//allOfferArrayList;
    [Session shared].isFromSearch = FALSE;
    topOfferVC.giftOfferName = offerNameStr;
    topOfferVC.giftOfferTextID = offerIDStr;
    topOfferVC.hidesBottomBarWhenPushed = TRUE;
    [self.navigationController pushViewController:topOfferVC animated:YES];
}

-(void)storeLocationClicked:(id)sender{
    StoreLocationViewController *storeLocationVC = [self.storyboard instantiateViewControllerWithIdentifier:@"storeViewC"];
    storeLocationVC.hidesBottomBarWhenPushed = TRUE;
    [self.navigationController pushViewController:storeLocationVC animated:YES];
}

-(void)imageTapped:(UITapGestureRecognizer *)recognizer{
    NSLog(@"#RecognizerTag :%ld",(long)recognizer.view.tag);

    GiftDetailViewController *giftDetailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"giftDetailViewC"];
    giftDetailVC.giftID = [dealArray objectAtIndex:recognizer.view.tag];
    giftDetailVC.giftOfferID = [offerIDArray objectAtIndex:recognizer.view.tag];
    giftDetailVC.shopName = [shopNameArray objectAtIndex:recognizer.view.tag]; //@"Cafe Coffee Day";
    giftDetailVC.hidesBottomBarWhenPushed = TRUE;
    [self.navigationController pushViewController:giftDetailVC animated:YES];
}

#pragma mark - SWReveal View Delegate

- (void)revealController:(SWRevealViewController *)revealController willMoveToPosition:(FrontViewPosition)position
{
    NSLog(@"#Position :%u",position);
    
    if (position == FrontViewPositionRight) {
    NSLog(@"Done Right");
        
    coverView =  [self onAddDimViewOnSuperView:self.view];
        
    }
    
    if (position == FrontViewPositionLeft) {
    NSLog(@"Done Left");
    [coverView removeFromSuperview];
    }
    
}

-(void)searchBtnClicked:(id)sender{
    NSLog(@"#Search");
}

#pragma mark - UI

-(void)setViewAccrodingToFrame{
    if([UIScreen mainScreen].bounds.size.width == 320){
    rechargeTxtLabel.frame = CGRectMake(self.view.frame.origin.x + 10, self.view.frame.origin.y + 20, self.view.frame.size.width - 20, 21);
        
    rechargeLabel.font = [UIFont fontWithName:@"OpenSans-Semibold" size:15];
    giftsLabel.font = [UIFont fontWithName:@"OpenSans-Semibold" size:15];
    coinLabel.font = [UIFont fontWithName:@"OpenSans-Semibold" size:15];
    rechargeLabel.frame = CGRectMake(self.view.frame.origin.x + 10,rechargeTxtLabel.frame.origin.y + rechargeTxtLabel.frame.size.height + 23, 80, 21);
        
    firstArrowButton.frame = CGRectMake(rechargeLabel.frame.origin.x + rechargeLabel.frame.size.width -5, rechargeTxtLabel.frame.origin.y + rechargeTxtLabel.frame.size.height + 25, 20, 20);
    coinLabel.frame = CGRectMake(firstArrowButton.frame.origin.x + firstArrowButton.frame.size.width -1,rechargeTxtLabel.frame.origin.y + rechargeTxtLabel.frame.size.height + 10 , 83, 50);
        
    secondArrowButton.frame = CGRectMake(coinLabel.frame.origin.x + coinLabel.frame.size.width + 3, rechargeTxtLabel.frame.origin.y + rechargeTxtLabel.frame.size.height + 25, 20, 20);

    giftsLabel.frame = CGRectMake(secondArrowButton.frame.origin.x + secondArrowButton.frame.size.width -8,rechargeTxtLabel.frame.origin.y + rechargeTxtLabel.frame.size.height + 10 , 115, 55);
        
    rechargeButton.frame = CGRectMake(firstArrowButton.frame.origin.x  - 20, rechargeLabel.frame.origin.y + rechargeLabel.frame.size.height + 40, 90, 110);
        
    
        
        
    giftButton.frame = CGRectMake(secondArrowButton.frame.origin.x  - 20, rechargeLabel.frame.origin.y + rechargeLabel.frame.size.height + 40, 90, 110);

    mapButtonView.frame = CGRectMake(0, rechargeButton.frame.origin.y + rechargeButton.frame.size.height + 20, self.view.frame.size.width, 52);
        
    seeAllButton.frame = CGRectMake(self.view.frame.size.width - 70, mapButtonView.frame.origin.y + mapButtonView.frame.size.height + 30, 60, 21);
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(seleAllClicked:)];
        tapGesture.delegate = self;
        
        [seeAllButton addGestureRecognizer:tapGesture];
        
        
        mapLocationButton.frame = CGRectMake(mapButtonView.frame.size.width - 40, 6, 30, 40);
        
        
        coinLabel.text = @"Collect \nGift Coins";
        giftsLabel.text = @"Exchange \nCoins for gifts";

       // coinLabel.textAlignment = NSTextAlignmentLeft;
        //giftsLabel.textAlignment = NSTextAlignmentLeft;

    }
    
    
}

-(void)getGifts{
    DBManager *dbManager = [[DBManager alloc]initWithDatabaseFilename:@"dbgiftdata.sql"];
    NSString*  query = [NSString stringWithFormat:@"select * from GIFTTABLE"];
    NSArray *theArray = [[NSArray alloc] initWithArray:[dbManager loadDataFromDB:query]];
    
    NSLog(@"#TheArray :%@",theArray);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
