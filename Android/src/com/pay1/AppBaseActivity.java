package com.pay1;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.app.FragmentActivity;

import com.pay1.constants.Constants;

public abstract class AppBaseActivity extends FragmentActivity {
	public static final String FINISH_ALL_ACTIVITIES_ACTIVITY_ACTION = "com.pay1.FINISH_ALL_ACTIVITIES_ACTIVITY_ACTION";
	private BaseActivityReceiver baseActivityReceiver = new BaseActivityReceiver();
	public static final IntentFilter INTENT_FILTER = createIntentFilter();

	private static IntentFilter createIntentFilter() {
		IntentFilter filter = new IntentFilter();
		filter.addAction(FINISH_ALL_ACTIVITIES_ACTIVITY_ACTION);
		return filter;
	}

	protected void registerBaseActivityReceiver() {
		registerReceiver(baseActivityReceiver, INTENT_FILTER);
	}

	protected void unRegisterBaseActivityReceiver() {
		unregisterReceiver(baseActivityReceiver);
	}

	public class BaseActivityReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction()
					.equals(FINISH_ALL_ACTIVITIES_ACTIVITY_ACTION)) {
				finish();
			}
		}
	}

	protected void closeAllActivities() {
		sendBroadcast(new Intent(FINISH_ALL_ACTIVITIES_ACTIVITY_ACTION));
		Intent intent = new Intent(AppBaseActivity.this, LoginActivity.class);
		startActivity(intent);
	}

	protected void closeAllActivitiesInApp() {
		sendBroadcast(new Intent(FINISH_ALL_ACTIVITIES_ACTIVITY_ACTION));
	}

	protected void closeAllActivitiesAfterRecharge(int rechargeType) {
		sendBroadcast(new Intent(FINISH_ALL_ACTIVITIES_ACTIVITY_ACTION));
		if (rechargeType == Constants.RECHARGE_DTH) {
			Intent intent = new Intent(AppBaseActivity.this,
					RechargeDthActivity.class);
			intent.putExtra(Constants.RECHARGE_FOR, Constants.RECHARGE_DTH);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
					| Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
		} else if (rechargeType == Constants.RECHARGE_MOBILE) {
			Intent intent = new Intent(AppBaseActivity.this,
					RechargeMobileActivity.class);
			intent.putExtra(Constants.RECHARGE_FOR, Constants.RECHARGE_MOBILE);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
					| Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
		} else if (rechargeType == Constants.RECHARGE_DATA) {
			Intent intent = new Intent(AppBaseActivity.this,
					RechargeMobileActivity.class);
			intent.putExtra(Constants.RECHARGE_FOR, Constants.RECHARGE_DATA);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
					| Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
		} else if (rechargeType == Constants.BILL_PAYMENT) {
			Intent intent = new Intent(AppBaseActivity.this,
					BillPaymentActivity.class);
			intent.putExtra(Constants.RECHARGE_FOR, Constants.BILL_PAYMENT);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
					| Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
		} else if (rechargeType == Constants.WALLET_PAYMENT) {
			Intent intent = new Intent(AppBaseActivity.this,
					WalletInfoActivity.class);
			startActivity(intent);
		} else if (rechargeType == Constants.SUBSCRIBE_DEAL) {
			Intent intent = new Intent(AppBaseActivity.this, DealActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
					| Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
		} else if (rechargeType == Constants.SUPPORT) {
			Intent intent = new Intent(AppBaseActivity.this, SupportActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
					| Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
		}
	}
}