package com.pay1.adapterhandler;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.pay1.R;

/***** Adapter class extends with ArrayAdapter ******/
public class SpinnerCustomAdapter extends ArrayAdapter<JSONObject> {

	private Context context;
	private ArrayList<JSONObject> data;
	private Typeface Reguler, Narrow, Narrow_thin;

	LayoutInflater inflater;

	public SpinnerCustomAdapter(Context context, int textViewResourceId,
			ArrayList<JSONObject> objects) {
		super(context, textViewResourceId, objects);

		this.context = context;
		data = objects;
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		Reguler = Typeface.createFromAsset(context.getAssets(),
				"EncodeSansNormal-400-Regular.ttf");

		Narrow = Typeface.createFromAsset(context.getAssets(),
				"EncodeSansNarrow-500-Medium.ttf");

		Narrow_thin = Typeface.createFromAsset(context.getAssets(),
				"EncodeSansNarrow-100-Thin.ttf");

	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		return getCustomView(position, convertView, parent);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return getCustomView(position, convertView, parent);
	}

	public View getCustomView(int position, View convertView, ViewGroup parent) {
		View row = inflater.inflate(R.layout.spinner_row, parent, false);
		TextView textOperator = (TextView) row.findViewById(R.id.textOperator);
		textOperator.setTypeface(Reguler);
		try {
			textOperator.setText(data.get(position).getString("name"));
		} catch (JSONException je) {

		}
		return row;
	}
}