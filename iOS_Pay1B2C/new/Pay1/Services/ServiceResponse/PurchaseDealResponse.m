//
//  PurchaseDealResponse.m
//  Pay1
//
//  Created by Annapurna on 23/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "PurchaseDealResponse.h"

@implementation PurchaseDealResponse

-(id)init{
    self = [super init];
    if(self) {
        self.dealArray = [[NSMutableArray alloc] init] ;
    }
    return self;
}

@end
