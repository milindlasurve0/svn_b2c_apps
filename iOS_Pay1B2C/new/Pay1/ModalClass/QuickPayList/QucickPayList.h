//
//  QucickPayList.h
//  Pay1
//
//  Created by Annapurna on 03/07/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface QucickPayList : NSObject

@property (nonatomic, strong) NSString *quickID;
@property (nonatomic, strong) NSString *qucikFlag;
@property (nonatomic, strong) NSString *quickName;
@property (nonatomic, strong) NSString *amount;
@property (nonatomic, strong) NSString *userID;
@property (nonatomic, strong) NSString *stv;
@property (nonatomic, strong) NSString *operatorID;
@property (nonatomic, strong) NSString *product;
@property (nonatomic, strong) NSString *phNumber;
@property (nonatomic, strong) NSString *delegateFlag;
@property (nonatomic, strong) NSString *missedNumber;
@property (nonatomic, strong) NSString *confirmation;
@property (nonatomic, strong) NSString *operatorName;
@property (nonatomic, strong) NSString *transactionFlag;
@property (nonatomic, strong) NSString *operatorCode;
@property (nonatomic, strong) NSString *dateTime;


@end
