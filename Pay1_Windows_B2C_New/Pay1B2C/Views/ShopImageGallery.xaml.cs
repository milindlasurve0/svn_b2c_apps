﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Pay1B2C.UserControls;
using System.Windows.Media.Imaging;

namespace Pay1B2C.Views
    {
    public partial class ShopImageGallery: PhoneApplicationPage
        {
        public ShopImageGallery()
            {
            InitializeComponent();
            }


        protected override void OnNavigatedTo(NavigationEventArgs e)
            {
            string index="0";
            if(NavigationContext.QueryString.ContainsKey("imageArray"))
                {
                string imageArray = NavigationContext.QueryString["imageArray"];
                if(NavigationContext.QueryString.ContainsKey("index"))
                    {
                    index = NavigationContext.QueryString["index"];
                    }
                if(string.IsNullOrEmpty(imageArray))
                    {
                    }
                else
                    {
                    var dict=(JArray)JsonConvert.DeserializeObject(imageArray);
                    PivotItem pivotItem=null;
                    foreach(var c in dict)
                        {
                        string imgUrl = (string)c;
                        FullImageHolder imgHolder=new FullImageHolder();
                        imgHolder.imageStore.Source=new BitmapImage(new Uri(imgUrl, UriKind.RelativeOrAbsolute));
                       // imageContainer.listBoxOffer.Items.Add(imgHolder);
                        pivotItem = new PivotItem();
                        pivotItem.Content = imgHolder;
                        infoPivot.Items.Add(pivotItem);
                        }

                    infoPivot.SelectedIndex=Convert.ToInt16(index)-1;
                    }
                }
            }
        }
    }