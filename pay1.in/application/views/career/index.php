<?php $this->load->view('common/doctype_html'); 
    $current_page_url =  "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    
    //fetching all the job deatils from using user_helper
    $allJobData = getAllJobDetails();
    foreach ($allJobData as $row){
        $job_title[] =  $row->job_title;
        $job_desc[] =  $row->job_desc;  
    }

    //fetching mete tag deatils from using user_helper
    $meteTagData = getMetaTagDetails();
    foreach ($meteTagData as $row){
      
      if($row->page_url == $current_page_url){
        $new_page_tittle =  $row->page_tittle;
        $new_page_meta_description =  $row->page_meta_description;  
        $new_page_meta_tag =  $row->page_meta_tag;  
      }
      
    }
?>
<meta name="description" content="<?php if($new_page_meta_description){ echo $new_page_meta_description; }else{ ?>Work at Pay1! Apply for a new job & launch a career with India's leading company. A job at Pay1 is not just a job, it offers unique opportunities for personal. <?php } ?>">
<meta name="keywords" content="<?php if($new_page_meta_tag){ echo $new_page_meta_tag; }else{ ?>Work with us , Careers , Pay1 - Recharges & Bill Payments <?php } ?>" />
<title><?php if($new_page_tittle){ echo $new_page_tittle; }else{ ?>Work with us | Careers | Pay1 - Recharges & Bill Payments <?php } ?> </title>
<?php $this->load->view('common/header');  ?>
      
        <link rel="stylesheet" href="/assets/css/style.css">
        <link rel="stylesheet" href="/assets/css/normalize.css">
        <link rel="stylesheet" href="/assets/css/media_query.css">
        <link rel="stylesheet" href="/assets/validation/validationEngine.jquery.css">


      <div class="clearfix" style="clear:both;"></div>
      <div class="career_page1"></div>
      <img src="/assets/images/background/careers.jpg" class="img-responsive" alt="">
      <div class="container-fluid">
         <div class="container border-both">
            <div class="row">
               <div class="col-sm-3 ">
                  <div class=''>
                     <div id="contact_navigation">
                        <ul class="tabs">
                           <li><a href="/about_us">About us</a></li>
                           <li><a href="/contact">Contact Us</a></li>
                           <li><a href="/terms">Terms & Conditions</a></li>
                           <!--<li><a href="#security" data-toggle="tab">Security Policy</a></li>-->
                           <li><a href="/faqs">FAQs</a></li>
                           <li><a href="/privacy-policy">Privacy Policy</a></li>
                           <li class="active"><a href="#careers" data-toggle="tab">Careers</a></li>
                           <li><a target="_blank" href="/partners-blog/" >Blog</a></li>
                           <li><a href="/sitemap">Sitemap</a></li>
                          
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="col-sm-9">
                  <div class='padding-both margin-top-30'>
                     <p class="heading_para">Careers</p>
                     <p class="lightitalic">Founded in 2011, the company has risen from a bunch of entusiastic co-founders to a team of quality
members who willing to work to make a difference with their presence.
                     </p>
                     <div class="mind_array padd_all col-md-12">
                       
                        <div class="clearfix"></div>
                        <div class="col-md-6 ">
                           <p class="heading_para">Working Here</p>
                          We believe in employee first attitude with the people
we are working with. An environment which is full 
of energy makes you believe in your own capabality.

                        </div>
                        <div class="col-md-6 ">
                            <div class="clearfix"></div>
                            <p class="heading_para">Our Culture</p>
                          We at Pay1 strongly believe in bridging the gap
between employer and employee. This not only 
helps to build a strong but a long lasting relation
between the two.
                        </div>
                     </div>
                  </div>
                  
                   <div class="col-md-12">
                     <div class="row">
                       <p class="heading_para margin-top-20">Join Us</p>
                        <div class="mind_array margin-top-30">
                            <form class="form" action="career/CareerData" id="test" method="POST" enctype="multipart/form-data" name="form1" onsubmit="return UploadResume(event)">
                              <h6 align="center"><label id="resumeSizeError" style="display:none;color:red">Please upload .doc having size less than 20kb.</label></h6>
                              <h6 align="center"><label id="resumeExtError" style="display:none;color:red">You can only upload a Word doc file with 'pdf', 'doc' & 'docx' extensions. </label></h6>
                              <h6 align="center"><label id="resumeSuccessMsg" style="display:none;color:blue">Your Resume sent successfully.</label></h6>
                              <table class="table border-none table-response contact_form">
                                <tr>
                                    <td><input type="text" name="name" placeholder="NAME" class="validate[required] form-control contact_input"></td>
                                    <td><input type="tel" pattern="[789][0-9]{9}" maxlength="10" name="phone" placeholder="PHONE" class="validate[required] form-control contact_input" required></td>
                                </tr>
                                 <tr>
                                     <td><input type="email" name="email" placeholder="EMAIL" class="validate[required] form-control contact_input" required></td>
                                    <td>
                                       <input type="file" name="resume" id="resume" class="form-control contact_input">
                                    </td>
                                 </tr>
                                 <tr>
                                    <td colspan="2" style="width:100%"><textarea rows="5" name="message" class="form-control validate[required] contact_input" placeholder="MESSAGE"></textarea></td>
                                 </tr>
                                 <tr>
                                     <td colspan="1"> <select id="job_title" name="job_title" class="form-control">
                                                            <option selected disabled>Current Job Listing</option>
                                                            <option value="Software Developer">Software Developer</option>
                                                            <option value="Relationship Manager Sales">Relationship Manager Sales</option>
                                                              <?php //print_r($job_title);
//                                                                    foreach ($job_title as $key => $status) { ?>
                                                                        <!--<option value="<?php // echo $key ;?>"><?php // echo $status   ;?></option>-->
                                                              <?php //   }    ?>
                                                        </select></td>  
                                 </tr> 
                                 <tr align="right">
                                    <td colspan="2" style="width:100%"><input type="submit" value="SUBMIT" class="btn  btn-submit validate[required]"></td>
                                 </tr>
                              </table>
                           </form>
                            
                            <div id="job_desc_div" style="padding-left: 15px;"></div>
                            
                            
                        </div>
                        <div class="margin-top-20 clearfix"></div>
                     </div>
                  </div>

 				  <div class="margin-top-30 clearfix"></div>
               </div>


            </div>
         </div>
      </div>
      <div class="clearfix"></div>
      
        <?php $this->load->view('common/footer'); ?>
      
      <div class="modal fade" role="dialog" id="failure">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="cls" data-dismiss="modal">&times;</button>
                  <h3>Payment Failure</h3>
               </div>
               <div class="modal-body">
                  <p>Sorry! Your previous payment attempt failed.</p>
                  <div class="pull-right">
                     <button class="modal_button" id="try-again">Try Again</button>
                     <button class="modal_button"id="cancel-button"data-dismiss="modal">Cancel</button>
                  </div>
                  <div class="clearfix"></div>
               </div>
            </div>
         </div>
      </div>
      <script src="/assets/js/bootstrap.min.js"></script>
      <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
      <script>
         $(document).ready(function(){
             $('[data-toggle="popover"]').popover();   
         });
      </script>
      <script src="/assets/validation/jquery.validationEngine.js"></script>
      <script src="/assets/validation/jquery.validationEngine-en.js"></script>
      <script type="text/javascript">
         $("#test").validationEngine();
      </script>
      <script src="/assets/js/jquery-scrollToTop.js"></script>
      <script type="text/javascript">
         $(document).ready(function($) {
             $('body').scrollToTop({skin: 'cycle'});
         });
         
        function UploadResume(ev){
            unsetUploadResume();
            var fsize = $('#resume')[0].files[0].size/ 1024; //get file size
            var fname = document.getElementById("resume");
            fname = fname.value;
            var extension = fname.split('.').pop().toLowerCase();
            var allowed = ['pdf', 'doc', 'docx'];
            if(fsize<=0 || fsize>500){
               $('#resumeSizeError').show();
               ev.preventDefault();
            }
            if(allowed.indexOf(extension) === -1){
               $('#resumeExtError').show();
               ev.preventDefault();
               //return false;
            }else{
              $('#resumeSuccessMsg').show();  
            }
           //return true;
        }
        function unsetUploadResume(){
           $('#resumeSizeError').hide();
           $('#resumeExtError').hide();
        }
        
        
        
        $('select').on('change', function() {
            var value = this.value;
//            alert( value ); // or $(this).val()
//            alert($("#job_title").val());
            var job_description = '';
               if(value === 'Software Developer'){
               job_description +='Software Developer having 1+ Exp. ';   
               }else if(value === 'Relationship Manager Sales'){
               job_description +='Relationship Manager Sales having 1+ Exp.';   
               }
            $('#job_desc_div').html(job_description);
        });
        

      </script>
