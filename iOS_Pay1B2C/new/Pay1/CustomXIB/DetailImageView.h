//
//  DetailImageView.h
//  Pay1
//
//  Created by Annapurna on 16/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailImageView : UIView{
    IBOutlet UIImageView *imageView;
    IBOutlet UIButton *closeButton;
   // IBOutlet UIScrollView *imageScrollView;
    
   // NSInteger imageTagValue;
    
   // CGPoint pointNow;
    
   // BOOL isMovingDirection;
    
  //  NSMutableArray *imageArray;
    
}
@property (nonatomic, copy) void (^onButtonClick)();
//-(void)setImageOnDetailView:(NSString *)imageURL;

-(void)setImageOnDetailView:(NSMutableArray *)imageURLArray tagValue:(NSInteger)tagValue;

@end
