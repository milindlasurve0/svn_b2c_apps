﻿#pragma checksum "F:\Pay1WP8\Pay1B2C\Pay1B2C\RegistrationLayout.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "43EF135D2600E6DB5F6253D8AE0CAC2D"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34014
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Microsoft.Phone.Controls;
using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace Pay1B2C {
    
    
    public partial class RegistrationLayout : Microsoft.Phone.Controls.PhoneApplicationPage {
        
        internal System.Windows.Controls.Grid LayoutRoot;
        
        internal System.Windows.Controls.Grid ContentPanel;
        
        internal System.Windows.Controls.WatermarkTextBox textMobileNumber;
        
        internal System.Windows.Controls.TextBlock textTerms;
        
        internal System.Windows.Controls.TextBlock textPrivacy;
        
        internal System.Windows.Controls.Button btnRegister;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/Pay1B2C;component/RegistrationLayout.xaml", System.UriKind.Relative));
            this.LayoutRoot = ((System.Windows.Controls.Grid)(this.FindName("LayoutRoot")));
            this.ContentPanel = ((System.Windows.Controls.Grid)(this.FindName("ContentPanel")));
            this.textMobileNumber = ((System.Windows.Controls.WatermarkTextBox)(this.FindName("textMobileNumber")));
            this.textTerms = ((System.Windows.Controls.TextBlock)(this.FindName("textTerms")));
            this.textPrivacy = ((System.Windows.Controls.TextBlock)(this.FindName("textPrivacy")));
            this.btnRegister = ((System.Windows.Controls.Button)(this.FindName("btnRegister")));
        }
    }
}

