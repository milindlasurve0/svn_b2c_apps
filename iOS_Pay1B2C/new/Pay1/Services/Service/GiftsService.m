//
//  GiftsService.m
//  Pay1
//
//  Created by Annapurna on 16/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "GiftsService.h"
#import "GiftResponse.h"
#import "GiftCriteria.h"
#import "GiftList.h"
#import "GiftCategoryList.h"
@implementation GiftsService

-(void)callGiftService:(GiftCriteria *)criteria callback:(giftResponse)callback{
    NSMutableDictionary *postData = [[NSMutableDictionary alloc]init];
    [postData setObject:criteria.type forKey:@"type"];
    [postData setObject:criteria.next forKey:@"next"];
    
    NSLog(@"#PostForGiftURL :%@",postData);
    
    RequestData *requestData = [[RequestData alloc]init];
    requestData.baseURL = kBaseURL;
    requestData.webServiceURL = kGiftServiceURL;
    requestData.postData = postData;
    requestData.showLoader = FALSE;
    
    requestData.requestSuccessBlock = ^(RequestData *request) {
        
    [self printResponse:request.responseData forRequest:@"GetGift Response"];
    if ([self handleNullResponse:request])
    return;
        
    callback(nil, [self parseGiftResponse:request]);
    };
    
    
    requestData.requestFailureBlock = ^(RequestData *request) {
    [self handleNetworkError:request.responseData];
    callback(request.responseData, nil);
    };
    
    [[ServiceManager sharedInstance] makeRequest:requestData];
}

-(GiftResponse *)parseGiftResponse:(RequestData *)request{
    GiftResponse *objGiftResponse = [[GiftResponse alloc]init];
    objGiftResponse.status = [request.responseData objectForKey:@"status"];
    objGiftResponse.errorCode = [request.responseData objectForKey:@"errCode"];
    NSMutableDictionary *descriptionDic = [request.responseData objectForKey:@"description"];
    NSMutableArray *offerArray = [descriptionDic valueForKey:@"offer_details"];
    
    for(NSMutableDictionary *dataDic in offerArray){
    GiftList *objGiftList = [[GiftList alloc]init];
    objGiftList.featured = [dataDic objectForKey:@"featured"];
    objGiftList.name = [dataDic objectForKey:@"name"];
    objGiftList.giftID = [dataDic objectForKey:@"id"];
    objGiftList.details = [dataDic objectForKey:@"details"];
    [objGiftResponse.giftArrayList addObject:objGiftList];
    }
    
    
    return objGiftResponse;
}



@end
