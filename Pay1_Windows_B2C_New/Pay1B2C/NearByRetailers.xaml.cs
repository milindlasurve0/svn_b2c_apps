﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Device.Location;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Controls.Maps;
using Microsoft.Phone.Controls.Maps.Platform;
using Microsoft.Phone.Shell;
using Pay1B2C.UserControls;
using Newtonsoft.Json;

using Newtonsoft.Json.Linq;
using Pay1B2C;
using Microsoft.Phone.Maps.Controls;
using Microsoft.Xna.Framework.Input.Touch;
using System.Windows.Input;
using System.Windows.Shapes;


namespace Pay1B2C
    {
    public partial class NearByRetailers: PhoneApplicationPage
        {
       
        ProgressBarManager ProgressBarManager;
        String url;
        double oldRadius;
        double oldLatitude;
        double oldLongitude;
        double oldCornerLatitude;
        double oldCornerLongitude;
        double zoomLevel;
        JArray dict;
        String shopTag="0";
        public NearByRetailers()
            {
            InitializeComponent();
            GoogleAnalytics.EasyTracker.GetTracker().SendView("ShopLocator");
            ProgressIndicator prog = new ProgressIndicator();
            ProgressBarManager = new ProgressBarManager();
            SystemTray.SetProgressIndicator(this, prog);
            ProgressBarManager.Hook(prog);
            zoomLevel=MyMap.ZoomLevel;
            TouchPanel.EnabledGestures = GestureType.FreeDrag;
          //  Touch.FrameReported += Touch_FrameReported;
            MyMap.Loaded += (a, b) =>
            {
            MyMap.Center=new GeoCoordinate(Constant.LoadPersistent<double>(Constant.SETTINGS_USER_LATTITUDE_DOUBLE), Constant.LoadPersistent<double>(Constant.SETTINGS_USER_LONGITUDE_DOUBLE));
            getAllPay1Shop(Constant.LoadPersistent<double>(Constant.SETTINGS_USER_LATTITUDE_DOUBLE).ToString(), Constant.LoadPersistent<double>(Constant.SETTINGS_USER_LONGITUDE_DOUBLE).ToString(),5);





            myCurrentLocation();
            };

            imgMyLocation.Tap+=(s, e) =>
            {
            MyMap.Center=new GeoCoordinate(Constant.LoadPersistent<double>(Constant.SETTINGS_USER_LATTITUDE_DOUBLE), Constant.LoadPersistent<double>(Constant.SETTINGS_USER_LONGITUDE_DOUBLE));
            };


            this.Loaded+=(s, e) =>
            {
            bool showNearByDialog=Constant.LoadPersistent<Boolean>("showNearByDialog");
            if(!showNearByDialog)
                {
                showDialogForFirstUser();
                }
            };
           
            }



         

        private void Touch_FrameReported(object sender, TouchFrameEventArgs e)
            {
            if(TouchPanel.IsGestureAvailable) // check only dragging 
                {
                // get point relative to Viewport
                TouchPoint mainTouch = e.GetPrimaryTouchPoint(MyMap);
                // drag started Key - down
                if(mainTouch.Action == TouchAction.Down)
                    {
                    }

                if(mainTouch.Action == TouchAction.Move)
                    {
                    }
                  

                // check if drag has completed (key up)
                if(mainTouch.Action == TouchAction.Up)
                    {
                    CallNearByRetailersApiOnDrag(false);
                    }
                }
            }



        public void showDialogForFirstUser()
            {

            CheckBox checkBox = new CheckBox()
            {
                Content = "Don't show again",
                Margin = new Thickness(0, 14, 0, -2)
            };

            TiltEffect.SetIsTiltEnabled(checkBox, true);


            //Create a new custom message box
            CustomMessageBox messageBox = new CustomMessageBox()
            {
                Caption = "NEAR BY PAY1 STORE",
                Message ="Pay cash at your nearest Pay1 Store to add money to your wallet.",
                Content = checkBox,
                LeftButtonContent = "ok",
                RightButtonContent = "cancel",
                IsFullScreen = false
            };

            //Define the dismissed event handler
            messageBox.Dismissed += (s1, e1) =>
            {
                switch(e1.Result)
                    {
                    case CustomMessageBoxResult.LeftButton:
                        if((bool)checkBox.IsChecked)
                            {
                            // Do not launch Marketplace review task.
                            //add the isolated settings here
                            // Do not ask again.
                            Constant.SavePersistent("showNearByDialog", true);
                            }
                        else
                            {
                            // Do not launch Marketplace review task.
                            //add the isolated settings here
                            // Ask again later.
                            }
                      
                        break;
                    case CustomMessageBoxResult.RightButton:
                    case CustomMessageBoxResult.None:
                        if((bool)checkBox.IsChecked)
                            {
                            // Do not launch Marketplace review task.
                            //add the isolated settings here
                            // Do not ask again.
                            }
                        else
                            {
                            // Do not launch Marketplace review task.
                            //add the isolated settings here
                            // Ask again later.
                            }
                        break;
                    default:
                        break;
                    }
            };

            //launch the task
            messageBox.Show();  

            }

        private void CallNearByRetailersApiOnDrag(bool isZooming)
            {
            LocationRectangle MapBoundsArea = GetVisibleMapArea();
            String lat =Constant.LoadPersistent<string>("last_latitude");
            String lng = Constant.LoadPersistent<string>("last_longitude");

            double c_lat = MapBoundsArea.Center.Latitude;
            double c_lng = MapBoundsArea.Center.Longitude;
            Constant.SavePersistent("last_latitude", c_lat.ToString());
            Constant.SavePersistent("last_longitude", c_lng.ToString());

            if(!string.IsNullOrEmpty(lat)&&!string.IsNullOrEmpty(lng))
                {




                var sCoord = new GeoCoordinate(Double.Parse(lat), Double.Parse(lng));
                var eCoord = new GeoCoordinate(MapBoundsArea.Center.Latitude, MapBoundsArea.Center.Longitude);

                double d = sCoord.GetDistanceTo(eCoord) / 1000;


                var sCoord1 = new GeoCoordinate(MapBoundsArea.Northwest.Latitude, MapBoundsArea.Northwest.Longitude);
                var eCoord1 = new GeoCoordinate(MapBoundsArea.Center.Latitude, MapBoundsArea.Center.Longitude);

                double r = sCoord1.GetDistanceTo(eCoord1) / 1000;

                if(((d > (r / 2)) && (r > 0))||isZooming)
                    {
                    System.Threading.ThreadPool.QueueUserWorkItem(obj =>
                    {
                        System.Threading.Thread.Sleep(3000);

                        Dispatcher.BeginInvoke(() =>
                        {

                            getAllPay1Shop(c_lat.ToString(), c_lng.ToString(), r);
                        });
                    });

                    }

                }
            else
                {
                oldLatitude = MapBoundsArea.Center.Latitude;
                oldLongitude = MapBoundsArea.Center.Longitude;
                oldCornerLatitude = MapBoundsArea.Northwest.Latitude;
                oldCornerLongitude = MapBoundsArea.Northwest.Longitude;
                var sCoord = new GeoCoordinate(oldLatitude, oldLongitude);
                var eCoord = new GeoCoordinate(oldCornerLatitude, oldCornerLongitude);

                oldRadius = sCoord.GetDistanceTo(eCoord) / 1000;

                getAllPay1Shop(Constant.LoadPersistent<string>(Constant.SETTINGS_USER_LATTITUDE), Constant.LoadPersistent<string>(Constant.SETTINGS_USER_LONGITUDE), oldRadius);
                }
            }

        private void buttonImage_Click_Back(object sender, RoutedEventArgs e)
            {
           
            NavigationService.GoBack();
            }


        
        private void MapResolveCompleted(object sender, MapResolveCompletedEventArgs e)
            {
               CallNearByRetailersApiOnDrag(false);
            }


        public LocationRectangle GetVisibleMapArea()
            {
            GeoCoordinate mCenter = MyMap.Center;
            Point pCenter = MyMap.ConvertGeoCoordinateToViewportPoint(mCenter);
            GeoCoordinate topLeft = MyMap.ConvertViewportPointToGeoCoordinate(new Point(0, 0));
            GeoCoordinate bottomRight = MyMap.ConvertViewportPointToGeoCoordinate(new Point(MyMap.ActualWidth, MyMap.ActualHeight));

            if(topLeft != null && bottomRight != null)
                {
                Point pNW = new Point(pCenter.X - MyMap.ActualWidth / 2, pCenter.Y - MyMap.ActualHeight / 2);
                Point pSE = new Point(pCenter.X + MyMap.ActualWidth / 2, pCenter.Y + MyMap.ActualHeight / 2);
                if(pNW != null && pSE != null)
                    {
                    GeoCoordinate gcNW = MyMap.ConvertViewportPointToGeoCoordinate(pNW);
                    GeoCoordinate gcSE = MyMap.ConvertViewportPointToGeoCoordinate(pSE);
                    return new LocationRectangle(gcNW, gcSE);
                    }
                }

            return null;


            }






        private async void getAllPay1Shop(string latitude1, string longitude1,double dis1)
            {
            shopTag="0";
            url="https://panel.pay1.in/apis/receiveWeb/mindsarray/mindsarray/json?method=getNearByRetailer&lat="+latitude1+"&lng="+longitude1+"&mobile="+Constant.getUserNumber()+"&distance="+dis1;

            ProgressBarManager.Show(27, "Please wait...", false, 10);
            Task<string> response = Constant.postParamsAndgetResponse(url);


            string result = await response;
            if(string.IsNullOrEmpty(result))
                {
                NavigationService.Navigate(new Uri("/Views/NoConnection.xaml?", UriKind.Relative));
                }
            else
                {
                string output = result.Remove(result.Length - 1, 1);
                string output1 = output.Remove(output.Length - 1, 1);
                string output2 = output1.Remove(0, 1);
                string output3 = output2.Remove(0, 1);
                string output4 = output3.Remove(output3.Length - 1, 1);
                dict = (JArray)JsonConvert.DeserializeObject(output4);
                bool isFirst = true;
               loadAllShopsOnMap(dict);
             
                }


            ProgressBarManager.Hide(27);
            }

        private void loadAllShopsOnMap(JArray dict)
            {
            MyMap.Layers.Clear();
            foreach(var shops in dict)
                {
                var shopsJobject = shops;

                var shopsDetails = shopsJobject["t"];

                string mobile = shopsDetails["mobile"].ToString();
                string shopname = shopsDetails["shopname"].ToString();
                string sale = shopsDetails["sale"].ToString();

                string latitude = shopsDetails["latitude"].ToString();
                string longitude = shopsDetails["longitude"].ToString();

                string pin = shopsDetails["pin"].ToString();
                string area_name = shopsDetails["area_name"].ToString();
                string city_name = shopsDetails["city_name"].ToString();
                string state_name = shopsDetails["state_name"].ToString();
                string distance = shopsDetails["D"].ToString();
                string address = shopsDetails["address"].ToString();

                string user_id = shopsDetails["user_id"].ToString();


                double dis = Convert.ToDouble(distance);
                if(dis < 1)
                    {
                    dis = dis * 1000;
                    distance = dis.ToString("#.##") + " M";
                    }
                else
                    {
                    distance = dis.ToString("#.##") + " KM";
                    }


                loadAllShopsOnMap(latitude, longitude, shopname, address + ",\n" + area_name + "," + city_name + ",\n" + state_name + ", " + pin, true, 0, (JObject)shopsDetails);
                }
            myCurrentLocation();
            }

      /*  private void loadAllShopsOnMap(string latitude, string longitude, string shopname, string shopAddress, bool p2, int p3, JObject jObject)
            {
            Uri uri = new Uri("/Images/ic_home_locate.png", UriKind.RelativeOrAbsolute);
            BitmapImage imgSource = new BitmapImage(uri);

            ShopNameWithIcon ShopNameWithIcon=new UserControls.ShopNameWithIcon();
            Uri shopImgUri = new Uri(jObject["imagepath"].ToString(), UriKind.RelativeOrAbsolute);
            BitmapImage shopImgSource = new BitmapImage(shopImgUri);
            ShopNameWithIcon.shopImage.Source=shopImgSource;
            ShopNameWithIcon.shopNameAddress.Text=shopname+" \n "+shopAddress;
           
            MapOverlay overlay = new MapOverlay
            {
                GeoCoordinate = new GeoCoordinate(Double.Parse(latitude), Double.Parse(longitude)),
               
                Content=ShopNameWithIcon,
             
            };


            Microsoft.Phone.Maps.Controls.MapLayer layer = new Microsoft.Phone.Maps.Controls.MapLayer();
            layer.Add(overlay);

        
            MyMap.Layers.Add(layer);
            }*/

        public void myCurrentLocation(){

        Ellipse myCircle = new Ellipse();
        myCircle.Fill = new SolidColorBrush(Colors.Blue);
        myCircle.Height = 20;
        myCircle.Width = 20;
        myCircle.Opacity = 50;

            MapOverlay overlay = new MapOverlay
            {
                GeoCoordinate = new GeoCoordinate(Constant.LoadPersistent<double>(Constant.SETTINGS_USER_LATTITUDE_DOUBLE), Constant.LoadPersistent<double>(Constant.SETTINGS_USER_LONGITUDE_DOUBLE)),

                Content=myCircle,
                PositionOrigin = new Point(0.0, 1.0)
            };


            Microsoft.Phone.Maps.Controls.MapLayer layer = new Microsoft.Phone.Maps.Controls.MapLayer();
            layer.Add(overlay);


            MyMap.Layers.Add(layer);
            }


        private void loadAllShopsOnMap(string latitude, string longitude, string shopname, string shopAddress, bool p2, int p3, JObject jObject)
            {

            UCCustomToolTip customToolTip=new UCCustomToolTip();
            customToolTip.mainName.Text=shopname;
             Uri shopImgUri = new Uri(jObject["imagepath"].ToString(), UriKind.RelativeOrAbsolute);
            BitmapImage shopImgSource = new BitmapImage(shopImgUri);
            customToolTip.retailerImage.Source=shopImgSource;
            customToolTip.Description=shopAddress;
         
            customToolTip.borderPanel.Tap+=(s, e) =>
            {
            PhoneApplicationService.Current.State[Constant.SHOP_DATA]=jObject.ToString();
            NavigationService.Navigate(new Uri("/Views/ShopInfo.xaml?",UriKind.RelativeOrAbsolute));
            };


            if(shopTag.Equals("0"))
                {
                customToolTip.borderPanel.Visibility = Visibility.Collapsed;
                }
            else if(shopTag.Equals(jObject["mobile"].ToString()))
                {
                customToolTip.borderPanel.Visibility = Visibility.Visible;
                }
            else
                {
                customToolTip.borderPanel.Visibility = Visibility.Collapsed;
                }

            customToolTip.imgmarker.Tap+=(a,b)=>{


            if(customToolTip.borderPanel.Visibility == Visibility.Collapsed)
                {
                customToolTip.borderPanel.Tag=jObject["mobile"].ToString();
                shopTag=customToolTip.borderPanel.Tag.ToString();
                loadAllShopsOnMap(dict);

                customToolTip.borderPanel.Visibility = Visibility.Visible;
                //borderPanel.Opacity = 1;
                //imgborder.Opacity = 1;
                }
            else
                {
                customToolTip.borderPanel.Visibility = Visibility.Collapsed;
                //borderPanel.Opacity = 0;
                //imgborder.Opacity = 0;
                }

           
           
           
          
                };

            
         /*   Uri uri = new Uri("/Images/ic_home_locate.png", UriKind.RelativeOrAbsolute);
            BitmapImage imgSource = new BitmapImage(uri);

            ShopNameWithIcon ShopNameWithIcon=new UserControls.ShopNameWithIcon();
           
            ShopNameWithIcon.shopImage.Source=shopImgSource;
            ShopNameWithIcon.shopNameAddress.Text=shopname+" \n "+shopAddress;
            */
            MapOverlay overlay = new MapOverlay
            {
                GeoCoordinate = new GeoCoordinate(Double.Parse(latitude), Double.Parse(longitude)),

                Content=customToolTip,
                PositionOrigin = new Point(0.0, 1.0)
            };


            Microsoft.Phone.Maps.Controls.MapLayer layer = new Microsoft.Phone.Maps.Controls.MapLayer();
            layer.Add(overlay);


            MyMap.Layers.Add(layer);
            }


        private void MyMap_CenterChanged(object sender, MapCenterChangedEventArgs e)
            {
            //  CallNearByRetailersApiOnDrag(false);
            }

        private void MyMap_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
           {
           //CallNearByRetailersApiOnDrag(false);
           }

        private void MyMap_ZoomLevelChanged(object sender, MapZoomLevelChangedEventArgs e)
            {
          
            if(zoomLevel-MyMap.ZoomLevel>1)
                {
                zoomLevel=MyMap.ZoomLevel;
                CallNearByRetailersApiOnDrag(true);
                }
            else if(zoomLevel-MyMap.ZoomLevel<-1)
                {
                zoomLevel=MyMap.ZoomLevel;
                CallNearByRetailersApiOnDrag(true);
                }
            }

        
        }
    }