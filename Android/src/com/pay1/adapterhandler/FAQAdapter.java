package com.pay1.adapterhandler;

import java.util.ArrayList;
import java.util.WeakHashMap;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.pay1.R;
import com.pay1.FAQActivity;

public class FAQAdapter extends BaseAdapter {

	private Context context;
	private ArrayList<WeakHashMap<String, String>> data;
	private Typeface Reguler, Narrow, Narrow_thin;

	public FAQAdapter(Context context,
			ArrayList<WeakHashMap<String, String>> data) {
		this.context = context;
		this.data = data;
		Reguler = Typeface.createFromAsset(context.getAssets(),
				"EncodeSansNormal-400-Regular.ttf");

		Narrow = Typeface.createFromAsset(context.getAssets(),
				"EncodeSansNarrow-500-Medium.ttf");

		Narrow_thin = Typeface.createFromAsset(context.getAssets(),
				"EncodeSansNarrow-100-Thin.ttf");
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder viewHolder;
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.faq_adapter, null);
			viewHolder = new ViewHolder();

			viewHolder.textView_Question = (TextView) convertView
					.findViewById(R.id.textView_Question);
			viewHolder.textView_Question.setTypeface(Narrow);
			viewHolder.textView_Answer = (TextView) convertView
					.findViewById(R.id.textView_Address);
			viewHolder.textView_Answer.setTypeface(Narrow_thin);
			convertView.setTag(viewHolder);

		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		WeakHashMap<String, String> map = new WeakHashMap<String, String>();
		map = data.get(position);

		viewHolder.textView_Question.setText(map.get(FAQActivity.QUESTION)
				.toString().trim());

		viewHolder.textView_Answer.setText(map.get(FAQActivity.ANSWER)
				.toString().trim());

		return convertView;
	}

	private class ViewHolder {
		TextView textView_Question, textView_Answer;
	}
}
