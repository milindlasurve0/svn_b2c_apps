package com.pay1;

import java.net.URLEncoder;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.larvalabs.svgandroid.SVGParser;
import com.pay1.constants.Constants;
import com.pay1.customviews.MyProgressDialog;
import com.pay1.requesthandler.RequestClass;
import com.pay1.utilities.EditTextValidator;
import com.pay1.utilities.Utility;

public class ChangePinActivity extends Activity {

	private static final String SCREEN_LABEL = "Change Password Screen";
	private EasyTracker easyTracker = null;

	private final String TAG = "Reset Password";
	private ImageView imageView_Back;
	private Button button_Confirm, button_Cancel;
	private EditText editText_OldPin, editText_NewPin, editText_ConfirmPin;
	private TextView textView_Title, textView_Msg, textView_TitleOldPin,
			textView_TitleNewPin, textView_TitleConfirmPin;
	private boolean is_new = false;

	@SuppressLint("NewApi")
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.change_pin_activity);
		try {

			try {
				is_new = getIntent().getBooleanExtra("IS_NEW", false);
			} catch (Exception e) {
			}
			easyTracker = EasyTracker.getInstance(ChangePinActivity.this);
			easyTracker.set(Fields.SCREEN_NAME, SCREEN_LABEL);

			Typeface Reguler = Typeface.createFromAsset(getAssets(),
					"Roboto-Regular.ttf");
			Typeface Medium = Typeface.createFromAsset(getAssets(),
					"Roboto-Regular.ttf");

			textView_Title = (TextView) findViewById(R.id.textView_Title);
			textView_Title.setTypeface(Medium);
			textView_Msg = (TextView) findViewById(R.id.textView_Msg);
			textView_Msg.setTypeface(Medium);
			textView_Msg
					.setText("You will receive an OTP via SMS. Kindly enter below."
							+ Utility.getMobileNumber(ChangePinActivity.this,
									Constants.SHAREDPREFERENCE_MOBILE));
			textView_TitleOldPin = (TextView) findViewById(R.id.textView_TitleOldPin);
			textView_TitleOldPin.setTypeface(Medium);
			textView_TitleOldPin.setVisibility(View.GONE);
			textView_TitleNewPin = (TextView) findViewById(R.id.textView_TitleNewPin);
			textView_TitleNewPin.setTypeface(Medium);
			textView_TitleNewPin.setVisibility(View.GONE);
			textView_TitleConfirmPin = (TextView) findViewById(R.id.textView_TitleConfirmPin);
			textView_TitleConfirmPin.setTypeface(Medium);
			textView_TitleConfirmPin.setVisibility(View.GONE);

			RelativeLayout back_layout = (RelativeLayout) findViewById(R.id.back_layout);
			back_layout.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onBackPressed();
				}
			});

			editText_OldPin = (EditText) findViewById(R.id.editText_OldPin);
			editText_OldPin.setTypeface(Reguler);
			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(editText_OldPin.getWindowToken(), 0);
			editText_OldPin.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					// TODO Auto-generated method stub
					textView_Msg.setVisibility(View.GONE);
					if (editText_OldPin.getText().toString().trim().length() != 0) {
						textView_TitleOldPin.setVisibility(View.GONE);
					} else {
						textView_TitleOldPin.setVisibility(View.GONE);
					}
					if (editText_OldPin.getText().toString().trim().length() > 0)
						EditTextValidator.hasText(ChangePinActivity.this,
								editText_OldPin,
								Constants.ERROR_PIN_BLANK_FIELD);
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					// TODO Auto-generated method stub

				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

				}
			});

			editText_NewPin = (EditText) findViewById(R.id.editText_NewPin);
			editText_NewPin.setTypeface(Reguler);
			editText_NewPin.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					// TODO Auto-generated method stub
					if (editText_NewPin.getText().toString().trim().length() != 0) {
						textView_TitleNewPin.setVisibility(View.GONE);
					} else {
						textView_TitleNewPin.setVisibility(View.GONE);
					}
					if (editText_NewPin.getText().toString().trim().length() > 0)
						EditTextValidator.hasText(ChangePinActivity.this,
								editText_NewPin,
								Constants.ERROR_PIN_BLANK_FIELD);
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					// TODO Auto-generated method stub

				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

				}
			});

			editText_ConfirmPin = (EditText) findViewById(R.id.editText_ConfirmPin);
			editText_ConfirmPin.setTypeface(Reguler);
			editText_ConfirmPin.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					// TODO Auto-generated method stub
					if (editText_ConfirmPin.getText().toString().trim()
							.length() != 0) {
						textView_TitleConfirmPin.setVisibility(View.GONE);
					} else {
						textView_TitleConfirmPin.setVisibility(View.GONE);
					}
					if (editText_ConfirmPin.getText().toString().trim()
							.length() > 0)
						EditTextValidator.hasText(ChangePinActivity.this,
								editText_ConfirmPin,
								Constants.ERROR_PIN_BLANK_FIELD);
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					// TODO Auto-generated method stub

				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

				}
			});

			editText_ConfirmPin
					.setOnEditorActionListener(new TextView.OnEditorActionListener() {

						@Override
						public boolean onEditorAction(TextView v, int actionId,
								KeyEvent event) {
							// TODO Auto-generated method stub
							if (actionId == EditorInfo.IME_ACTION_GO) {
								try {
									if (checkValidation(ChangePinActivity.this)) {
										InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
										imm.hideSoftInputFromWindow(
												v.getWindowToken(), 0);
										if (editText_OldPin
												.getText()
												.toString()
												.trim()
												.equalsIgnoreCase(
														editText_NewPin
																.getText()
																.toString()
																.trim())) {
											Constants
													.showOneButtonDialog(
															ChangePinActivity.this,
															TAG,
															"Current Password and Account Password must be different.",
															Constants.DIALOG_CLOSE);

										} else if (editText_OldPin
												.getText()
												.toString()
												.trim()
												.equalsIgnoreCase(
														editText_NewPin
																.getText()
																.toString()
																.trim())) {
											Constants
													.showOneButtonDialog(
															ChangePinActivity.this,
															TAG,
															"Account Password and Retype Password must be same.",
															Constants.DIALOG_CLOSE);

										} else {
											new ChangePinTask().execute(
													Utility.getMobileNumber(
															ChangePinActivity.this,
															Constants.SHAREDPREFERENCE_MOBILE),
													editText_OldPin.getText()
															.toString().trim(),
													editText_NewPin.getText()
															.toString().trim(),
													editText_ConfirmPin
															.getText()
															.toString().trim());
										}
									}
								} catch (Exception e) {
									// e.printStackTrace();
								}
								return true;
							}
							return false;
						}
					});

			imageView_Back = (ImageView) findViewById(R.id.imageView_Back);
			imageView_Back.setImageDrawable(SVGParser.getSVGFromResource(
					getResources(), R.raw.ic_back_old).createPictureDrawable());
			if (Build.VERSION.SDK_INT >= 11)
				imageView_Back.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
			imageView_Back.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onBackPressed();
				}
			});

			button_Confirm = (Button) findViewById(R.id.button_Confirm);
			button_Confirm.setTypeface(Reguler);
			button_Confirm.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (checkValidation(ChangePinActivity.this)) {
						InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
						imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
						if (editText_OldPin
								.getText()
								.toString()
								.trim()
								.equalsIgnoreCase(
										editText_NewPin.getText().toString()
												.trim())) {
							Constants
									.showOneButtonDialog(
											ChangePinActivity.this,
											TAG,
											"Current Password and New Password must be different.",
											Constants.DIALOG_CLOSE);

						} else if (!(editText_NewPin.getText().toString()
								.trim().equalsIgnoreCase(editText_ConfirmPin
								.getText().toString().trim()))) {
							Constants
									.showOneButtonDialog(
											ChangePinActivity.this,
											TAG,
											"New Password and Retype Password must be same.",
											Constants.DIALOG_CLOSE);

						} else {
							new ChangePinTask().execute(
									Utility.getMobileNumber(
											ChangePinActivity.this,
											Constants.SHAREDPREFERENCE_MOBILE),
									editText_OldPin.getText().toString().trim(),
									editText_NewPin.getText().toString().trim(),
									editText_ConfirmPin.getText().toString()
											.trim());
						}
					}
				}
			});

			button_Cancel = (Button) findViewById(R.id.button_Cancel);
			button_Cancel.setTypeface(Reguler);
			button_Cancel.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					editText_OldPin.setText("");
					editText_NewPin.setText("");
					editText_ConfirmPin.setText("");
					textView_Msg.setVisibility(View.VISIBLE);
					new ForgotPasswordTask().execute(Utility.getMobileNumber(
							ChangePinActivity.this,
							Constants.SHAREDPREFERENCE_MOBILE));
				}
			});

		} catch (Exception e) {
		}

		overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}

	@Override
	protected void onNewIntent(Intent intent) {
		// TODO Auto-generated method stub
		super.onNewIntent(intent);
		String opt = null;
		Bundle bundle = intent.getExtras();
		if (bundle != null)
			opt = bundle.getString("OTP").trim();

		editText_OldPin.setText(opt);
		textView_Msg.setVisibility(View.GONE);

	}

	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView().build());
		// EasyTracker.getInstance(this).activityStart(this);
		easyTracker.send(MapBuilder.createAppView().build());
	}

	@Override
	public void onStop() {
		super.onStop();
		// EasyTracker.getInstance(this).activityStop(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
	}

	private boolean checkValidation(Context context) {
		boolean ret = true;

		if (!EditTextValidator.hasText(context, editText_OldPin,
				Constants.ERROR_LOGIN_PIN_BLANK_FIELD)) {
			ret = false;
			editText_OldPin.requestFocus();
			return ret;
		} else if (!EditTextValidator.hasText(context, editText_NewPin,
				Constants.ERROR_LOGIN_PIN_BLANK_FIELD)) {
			ret = false;
			editText_NewPin.requestFocus();
			return ret;
		} else if (!EditTextValidator.hasText(context, editText_ConfirmPin,
				Constants.ERROR_LOGIN_PIN_BLANK_FIELD)) {
			ret = false;
			editText_ConfirmPin.requestFocus();
			return ret;
		} else if (!EditTextValidator.isValidPin(context, editText_OldPin,
				Constants.ERROR_LOGIN_PIN_VALID_FIELD)) {
			ret = false;
			editText_OldPin.requestFocus();
			return ret;
		} else if (!EditTextValidator.isValidPin(context, editText_NewPin,
				Constants.ERROR_LOGIN_PIN_VALID_FIELD)) {
			ret = false;
			editText_NewPin.requestFocus();
			return ret;
		} else if (!EditTextValidator.isValidPin(context, editText_ConfirmPin,
				Constants.ERROR_LOGIN_PIN_VALID_FIELD)) {
			ret = false;
			editText_ConfirmPin.requestFocus();
			return ret;
		} else
			return ret;
	}

	public class ChangePinTask extends AsyncTask<String, String, String>
			implements OnDismissListener {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			try {

				String response = RequestClass.getInstance()
						.readPay1B2CRequest(
								ChangePinActivity.this,
								Constants.B2C_URL
										+ "update_password/?mobile_number="
										+ params[0] + "&current_password="
										+ URLEncoder.encode(params[1], "utf-8")
										+ "&password="
										+ URLEncoder.encode(params[2], "utf-8")
										+ "&confirm_password="
										+ URLEncoder.encode(params[3], "utf-8")
										+ "&code="
										+ URLEncoder.encode(params[1], "utf-8")
										+ "&vtype=1&api_version="
										+ Constants.API_VERSION);

				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			try {

				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONObject jsonObject = new JSONObject(replaced);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {

						Utility.setLoginFlag(ChangePinActivity.this,
								Constants.SHAREDPREFERENCE_IS_LOGIN, true);

						JSONObject jsonObject2 = jsonObject
								.getJSONObject("description");

						Utility.setUserName(ChangePinActivity.this,
								Constants.SHAREDPREFERENCE_NAME, jsonObject2
										.getString("name").trim());
						Utility.setMobileNumber(ChangePinActivity.this,
								Constants.SHAREDPREFERENCE_MOBILE, jsonObject2
										.getString("mobile").trim());
						Utility.setEmail(ChangePinActivity.this,
								Constants.SHAREDPREFERENCE_EMAIL, jsonObject2
										.getString("email").trim());
						Utility.setGender(ChangePinActivity.this,
								Constants.SHAREDPREFERENCE_GENDER, jsonObject2
										.getString("gender").trim());
						Utility.setUserID(ChangePinActivity.this,
								Constants.SHAREDPREFERENCE_USER_ID, jsonObject2
										.getString("user_id").trim());
						Utility.setBalance(ChangePinActivity.this,
								Constants.SHAREDPREFERENCE_BALANCE, jsonObject2
										.getString("wallet_balance").trim());
						Utility.setPin(ChangePinActivity.this,
								Constants.SHAREDPREFERENCE_PIN, editText_NewPin
										.getText().toString().trim());
						Utility.setDOB(ChangePinActivity.this,
								Constants.SHAREDPREFERENCE_DOB, jsonObject2
										.getString("date_of_birth").trim());
						Utility.setLatitude(ChangePinActivity.this,
								Constants.SHAREDPREFERENCE_LATITUDE,
								jsonObject2.getString("latitude").trim());
						Utility.setLongitude(ChangePinActivity.this,
								Constants.SHAREDPREFERENCE_LONGITUDE,
								jsonObject2.getString("longitude").trim());
						Utility.setSupportNumber(ChangePinActivity.this,
								Constants.SHAREDPREFERENCE_SUPPORT_NUMBER,
								jsonObject2.getString("support_number").trim());
						Utility.setCookieName(ChangePinActivity.this,
								Constants.SHAREDPREFERENCE_COOKIE_NAME,
								jsonObject2.getString("user_val_name"));
						Utility.setCookieValue(ChangePinActivity.this,
								Constants.SHAREDPREFERENCE_COOKIE_VALUE,
								jsonObject2.getString("user_val"));

						// JSONObject jsonObjectTemplate = jsonObject2
						// .getJSONObject("ErrorMsg");
						// Utility.setTemplate(
						// ChangePinActivity.this,
						// Constants.SHAREDPREFERENCE_SMS_BILLPAYMENT_FAILURE,
						// jsonObjectTemplate
						// .getString("BILLPAYMENT_FAILURE"));
						// Utility.setTemplate(
						// ChangePinActivity.this,
						// Constants.SHAREDPREFERENCE_SMS_RECHARGE_FAILURE,
						// jsonObjectTemplate
						// .getString("RECHARGE_FAILURE"));
						// Utility.setTemplate(
						// ChangePinActivity.this,
						// Constants.SHAREDPREFERENCE_SMS_WALLET_REFILLED_RETAILER,
						// jsonObjectTemplate
						// .getString("WALLET_REFILLED_RETAILER"));

						Utility.setSharedPreferences(
								ChangePinActivity.this,
								Constants.SHAREDPREFERENCE_SERVICE_CHARGE_PERCENT,
								jsonObject2.getString("service_charge_percent")
										.trim());

						Utility.setSharedPreferences(ChangePinActivity.this,
								Constants.SHAREDPREFERENCE_SERVICE_TAX_PERCENT,
								jsonObject2.getString("service_tax_percent")
										.trim());

						final Dialog dialog = new Dialog(ChangePinActivity.this);
						dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
						dialog.getWindow().setBackgroundDrawableResource(
								android.R.color.transparent);
						dialog.setContentView(R.layout.dialog_one_button);
						dialog.setCancelable(false);
						dialog.setCanceledOnTouchOutside(false);
						// dialog.setTitle(null);

						// set the custom dialog components - text, image and
						// button
						Typeface Reguler = Typeface.createFromAsset(
								getAssets(), "Roboto-Regular.ttf");

						Typeface Medium = Typeface.createFromAsset(getAssets(),
								"Roboto-Regular.ttf");

						// set the custom dialog components - text, image and
						// button
						TextView textView_Title = (TextView) dialog
								.findViewById(R.id.textView_Title);
						textView_Title.setText(TAG);
						textView_Title.setTypeface(Reguler);

						TextView textView_Message = (TextView) dialog
								.findViewById(R.id.textView_Message);
						textView_Message
								.setText("Password updated successfully.");
						textView_Message.setTypeface(Medium);

						Button button_Ok = (Button) dialog
								.findViewById(R.id.button_Ok);
						button_Ok.setTypeface(Reguler);
						// if button is clicked, close the custom dialog
						button_Ok
								.setOnClickListener(new View.OnClickListener() {
									@Override
									public void onClick(View v) {
										dialog.dismiss();
										if (is_new) {
											Intent intent = new Intent(
													ChangePinActivity.this,
													MainActivity.class);
											intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
													| Intent.FLAG_ACTIVITY_NEW_TASK);
											intent.putExtra("IS_FLAG", is_new);
											startActivity(intent);
											finish();
										} else {
											Intent intent = new Intent(
													ChangePinActivity.this,
													RegistrationActivity.class);
											startActivity(intent);
											finish();
										}
									}
								});

						dialog.show();

					} else {
						Constants.showOneButtonDialog(ChangePinActivity.this,
								TAG, Constants.checkCode(replaced),
								Constants.DIALOG_CLOSE);
					}

				} else {
					Intent intent = new Intent(ChangePinActivity.this,
							ConnectivityErrorActivity.class);
					startActivity(intent);
				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonDialog(ChangePinActivity.this, TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonDialog(ChangePinActivity.this, TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(ChangePinActivity.this);
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							ChangePinTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			ChangePinTask.this.cancel(true);
			dialog.cancel();
		}

	}

	public class ForgotPasswordTask extends AsyncTask<String, String, String>
			implements OnDismissListener {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			try {

				String response = RequestClass.getInstance()
						.readPay1B2CRequest(
								ChangePinActivity.this,
								Constants.B2C_URL + "forgotpwd/?user_name="
										+ URLEncoder.encode(params[0], "utf-8")
										+ "&vtype=1");

				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			try {

				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONObject jsonObject = new JSONObject(replaced);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {

						editText_OldPin.requestFocus();
						editText_OldPin.setText("");

					} else {
						Constants.showOneButtonDialog(ChangePinActivity.this,
								TAG, Constants.checkCode(replaced),
								Constants.DIALOG_CLOSE_FORGOTPIN);
					}

				} else {
					Intent intent = new Intent(ChangePinActivity.this,
							ConnectivityErrorActivity.class);
					startActivity(intent);

				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonDialog(ChangePinActivity.this, TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonDialog(ChangePinActivity.this, TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(ChangePinActivity.this);
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							ForgotPasswordTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			ForgotPasswordTask.this.cancel(true);
			dialog.cancel();
		}

	}

}
