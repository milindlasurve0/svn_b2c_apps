//
//  TopOfferCell.m
//  Pay1
//
//  Created by Annapurna on 11/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "MyLikesCell.h"
#import "DBManager.h"
@implementation MyLikesCell

- (void)awakeFromNib {
    // Initialization code
    
    offerIDArrayList = [[NSMutableArray alloc]init];
    imageURLArray = [[NSMutableArray alloc]init];
    shopNameArray = [[NSMutableArray alloc]init];
    offerArray = [[NSMutableArray alloc]init];
    thumbURLArray = [[NSMutableArray alloc]init];
    priceArray = [[NSMutableArray alloc]init];
    dealArray = [[NSMutableArray alloc]init];
    offerIDList = [[NSMutableArray alloc]init];
    offerPriceArray = [[NSMutableArray alloc]init];

    
    blackView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.2];
    
    if([UIScreen mainScreen].bounds.size.width == 320){
    _offerImageView.frame = CGRectMake(8, 10, 320- 15, 160);
    _heartButton.frame = CGRectMake(320-40, 21, 25, 25);
    blackView.frame = CGRectMake(9, 125, 320-15, 45);

    }
    
    blackView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.2];

    
    _offerThumbNailImageView.layer.cornerRadius = _offerThumbNailImageView.frame.size.height/2;
    _offerThumbNailImageView.clipsToBounds = YES;
    _offerThumbNailImageView.layer.masksToBounds = YES;

}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
    NSArray *tblView=[[NSBundle mainBundle]loadNibNamed:@"MyLikesCell" owner:self options:nil];
    self=[tblView objectAtIndex:0];

    }
    return self;
}
-(void)fetchMyLikesDataFromDb:(NSString *)likeStr{
    [offerIDArrayList addObject:likeStr];
    
    NSLog(@"offerIDArrayList:%@",offerIDArrayList);
    
    [self fetchValueFromDBForLikes:offerIDArrayList];
}

-(void)fetchValueFromDBForLikes:(NSMutableArray *)dataArray{
    for(int i=0;i<dataArray.count;i++){
    offerIDArray= [[dataArray objectAtIndex:i] componentsSeparatedByString:@","];
   // NSLog(@"Items :%@",offerIDArray);
    [self fetchDataFromDB];
    }
    
}

-(void)fetchDataFromDB{
    
    NSString *query;
    NSMutableArray *offerArrayList = [[NSArray arrayWithArray:offerIDArray] mutableCopy];
    DBManager *dbManager = [[DBManager alloc]initWithDatabaseFilename:@"dbgiftdata.sql"];
    for(int i=0;i<offerArrayList.count;i++){
        NSString *offserValue = [offerArrayList objectAtIndex:i];
        
     //   NSLog(@"offserValue :%@",offserValue);
        query = [NSString stringWithFormat:@"select * from GIFTTABLE WHERE FREEBIEOFFERID=\"%@\"",offserValue];
      //  NSLog(@"DB is Mobile:%@ %lu",[[NSArray alloc] initWithArray:[dbManager loadDataFromDB:query]],(unsigned long)[[NSArray alloc] initWithArray:[dbManager loadDataFromDB:query]].count);
        
        if([[NSArray alloc] initWithArray:[dbManager loadDataFromDB:query]].count == 0){
        //    NSLog(@"Array is empty");
            [_rupeeImageView removeFromSuperview];
            [_offerDetailLabel removeFromSuperview];
            [_offerPointLabel removeFromSuperview];
            [_heartButton removeFromSuperview];
            [_moneyImageView removeFromSuperview];
            [_heartBGButton removeFromSuperview];
            
            
            
        }
        else{
            NSArray *theArray = [[NSArray alloc] initWithArray:[dbManager loadDataFromDB:query]];
            
            NSInteger indexOfImageURL = [dbManager.arrColumnNames indexOfObject:@"FREEBIEURL"];
            for(int i=0;i<theArray.count;i++){
             //   NSLog(@">>>>>%@", [[theArray objectAtIndex:i] objectAtIndex:indexOfImageURL]);
                [imageURLArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfImageURL]];
            }
            NSInteger indexOfShopNameURL = [dbManager.arrColumnNames indexOfObject:@"FREEBIEDEALNAME"];
            for(int i=0;i<theArray.count;i++){
              //  NSLog(@">>>>>%@", [[theArray objectAtIndex:i] objectAtIndex:indexOfShopNameURL]);

                [shopNameArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfShopNameURL]];
            }
            NSInteger indexOfAreaURL = [dbManager.arrColumnNames indexOfObject:@"FREEBIELOGOURL"];
            for(int i=0;i<theArray.count;i++){
               // NSLog(@">>>>>%@", [[theArray objectAtIndex:i] objectAtIndex:indexOfAreaURL]);
                [thumbURLArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfAreaURL]];
            }
            NSInteger indexOfOfferURL = [dbManager.arrColumnNames indexOfObject:@"FREEBIESHORTDESC"];
            for(int i=0;i<theArray.count;i++){
              //  NSLog(@">>>>>%@", [[theArray objectAtIndex:i] objectAtIndex:indexOfOfferURL]);
                [offerArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfOfferURL]];
            }
            NSInteger indexOfPriceURL = [dbManager.arrColumnNames indexOfObject:@"FREEBIEMINAMOUNT"];
            for(int i=0;i<theArray.count;i++){
              //  NSLog(@">>>>>%@", [[theArray objectAtIndex:i] objectAtIndex:indexOfPriceURL]);
                [priceArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfPriceURL]];
            }
            
            NSInteger indexOfDeal = [dbManager.arrColumnNames indexOfObject:@"FREEBIEDEALID"];
            for(int i=0;i<theArray.count;i++){
               // NSLog(@">>>>>%@", [[theArray objectAtIndex:i] objectAtIndex:indexOfDeal]);
                [dealArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfDeal]];
            }
            NSInteger indexOfOfferID = [dbManager.arrColumnNames indexOfObject:@"FREEBIEOFFERID"];
            for(int i=0;i<theArray.count;i++){
               // NSLog(@"OfferID%@", [[theArray objectAtIndex:i] objectAtIndex:indexOfOfferID]);
                [offerIDList addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfOfferID]];
            }
            
            NSInteger indexOfOfferPriceURL = [dbManager.arrColumnNames indexOfObject:@"FREEBIEOFFERPRICE"];
            for(int i=0;i<theArray.count;i++){
                [offerPriceArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfOfferPriceURL]];
            }
            
            
            [_offerImageView setImageWithURL:[NSURL URLWithString:[imageURLArray objectAtIndex:i]] placeholderImage:[UIImage imageNamed:@"deal_default.png"]completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
                UIImage *newImage = [UIImage imageWithImage:image  scaledToSize:CGSizeMake(320,212)];
                _offerImageView.image = newImage;
            }usingActivityIndicatorStyle:(UIActivityIndicatorViewStyle)UIActivityIndicatorViewStyleGray];
            
            [_offerThumbNailImageView setImageWithURL:[NSURL URLWithString:[thumbURLArray objectAtIndex:i]] placeholderImage:[UIImage imageNamed:@"deal_default.png"]completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
                UIImage *newImage = [UIImage imageWithImage:image  scaledToSize:CGSizeMake(320,212)];
                _offerThumbNailImageView.image = newImage;
            }usingActivityIndicatorStyle:(UIActivityIndicatorViewStyle)UIActivityIndicatorViewStyleGray];
            
            _offerImageView.userInteractionEnabled = TRUE;
            
            UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(imageTapped:)];
            tapGesture.delegate = self;
            self.tag = i; //[[dealArray objectAtIndex:i]intValue]; //[objGiftDetailList.offerID intValue];
            
            [self addGestureRecognizer:tapGesture];
            
            
            _offerDetailLabel.text = [offerArray objectAtIndex:i]; //objGiftDetailList.offer;
            _offerDetailLabel.numberOfLines = 2;
            [_offerDetailLabel sizeToFit];
            
            NSString *name = [[shopNameArray objectAtIndex:i] stringByReplacingOccurrencesOfString:@"%27" withString:@"'"];

            
            _offerNameLabel.text = name;//[shopNameArray objectAtIndex:i]; //objGiftDetailList.deal;
            
           /* CGSize yourLabelSize = [_offerDetailLabel.text sizeWithAttributes:@{NSFontAttributeName : [UIFont fontWithName:@"Open Sans" size:17]}];
            
            CGRect rect = _offerDetailLabel.frame;
            rect.size.height = yourLabelSize.height;
            _offerDetailLabel.frame = rect;*/

            offerPriceBGView.frame = CGRectMake([UIScreen mainScreen].bounds.size.width - 150, _offerDetailLabel.frame.origin.y + _offerDetailLabel.frame.size.height, 150, 50);
            
            _offerPointLabel.text = [priceArray objectAtIndex:i];
            
            
            if(![[offerPriceArray objectAtIndex:i]isEqualToString:@"0"]){
           // _rupeeLabel.text = [NSString stringWithFormat:@"%@",[offerPriceArray objectAtIndex:i]];
                
                NSString *ofPrice = [offerPriceArray objectAtIndex:i];
                
                _rupeeLabel.text = [NSString stringWithFormat:@"%@",ofPrice];
                
                if(ofPrice.length > 3){
                    _rupeeImageView.frame = CGRectMake(12, 12, 18, 18);
                    _rupeeLabel.frame = CGRectMake(32, 9, 38, 21);
                    _plusImageView.frame = CGRectMake(75, 16, 10, 10);
                }
                
                if(ofPrice.length == 3){
                    _rupeeImageView.frame = CGRectMake(22, 12, 18, 18);
                    _rupeeLabel.frame = CGRectMake(43, 9, 38, 21);
                    _plusImageView.frame = CGRectMake(72, 16, 10, 10);
                }
                
                else{
                    
                    _rupeeImageView.frame = CGRectMake(25, 12, 18, 18);
                    
                    _rupeeLabel.frame = CGRectMake(45, 9, 30, 21);
                    _plusImageView.frame = CGRectMake(72, 16, 10, 10);

                }
                
                //[NSString stringWithFormat:@"%@",[offerPriceArray objectAtIndex:indexPath.row]];
                
                [_rupeeLabel sizeToFit];
                
                
            }
            else{
                _plusImageView.hidden = TRUE;
            _rupeeLabel.hidden = TRUE;
            _rupeeImageView.hidden = TRUE;
            }
            
        }
        
    }
    
    
}


-(void)imageTapped:(UIGestureRecognizer *)tapGesture{
    NSLog(@"#TheTapGestureValueis :%ld",(long)tapGesture.view.tag);
    NSString *deal = [dealArray objectAtIndex:tapGesture.view.tag];
    NSString *offer = [offerIDList objectAtIndex:tapGesture.view.tag];
    NSString *shopText = [shopNameArray objectAtIndex:tapGesture.view.tag];
    NSLog(@"## :%@ %@",deal,offer);

    
    if(_onTableRowClick){
        _onTableRowClick(deal,offer,shopText);
    }
}
-(IBAction)heartClick:(id)sender{
    if(_onHeartClick)
    _onHeartClick();
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
