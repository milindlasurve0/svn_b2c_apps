package com.pay1.adapterhandler;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.pay1.R;

public class PlansAdapter extends ArrayAdapter<JSONObject> {
	Context mContext;
	List<JSONObject> jsonObject;
	LayoutInflater inflater;
	TextView textPlanAmount, textPlanValidity, textPlanDesc,textView_Validity;
	Typeface Normal,Reguler,Medium;

	public PlansAdapter(Context context, int textViewResourceId,
			List<JSONObject> objects) {
		super(context, textViewResourceId, objects);
		// TODO Auto-generated constructor stub
		this.mContext = context;
		this.jsonObject = objects;
		 Normal = Typeface.createFromAsset(context.getAssets(),
				"EncodeSansNormal-100-Thin.ttf");
		 Reguler = Typeface.createFromAsset(context.getAssets(),
				"EncodeSansNormal-400-Regular.ttf");
		 Medium = Typeface.createFromAsset(context.getAssets(),
				"EncodeSansNarrow-500-Medium.ttf");
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View view = convertView;
		if (convertView == null) {
			inflater = (LayoutInflater) mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.plans_adapter, null);
		}

		// if (position % 2 == 1) {
		// view.setBackgroundColor(Color.WHITE);
		// } else {
		//
		// view.setBackgroundColor(mContext.getResources().getColor(
		// R.color.row_light_green));
		// }

		textPlanAmount = (TextView) view.findViewById(R.id.textPlanAmount);
		textPlanValidity = (TextView) view.findViewById(R.id.textPlanValidity);
		textPlanDesc = (TextView) view.findViewById(R.id.textPlanDesc);
		textView_Validity=(TextView)view.findViewById(R.id.textView_Validity);
		textView_Validity.setTypeface(Reguler);
		textPlanDesc.setTypeface(Normal);
		textPlanAmount.setTypeface(Normal);
		textPlanValidity.setTypeface(Normal);

		try {
			textPlanAmount.setText(""+jsonObject.get(position).getString("amount"));
			textPlanValidity.setText(jsonObject.get(position).getString("validity"));
			textPlanDesc.setText(jsonObject.get(position).getString(
					"description"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}

		return view;
	}

}
