package com.pay1.adapterhandler;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.pay1.DTHFragment;
import com.pay1.DataCardFragment;
import com.pay1.QuickPayRechargeFragment;
import com.pay1.RechargeFragment;

public class RechargeFragmentAdapter extends FragmentStatePagerAdapter
// implements IconPagerAdapter
{
	protected static final String[] CONTENT = new String[] { "One Touch Recharge","Mobile", "DTH",
			"Data card" };

	private int mCount = CONTENT.length;

	public RechargeFragmentAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int position) {
		//
		switch (position) {
		case 0:
			return QuickPayRechargeFragment.newInstance();
		case 1:
			return RechargeFragment.newInstance();
		case 2:
			return DTHFragment.newInstance();
		case 3:
			return DataCardFragment.newInstance();
		default:
			return RechargeFragment.newInstance();
		}
	}

	@Override
	public int getCount() {
		return mCount;
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return RechargeFragmentAdapter.CONTENT[position % CONTENT.length];
	}

	// @Override
	// public int getIconResId(int index) {
	// return ICONS[index % ICONS.length];
	// }

	// public void setCount(int count) {
	// if (count > 0 && count <= 10) {
	// mCount = count;
	// notifyDataSetChanged();
	// }
	// }
}