package com.pay1;

import java.net.URLEncoder;

import org.json.JSONException;
import org.json.JSONObject;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.larvalabs.svgandroid.SVGParser;
import com.pay1.constants.Constants;
import com.pay1.customviews.MyProgressDialog;
import com.pay1.requesthandler.RequestClass;
import com.pay1.utilities.EditTextValidator;
import com.pay1.utilities.Utility;

public class ChangeEmailActivity extends Activity {

	private static final String SCREEN_LABEL = "Update Email Screen";
	private EasyTracker easyTracker = null;

	private final String TAG = "Update Email";
	private Button button_Confirm, button_Cancel;
	private ImageView imageView_Back;
	private EditText editText_Email, editText_Pin;
	private TextView textView_Title, textView_TitleEmail, textView_TitlePin;

	@SuppressLint("NewApi")
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.change_email_activity);
		try {
			easyTracker = EasyTracker.getInstance(ChangeEmailActivity.this);
			easyTracker.set(Fields.SCREEN_NAME, SCREEN_LABEL);

			Typeface Reguler = Typeface.createFromAsset(getAssets(),
					"Roboto-Regular.ttf");
			Typeface Medium = Typeface.createFromAsset(getAssets(),
					"Roboto-Regular.ttf");

			textView_Title = (TextView) findViewById(R.id.textView_Title);
			textView_Title.setTypeface(Medium);
			textView_TitleEmail = (TextView) findViewById(R.id.textView_TitleEmail);
			textView_TitleEmail.setTypeface(Medium);
			textView_TitleEmail.setVisibility(View.GONE);
			textView_TitlePin = (TextView) findViewById(R.id.textView_TitlePin);
			textView_TitlePin.setTypeface(Medium);
			textView_TitlePin.setVisibility(View.GONE);

			RelativeLayout back_layout=(RelativeLayout)findViewById(R.id.back_layout);
			back_layout.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onBackPressed();
				}
			});
			
			
			editText_Email = (EditText) findViewById(R.id.editText_Email);
			editText_Email.setTypeface(Reguler);
			editText_Email.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					// TODO Auto-generated method stub
					if (editText_Email.getText().toString().trim().length() != 0) {
						textView_TitleEmail.setVisibility(View.GONE);
					} else {
						textView_TitleEmail.setVisibility(View.GONE);
					}
					if (editText_Email.getText().toString().trim().length() > 0)
						EditTextValidator.hasText(ChangeEmailActivity.this,
								editText_Email,
								Constants.ERROR_EMAIL_BLANK_FIELD);
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					// TODO Auto-generated method stub

				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

				}
			});

			try {
				Bundle bundle = getIntent().getExtras();
				if (bundle != null) {
					String str = bundle.getString("EMAIL");

					if (str.equalsIgnoreCase("")) {
						try {
							AccountManager accountManager = AccountManager
									.get(this);
							Account[] accounts = accountManager
									.getAccountsByType("com.google");
							editText_Email.setText(accounts[0].name.toString()
									.trim());
						} catch (Exception e) {
						}
					} else {
						editText_Email.setText(str);
					}
				}
			} catch (Exception e) {
			}
			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(editText_Email.getWindowToken(), 0);

			editText_Pin = (EditText) findViewById(R.id.editText_Pin);
			editText_Pin.setTypeface(Reguler);
			editText_Pin.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					// TODO Auto-generated method stub
					if (editText_Pin.getText().toString().trim().length() != 0) {
						textView_TitlePin.setVisibility(View.GONE);
					} else {
						textView_TitlePin.setVisibility(View.GONE);
					}
					if (editText_Pin.getText().toString().trim().length() > 0)
						EditTextValidator.hasText(ChangeEmailActivity.this,
								editText_Pin, Constants.ERROR_PIN_BLANK_FIELD);
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					// TODO Auto-generated method stub

				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

				}
			});

			editText_Pin
					.setOnEditorActionListener(new TextView.OnEditorActionListener() {

						@Override
						public boolean onEditorAction(TextView v, int actionId,
								KeyEvent event) {
							// TODO Auto-generated method stub
							if (actionId == EditorInfo.IME_ACTION_GO) {
								try {
									if (checkValidation(ChangeEmailActivity.this)) {
										new UpdateEmailTask().execute(
												editText_Email.getText()
														.toString().trim(),
												editText_Pin.getText()
														.toString().trim());
									}
								} catch (Exception e) {
									// e.printStackTrace();
								}
								return true;
							}
							return false;
						}
					});
			imageView_Back = (ImageView) findViewById(R.id.imageView_Back);
			imageView_Back.setImageDrawable(SVGParser.getSVGFromResource(
					getResources(), R.raw.ic_back_old).createPictureDrawable());
			if (Build.VERSION.SDK_INT >= 11)
				imageView_Back.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
			imageView_Back.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onBackPressed();
				}
			});

			button_Confirm = (Button) findViewById(R.id.button_Confirm);
			button_Confirm.setTypeface(Reguler);
			button_Confirm.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (checkValidation(ChangeEmailActivity.this)) {

						new UpdateEmailTask().execute(editText_Email.getText()
								.toString().trim(), editText_Pin.getText()
								.toString().trim());

					}
				}
			});

			button_Cancel = (Button) findViewById(R.id.button_Cancel);
			button_Cancel.setTypeface(Reguler);
			button_Cancel.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					editText_Email.setText("");
					editText_Pin.setText("");
				}
			});

			overridePendingTransition(R.anim.slide_in_right,
					R.anim.slide_out_left);

		} catch (Exception e) {
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView().build());
		// EasyTracker.getInstance(this).activityStart(this);
		easyTracker.send(MapBuilder.createAppView().build());
	}

	@Override
	public void onStop() {
		super.onStop();
		// EasyTracker.getInstance(this).activityStop(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
	}

	private boolean checkValidation(Context context) {
		boolean ret = true;

		if (!EditTextValidator.hasText(context, editText_Email,
				Constants.ERROR_EMAIL_BLANK_FIELD)) {
			ret = false;
			editText_Email.requestFocus();
			return ret;
		} else if (!EditTextValidator.isEmailAddress(context, editText_Email,
				Constants.ERROR_EMAIL_VALID_FIELD)) {
			ret = false;
			editText_Email.requestFocus();
			return ret;
		} else if (!EditTextValidator.hasText(context, editText_Pin,
				Constants.ERROR_LOGIN_PIN_BLANK_FIELD)) {
			ret = false;
			editText_Pin.requestFocus();
			return ret;
		} else if (!EditTextValidator.isValidPin(context, editText_Pin,
				Constants.ERROR_LOGIN_PIN_VALID_FIELD)) {
			ret = false;
			editText_Pin.requestFocus();
			return ret;
		} else
			return ret;
	}

	public class UpdateEmailTask extends AsyncTask<String, String, String>
			implements OnDismissListener {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			try {

				String response = RequestClass
						.getInstance()
						.readPay1B2CRequest(
								ChangeEmailActivity.this,
								Constants.B2C_URL + "update_profile/?email="
										+ URLEncoder.encode(params[0], "utf-8")
										+ "&password="
										+ URLEncoder.encode(params[1], "utf-8"));
				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			try {

				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONObject jsonObject = new JSONObject(replaced);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {

						// JSONObject jsonObject2 = jsonObject
						// .getJSONObject("description");

						Utility.setEmail(ChangeEmailActivity.this,
								Constants.SHAREDPREFERENCE_EMAIL,
								editText_Email.getText().toString().trim());

						// Constants.showOneButtonDialog(ChangeEmailActivity.this,
						// TAG, "Email updated successfully.",
						// Constants.DIALOG_CLOSE_CONFIRM);

						final Dialog dialog = new Dialog(
								ChangeEmailActivity.this);
						dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
						// dialog.getWindow().setBackgroundDrawableResource(
						// android.R.color.transparent);
						dialog.setContentView(R.layout.dialog_one_button);
						dialog.setCancelable(false);
						dialog.setCanceledOnTouchOutside(false);
						// dialog.setTitle(null);

						Typeface Reguler = Typeface
								.createFromAsset(getAssets(),
										"Roboto-Regular.ttf");

						Typeface Medium = Typeface.createFromAsset(getAssets(),
								"Roboto-Regular.ttf");

						// set the custom dialog components - text, image and
						// button
						TextView textView_Title = (TextView) dialog
								.findViewById(R.id.textView_Title);
						textView_Title.setText(TAG);
						textView_Title.setTypeface(Reguler);

						TextView textView_Message = (TextView) dialog
								.findViewById(R.id.textView_Message);
						textView_Message.setText("Email updated successfully.");
						textView_Message.setTypeface(Medium);

						Button button_Ok = (Button) dialog
								.findViewById(R.id.button_Ok);
						button_Ok.setTypeface(Reguler);
						// if button is clicked, close the custom dialog
						button_Ok
								.setOnClickListener(new View.OnClickListener() {
									@Override
									public void onClick(View v) {
										dialog.dismiss();

										Intent returnIntent = new Intent();
										returnIntent.putExtra("EMAIL",
												editText_Email.getText()
														.toString().trim());
										setResult(RESULT_OK, returnIntent);
										onBackPressed();
									}
								});

						dialog.show();

					} else {
						Constants.showOneButtonDialog(ChangeEmailActivity.this,
								TAG, Constants.checkCode(replaced),
								Constants.DIALOG_CLOSE);
					}
				} else {
					Intent intent = new Intent(ChangeEmailActivity.this,
							ConnectivityErrorActivity.class);
					startActivity(intent);
				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonDialog(ChangeEmailActivity.this, TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonDialog(ChangeEmailActivity.this, TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(ChangeEmailActivity.this);
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							UpdateEmailTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			UpdateEmailTask.this.cancel(true);
			dialog.cancel();
		}

	}

}
