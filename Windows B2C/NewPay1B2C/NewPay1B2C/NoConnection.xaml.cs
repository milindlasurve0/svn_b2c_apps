﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace NewPay1B2C
	{
	public partial class NoConnection : PhoneApplicationPage
		{
		public NoConnection ()
			{
			InitializeComponent ();
			GoogleAnalytics.EasyTracker.GetTracker ().SendView ("No Connection");
			textReconnect.MouseEnter += (s, e) =>
			{
				NavigationService.GoBack ();
			};
			}
		}
	}