//
//  SupportViewController.h
//  Pay1
//
//  Created by webninjaz on 16/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "BaseViewController.h"
@class MailCreator;
@interface SupportViewController : BaseViewController{
    
    
    IBOutlet UILabel *mailLabel;
    IBOutlet UILabel *missedNumLabel;
    
    MailCreator *mailClient;
    
    UIView *coverView;


}

@end
