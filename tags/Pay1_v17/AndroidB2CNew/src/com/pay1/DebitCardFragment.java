package com.pay1;

import org.json.JSONException;
import org.json.JSONObject;
import android.support.v4.app.Fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.pay1.constants.Constants;
import com.pay1.customviews.MyProgressDialog;
import com.pay1.requesthandler.RequestClass;
import com.pay1.utilities.EditTextValidator;

public class DebitCardFragment extends Fragment {

	private static final String SCREEN_LABEL = "PG Amount Screen";
	private EasyTracker easyTracker = null;

	private final String TAG = "Wallet Topup";
	private EditText editText_Amount;
	private TextView textView_TitleAmount, textView_Text;
	private Button button_Confirm, button_Cancel;

	public static DebitCardFragment newInstance() {
		DebitCardFragment fragment = new DebitCardFragment();
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// if ((savedInstanceState != null)
		// && savedInstanceState.containsKey(KEY_CONTENT)) {
		// mContent = savedInstanceState.getString(KEY_CONTENT);
		// }
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		final View view = inflater.inflate(R.layout.debit_credit_fragment,
				container, false);
		try {
			easyTracker = EasyTracker.getInstance(getActivity());
			easyTracker.set(Fields.SCREEN_NAME, SCREEN_LABEL);

			// timestamp = System.currentTimeMillis();

			// faqDataSource = new FAQDataSource(getActivity());

			Typeface Reguler = Typeface.createFromAsset(getActivity()
					.getAssets(), "EncodeSansNormal-400-Regular.ttf");
			Typeface Medium = Typeface.createFromAsset(getActivity()
					.getAssets(), "EncodeSansNarrow-500-Medium.ttf");

			textView_Text = (TextView) view.findViewById(R.id.textView_Text);
			textView_Text.setTypeface(Reguler);
			textView_TitleAmount = (TextView) view
					.findViewById(R.id.textView_TitleAmount);
			textView_TitleAmount.setTypeface(Medium);
			textView_TitleAmount.setVisibility(View.GONE);

			editText_Amount = (EditText) view
					.findViewById(R.id.editText_Amount);
			InputMethodManager imm = (InputMethodManager) getActivity()
					.getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(editText_Amount.getWindowToken(), 0);
			editText_Amount.setTypeface(Reguler);
			try {
				Bundle bundle = getArguments();
				if (bundle != null) {
					String str = bundle.getString("AMOUNT");
					editText_Amount.setText(str);
				}
			} catch (Exception e) {

			}
			editText_Amount.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					// TODO Auto-generated method stub
					if (editText_Amount.getText().toString().trim().length() != 0) {
						textView_TitleAmount.setVisibility(View.GONE);
					} else {
						textView_TitleAmount.setVisibility(View.GONE);
					}
					if (editText_Amount.getText().toString().trim().length() > 0)
						EditTextValidator.hasFragmentText(getActivity(),
								editText_Amount,
								Constants.ERROR_AMOUNT_BLANK_FIELD);
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					// TODO Auto-generated method stub

				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

				}
			});

			editText_Amount
					.setOnEditorActionListener(new TextView.OnEditorActionListener() {

						@Override
						public boolean onEditorAction(TextView v, int actionId,
								KeyEvent event) {
							// TODO Auto-generated method stub
							if (actionId == EditorInfo.IME_ACTION_GO) {
								submitClick(v);
								return true;
							}
							return false;
						}
					});

			button_Confirm = (Button) view.findViewById(R.id.button_Confirm);
			button_Confirm.setTypeface(Reguler);
			button_Confirm.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					submitClick(v);
				}
			});
			button_Cancel = (Button) view.findViewById(R.id.button_Cancel);
			button_Cancel.setTypeface(Reguler);
			button_Cancel.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					editText_Amount.setText("");
				}
			});

		} catch (Exception e) {

		}
		return view;
	}

	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView().build());
		// EasyTracker.getInstance(this).activityStart(this);
		easyTracker.send(MapBuilder.createAppView().build());
	}

	@Override
	public void onStop() {
		super.onStop();
		// EasyTracker.getInstance(this).activityStop(this);
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.Fragment#onDestroy()
	 */
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		// Intent intent = new Intent(getActivity(), MainActivity.class);
		// intent.putExtra("FROM_QUICKPAY", false);
		// intent.putExtra("IS_GIFT", false);
		// intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
		// | Intent.FLAG_ACTIVITY_NEW_TASK);
		// getActivity().startActivity(intent);
		// getActivity().finish();
	}

	private void submitClick(View v) {
		try {
			InputMethodManager imm = (InputMethodManager) getActivity()
					.getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

			if (EditTextValidator.hasFragmentText(getActivity(),
					editText_Amount, Constants.ERROR_AMOUNT_BLANK_FIELD)
					&& EditTextValidator
							.isValidFragmentAmount(getActivity(),
									editText_Amount,
									Constants.ERROR_AMOUNT_VALID_FIELD)) {
				new WalletTopupPGTask().execute(editText_Amount.getText()
						.toString().trim());
			}
		} catch (Exception e) {
		}
	}

	public class WalletTopupPGTask extends AsyncTask<String, String, String>
			implements OnDismissListener {
		private MyProgressDialog dialog;
		String url;

		@Override
		protected String doInBackground(String... params) {
			try {
				String response = RequestClass.getInstance()
						.readPay1B2CRequest(
								getActivity(),
								Constants.B2C_URL
										+ "online_walletrefill/?amount="
										+ params[0]);

				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onCancelled() {
			// TODO Auto-generated method stub
			super.onCancelled();
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			try {

				if (!result.startsWith("Error")) {
					// result = result.replaceAll("\\\\", "");
					JSONObject jsonObject = new JSONObject(result);

					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {
						JSONObject description = jsonObject
								.getJSONObject("description");

						Intent intent = new Intent(getActivity(),
								PGActivity.class);
						intent.putExtra("OPERATOR_NAME", "");
						intent.putExtra("TAX", "");
						intent.putExtra("MOBILE_NUMBER", "");
						intent.putExtra("RECHARGE_AMOUNT", editText_Amount
								.getText().toString().trim());
						intent.putExtra("RECHARGE_FOR",
								Constants.WALLET_PAYMENT);
						intent.putExtra("TRANS_ID",
								description.getString("txnid"));
						intent.putExtra("CONTENT",
								description.getString("form_content")
										.replaceAll("\\\\", ""));

						startActivity(intent);
					} else {
						Constants.showOneButtonDialog(getActivity(), TAG,
								Constants.checkCode(result),
								Constants.DIALOG_CLOSE);
					}
				} else {
					Intent intent = new Intent(getActivity(),
							ConnectivityErrorActivity.class);
					startActivity(intent);
				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonDialog(getActivity(), "Online Topup",
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonDialog(getActivity(), "Online Topup",
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(getActivity());
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(false);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							WalletTopupPGTask.this.cancel(true);
							dialog.cancel();
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			WalletTopupPGTask.this.cancel(true);
			dialog.cancel();
		}

	}
}
