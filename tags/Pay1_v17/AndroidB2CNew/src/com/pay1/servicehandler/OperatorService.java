package com.pay1.servicehandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.IntentService;
import android.content.Intent;
import android.database.SQLException;
import android.os.AsyncTask;

import com.pay1.constants.Constants;
import com.pay1.databasehandler.OperatorDataSource;
import com.pay1.requesthandler.RequestClass;

public class OperatorService extends IntentService {

	private OperatorDataSource operatorDataSource;

	public OperatorService() {
		super("OperatorService");
		// TODO Auto-generated constructor stub
		operatorDataSource = new OperatorDataSource(OperatorService.this);
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		new GetOperatorTask().execute();
	}

	public class GetOperatorTask extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... params) {
			try {
				String response = RequestClass.getInstance()
						.readPay1B2CRequest(OperatorService.this,
								Constants.B2C_URL + "get_all_operators/?");
				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			try {

				long timestamp = System.currentTimeMillis();
				operatorDataSource.open();
				String replaced;
				if (!result.startsWith("Error")) {
					replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");
				} else {
					replaced = Constants.loadJSONFromAsset(
							OperatorService.this, "operator.json");
				}
				JSONObject jsonObject = new JSONObject(replaced);
				String status = jsonObject.getString("status");
				if (status.equalsIgnoreCase("success")) {
					JSONObject description = jsonObject
							.getJSONObject("description");
					JSONArray mobile = description.getJSONArray("mobile");
					JSONArray data = description.getJSONArray("data");
					JSONArray postpaid = description.getJSONArray("postpaid");
					JSONArray dth = description.getJSONArray("dth");
					operatorDataSource.deleteOperator();
					operatorDataSource.createOperator(mobile,
							Constants.RECHARGE_MOBILE, timestamp);
					operatorDataSource.createOperator(data,
							Constants.RECHARGE_DATA, timestamp);
					operatorDataSource.createOperator(postpaid,
							Constants.BILL_PAYMENT, timestamp);
					operatorDataSource.createOperator(dth,
							Constants.RECHARGE_DTH, timestamp);
					// for (int i = 0; i < mobile.length(); i++) {
					//
					// JSONObject object = mobile.getJSONObject(i);
					//
					// operatorDataSource
					// .createOperator(
					// Integer.parseInt(object.getString("id")
					// .trim()),
					// object.getString("opr_code").trim(),
					// Integer.parseInt(object.getString(
					// "product_id").trim()),
					// object.getString("name").trim(),
					// Integer.parseInt(object.getString(
					// "service_id").trim()),
					// Integer.parseInt(object.getString(
					// "flag").trim()),
					// Integer.parseInt(object.getString(
					// "delegate").trim()),
					// Integer.parseInt(object
					// .getString("stv").trim()),
					// Constants.RECHARGE_MOBILE,
					// timestamp,
					// Integer.parseInt(object
					// .getString("max")),
					// Integer.parseInt(object
					// .getString("min")),
					// Integer.parseInt(object
					// .getString("service_charge_amount")),
					// Integer.parseInt(object
					// .getString("service_tax_percent")),
					// Integer.parseInt(object
					// .getString("service_charge_percent")),
					// object.getString("charges_slab")
					// .toString(), Integer
					// .parseInt(object
					// .getString("length")),
					// object.getString("prefix").toString());
					//
					// }
					// for (int i = 0; i < data.length(); i++) {
					// JSONObject object = data.getJSONObject(i);
					//
					// operatorDataSource
					// .createOperator(
					// Integer.parseInt(object.getString("id")
					// .trim()),
					// object.getString("opr_code").trim(),
					// Integer.parseInt(object.getString(
					// "product_id").trim()),
					// object.getString("name").trim(),
					// Integer.parseInt(object.getString(
					// "service_id").trim()),
					// Integer.parseInt(object.getString(
					// "flag").trim()),
					// Integer.parseInt(object.getString(
					// "delegate").trim()),
					// Integer.parseInt(object
					// .getString("stv").trim()),
					// Constants.RECHARGE_DATA,
					// timestamp,
					// Integer.parseInt(object
					// .getString("max")),
					// Integer.parseInt(object
					// .getString("min")),
					// Integer.parseInt(object
					// .getString("service_charge_amount")),
					// Integer.parseInt(object
					// .getString("service_tax_percent")),
					// Integer.parseInt(object
					// .getString("service_charge_percent")),
					// object.getString("charges_slab")
					// .toString(), Integer
					// .parseInt(object
					// .getString("length")),
					// object.getString("prefix").toString());
					// }
					// for (int i = 0; i < postpaid.length(); i++) {
					// JSONObject object = postpaid.getJSONObject(i);
					//
					// operatorDataSource
					// .createOperator(
					// Integer.parseInt(object.getString("id")
					// .trim()),
					// object.getString("opr_code").trim(),
					// Integer.parseInt(object.getString(
					// "product_id").trim()),
					// object.getString("name").trim(),
					// Integer.parseInt(object.getString(
					// "service_id").trim()),
					// Integer.parseInt(object.getString(
					// "flag").trim()),
					// Integer.parseInt(object.getString(
					// "delegate").trim()),
					// Integer.parseInt(object
					// .getString("stv").trim()),
					// Constants.BILL_PAYMENT,
					// timestamp,
					// Integer.parseInt(object
					// .getString("max")),
					// Integer.parseInt(object
					// .getString("min")),
					// Integer.parseInt(object
					// .getString("service_charge_amount")),
					// Integer.parseInt(object
					// .getString("service_tax_percent")),
					// Integer.parseInt(object
					// .getString("service_charge_percent")),
					// object.getString("charges_slab")
					// .toString(), Integer
					// .parseInt(object
					// .getString("length")),
					// object.getString("prefix").toString());
					// }
					// for (int i = 0; i < dth.length(); i++) {
					//
					// JSONObject object = dth.getJSONObject(i);
					//
					// operatorDataSource
					// .createOperator(
					// Integer.parseInt(object.getString("id")
					// .trim()),
					// object.getString("opr_code").trim(),
					// Integer.parseInt(object.getString(
					// "product_id").trim()),
					// object.getString("name").trim(),
					// Integer.parseInt(object.getString(
					// "service_id").trim()),
					// Integer.parseInt(object.getString(
					// "flag").trim()),
					// Integer.parseInt(object.getString(
					// "delegate").trim()),
					// Integer.parseInt(object
					// .getString("stv").trim()),
					// Constants.RECHARGE_DTH,
					// timestamp,
					// Integer.parseInt(object
					// .getString("max")),
					// Integer.parseInt(object
					// .getString("min")),
					// Integer.parseInt(object
					// .getString("service_charge_amount")),
					// Integer.parseInt(object
					// .getString("service_tax_percent")),
					// Integer.parseInt(object
					// .getString("service_charge_percent")),
					// object.getString("charges_slab")
					// .toString(), Integer
					// .parseInt(object
					// .getString("length")),
					// object.getString("prefix").toString());
					// }

				}

			} catch (JSONException e) {
			} catch (SQLException exception) {
			} finally {
				operatorDataSource.close();
			}
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}
	}

}
