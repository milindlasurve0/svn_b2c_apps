//
//  WalletHistoryCustomCell.h
//  Pay1
//
//  Created by webninjaz on 02/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WalletHistoryCustomCell : UITableViewCell{
    
    
}

@property (nonatomic,strong)IBOutlet UIImageView *walletImageView;
@property (nonatomic,strong)IBOutlet UILabel *walletPhoneNumber;
@property (nonatomic,strong)IBOutlet UILabel *walletStatusLabel;
@property (nonatomic,strong)IBOutlet UILabel *walletTimeLabel;
@property (nonatomic,strong)IBOutlet UILabel *walletMoneyLabel;
@property (nonatomic,strong)IBOutlet UILabel *closingBalanceTxtLabel;

@property (nonatomic,strong)IBOutlet UIImageView *operatorLogoImageView;



@end
