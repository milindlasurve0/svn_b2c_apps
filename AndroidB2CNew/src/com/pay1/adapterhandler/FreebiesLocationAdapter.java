package com.pay1.adapterhandler;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.WeakHashMap;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.larvalabs.svgandroid.SVGParser;
import com.pay1.FreeBiesActivity;
import com.pay1.FreeBiesLocatorActivity;
import com.pay1.MyFreeBiesDetailsActivity;
import com.pay1.R;
import com.pay1.constants.Constants;
import com.pay1.utilities.Utility;

public class FreebiesLocationAdapter extends BaseAdapter {

	private Context context;
	private ArrayList<WeakHashMap<String, String>> data;
	private Typeface Reguler, Medium;
	private String deal_name;
	private boolean show_route;
	OnClickListener clickListener;

	public FreebiesLocationAdapter(Context context, boolean show_route,
			String deal_name, ArrayList<WeakHashMap<String, String>> data,
			OnClickListener mOnClickListener) {
		this.context = context;
		this.data = data;
		this.show_route = show_route;
		this.deal_name = deal_name;
		Reguler = Typeface.createFromAsset(context.getAssets(),
				"Roboto-Regular.ttf");

		Medium = Typeface.createFromAsset(context.getAssets(),
				"Roboto-Regular.ttf");
		this.clickListener = mOnClickListener;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@SuppressLint("NewApi")
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final ViewHolder viewHolder;
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.freebies_location_adapter,
					null);
			viewHolder = new ViewHolder();

			viewHolder.imageView_Locate = (ImageView) convertView
					.findViewById(R.id.imageView_Locate);
			viewHolder.imageView_Locate.setImageDrawable(SVGParser
					.getSVGFromResource(context.getResources(),
							R.raw.ic_shoplocator).createPictureDrawable());
			viewHolder.imageView_Locate.setTag(position);

			viewHolder.textView_Distance = (TextView) convertView
					.findViewById(R.id.textViewDistance);
			// textView_Distance.setText(String.valueOf(position));

			if (Build.VERSION.SDK_INT >= 11)
				viewHolder.imageView_Locate.setLayerType(
						View.LAYER_TYPE_SOFTWARE, null);

			// viewHolder.imageView_Locate.setOnClickListener(clickListener);

			// int fistVisiblePosition =
			// context.getListView().getFirstVisiblePosition();
			/*
			 * viewHolder.imageView_Locate .setOnClickListener(new
			 * View.OnClickListener() {
			 * 
			 * @Override public void onClick(View v) { // TODO Auto-generated
			 * method stub Intent intent = new Intent(context,
			 * FreeBiesLocatorActivity.class); int
			 * pos=Integer.parseInt(textViewPosition.getText().toString());
			 * WeakHashMap<String, String> map = new WeakHashMap<String,
			 * String>( data.get(pos)); intent.putExtra("ROUTE", show_route);
			 * intent.putExtra("DEAL", deal_name); intent.putExtra("LATITUDE",
			 * map.get(FreeBiesActivity.LATITUDE)); intent.putExtra("LONGITUDE",
			 * map.get(FreeBiesActivity.LONGITUDE)); intent.putExtra("ADDRESS",
			 * map.get(FreeBiesActivity.ADDRESS)); intent.putExtra("CITY",
			 * map.get(FreeBiesActivity.CITY)); intent.putExtra("STATE",
			 * map.get(FreeBiesActivity.STATE)); intent.putExtra("FULL_ADDRESS",
			 * map.get("full_adderess")); context.startActivity(intent); } });
			 */
			viewHolder.textView_DealName = (TextView) convertView
					.findViewById(R.id.textView_DealName);
			viewHolder.textView_DealName.setTypeface(Medium);
			viewHolder.textView_Address = (TextView) convertView
					.findViewById(R.id.textView_Address);
			viewHolder.textView_Number = (TextView) convertView
					.findViewById(R.id.textView_Number);

			viewHolder.textView_Address.setTypeface(Medium);
			convertView.setTag(viewHolder);

		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		try {
			WeakHashMap<String, String> map = new WeakHashMap<String, String>(
					data.get(position));

			double dis = 0;
			DecimalFormat decimalFormat = new DecimalFormat("0.0");
			try {
				String lat = Utility.getCurrentLatitude(context,
						Constants.SHAREDPREFERENCE_CURRENT_LATITUDE);
				String lng = Utility.getCurrentLongitude(context,
						Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE);
				String dist = "0";

				if (lat != "" && lng != "")
					dist = String.valueOf(Constants.distanceFrom(Double
							.parseDouble(lat), Double.parseDouble(lng), Double
							.parseDouble(map.get(FreeBiesActivity.LATITUDE)),
							Double.parseDouble(map
									.get(FreeBiesActivity.LONGITUDE))));

				dis = Double.parseDouble(dist);

				if (dis < 1) {
					// dis = dis * 1000;
					viewHolder.textView_Distance.setText(decimalFormat
							.format(dis));
					// textView_Unit.setText("(M)");
					/*
					 * textViewDistance.setText("< "+ decimalFormat .format(dis)
					 * + "\nkm");
					 */
					viewHolder.textView_Distance.setText("  < 1 km ");
				} else {
					viewHolder.textView_Distance.setText(decimalFormat
							.format(dis));
					// textView_Unit.setText("(KM)");
					viewHolder.textView_Distance.setText("  "
							+ decimalFormat.format(dis) + "km");
				}

			} catch (Exception e) {
			}

			// viewHolder.textView_DealName.setText(deal_name);
			/*
			 * viewHolder.textView_Address.setText(map
			 * .get(FreeBiesActivity.ADDRESS) + "," +
			 * map.get(FreeBiesActivity.AREA) + "," +
			 * map.get(FreeBiesActivity.CITY));
			 */
			viewHolder.textView_Address.setText(map
					.get(MyFreeBiesDetailsActivity.FULL_ADDERESS));
			String number = map.get(MyFreeBiesDetailsActivity.NUMBER);
			SpannableString content = new SpannableString(number);
			content.setSpan(new UnderlineSpan(), 0, number.length(), 0);

			viewHolder.textView_Number.setText(content);
			viewHolder.textView_Number
					.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							showDialogForCallList(viewHolder.textView_Number
									.getText().toString().split(","));
						}
					});

		} catch (Exception e) {
		}
		return convertView;
	}

	private class ViewHolder {
		TextView textView_DealName, textView_Address, textView_Number,
				textView_Distance;
		ImageView imageView_Locate;
	}

	public void showDialogForCallList(final String[] numbers) {
		try {
			final Dialog dialog = new Dialog(context);

			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

			dialog.setCancelable(true);
			dialog.setCanceledOnTouchOutside(true);
			dialog.getWindow().setBackgroundDrawableResource(
					android.R.color.transparent);
			dialog.setContentView(R.layout.dialog_call_list);
			ImageView imageView_Close = (ImageView) dialog
					.findViewById(R.id.imageView_Close);
			imageView_Close.setImageDrawable(SVGParser.getSVGFromResource(
					context.getResources(), R.raw.ic_close)
					.createPictureDrawable());
			imageView_Close.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					dialog.dismiss();
				}
			});
			ListView callListView = (ListView) dialog
					.findViewById(R.id.listViewCall);
			CallListAdapterLocation adapter = new CallListAdapterLocation(
					context, numbers);

			callListView.setAdapter(adapter);
			/*
			 * callListView.setAdapter(new ArrayAdapter<String>(context,
			 * R.layout.call_number_at_location, numbers));
			 */

			callListView.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {
					// TODO Auto-generated method stub
					dialog.dismiss();
					/*
					 * String number =
					 * viewHolder.textView_Number.getText().toString().trim();
					 * String[] numberArray = number.split(",");
					 */
					String firstNumber = numbers[arg2];
					if (!firstNumber.isEmpty()) {
						Intent callIntent = new Intent(Intent.ACTION_CALL);
						callIntent.setData(Uri.parse("tel:" + firstNumber));
						context.startActivity(callIntent);
					} else {
						Toast.makeText(context, "No number available",
								Toast.LENGTH_LONG).show();
					}
				}
			});

			dialog.show();
		} catch (Exception exc) {
			Log.d("msg", "Msg");
		}
	}

}
