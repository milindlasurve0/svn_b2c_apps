﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Input;
using Microsoft.Phone.Tasks;

namespace Pay1B2C.Views
    {
    public partial class Support: PhoneApplicationPage
        {
        public Support()
            {
            InitializeComponent();

            GoogleAnalytics.EasyTracker.GetTracker().SendView("Support");
           
            textSupport.Text = "To register the complaint of your last transaction, give us a missed call on " + Constant.LoadPersistent<string>(Constant.SETTINGS_USER_SUPPORT_NUMBER).ToString();
            mail_event.MouseEnter += (s, e) =>
            {
                EmailComposeTask task = new EmailComposeTask();
                task.To = "listen@pay1.in";
                task.Subject = "Support";
                task.Body = "\n  Thanks &  Regards\n  " + Constant.getUserNumber();
                task.Show();
            };

            call_event.MouseEnter += (s, e) =>
            {
                PhoneCallTask phoneCallTask = new PhoneCallTask();

                phoneCallTask.PhoneNumber = Constant.LoadPersistent<string>(Constant.SETTINGS_USER_SUPPORT_NUMBER);
                phoneCallTask.DisplayName = "Pay1 Support";

                phoneCallTask.Show();
            };
            }


        private void buttonImage_Click_Back(object sender, RoutedEventArgs e)
            {
            NavigationService.GoBack();
            }
        }
    }