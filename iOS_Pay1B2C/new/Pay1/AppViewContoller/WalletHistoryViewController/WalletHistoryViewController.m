//
//  WalletHistoryViewController.m
//  Pay1
//
//  Created by webninjaz on 19/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "WalletHistoryViewController.h"
#import "TransactionHistoryView.h"
#import "WalletHistoryService.h"
#import "WalletHistoryCriteria.h"
#import "WalletHistoryResponse.h"
#import "WalletHistoryList.h"
#import "WalletHistoryCustomCell.h"
#import "TransactionList.h"
@interface WalletHistoryViewController ()

@end

@implementation WalletHistoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self showHUDOnView];
    
    walletContentArray = [[NSMutableArray alloc]init];
    
    walletHistoryTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

    
    walletValue = 0;

    [self setNaviationButtonWithText:@"Wallet History" selector:@selector(backClicked:) isLeft:TRUE isReduceSize:TRUE isMyFont:18.0];
    
    
    
   // walletHistoryLabel.text = @"PLEASE MAKE ANY \n TRANSACTION TO GET \n STARTED";
    
  /* if([UIScreen mainScreen].bounds.size.width == 320){
    walletHistoryImageView.frame = CGRectMake([UIScreen mainScreen].bounds.size.width /2 -50, [UIScreen mainScreen].bounds.size.height/2 -100, 100, 100);
    }*/
    

    [walletHistoryTableView removeFromSuperview];
    [self callWalletHistoryServiceBlock];
    
}


-(void)callWalletHistoryServiceBlock{
    WalletHistoryService *walletHistoryService = [[WalletHistoryService alloc]init];
    [walletHistoryService callWalletHistoryService:self.walletHistoryCriteria callback:^(NSError *error, WalletHistoryResponse *walletHistoryResponse) {        

        
    if(walletHistoryResponse.walletDataArray.count == 0){
    NSLog(@"The Status get error");
        [self hideHUDOnView];

    [walletHistoryTableView removeFromSuperview];
        [self.view addSubview:walletHistoryImageView];
        [self.view addSubview:walletHistoryLabel];
        
    
            
        
    }else{
        [self hideHUDOnView];
    NSLog(@"The wallet history response is%@",walletHistoryResponse.walletDataArray);
        
        for(int i=0;i<walletHistoryResponse.walletDataArray.count;i++){
            TransactionList *walletTransDataObj = [walletHistoryResponse.walletDataArray objectAtIndex:i];
            [walletContentArray addObject:walletTransDataObj];
        }
        
            
    if(walletHistoryResponse.walletDataArray.count > 9){
    bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, walletHistoryTableView.frame.origin.y + walletHistoryTableView.frame.size.height, walletHistoryTableView.frame.size.width, 40)];
    bottomButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    bottomButton.frame = CGRectMake(0,0 , bottomView.frame.size.width, bottomView.frame.size.height);
    bottomButton.backgroundColor = [UIColor colorWithRed:36/255.0 green:182/255.0 blue:82/255.0 alpha:1.0]; //[UIColor greenColor];
                
    [bottomButton setTitle:@"Load more history" forState:UIControlStateNormal];
    [bottomButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [bottomButton addTarget:self action:@selector(buttonLoadMoreTouched) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:bottomButton];
    walletHistoryTableView.tableFooterView = bottomView;
                
    }
    else{
    [bottomView removeFromSuperview];
    }
        
  //  walletContentArray = walletHistoryResponse.walletDataArray;
            
    [walletHistoryImageView removeFromSuperview];
    [walletHistoryLabel removeFromSuperview];
    [self.view addSubview:walletHistoryTableView];
    [walletHistoryTableView reloadData];
    [self hideHUDOnView];
    }
    }];
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [walletContentArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"WalletHistoryCustomCell";
    
    walletHistoryCustomCell=(WalletHistoryCustomCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(walletHistoryCustomCell==nil){
        [[NSBundle mainBundle] loadNibNamed:@"WalletHistoryCustomCell" owner:self options:nil];
    }
    
    TransactionList *walletTransDataObj = [walletContentArray objectAtIndex:indexPath.row];
    walletHistoryCustomCell.walletPhoneNumber.text = walletTransDataObj.transactionPhoneNumber;
  //  walletHistoryCustomCell.walletStatusLabel.text = [walletTransDataObj.transactionTransCategory uppercaseString];
    
 //   NSLog(@"walletTransDataObj.transactionAmount---- %@",walletTransDataObj.transactionOperatorName);
    
    
    if([walletTransDataObj.transactionAmount isEqual:[NSNull null]]){
        NSLog(@"Its Null");
    }
    else
    walletHistoryCustomCell.walletMoneyLabel.text = [NSString stringWithFormat:@"Rs. %@.00",walletTransDataObj.transactionAmount];
    
  /*  if([walletTransDataObj.transactionTransCategory isEqualToString:@"refill"] || [walletTransDataObj.transactionTransCategory isEqualToString:@"recharge"] ){
    walletHistoryCustomCell.closingBalanceTxtLabel.textColor = [UIColor redColor];
    
        
    if([walletTransDataObj.transactionOperatorName isEqual:[NSNull null]]){
    NSLog(@"#NoOperatorFound");
    }
    else
    walletHistoryCustomCell.operatorLogoImageView.image = [UIImage imageNamed:[self giveLogoImageAccordingToOperator:walletTransDataObj.transactionOperatorName]];
    }*/
    
    if([walletTransDataObj.transactionClosingBalance isEqual:[NSNull null]]){
        NSLog(@"Its Null");
    }
    else
    walletHistoryCustomCell.closingBalanceTxtLabel.text = [NSString stringWithFormat:@"Closing balance: Rs.%@.00",walletTransDataObj.transactionClosingBalance];
    
    
    if([walletTransDataObj.transactionStatus intValue] == 4){
        walletHistoryCustomCell.walletStatusLabel.text = @"TRANSACTION REVERSED";
        //[walletTransDataObj.transactionTransCategory uppercaseString];
    }
    
    if([walletTransDataObj.transactionStatus intValue] == 3 && [walletTransDataObj.transactionServiceID intValue] == 1){
        walletHistoryCustomCell.walletStatusLabel.text = @"PREPAID RECHARGE";
        //[walletTransDataObj.transactionTransCategory uppercaseString];
        
        walletHistoryCustomCell.operatorLogoImageView.image = [UIImage imageNamed:[self giveLogoImageAccordingToOperator:walletTransDataObj.transactionOperatorName]];
        walletHistoryCustomCell.closingBalanceTxtLabel.textColor = [UIColor redColor];

    }
    
    if([walletTransDataObj.transactionStatus intValue] == 3 && [walletTransDataObj.transactionServiceID intValue] == 2){
        walletHistoryCustomCell.walletStatusLabel.text = @"DTH RECHARGE";
        //[walletTransDataObj.transactionTransCategory uppercaseString];
        
        walletHistoryCustomCell.operatorLogoImageView.image = [UIImage imageNamed:[self giveLogoImageAccordingToOperator:walletTransDataObj.transactionOperatorName]];
        walletHistoryCustomCell.closingBalanceTxtLabel.textColor = [UIColor redColor];

    }
    
    if([walletTransDataObj.transactionStatus intValue] == 3 && [walletTransDataObj.transactionServiceID intValue] == 4){
        walletHistoryCustomCell.walletStatusLabel.text = @"POSTPAID BILL";
        //[walletTransDataObj.transactionTransCategory uppercaseString];
        
        walletHistoryCustomCell.operatorLogoImageView.image = [UIImage imageNamed:[self giveLogoImageAccordingToOperator:walletTransDataObj.transactionOperatorName]];
        walletHistoryCustomCell.closingBalanceTxtLabel.textColor = [UIColor redColor];

    }
    
    if([walletTransDataObj.transactionStatus intValue] == 3 && [walletTransDataObj.transactionServiceID intValue] == 5){
        walletHistoryCustomCell.walletStatusLabel.text = @"DATACARD RECHARGE";
        //[walletTransDataObj.transactionTransCategory uppercaseString];
        
        walletHistoryCustomCell.operatorLogoImageView.image = [UIImage imageNamed:[self giveLogoImageAccordingToOperator:walletTransDataObj.transactionOperatorName]];
        walletHistoryCustomCell.closingBalanceTxtLabel.textColor = [UIColor redColor];

    }
    
    if([walletTransDataObj.transactionStatus intValue] == 2){
        walletHistoryCustomCell.walletStatusLabel.text = @"WALLET TOPUP";
        //[walletTransDataObj.transactionTransCategory uppercaseString];
    }
    
    
    walletHistoryCustomCell.walletTimeLabel.text = [Constant convertServerDate:walletTransDataObj.transactionDateTime];
    
    [walletHistoryImageView removeFromSuperview];
    [walletHistoryLabel removeFromSuperview];
    
    
    
    return walletHistoryCustomCell;
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath;
{
    return 88;
}



-(WalletHistoryCriteria *)walletHistoryCriteria{
    
    WalletHistoryCriteria *criteria = [[WalletHistoryCriteria alloc]init];
    criteria.walletHistoryParamCriteria = @"Description";
    criteria.walletHistoryPageCriteria = [NSString stringWithFormat:@"%d",walletValue];
    criteria.walletHistoryLimitCriteria = @"10";
    
    return criteria;
}

-(void)buttonLoadMoreTouched{
    NSLog(@"#ReloadMore");
    
    [self showHUDOnView];
    
    [bottomButton setTitle:@"Loading..." forState:UIControlStateNormal];

    
    walletValue++;
    
    [self callWalletHistoryServiceBlock];
}

-(void)backClicked:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
