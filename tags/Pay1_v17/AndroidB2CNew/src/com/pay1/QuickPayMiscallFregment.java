package com.pay1;

import java.util.ArrayList;
import java.util.List;
import java.util.WeakHashMap;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.SQLException;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.pay1.adapterhandler.QuickPayMiscallAdapter;
import com.pay1.constants.Constants;
import com.pay1.databasehandler.QuickPay;
import com.pay1.databasehandler.QuickPayDataSource;

public class QuickPayMiscallFregment extends Fragment {

	private static final String SCREEN_LABEL = "Quick Pay Misscall Screen";
	private EasyTracker easyTracker = null;
	private TextView textView_Title, textView_NoData, textView_Shop;
	private ListView gridView_Recharges;
	private QuickPayMiscallAdapter adapter;
	private RelativeLayout relativeLayout_EmptyView;
	private ArrayList<WeakHashMap<String, String>> data;

	public static final String ID = "id";
	public static final String OPERATOR = "operator";
	public static final String MOBILE = "mobile";
	public static final String AMOUNT = "amount";
	public static final String FLAG = "delegate_flag";
	public static final String MISSCALL = "missed_number";

	int rechargeType, rechAmount, service_charge;
	String ed_mobile, ed_amount, ed_OperatorCode, ed_OperatorID,
			ed_operatorProductID, ed_stv;

	private QuickPayDataSource quickPayDataSource;

	public static QuickPayMiscallFregment newInstance() {
		QuickPayMiscallFregment fragment = new QuickPayMiscallFregment();
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	@SuppressLint("NewApi")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.quickpay_miscall_fragment,
				container, false);
		try {
			easyTracker = EasyTracker.getInstance(getActivity());
			easyTracker.set(Fields.SCREEN_NAME, SCREEN_LABEL);
			quickPayDataSource = new QuickPayDataSource(getActivity());

			Typeface Reguler = Typeface.createFromAsset(getActivity()
					.getAssets(), "EncodeSansNormal-400-Regular.ttf");
			textView_Title = (TextView) view.findViewById(R.id.textView_Title);
			textView_Title.setTypeface(Reguler);
			textView_Title.setVisibility(View.GONE);

			textView_NoData = (TextView) view
					.findViewById(R.id.textView_NoData);
			textView_NoData.setTypeface(Reguler);
			textView_Shop = (TextView) view.findViewById(R.id.textView_Shop);
			textView_Shop.setTypeface(Reguler);
			textView_Shop.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent intent = new Intent(getActivity(),
							MainActivity.class);
					intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
							| Intent.FLAG_ACTIVITY_NEW_TASK);
					intent.putExtra("FROM_QUICKPAY", true);
					intent.putExtra("IS_GIFT", false);
					getActivity().startActivity(intent);
				}
			});

			data = new ArrayList<WeakHashMap<String, String>>();

			try {
				quickPayDataSource.open();

				List<QuickPay> list = quickPayDataSource.getAllQuickPay(1);

				if (list.size() != 0) {
					for (int i = 0; i < list.size(); i++) {
						if (list.get(i).getQuickPayFlag() != Constants.BILL_PAYMENT
								&& list.get(i).getQuickPayFlag() != Constants.RECHARGE_DTH) {
							WeakHashMap<String, String> map = new WeakHashMap<String, String>();
							map.put(OPERATOR, ""
									+ list.get(i).getQuickPayProductID());
							map.put(MOBILE, list.get(i).getQuickPayMobile());
							map.put(AMOUNT, ""
									+ list.get(i).getQuickPayAmount());
							map.put(ID, "" + list.get(i).getQuickPayID());
							map.put(FLAG, ""
									+ list.get(i).getQuickPayDelegateFlag());
							map.put(MISSCALL, list.get(i)
									.getQuickPayMissCallNumber());
							data.add(map);
						}
					}
					textView_Title.setVisibility(View.VISIBLE);
				}

			} catch (SQLException exception) {
				// TODO: handle exception
				// // // Log.e("Er ", exception.getMessage());
			} catch (Exception e) {
				// TODO: handle exception
				// // // Log.e("Er ", e.getMessage());
			} finally {
				quickPayDataSource.close();
			}

			gridView_Recharges = (ListView) view
					.findViewById(R.id.gridView_Recharges);
			relativeLayout_EmptyView = (RelativeLayout) view
					.findViewById(R.id.relativeLayout_EmptyView);
			gridView_Recharges.setEmptyView(relativeLayout_EmptyView);

			adapter = new QuickPayMiscallAdapter(getActivity(), data);
			gridView_Recharges.setAdapter(adapter);
			// gridView_Recharges
			// .setOnItemClickListener(new AdapterView.OnItemClickListener() {
			//
			// @Override
			// public void onItemClick(AdapterView<?> arg0, View arg1,
			// int pos, long arg3) {
			// // TODO Auto-generated method stub
			//
			//
			// }
			// });

		} catch (Exception e) {
		}
		return view;
	}

	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView().build());
		// EasyTracker.getInstance(this).activityStart(this);
		easyTracker.send(MapBuilder.createAppView().build());
	}

	@Override
	public void onStop() {
		super.onStop();
		// EasyTracker.getInstance(this).activityStop(this);
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		// outState.putString(KEY_CONTENT, mContent);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.Fragment#onDestroy()
	 */
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		// Intent intent = new Intent(getActivity(), MainActivity.class);
		// intent.putExtra("FROM_QUICKPAY", false);
		// intent.putExtra("IS_GIFT", false);
		// intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
		// | Intent.FLAG_ACTIVITY_NEW_TASK);
		// getActivity().startActivity(intent);
		// getActivity().finish();
	}

}