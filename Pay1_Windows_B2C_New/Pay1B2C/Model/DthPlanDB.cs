﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pay1B2C.Model
    {
    class DthPlanDB
        {
        public DthPlanDB()
            {
            }
        public DthPlanDB(int dthPlanAmount1, string dthPlanOpertorID1, string dthPlanOperatorName1, string dthPlanCircleID1, string dthPlanCircleName1, string dthPlanType1, string dthPlanValidity1, string dthPlanDescription1, DateTime dthPlanUptateTime1)
            {


            dthPlanAmount = dthPlanAmount1;
            dthPlanOpertorID = dthPlanOpertorID1;
            dthPlanOperatorName = dthPlanOperatorName1;
            dthPlanCircleID = dthPlanCircleID1;
            dthPlanCircleName = dthPlanCircleName1;
            dthPlanType = dthPlanType1;
            dthPlanValidity = dthPlanValidity1;
            dthPlanDescription = dthPlanDescription1;
            dthPlanUptateTime = dthPlanUptateTime1;

            }



        int _dthPlanAmount;
        string _dthPlanOpertorID;
        string _dthPlanOperatorName;
        string _dthPlanCircleID;
        string _dthPlanCircleName;
        string _dthPlanType;
        string _dthPlanValidity;
        string _dthPlanDescription;
        DateTime _dthPlanUptateTime;



        public int dthPlanAmount
            {
            get { return _dthPlanAmount; }
            set { _dthPlanAmount = value; }
            }

        public string dthPlanOpertorID
            {
            get { return _dthPlanOpertorID; }
            set { _dthPlanOpertorID = value; }
            }

        public string dthPlanOperatorName
            {
            get { return _dthPlanOperatorName; }
            set { _dthPlanOperatorName = value; }
            }
        public string dthPlanCircleID
            {
            get { return _dthPlanCircleID; }
            set { _dthPlanCircleID = value; }
            }

        public string dthPlanCircleName
            {
            get { return _dthPlanCircleName; }
            set { _dthPlanCircleName = value; }
            }
        public string dthPlanType
            {
            get { return _dthPlanType; }
            set { _dthPlanType = value; }
            }
        public string dthPlanValidity
            {
            get { return _dthPlanValidity; }
            set { _dthPlanValidity = value; }
            }

        public string dthPlanDescription
            {
            get { return _dthPlanDescription; }
            set { _dthPlanDescription = value; }
            }
        public DateTime dthPlanUptateTime
            {
            get { return _dthPlanUptateTime; }
            set { _dthPlanUptateTime = value; }
            }


        }
    }

