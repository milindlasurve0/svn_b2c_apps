package com.pay1.adapterhandler;

import java.util.ArrayList;
import java.util.WeakHashMap;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.location.LocationManager;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pay1.DealDetailsActivity;
import com.pay1.DealGalleryActivity;
import com.pay1.DealLocationActivity;
import com.pay1.R;
import com.pay1.constants.Constants;
import com.pay1.imagehandler.ImageLoader;

public class DealDetailsAdapter extends BaseAdapter {

	private Context context;
	private ArrayList<WeakHashMap<String, String>> data;
	private ArrayList<WeakHashMap<String, String>> data_image;
	private ArrayList<WeakHashMap<String, String>> data_location;
	private ImageLoader imageLoader;
	private Typeface Reguler, Narrow, Narrow_thin;

	// ArrayList<String> path;

	public DealDetailsAdapter(Context context,
			ArrayList<WeakHashMap<String, String>> data,
			ArrayList<WeakHashMap<String, String>> data_image,
			ArrayList<WeakHashMap<String, String>> data_location) {
		this.context = context;
		this.data = data;
		this.data_image = data_image;
		this.data_location = data_location;
		imageLoader = new ImageLoader(context);
		Reguler = Typeface.createFromAsset(context.getAssets(),
				"EncodeSansNormal-400-Regular.ttf");

		Narrow = Typeface.createFromAsset(context.getAssets(),
				"EncodeSansNarrow-500-Medium.ttf");

		Narrow_thin = Typeface.createFromAsset(context.getAssets(),
				"EncodeSansNarrow-100-Thin.ttf");
		// path = new ArrayList<String>();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@SuppressLint("NewApi")
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder viewHolder;
		// if (convertView == null) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		viewHolder = new ViewHolder();

		WeakHashMap<String, String> map = new WeakHashMap<String, String>();
		map = data.get(position);

		if ("HEADER"
				.equalsIgnoreCase(map.get(DealDetailsActivity.CONTENT_TYPE))) {
			convertView = inflater.inflate(
					R.layout.deal_details_header_adapter, null);

			viewHolder = new ViewHolder();
			viewHolder.imageView_Pic = (ImageView) convertView
					.findViewById(R.id.imageView_Pic);
			viewHolder.textView_DealName = (TextView) convertView
					.findViewById(R.id.textView_DealName);
			viewHolder.textView_DealName.setTypeface(Reguler);
			viewHolder.textView_Amount = (TextView) convertView
					.findViewById(R.id.textView_Amount);
			viewHolder.textView_Amount.setTypeface(Reguler);
			viewHolder.textView_OfferAmount = (TextView) convertView
					.findViewById(R.id.textView_OfferAmount);
			viewHolder.textView_OfferAmount.setTypeface(Narrow);
			viewHolder.textView_Sold = (TextView) convertView
					.findViewById(R.id.textView_Sold);
			viewHolder.textView_Sold.setTypeface(Narrow);
			viewHolder.textView_Discount = (TextView) convertView
					.findViewById(R.id.textView_Discount);
			viewHolder.textView_Discount.setTypeface(Narrow);

		} else if ("DEAL".equalsIgnoreCase(map
				.get(DealDetailsActivity.CONTENT_TYPE))) {
			convertView = inflater.inflate(
					R.layout.deal_details_content_adapter, null);

			viewHolder.textView_Title = (TextView) convertView
					.findViewById(R.id.textView_Title);
			viewHolder.webView_Details = (WebView) convertView
					.findViewById(R.id.webView_Details);
			viewHolder.webView_Details
					.setOnLongClickListener(new View.OnLongClickListener() {

						@Override
						public boolean onLongClick(View v) {
							// TODO Auto-generated method stub
							return true;
						}
					});
			viewHolder.webView_Details.setLongClickable(false);

			viewHolder.webView_Details.setBackgroundColor(0x00000000);
			if (Build.VERSION.SDK_INT >= 11)
				viewHolder.webView_Details.setLayerType(
						WebView.LAYER_TYPE_SOFTWARE, null);
		} else if ("LOCATION".equalsIgnoreCase(map
				.get(DealDetailsActivity.CONTENT_TYPE))) {
			convertView = inflater.inflate(
					R.layout.deal_details_location_adapter, null);

			viewHolder.linearLayout_Address = (LinearLayout) convertView
					.findViewById(R.id.linearLayout_Address);
			viewHolder.textView_TitleLoc = (TextView) convertView
					.findViewById(R.id.textView_TitleLoc);
			viewHolder.textView_TitleLoc.setTypeface(Reguler);
			viewHolder.textView_Address = (TextView) convertView
					.findViewById(R.id.textView_Address);
			viewHolder.textView_Address.setTypeface(Reguler);
			viewHolder.textView_City = (TextView) convertView
					.findViewById(R.id.textView_City);
			viewHolder.textView_City.setTypeface(Reguler);
			viewHolder.textView_State = (TextView) convertView
					.findViewById(R.id.textView_State);
			viewHolder.textView_State.setTypeface(Reguler);
			viewHolder.textView_Count = (TextView) convertView
					.findViewById(R.id.textView_Count);
			viewHolder.textView_Count.setTypeface(Reguler);
		} else if ("IMAGE".equalsIgnoreCase(map
				.get(DealDetailsActivity.CONTENT_TYPE))) {
			// convertView = inflater.inflate(
			// R.layout.deal_details_image_gallery_adapter, null);
			// viewHolder.viewPager = (ViewPager) convertView
			// .findViewById(R.id.pager);
			convertView = inflater.inflate(
					R.layout.deal_details_gallery_adapter, null);
			viewHolder.textView_TitleGal = (TextView) convertView
					.findViewById(R.id.textView_TitleGal);
			viewHolder.textView_TitleGal.setTypeface(Reguler);
			viewHolder.textView_Info = (TextView) convertView
					.findViewById(R.id.textView_Info);
			viewHolder.textView_Info.setTypeface(Reguler);
			viewHolder.imageView_Gallery = (ImageView) convertView
					.findViewById(R.id.imageView_Gallery);
		}

		convertView.setTag(viewHolder);

		// } else {
		// viewHolder = (ViewHolder) convertView.getTag();
		// }

		if ("HEADER"
				.equalsIgnoreCase(map.get(DealDetailsActivity.CONTENT_TYPE))) {
			imageLoader.getDealImage(map.get(DealDetailsActivity.URL),
					viewHolder.imageView_Pic);

			viewHolder.textView_DealName.setText(map
					.get(DealDetailsActivity.DEAL_NAME));
			viewHolder.textView_Amount.setText("Rs. "
					+ map.get(DealDetailsActivity.ACTUAL_PRICE));
			viewHolder.textView_Amount.setPaintFlags(viewHolder.textView_Amount
					.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
			viewHolder.textView_OfferAmount.setText("Rs. "
					+ map.get(DealDetailsActivity.OFFER_PRICE));
			viewHolder.textView_Sold.setText(map
					.get(DealDetailsActivity.STOCK_SOLD) + " Bought");
			viewHolder.textView_Discount.setText(map
					.get(DealDetailsActivity.DISCOUNT) + "% Discount");

			double actual_amt = 0;
			try {
				actual_amt = Double.parseDouble(map
						.get(DealDetailsActivity.ACTUAL_PRICE));
			} catch (Exception e) {
				actual_amt = 0;
			}

			if (actual_amt == 0) {
				viewHolder.textView_Amount.setVisibility(View.GONE);
				viewHolder.textView_Discount.setVisibility(View.GONE);
			} else {
				viewHolder.textView_Amount.setVisibility(View.VISIBLE);
				viewHolder.textView_Discount.setVisibility(View.VISIBLE);
			}

		} else if ("DEAL".equalsIgnoreCase(map
				.get(DealDetailsActivity.CONTENT_TYPE))) {
			// viewHolder.textView_Title.setText(map
			// .get(DealDetailsActivity.TITLE));
			String str = map.get(DealDetailsActivity.CONTENT).toString().trim();
			viewHolder.webView_Details
					.loadDataWithBaseURL(
							null,
							"<html><head><style>@font-face {font-family: Normal;src: url(\"file:///android_asset/EncodeSansNormal-100-Thin.ttf\")}"
									+ "@font-face {font-family: Narrow;src: url(\"file:///android_asset/EncodeSansNarrow-500-Medium.ttf\")}"
									+ "body {margin: 0px 15px;padding:0px;color:#6E6E70;}"
									+ "h1{font-size:24px;font-family:\"Normal\";margin:0;padding:10px 0 5px 0;color:#3F3F40;}"
									+ "h2{font-size:16px;font-family:\"Narrow\";margin:0;padding:10px 0 5px 0;color:#513D98;}"
									+ "ul{margin:0 0 10px 0;padding-left:20px;}"
									+ "li{font-size:16px;margin:0 0 2px 0;font-family:\"Normal\";}</style></head><body>"
									+ str + "</body></html>", "text/html",
							"UTF-8", null);
		} else if ("LOCATION".equalsIgnoreCase(map
				.get(DealDetailsActivity.CONTENT_TYPE))) {

			viewHolder.textView_Address.setText(map
					.get(DealDetailsActivity.ADDRESS));
			viewHolder.textView_City.setText(map.get(DealDetailsActivity.CITY));
			viewHolder.textView_State.setText(map
					.get(DealDetailsActivity.STATE));
			if (data_location.size() > 1) {
				viewHolder.textView_Count.setText((data_location.size() - 1)
						+ " more location.");
			} else {
				viewHolder.textView_Count.setVisibility(View.GONE);
			}
			viewHolder.linearLayout_Address
					.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							boolean gps_enabled = false;
							boolean network_enabled = false;
							LocationManager lm = (LocationManager) context
									.getSystemService(Context.LOCATION_SERVICE);
							try {
								gps_enabled = lm
										.isProviderEnabled(LocationManager.GPS_PROVIDER);
							} catch (Exception ex) {
							}
							try {
								network_enabled = lm
										.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
							} catch (Exception ex) {
							}

							// don't start listeners if no provider is
							// enabled
							if (gps_enabled || network_enabled) {
								Intent intent = new Intent(context,
										DealLocationActivity.class);
								intent.putExtra("LOCATIONS", data_location);
								context.startActivity(intent);
							} else {
								Constants
										.showTwoButtonDialog(
												context,
												"Deal",
												"To enhance your Shop Locator experience:\n\n1.Turn on GPS and mobile network location\n2.Turn on Wi-Fi",
												Constants.DIALOG_CLOSE_LOCATION);
							}

						}
					});

		} else if ("IMAGE".equalsIgnoreCase(map
				.get(DealDetailsActivity.CONTENT_TYPE))) {

			// if (path.size() == 0) {
			// for (int i = 0; i < data.size(); i++) {
			// WeakHashMap<String, String> map1 = new WeakHashMap<String,
			// String>();
			// map1 = data.get(i);
			// if ("IMAGE".equalsIgnoreCase(map1
			// .get(DealDetailsActivity.CONTENT_TYPE))) {
			// path.add(map1.get(DealDetailsActivity.GALARY_URL));
			// }
			// }
			// }

			/** Sliding Image Gallery **/
			/*
			 * if (data_image.size() != 0) { DealDetailsImageGalleryAdapter
			 * adapter = new DealDetailsImageGalleryAdapter( context,
			 * data_image); viewHolder.viewPager.setAdapter(adapter);
			 * 
			 * // displaying selected image first
			 * viewHolder.viewPager.setCurrentItem(0); } else {
			 * viewHolder.viewPager.setVisibility(View.GONE); }
			 */

			imageLoader.getDealImage(
					data.get(position).get(DealDetailsActivity.GALARY_URL),
					viewHolder.imageView_Gallery);
			viewHolder.textView_Info.setText(map
					.get(DealDetailsActivity.GALARY_INFO));
			viewHolder.imageView_Gallery
					.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							Intent intent = new Intent(context,
									DealGalleryActivity.class);
							intent.putExtra("GALLERY", data_image);
							context.startActivity(intent);
						}
					});
		}
		// viewHolder.webView_Details
		// .loadDataWithBaseURL(
		// null,
		// "<div style='border-radius: 10px;box-shadow: 0 1px 2px #888888;float: left;height: 320px;margin: 5px;'>"
		// + "<div>"
		// +
		// "<img src='http://cdev.pay1.in/kcfinder/upload/files/1397196557324.jpg' style='border-radius:10px 10px 0px 0px; height:176px; width:292px' />"
		// + "</div>"
		// +
		// "<div class='detailsContainer' style='height: 112px;padding-left: 10px;position: absolute;width: 275px;'>"
		// +
		// "<div class='dealsTitle' style='color: #0185C6; display: inline-block; font-size: 1.1em; font-weight: bold;  overflow: hidden; width: 100%;'>"
		// + "7 Locations: Flat 50% OFF From The Menu at Rollacosta Restaurant"
		// + "</div>"
		// + "<div class='dealsTitleSold'>"
		// +
		// "<p><strong><span style='color:#A52A2A'>Rollacosta Restaurant</span></strong></p>"
		// +
		// "<div class='price_sold' style=' color: #89B556;float: right;font-size: 1.4em;font-weight: 500;line-height: 1.1em;margin-top: 8px;padding-right: 5px;text-align: right;width: 100%;'>"
		// + "<span style='color:#A9A9A9'>"
		// + "<s>Rs29.00</s>"
		// + "</span> Rs29.00"
		// + "</div>" + "</div>" + "</div>" + "</div>",
		// "text/html", "UTF-8", null);

		return convertView;
	}

	private class ViewHolder {
		TextView textView_Title, textView_DealName, textView_Amount,
				textView_OfferAmount, textView_Sold, textView_Discount,
				textView_TitleLoc, textView_Address, textView_City,
				textView_State, textView_Count, textView_TitleGal,
				textView_Info;
		WebView webView_Details;
		ImageView imageView_Pic, imageView_Gallery;
		LinearLayout linearLayout_Address;
		// ViewPager viewPager;
	}
}
