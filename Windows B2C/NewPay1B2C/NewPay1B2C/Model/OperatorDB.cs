﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace NewPay1B2C.Model
{
    public class OperatorDB
    {
        public OperatorDB()
        {
        }
        public OperatorDB(string Id, string Name, string Code, string P_id, string Stv, string flag1, string service_id1, string delegateflag1, string min1, string max1, string charges_slab1, string service_charge_amount1, string service_tax_percent1, string service_charge_percent1, string prefix1, string length1)
        {
            id = Id;
            name = Name;
            code = Code;
            p_id = P_id;
            stv = Stv;
            flag = flag1;
            service_id = service_id1;
            delegateFlag = delegateflag1;
            min=min1;
            max=max1;
            charges_slab=charges_slab1;
            service_charge_amount=service_charge_amount1;
            service_tax_percent=service_tax_percent1;
            service_charge_percent = service_charge_percent1;
            prefix=prefix1;
            length=length1;
        }
        string _id;
        string _code;
        string _p_id;
        string _stv;
        string _name;
        string _max;
        string _min;
        string _charges_slab;
        string _service_charge_amount;
        string _service_tax_percent;
        string _service_charge_percent;
        string _prefix;
        string _flag;
        string _service_id;
        string _delegateFlag;
        string _length;

        public string flag
        {
            get { return _flag; }
            set { _flag = value; }
        }

        public string length
        {
            get { return _length; }
            set { _length = value; }
        }

        public string prefix
        {
            get { return _prefix; }
            set { _prefix = value; }
        }
        
        public string service_id
        {
            get { return _service_id; }
            set { _service_id = value; }
        }

        public string delegateFlag
        {
            get { return _delegateFlag; }
            set { _delegateFlag = value; }
        }

        public string id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string name
        {
            get { return _name; }
            set { _name = value; }
        }

        public string code
        {
            get { return _code; }
            set { _code = value; }
        }
        public string p_id
        {
            get { return _p_id; }
            set { _p_id = value; }
        }
        public string stv
        {
            get { return _stv; }
            set { _stv = value; }
        }


        public string min
        {
            get { return _min; }
            set { _min = value; }
        }
        public string max
        {
            get { return _max; }
            set { _max = value; }
        }


        public string charges_slab
        {
            get { return _charges_slab; }
            set { _charges_slab = value; }
        }
        public string service_charge_amount
        {
            get { return _service_charge_amount; }
            set { _service_charge_amount = value; }
        }

        public string service_tax_percent
        {
            get { return _service_tax_percent; }
            set { _service_tax_percent = value; }
        }
        public string service_charge_percent
        {
            get { return _service_charge_percent; }
            set { _service_charge_percent = value; }
        }
       
    }
}
