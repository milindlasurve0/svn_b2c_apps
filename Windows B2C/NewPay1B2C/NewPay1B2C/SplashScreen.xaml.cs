﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Phone.Info;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Xml;
using System.Globalization;
using Microsoft.Phone.Tasks;
using System.ComponentModel;
using NewPay1B2C.Model;
using SQLiteClient;

namespace NewPay1B2C
{
    public partial class SplashScreen : PhoneApplicationPage
    {
		//SQLiteConnection db = null;
        public SplashScreen()
        {
            InitializeComponent();
			if (Constant.checkInternetConnection ())
				{
				
			GoogleAnalytics.EasyTracker.GetTracker ().SendView ("SplashScreen");
			
			

				/*try
					{
					SQLiteCommand cmd = db.CreateCommand ("Create table NumberAndCircle (area_name text,area text,opr_name text,numberOperator text,number text,product_id text)");
					int i = cmd.ExecuteNonQuery ();
					}
				catch (Exception ee)
					{
					System.Diagnostics.Debug.WriteLine (ee.Message);
					}
					   */



				try
					{
					SQLiteCommand cmdFreeBie = db.CreateCommand ("Create table FreeBieDataBase (freeBie text)");
					int i3 = cmdFreeBie.ExecuteNonQuery ();
					}
				catch (Exception ee)
					{
					System.Diagnostics.Debug.WriteLine (ee.Message);
					}

				
				}
			else
				{
				}
				}


			//getDealsTask (Constant.LoadPersistent<string> (Constant.SETTINGS_USER_LATTITUDE), Constant.LoadPersistent<string> (Constant.SETTINGS_USER_LONGITUDE));
            BackgroundWorker backroungWorker = new BackgroundWorker ();
            backroungWorker.DoWork += new DoWorkEventHandler (backroungWorker_DoWork);
           // backroungWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(backroungWorker_RunWorkerCompleted);

            backroungWorker.RunWorkerAsync ();
          
          
            
        }

        private void backroungWorker_RunWorkerCompleted (object sender, RunWorkerCompletedEventArgs e)
            {
            NavigationService.Navigate (new Uri ("/HomePage.xaml?", UriKind.RelativeOrAbsolute));

            NavigationService.RemoveBackEntry ();
            }

        private void backroungWorker_DoWork (object sender, DoWorkEventArgs e)
            {

			try
				{
				if (db == null)
					{
					db = new SQLiteConnection (Constant.DATABASE_NAME);
					db.Open ();
					}
				}
			catch (SQLiteException sqe)
				{
				}
			catch (Exception ee)
				{
				}
			PhoneApplicationService.Current.State["loginCall"] = 1;
            autoLogin ();


			try
				{

				SQLiteCommand cmd1 = db.CreateCommand ("SELECT *  FROM OperatorTable");
				var lst = cmd1.ExecuteQuery<OperatorDB> ();
				List<OperatorDB> queryResult = lst.ToList ();
				if (queryResult.Count <= 1)
					{
					DateTime currentDateOp = DateTime.Now;//.ToString();
					DateTime updatedDateOp = Constant.LoadPersistent<DateTime> (Constant.OPERATOR_LIST_UPDATE_TIME);
					TimeSpan durationOp = currentDateOp - updatedDateOp;
					if (durationOp.Days >= 7)
						{
						SQLiteCommand cmd2 = db.CreateCommand ("DELETE FROM OperatorTable");
						int i = cmd2.ExecuteNonQuery ();
						GetAndSaveOperator ();
						}
					}
				//GetAndSaveOperator ();
				}
			catch (SQLiteException se)
				{
				string see = se.Message;
				}
			   try
				{
			SQLiteCommand cmd11 = db.CreateCommand ("SELECT * FROM NumberAndCircle");
			var lst1 = cmd11.ExecuteQuery<NumberAndCircle> ();
			List<NumberAndCircle> result1 = lst1.ToList ();
			if (result1.Count >= 3)
				{
				DateTime CurrentTime = DateTime.Now;
				DateTime EndTime = Constant.LoadPersistent<DateTime> (Constant.CIRCLE_INSERT_TIME);
				if (EndTime != null)
					{
					TimeSpan ts = EndTime - CurrentTime;

					if (ts.Days > 1)
						{
						if (Constant.checkInternetConnection ())
							{
							 getAllCircles ();
							}
						}
					}
				}
			else
				{
				if (Constant.checkInternetConnection ())
					{
					getAllCircles ();
					}
				}
				}
			   catch (SQLiteException se)
				   {
				   string see = se.Message;
				   }
			try{
				SQLiteCommand cmd22 = db.CreateCommand ("DELETE FROM FreeBieDataBase");
				int i2 = cmd22.ExecuteNonQuery ();
				//getDealsTask (Constant.LoadPersistent<string> (Constant.SETTINGS_USER_LATTITUDE), Constant.LoadPersistent<string> (Constant.SETTINGS_USER_LONGITUDE));
			}
			catch (SQLiteException se)
				{
				string see = se.Message;
				}
            
            }


        private async void autoLogin()
        {			  try{
                        var values = new List<KeyValuePair<string, string>>
                        { 
                        new KeyValuePair<string, string>("actiontype", "signin"),
                        new KeyValuePair<string, string>("username",Constant.getUserNumber()),
                        new KeyValuePair<string, string>("password",Constant.getOtp()),
                        new KeyValuePair<string, string>("uuid", Constant.getUUID()),
                        new KeyValuePair<string, string>("latitude","00"),
                        new KeyValuePair<string, string>("longitude","00"),
                        new KeyValuePair<string, string>("device_type","windows"),
                        new KeyValuePair<string, string>("manufacturer",DeviceStatus.DeviceManufacturer),
                        new KeyValuePair<string, string>("os_info",Environment.OSVersion.Version.ToString()),
                        new KeyValuePair<string, string>("gcm_reg_id",Constant.LoadPersistent<string>(Constant.NOTIFICATION_URL)),
                        new KeyValuePair<string, string>("api","true"),
                        };

            FormUrlEncodedContent values1 = new FormUrlEncodedContent(values);
            Task<string> response = Constant.postParamsAndgetResponse(Constant.B2C_URL, values1);

            string result = await response;
            if (string.IsNullOrEmpty(result))
            {
               // NavigationService.Navigate(new Uri("/NoConnection.xaml?", UriKind.Relative));
            }
            else
            {
                var dict = (JObject)JsonConvert.DeserializeObject(result);
                string status = dict["status"].ToString();
                if (status.Equals("success"))
                {
                    var description = dict["description"];
                    Constant.SavePersistent(Constant.SETTINGS_USER_NAME, description["name"].ToString());
                    Constant.SavePersistent(Constant.SETTINGS_USER_PIN, Constant.getOtp());
                    Constant.setUserNumber(description["mobile"].ToString());
                    Constant.SavePersistent(Constant.SETTINGS_USER_GENDER, description["gender"].ToString());
                    Constant.SavePersistent(Constant.SETTINGS_USER_EMAIL, description["email"].ToString());
                    Constant.SavePersistent(Constant.SETTINGS_USER_ID, description["user_id"].ToString());
                    Constant.SavePersistent(Constant.SETTINGS_USER_DOB, description["date_of_birth"].ToString());
                    Constant.setbalance(description["wallet_balance"].ToString());
                    Constant.SavePersistent(Constant.SETTINGS_USER_LATTITUDE, description["latitude"].ToString());
                    Constant.SavePersistent(Constant.SETTINGS_USER_LONGITUDE, description["longitude"].ToString());
                    Constant.SavePersistent(Constant.SETTINGS_USER_SUPPORT_NUMBER, description["support_number"].ToString());
                    Constant.SavePersistent(Constant.SETTINGS_IS_LOGIN, true);
                    Dispatcher.BeginInvoke (delegate ()
                    {

                   NavigationService.Navigate (new Uri ("/HomePage.xaml", UriKind.RelativeOrAbsolute));
                   //this.NavigationService.RemoveBackEntry ();

                    });
                }
                else
                {
                    MessageBox.Show(dict["description"].ToString());
                }

            }
		}
		catch (Exception e)
			{
			}
        }


	


		private async void getAllCircles ()
			{
			DateTime lastStorageCircle = Constant.LoadPersistent<DateTime> (Constant.CIRCLE_INSERT_TIME);
			// Use current time
			string format = "yyyy-MM-dd HH:mm:ss";    // Use this format
			Console.WriteLine (lastStorageCircle.ToString (format));
			String lastUrl = "https://panel.pay1.in/apis/receiveWeb/mindsarray/mindsarray/json?method=getMobileDetails&mobile=all&timestamp=";// +lastStorageCircle.ToString (format);
			var httpClient = new HttpClient (new HttpClientHandler ());
			HttpResponseMessage response = await httpClient.PostAsync (lastUrl, null);
			response.EnsureSuccessStatusCode ();
			long cl = (long)response.Content.Headers.ContentLength;
			var responseString = await response.Content.ReadAsStringAsync ();
			int count = 0;
			try
				{
				db.Open ();


				string output = responseString.Remove (responseString.Length - 1, 1);
				string output1 = output.Remove (output.Length - 1, 1);
				string output2 = output1.Remove (0, 1);
				string output3 = output2.Remove (0, 1);
				string output4 = output3.Remove (output3.Length - 1, 1);
				NumberAndCircle numberAndCircle = new NumberAndCircle ();
				var dict = (JObject)JsonConvert.DeserializeObject (output4);
				string status = dict["status"].ToString ();
				int inseted = 0;
				var details = dict["details"];
				foreach (var item in details)
					{
					inseted = inseted + 1;
					numberAndCircle.area = item["area"].ToString ();
					numberAndCircle.area_name = item["area_name"].ToString ();
					numberAndCircle.number = item["number"].ToString ();
					numberAndCircle.numberOperator = item["operator"].ToString ();
					numberAndCircle.opr_name = item["opr_name"].ToString ();
					numberAndCircle.product_id = item["product_id"].ToString ();
					try
						{
						string strInsert = " Insert into NumberAndCircle (area_name,area,opr_name,numberOperator,number,product_id) values (@area_name,@area,@opr_name,@numberOperator,@number,@product_id)";
						int rec = (Application.Current as App).dbh.Insert<NumberAndCircle> (numberAndCircle, strInsert);


					/*	SQLiteCommand cmd = db.CreateCommand ("");
						cmd.CommandText = " Insert into NumberAndCircle (area_name,area,opr_name,numberOperator,number,product_id) values (@area_name,@area,@opr_name,@numberOperator,@number,@product_id)";
						int rec = cmd.ExecuteNonQuery (numberAndCircle);
						//System.Diagnostics.Debug.WriteLine("Inserted  Circle  " + inseted);  */
						}
					catch (Exception ex)
						{
						string ms = ex.Message;
						}
					}

				Constant.SavePersistent (Constant.CIRCLE_INSERT_TIME, DateTime.Now);
				System.Diagnostics.Debug.WriteLine ("Inserted  Circle  " + inseted);

				}
			catch (JsonException je)
				{
				}
			catch (Exception e)
				{
				}
			}




		private async void GetAndSaveOperator ()
			{
			//ProgressBarManager.Show(10, "Please wait...", false, 10);
			try{
			var values = new List<KeyValuePair<string, string>>
                        { 
                        new KeyValuePair<string, string>("actiontype", "get_all_operators"),
                        new KeyValuePair<string, string>("api","true"),
                        };

			FormUrlEncodedContent values1 = new FormUrlEncodedContent (values);
			Task<string> response = Constant.postParamsAndgetResponse (Constant.B2C_URL, values1);

			string result = await response;
			var dict = (JObject)JsonConvert.DeserializeObject (result);
			string status = dict["status"].ToString ();
			if (status.Equals ("success"))
				{
				Constant.SavePersistent (Constant.OPERATOR_LIST_UPDATE_TIME, DateTime.Now);
				var description = dict["description"];
				var mobile = description["mobile"];
				foreach (var item in mobile)
					{
					OperatorDB opeDb = new OperatorDB ();
					opeDb.id = item["id"].ToString ();
					opeDb.code = item["opr_code"].ToString ();
					opeDb.delegateFlag = item["delegate"].ToString ();
					opeDb.flag = item["flag"].ToString ();
					opeDb.name = item["name"].ToString ();
					opeDb.p_id = item["product_id"].ToString ();
					opeDb.service_id = item["service_id"].ToString ();
					opeDb.stv = item["stv"].ToString ();
					opeDb.prefix = item["prefix"].ToString ();
					opeDb.length = item["length"].ToString ();
					opeDb.min = item["min"].ToString ();
					opeDb.max = item["max"].ToString ();
					opeDb.charges_slab = item["charges_slab"].ToString ();
					opeDb.service_charge_amount = item["service_charge_amount"].ToString ();
					opeDb.service_tax_percent = item["service_tax_percent"].ToString ();
					opeDb.service_charge_percent = item["service_charge_percent"].ToString ();
					insertOperatorToDb (opeDb);
					}
				var data = description["data"];
				foreach (var item in data)
					{
					OperatorDB opeDb = new OperatorDB ();
					opeDb.id = item["id"].ToString ();
					opeDb.code = item["opr_code"].ToString ();
					opeDb.delegateFlag = item["delegate"].ToString ();
					opeDb.flag = item["flag"].ToString ();
					opeDb.name = item["name"].ToString ();
					opeDb.p_id = item["product_id"].ToString ();
					opeDb.service_id = item["service_id"].ToString ();
					opeDb.stv = item["stv"].ToString ();
					opeDb.prefix = item["prefix"].ToString ();
					opeDb.length = item["length"].ToString ();
					opeDb.min = item["min"].ToString ();
					opeDb.max = item["max"].ToString ();
					opeDb.charges_slab = item["charges_slab"].ToString ();
					opeDb.service_charge_amount = item["service_charge_amount"].ToString ();
					opeDb.service_tax_percent = item["service_tax_percent"].ToString ();
					opeDb.service_charge_percent = item["service_charge_percent"].ToString ();
					insertOperatorToDb (opeDb);
					}
				var postpaid = description["postpaid"];
				foreach (var item in postpaid)
					{
					OperatorDB opeDb = new OperatorDB ();
					opeDb.id = item["id"].ToString ();
					opeDb.code = item["opr_code"].ToString ();
					opeDb.delegateFlag = item["delegate"].ToString ();
					opeDb.flag = item["flag"].ToString ();
					opeDb.name = item["name"].ToString ();
					opeDb.p_id = item["product_id"].ToString ();
					opeDb.service_id = item["service_id"].ToString ();
					opeDb.stv = item["stv"].ToString ();
					opeDb.prefix = item["prefix"].ToString ();
					opeDb.length = item["length"].ToString ();
					opeDb.min = item["min"].ToString ();
					opeDb.max = item["max"].ToString ();
					opeDb.charges_slab = item["charges_slab"].ToString ();
					opeDb.service_charge_amount = item["service_charge_amount"].ToString ();
					opeDb.service_tax_percent = item["service_tax_percent"].ToString ();
					opeDb.service_charge_percent = item["service_charge_percent"].ToString ();
					insertOperatorToDb (opeDb);
					}
				var dth = description["dth"];
				foreach (var item in dth)
					{
					OperatorDB opeDb = new OperatorDB ();
					opeDb.id = item["id"].ToString ();
					opeDb.code = item["opr_code"].ToString ();
					opeDb.delegateFlag = item["delegate"].ToString ();
					opeDb.flag = item["flag"].ToString ();
					opeDb.name = item["name"].ToString ();
					opeDb.p_id = item["product_id"].ToString ();
					opeDb.service_id = item["service_id"].ToString ();
					opeDb.stv = item["stv"].ToString ();
					opeDb.prefix = item["prefix"].ToString ();
					opeDb.length = item["length"].ToString ();
					opeDb.min = item["min"].ToString ();
					opeDb.max = item["max"].ToString ();
					opeDb.charges_slab = item["charges_slab"].ToString ();
					opeDb.service_charge_amount = item["service_charge_amount"].ToString ();
					opeDb.service_tax_percent = item["service_tax_percent"].ToString ();
					opeDb.service_charge_percent = item["service_charge_percent"].ToString ();
					insertOperatorToDb (opeDb);
					}
				}
			}
			catch (Exception e)
				{
				}
			// ProgressBarManager.Hide(10);
			}

		public void insertOperatorToDb (OperatorDB opeDb)
			{
			if (db != null)
				{
				try
					{
					SQLiteCommand cmd = db.CreateCommand ("");

					cmd.CommandText = "Insert into OperatorTable (id,name,code,p_id,stv,flag,service_id,delegateFlag,min,max,charges_slab,service_charge_amount,service_tax_percent,service_charge_percent,prefix,length) values (@id,@name,@code,@p_id,@stv,@flag,@service_id,@delegateFlag,@min,@max,@charges_slab,@service_charge_amount,@service_tax_percent,@service_charge_percent,@prefix,@length)";
					int rec = cmd.ExecuteNonQuery<OperatorDB> (opeDb);
					}
				catch (SQLiteException exc)
					{
					System.Diagnostics.Debug.WriteLine (exc.Message);
					}
				}
			}
       
    }
}