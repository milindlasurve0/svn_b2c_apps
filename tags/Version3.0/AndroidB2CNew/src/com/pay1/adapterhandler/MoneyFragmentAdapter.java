package com.pay1.adapterhandler;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.pay1.CouponCodeFragment;
import com.pay1.DebitCardFragment;
import com.pay1.MapFragment;

public class MoneyFragmentAdapter extends FragmentStatePagerAdapter
// implements IconPagerAdapter
{
	protected static final String[] CONTENT = new String[] { "Locate store",
			"Online Payment", "Redeem voucher" };

	private int mCount = CONTENT.length;

	public MoneyFragmentAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int position) {
		//
		switch (position) {
		case 0:
			return MapFragment.newInstance(false, true);
		case 1:
			return DebitCardFragment.newInstance();
		case 2:
			return CouponCodeFragment.newInstance();
		default:
			return MapFragment.newInstance(false, true);
		}
	}

	@Override
	public int getCount() {
		return mCount;
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return MoneyFragmentAdapter.CONTENT[position % CONTENT.length];
	}

	// @Override
	// public int getIconResId(int index) {
	// return ICONS[index % ICONS.length];
	// }

	// public void setCount(int count) {
	// if (count > 0 && count <= 10) {
	// mCount = count;
	// notifyDataSetChanged();
	// }
	// }
}