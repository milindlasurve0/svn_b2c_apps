package com.pay1.databasehandler;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class SupportDataSource {
	// Database fields
	private SQLiteDatabase database;
	private Pay1SQLiteHelper dbHelper;

	public SupportDataSource(Context context) {
		dbHelper = new Pay1SQLiteHelper(context);
	}

	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
	}

	public void createSupport(int supportSent, String supportQuery,
			long supportUptateTime) {
		ContentValues values = new ContentValues();
		values.put(Pay1SQLiteHelper.SUPPORT_ISSENT, supportSent);
		values.put(Pay1SQLiteHelper.SUPPORT_QUERY, supportQuery);
		values.put(Pay1SQLiteHelper.SUPPORT_TIME, supportUptateTime);

		database.insert(Pay1SQLiteHelper.TABLE_SUPPORT, null, values);
		// Cursor cursor = database.query(Pay1SQLiteHelper.TABLE_Support,
		// null, Pay1SQLiteHelper.Support_ID + " = " + insertId, null,
		// null, null, null);
		// cursor.moveToFirst();
		// Support neSupport = cursorToSupport(cursor);
		// cursor.close();
		// return newSupport;
	}

	public void updateSupport(int supportSent, String supportQuery,
			long supportUptateTime, int supportID) {

		ContentValues values = new ContentValues();
		values.put(Pay1SQLiteHelper.SUPPORT_ISSENT, supportSent);
		values.put(Pay1SQLiteHelper.SUPPORT_QUERY, supportQuery);
		values.put(Pay1SQLiteHelper.SUPPORT_TIME, supportUptateTime);
		long updatetId = database.update(Pay1SQLiteHelper.TABLE_SUPPORT,
				values, Pay1SQLiteHelper.SUPPORT_ID + " = " + supportID, null);
		if (updatetId > 0) {
		}
		// // Log.i(Tag, "Monitor updated with id: " + monitor_ID);
		else {
		}
	}

	public void deleteSupport() {
		database.delete(Pay1SQLiteHelper.TABLE_SUPPORT, null, null);
	}

	public void deleteSupport(Support support) {
		long id = support.getSupportID();
		database.delete(Pay1SQLiteHelper.TABLE_SUPPORT,
				Pay1SQLiteHelper.SUPPORT_ID + " = " + id, null);
	}

	public List<Support> getAllSupport() {
		List<Support> supports = new ArrayList<Support>();

		Cursor cursor = database.query(Pay1SQLiteHelper.TABLE_SUPPORT, null,
				null, null, null, null, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Support support = cursorToSupport(cursor);
			supports.add(support);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return supports;
	}

	private Support cursorToSupport(Cursor cursor) {
		Support Support = new Support();
		Support.setSupportID(cursor.getInt(0));
		Support.setSupportSent(cursor.getInt(1));
		Support.setSupportQuery(cursor.getString(2));
		Support.setSupportUptateTime(cursor.getLong(3));
		return Support;
	}
}
