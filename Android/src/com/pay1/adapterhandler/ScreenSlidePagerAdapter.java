package com.pay1.adapterhandler;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.pay1.DealFragment;
import com.pay1.QuickPayFragment;
import com.pay1.RechargeMobileFragment;

public class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
	public ScreenSlidePagerAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int position) {
		switch (position) {
		case 0:
			return DealFragment.newInstance();
		case 1:
			return RechargeMobileFragment.newInstance();
		case 2:
			return QuickPayFragment.newInstance();
		}
		return null;

	}

	@Override
	public int getCount() {
		return 3;
	}
}