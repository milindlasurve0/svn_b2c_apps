﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.ComponentModel;
using System.Threading;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Phone.Info;
using System.Device.Location;
using Pay1B2C.ViewModel;
using Pay1B2C.Model;
using System.Windows.Markup;
using System.Windows.Shapes;

namespace Pay1B2C.Views
    {
    public partial class SplashScreen: PhoneApplicationPage
        {
        GeoCoordinateWatcher watcher;
        public SplashScreen()
            {
            InitializeComponent();
            Loaded+=(a, b) =>
            {

            PhoneApplicationService.Current.State["loginCall"] = 1;
            loginUser();
                StartLoadingData();
                if(watcher == null)
                    {
                    watcher = new GeoCoordinateWatcher(GeoPositionAccuracy.High); // Use high accuracy. 
                    watcher.MovementThreshold = 20; // Use MovementThreshold to ignore noise in the signal. 
                    watcher.StatusChanged += new EventHandler<GeoPositionStatusChangedEventArgs>(watcher_StatusChanged);
                    if(watcher.Permission == GeoPositionPermission.Denied)
                        {
                        //Location services is disable on the phone. Show a message to the user.
                        MessageBox.Show("Your location is not enabled.");

                        }
                    else
                        {
                        watcher.Start();
                        }
                    }
            };
            }

        private BackgroundWorker backroungWorker;
        private void StartLoadingData()
            {

            Constant.getCurrentLatLong();
            backroungWorker = new BackgroundWorker();
            backroungWorker.DoWork += new DoWorkEventHandler(backroungWorker_DoWork);
            backroungWorker.RunWorkerCompleted +=
new RunWorkerCompletedEventHandler(backroungWorker_RunWorkerCompleted);
            backroungWorker.RunWorkerAsync();
            }
        void backroungWorker_DoWork(object sender, DoWorkEventArgs e)
            {
             DateTime lastStorageCircle = Constant.LoadPersistent<DateTime>(Constant.CIRCLE_INSERT_TIME);
            // Use current time
            string format = "yyyy-MM-dd HH:mm:ss";    // Use this format

            Console.WriteLine(lastStorageCircle.ToString(format));
            DateTime oldTime=  lastStorageCircle;
            DateTime curTime=DateTime.Now;
            TimeSpan span=curTime-oldTime;
            if(span.Days>1)
                {
                getAllNumbers();
                }
            Thread.Sleep(4000);
            }
        void backroungWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
            {
            this.Dispatcher.BeginInvoke(() =>
            {
                (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri("/HomePage.xaml?", UriKind.RelativeOrAbsolute));
            });
            }

        private void watcher_StatusChanged(object sender, GeoPositionStatusChangedEventArgs e)
            {
            try
                {
                if(e.Status == GeoPositionStatus.Ready)
                    {
                    // Use the Position property of the GeoCoordinateWatcher object to get the current location. 
                    GeoCoordinate co = watcher.Position.Location;
                   
                    Constant.SavePersistent(Constant.SETTINGS_USER_LATTITUDE_DOUBLE, co.Latitude);
                    Constant.SavePersistent(Constant.SETTINGS_USER_LONGITUDE_DOUBLE, co.Longitude);

                    watcher.Stop();
                    }
                }
            catch(Exception exc)
                {
                }
            }




        private async void getAllNumbers()
            {
            DateTime lastStorageCircle = Constant.LoadPersistent<DateTime>(Constant.CIRCLE_INSERT_TIME);
            // Use current time
            string format = "yyyy-MM-dd HH:mm:ss";    // Use this format

            Console.WriteLine(lastStorageCircle.ToString(format));
            DateTime oldTime=  lastStorageCircle;
            DateTime curTime=DateTime.Now;
            TimeSpan span=curTime-oldTime;
            if(span.Days>1)
                {

                String lastUrl = "https://panel.pay1.in/apis/receiveWeb/mindsarray/mindsarray/json?method=getMobileDetails&mobile=all&timestamp=" + lastStorageCircle.ToString(format);
                var httpClient = new HttpClient(new HttpClientHandler());
                HttpResponseMessage response = await httpClient.PostAsync(lastUrl, null);
                response.EnsureSuccessStatusCode();
                long cl = (long)response.Content.Headers.ContentLength;
                var responseString = await response.Content.ReadAsStringAsync();
                int count = 0;
                try
                    {
                    //db.Open ();


                    string output = responseString.Remove(responseString.Length - 1, 1);
                    string output1 = output.Remove(output.Length - 1, 1);
                    string output2 = output1.Remove(0, 1);
                    string output3 = output2.Remove(0, 1);
                    string output4 = output3.Remove(output3.Length - 1, 1);

                    var dict = (JObject)JsonConvert.DeserializeObject(output4);
                    string status = dict["status"].ToString();
                    int inseted = 0;
                    var details = dict["details"];
                    getAllCircles(dict.ToString());
                    }
                catch(Exception ee)
                    {
                    }
                }
            }


        private void getAllCircles(string allNumberDetails)
            {

            NumberAndCircle numberAndCircle = new NumberAndCircle();
            var dict = (JObject)JsonConvert.DeserializeObject(allNumberDetails);
            string status = dict["status"].ToString();
            int inseted = 0;
            var details = dict["details"];

            foreach(var item in details)
                {
                NumberCircleHelper numberHelper=new NumberCircleHelper();
                inseted = inseted + 1;
                numberAndCircle.area = item["area"].ToString();
                numberAndCircle.area_name = item["area_name"].ToString();
                numberAndCircle.number = item["number"].ToString();
                numberAndCircle.numberOperator = item["operator"].ToString();
                numberAndCircle.opr_name = item["opr_name"].ToString();
                numberAndCircle.product_id = item["product_id"].ToString();
                numberHelper.Insert(numberAndCircle);
                }

            Constant.SavePersistent(Constant.CIRCLE_INSERT_TIME, DateTime.Now);
            System.Diagnostics.Debug.WriteLine("Inserted  Circle  " + inseted);

            }









        private async void loginUser()
            {

          
            var values = new List<KeyValuePair<string, string>>();

            values.Add(new KeyValuePair<string, string>("actiontype", "signin"));
            values.Add(new KeyValuePair<string, string>("username", Constant.getUserNumber()));
            values.Add(new KeyValuePair<string, string>("password", Constant.LoadPersistent<String>(Constant.SETTINGS_USER_PIN)));
            // values.Add(new KeyValuePair<string, string>("uuid", Constant.getUUID()));
            values.Add(new KeyValuePair<string, string>("latitude", Constant.LoadPersistent<string>(Constant.SETTINGS_USER_LATTITUDE)));
            values.Add(new KeyValuePair<string, string>("longitude", Constant.LoadPersistent<string>(Constant.SETTINGS_USER_LONGITUDE)));
            values.Add(new KeyValuePair<string, string>("device_type", "windows"));
            values.Add(new KeyValuePair<string, string>("manufacturer", DeviceStatus.DeviceManufacturer));
            values.Add(new KeyValuePair<string, string>("os_info", Environment.OSVersion.Version.ToString()));
          //  values.Add(new KeyValuePair<string, string>("gcm_reg_id", Constant.LoadPersistent<string>(Constant.NOTIFICATION_URL)));
            values.Add(new KeyValuePair<string, string>("gcm_reg_id", Constant.LoadPersistent<string>(Constant.NOTIFICATION_URL)));
            values.Add(new KeyValuePair<string, string>("api", "true"));
            values.Add(new KeyValuePair<string, string>("api_version", Constant.API_VERSION));


            FormUrlEncodedContent values1 = new FormUrlEncodedContent(values);
            Task<string> response = Constant.postParamsAndgetResponse(Constant.B2C_URL, values1);

            string result = await response;
            if(string.IsNullOrEmpty(result))
                {
                (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri("/Views/NoConnection.xaml?", UriKind.Relative));
                }
            else
                {
                var dict = (JObject)JsonConvert.DeserializeObject(result);
                string status = dict["status"].ToString();
                var description = dict["description"];
                if(status.Equals("success"))
                    {

                    Constant.SavePersistent(Constant.SETTINGS_USER_NAME, description["name"].ToString());
                    
                    Constant.setUserNumber(description["mobile"].ToString());
                    Constant.SavePersistent(Constant.SETTINGS_USER_PIN, Constant.LoadPersistent<String>(Constant.SETTINGS_USER_PIN));
                    Constant.SavePersistent(Constant.SETTINGS_USER_GENDER, description["gender"].ToString());
                    Constant.SavePersistent(Constant.SETTINGS_USER_DOB, description["date_of_birth"].ToString());
                    Constant.SavePersistent(Constant.SETTINGS_USER_EMAIL, description["email"].ToString());
                    Constant.setEmail(description["email"].ToString());
                    Constant.setUserName(description["name"].ToString());
                    Constant.SavePersistent(Constant.SETTINGS_USER_ID, description["user_id"].ToString());
                    Constant.setUserimage(description["image"].ToString());
                    Constant.setbalance(description["wallet_balance"].ToString());
                    Constant.setLoyalityPoints(description["loyalty_points"].ToString());
                    Constant.SavePersistent(Constant.SETTINGS_USER_LATTITUDE, description["latitude"].ToString());
                    Constant.SavePersistent(Constant.SETTINGS_USER_LONGITUDE, description["longitude"].ToString());
                    Constant.SavePersistent(Constant.FEATURED_DEALS, description["Deals_offer"].ToString());
                    Constant.SavePersistent(Constant.SETTINGS_USER_SUPPORT_NUMBER, description["support_number"].ToString());
                    Constant.SavePersistent(Constant.SETTINGS_IS_LOGIN, true);

                    try
                        {
                        var offer_likes=description["Deals_offer"]["offer_likes"];
                        string[] offer_likes_str=offer_likes.ToString().Split(',');
                        GiftDataHelper gdh=new GiftDataHelper();
                        foreach(string ids in offer_likes_str)
                            {
                            gdh.updateGiftForLikeByOfferId(Convert.ToInt16(ids.ToString()), 1);
                            }

                        }
                    catch(Exception exc)
                        {
                        }



                    NavigationService.Navigate(new Uri("/HomePage.xaml?", UriKind.RelativeOrAbsolute));

                    NavigationService.RemoveBackEntry();
                    }
                else
                    {
                    Constant.checkForErrorCode(result);
                    }

                }
            }


        }
    }