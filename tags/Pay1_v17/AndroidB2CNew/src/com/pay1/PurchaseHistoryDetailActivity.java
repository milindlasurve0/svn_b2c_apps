package com.pay1;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.larvalabs.svgandroid.SVGParser;

public class PurchaseHistoryDetailActivity extends Activity {
	private static final String SCREEN_LABEL = "Purchase History Details Screen";
	private EasyTracker easyTracker = null;
	private TextView textPaymentType, textPaymentDetails, textTransaction,
			textNumber, textAmount, textTAX, textOperator, textContactName,
			textTime, textView_Title, textView_TitleTransactionId,
			textView_TitlePay, textView_TitleStatus, textView_Status;
	private ImageView imageView_Back;
	Typeface Reguler, Medium;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.purchase_details_activity);
		try {
			easyTracker = EasyTracker
					.getInstance(PurchaseHistoryDetailActivity.this);
			easyTracker.set(Fields.SCREEN_NAME, SCREEN_LABEL);
			findViewById();

			String purchaseDetails = getIntent().getExtras().getString(
					"PURCHASE_DETAILS");

			Reguler = Typeface.createFromAsset(getAssets(),
					"EncodeSansNormal-400-Regular.ttf");
			Medium = Typeface.createFromAsset(getAssets(),
					"EncodeSansNarrow-500-Medium.ttf");
			textView_Title.setTypeface(Reguler);
			textAmount.setTypeface(Reguler);
			textTAX.setTypeface(Reguler);
			textOperator.setTypeface(Medium);
			textTime.setTypeface(Reguler);
			textView_TitleTransactionId.setTypeface(Medium);
			textView_TitlePay.setTypeface(Medium);
			textView_TitleStatus.setTypeface(Medium);

			RelativeLayout back_layout=(RelativeLayout)findViewById(R.id.back_layout);
			back_layout.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onBackPressed();
				}
			});
			
			try {
				JSONObject jsonObject = new JSONObject(purchaseDetails);
				textPaymentType.setText(jsonObject
						.getString("transaction_mode"));
				if (!jsonObject.getString("payment_mode").toString().trim()
						.equalsIgnoreCase("null"))
					textPaymentDetails.setText(jsonObject
							.getString("payment_mode"));
				else
					textPaymentDetails.setText("");
				textTransaction.setText(jsonObject.getString("transaction_id"));
				if (jsonObject.getString("trans_category").equalsIgnoreCase(
						"deal")) {
					textNumber.setText(jsonObject.getString("offer_name"));
					textOperator.setText(jsonObject.getString("deal_name"));
				} else if (jsonObject.getString("trans_category")
						.equalsIgnoreCase("refill")) {
					textNumber.setText(jsonObject.getString("number"));
					textOperator.setText("Wallet topup");
				} else {
					textNumber.setText(jsonObject.getString("number"));
					textOperator.setText(jsonObject.getString("operator_name"));
				}

				int total = 0, base = 0, tax = 0;
				try {
					total = Integer.parseInt(jsonObject
							.getString("transaction_amount"));
					base = Integer
							.parseInt(jsonObject.getString("base_amount"));
					tax = total - base;
				} catch (Exception e) {
				}
				textAmount.setText("Rs. " + total);
				textTAX.setText("(" + total + " = " + base + "+" + tax
						+ " service charge inclusive of all taxes.)");
				// textContactName.setText(Constants.getContactNameFromList(
				// PurchaseHistoryDetailActivity.this,
				// jsonObject.getString("number")));
				if (textContactName.getText().toString().trim()
						.equalsIgnoreCase(""))
					textContactName.setText("Unknown");
				textContactName.setVisibility(View.GONE);
				int statusId = 0;
				try {
					statusId = Integer.parseInt(jsonObject.getString("status"));
				} catch (Exception e) {
					statusId = 0;
				}
				// Bitmap bitmap=Constants.getBitmapById(statusId,context);
				switch (statusId) {
				case 0:
				case 1:
					textView_Status.setText("In process");
					textView_Status.setTextColor(getResources().getColor(
							R.color.Blue));
					break;
				case 2:
					textView_Status.setText("Success");
					textView_Status.setTextColor(getResources().getColor(
							R.color.Green));
					break;
				case 3:
				case 4:
					textView_Status.setText("Failure");
					textView_Status.setTextColor(getResources().getColor(
							R.color.Red));
					break;
				// case 3:
				// viewHolder.imageStatusSuccess
				// .setImageResource(R.drawable.nav_ok_b);
				}

				int service_id = 1;
				try {
					service_id = Integer.parseInt(jsonObject
							.getString("service_id"));
				} catch (Exception e) {
					service_id = 1;
				}
				if (service_id == 4)
					textTAX.setVisibility(View.VISIBLE);
				else
					textTAX.setVisibility(View.GONE);

				textTime.setText(jsonObject.getString("trans_datetime"));

				textPaymentType.setTypeface(Medium);
				textPaymentDetails.setTypeface(Medium);
				textTransaction.setTypeface(Medium);
				textNumber.setTypeface(Medium);
				textContactName.setTypeface(Medium);
				textView_Status.setTypeface(Medium);

			} catch (JSONException je) {
				// je.printStackTrace();
			}
			imageView_Back = (ImageView) findViewById(R.id.imageView_Back);
			imageView_Back.setImageDrawable(SVGParser.getSVGFromResource(
					getResources(), R.raw.ic_back_old).createPictureDrawable());
			if (Build.VERSION.SDK_INT >= 11)
				imageView_Back.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
			imageView_Back.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onBackPressed();
				}
			});
		} catch (Exception e) {
		}
		overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}

	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView().build());
		// EasyTracker.getInstance(this).activityStart(this);
		easyTracker.send(MapBuilder.createAppView().build());
	}

	@Override
	public void onStop() {
		super.onStop();
		// EasyTracker.getInstance(this).activityStop(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
	}

	private void findViewById() {
		// TODO Auto-generated method stub
		textPaymentType = (TextView) findViewById(R.id.textPaymentType);
		textPaymentDetails = (TextView) findViewById(R.id.textPaymentDetails);
		textTransaction = (TextView) findViewById(R.id.textTransaction);
		textNumber = (TextView) findViewById(R.id.textNumber);
		textAmount = (TextView) findViewById(R.id.textAmount);
		textTAX = (TextView) findViewById(R.id.textTAX);
		textOperator = (TextView) findViewById(R.id.textOperator);
		textContactName = (TextView) findViewById(R.id.textContactName);
		textTime = (TextView) findViewById(R.id.textTime);
		textView_Title = (TextView) findViewById(R.id.textView_Title);
		textView_TitleTransactionId = (TextView) findViewById(R.id.textView_TitleTransactionId);
		textView_TitlePay = (TextView) findViewById(R.id.textView_TitlePay);
		textView_TitleStatus = (TextView) findViewById(R.id.textView_TitleStatus);
		textView_Status = (TextView) findViewById(R.id.textStatus);
	}

}
