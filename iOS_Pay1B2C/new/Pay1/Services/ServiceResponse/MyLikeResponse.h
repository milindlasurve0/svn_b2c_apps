//
//  MyLikeResponse.h
//  Pay1
//
//  Created by Annapurna on 17/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyLikeResponse : NSObject
@property (nonatomic, strong) NSString *errorCode;
@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) NSString *message;
@property (nonatomic, assign) BOOL likeStatus;
@end
