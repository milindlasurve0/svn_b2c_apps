package com.pay1;

import java.util.ArrayList;
import java.util.List;
import java.util.WeakHashMap;

import android.annotation.SuppressLint;
import android.content.res.Configuration;
import android.database.SQLException;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.larvalabs.svgandroid.SVGParser;
import com.pay1.adapterhandler.FreeBiesPagerAdapter;
import com.pay1.constants.Constants;
import com.pay1.databasehandler.FreeBies;
import com.pay1.databasehandler.FreeBiesDataSource;
import com.pay1.databasehandler.FreeBiesLocation;
import com.pay1.databasehandler.FreeBiesLocationDataSource;
import com.pay1.utilities.Utility;

public class FreeBiesDetailActivity extends FragmentActivity {

	private static final String SCREEN_LABEL = "Freebies Detail Screen";
	// private final String TAG = "Freebies";
	private ViewPager pager;
	private FreeBiesPagerAdapter adapter;
	private ImageView imageView_Back;
	private TextView textView_Title;

	private ArrayList<WeakHashMap<String, String>> data;
	private ArrayList<WeakHashMap<String, String>> data_location;

	public static final String DEAL_ID = "id";
	public static final String DEAL_NAME = "dealname";
	public static final String URL = "img_url";
	public static final String CATAGORY = "category";
	public static final String CATAGORY_ID = "category_id";
	public static final String LATITUDE = "latitude";
	public static final String LONGITUDE = "longitude";
	public static final String ADDRESS = "address";
	public static final String AREA = "area";
	public static final String CITY = "city";
	public static final String STATE = "state";
	public static final String PIN = "pin";
	public static final String DISTANCE = "distance";
	public static final String LOCATION_COUNT = "location_count";
	public static final String OFFER_ID = "o_id";
	public static final String OFFER_NAME = "name";
	public static final String VALIDITY = "validity";
	public static final String SHORT_DESC = "offer_desc";
	public static final String MIN_AMOUNT = "min_amount";

	private EasyTracker easyTracker = null;

	int deal_id = 0;
	String trans_id = "";
	private FreeBiesDataSource freeBiesDataSource;
	private FreeBiesLocationDataSource freeBiesLocationDataSource;
	boolean check_flag = false;
	int page = 1;
	int selected_item = 0;

	@SuppressLint("NewApi")
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.freebies_detail_activity);
		try {
			easyTracker = EasyTracker.getInstance(FreeBiesDetailActivity.this);
			easyTracker.set(Fields.SCREEN_NAME, SCREEN_LABEL);
			freeBiesDataSource = new FreeBiesDataSource(
					FreeBiesDetailActivity.this);
			freeBiesLocationDataSource = new FreeBiesLocationDataSource(
					FreeBiesDetailActivity.this);

			Typeface Reguler = Typeface.createFromAsset(getAssets(),
					"Roboto-Regular.ttf");
			textView_Title = (TextView) findViewById(R.id.textView_Title);
			textView_Title.setTypeface(Reguler);

			imageView_Back = (ImageView) findViewById(R.id.imageView_Back);
			imageView_Back.setImageDrawable(SVGParser.getSVGFromResource(
					getResources(), R.raw.ic_back_old).createPictureDrawable());
			if (Build.VERSION.SDK_INT >= 11)
				imageView_Back.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
			imageView_Back.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onBackPressed();
				}
			});

			try {

				// textView_CALL.setText("If your transaction of Rs."
				// + getIntent().getStringExtra("AMOUNT")
				// + " on "
				// + getIntent().getStringExtra("MOBILE")
				// + " does not succeed, give a missed call on "
				// + Utility.getSupportNumber(FreeBiesDetailActivity.this,
				// Constants.SHAREDPREFERENCE_SUPPORT_NUMBER)
				// + ".");
				deal_id = getIntent().getIntExtra("ID", 0);
			} catch (Exception e) {
				deal_id = 0;
				trans_id = "";
			}

			pager = (ViewPager) findViewById(R.id.pager);

			easyTracker = EasyTracker.getInstance(FreeBiesDetailActivity.this);
			easyTracker.set(Fields.SCREEN_NAME, SCREEN_LABEL);

			data = new ArrayList<WeakHashMap<String, String>>();
			data_location = new ArrayList<WeakHashMap<String, String>>();

			adapter = new FreeBiesPagerAdapter(
					this.getSupportFragmentManager(), 0, 0, trans_id, false,
					data, data_location);
			pager.setAdapter(adapter);

			try {
				freeBiesDataSource.open();
				freeBiesLocationDataSource.open();

				FreeBies list = freeBiesDataSource.getFreeBie(deal_id);

				if (list != null) {

					data.clear();
					data_location.clear();

					WeakHashMap<String, String> map = new WeakHashMap<String, String>();

					// if (jsonArrayDesc.getJSONObject(i).has("allow"))
					// {
					// if (jsonArrayDesc.getJSONObject(i)
					// .getString("allow")
					// .equalsIgnoreCase("0"))
					// continue;
					// }
					// if (recharge_amount < Integer
					// .parseInt(jsonObjectOffer
					// .getString("min_amount"))) {
					// // textView_Title.setText("");
					// textView_NoData
					// .setText("You missed out on a free gift! Recharge of minimum Rs. "
					// + jsonObjectOffer
					// .getString("min_amount")
					// + " and win a free gift.");
					// continue;
					// }
					map.put(DEAL_ID, String.valueOf(list.getFreebieDealID()));
					map.put(DEAL_NAME, list.getFreebieDealName());
					map.put(CATAGORY, list.getFreebieCategory());
					map.put(CATAGORY_ID,
							String.valueOf(list.getFreebieCategoryID()));
					// map.put(LATITUDE, jsonArrayDesc.getJSONObject(i)
					// .getString("latitude"));
					// map.put(LONGITUDE, jsonArrayDesc.getJSONObject(i)
					// .getString("longitude"));

					map.put(URL, list.getFreebieURL());

					map.put(OFFER_ID, String.valueOf(list.getFreebieOfferID()));
					map.put(OFFER_NAME, list.getFreebieOfferName());
					map.put(SHORT_DESC, list.getFreebieShortDesc());
					map.put(MIN_AMOUNT,
							String.valueOf(list.getFreebieMinAmount()));
					map.put(VALIDITY, list.getFreebieValidity());
					map.put(LOCATION_COUNT,
							String.valueOf(list.getFreebieLocationCount()));

					List<FreeBiesLocation> locations = freeBiesLocationDataSource
							.getAllFreeBiesLocation(list.getFreebieOfferID());

					for (int j = 0; j < locations.size(); j++) {

						WeakHashMap<String, String> map1 = new WeakHashMap<String, String>();
						map1.put(DEAL_ID, String.valueOf(locations.get(j)
								.getLocationDealID()));
						double latitude = locations.get(j).getLocationLat();
						double longitude = locations.get(j).getLocationLng();
						map1.put(LATITUDE, String.valueOf(latitude));
						map1.put(LONGITUDE, String.valueOf(longitude));
						map1.put(ADDRESS, locations.get(j).getLocationAddress());
						map1.put(CITY, locations.get(j).getLocationCity());
						map1.put(STATE, locations.get(j).getLocationState());
						String lat = Utility.getCurrentLatitude(
								FreeBiesDetailActivity.this,
								Constants.SHAREDPREFERENCE_CURRENT_LATITUDE);
						String lng = Utility.getCurrentLongitude(
								FreeBiesDetailActivity.this,
								Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE);
						String dis = "0";
						if (lat != "" && lng != "")
							dis = String.valueOf(Constants.distanceFrom(
									Double.parseDouble(lat),
									Double.parseDouble(lng), latitude,
									longitude));
						map1.put(DISTANCE, dis);

						data_location.add(map1);

						data_location.add(map1);

						if (j == 0) {
							map.put(ADDRESS, locations.get(j)
									.getLocationAddress());
							map.put(DISTANCE, dis);
						}
					}
					data.add(map);
					//
					// if (i == 2)
					// break;
				}
				if (data.size() == 0) {
					pager.setVisibility(View.GONE);
				} else {

					adapter = new FreeBiesPagerAdapter(
							FreeBiesDetailActivity.this
									.getSupportFragmentManager(),
							0, 0, "", false, data, data_location);
					pager.setAdapter(adapter);
					pager.setVisibility(View.VISIBLE);
				}

			} catch (SQLException exception) {
				// TODO: handle exception
				// // // Log.e("Er ", exception.getMessage());
			} catch (Exception e) {
				// TODO: handle exception
				// // // Log.e("Er ", e.getMessage());
			} finally {
				freeBiesDataSource.close();
				freeBiesLocationDataSource.close();
			}

		} catch (Exception e) {
		}
		overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}

	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView().build());
		// EasyTracker.getInstance(this).activityStart(this);
		easyTracker.send(MapBuilder.createAppView().build());
	}

	@Override
	public void onStop() {
		super.onStop();
		// EasyTracker.getInstance(this).activityStop(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);

	}

}