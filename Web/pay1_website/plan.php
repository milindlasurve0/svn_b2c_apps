
<!DOCTYPE html>
<?php 
error_reporting(E_ALL);
session_start();
require_once 'common.php';
require_once 'index-js.php';
$getcircle = file_get_contents("circle.json");
$getcircle = json_decode($getcircle,TRUE);

if(isset($_GET['logout']) && !empty($_GET['logout'])){
	unset($_SESSION);
	
}
?>
 <html>
   <head>
    <title>Pay1</title>
   
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
 
	<link href="css/bootstrap.css" rel="stylesheet" media="screen">

	<link href="css/style.css" rel="stylesheet" media="screen">
    
    <link href="css/social-buttons.css" rel="stylesheet" media="screen">
    
    <link rel="stylesheet" href="css/font-awesome.min.css">

	  <!-- Contractible header -->
<!--	 <script src="js/contract_head.js"></script>
     <script src="../../assets/js/html5shiv.js"></script>
     <script src="../../assets/js/respond.min.js"></script>-->
     <script type="text/javascript" src="/js/jquery.min.2.1.1.js"></script>
     <script src="/js/bootstrap.min.js"></script>
	 <script src="/js/md5.js"></script>
	 <script type="text/javascript" src="js/common.js"></script>
<!--     <script src="js/gooeymenu.js"></script>-->
     <link rel="stylesheet" type="text/css" href="css/gooeymenu.css" />
             <!-- Contractible headerends -->  

  </head>
  
  <body>
 <nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#"><img src="img/pay1_logo.png" width="145" height="55" border="0" alt="" class="pay1_logo"></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right navbarspace">
	  	 <li><a href="#"  style="cursor:hand; cursor:pointer">Recharge/Pay Bills</a></li>
		 <li><a href="#" class="btn btn-info">Redeem Gift Coin</a></li>
        <li><a href="#basicModal1" role="button" class="" data-toggle="modal">Sign Up</a></li>
		<?php if(isset($_SESSION['flag']) && !empty($_SESSION['flag'])){ ?>
		<li><a href="plan.php?logout=logout" role="button" class="">Logout</a></li>
		<?php } else { ?>
		 <li><a href="#basicModal" role="button" class="" data-toggle="modal">Login</a></li>
		<?php } ?>
       
      </ul>
    </div>
  </div>
</nav>
            
         <div class="container-fluid recharge_menu">
         
            <div class="row">
                <div  class="switchcontent leftnavi">
                    <ul class="underlinemenu nav nav-tabs mobile">
                        <li><a href="#mobile" role="tab">PREPAID MOBILE</a></li>
                        <li><a href="#postpaid" role="tab">POSTPAID MOBILE</a></li>
                        <li><a href="#dth" role="tab">DTH </a></li>
                        <li><a href="#data" role="tab">DATACARD</a></li>
                        <li><a href="#" role="tab">QUICK PAY</a></li>
                        <li><a href="#" role="tab">ADD MONEY</a></li>

                    </ul>
                </div>
           </div>
           </div>
           
           <div class="container-fluid recharge_space">
           
           <div class="row">
           
           <div class="col-md-4 col-sm-4 col-xs-12 mob" id="mobile">
			   <form method="post" id="mob_form" action="">
                        <input type="hidden" id="mob_flag" name="mob_flag" value="1">
                        <input type="hidden" id="mob_circle" name="mob_circle" value="<?php echo $_POST["rec_circle"] ?>">
                        <input type="hidden" id="mob_max" name="mob_max" value="<?php echo $_POST["rec_max"]; ?>">
                        <input type="hidden" id="mob_min" name="mob_min" value="<?php echo $_POST["rec_min"]; ?>">
                         <input type="hidden" id="mob_id" name="mob_id" value="<?php echo $_POST["rec_id"]; ?>">
                          <input type="hidden" id="mob_product" name="mob_product" value="<?php echo $_POST["rec_product"]?>">

            <h4 class="recharge_header">Recharge Your Prepaid Mobile</h4>
            <div class="form-group">           
                <div class="input-group"><input type="text" size="30" class="form-control" placeholder="Enter your mobile no" name="mob_number" <?php if($_POST["rec_type"]=="1"){ ?>value="<?php echo $_POST["rec_number"]; ?>" <?php } ?>id="mob_number" maxlength="10" onkeypress="getoperatorvalue(this.value,'mob_provider','mob_amount');return isNumberKey(event)" onchange="getoperatorvalue(this.value,'mob_provider','mob_amount');return isNumberKey(event)"></div>           
            </div>
            <br/>
            <div class="form-group">
            <div class="input-group" style="width:280px;"> 
            <select class="form-control" name="mob_provider" id="mob_provider" onchange="getplanoperatorchange('mob')">
                <option value="">Select Service Provider</option>
                                                <?php foreach($arrayval["Mobile"] as $key => $val){?>
                                                <option value="<?php echo $val["opr_code"] ?>" <?php if($_POST["rec_provider"]==$val["opr_code"] && $_POST["rec_type"]=="1"){ ?> selected="selected"<?php }?>><?php echo $val["name"] ?></option>
                                                <?php } ?>
            </select></div>
            </div>
            <br/>
            <div class="form-group">            
            <div class="input-group" style="width:280px;"><input type="text" class="form-control" placeholder="Enter Amount" <?php if($_POST["rec_type"]=="1"){ ?>value="<?php echo $_POST["rec_amount"];?>"<?php } ?> name="mob_amount" id="mob_amount"  onkeypress="return isNumberKey(event)"></div>                 
            </div>
            <br/>
            <div class="form-group">           
            <div class="input-group">
				<button class="btn btn-success" style="width:280px;" onclick="orderclick('mob');" data-toggle="modal" type="button" data-target="">Continue to Payment
				</button> 
			</div>
             </div>
			   </form>
             
             </div>
			     
             <div class="col-md-8 col-sm-8 col-xs-12 mob" style="display:none;">
				 <div class="checkplans" id="showmobplan">
                                              <div class="sliderowbar">
						<ul class="nav nav-tabs" role="tablist" id ="mob_plan">
							
						</ul>
                                              </div>
                                            <table class="form-control" id="mobile_circle"  style="display:none;border: none;height:50px;">
                                                
                                                <tr width="673">
                                                    <th>Select Circle</th>
                                                    
                                                    <td><select class="form-control" name="mob_cir" id="mob_cir" onchange="getplanoperatorchange('mob')">
						<option value="">Select Circle</option>
                                                <?php foreach($getcircle as $key => $val){ if($val["code"]!="SC"){?>
                                                <option value="<?php echo $val["code"] ?>"><?php echo $val["name"] ?></option>
                                                <?php }} ?>
						
					</select></td>
                                                </tr>
                                            </table>

						<!-- Tab panes -->
						<div class="tab-content mT20"  id ="mob_plandetails">
    
						</div>
						 <div class="text-right">
                         <a href="javascript:void(0);" class="hidecheckplans"  title="Close">Close &nbsp;</a>
			             </div>	
					</div>
				
		
             
         </div>
               <div class="col-md-4 col-sm-4 col-xs-12" id="postpaid" style="display: none;">
				    <form method="post" id="post_form" action="login.php">  
                        <input type="hidden" name="post_flag" id="post_flag" value="4">
                         <input type="hidden" name="post_id" id="post_id" value="<?php echo $_POST["rec_id"]?>">
                         <input type="hidden" name="post_min" id="post_min" value="<?php echo $_POST["rec_min"]?>">
                         <input type="hidden" name="post_max" id="post_max" value="<?php echo $_POST["rec_max"]?>">
                         <input type="hidden" name="post_charge" id="post_charge" value="<?php echo $_POST["rec_charge"]?>">
                         <input type="hidden" name="post_servicecharge" id="post_servicecharge" value="">

            <h4 class="recharge_header">Recharge Your Postpaid Mobile</h4>
            <div class="form-group">           
                <div class="input-group"><input type="text" size="30" class="form-control" placeholder="Enter  PostPaid Number" <?php if($_POST["rec_type"]=="4"){ ?> value="<?php echo $_POST["rec_number"];?>" <?php } ?> maxlength="10"  id="post_number" name="post_number" onkeypress="getdataoperatorvalue(this.value,'post_provider','post_amount');return isNumberKey(event)"></div>           
            </div>
            <br/>
            <div class="form-group">
            <div class="input-group" style="width:280px;"> 
            <select class="form-control" name="post_provider" id="post_provider" onchange="getplanoperatorchange('post');">
               	<option value="">Select Service Provider</option>
                                                <?php foreach($arrayval["postpaid"] as $key => $val){?>
                                                <option value="<?php echo $val["opr_code"] ?>" <?php if($_POST["rec_provider"]==$val["opr_code"] && $_POST["rec_type"]=="4"){ ?> selected="selected"<?php }?>><?php echo $val["name"] ?></option>
                                                <?php } ?>
            </select></div>
            </div>
            <br/>
            <div class="form-group">            
            <div class="input-group" style="width:280px;"><input type="text"   value="" class="form-control" name="post_amount" id="post_amount" onkeypress="return isNumberKey(event);" placeholder="Enter Amount"></div>                 
            </div>
            <br/>
            <div class="form-group">           
            <div class="input-group"><button class="btn btn-success"  onclick="orderclick('post');" type="button" data-toggle="modal" data-target=".orderconfirmation" class="btn btn-primary btn-lg btn-block" style="width:280px;">Continue to Payment</button>  </div>
             </div>
					</form>
             
             </div>
               <div class="col-md-4 col-sm-4 col-xs-12" id="dth" style="display: none;">
				   <form method="post" id="dth_form" action="login.php">
               <input type="hidden" name="dth_flag" id="dth_flag" value="2">
               <input type="hidden" name="dth_max" id="dth_max" value="<?php echo $_POST["rec_max"]; ?>">
                <input type="hidden" name="dth_min" id="dth_min" value="<?php echo $_POST["rec_min"]; ?>">
                <input type="hidden" name="dth_id" id="dth_id" value="<?php echo $_POST["rec_id"]; ?>">
                 <input type="hidden" name="dth_product" id="dth_product" value="<?php echo $_POST["rec_product"]; ?>">
                  <input type="hidden" name="dth_circle" id="dth_circle" value="<?php echo $_POST["rec_circle"]; ?>">

            <h4 class="recharge_header">Recharge Your DTH</h4>
			 <div class="form-group">
            <div class="input-group" style="width:280px;"> 
            <select class="form-control" name="dth_provider" id="dth_provider" onchange="getplanoperatorchange('dth');">
               <option value="">Select Operator</option>
                                                <?php foreach($arrayval["dth"] as $key => $val){?>
                                                <option value="<?php echo $val["opr_code"] ?>" <?php if($_POST["rec_provider"]==$val["opr_code"] && $_POST["rec_type"]=="2"){ ?> selected="selected"<?php }?>><?php echo $val["name"] ?></option>
                                                <?php } ?>
            </select></div>
            </div>
			<br/>
            <div class="form-group">           
                <div class="input-group"><input type="text" size="30" class="form-control" value="" onkeypress="return isNumberKey(event)" name="dth_number" id="dth_number"></div>           
            </div>
            
           
            <br/>
            <div class="form-group">            
            <div class="input-group" style="width:280px;"><input type="text" class="form-control" placeholder="Enter Amount" name="dth_amount" placeholder="Enter Amount" id="dth_amount"  <?php if($_POST["rec_type"]=="2"){ ?>value="<?php echo $_POST["rec_amount"] ?>" <?php } ?>onkeypress="return isNumberKey(event)" class="form-control"></div>                 
            </div>
            <br/>
            <div class="form-group">           
            <div class="input-group"><button class="btn btn-success" type="button" onclick="orderclick('dth');" data-target=".orderconfirmation"  style="width:280px;">Continue to Payment</button>  </div>
             </div>
				   </form>
             
             </div>
			     <div class="col-md-8 col-sm-8 col-xs-12 dth" style="display:none;">
                  <div class="checkplans" id="showdthplan">
                                            <div class="sliderowbar">
						<ul class="nav nav-tabs" id="dth_plan" role="tablist">
						 
						</ul>
                                            </div>
						<div class="tab-content mT20" id="dth_plandetails">
						 
						</div>
						  
						<div class="text-right">
                         <a href="javascript:void(0);" class="hidecheckplans"  title="Close">Close &nbsp;</a>
						</div>						
					</div>
		
             
         </div>
			  
			   
               <div class="col-md-4 col-sm-4 col-xs-12" id="data" style="display: none;">
				   <form method="post" id="data_form" action="login.php">
                    <input type="hidden" name="data_id" id="data_id" value="<?php echo $_POST["rec_id"]; ?>">
                    <input type="hidden" name="data_max" id="data_max" value="<?php echo $_POST["rec_max"]; ?>">
                     <input type="hidden" name="data_min" id="data_min" value="<?php echo $_POST["rec_min"]; ?>">
                     <input type="hidden" id="data_flag" name="data_flag" value="3">
                       <input type="hidden" id="data_product" name="data_product" value="<?php echo $_POST["rec_product"]; ?>">
                      <input type="hidden" id="data_circle" name="data_circle" <?php if($_POST["rec_type"]=="3"){?>value="<?php echo $_POST["rec_circle"]; ?>" <?php }?>>

            <h4 class="recharge_header">Recharge Your Data card</h4>
            <div class="form-group">           
                <div class="input-group"><input type="text" size="30" value="" name="data_number" id="data_number" onkeypress="getdataoperatorvalue(this.value,'data_provider','data_amount')" class="form-control" placeholder="Enter your mobile no"></div>           
            </div>
            <br/>
            <div class="form-group">
            <div class="input-group" style="width:280px;"> 
            <select class="form-control" id="data_provider" name="data_provider" onchange="getplanoperatorchange('data')">
				<option value="">Select Service Provider</option>
				<?php foreach ($arrayval["data"] as $key => $val) { ?>
					<option value="<?php echo $val["opr_code"] ?>"<?php if ($_POST["rec_provider"] == $val["opr_code"] && $_POST["rec_type"] == "3") { ?> selected="selected"<?php } ?>><?php echo $val["name"] ?></option>
				<?php } ?>
            </select></div>
            </div>
            <br/>
            <div class="form-group">            
            <div class="input-group" style="width:280px;"><input type="text" name="data_amount"  value=""  id="data_amount" class="form-control"  onkeypress="return isNumberKey(event)"  placeholder="Enter Amount"></div>                 
            </div>
            <br/>
            <div class="form-group">           
            <div class="input-group"><button class="btn btn-success" type="button" data-toggle="modal" onclick="orderclick('data');" data-target=".orderconfirmation"  style="width:280px;">Continue to Payment</button>  </div>
             </div>
				   </form>
             
             </div>
			   <div class="col-md-8 col-sm-8 col-xs-12 data" style="display:none;">
				   <div class="checkplans" id="showdataplan">
					   <div class="sliderowbar">
						   <ul class="nav nav-tabs"  id ="data_plan" role="tablist">

						   </ul>
					   </div>

					   <table class="form-control" id="dat_circle" style="display:none;border: none; height:50px;">

						   <tr>
							   <th style="border:none;">Select Circle</th>
							   <td style="border:none;"><select class="form-control" name="data_cir" id="data_cir" onchange="getplanoperatorchange('data')">
									   <option value="">Select Circle</option>
									   <?php foreach ($getcircle as $key => $val) {
										   if ($val["code"] != "SC") { ?>
											   <option value="<?php echo $val["code"] ?>"><?php echo $val["name"] ?></option>
	<?php }
} ?>

								   </select></th>
						   </tr>
					   </table>

					  
					   <div class="tab-content mT20" id="data_plandetails">

					   </div>
					   <!-- close -->
					   <div class="text-right">
						   <a href="javascript:void(0)" class="hidecheckplans" title="Close">Close &nbsp;</a>
					   </div>						
				   </div>
			   </div>
           
         
         </div>
         
         
         </div>
	    <div class="modal fade" id="basicModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    		<div class="modal-dialog">
      		  <div class="modal-content">
          		  <div class="modal-header">
 					 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
           				 <h4 class="modal-title" id="myModalLabel" align="center">Awesome! Your account is created with Pay1!</h4>
           			 </div>
            <div class="modal-body">
            <h4 class="modal-title login_font_title" id="myModalLabel">LOGIN WITH PAY1</h4>
            <div class="row">
            
            <div class="col-sm-6 col-md-6 col-xs-12 bottomfb">
    		<div class="rightborder">
            <form method="post" id="login_modal" action = ''>       
         	 <div class="login_font">Login using mobile number</div>
         	 <input type="text" class="form-control input_margin"  id ="mob-number" required="" name="mob-number" type="text" autocomplete="off" maxlength="10">
         	 <input type="password" class="form-control input_margin" name="password" id="password"  placeholder="Password" required=""/>      
       
         	 <button class="btn btn-lg btn-primary btn-block"  type="submit">Login</button>   
             </form>
             </div>
 			 </div>
             
           <div class="col-sm-6 col-md-6 col-xs-12 topspbtn">
            <div class="login_fb">
            <a class="btn btn-block btn-social btn-facebook">
            <i class="fa fa-facebook"></i> Sign in with facebook
              </a>
              </div>
			   <br/>
              
              <div class="login_fb"><a class="btn btn-block btn-social btn-google-plus">
                <i class="fa fa-facebook"></i> Sign in with Google+
              </a>
              </div>
			    <br/>
				<button class="btn btn-lg btn-primary btn-block topsignup" type="button" onclick="signup();">Sign up</button>   
              </div>
 			 
             </div>
				<!--row ends-->
            </div> 
    </div>
  </div>
</div>
	  
	  <div class="modal fade" id="basicModal1" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    		<div class="modal-dialog">
      		  <div class="modal-content">
          		  <div class="modal-header">
 					 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                     <h4 class="modal-title modal-title signup_font_title" id="myModalLabel" align="center">SIGNUP WITH PAY1</h4>
           			 </div>
            <div class="modal-body">
            <h4 class="" id="myModalLabel"></h4>
            <div class="row">
            
            <div class="col-sm-6 col-md-6 col-xs-12 bottomfb">
    		<div class="rightborder">
            <form> 
				<span id ="sign_msg" style="color:red;"></span>
         	 <div class="login_font">Signup using mobile number</div>
			  
                 <div class="input-group mobno">
                 <div class="input-group-addon">+91</div>
                  <input class="form-control" onkeypress="return isNumberKey(event)" maxlength="10" type="text" id="mobnumber" name="mobnumber">
                </div>
			 
			  <div class="input-group otp" style="display: none;">
                  <input class="form-control" onkeypress="return isNumberKey(event)" maxlength="10" type="text" id="otp" name="otp">
            </div>
			 <br/>
         	 <button class="btn btn-lg btn-primary btn-block" type="button" id="register_user"  onclick="register();">Create an account</button>
			 <button class="btn btn-lg btn-primary btn-block" id="check_otp" style="display: none;" type="button" onclick="checkotp();">Submit</button>
             <p class="agree_txt">By Signing Up, you agree to our <span class="agree_bluetxt">T&C </span> and that
             you have read our <span class="agree_bluetxt">Privacy policy</span></p>
             </form>
             </div>
 			 </div>
             
           <div class="col-sm-6 col-md-6 col-xs-12 topspbtn">
           
            <div class="login_fb">
            <a class="btn btn-block btn-social btn-facebook">
            <i class="fa fa-facebook"></i> Sign in with facebook
              </a>
              </div>
			   <br/>
              
              <div class="login_fb"><a class="btn btn-block btn-social btn-google-plus">
                <i class="fa fa-facebook"></i> Sign in with Google+
              </a>
              </div>
 			
              </div>
 			 
             </div>
				<!--row ends-->
            </div> 
    </div>
  </div>
</div>
	  <div class="modal fade" id="orderconfirmation" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    		<div class="modal-dialog">
				<form method="post" action="" id="payment_form">
      		  <div class="modal-content">
          		  <div class="modal-header order_bottomgreenline">
 					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <input type="hidden" name="recharge_amount" id="recharge_amount" value="">
                    <input type="hidden" name="recharge_operator" id="recharge_operator" value="">
                    <input type="hidden" name="recharge_flag" id="recharge_flag" value="">
                    <input type ="hidden" name="recharge_number" id="recharge_number" value="">
                    <input type ="hidden" name="payment_option" value="" id="payment_option">
                    <input type="hidden" name="service_charge" id ="service_charge" value="">
                    <input type="hidden" name="service_charge_percent" id ="service_charge_percent" value="">
                    <input type="hidden" name="stv" id ="stv" value="">
                    <input type="hidden" name="total_amount" id ="total_amount" value="">
           		    <h4 class="modal-title" id="myModalLabel">Order Confirmation</h4>
           			</div>
            <div class="modal-body">
            <h4 class="modal-title login_font_title" id="myModalLabel"></h4>
            <div class="row">
            
            <div class="col-sm-6 col-md-6 col-xs-12 ">      		
                   <div class="row order_left">
                        
                 <div class="order_font">Mobile number</div>
                <h4 class="h4bold" id ="number"></h4>         
              <div class="service_provider">Service Provider</div>
                <h4 class="h4bold" id="provider_name"></h4>    
                </div>     	          
             </div>  
 		             
           <div class="col-sm-6 col-md-6 col-xs-12">  
                <div class="order_font">Amount</div>
                <h4 class="h4bold" id ="trans_amount"></h4>         
				<div class="blue_ordertxt" id="service_msg" style="display: none;"></div>  
              </div>
             </div>
        
				<!--row ends-->
         
                  <div class="modal-header order_bottomgreenline">
             <h4>Pay now via</h4>         
             </div>
             
               <div class="row pay_via">
               
             	<div class="col-md-6 col-sm-6 col-xs-12">                             
                <div class="row">                
                <div class="col-md-4 col-sm-4 col-xs-12">
                <img src="img/wallet.svg" height="68" width="68">
                </div>
                
                <div class="col-md-6 col-sm-6 col-xs-12">         
                <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">Balance</div>
                <div class="col-md-12 col-sm-12 col-xs-12"><h4 class="h4bold" id="balance"><?php echo ($_SESSION["wallet_balance"]>0)?$_SESSION["wallet_balance"]:0 ?></h4></div>                                          
                </div>
             
                </div>
                 
                 
                 <div class="col-md-10 col-sm-10 col-xs-12 btn_via">
					 <input type="button" class="btn btn-lg btn-success btn-block"  value="Pay with wallet" id="wallet"  onclick="payment('recharge_amount','recharge_operator','recharge_flag','recharge_number',this.value)"></button>
                </div>
                </div>
                </div>
                
                <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-12"><img src="img/creditcard.svg" height="68" width="68"></div>
                <div class="col-md-6 col-sm-6 col-xs-12">Pay with your Debit or Credit Card</div>              
                </div>
                 <div class="col-md-10 col-sm-10 col-xs-12 btn_via">
					 <input type="button" class="btn btn-lg btn-success btn-block" value="Payment Gateway" onclick="payment('recharge_amount','recharge_operator','recharge_flag','recharge_number',this.value)"></button>
                 
                </div>
                </div>
             
             </div>
    </div>
  </div>
				</form>
</div>
	  </div>

	 <div class="modal fade in ordersuccess1" id="ordersuccess1" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" data-dismiss="modal" type="button" title="Close">
					<span aria-hidden="true" onclick="closepopup();">×</span>
                                        <span class="sr-only" >Close</span>
				</button>
				<h4 id="mySmallModalLabel" class="modal-title">
					<span class="" id="order_response"></span>
				</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-12 mT20">
						<p class="ordersucHead" id ="order_msg"></p>
					</div>
					<div class="col-sm-5">
						<small id="order_type"></small>
						<p id ="order_number"></p>
					</div>
					<div class="col-sm-3">
						<small >Amount</small>
						<p id="order_amount"></p>
					</div>
                                    
<!--					<div class="col-sm-4">
						<small id="order_desscription">500MB 3G Plan valid for 30 days.</small>
					</div>-->
				</div>
				<div class="row">
					<div class="col-sm-5">
						<small>Service Provider</small>
						<p id="order_serviceprovider">Reliance</p>
					</div>
                                    <div class="col-sm-5">
						<small>Transaction Id</small>
						<p id="trans_id"></p>
					</div>
					<div class="col-sm-6">
						<small>Balance</small>
						<p id="closing_balanace"></p>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12 popupdivision">
						<small>&nbsp;</small>
					</div>
					<div class="col-sm-12 mT15" style="display:none;" id="success_msg">
						<p>If your transaction does not Succeed</p>
						<button class="callaskBTN"><span class="popupicons-call"></span> Call Us</button>
						<button class="callaskBTN"><span class="popupicons-ask"></span> Ask Us</button>
						<br><br>
					</div>
                                       <div class="col-sm-12 mT15" style="display:none;" id="error_msg">
						<p>Kindly contact us for assistance</p>
						<button class="callaskBTN"><span class="popupicons-call"></span> Call Us</button>
						<button class="callaskBTN"><span class="popupicons-ask"></span> Ask Us</button>
						<br><br>
					</div>
                                    <div class="col-sm-12 mT15" style="display:none;"  id="order_deal">
						
						<button class="common-btn-submit"  onclick="getfreebie('ordersuccess1');"> Click to get free deals</button>
						
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
     </div>

<div class="modal fade in ordersuccess2" id="ordersuccess2" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" data-dismiss="modal" type="button" title="Close">
					<span aria-hidden="true" onclick="closepopup();">×</span>
                                        <span class="sr-only" >Close</span>
				</button>
				<h4 id="mySmallModalLabel" class="modal-title">
					<span class="" id="wallet_response"></span>
				</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-12 mT20">
						<p class="ordersucHead" id ="wallet_msg"></p>
					</div>
					
					<div class="col-sm-3">
						<small >Amount</small>
						<p id="wallet_amount"></p>
					</div>
                                    
<!--					<div class="col-sm-4">
						<small id="order_desscription">500MB 3G Plan valid for 30 days.</small>
					</div>-->
                                         <div class="col-sm-12 mT15" style="display:none;"  id="wallet_deal">
						
						<button  class="common-btn-submit" onclick="getfreebie('ordersuccess2');">Click to get free deals</button>
						
						
					</div>
				
				</div>
                            
				</div>
			</div>
		</div>
	</div>

<div class="modal fade in ordersuccess3" id="ordersuccess3" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" data-dismiss="modal" type="button" title="Close">
					<span aria-hidden="true" onclick="closepopup();">×</span>
                                        <span class="sr-only" >Close</span>
				</button>
				
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-12 mT20">
						<p class="ordersucHead" id ="">Deals claimed Successfully!!!</p>
					</div>
					
					
				</div>
                            
				
				</div>
			</div>
		</div>
	</div>
    <div class="modal fade in ordersuccess4" id="ordersuccess4" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" data-dismiss="modal" type="button" title="Close">
					<span aria-hidden="true" onclick="closepopup();">×</span>
                                        <span class="sr-only" >Close</span>
				</button>
				
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-12 mT20">
						<p class="ordersucHead" id ="freebievalidate"></p>
					</div>
					
					
				</div>
                            
				
				</div>
			</div>
		</div>
	</div>
         
         
<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
	 <li data-target="#carousel-example-generic" data-slide-to="2"></li>
	<li data-target="#carousel-example-generic" data-slide-to="3"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" style="max-width: 1366px; margin: 0 auto">
    
	<div class="item active">
      <img src="img/homepage1.png" width="1366" height="516" border="0" alt="">
      <div class="carousel-caption">
     
      </div>
		</div>
    
	<div class="item">
      <img src="img/homepage2.png" width="1366" height="516" border="0" alt="">
      <div class="carousel-caption">    
      </div>
    </div>
		 
	 <div class="item">
      <img src="img/homepage3.png" width="1366" height="516" border="0" alt="">
      <div class="carousel-caption">       
      </div>
    </div>
	

	 <div class="item">
      <img src="img/homepage4.png" width="1366" height="516" border="0" alt="">
      <div class="carousel-caption">      
      </div>
    </div>
  </div>

  <!-- Controls 
  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
     <span class="glyphicon glyphicon-chevron-left"></span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
     <span class="glyphicon glyphicon-chevron-right"></span>
  </a>-->
</div>

<div class="container-fluid">
<div class="row" align="center"> 
	<form class="form-inline">
  <div class="form-group">
    <label class="sr-only" for="exampleInputAmount">Enter your mobile no</label>
        <div class="input-group">
              <div class="input-group-addon">+91</div>
              <input type="text" class="form-control" id="exampleInputAmount" placeholder="Enter your mobile no">
            </div>
      </div>   
      <div class="input-group">
          <button type="submit" class="btn btn-primary">Start</button>
      </div>
</form> 
</div> 

</div>
<div class="container-fluid gift_heading">Pocket a Gift in 3 simple steps</div>
<div class="container-fluid spacing_top">

<div class="row">

<div class="col-xs-12 col-md-4 col-sm-4">
<div class="thumbnail">
<img src="img/ic1.png">
<p align="center" class="thumbnail_txt">1. Use Cash or Card<br/> To Recharge</p>
</div>
</div>

<div class="col-xs-12 col-md-4 col-sm-4">
<div class="thumbnail">
<img src="img/ic2.png">
<p align="center" class="thumbnail_txt">2. Collect Gift Coins<br/> on every Recharge</p>
</div>
</div>

<div class="col-xs-12 col-md-4 col-sm-4">
<div class="thumbnail">
<img src="img/ic3.png"><p align="center" class="thumbnail_txt">3. Pocket amazing<br/> gifts near you</p>
</div>
</div>

</div>
</div>

<div class="row">
<div align="center">
<div class="btn-group">
  <button type="button" class="btn btn-get">Let�s get started</button>
</div>
 </div>
</div>
<br/>
<div class="row topspace50">
<div class="mapbg">
<div class="row">
<div class="col-md-12 col-sm-12 col-xs-12 discover_txt"><h1>Discover Your Neighbourhood</h1></div>
</div>

<div class="row map_width">
<div class="col-md-4 col-sm-4 col-xs-12">
<div class="map_thumnail"><img src="img/map_thum1.jpg">
        <div class="mt_text">Tasty, yummy burger</div>
        <div class="mt_texthd">McDonalds</div>
            <div class="mp_gift_ic">
                <div class="mp_gift_im"><img src="img/mygifts.png"></div>
                <div class="mt_num">50</div>
            </div>
</div>
</div>
<div class="col-md-4 col-sm-4 col-xs-12">
<div class="map_thumnail"><img src="img/map_thum2.png">
        <div class="mt_text">Tasty, yummy burger</div>
        <div class="mt_texthd">McDonalds</div>
            <div class="mp_gift_ic">
                <div class="mp_gift_im"><img src="img/mygifts.png"></div>
                <div class="mt_num">50</div>
            </div>
</div>

</div>
<div class="col-md-4 col-sm-4 col-xs-12">
<div class="map_thumnail"><img src="img/map_thum3.png">
        <div class="mt_text">Tasty, yummy burger</div>
        <div class="mt_texthd">McDonalds</div>
            <div class="mp_gift_ic">
                <div class="mp_gift_im"><img src="img/mygifts.png"></div>
                <div class="mt_num">50</div>
            </div>
</div>
</div>
</div>

</div>  
	</div>

<div class="container-fluid topspace50">
<h1 align="center">Gift Store</h1>
<h2 align="center">Categories</h2>
<div class="row"> 
<div class="col-lg-9 col-md-9 col-sm-9"></div>
<div class="col-lg-3 col-md-3 col-sm-3"><a href="#" class="btn btn-large disabled">SEE ALL ></a></div>
</div>
<div class="container-fluid"><br/>
	<div class="row">
		<div class="col-xs-6 col-md-4 col-sm-4"><div class="thumbnail"><img src="img/food.jpg"></div></div>
        <div class="col-xs-6 col-md-4 col-sm-4"><div class="thumbnail"><img src="img/drinks.jpg"></div></div>
        <div class="col-xs-6 col-md-4 col-sm-4"><div class="thumbnail"><img src="img/beauty.jpg"></div></div>
    </div>
</div>
</div>
<hr>

<div class="container-fluid"> 
<h2 align="center">Popular</h2>
<div class="col-lg-9 col-md-9 col-sm-9"></div>
<div class="col-lg-3 col-md-3 col-sm-3"><a href="#" class="btn btn-large disabled">SEE ALL ></a></div>
</div>
<div class="container-fluid giftstore_width"><br/>
	<div class="row">
		<div class="col-xs-6 col-md-2 col-sm-2"><div class="thumbnail"><img src="img/icecream.jpg"></div>
        <div class="popular_thumtxt">ICE CREAM</br>Kwality<br/>50</div></div>
        <div class="col-xs-6 col-md-2 col-sm-2"><div class="thumbnail"><img src="img/yoga_thum.jpg"></div><div class="popular_thumtxt">YOGA CLASS</br>Yoga 101<br/>200</div></div>
        <div class="col-xs-6 col-md-2 col-sm-2"><div class="thumbnail"><img src="img/levis.jpg"></div><div class="popular_thumtxt">LEVIS JEANS</br>Levis<br/>50</div></div>
        <div class="col-xs-6 col-md-2 col-sm-2"><div class="thumbnail"><img src="img/facepack.jpg"></div><div class="popular_thumtxt">FACEPACK</br>Herballife<br/>50</div></div>
        <div class="col-xs-6 col-md-2 col-sm-2"><div class="thumbnail"><img src="img/facepack.jpg"></div><div class="popular_thumtxt">FACEPACK</br>Herballife<br/>50</div></div>
        <div class="col-xs-6 col-md-2 col-sm-2"><div class="thumbnail"><img src="img/keyboard.jpg"></div><div class="popular_thumtxt">KEYBOARD</br>Kwality<br/>50</div></div>
    </div>
</div>
</div>

<div class="container-fluid"> <h2 align="center">Offer of the day</h2>
<div class="col-lg-9 col-md-9 col-sm-9"></div>
<div class="col-lg-3 col-md-3 col-sm-3"><a href="#" class="btn btn-large disabled">SEE ALL ></a></div>
</div>
<div class="container-fluid giftstore_width"><br/>
	<div class="row">
		<div class="col-xs-6 col-md-2 col-sm-2"><div class="thumbnail"><img src="img/icecream.jpg"></div>
        <div class="popular_thumtxt">ICE CREAM</br>Kwality<br/>50</div></div>
        <div class="col-xs-6 col-md-2 col-sm-2"><div class="thumbnail"><img src="img/yoga_thum.jpg"></div><div class="popular_thumtxt">YOGA CLASS</br>Yoga 101<br/>200</div></div>
        <div class="col-xs-6 col-md-2 col-sm-2"><div class="thumbnail"><img src="img/levis.jpg"></div><div class="popular_thumtxt">LEVIS JEANS</br>Levis<br/>50</div></div>
        <div class="col-xs-6 col-md-2 col-sm-2"><div class="thumbnail"><img src="img/facepack.jpg"></div><div class="popular_thumtxt">FACEPACK</br>Herballife<br/>50</div></div>
        <div class="col-xs-6 col-md-2 col-sm-2"><div class="thumbnail"><img src="img/facepack.jpg"></div><div class="popular_thumtxt">FACEPACK</br>Herballife<br/>50</div></div>
        <div class="col-xs-6 col-md-2 col-sm-2"><div class="thumbnail"><img src="img/keyboard.jpg"></div><div class="popular_thumtxt">KEYBOARD</br>Kwality<br/>50</div></div>
    </div>
</div>
</div>


<div class="row"> <h2 align="center">Featured</h2>
<div class="col-lg-9 col-md-9 col-sm-9"></div>
<div class="col-lg-3 col-md-3 col-sm-3"><a href="#" class="btn btn-large disabled">SEE ALL ></a></div>
</div>
<div class="container-fluid"><br/>
	<div class="row">
		<div class="col-xs-6 col-md-4 col-sm-4"><div class="thumbnail"><img src="img/dynamic.jpg"></div></div>
        <div class="col-xs-6 col-md-4 col-sm-4"><div class="thumbnail"><img src="img/food.jpg"></div></div>
        <div class="col-xs-6 col-md-4 col-sm-4"><div class="thumbnail"><img src="img/habibs.jpg"></div></div>
    </div>
</div>

<div class="container-fluid1"><br/>
<div class="row">
    <div class="col-lg-4 col-md-4 col-sm-4">
        <ul class="list-unstyled">
        <h4>MOBILE RECHARGE</h4>
        <hr class="hr_zero">
        <li>Airtel (Plans)</li>
        <li>Aircel (Plans)</li>
        <li>Vodafone (Plans)</li>
        <li>BSNL (Plans)</li>
        <li>Tata Docomo GSM (Plans)</li>
        <li>Idea (Plans)</li>
        <li>Indicom Walky (Plans)</li>
        <li>MTNL Delhi (Plans)</li>
        <li>Reliance CDMA (Plans)</li>
        <li>Reliance GSM (Plans)</li>
        <li>Tata Indicom (Plans)</li>
        <li>Uninor (Plans)</li>
        <li>MTS (Plans)</li>
        <li>Videocon(Plans)</li>
        <li>Virgin CDMA (Plans)</li>
        <li>VirgnGSM (Plans)</li>
        <li>Tata Docomo CDMA (Plans)</li>
        </ul>
    </div>
        <div class="col-lg-4 col-md-4 col-sm-4">
        <ul class="list-unstyled">
        <h4>DATA CARD RECHARGE</h4>
       <hr class="hr_zero">
        <li>Tata Photon</li>
        <li>MTS MBlaze</li>
        <li>MTS MBrowse</li>
        <li>Reliance NetConnect</li>
        <li>Airtel</li>
        <li>BSNL</li>
        <li>Aircel</li>
        <li>MTNL Delhi</li>
        <li>Vodafone</li>
        <li>Idea</li>
        <li>Reliance-GSM</li>
        <li>MTNL Mumbai</li>
        <li>T24</li>
        <li>Tata Docomo</li>
        </ul>
        </div>
        
<div class="col-lg-4 col-md-4 col-sm-4">
<ul class="list-unstyled">
<h4>POSTPAID</h4>
<hr class="hr_zero">
<li>Airtel Bill Payment</li>
<li>BSNL Bill Payment</li>
<li>Tata Docomo GSM Bill Payment</li>
<li>Tata Docomo CDMA Bill Payment</li>
<li>Idea Bill Payment</li>
<li>Vodafone Bill Payment</li>
<li>Reliance GSM Bill Payment</li>
<li>Reliance CDMA Bill Payment</li>
</ul></div>

<div class="col-lg-4 col-md-4 col-sm-4">
<ul class="list-unstyled">
<h4>Pay1</h4>
<hr class="hr_zero">
<li>About Us</li>
<li>Careers</li>
<li>FAQ</li>
<li>Support</li>
<li>Contact Us</li>
<li>Privacy Policy</li>
<li>Sitemap</li>
<li>T & C</li>
<li>Blog</li>
<li>
Security Policy</li>
</ul></div>

<div class="col-lg-4 col-md-4 col-sm-4"><ul class="list-unstyled">
<h4>SUPPORT</h4>
<hr class="hr_zero">
<li>For help, send email to listen@pay1.in</li>
</ul></div>

<div class="col-lg-4 col-md-4 col-sm-4"><ul class="list-unstyled">
<h4>DTH (TV) RECHARGE</h4>
<hr class="hr_zero">
<li>Airtel Digital TV</li>
<li>Reliance Digital TV</li>
<li>Dish TV</li>
<li>Tata Sky</li>
<li>Sun Direct</li>
<li>Videocon D2H</li>
</ul></div>

<div class="col-lg-4 col-md-4 col-sm-4">
<ul class="list-unstyled">
<h4>APP DOWNLOAD</h4>
<hr class="hr_zero">
<li>Airtel Digital TV</li>
<li>Reliance Digital TV</li>
<li>Dish TV</li>
<li>Tata Sky</li>
<li>Sun Direct</li>
<li>Videocon D2H</li>
</ul>
</div>

<div class="col-lg-4 col-md-4 col-sm-4"><ul class="list-unstyled">
<h4>SECURED</h4>
<hr class="hr_zero">
<div class="col-xs-6 col-md-4 col-sm-4"><img src="img/visa.jpg"></div>
<div class="col-xs-6 col-md-4 col-sm-4"><img src="img/verified.jpg"></div>
<div class="col-xs-6 col-md-4 col-sm-4"><img src="img/mastercard.jpg"></div>
<div class="col-xs-6 col-md-4 col-sm-4"><img src="img/pci.jpg"></div>
<div class="col-xs-6 col-md-4 col-sm-4"><img src="img/american.jpg"></div>
<div class="col-xs-6 col-md-4 col-sm-4"><img src="img/payu.jpg"></div>
</ul></div>
<div class="col-lg-4 col-md-4 col-sm-4"><ul class="list-unstyled">
<h4>GIFTS</h4>
<hr class="hr_zero">
<li>My Gifts</li>
<li>Gift Coins</li>
</ul>
</div>

<div class="col-lg-4 col-md-4 col-sm-4"><h4>CONNECT US ON</h4>
<hr class="hr_zero">
<ul class="social">
<li><a href="#" class="btn btn-social-icon btn-twitter"><i class="fa fa-twitter"></i></a></li>
 <li><a href="#" class="btn btn-social-icon btn-facebook"><i class="fa fa-facebook"></i></a></li>
  <li><a href="#" class="btn btn-social-icon btn-google-plus"><i class="fa fa-google-plus"></i></a></li>
</ul>
</div>

</div>
</div>
<footer>� Mindsarray Technologies Pvt. Ltd. All Rights Reserved</footer>
  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    
    <!-- Include all compiled plugins (below), or include individual files as needed -->

</body>
</html>
<script src="js/jquery-ui.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script type="text/javascript">
	
	  $("ul.mobile li a").click(function(e){
            e.preventDefault();
             var operatorid = $(this).attr("href");
			 
              if(operatorid =="#mobile")
              {
                  $("#dth").hide();
                  $("#data").hide();
                  $("#postpaid").hide();
				  $(".dth").hide();
                  $(".data").hide();
                  //$(".postpaid").hide();
                  $(operatorid).show();
				  
              }
              else if(operatorid =="#dth")
              {
                  $("#mobile").hide();
                  $("#data").hide();
                  $("#postpaid").hide();
				  $(".mob").hide();
                  $(".data").hide();
                  $(".postpaid").hide();
                  $(operatorid).show();
				  
              }
              else if(operatorid =="#data")
              {
                  $("#mobile").hide();
                  $("#postpaid").hide();
                  $("#dth").hide();
				  $(".mob").hide();
                  $(".dth").hide();
                  $(".postpaid").hide();
                  $(operatorid).show();
              }
              else if(operatorid =="#postpaid")
              {
                  $("#mobile").hide();
                  $("#data").hide();
                  $("#dth").hide();
				  $(".mob").hide();
                  $(".dth").hide();
                  $(".data").hide();
                  $(operatorid).show();
              }
              
        });
		$(".hidecheckplans").click(function(){
                $(".checkplans").slideUp();
                $(".opencheckplans").prop('disabled', false);
            });
    
$('#login_modal').on('submit', function (event) {
var mobileNo = $("#mob-number").val();
    var password = $("#password").val();
    if (mobileNo == '')
    {
        alert("Please Enter mobile no.");
        $("#mob-number").focus();
        return false;
    }
    else if (password == '')
    {
        alert("Password can not be blank");
        $("#password").focus();
        return false;
    }
    else
    {
        var url = "<?Php echo CDEV_URL; ?>/index.php/api_new/action/actiontype/signin/api/true/?";
        $.ajax({
            url: url,
            type: "GET",
            data: {username: mobileNo,
                password: password,
                uuid: "",
                latitude: "",
                logitude: "",
                device_type: "",
                res_format: "jsonp"
            },
            timeout: 50000,
            dataType: "jsonp",
            crossDomain: true,
            success: function(data) {
                if (data.status == "failure")
                {
				 alert(data.description);
                }
                else
                {
					setUserData(data);
                }

            },
            error: function(xhr, error) {
				
          },
        });
    }
	return false;
});

function setUserData(data)
{
    var mobileNo = data.description["mobile"];
    var name = data.description["name"];
    var email = data.description["email"];
    var dob = data.description["date_of_birth"];
    var longitude =  data.description["longitude"]; 
    var latitude =  data.description["latitude"]; 
    var walletbal = data.description["wallet_balance"];
    var device_type = data.description["device_type"];
    var gender = data.description["gender"];
    //var password =  $("#password").val();

       $.ajax({
            url: "user_details.php",
            type:"POST",
            data:{
                 name:name,
                 email:email,
                 dob:dob,
                 username:mobileNo,
                 device_type:device_type,
                 gender:gender,
                 walletbal : walletbal,
                 },
            timeout: 50000,
            dataType: "json",
            success:function(data){
			if(data.status=='success'){	
            window.location.reload();
		    }
            },
            error: function (error) {
          }
            });
}
        $(document).ready(function(){
	    var amount = localStorage.getItem('amount');
        var mobileno = "<?php echo $_SESSION["mobile"]; ?>";
        var paymentstatus = localStorage.getItem("paymentstatus");
        var walletstatus = localStorage.getItem("walletstatus");
        var provider =  localStorage.getItem('provider');
        var number = localStorage.getItem('number');
        var type = localStorage.getItem('type');
        var balance = "<?php echo $_SESSION["wallet_balance"] ?>";
        
        var transid = localStorage.getItem('trans_id');
		$("#order_type").html(type); 
		$("#order_number").html(number);
		$("#order_amount").html(amount);
		$("#order_serviceprovider").html(provider);
		$("#closing_balanace").html(balance);
		$("#wallet_amount").html(balance);
		$("#trans_id").html(transid);
		
	   if(paymentstatus=="success" && walletstatus!='true' ){
		$("#order_msg").html("Transaction completed succcessfully!");
		$("#order_response").html("Success!");
		$("#order_msg").addClass("ordersucHead");
		$("#order_response").addClass("ordersucHead");
		 if(type!='Postpaid Mobile Number'){
		  $("#order_deal").show();
		 }
	   $("#success_msg").show();
	   $('#ordersuccess1').modal('toggle' );
	   localStorage.removeItem("paymentstatus");
	   }
	   
	   else if(paymentstatus=="error" && walletstatus!='true')
	   {
		$("#order_msg").html("Transaction failed!!!");
		$("#order_response").html("failed!");
		$("#order_msg").addClass("transfaildTXT");
		$("#order_response").addClass("transfaildTXT");
		$("#error_msg").show();
	    $('#ordersuccess1').modal( 'toggle' );
	    localStorage.removeItem("paymentstatus");
	   }
	   
	   else if(paymentstatus=="success"  && walletstatus=="true")
	   {
		$("#wallet_msg").html("wallet recharge succcessfully!!!");
		$("#wallet_response").html("Success!");
		$("#wallet_msg").addClass("ordersucHead");
		$("#wallet_response").addClass("ordersucHead");
	    $('#ordersuccess2').modal( 'toggle' );
		localStorage.removeItem("paymentstatus");
	    localStorage.removeItem("walletstatus");
	   }
	   else if(paymentstatus=="error"  && walletstatus=="true")
	   {
			$("#wallet_msg").html("wallet recharge failed!!!");
			$("#wallet_response").html("failed!");
			$("#wallet_msg").addClass("transfaildTXT");
			$("#wallet_response").addClass("transfaildTXT");
		   $('#ordersuccess2').modal( 'toggle' );
		   localStorage.removeItem("paymentstatus");
		   localStorage.removeItem("walletstatus");
		   
	    }
		   
   });
   
   function signup(){
	   $("#basicModal").modal('hide');
	   $("#basicModal1").modal('toggle');
   }
   
   function register(){
	   var mobileNo = $("#mobnumber").val();
	   if(mobileNo =='')
        {
            alert("Please enter a  mobile no.");
            return false;
        }
		else
        {
           var url = "<?php echo CDEV_URL; ?>/index.php/api_new/action/api/true/actiontype/create_user/?";
            $.ajax({
            url: url,
            type:"GET",
            data:{mobile_number:mobileNo,
                  res_format : "jsonp"
                 },
            timeout: 50000,
            dataType: "jsonp",
            jsonpCallback: 'callback',
            crossDomain: true,
            success:function(data){
				console.log(data);
                if(data.status=="success")
                {
					localStorage.setItem("mob_number",mobileNo);
					localStorage.setItem("otp",data.description.otp);
					$(".mobno").hide();
					$(".otp").show();
                }
                else
                { 
					$(".mobno").hide();
					$(".otp").show();
					$("#sign_msg").html("User already registered with this Mobile Number");
					$("#register_user").hide();
					$("#check_otp").show();
					localStorage.setItem("mob_number",mobileNo);
					localStorage.setItem("otp",data.description.otp);
				}
            },
            error: function (xhr,error) {
             },  
         
            });
	   
   }
   }
   
   function checkotp(){
         
        var mobileno = localStorage.getItem("mob_number");
        var gender = localStorage.getItem("gender");
        var otp = $("#otp").val();
        var resonseotp = localStorage.getItem("otp");
        var confirmotp = md5(mobileno)+md5(otp);
        
        if(otp=='')
        {
            alert("Please enter a one time password.");
            return false;
        }
        else if(confirmotp != resonseotp){
            
            alert("Invalid Otp");
            return false;
        }
		else
        {
            var url = "<?php echo CDEV_URL; ?>/index.php/api_new/action/api/true/actiontype/update_password/?";
            $.ajax({
            url: url,
            type:"GET",
            data:{mobile_number:mobileno,
                  password:otp,
                  confirm_password:otp,
                  code:otp,
                  vtype:"1",
                  res_format : "jsonp"
                 },
            timeout: 50000,
            dataType: "jsonp",
            crossDomain: true,
            success:function(data){
            if(data.status=="success")
             {
            var loginurl = "<?php echo CDEV_URL; ?>/index.php/api_new/action/actiontype/signin/api/true/?";
            $.ajax({
            url: loginurl,
            type:"GET",
            data:{username:mobileno,
                  password:otp,
                  uuid:"",
                  latitude:"",
                  logitude:"",
                  device_type:"",
                  res_format : "jsonp"
                 },
            timeout: 50000,
            dataType: "jsonp",
            jsonpCallback: 'callback',
            crossDomain: true,
            success:function(data){
               if(data.status=="success")
               {    
                   setUserData(data);
               }
                else {
                   
                }

            },
            error: function (xhr,error) {
				console.log(error);
             },  
         
            });
              } else {
                   $("#msg").html(data.description);
              }
               
            },
            error: function (xhr,error) {
				console.log(error);
             },  
         
            });
        }
   
   }
    
</script>

