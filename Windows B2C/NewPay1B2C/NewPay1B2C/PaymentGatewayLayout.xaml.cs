﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace NewPay1B2C
    {
    public partial class PaymentGatewayLayout : PhoneApplicationPage
        {

        public PaymentGatewayLayout ()
            {
            InitializeComponent ();
            //GoogleAnalytics.EasyTracker.GetTracker ().SendView ("PaymentGatewayLayout");
            }
        private void wb1_Loaded (object sender, RoutedEventArgs e)
            {
            paymentBrowser.ApplyTemplate ();
            paymentBrowser.IsScriptEnabled = true;

            string paymentHtml = PhoneApplicationService.Current.State[Constant.PAYMENT_GATEWAY_HTML].ToString ();
            paymentBrowser.NavigateToString (paymentHtml);

            }



        private void wb1_unloaded (object sender, RoutedEventArgs e)
            {
            var res = e.OriginalSource;
            }

        private void Browser_Navigated (object sender, System.Windows.Navigation.NavigationEventArgs e)
            {
            string uri = e.Uri.OriginalString;

            if (uri.Contains ("api_new/action/api/true/actiontype/payu_update/page/success"))
                {
                MessageBox.Show ("success");
				NavigationService.GoBack ();
                }
            else if (uri.Contains ("api_new/action/api/true/actiontype/payu_update/page/failure"))
                {
                MessageBox.Show ("failure");
				NavigationService.GoBack ();
                }
            else if (uri.Contains ("api_new/action/api/true/actiontype/payu_update/page/timeout"))
                {
                MessageBox.Show ("timeout");
				NavigationService.GoBack ();
                }
            else if (uri.Contains ("api_new/action/api/true/actiontype/payu_update/page/cancel"))
                {
                MessageBox.Show ("cancel");
				NavigationService.GoBack ();
                }
			//
            }
        private void buttonImage_Click_Back (object sender, RoutedEventArgs e)
            {
            NavigationService.GoBack ();
            }
        }
    }