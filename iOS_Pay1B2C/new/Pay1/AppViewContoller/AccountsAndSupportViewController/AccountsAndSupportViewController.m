//
//  AccountsAndSupportViewController.m
//  Pay1
//
//  Created by webninjaz on 16/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "AccountsAndSupportViewController.h"
#import "ProfileViewController.h"
#import "SupportViewController.h"
#import "FAQViewController.h"
#import "PrivacyPolicyViewController.h"
#import "TermsAndConditionViewController.h"

@interface AccountsAndSupportViewController ()

@end

@implementation AccountsAndSupportViewController

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)

- (void)viewDidLoad {
    [super viewDidLoad];
  //  [self addOfferButtonOnAllView];
    
    [self hideNavBackButton];
    
    [self setNaviationButtonWithText:@"BACK" selector:@selector(backClicked:) isLeft:TRUE isReduceSize:TRUE isMyFont:18.0];

    
    accountsAndSupportTableView = [[UITableView alloc]init];
    accountsAndSupportArray = [[NSMutableArray alloc]initWithObjects:@"My Profile",@"Support",@"FAQ",@"Privacy Policy",@"Terms & Conditions",@"Rate Us", nil];
    
   
    if ([UIScreen mainScreen].bounds.size.height == 480){
        accountsAndSupportTableView.frame = CGRectMake(0, 0, 320, 265);
    }
    
    else if ([UIScreen mainScreen].bounds.size.height == 568) {
        accountsAndSupportTableView.frame = CGRectMake(0, 0, 320, 265);
    }
    
    else if ([UIScreen mainScreen].bounds.size.height == 667) {
        accountsAndSupportTableView.frame = CGRectMake(0, 0, 375, 250);
    }
    else if([UIScreen mainScreen].bounds.size.height == 736){
        accountsAndSupportTableView.frame = CGRectMake(0, 0, 414, 250);
    }
    
    
    
 /*   revealViewController = [self revealViewController];
    
    [revealViewController panGestureRecognizer];
    [revealViewController tapGestureRecognizer];
    self.revealViewController.delegate = self;
    
    
    UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"navigation_sliding_bar.png"] style:UIBarButtonItemStyleDone target:self.revealViewController action:@selector(revealToggle:)];
    leftBarButton.tintColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItem = leftBarButton;
    self.navigationItem.rightBarButtonItem = nil;
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];*/
    
    
    NSLog(@"FrameWidth :%f",self.view.frame.size.width);
    

}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return accountsAndSupportArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    cell.textLabel.text = [accountsAndSupportArray objectAtIndex:indexPath.row];
    NSLog(@"The text label is%@",cell.textLabel.text);
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    cell.textLabel.textColor = [UIColor blackColor];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
     [tableView deselectRowAtIndexPath:indexPath animated:YES];
      NSLog(@"%ld", (long)indexPath.row);
    if (indexPath.row == 0) {
        ProfileViewController *profileVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
        [self.navigationController pushViewController:profileVC animated:YES];
    }
    if (indexPath.row == 1) {
        SupportViewController *supportVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SupportViewController"];
        [self.navigationController pushViewController:supportVC animated:YES];
    }
    
    if (indexPath.row == 2) {
        FAQViewController *FAQVC = [self.storyboard instantiateViewControllerWithIdentifier:@"FAQViewController"];
        [self.navigationController pushViewController:FAQVC animated:YES];
    }
    if (indexPath.row == 3) {
        PrivacyPolicyViewController *privacyVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PrivacyPolicyViewController"];
        [self.navigationController pushViewController:privacyVC animated:YES];
    }
    if (indexPath.row == 4) {
        TermsAndConditionViewController *termsAndConditionVC = [self.storyboard instantiateViewControllerWithIdentifier:@"TermsAndConditionViewController"];
        [self.navigationController pushViewController:termsAndConditionVC animated:YES];
    }
    
    if (indexPath.row == 5) {
       // RateUsViewController *rateVC = [self.storyboard instantiateViewControllerWithIdentifier:@"RateUsViewController"];
      //  [self.navigationController pushViewController:rateVC animated:YES];
    }
    
   
}

-(void)backClicked:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
