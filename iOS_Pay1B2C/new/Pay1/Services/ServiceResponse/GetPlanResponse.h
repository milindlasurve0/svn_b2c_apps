//
//  GetPlanResponse.h
//  Pay1
//
//  Created by Annapurna on 05/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GetPlanResponse : NSObject

@property (nonatomic, strong) NSString *productCode;
@property (nonatomic, strong) NSString *operatorName;


@property (nonatomic, strong) NSMutableDictionary *getPlansDic;






@end
