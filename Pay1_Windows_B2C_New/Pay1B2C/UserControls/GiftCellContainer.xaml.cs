﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace Pay1B2C.UserControls
    {
    public partial class GiftCellContainer: UserControl
        {
        public GiftCellContainer()
            {
            InitializeComponent();
            }

        private void listBoxOffer_SelectionChanged(object sender, SelectionChangedEventArgs e)
            {
            GiftCells GiftCells=listBoxOffer.SelectedValue as GiftCells;
          
            String offId=GiftCells.textDealId.Text.ToString();
            (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri("/Views/GiftDetailsPage.xaml?offerId="+offId+"&fromMyDeal=0", UriKind.RelativeOrAbsolute));
            }
        }
    }
