//
//  RateView.m
//  Pay1
//
//  Created by Annapurna on 24/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "RateView.h"

@implementation RateView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self awakeFromNib];
    }
    return self;
}

-(void)awakeFromNib {
    
    UIView* myView = [[[NSBundle mainBundle] loadNibNamed:@"RateView" owner:self options:nil] objectAtIndex:0];
    _loveITLabel.textColor = [UIColor colorWithRed:50/255.0 green:172/255.0 blue:59/255.0 alpha:1];
    _likeITLabel.textColor = [UIColor colorWithRed:140/255.0 green:202/255.0 blue:91/255.0 alpha:1];
    _justOKLabel.textColor = [UIColor colorWithRed:163/255.0 green:205/255.0 blue:58/255.0 alpha:1];
    _poorLabel.textColor = [UIColor colorWithRed:192/255.0 green:134/255.0 blue:33/255.0 alpha:1];
    _badLabel.textColor = [UIColor colorWithRed:163/255.0 green:26/255.0 blue:35/255.0 alpha:1];

    [self addSubview:myView];
    
}

-(IBAction)loveItClicked:(id)sender{
    NSLog(@"LoveITClicked");
    if(_onLoveITClick)
    _onLoveITClick();
}

-(IBAction)likeItClicked:(id)sender{
    NSLog(@"LikeITClicked");
    if(_onLikeITClick)
    _onLikeITClick();
}

-(IBAction)justOKClicked:(id)sender{
    NSLog(@"JustOKClicked");
    if(_onJustOKClick)
    _onJustOKClick();
}

-(IBAction)poorClicked:(id)sender{
    NSLog(@"PoorClicked");
    if(_onPoorClick)
    _onPoorClick();
}

-(IBAction)itsBadClicked:(id)sender{
    NSLog(@"BadClicked");
    if(_onItsBadClick)
    _onItsBadClick();
}

-(IBAction)crossClicked:(id)sender{
    if(_onCrossClick)
    _onCrossClick();
    
}

@end
