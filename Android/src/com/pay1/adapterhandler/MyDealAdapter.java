package com.pay1.adapterhandler;

import java.util.ArrayList;
import java.util.WeakHashMap;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.pay1.ConnectivityErrorActivity;
import com.pay1.DealDetailsActivity;
import com.pay1.DealVoucherQRCodeActivity;
import com.pay1.MyDealActivity;
import com.pay1.R;
import com.pay1.constants.Constants;
import com.pay1.customviews.MyProgressDialog;
import com.pay1.imagehandler.ImageLoader;
import com.pay1.requesthandler.RequestClass;

public class MyDealAdapter extends BaseAdapter {

	private Context context;
	private ArrayList<WeakHashMap<String, String>> data;
	private ImageLoader imageLoader;
	private Typeface Reguler;

	public MyDealAdapter(Context context,
			ArrayList<WeakHashMap<String, String>> data) {
		this.context = context;
		this.data = data;
		imageLoader = new ImageLoader(context);
		Reguler = Typeface.createFromAsset(context.getAssets(),
				"EncodeSansNormal-400-Regular.ttf");
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder viewHolder;
		try {
			if (convertView == null) {
				LayoutInflater inflater = (LayoutInflater) context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(R.layout.my_deal_adapter, null);
				viewHolder = new ViewHolder();
				viewHolder.imageView_Image = (ImageView) convertView
						.findViewById(R.id.imageView_Image);
				viewHolder.imageView_Image
						.setOnClickListener(new View.OnClickListener() {

							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								try {
									WeakHashMap<String, String> map = new WeakHashMap<String, String>();
									map = data.get(position);
									Intent intent = new Intent(context,
											DealDetailsActivity.class);
									intent.putExtra("IS_BUY", 0);
									intent.putExtra("URL",
											map.get(MyDealActivity.URL));
									intent.putExtra("DEAL_ID",
											map.get(MyDealActivity.DEAL_ID));
									intent.putExtra("DEAL_NAME",
											map.get(MyDealActivity.DEAL_NAME));
									intent.putExtra("ACTUAL_PRICE", map
											.get(MyDealActivity.ACTUAL_PRICE));
									intent.putExtra("OFFER_PRICE",
											map.get(MyDealActivity.OFFER_PRICE));
									intent.putExtra("STOCK_SOLD",
											map.get(MyDealActivity.STOCK_SOLD));
									intent.putExtra("DISCOUNT",
											map.get(MyDealActivity.DISCOUNT));

									context.startActivity(intent);
								} catch (Exception e) {
								}
							}
						});
				viewHolder.textView_DealName = (TextView) convertView
						.findViewById(R.id.textView_DealName);
				viewHolder.textView_DealName.setTypeface(Reguler);
				viewHolder.textView_Coupon = (TextView) convertView
						.findViewById(R.id.textView_Coupon);
				viewHolder.textView_Coupon.setTypeface(Reguler);
				viewHolder.textView_Expity = (TextView) convertView
						.findViewById(R.id.textView_Expity);
				viewHolder.textView_Expity.setTypeface(Reguler);
				viewHolder.textView_TitleCoupon = (TextView) convertView
						.findViewById(R.id.textView_TitleCoupon);
				viewHolder.textView_TitleCoupon.setTypeface(Reguler);
				viewHolder.textView_TitleExpiry = (TextView) convertView
						.findViewById(R.id.textView_TitleExpiry);
				viewHolder.textView_TitleExpiry.setTypeface(Reguler);
				viewHolder.imageView_ViewDetails = (ImageView) convertView
						.findViewById(R.id.imageView_ViewDetails);

				viewHolder.imageView_SMS = (ImageView) convertView
						.findViewById(R.id.imageView_SMS);

				viewHolder.imageView_Email = (ImageView) convertView
						.findViewById(R.id.imageView_Email);

				convertView.setTag(viewHolder);

			} else {
				viewHolder = (ViewHolder) convertView.getTag();
			}
			WeakHashMap<String, String> map = new WeakHashMap<String, String>();
			map = data.get(position);
			imageLoader.getDealImage(map.get(MyDealActivity.URL),
					viewHolder.imageView_Image);
			viewHolder.textView_DealName.setText(map
					.get(MyDealActivity.DEAL_NAME));
			viewHolder.textView_Coupon.setText(map
					.get(MyDealActivity.COUPON_STATUS));
			viewHolder.textView_Expity.setText(map.get(MyDealActivity.EXPIRY));

			viewHolder.imageView_ViewDetails
					.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							// Toast.makeText(context, "View for QR code.",
							// Toast.LENGTH_LONG).show();
							WeakHashMap<String, String> map = new WeakHashMap<String, String>();
							map = data.get(position);
							String code = map.get(MyDealActivity.VOUCHER_CODE);
							if (!code.equalsIgnoreCase("null")) {
								Intent intent = new Intent(context,
										DealVoucherQRCodeActivity.class);
								intent.putExtra("CODE", code);
								intent.putExtra("EXPIRY",
										map.get(MyDealActivity.EXPIRY));
								intent.putExtra("QUANTITY",
										map.get(MyDealActivity.QUANTITY));
								intent.putExtra("ID",
										map.get(MyDealActivity.TRANSACTION_ID));

								context.startActivity(intent);
							} else {
								Toast.makeText(
										context,
										"Sorry, you do not have valid voucher code.",
										Toast.LENGTH_LONG).show();
							}
						}
					});

			viewHolder.imageView_SMS
					.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							WeakHashMap<String, String> map = new WeakHashMap<String, String>();
							map = data.get(position);
							new NofityMeTask().execute("sms",
									map.get(MyDealActivity.TRANSACTION_ID));
						}
					});

			viewHolder.imageView_Email
					.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							WeakHashMap<String, String> map = new WeakHashMap<String, String>();
							map = data.get(position);
							new NofityMeTask().execute("email",
									map.get(MyDealActivity.TRANSACTION_ID));
						}
					});

			convertView.setClickable(false);
		} catch (Exception e) {
		}
		return convertView;
	}

	private class ViewHolder {
		ImageView imageView_Image;
		TextView textView_DealName, textView_Coupon, textView_Expity,
				textView_TitleCoupon, textView_TitleExpiry;
		ImageView imageView_ViewDetails, imageView_SMS, imageView_Email;
	}

	public class NofityMeTask extends AsyncTask<String, String, String>
			implements OnDismissListener {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			try {
				String response = RequestClass.getInstance()
						.readPay1B2CRequest(
								context,
								Constants.B2C_URL + "notifyme/?type="
										+ params[0] + "&transaction_id="
										+ params[1]);

				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			try {

				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONObject jsonObject = new JSONObject(replaced);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {
						Constants.showOneButtonDialog(context, "My Deal",
								jsonObject.getString("description"),
								Constants.DIALOG_CLOSE);

					} else {
						Constants.showOneButtonDialog(context, "My Deal",
								Constants.checkCode(replaced),
								Constants.DIALOG_CLOSE);
					}

				} else {
					// Constants.showOneButtonDialog(context,
					// "My Deal", Constants.ERROR_INTERNET,
					// Constants.DIALOG_CLOSE);
					Intent intent = new Intent(context,
							ConnectivityErrorActivity.class);
					intent.putExtra(Constants.RECHARGE_FOR,
							Constants.SUBSCRIBE_DEAL);
					context.startActivity(intent);

				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonDialog(context, "My Deal",
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonDialog(context, "My Deal",
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(context);
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							NofityMeTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			NofityMeTask.this.cancel(true);
			dialog.cancel();
		}

	}
}
