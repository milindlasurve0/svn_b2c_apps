package com.pay1;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.provider.SyncStateContract.Constants;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.pay1.adapterhandler.InfoAdapter;
import com.pay1.constants.*;

public class HistoryFragment extends Fragment {

	private ListView listView_History;
	private ArrayList<String> list;
	private InfoAdapter adapter;

	public static HistoryFragment newInstance() {
		HistoryFragment fragment = new HistoryFragment();
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// if ((savedInstanceState != null)
		// && savedInstanceState.containsKey(KEY_CONTENT)) {
		// mContent = savedInstanceState.getString(KEY_CONTENT);
		// }
	}

	@SuppressLint("NewApi")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.history_fragment, container,
				false);
		
		try {
			TextView textViewBe=(TextView)view.findViewById(R.id.textViewBe);
			textViewBe.setTypeface(Typeface.createFromAsset(getActivity().getAssets(),
					"Roboto-Regular.ttf"));
			

			listView_History = (ListView) view
					.findViewById(R.id.listView_History);
			list = new ArrayList<String>();
			list.clear();
			list.add("Transaction History");
			list.add("Wallet History");
			list.add("Gifts Coin History");
			adapter = new InfoAdapter(getActivity(), 2, list);
			listView_History.setAdapter(adapter);
			listView_History
					.setOnItemClickListener(new AdapterView.OnItemClickListener() {

						@Override
						public void onItemClick(AdapterView<?> parent,
								View view, int position, long id) {
							// TODO Auto-generated method stub
							switch (position) {
							case 0:
								startActivity(new Intent(getActivity(),
										PurchaseHistoryActivity.class));
								break;
							case 1:
								startActivity(new Intent(getActivity(),
										WalletHistoryActivity.class));
								break;
							case 2:
								startActivity(new Intent(getActivity(),
										CoinHistoryActivity.class));
								break;
							default:
										break;
							}

						}
					});

		} catch (Exception e) {
		}

		return view;
	}

	@Override
	public void onStart() {
		super.onStart();
	}

	@Override
	public void onStop() {
		super.onStop();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.Fragment#onDestroy()
	 */
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();

	}

}
