//
//  HomeViewController.h
//  Pay1
//
//  Created by Annapurna on 21/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "BaseViewController.h"
@interface HomeViewController : BaseViewController<UIScrollViewDelegate,UIGestureRecognizerDelegate>{
    UIView *coverView;
    NSArray* offerIDArray;
    
    IBOutlet UIView *mapButtonView;
    
    IBOutlet UIImageView *offerImageView;
    IBOutlet UIView *offerCoverView;
    IBOutlet UILabel *offerTextLabel;
    
    IBOutlet UIScrollView *scrollView;
    
    float imageY;
    

    
    NSMutableArray *array;
    NSMutableArray *offerTextArray;
    NSMutableArray *allOfferArrayList;
    NSMutableArray *dealArray;
    
    NSString *offerIDS;
    
    NSMutableArray *shopNameArray;
    
    IBOutlet UIButton *giftButton;
    IBOutlet UIButton *rechargeButton;
    IBOutlet UILabel *rechargeTxtLabel;
    
    IBOutlet UILabel *rechargeLabel,*coinLabel,*giftsLabel;
    IBOutlet UIButton *firstArrowButton,*secondArrowButton;

    IBOutlet UIView *seeAllButton;
    IBOutlet UIButton *mapLocationButton;
    
    int  callGetDealValue ;
    NSString *offerNameStr;
    NSString *offerIDStr;

    
}

@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;

-(void)loginDone;

@end
