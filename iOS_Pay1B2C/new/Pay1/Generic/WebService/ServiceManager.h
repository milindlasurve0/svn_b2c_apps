//
//  ServiceManager.h
//  ASIHTTPDemo
//
//  Created by Sunita on 02/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RequestData.h"

@interface ServiceManager : NSObject
{
  
}
+ (id)sharedInstance;

-(void)makeRequest:(RequestData *)requestData;
-(void)makeRequestForGet:(RequestData *)requestData;
-(void)makeRequestForPost:(RequestData *)requestData;
-(NSURL *)createRequestUrl:(RequestData *)requestData;
-(NSString*) makeKeyUsingDictionary:(NSDictionary *)dic;
@end
