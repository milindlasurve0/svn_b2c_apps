package com.pay1;

import java.util.ArrayList;
import java.util.List;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.SQLException;
import android.graphics.Typeface;
import android.location.Location;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.larvalabs.svgandroid.SVGParser;
import com.pay1.adapterhandler.GiftGridAdapter;
import com.pay1.constants.Constants;
import com.pay1.databasehandler.FreeBies;
import com.pay1.databasehandler.FreeBiesDataSource;
import com.pay1.servicehandler.GPSTracker;
import com.pay1.utilities.Utility;

public class GiftNearFragment extends Fragment implements ConnectionCallbacks,
		OnConnectionFailedListener {

	private GridView gridView_Gifts;
	private GiftGridAdapter adapter;
	private ArrayList<FreeBies> data;
	private FreeBiesDataSource freeBiesDataSource;
TextView textView_NoData;
	private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;

	private Location mLastLocation;

	// Google client to interact with Google API
	private GoogleApiClient mGoogleApiClient;

	// boolean flag to toggle periodic location updates
	private boolean mRequestingLocationUpdates = false;

	private LocationRequest mLocationRequest;
	RelativeLayout relativeLayout_EmptyView;
	// Location updates intervals in sec
	private static int UPDATE_INTERVAL = 10000; // 10 sec
	private static int FATEST_INTERVAL = 5000; // 5 sec
	private static int DISPLACEMENT = 0; // 10 meters
	ImageView imageViewMyGift;

	public static GiftNearFragment newInstance() {
		GiftNearFragment fragment = new GiftNearFragment();
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// if ((savedInstanceState != null)
		// && savedInstanceState.containsKey(KEY_CONTENT)) {
		// mContent = savedInstanceState.getString(KEY_CONTENT);
		// }
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		final View view = inflater.inflate(R.layout.gift_grid_fragment,
				container, false);
		try {
			relativeLayout_EmptyView = (RelativeLayout) view
					.findViewById(R.id.relativeLayout_EmptyView);
			imageViewMyGift = (ImageView) view
					.findViewById(R.id.imageViewMyGift);
			// First we need to check availability of play services
			if (checkPlayServices()) {

				// Building the GoogleApi client
				buildGoogleApiClient();
			}
			textView_NoData=(TextView)view.findViewById(R.id.textView_NoData);
			freeBiesDataSource = new FreeBiesDataSource(getActivity());
			textView_NoData.setTypeface(Typeface.createFromAsset(getActivity().getAssets(),
					"Roboto-Regular.ttf"));
			data = new ArrayList<FreeBies>();
			gridView_Gifts = (GridView) view.findViewById(R.id.gridView_Gifts);
			
			// String ExternalStorageDirectoryPath = Environment
			// .getExternalStorageDirectory().getAbsolutePath();
			//
			// String targetPath = ExternalStorageDirectoryPath + "/Pay1Cache/";
			//
			// File targetDirector = new File(targetPath);
			//
			// File[] files = targetDirector.listFiles();
			// int i = 1;
			// for (File file : files) {
			// items.add(new GiftItem(i, "Gift " + i, file.getAbsolutePath(),
			// i * 10));
			// i++;
			// }

			
			
			
			
			try {/*
				freeBiesDataSource.open();
				String lat = Utility.getCurrentLatitude(getActivity(),
						Constants.SHAREDPREFERENCE_CURRENT_LATITUDE);
				String lng = Utility.getCurrentLongitude(getActivity(),
						Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE);

				if (lat != "" && lng != "") {
					List<FreeBies> list = freeBiesDataSource
							.getAllNearFreeBies(Double.parseDouble(lat),
									Double.parseDouble(lng), 5);

					int len = list.size();
					
					if (len != 0) {

						for (FreeBies freebie : list)
							data.add(freebie);

						// Collections.sort(data);

					} else {
						gridView_Gifts.setVisibility(View.GONE);
						relativeLayout_EmptyView.setVisibility(View.VISIBLE);
						
						// new FreeBiesTask()
						// .execute(
						// Utility.getCurrentLatitude(
						// getActivity(),
						// Constants.SHAREDPREFERENCE_CURRENT_LATITUDE),
						// Utility.getCurrentLongitude(
						// getActivity(),
						// Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE));
						// pager.setVisibility(View.GONE);

					}
				}else{
					relativeLayout_EmptyView.setVisibility(View.VISIBLE);
					gridView_Gifts.setVisibility(View.GONE);
				}

			*/} catch (SQLException exception) {
				// TODO: handle exception
				Log.e("Er ", exception.getMessage());
			} catch (Exception e) {
				// TODO: handle exception
				// Log.e("Er ", e.getMessage());
			} finally {
				freeBiesDataSource.close();
			}
			adapter = new GiftGridAdapter(getActivity(), data, false, true,
					false);

			// items.clear();
			gridView_Gifts.setAdapter(adapter);
			gridView_Gifts
					.setOnItemClickListener(new AdapterView.OnItemClickListener() {

						@Override
						public void onItemClick(AdapterView<?> parent,
								View view, int position, long id) {
							// TODO Auto-generated method stub
							try {
								FreeBies freeBie = data.get(position);
								Intent intent = new Intent(getActivity(),
										MyFreeBiesDetailsActivity.class);
								intent.putExtra("GIFT", freeBie);
								intent.putExtra("IS_BUY", 0);
								startActivity(intent);
							} catch (Exception e) {
							}
						}
					});
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return view;
	}
	
	

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		// outState.putString(KEY_CONTENT, mContent);
	}

	@Override
	public void onStart() {
		super.onStart();
		if (mGoogleApiClient != null) {
			mGoogleApiClient.connect();
		}
	}

	@Override
	public void onResume() {
		super.onResume();

		checkPlayServices();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.Fragment#onDestroy()
	 */
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	/**
	 * Method to display the location on UI
	 * */
	private void displayLocation() {

		mLastLocation = LocationServices.FusedLocationApi
				.getLastLocation(mGoogleApiClient);

		if (mLastLocation != null) {
			double latitude = mLastLocation.getLatitude();
			double longitude = mLastLocation.getLongitude();

			/*
			 * Utility.setCurrentLatitude(getActivity(),
			 * Constants.SHAREDPREFERENCE_CURRENT_LATITUDE,
			 * String.valueOf(latitude));
			 * Utility.setCurrentLongitude(getActivity(),
			 * Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE,
			 * String.valueOf(longitude));
			 */

			GPSTracker gps = new GPSTracker(getActivity());
			if (gps.canGetLocation()) { // gps enabled} // return boolean
										// true/false

				gps.getLatitude(); // returns latitude
				gps.getLongitude(); // returns longitude

				Utility.setCurrentLatitude(getActivity(),
						Constants.SHAREDPREFERENCE_CURRENT_LATITUDE,
						String.valueOf(gps.getLatitude()));
				Utility.setCurrentLongitude(getActivity(),
						Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE,
						String.valueOf(gps.getLongitude()));
			}else{
				
			}

			Log.e("NEAR", latitude + ", " + longitude);

			try {
				freeBiesDataSource.open();
				String lat = Utility.getCurrentLatitude(getActivity(),
						Constants.SHAREDPREFERENCE_CURRENT_LATITUDE);
				String lng = Utility.getCurrentLongitude(getActivity(),
						Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE);

				if (lat != "" && lng != "") {
					data.clear();
					/*List<FreeBies> list = freeBiesDataSource
							.getAllNearFreeBies(Double.parseDouble(lat),
									Double.parseDouble(lng), 20);*/
					List<FreeBies> list = freeBiesDataSource
							.getAllNearFreeBiesShortest(Double.parseDouble(lat),
									Double.parseDouble(lng), 10);
					

					int len = list.size();
					
					if (len != 0) {

						for (FreeBies freebie : list)
							data.add(freebie);

					} else {
						
						relativeLayout_EmptyView.setVisibility(View.VISIBLE);
						gridView_Gifts.setVisibility(View.GONE);
						// new FreeBiesTask()
						// .execute(
						// Utility.getCurrentLatitude(
						// getActivity(),
						// Constants.SHAREDPREFERENCE_CURRENT_LATITUDE),
						// Utility.getCurrentLongitude(
						// getActivity(),
						// Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE));
						// pager.setVisibility(View.GONE);

					}
				}else{
					relativeLayout_EmptyView.setVisibility(View.VISIBLE);
					gridView_Gifts.setVisibility(View.GONE);
				}
				getActivity().runOnUiThread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						adapter.notifyDataSetChanged();
					}
				});
			} catch (SQLException exception) {
				// TODO: handle exception
				Log.e("Er ", exception.getMessage());
			} catch (Exception e) {
				// TODO: handle exception
				// Log.e("Er ", e.getMessage());
			} finally {
				freeBiesDataSource.close();
			}

		} else {

			Log.e("NEAR",
					"(Couldn't get the location. Make sure location is enabled on the device)");
		}
	}
	
	
	

	/**
	 * Creating location request object
	 * */
	protected void createLocationRequest() {
		mLocationRequest = new LocationRequest();
		mLocationRequest.setInterval(UPDATE_INTERVAL);
		mLocationRequest.setFastestInterval(FATEST_INTERVAL);
		mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
		mLocationRequest.setSmallestDisplacement(DISPLACEMENT);
	}

	/**
	 * Creating google api client object
	 * */
	protected synchronized void buildGoogleApiClient() {
		mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
				.addConnectionCallbacks(this)
				.addOnConnectionFailedListener(this)
				.addApi(LocationServices.API).build();
	}

	/**
	 * Method to verify google play services on the device
	 * */
	private boolean checkPlayServices() {
		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(getActivity());
		if (resultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
				GooglePlayServicesUtil.getErrorDialog(resultCode,
						getActivity(), PLAY_SERVICES_RESOLUTION_REQUEST).show();
			} else {
				Toast.makeText(getActivity(), "This device is not supported.",
						Toast.LENGTH_LONG).show();

			}
			return false;
		}
		return true;
	}

	@Override
	public void onConnectionFailed(ConnectionResult arg0) {
		// TODO Auto-generated method stub
		Log.i("NEAR", "Connection failed: ConnectionResult.getErrorCode() = "
				+ arg0.getErrorCode());
	}
	
	 @Override
	    public void setMenuVisibility(final boolean visible) {
	        super.setMenuVisibility(visible);
	        if (visible) {

				GPSTracker gps = new GPSTracker(getActivity());
				if (gps.canGetLocation()) { // gps enabled} // return boolean
											// true/false

					gps.getLatitude(); // returns latitude
					gps.getLongitude(); // returns longitude

					Utility.setCurrentLatitude(getActivity(),
							Constants.SHAREDPREFERENCE_CURRENT_LATITUDE,
							String.valueOf(gps.getLatitude()));
					Utility.setCurrentLongitude(getActivity(),
							Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE,
							String.valueOf(gps.getLongitude()));
				}else{
					showTwoButtonDialog(
							getActivity(),
							"Gift Store",
							"Turn on your location service on the phone to locate the nearest gift store.",
							Constants.DIALOG_CLOSE_LOCATION);
				}
	        }
	    }

	@Override
	public void onConnected(Bundle arg0) {
		// TODO Auto-generated method stub
		displayLocation();
		GPSTracker gps = new GPSTracker(getActivity());
		if (gps.canGetLocation()) { // gps enabled} // return boolean true/false

			gps.getLatitude(); // returns latitude
			gps.getLongitude(); // returns longitude

			Utility.setCurrentLatitude(getActivity(),
					Constants.SHAREDPREFERENCE_CURRENT_LATITUDE,
					String.valueOf(gps.getLatitude()));
			Utility.setCurrentLongitude(getActivity(),
					Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE,
					String.valueOf(gps.getLongitude()));
		}
	}

	@Override
	public void onConnectionSuspended(int arg0) {
		// TODO Auto-generated method stub
		mGoogleApiClient.connect();
	}
	
	
	public void showTwoButtonDialog(final Context context, String Title,
			String message, final int Type) {
		try {

			final Dialog dialog = new Dialog(context);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			// dialog.getWindow().setBackgroundDrawableResource(
			// android.R.color.transparent);
			dialog.setContentView(R.layout.dialog_two_button);
			dialog.setCancelable(false);
			dialog.setCanceledOnTouchOutside(false);
			// dialog.setTitle(null);

			Typeface Reguler = Typeface.createFromAsset(context.getAssets(),
					"Roboto-Regular.ttf");

			Typeface Medium = Typeface.createFromAsset(context.getAssets(),
					"Roboto-Regular.ttf");

			// set the custom dialog components - text, image and button
			TextView textView_Title = (TextView) dialog
					.findViewById(R.id.textView_Title);
			textView_Title.setText(Title);
			textView_Title.setTypeface(Reguler);

			TextView textView_Message = (TextView) dialog
					.findViewById(R.id.textView_Message);
			textView_Message.setText(message);
			textView_Message.setTypeface(Medium);

			Button button_Ok = (Button) dialog.findViewById(R.id.button_Ok);
			button_Ok.setTypeface(Reguler);
			// if button is clicked, close the custom dialog
			button_Ok.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();

					Intent viewIntent = new Intent(
							Settings.ACTION_LOCATION_SOURCE_SETTINGS);
					context.startActivity(viewIntent);
				}
			});

			Button button_Cancel = (Button) dialog
					.findViewById(R.id.button_Cancel);
			button_Cancel.setTypeface(Reguler);
			// if button is clicked, close the custom dialog
			button_Cancel.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {dialog.dismiss();}
			});

			button_Ok.setText("Ok");
			button_Cancel.setText("Not now");

			dialog.show();

		} catch (Exception exception) {
		}
	}

	
	/*public class CustomComparator implements Comparator<FreeBies> {
	    @Override
	    public int compare(FreeBies o1, FreeBies o2) {
	        return o1.().compareTo(o2.getStartDate());
	    }
	}
	*/
}
