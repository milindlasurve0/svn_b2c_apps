<?php

class Store extends CI_Controller
{
    public function index()
    {
//      $this->load->view('store/index');
        $this->load->view('categories/index');
    }
    
    public function nearyou()
    {
        $data['sidebarCategories']=parent::setSideBarCategories();
         
        $this->load->view('store/nearyou',$data);
    }
    
    public function featuredoffer()
    {
        $this->load->view('store/featuredoffer');
    }
    
//    public function featuredoffers($id)
//    {
//        $data['sidebarCategories']=parent::setSideBarCategories();
//         
//        $url=$this->config->item('api_domain')."index.php/api_new/action/api/true/actiontype/GetGiftsByType_WEB/?type=gift&type_id={$id}";
//        
//        $data['response']=json_decode($this->curl->get($url));
//        
//        $this->load->view('store/featuredoffer',$data);
//    }
    
    public function topoffers($id)
    {
        $data['sidebarCategories']=parent::setSideBarCategories();
         
        $url=$this->config->item('api_domain')."index.php/api_new/action/api/true/actiontype/GetGiftsByType_WEB/?type=gift&type_id={$id}";
        
        $data['response']=json_decode($this->curl->get($url));
        
        $this->load->view('store/topoffer',$data);
        
    }
    
    public function popular($id)
    {
         $data['sidebarCategories']=parent::setSideBarCategories();
         
        $url=$this->config->item('api_domain')."index.php/api_new/action/api/true/actiontype/GetGiftsByType_WEB/?type=gift&type_id={$id}";
        
        $data['response']=json_decode($this->curl->get($url));
        
        $this->load->view('store/popular',$data);
    }
    
    public function recommendedforyou($id)
    {
   
          $data['sidebarCategories']=parent::setSideBarCategories();
         
        $url=$this->config->item('api_domain')."index.php/api_new/action/api/true/actiontype/GetGiftsByType_WEB/?type=gift&type_id={$id}";
        
        $data['response']=json_decode($this->curl->get($url));
        
        $this->load->view('store/recommendedforyou',$data);
    }
    
    public function offeroftheday($id)
    {
       $data['sidebarCategories']=parent::setSideBarCategories();
         
        $url=$this->config->item('api_domain')."index.php/api_new/action/api/true/actiontype/GetGiftsByType_WEB/?type=gift&type_id={$id}";
        
        $data['response']=json_decode($this->curl->get($url));
        
        $this->load->view('store/offeroftheday',$data);
    }
    
}

