package com.pay1;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.SearchManager;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.SQLException;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.larvalabs.svgandroid.SVGParser;
import com.pay1.adapterhandler.GiftGridAdapter;
import com.pay1.databasehandler.FreeBies;
import com.pay1.databasehandler.FreeBiesDataSource;

public class GiftSearchActivity extends Activity {

	private static final String SCREEN_LABEL = "Gift Category Screen";
	private EasyTracker easyTracker = null;

	private ImageView imageView_Back;
	private TextView textView_Title, textView_NoData;
	private GridView gridView_Gifts;
	private GiftGridAdapter adapter;
	private ArrayList<FreeBies> data;
	private FreeBiesDataSource freeBiesDataSource;
	int typeID = 0, page = 0;

	private int mVisibleThreshold = 5;
	private int mCurrentPage = 0;
	private int mPreviousTotal = 0;
	private boolean mLoading = true;
	private boolean mLastPage = false;
	int fromView = 0;

	@SuppressLint("NewApi")
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.gift_grid_activity);
		try {
			easyTracker = EasyTracker.getInstance(GiftSearchActivity.this);
			easyTracker.set(Fields.SCREEN_NAME, SCREEN_LABEL);
			freeBiesDataSource = new FreeBiesDataSource(GiftSearchActivity.this);

			Typeface Medium = Typeface.createFromAsset(getAssets(),
					"Roboto-Regular.ttf");

			textView_Title = (TextView) findViewById(R.id.textView_Title);
			textView_Title.setTypeface(Medium);
			textView_Title.setText("Search");
			textView_NoData = (TextView) findViewById(R.id.textView_NoData);
			textView_NoData.setTypeface(Medium);
			textView_NoData.setVisibility(View.VISIBLE);

			imageView_Back = (ImageView) findViewById(R.id.imageView_Back);
			imageView_Back.setImageDrawable(SVGParser.getSVGFromResource(
					getResources(), R.raw.ic_back_old).createPictureDrawable());
			if (Build.VERSION.SDK_INT >= 11)
				imageView_Back.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
			imageView_Back.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onBackPressed();
				}
			});

			RelativeLayout back_layout = (RelativeLayout) findViewById(R.id.back_layout);
			back_layout.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onBackPressed();
				}
			});
			data = new ArrayList<FreeBies>();
			gridView_Gifts = (GridView) findViewById(R.id.gridView_Gifts);
			gridView_Gifts.setEmptyView(textView_NoData);
			adapter = new GiftGridAdapter(GiftSearchActivity.this, data, false,
					false, false);
			// items.clear();
			gridView_Gifts.setAdapter(adapter);
			gridView_Gifts
					.setOnItemClickListener(new AdapterView.OnItemClickListener() {

						@Override
						public void onItemClick(AdapterView<?> parent,
								View view, int position, long id) {
							// TODO Auto-generated method stub
							try {
								FreeBies freeBie = data.get(position);
								Intent intent = new Intent(
										GiftSearchActivity.this,
										MyFreeBiesDetailsActivity.class);
								intent.putExtra("GIFT", freeBie);
								intent.putExtra("IS_BUY", 0);
								startActivity(intent);
							} catch (Exception e) {
							}
						}
					});

		} catch (Exception e) {
		}

		try {

			freeBiesDataSource.open();

			List<FreeBies> list = freeBiesDataSource
					.getAllSearchFreeBies(getIntent().getStringExtra("QUERY"));
			int len = list.size();
			if (len != 0) {
				for (FreeBies freebie : list)
					data.add(freebie);
			} else {
				// new FreeBiesTask()
				// .execute(
				// Utility.getCurrentLatitude(
				// getActivity(),
				// Constants.SHAREDPREFERENCE_CURRENT_LATITUDE),
				// Utility.getCurrentLongitude(
				// getActivity(),
				// Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE));
				// pager.setVisibility(View.GONE);

			}
			GiftSearchActivity.this.runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated
					// method stub
					adapter.notifyDataSetChanged();
				}
			});

		} catch (SQLException exception) {
			// TODO: handle exception
			// Log.e("Er ", exception.getMessage());
		} catch (Exception e) {
			// TODO: handle exception
			// Log.e("Er ", e.getMessage());
		} finally {
			freeBiesDataSource.close();
		}
		// handleIntent(getIntent());
		overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}

	@Override
	protected void onNewIntent(Intent intent) {
		setIntent(intent);
		handleIntent(intent);
	}

	/**
	 * Handling intent data
	 */
	private void handleIntent(Intent intent) {
		if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
			String query = intent.getStringExtra(SearchManager.QUERY);

			/**
			 * Use this query to display search results like 1. Getting the data
			 * from SQLite and showing in listview 2. Making webrequest and
			 * displaying the data For now we just display the query only
			 */
			try {

				freeBiesDataSource.open();

				List<FreeBies> list = freeBiesDataSource
						.getAllSearchFreeBies(query);
				int len = list.size();
				if (len != 0) {
					for (FreeBies freebie : list)
						data.add(freebie);
				} else {
					// new FreeBiesTask()
					// .execute(
					// Utility.getCurrentLatitude(
					// getActivity(),
					// Constants.SHAREDPREFERENCE_CURRENT_LATITUDE),
					// Utility.getCurrentLongitude(
					// getActivity(),
					// Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE));
					// pager.setVisibility(View.GONE);

				}
				GiftSearchActivity.this.runOnUiThread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated
						// method stub
						adapter.notifyDataSetChanged();
					}
				});

			} catch (SQLException exception) {
				// TODO: handle exception
				// Log.e("Er ", exception.getMessage());
			} catch (Exception e) {
				// TODO: handle exception
				// Log.e("Er ", e.getMessage());
			} finally {
				freeBiesDataSource.close();
			}

		}

	}

	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView().build());
		// EasyTracker.getInstance(this).activityStart(this);
		easyTracker.send(MapBuilder.createAppView().build());
	}

	@Override
	public void onStop() {
		super.onStop();
		// EasyTracker.getInstance(this).activityStop(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
	}

}