//
//  AllDealList.h
//  Pay1
//
//  Created by Annapurna on 09/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AllDealList : NSObject
@property (nonatomic, assign) int dealID;
@property (nonatomic, strong) NSString *dealName;
@property (nonatomic, strong) NSString *dealImgURL;
@property (nonatomic, strong) NSString *dealLogoURL;
@property (nonatomic, strong) NSString *dealCat;
@property (nonatomic, assign) int dealCatID;
@property (nonatomic, assign) int dealMin;
@property (nonatomic, assign) int dealOfferID;
@property (nonatomic, strong) NSString *dealOfferName;
@property (nonatomic,assign)  NSString * dealValid;
@property (nonatomic, strong) NSString *dealDescription;
@property (nonatomic, strong) NSString *dealOfferDescription;
@property (nonatomic, strong) NSString *dealerContact;
@property (nonatomic, strong) NSString *dealOfferPrice;
@property (nonatomic, strong) NSString *dealByVoucher;

@property (nonatomic, strong) NSMutableArray *locationArrayList;
@end
