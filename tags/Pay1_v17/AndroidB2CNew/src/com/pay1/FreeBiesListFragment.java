package com.pay1;

import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.WeakHashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.SQLException;
import android.graphics.Typeface;
import android.location.LocationManager;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.pay1.adapterhandler.FreeBiesPagerAdapter;
import com.pay1.constants.Constants;
import com.pay1.customviews.MyProgressDialog;
import com.pay1.databasehandler.FreeBies;
import com.pay1.databasehandler.FreeBiesDataSource;
import com.pay1.databasehandler.FreeBiesLocation;
import com.pay1.databasehandler.FreeBiesLocationDataSource;
import com.pay1.databasehandler.FreeBiesOrder;
import com.pay1.databasehandler.FreeBiesOrderDataSource;
import com.pay1.requesthandler.RequestClass;
import com.pay1.utilities.Utility;

public class FreeBiesListFragment extends Fragment {
	private static final String SCREEN_LABEL = "Freebies Screen";
	private EasyTracker easyTracker = null;
	public static final String TAG = "Freebies";

	private ArrayList<WeakHashMap<String, String>> data;
	private ArrayList<WeakHashMap<String, String>> data_location;
	private Set<Integer> deal_id_hash;

	private FreeBiesDataSource freeBiesDataSource;
	private FreeBiesLocationDataSource freeBiesLocationDataSource;
	private FreeBiesOrderDataSource freeBiesOrderDataSource;

	private ViewPager pager;
	public static FreeBiesPagerAdapter adapter;

	public static final String DEAL_ID = "id";
	public static final String DEAL_NAME = "dealname";
	public static final String URL = "img_url";
	public static final String CATAGORY = "category";
	public static final String CATAGORY_ID = "category_id";
	public static final String LATITUDE = "latitude";
	public static final String LONGITUDE = "longitude";
	public static final String ADDRESS = "address";
	public static final String AREA = "area";
	public static final String CITY = "city";
	public static final String STATE = "state";
	public static final String PIN = "pin";
	public static final String DISTANCE = "distance";
	public static final String LOCATION_COUNT = "location_count";
	public static final String OFFER_ID = "o_id";
	public static final String OFFER_NAME = "name";
	public static final String VALIDITY = "validity";
	public static final String SHORT_DESC = "offer_desc";
	public static final String MIN_AMOUNT = "min_amount";
	int page = 0;
	int current_page = 1;

	int selected_item = 0;
	int direction = 0;
	boolean rev_flag = false;

	// int selected_item = 0;

	public static FreeBiesListFragment newInstance() {
		FreeBiesListFragment fragment = new FreeBiesListFragment();
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// if ((savedInstanceState != null)
		// && savedInstanceState.containsKey(KEY_CONTENT)) {
		// mContent = savedInstanceState.getString(KEY_CONTENT);
		// }
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.freebies_list_fragment,
				container, false);
		try {

			freeBiesDataSource = new FreeBiesDataSource(getActivity());
			freeBiesLocationDataSource = new FreeBiesLocationDataSource(
					getActivity());
			freeBiesOrderDataSource = new FreeBiesOrderDataSource(getActivity());

			// imageView_Back = (ImageView) findViewById(R.id.imageView_Back);
			// imageView_Back.setImageDrawable(SVGParser.getSVGFromResource(
			// getResources(), R.raw.ic_back).createPictureDrawable());
			// if (Build.VERSION.SDK_INT >= 11)
			// imageView_Back.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
			// imageView_Back.setOnClickListener(new View.OnClickListener() {
			//
			// @Override
			// public void onClick(View v) {
			// // TODO Auto-generated method stub
			// onBackPressed();
			// }
			// });

			pager = (ViewPager) view.findViewById(R.id.pager);

			easyTracker = EasyTracker.getInstance(getActivity());
			easyTracker.set(Fields.SCREEN_NAME, SCREEN_LABEL);

			data = new ArrayList<WeakHashMap<String, String>>();
			data_location = new ArrayList<WeakHashMap<String, String>>();
			deal_id_hash = new HashSet<Integer>();

			adapter = new FreeBiesPagerAdapter(getActivity()
					.getSupportFragmentManager(), 0, 0, "", false, data,
					data_location);
			pager.setAdapter(adapter);
			pager.setVisibility(View.GONE);

			pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

				@Override
				public void onPageSelected(int arg0) {
					// TODO Auto-generated method stub
					selected_item = arg0;
					int c = pager.getCurrentItem();
					if (c > direction && c == (data.size() - 2)) {
						new FreeBiesTask().execute(
								Utility.getCurrentLatitude(
										getActivity(),
										Constants.SHAREDPREFERENCE_CURRENT_LATITUDE),
								Utility.getCurrentLongitude(
										getActivity(),
										Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE),
								Utility.getMobileNumber(getActivity(),
										Constants.SHAREDPREFERENCE_MOBILE),
								Utility.getSharedPreferences(getActivity(),
										Constants.LAST_GIFT_UPDATED));
					}
					direction = c;
				}

				@Override
				public void onPageScrolled(int arg0, float arg1, int arg2) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onPageScrollStateChanged(int arg0) {
					// TODO Auto-generated method stub

				}
			});

			// try {
			//
			// String lat = Utility.getCurrentLatitude(getActivity(),
			// Constants.SHAREDPREFERENCE_CURRENT_LATITUDE);
			// String lng = Utility.getCurrentLongitude(getActivity(),
			// Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE);
			// if (lat != null && lng != null) {
			// new DealTask().execute(lat, lng);
			// } else {
			// new DealTask().execute(Utility.getLatitude(
			// getActivity(),
			// Constants.SHAREDPREFERENCE_LATITUDE), Utility
			// .getLongitude(getActivity(),
			// Constants.SHAREDPREFERENCE_LONGITUDE));
			//
			// }
			// } catch (Exception exception) {
			// new DealTask().execute(null, null);
			// }

			try {
				freeBiesDataSource.open();
				freeBiesLocationDataSource.open();
				freeBiesOrderDataSource.open();

				FreeBiesOrder order = freeBiesOrderDataSource
						.getFreeBiesOrder();

				String temp = Utility.getSelectedGift(getActivity(),
						Constants.SHAREDPREFERENCE_SELECTED_GIFT);
				temp = temp != "" ? temp + "," : "";
				String od = order != null ? order.getOrderData() : "";
				if (!temp.equalsIgnoreCase("") && od.contains(temp))
					od = od.replace(temp, "");

				List<FreeBies> list = freeBiesDataSource
						.getAllFreeBies(order != null ? temp + od : temp + "");
				int len = list.size();
				if (len != 0) {

					data.clear();
					data_location.clear();

					for (FreeBies freebie : list) {

						int id = freebie.getFreebieDealID();
						if (!deal_id_hash.contains(id)) {
							WeakHashMap<String, String> map = new WeakHashMap<String, String>();
							deal_id_hash.add(id);
							map.put(DEAL_ID,
									String.valueOf(freebie.getFreebieDealID()));
							map.put(DEAL_NAME, freebie.getFreebieDealName());
							map.put(CATAGORY, freebie.getFreebieCategory());
							map.put(CATAGORY_ID, String.valueOf(freebie
									.getFreebieCategoryID()));

							map.put(URL, freebie.getFreebieURL());

							map.put(OFFER_ID,
									String.valueOf(freebie.getFreebieOfferID()));
							map.put(OFFER_NAME, freebie.getFreebieOfferName());
							map.put(SHORT_DESC, freebie.getFreebieShortDesc());
							map.put(MIN_AMOUNT, String.valueOf(freebie
									.getFreebieMinAmount()));
							map.put(VALIDITY, freebie.getFreebieValidity());
							map.put(LOCATION_COUNT, String.valueOf(freebie
									.getFreebieLocationCount()));

							List<FreeBiesLocation> locations = freeBiesLocationDataSource
									.getAllFreeBiesLocation(freebie
											.getFreebieOfferID());

							int j=0;
							for (FreeBiesLocation location : locations) {

								WeakHashMap<String, String> map1 = new WeakHashMap<String, String>();
								map1.put(DEAL_ID, String.valueOf(location.getLocationDealID()));
								double latitude = location.getLocationLat();
								double longitude = location.getLocationLng();
								map1.put(LATITUDE, String.valueOf(latitude));
								map1.put(LONGITUDE, String.valueOf(longitude));
								map1.put(ADDRESS, location.getLocationAddress());
								map1.put(CITY, location.getLocationCity());
								map1.put(STATE, location.getLocationState());
								String lat = Utility
										.getCurrentLatitude(
												getActivity(),
												Constants.SHAREDPREFERENCE_CURRENT_LATITUDE);
								String lng = Utility
										.getCurrentLongitude(
												getActivity(),
												Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE);
								String dis = "0";
								if (lat != "" && lng != "")
									dis = String.valueOf(Constants
											.distanceFrom(
													Double.parseDouble(lat),
													Double.parseDouble(lng),
													latitude, longitude));
								map1.put(DISTANCE, dis);

								data_location.add(map1);

								if (j == 0) {
									map.put(ADDRESS,
											location.getLocationAddress());
									map.put(DISTANCE, dis);
									++j;
								}
							}
							data.add(map);
							//
							// if (i == 2)
							// break;
						}
					}
					if (data.size() == 0) {
						pager.setVisibility(View.GONE);
					} else {
						// Collections.sort(data,
						// new Comparator<WeakHashMap<String, String>>() {
						//
						// @Override
						// public int compare(
						// WeakHashMap<String, String> lhs,
						// WeakHashMap<String, String> rhs) {
						// // TODO Auto-generated
						// // method stub
						// int l, r;
						// try {
						// l = Integer.parseInt(lhs
						//
						// .get(FreeBiesListFragment.MIN_AMOUNT));
						// r = Integer.parseInt(rhs
						// .get(FreeBiesListFragment.MIN_AMOUNT));
						// } catch (Exception e) {
						// l = 0;
						// r = 0;
						// }
						// return Integer.valueOf(l).compareTo(
						// Integer.valueOf(r));
						// }
						// });
						int cc = freeBiesDataSource.getAllFreeBiesCount("", "");
						adapter = new FreeBiesPagerAdapter(getActivity()
								.getSupportFragmentManager(), cc, 0, "", false,
								data, data_location);
						pager.setAdapter(adapter);
						pager.setVisibility(View.VISIBLE);
					}

				} else {
					// new FreeBiesTask()
					// .execute(
					// Utility.getCurrentLatitude(
					// getActivity(),
					// Constants.SHAREDPREFERENCE_CURRENT_LATITUDE),
					// Utility.getCurrentLongitude(
					// getActivity(),
					// Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE));
					pager.setVisibility(View.GONE);

				}
			} catch (SQLException exception) {
				// TODO: handle exception
				// // // Log.e("Er ", exception.getMessage());
			} catch (Exception e) {
				// TODO: handle exception
				Log.e("Er ", e.getMessage());
			} finally {
				freeBiesDataSource.close();
				freeBiesLocationDataSource.close();
				freeBiesOrderDataSource.close();
			}
			new FreeBiesTask().execute(
					Utility.getCurrentLatitude(getActivity(),
							Constants.SHAREDPREFERENCE_CURRENT_LATITUDE),
					Utility.getCurrentLongitude(getActivity(),
							Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE),
					Utility.getMobileNumber(getActivity(),
							Constants.SHAREDPREFERENCE_MOBILE), Utility
							.getSharedPreferences(getActivity(),
									Constants.LAST_GIFT_UPDATED));

			if (!isLocationOn()) {
				showTwoButtonDialog(
						getActivity(),
						"Free gifts",
						"Turn on your location service on the phone to locate nearby free gifts.",
						Constants.DIALOG_CLOSE_LOCATION);
			}
		} catch (Exception e) {
			// System.out.println(e.getMessage());
		}
		return view;
	}

	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView().build());
		// EasyTracker.getInstance(this).activityStart(this);
		easyTracker.send(MapBuilder.createAppView().build());
	}

	@Override
	public void onStop() {
		super.onStop();
		// EasyTracker.getInstance(this).activityStop(this);
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		// outState.putString(KEY_CONTENT, mContent);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.Fragment#onDestroy()
	 */
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		// Intent intent = new Intent(getActivity(), MainActivity.class);
		// intent.putExtra("FROM_QUICKPAY", false);
		// intent.putExtra("IS_GIFT", false);
		// intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
		// | Intent.FLAG_ACTIVITY_NEW_TASK);
		// getActivity().startActivity(intent);
		// getActivity().finish();
		try {
			data.clear();
			data_location.clear();
			pager.setAdapter(null);
		} catch (Exception e) {
		}
	}

	private boolean isLocationOn() {

		try {

			boolean gps_enabled = false;
			boolean network_enabled = false;
			LocationManager lm = (LocationManager) getActivity()
					.getSystemService(Context.LOCATION_SERVICE);
			try {
				gps_enabled = lm
						.isProviderEnabled(LocationManager.GPS_PROVIDER);
			} catch (Exception ex) {
			}
			try {
				network_enabled = lm
						.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
			} catch (Exception ex) {
			}

			// don't start listeners if no provider is
			// enabled
			boolean hasGPS = getActivity().getPackageManager()
					.hasSystemFeature(PackageManager.FEATURE_LOCATION_GPS);
			WifiManager wifi = (WifiManager) getActivity().getSystemService(
					Context.WIFI_SERVICE);
			if (!(network_enabled && wifi.isWifiEnabled())
					&& (hasGPS && !gps_enabled)) {
				return false;
			} else if (gps_enabled || network_enabled) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return true;
		}

	}

	public void showTwoButtonDialog(final Context context, String Title,
			String message, final int Type) {
		try {

			final Dialog dialog = new Dialog(context);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			// dialog.getWindow().setBackgroundDrawableResource(
			// android.R.color.transparent);
			dialog.setContentView(R.layout.dialog_two_button);
			dialog.setCancelable(false);
			dialog.setCanceledOnTouchOutside(false);
			// dialog.setTitle(null);

			Typeface Reguler = Typeface.createFromAsset(context.getAssets(),
					"EncodeSansNormal-400-Regular.ttf");

			Typeface Medium = Typeface.createFromAsset(context.getAssets(),
					"EncodeSansNarrow-500-Medium.ttf");

			// set the custom dialog components - text, image and button
			TextView textView_Title = (TextView) dialog
					.findViewById(R.id.textView_Title);
			textView_Title.setText(Title);
			textView_Title.setTypeface(Reguler);

			TextView textView_Message = (TextView) dialog
					.findViewById(R.id.textView_Message);
			textView_Message.setText(message);
			textView_Message.setTypeface(Medium);

			Button button_Ok = (Button) dialog.findViewById(R.id.button_Ok);
			button_Ok.setTypeface(Reguler);
			// if button is clicked, close the custom dialog
			button_Ok.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
					Utility.setBackRequired(getActivity(),
							Constants.SHAREDPREFERENCE_BACK_LOCATION, true);
					Intent viewIntent = new Intent(
							Settings.ACTION_LOCATION_SOURCE_SETTINGS);
					context.startActivity(viewIntent);
				}
			});

			Button button_Cancel = (Button) dialog
					.findViewById(R.id.button_Cancel);
			button_Cancel.setTypeface(Reguler);
			// if button is clicked, close the custom dialog
			button_Cancel.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
				}
			});

			button_Ok.setText("Ok");
			button_Cancel.setText("Not now");

			dialog.show();

		} catch (Exception exception) {
		}
	}

	public class FreeBiesTask extends AsyncTask<String, String, String>
			implements OnDismissListener {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			try {

				String time = params[3] == null ? "" : params[3];

				if (time != "") {
					try {
						DateFormat dateFormat = new SimpleDateFormat(
								"yyyy-MM-dd HH:mm:ss");
						Date date = new Date(Long.parseLong(time));
						time = dateFormat.format(date);
					} catch (Exception e) {
					}
				}
				String response = RequestClass.getInstance()
						.readPay1B2CRequest(
								getActivity(),
								Constants.B2C_URL + "GetupdatedDeal/?latitude="
										+ params[0] + "&longitude=" + params[1]
										+ "&user_mobile=" + params[2]
										+ "&updatedTime="
										+ URLEncoder.encode(time, "utf-8")
										+ "&next=" + page);

				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				if (this.dialog.isShowing()) {
					this.dialog.dismiss();
				}
			} catch (Exception e) {
			}
			boolean flag = false;
			try {
				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");
					JSONObject jsonObject = new JSONObject(replaced);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {

						try {

							freeBiesDataSource.open();
							freeBiesLocationDataSource.open();
							freeBiesOrderDataSource.open();

							JSONObject jsonObjectDesc = jsonObject
									.getJSONObject("description");
							freeBiesOrderDataSource.deleteFreeBiesOrder();
							String od = jsonObjectDesc
									.getString("offer_details");

							if (!od.equalsIgnoreCase("")) {
								freeBiesOrderDataSource.createFreeBiesOrder(
										(page + 1), od);
								flag = true;
							} else
								flag = false;

							Utility.setOrder(getActivity(),
									Constants.SHAREDPREFERENCE_GIFT,
									jsonObjectDesc.getString("claimedGifts"));
							Utility.setOrder(getActivity(),
									Constants.SHAREDPREFERENCE_GIFT_EXPIRED,
									jsonObjectDesc.getString("ExpiredOffers"));

							try {
								freeBiesDataSource
										.deleteFreeBies(jsonObjectDesc
												.getString("ExpiredOffers"));
								freeBiesLocationDataSource
										.deleteFreeBiesLocation(jsonObjectDesc
												.getString("ExpiredOffers"));
							} catch (Exception e) {

							}

							JSONArray jsonArrayAlldeals = jsonObjectDesc
									.getJSONArray("Alldeals");
							long timestamp = System.currentTimeMillis();

							freeBiesDataSource.createFreeBies(
									jsonArrayAlldeals, timestamp);
							// for (int i = 0; i < jsonArrayAlldeals.length();
							// i++) {
							// String deal_id = jsonArrayAlldeals
							// .getJSONObject(i).getString("id");
							// String offer_id = jsonArrayAlldeals
							// .getJSONObject(i).getString("of_id");
							// try {
							// freeBiesDataSource.deleteFreeBies(Integer
							// .parseInt(deal_id));
							// freeBiesLocationDataSource
							// .deleteFreeBiesLocation(Integer
							// .parseInt(offer_id));
							// } catch (Exception e) {
							// continue;
							// }
							//
							// String deal_name = jsonArrayAlldeals
							// .getJSONObject(i).getString("name");
							// String url = jsonArrayAlldeals.getJSONObject(i)
							// .getString("i_url");
							// String catagory = jsonArrayAlldeals
							// .getJSONObject(i).getString("cat");
							// String catagory_id = jsonArrayAlldeals
							// .getJSONObject(i).getString("cat_id");
							// String min_amt = jsonArrayAlldeals
							// .getJSONObject(i).getString("min");
							// String offer_name = jsonArrayAlldeals
							// .getJSONObject(i).getString("of_name");
							// String validity = jsonArrayAlldeals
							// .getJSONObject(i).getString("valid");
							// String desc = jsonArrayAlldeals
							// .getJSONObject(i).getString("desc");
							// String offer_lat = "0", offer_lng = "0",
							// offer_add = "", offer_area = "", offer_city = "",
							// offer_state = "";
							//
							// JSONArray jsonArrayLocation = jsonArrayAlldeals
							// .getJSONObject(i).getJSONArray("locs");
							//
							// for (int j = 0; j < jsonArrayLocation.length();
							// j++) {
							// JSONObject jsonObjectInner = jsonArrayLocation
							// .getJSONObject(j);
							// WeakHashMap<String, String> map1 = new
							// WeakHashMap<String, String>();
							//
							// String loc_lat = jsonObjectInner
							// .getString("lat");
							// String loc_lng = jsonObjectInner
							// .getString("lng");
							// String loc_add = jsonObjectInner
							// .getString("addr");
							// String loc_area = jsonObjectInner
							// .getString("area");
							// String loc_city = jsonObjectInner
							// .getString("city");
							// String loc_state = jsonObjectInner
							// .getString("state");
							//
							// if (j == 0) {
							// offer_lat = loc_lat;
							// offer_lng = loc_lng;
							// offer_add = loc_add;
							// offer_area = loc_area;
							// offer_city = loc_city;
							// offer_state = loc_state;
							// }
							//
							// freeBiesLocationDataSource
							// .createFreeBiesLocation(
							// Integer.parseInt(offer_id),
							// Double.parseDouble(loc_lat),
							// Double.parseDouble(loc_lng),
							// loc_add, loc_area,
							// loc_city, loc_state, 0, 0,
							// timestamp);
							// }
							// freeBiesDataSource.createFreeBies(
							// Integer.parseInt(deal_id), deal_name,
							// url, catagory,
							// Integer.parseInt(catagory_id),
							// Double.parseDouble(offer_lat),
							// Double.parseDouble(offer_lng),
							// offer_add, offer_area, offer_city,
							// offer_state, 0,
							// jsonArrayLocation.length(),
							// Integer.parseInt(offer_id), offer_name,
							// validity, desc,
							// Integer.parseInt(min_amt), timestamp);
							// }

							Utility.setSharedPreferences(getActivity(),
									Constants.LAST_GIFT_UPDATED,
									String.valueOf(System.currentTimeMillis()));
						} catch (SQLException exception) {
							// TODO: handle exception
							// Log.e("Er ", exception.getMessage());
						} catch (Exception e) {
							// TODO: handle exception
							// Log.e("Er ", e.getMessage());
						} finally {
							freeBiesDataSource.close();
							freeBiesLocationDataSource.close();
							freeBiesOrderDataSource.close();
						}

					}
				}

				if (flag) {
					try {
						freeBiesDataSource.open();
						freeBiesLocationDataSource.open();
						freeBiesOrderDataSource.open();

						FreeBiesOrder order = freeBiesOrderDataSource
								.getFreeBiesOrder((page + 1));

						String temp = Utility.getSelectedGift(getActivity(),
								Constants.SHAREDPREFERENCE_SELECTED_GIFT);
						temp = temp != "" ? temp + "," : "";
						String od = order != null ? order.getOrderData() : "";
						if (!temp.equalsIgnoreCase("") && od.contains(temp))
							od = od.replace(temp, "");

						List<FreeBies> freebies = freeBiesDataSource
								.getAllFreeBies(order != null ? temp + od
										: temp + "");
						Utility.setSelectedGift(getActivity(),
								Constants.SHAREDPREFERENCE_SELECTED_GIFT, "");

						int len = freebies.size();
						if (len != 0) {

							if (page == 0) {
								data.clear();
								data_location.clear();
								deal_id_hash.clear();
							}
							++page;
							for (FreeBies freebie : freebies) {

								// if
								// (jsonArrayDesc.getJSONObject(i).has("allow"))
								// {
								// if (jsonArrayDesc.getJSONObject(i)
								// .getString("allow")
								// .equalsIgnoreCase("0"))
								// continue;
								// }
								// if (recharge_amount < Integer
								// .parseInt(jsonObjectOffer
								// .getString("min_amount"))) {
								// // textView_Title.setText("");
								// textView_NoData
								// .setText("You missed out on a free gift! Recharge of minimum Rs. "
								// + jsonObjectOffer
								// .getString("min_amount")
								// + " and win a free gift.");
								// continue;
								// }

								int id = freebie.getFreebieDealID();
								if (!deal_id_hash.contains(id)) {
									deal_id_hash.add(id);
									WeakHashMap<String, String> map = new WeakHashMap<String, String>();
									map.put(DEAL_ID, String.valueOf(id));
									map.put(DEAL_NAME,
											freebie.getFreebieDealName());
									map.put(CATAGORY,
											freebie.getFreebieCategory());
									map.put(CATAGORY_ID, String.valueOf(freebie
											.getFreebieCategoryID()));
									// map.put(LATITUDE,
									// jsonArrayDesc.getJSONObject(i)
									// .getString("latitude"));
									// map.put(LONGITUDE,
									// jsonArrayDesc.getJSONObject(i)
									// .getString("longitude"));

									map.put(URL, freebie.getFreebieURL());

									map.put(OFFER_ID, String.valueOf(freebie
											.getFreebieOfferID()));
									map.put(OFFER_NAME,
											freebie.getFreebieOfferName());
									map.put(SHORT_DESC,
											freebie.getFreebieShortDesc());
									map.put(MIN_AMOUNT, String.valueOf(freebie
											.getFreebieMinAmount()));
									map.put(VALIDITY,
											freebie.getFreebieValidity());
									map.put(LOCATION_COUNT, String
											.valueOf(freebie
													.getFreebieLocationCount()));

									List<FreeBiesLocation> locations = freeBiesLocationDataSource
											.getAllFreeBiesLocation(freebie
													.getFreebieOfferID());

									int j = 0;
									for (FreeBiesLocation location : locations) {

										WeakHashMap<String, String> map1 = new WeakHashMap<String, String>();
										map1.put(DEAL_ID, String
												.valueOf(location
														.getLocationDealID()));
										double latitude = location
												.getLocationLat();
										double longitude = location
												.getLocationLng();
										map1.put(LATITUDE,
												String.valueOf(latitude));
										map1.put(LONGITUDE,
												String.valueOf(longitude));
										map1.put(ADDRESS,
												location.getLocationAddress());
										map1.put(CITY,
												location.getLocationCity());
										map1.put(STATE,
												location.getLocationState());
										String lat = Utility
												.getCurrentLatitude(
														getActivity(),
														Constants.SHAREDPREFERENCE_CURRENT_LATITUDE);
										String lng = Utility
												.getCurrentLongitude(
														getActivity(),
														Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE);
										String dis = "0";
										if (lat != "" && lng != "")
											dis = String
													.valueOf(Constants.distanceFrom(
															Double.parseDouble(lat),
															Double.parseDouble(lng),
															latitude, longitude));
										map1.put(DISTANCE, dis);

										data_location.add(map1);

										if (j == 0) {
											map.put(ADDRESS, location
													.getLocationAddress());
											map.put(DISTANCE, dis);
											++j;
										}
									}
									data.add(map);
								}
								//
								// if (i == 2)
								// break;
							}
							if (data.size() == 0) {
								pager.setVisibility(View.GONE);
							} else {
								// Collections.sort(data,
								// new Comparator<WeakHashMap<String, String>>()
								// {
								//
								// @Override
								// public int compare(
								// WeakHashMap<String, String> lhs,
								// WeakHashMap<String, String> rhs) {
								// // TODO Auto-generated
								// // method stub
								// int l, r;
								// try {
								// l = Integer.parseInt(lhs
								//
								// .get(FreeBiesListFragment.MIN_AMOUNT));
								// r = Integer.parseInt(rhs
								// .get(FreeBiesListFragment.MIN_AMOUNT));
								// } catch (Exception e) {
								// l = 0;
								// r = 0;
								// }
								// return Integer.valueOf(l).compareTo(
								// Integer.valueOf(r));
								// }
								// });

								int cc = freeBiesDataSource
										.getAllFreeBiesCount(
												Utility.getOrder(
														getActivity(),
														Constants.SHAREDPREFERENCE_GIFT_EXPIRED),
												"");
								adapter = new FreeBiesPagerAdapter(
										getActivity()
												.getSupportFragmentManager(),
										cc, 0, "", false, data, data_location);
								pager.setAdapter(adapter);
								pager.setVisibility(View.VISIBLE);

							}

						} else {
							// new FreeBiesTask()
							// .execute(
							// Utility.getCurrentLatitude(
							// getActivity(),
							// Constants.SHAREDPREFERENCE_CURRENT_LATITUDE),
							// Utility.getCurrentLongitude(
							// getActivity(),
							// Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE));
							pager.setVisibility(View.GONE);

						}
					} catch (SQLException exception) {
						// TODO: handle exception
						// // // Log.e("Er ", exception.getMessage());
					} catch (Exception e) {
						// TODO: handle exception
						// // // Log.e("Er ", e.getMessage());
					} finally {
						freeBiesDataSource.close();
						freeBiesLocationDataSource.close();
						freeBiesOrderDataSource.close();
					}
				} else {
					adapter = new FreeBiesPagerAdapter(getActivity()
							.getSupportFragmentManager(), data.size(), 0, "",
							false, data, data_location);
					pager.setAdapter(adapter);
					pager.setVisibility(View.VISIBLE);
				}
			} catch (JSONException e) {
				// e.printStackTrace();

			} catch (Exception e) {
				// TODO: handle exception
			}
			pager.setCurrentItem(selected_item);
		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(getActivity());
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							dialog.dismiss();
							FreeBiesTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			dialog.dismiss();
			FreeBiesTask.this.cancel(true);
		}
	}

	// public class FreeBiesTask extends AsyncTask<String, String, String>
	// implements OnDismissListener {
	// private MyProgressDialog dialog;
	//
	// @Override
	// protected String doInBackground(String... params) {
	// try {
	// String response = RequestClass
	// .getInstance()
	// .readPay1B2CRequest(
	// getActivity(),
	// Constants.B2C_URL
	// + "Get_all_deal_data/?latitude="
	// + params[0]
	// + "&longitude="
	// + params[1]
	// + "&mobile="
	// + Utility
	// .getMobileNumber(
	// getActivity(),
	// Constants.SHAREDPREFERENCE_MOBILE));
	//
	// return response;
	// } catch (Exception e) {
	// return "Error";
	// }
	// }
	//
	// @Override
	// protected void onPostExecute(String result) {
	// super.onPostExecute(result);
	// try {
	// if (this.dialog.isShowing()) {
	// this.dialog.dismiss();
	// }
	// } catch (Exception e) {
	// }
	// try {
	//
	// if (!result.startsWith("Error")) {
	// String replaced = result.replace("(", "").replace(")", "")
	// .replace(";", "");
	//
	// JSONObject jsonObject = new JSONObject(replaced);
	// String status = jsonObject.getString("status");
	// if (status.equalsIgnoreCase("success")) {
	//
	// try {
	//
	// freeBiesDataSource.open();
	// freeBiesLocationDataSource.open();
	//
	// freeBiesDataSource.deleteFreeBies();
	// freeBiesLocationDataSource.deleteFreeBiesLocation();
	//
	// data.clear();
	// data_location.clear();
	//
	// JSONArray jsonArrayDesc = jsonObject
	// .getJSONArray("description");
	//
	// long timestamp = System.currentTimeMillis();
	// for (int i = 0; i < jsonArrayDesc.length(); i++) {
	// WeakHashMap<String, String> map = new WeakHashMap<String, String>();
	//
	// JSONObject jsonObjectOffer = jsonArrayDesc
	// .getJSONObject(i).getJSONObject(
	// "offer_detail");
	//
	// // if
	// // (jsonArrayDesc.getJSONObject(i).has("allow"))
	// // {
	// // if (jsonArrayDesc.getJSONObject(i)
	// // .getString("allow")
	// // .equalsIgnoreCase("0"))
	// // continue;
	// // }
	// // if (recharge_amount < Integer
	// // .parseInt(jsonObjectOffer
	// // .getString("min_amount"))) {
	// // // textView_Title.setText("");
	// // textView_NoData
	// // .setText("You missed out on a free gift! Recharge of minimum Rs. "
	// // + jsonObjectOffer
	// // .getString("min_amount")
	// // + " and win a free gift.");
	// // continue;
	// // }
	// String deal_id = jsonArrayDesc.getJSONObject(i)
	// .getString("id");
	// String deal_name = jsonArrayDesc.getJSONObject(
	// i).getString("dealname");
	// String catagory = jsonArrayDesc
	// .getJSONObject(i).getString("category");
	// String catagory_id = jsonArrayDesc
	// .getJSONObject(i).getString(
	// "category_id");
	// String url = jsonArrayDesc.getJSONObject(i)
	// .getString("img_url");
	// String offer_id = jsonObjectOffer
	// .getString("id");
	// String allow = jsonObjectOffer
	// .getString("allow");
	// String max_qty = jsonObjectOffer
	// .getString("max_limit_peruser");
	// String offer_name = jsonObjectOffer
	// .getString("name");
	// String desc = jsonObjectOffer
	// .getString("short_desc");
	// String min_amt = jsonObjectOffer
	// .getString("min_amount");
	// String validity = jsonObjectOffer
	// .getString("validity");
	// String offer_status = jsonObjectOffer
	// .getString("status");
	// String loc_count = jsonArrayDesc.getJSONObject(
	// i).getString("location_count");
	// String offer_lat = "0", offer_lng = "0", offer_add = "", offer_city = "",
	// offer_state = "";
	// map.put(DEAL_ID, deal_id);
	// map.put(DEAL_NAME, deal_name);
	// map.put(CATAGORY, catagory);
	// map.put(CATAGORY_ID, catagory_id);
	// // map.put(LATITUDE,
	// // jsonArrayDesc.getJSONObject(i)
	// // .getString("latitude"));
	// // map.put(LONGITUDE,
	// // jsonArrayDesc.getJSONObject(i)
	// // .getString("longitude"));
	//
	// map.put(URL, url);
	// map.put(OFFER_ID, offer_id);
	// map.put(ALLOW, allow);
	// map.put(OFFER_NAME, offer_name);
	// map.put(SHORT_DESC, desc);
	// map.put(MIN_AMOUNT, min_amt);
	// map.put(VALIDITY, validity);
	// map.put(LOCATION_COUNT, loc_count);
	//
	// // JSONObject jsonObjectLocation = jsonArrayDesc
	// // .getJSONObject(i).getJSONObject(
	// // "location");
	//
	// JSONArray jsonArrayLocation = jsonArrayDesc
	// .getJSONObject(i).getJSONArray(
	// "location_detail");
	//
	// for (int j = 0; j < jsonArrayLocation.length(); j++) {
	// JSONObject jsonObjectInner = jsonArrayLocation
	// .getJSONObject(j);
	// WeakHashMap<String, String> map1 = new WeakHashMap<String, String>();
	//
	// String loc_lat = jsonObjectInner
	// .getString("lat");
	// String loc_lng = jsonObjectInner
	// .getString("lng");
	// String loc_add = jsonObjectInner
	// .getString("address");
	// String loc_city = jsonObjectInner
	// .getString("city");
	// String loc_state = jsonObjectInner
	// .getString("state");
	// String loc_distance = jsonObjectInner
	// .getString("distance");
	// map1.put(DEAL_ID, deal_id);
	// map1.put(LATITUDE, loc_lat);
	// map1.put(LONGITUDE, loc_lng);
	// map1.put(ADDRESS, loc_add);
	// map1.put(CITY, loc_city);
	// map1.put(STATE, loc_state);
	// map1.put(DISTANCE, loc_distance);
	//
	// data_location.add(map1);
	//
	// if (j == 0) {
	// offer_lat = loc_lat;
	// offer_lng = loc_lng;
	// offer_add = loc_add;
	// offer_city = loc_city;
	// offer_state = loc_state;
	//
	// map.put(ADDRESS, loc_add);
	// map.put(DISTANCE, loc_distance);
	// }
	//
	// freeBiesLocationDataSource
	// .createFreeBiesLocation(
	// Integer.parseInt(offer_id),
	// Double.parseDouble(loc_lat),
	// Double.parseDouble(loc_lng),
	// loc_add,
	// "",
	// loc_city,
	// loc_state,
	// 0,
	// Double.parseDouble(loc_distance),
	// timestamp);
	// }
	// data.add(map);
	// freeBiesDataSource.createFreeBies(
	// Integer.parseInt(deal_id), deal_name,
	// url, catagory,
	// Integer.parseInt(catagory_id),
	// Double.parseDouble(offer_lat),
	// Double.parseDouble(offer_lng),
	// offer_add, "", offer_city, offer_state,
	// 0, Integer.parseInt(loc_count),
	// Integer.parseInt(offer_id), offer_name,
	// validity,
	// Integer.parseInt(offer_status), desc,
	// Integer.parseInt(min_amt),
	// Integer.parseInt(max_qty),
	// Integer.parseInt(allow), timestamp);
	// }
	// if (data.size() == 0) {
	// pager.setVisibility(View.GONE);
	// } else {
	// Collections
	// .sort(data,
	// new Comparator<WeakHashMap<String, String>>() {
	//
	// @Override
	// public int compare(
	// WeakHashMap<String, String> lhs,
	// WeakHashMap<String, String> rhs) {
	// // TODO Auto-generated
	// // method stub
	// int l, r;
	// try {
	// l = Integer
	// .parseInt(lhs
	//
	// .get(FreeBiesListFragment.MIN_AMOUNT));
	// r = Integer
	// .parseInt(rhs
	// .get(FreeBiesListFragment.MIN_AMOUNT));
	// } catch (Exception e) {
	// l = 0;
	// r = 0;
	// }
	// return Integer
	// .valueOf(l)
	// .compareTo(
	// Integer.valueOf(r));
	// }
	// });
	// adapter = new FreeBiesPagerAdapter(
	// getActivity()
	// .getSupportFragmentManager(),
	// 0, "", false, data, data_location);
	// pager.setAdapter(adapter);
	// pager.setVisibility(View.VISIBLE);
	// }
	// } catch (SQLException exception) {
	// // TODO: handle exception
	// // Log.e("Er ", exception.getMessage());
	// } catch (Exception e) {
	// // TODO: handle exception
	// // Log.e("Er ", e.getMessage());
	// } finally {
	// freeBiesDataSource.close();
	// freeBiesLocationDataSource.close();
	// }
	// }
	// }
	// getActivity().runOnUiThread(new Runnable() {
	//
	// @Override
	// public void run() {
	// // TODO Auto-generated method stub
	// adapter.notifyDataSetChanged();
	// }
	// });
	//
	// pager.setVisibility(View.VISIBLE);
	// } catch (JSONException e) {
	// // e.printStackTrace();
	// } catch (Exception e) {
	// // TODO: handle exception
	// }
	// }
	//
	// protected void onPreExecute() {
	// dialog = new MyProgressDialog(getActivity());
	// // this.dialog.setMessage("Please wait.....");
	// this.dialog.setCancelable(true);
	// this.dialog
	// .setOnCancelListener(new DialogInterface.OnCancelListener() {
	//
	// @Override
	// public void onCancel(DialogInterface dialog) {
	// // TODO Auto-generated method stub
	// dialog.dismiss();
	// FreeBiesTask.this.cancel(true);
	// }
	// });
	// this.dialog.show();
	// super.onPreExecute();
	// }
	//
	// @Override
	// public void onDismiss(DialogInterface dialog) {
	// // TODO Auto-generated method stub
	// dialog.dismiss();
	// FreeBiesTask.this.cancel(true);
	// }
	// }

}