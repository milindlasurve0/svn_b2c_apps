﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pay1B2C.Model
    {
    class NumberAndCircle
        {
        public NumberAndCircle()
            {
            }
        public NumberAndCircle(string area_name1, string area1, string opr_name1, string operator1, string product_id1, string number1)
            {
            area_name = area_name1;
            area = area1;
            opr_name = opr_name1;
            numberOperator = operator1;
            number = number1;
            product_id = product_id1;

            }

        string _area_name;
        string _area;
        string _opr_name;
        string _operator;
        string _number;
        string _product_id;


        public string number
            {
            get { return _number; }
            set { _number = value; }
            }
        public string area_name
            {
            get { return _area_name; }
            set { _area_name = value; }
            }

        public string area
            {
            get { return _area; }
            set { _area = value; }
            }
        public string opr_name
            {
            get { return _opr_name; }
            set { _opr_name = value; }
            }

        public string numberOperator
            {
            get { return _operator; }
            set { _operator = value; }
            }

        public string product_id
            {
            get { return _product_id; }
            set { _product_id = value; }
            }

        }
    }