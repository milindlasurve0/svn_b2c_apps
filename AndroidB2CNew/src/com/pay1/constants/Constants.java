package com.pay1.constants;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Vibrator;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AlphaAnimation;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.larvalabs.svgandroid.SVGParser;
import com.pay1.FreeBiesActivity;
import com.pay1.LoginActivity;
import com.pay1.MainActivity;
import com.pay1.R;
import com.pay1.utilities.Utility;

public class Constants {

	public static final String SENDER_ID = "816530354148";

	// public static ArrayList<WeakHashMap<String, String>> alContacts;
	public static ArrayList<JSONObject> jsonContact;
	public static ArrayList<String> notificationList = new ArrayList<String>();

	public static final String IS_FIRST_TIME = "isFirstTime";

	public static int RECHARGE_MOBILE = 1;
	public static int RECHARGE_DTH = 2;
	public static int RECHARGE_DATA = 3;
	public static int BILL_PAYMENT = 4;
	public static int WALLET_PAYMENT = 5;
	public static int QUICK_PAY = 6;
	public static int SUBSCRIBE_DEAL = 7;
	public static int SUPPORT = 8;
	public static String RECHARGE_FOR = "rechargeFor";
	public static final String PLAN_JSON = "plan_json";
	public static final String DATABASE_INIT_TIME = "1429747200000";
	public static final String SYNC_DATABASE = "syncdatabase.db";
	// public static String URL =
	// "http://192.168.0.13:9193/index.php/api_new/action/api/true/actiontype/";
	public static String B2C_URL = "http://b2c.pay1.in/index.php/api_new/action/api/true/actiontype/";
	public static final String B2B_URL = "https://panel.pay1.in/apis/receiveWeb/mindsarray/mindsarray/json?";

	public static final String ERROR_INTERNET = "Error connecting to server. Please check your internet connection.";
	public static final String ERROR_SERVER = "Error connecting to pay1 server. Please try again.";
	public static final String ERROR_MESSAGE_FOR_ALL_REQUEST = "Something went wrong. Please try after sometime.";
	public static final String SHAREDPREFERENCE_IS_NEW = "is_new";
	public static final String SHAREDPREFERENCE_USER_ID = "User_ID";
	public static final String SHAREDPREFERENCE_FACEBOOK_ID = "facebook_id";
	public static final String SHAREDPREFERENCE_MOBILE = "MOBILE_NUMBER";
	public static final String SHAREDPREFERENCE_EMAIL = "email";
	public static final String SHAREDPREFERENCE_GENDER = "gender";
	public static final String SHAREDPREFERENCE_IS_LOGIN = "isLogin";
	public static final String SHAREDPREFERENCE_IS_LOGIN_GIFT = "isLoginGift";
	public static final String SHAREDPREFERENCE_BALANCE = "Balance";
	public static final String SHAREDPREFERENCE_LOYALTY_POINTS = "loyalty_points";

	public static final String SHAREDPREFERENCE_MY_REVIEWS = "total_myreviews";
	public static final String SHAREDPREFERENCE_MY_LIKES = "total_mylikes";
	public static final String SHAREDPREFERENCE_TOTAL_REDEEM = "total_redeem";
	public static final String SHAREDPREFERENCE_TOTAL_GIFTS = "total_gifts";

	public static final String SHAREDPREFERENCE_LATITUDE = "latitude";
	public static final String SHAREDPREFERENCE_LONGITUDE = "longitude";
	public static final String SHAREDPREFERENCE_CURRENT_LATITUDE = "clat";
	public static final String SHAREDPREFERENCE_CURRENT_LONGITUDE = "clng";
	public static final String SHAREDPREFERENCE_LAST_LATITUDE = "llat";
	public static final String SHAREDPREFERENCE_LAST_LONGITUDE = "llng";
	public static final String SHAREDPREFERENCE_GCMID = "gcm";
	public static final String SHAREDPREFERENCE_UUID = "uuid";
	public static final String SHAREDPREFERENCE_NAME = "User_Name";
	public static final String SHAREDPREFERENCE_DOB = "dob";
	public static final String SHAREDPREFERENCE_SERVICE_CHARGE_PERCENT = "service_charge_percent";
	public static final String SHAREDPREFERENCE_SERVICE_TAX_PERCENT = "service_tax_percent";
	public static final String SHAREDPREFERENCE_PIN = "pin";
	public static final String SHAREDPREFERENCE_ONE_TIME = "ot";
	public static final String SHAREDPREFERENCE_SUPPORT_NUMBER = "support_number";
	public static final String SHAREDPREFERENCE_OS_VERSION = "os_version";
	public static final String SHAREDPREFERENCE_OS_MANUFACTURER = "os_manufacturer";
	public static final String SHAREDPREFERENCE_REGISTRATION = "regi_one_time_api_4";
	public static final String SHAREDPREFERENCE_PREFILLED = "pre_filled";
	public static final String SHAREDPREFERENCE_FREEBIES_TIME = "freebies_time";
	public static final String SHAREDPREFERENCE_PURCHASE_DATA = "purchase_data";
	public static final String SHAREDPREFERENCE_COIN_DATA = "coin_data";
	public static final String SHAREDPREFERENCE_WALLET_DATA = "wallet_data";
	public static final String SHAREDPREFERENCE_MY_GIFTS_DATA = "my_gifts_data";
	public static final String SHAREDPREFERENCE_BACK_LOCATION = "back_location";
	public static final String SHAREDPREFERENCE_SELECTED_GIFT = "selected_gift";
	public static final String SHAREDPREFERENCE_SMS_BILLPAYMENT_FAILURE = "BILLPAYMENT_FAILURE";
	public static final String SHAREDPREFERENCE_SMS_RECHARGE_FAILURE = "RECHARGE_FAILURE";
	public static final String SHAREDPREFERENCE_SMS_WALLET_REFILLED_RETAILER = "WALLET_REFILLED_RETAILER";
	public static final String SHAREDPREFERENCE_REFESH_QUICKPAY = "REFESH_QUICKPAY";
	public static final String SHAREDPREFERENCE_REFESH_TRANSATION = "REFESH_TRANSATION";
	public static final String SHAREDPREFERENCE_REFESH_WALLET = "REFESH_WALLET";
	public static final String SHAREDPREFERENCE_REFESH_MY_GIFT = "REFESH_MY_GIFT";
	// public static final String SHAREDPREFERENCE_ORDER = "order";
	// public static final String SHAREDPREFERENCE_ORDER_NEXT = "order_next";
	public static final String SHAREDPREFERENCE_GIFT = "gift";
	public static final String SHAREDPREFERENCE_GIFT_EXPIRED = "gift_expired";

	public static final String SHAREDPREFERENCE_COOKIE_VERSION = "version";
	public static final String SHAREDPREFERENCE_COOKIE_NAME = "name";
	public static final String SHAREDPREFERENCE_COOKIE_VALUE = "value";
	public static final String SHAREDPREFERENCE_COOKIE_DOMAIN = "domain";
	public static final String SHAREDPREFERENCE_COOKIE_PATH = "path";
	public static final String SHAREDPREFERENCE_COOKIE_EXPIRY = "expiry";

	public static final String SHAREDPREFERENCE_COOKIE_VERSION_PANEL = "versionPanel";
	public static final String SHAREDPREFERENCE_COOKIE_NAME_PANEL = "namePanel";
	public static final String SHAREDPREFERENCE_COOKIE_VALUE_PANEL = "valuePanel";
	public static final String SHAREDPREFERENCE_COOKIE_DOMAIN_PANEL = "domainPanel";
	public static final String SHAREDPREFERENCE_COOKIE_PATH_PANEL = "pathPanel";
	public static final String SHAREDPREFERENCE_COOKIE_EXPIRY_PANEL = "expiryPanel";

	// public static final String SHAREDPREFERENCE_CHAT_TIME = "chat_time";
	// public static final String SHAREDPREFERENCE_CHAT_FLAG = "chat_flag";
	public static final String SHAREDPREFERENCE_CITY_LAST_UPDATE_TIME = "city_time";
	public static final String SHAREDPREFERENCE_GIFT_LAST_UPDATE_TIME = "gift_time";

	public static final String ERROR_NAME_BLANK_FIELD = "Please enter name.";
	public static final String ERROR_EMAIL_BLANK_FIELD = "Please enter email.";
	public static final String ERROR_EMAIL_VALID_FIELD = "Please enter valid email.";
	public static final String ERROR_MOBILE_BLANK_FIELD = "Please enter mobile number.";
	public static final String ERROR_OPERATOR_BLANK_FIELD = "Please select operator.";
	public static final String ERROR_OTP_BLANK_FIELD = "Please enter OTP.";
	public static final String ERROR_PIN_BLANK_FIELD = "Please enter password.";
	public static final String ERROR_AMOUNT_BLANK_FIELD = "Please enter amount.";
	public static final String ERROR_AMOUNT_VALID_FIELD = "Please enter valid amount.";
	public static final String ERROR_AMOUNT_RANG_FIELD = "Please enter amount between range ";
	public static final String ERROR_QUERY_BLANK_FIELD = "Please enter request query.";
	public static final String ERROR_COUPON_BLANK_FIELD = "Please enter coupon code.";
	public static final String ERROR_MOBILE_LENGTH_FIELD = "Please enter valid mobile number.";
	public static final String ERROR_DATA_LENGTH_FIELD = "Please enter valid datacard number.";
	public static final String ERROR_SUB_ID_BLANK_FIELD = "Sub ID can not be blank.";
	public static final String ERROR_SUB_ID_LENGTH_FIELD = "Please enter valid subscriber id.";
	public static final String ERROR_ADDRESS_BLANK_FIELD = "Please enter address.";
	public static final String ERROR_AREA_BLANK_FIELD = "Please enter area.";
	public static final String ERROR_CITY_BLANK_FIELD = "Please enter city.";
	public static final String ERROR_STATE_BLANK_FIELD = "Please enter state.";
	public static final String ERROR_ZIP_BLANK_FIELD = "Please enter zip code.";
	public static final String ERROR_LOGIN_PIN_BLANK_FIELD = "Please enter password.";
	public static final String ERROR_LOGIN_PIN_VALID_FIELD = "Password must be more than 4 characters.";
	public static final String ERROR_BUS_CITY_FIELD = "Please enter travel city.";
	public static final String ERROR_BUS_TRAVEL_NAME_FIELD = "Please enter traveller's name.";
	public static final String ERROR_BUS_TRAVEL_AGE_FIELD = "Please enter traveller's age.";
	public static final String ERROR_BUS_TRAVEL_GENDER_FIELD = "Please enter traveller's gender.";

	// public static final String LAST_CIRCLE_UPDATED = "circle_updated_time";
	public static final String LAST_PLANS_UPDATED = "plans_updated_time";
	public static final String LAST_CODE_UPDATED = "code_updated_time";
	public static final String LAST_GIFT_UPDATED = "gift_time_1";

	public static final String API_VERSION = "3";
public static final int E_VOUCHER_ID=17;
	public static int DEAL_NOTIFICATION = 2000;
	public static int BALANCE_NOTIFICATION = 1000;

	public static int DIALOG_CLOSE = 0;
	public static int DIALOG_CLOSE_SIGNUP = 1;
	public static int DIALOG_CLOSE_CONFIRM = 2;
	public static int DIALOG_CLOSE_CHANGE_PIN = 3;
	public static int DIALOG_CLOSE_ORDER = 4;
	public static int DIALOG_CLOSE_LOGIN = 5;
	public static int DIALOG_CLOSE_QUICK_PAY = 6;
	public static int DIALOG_CLOSE_FORGOTPIN = 7;
	public static int DIALOG_CLOSE_MAP_STORE=10;
	public static int DIALOG_CLOSE_LOCATION = 8;
	public static int DIALOG_CLOSE_NOTIFICATION = 9;

	public static int SMS_SETTINGS = 0;
	public static int RECHARGE_SETTINGS = 1;
	public static int QUICK_SETTINGS = 2;
	public static int CHANGE_PIN_SETTINGS = 3;
	public static int COMPLAINT_SETTINGS = 5;
	public static int DEAL_SETTINGS = 6;

	public static String checkCode(String result) {
		try {
			JSONObject jsonObject = new JSONObject(result);
			String code1 = jsonObject.getString("errCode");
			int code = 0;
			if (!code1.equalsIgnoreCase(""))
				code = Integer.parseInt(code1);
			String description = jsonObject.getString("description");

			if (code == 404) {
				return description;// "Invalid Access";
			} else if (code == 201)
				return code + "";
			// else if (code == 30 || code == 2 || code == 28 || code == 4
			// || code == 5 || code == 6 || code == 7 || code == 32
			// || code == 8 || code == 9 || code == 29 || code == 33
			// || code == 14 || code == 31) {
			//
			// return description;
			// }
			else {

				return description;
			}

		} catch (JSONException e) {
			// e.printStackTrace();
			return ERROR_MESSAGE_FOR_ALL_REQUEST;
		}
	}

	/** Cancel notification **/
	public static void CancelNotification(Context ctx, int notifyId) {
		String ns = Context.NOTIFICATION_SERVICE;
		NotificationManager nMgr = (NotificationManager) ctx
				.getSystemService(ns);
		nMgr.cancel(notifyId);
	}

	/** To show plan circles from assert package. */
	public static String loadJSONFromAsset(Context context, String filename) {
		String json = null;
		try {

			InputStream is = context.getAssets().open(filename);

			int size = is.available();

			byte[] buffer = new byte[size];

			is.read(buffer);

			is.close();

			json = new String(buffer, "UTF-8");

		} catch (IOException ex) {
			// ex.printStackTrace();
			return null;
		}
		return json;

	}

	public static void showFullOneButtonDialog(final Context context,
			String Title, final String message, final int Type) {
		try {
			if (message.equalsIgnoreCase("201")) {
				context.startActivity(new Intent(context, LoginActivity.class));
			} else {
				final Dialog dialog = new Dialog(context);
				dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				// dialog.getWindow().setBackgroundDrawableResource(
				// android.R.color.transparent);
				dialog.setContentView(R.layout.dialog_full_one_button);
				dialog.setCancelable(false);
				dialog.setCanceledOnTouchOutside(false);
				// dialog.setTitle(null);

				Typeface Reguler = Typeface
						.createFromAsset(context.getAssets(),
								"Roboto-Regular.ttf");

				Typeface Medium = Typeface.createFromAsset(context.getAssets(),
						"Roboto-Regular.ttf");

				// set the custom dialog components - text, image and button
				TextView textView_Title = (TextView) dialog
						.findViewById(R.id.textView_Title);
				textView_Title.setText(Title);
				textView_Title.setTypeface(Reguler);

				TextView textView_Message = (TextView) dialog
						.findViewById(R.id.textView_Message);
				textView_Message.setText(message);
				textView_Message.setTypeface(Medium);

				Button button_Ok = (Button) dialog.findViewById(R.id.button_Ok);
				button_Ok.setTypeface(Reguler);
				// if button is clicked, close the custom dialog
				button_Ok.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						dialog.dismiss();
						if (DIALOG_CLOSE_SIGNUP == Type) {
							((Activity) context).startActivity(new Intent(
									context, LoginActivity.class));
							((Activity) context).finish();
						} else if (DIALOG_CLOSE_CONFIRM == Type) {
							((Activity) context).finish();
						} else if (DIALOG_CLOSE_CHANGE_PIN == Type) {
							((Activity) context).startActivity(new Intent(
									context, LoginActivity.class));
							((Activity) context).finish();
						} else if (DIALOG_CLOSE_ORDER == Type) {
							((Activity) context).finish();
						} else if (DIALOG_CLOSE_LOGIN == Type) {
							((Activity) context).startActivity(new Intent(
									context, LoginActivity.class));
						} else if (DIALOG_CLOSE_FORGOTPIN == Type) {
							// context.startActivity(new Intent(context,
							// SignUpActivity.class));
							// ((Activity) context).finish();
						}
					}
				});

				dialog.show();
			}
		} catch (Exception exception) {
		}
	}

	public static void showFullTwoButtonDialog(final Context context,
			String Title, String message, final int Type) {
		try {

			final Dialog dialog = new Dialog(context);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			// dialog.getWindow().setBackgroundDrawableResource(
			// android.R.color.transparent);
			dialog.setContentView(R.layout.dialog_full_two_button);
			dialog.setCancelable(false);
			dialog.setCanceledOnTouchOutside(false);
			// dialog.setTitle(null);

			Typeface Reguler = Typeface.createFromAsset(context.getAssets(),
					"Roboto-Regular.ttf");

			Typeface Medium = Typeface.createFromAsset(context.getAssets(),
					"Roboto-Regular.ttf");

			// set the custom dialog components - text, image and button
			TextView textView_Title = (TextView) dialog
					.findViewById(R.id.textView_Title);
			textView_Title.setText(Title);
			textView_Title.setTypeface(Reguler);

			TextView textView_Message = (TextView) dialog
					.findViewById(R.id.textView_Message);
			textView_Message.setText(message);
			textView_Message.setTypeface(Medium);

			Button button_Ok = (Button) dialog.findViewById(R.id.button_Ok);
			button_Ok.setTypeface(Reguler);
			// if button is clicked, close the custom dialog
			button_Ok.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
					if (DIALOG_CLOSE_LOCATION == Type) {
						Intent viewIntent = new Intent(
								Settings.ACTION_LOCATION_SOURCE_SETTINGS);
						context.startActivity(viewIntent);
					}
				}
			});

			Button button_Cancel = (Button) dialog
					.findViewById(R.id.button_Cancel);
			button_Cancel.setTypeface(Reguler);
			// if button is clicked, close the custom dialog
			button_Cancel.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
				}
			});

			dialog.show();

		} catch (Exception exception) {
		}
	}

	public static void showOneButtonDialog(final Context context, String Title,
			final String message, final int Type) {
		try {
			if (message.equalsIgnoreCase("201")) {
				
				context.startActivity(new Intent(context, LoginActivity.class));
			} else {
				final Dialog dialog = new Dialog(context);
				dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				dialog.getWindow().setBackgroundDrawableResource(
						android.R.color.transparent);
				dialog.setContentView(R.layout.dialog_one_button);
				dialog.setCancelable(false);
				dialog.setCanceledOnTouchOutside(false);
				// dialog.setTitle(null);

				Typeface Reguler = Typeface
						.createFromAsset(context.getAssets(),
								"Roboto-Regular.ttf");

				Typeface Medium = Typeface.createFromAsset(context.getAssets(),
						"Roboto-Regular.ttf");

				// set the custom dialog components - text, image and button
				TextView textView_Title = (TextView) dialog
						.findViewById(R.id.textView_Title);
				textView_Title.setText(Title);
				textView_Title.setTypeface(Reguler);

				TextView textView_Message = (TextView) dialog
						.findViewById(R.id.textView_Message);
				textView_Message.setText(message);
				textView_Message.setTypeface(Medium);

				Button button_Ok = (Button) dialog.findViewById(R.id.button_Ok);
				button_Ok.setTypeface(Reguler);
				// if button is clicked, close the custom dialog
				button_Ok.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						dialog.dismiss();
						if (DIALOG_CLOSE_SIGNUP == Type) {
							((Activity) context).startActivity(new Intent(
									context, LoginActivity.class));
							((Activity) context).finish();
						} else if (DIALOG_CLOSE_CONFIRM == Type) {
							((Activity) context).finish();
						} else if (DIALOG_CLOSE_CHANGE_PIN == Type) {
							((Activity) context).startActivity(new Intent(
									context, LoginActivity.class));
							((Activity) context).finish();
						} else if (DIALOG_CLOSE_ORDER == Type) {
							((Activity) context).finish();
						} else if (DIALOG_CLOSE_LOGIN == Type) {
							((Activity) context).startActivity(new Intent(
									context, LoginActivity.class));
						} else if (DIALOG_CLOSE_FORGOTPIN == Type) {
							// context.startActivity(new Intent(context,
							// SignUpActivity.class));
							// ((Activity) context).finish();
						} else if (DIALOG_CLOSE_MAP_STORE == Type) {
							// context.startActivity(new Intent(context,
							// SignUpActivity.class));
							// ((Activity) context).finish();
						}
					}
				});

				dialog.show();
			}
		} catch (Exception exception) {
		}
	}

	public static String encodeTobase64(Bitmap image) {
		Bitmap immage = image;
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		immage.compress(Bitmap.CompressFormat.PNG, 100, baos);
		byte[] b = baos.toByteArray();
		String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);

		Log.d("Image Log:", imageEncoded);
		return imageEncoded;
	}

	public static Bitmap decodeBase64(String input) {
		byte[] decodedByte = Base64.decode(input, 0);
		return BitmapFactory
				.decodeByteArray(decodedByte, 0, decodedByte.length);
	}

	public static void showTwoButtonDialog(final Context context, String Title,
			String message, final int Type) {
		try {

			final Dialog dialog = new Dialog(context);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			// dialog.getWindow().setBackgroundDrawableResource(
			// android.R.color.transparent);
			dialog.setContentView(R.layout.dialog_two_button);
			dialog.setCancelable(false);
			dialog.setCanceledOnTouchOutside(false);
			// dialog.setTitle(null);

			Typeface Reguler = Typeface.createFromAsset(context.getAssets(),
					"Roboto-Regular.ttf");

			Typeface Medium = Typeface.createFromAsset(context.getAssets(),
					"Roboto-Regular.ttf");

			// set the custom dialog components - text, image and button
			TextView textView_Title = (TextView) dialog
					.findViewById(R.id.textView_Title);
			textView_Title.setText(Title);
			textView_Title.setTypeface(Reguler);

			TextView textView_Message = (TextView) dialog
					.findViewById(R.id.textView_Message);
			textView_Message.setText(message);
			textView_Message.setTypeface(Medium);

			Button button_Ok = (Button) dialog.findViewById(R.id.button_Ok);
			button_Ok.setTypeface(Reguler);
			// if button is clicked, close the custom dialog
			button_Ok.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
					if (DIALOG_CLOSE_LOCATION == Type) {
						Intent viewIntent = new Intent(
								Settings.ACTION_LOCATION_SOURCE_SETTINGS);
						context.startActivity(viewIntent);
						((Activity) context).finish();
					}
				}
			});

			Button button_Cancel = (Button) dialog
					.findViewById(R.id.button_Cancel);
			button_Cancel.setTypeface(Reguler);
			// if button is clicked, close the custom dialog
			button_Cancel.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
				}
			});

			if (Type == DIALOG_CLOSE_LOCATION) {
				button_Ok.setText("Ok");
				button_Cancel.setText("Not now");
			}
			dialog.show();

		} catch (Exception exception) {
		}
	}

	@SuppressLint("NewApi")
	public static void showSuccessFailureDialog(final Context context,
			final String Title, final int rechargeType, final String mobile,
			final String operatorProductID, final String amount,
			final String tax, final boolean type, final String reason,
			final String trans_id, String loyalty_points) {
		try {

			final Dialog dialog = new Dialog(context);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.getWindow().setBackgroundDrawableResource(
					android.R.color.transparent);

			if (type)
				dialog.setContentView(R.layout.dialog_success);
			else
				dialog.setContentView(R.layout.dialog_failure);
			// dialog.setTitle(null);
			dialog.setCancelable(false);
			dialog.setCanceledOnTouchOutside(false);

			Utility.setRefreshRequired(context,
					Constants.SHAREDPREFERENCE_REFESH_QUICKPAY, true);
			Utility.setRefreshRequired(context,
					Constants.SHAREDPREFERENCE_REFESH_MY_GIFT, true);
			Utility.setRefreshRequired(context,
					Constants.SHAREDPREFERENCE_REFESH_TRANSATION, true);
			Utility.setRefreshRequired(context,
					Constants.SHAREDPREFERENCE_REFESH_WALLET, true);

			Typeface Reguler = Typeface.createFromAsset(context.getAssets(),
					"Roboto-Regular.ttf");

			Typeface Medium = Typeface.createFromAsset(context.getAssets(),
					"Roboto-Regular.ttf");

			// set the custom dialog components - text, image and button
			TextView textView_Title = (TextView) dialog
					.findViewById(R.id.textView_Title);
			textView_Title.setTypeface(Reguler);

			TextView textView_Message = (TextView) dialog
					.findViewById(R.id.textView_Message);
			textView_Message.setTypeface(Medium);

			TextView textView_Mobile = (TextView) dialog
					.findViewById(R.id.textView_Mobile);
			textView_Mobile.setTypeface(Medium);

			ImageView imageView_Operator = (ImageView) dialog
					.findViewById(R.id.imageView_Operator);

			TextView textView_Symbol = (TextView) dialog
					.findViewById(R.id.textView_Symbol);
			textView_Symbol.setTypeface(Reguler);

			TextView textView_Amount = (TextView) dialog
					.findViewById(R.id.textView_Amount);
			textView_Amount.setTypeface(Medium);

			TextView textView_Call = (TextView) dialog
					.findViewById(R.id.textView_Call);
			Button button_cancel = null;
			if (type) {
				TextView textView_Congrats = (TextView) dialog
						.findViewById(R.id.textView_Congrats);
				textView_Congrats.setTypeface(Reguler);

				TextView textCoins = (TextView) dialog
						.findViewById(R.id.textCoins);
				textCoins.setTypeface(Reguler);

				Button button_Ok = (Button) dialog.findViewById(R.id.button_Ok);
				button_Ok.setTypeface(Reguler);

				button_cancel = (Button) dialog
						.findViewById(R.id.button_cancel);
				button_cancel.setTypeface(Reguler);
				button_cancel.setTypeface(Reguler);
				button_cancel.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						// dialog.dismiss();
						// ((Activity) context).finish();

						Intent intent = new Intent(context, MainActivity.class);
						intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
								| Intent.FLAG_ACTIVITY_NEW_TASK);
						intent.putExtra("FROM_QUICKPAY", false);
						intent.putExtra("IS_GIFT", false);
						context.startActivity(intent);
						((Activity) context).finish();
					}
				});

				LinearLayout msg_layout = (LinearLayout) dialog
						.findViewById(R.id.msg_layout);
				TextView textViewShowPoints = (TextView) dialog
						.findViewById(R.id.textViewShowPoints);
				textViewShowPoints.setTypeface(Reguler);
				textViewShowPoints.setText("You have earned " + loyalty_points
						+ " gifts coins!");
				if (loyalty_points.isEmpty()) {
					msg_layout.setVisibility(View.GONE);

				} else {
					msg_layout.setVisibility(View.VISIBLE);
				}
			}

			textView_Call.setTypeface(Medium);
			textView_Call
					.setText("Give us a missed call to raise the complaint of this transaction on "
							+ Utility.getSupportNumber(context,
									Constants.SHAREDPREFERENCE_SUPPORT_NUMBER));

			ImageView imageView_Call = (ImageView) dialog
					.findViewById(R.id.imageView_Call);
			imageView_Call.setImageDrawable(SVGParser.getSVGFromResource(
					context.getResources(), R.raw.ic_miscall)
					.createPictureDrawable());
			if (Build.VERSION.SDK_INT >= 11)
				imageView_Call.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
			imageView_Call.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent callIntent = new Intent(Intent.ACTION_CALL);
					// callIntent.setData(Uri.parse("tel:+912267242266"));
					callIntent.setData(Uri.parse("tel:"
							+ Utility.getSupportNumber(context,
									Constants.SHAREDPREFERENCE_SUPPORT_NUMBER)));
					context.startActivity(callIntent);
				}
			});

			TextView textView_Tax = (TextView) dialog
					.findViewById(R.id.textView_Tax);
			textView_Tax.setTypeface(Medium);
			textView_Tax.setPaintFlags(textView_Tax.getPaintFlags()
					| Paint.UNDERLINE_TEXT_FLAG);

			TextView textView_Location = (TextView) dialog
					.findViewById(R.id.textView_Location);
			textView_Location.setTypeface(Medium);
			textView_Location.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent viewIntent = new Intent(
							Settings.ACTION_LOCATION_SOURCE_SETTINGS);
					context.startActivity(viewIntent);
				}
			});

			// RelativeLayout relativeLayout_Operator = (RelativeLayout) dialog
			// .findViewById(R.id.relativeLayout_Operator);
			// if (rechargeType == Constants.WALLET_PAYMENT)
			// relativeLayout_Operator.setVisibility(View.GONE);

			// boolean gps_enabled = false;
			// boolean network_enabled = false;
			// LocationManager lm = (LocationManager) context
			// .getSystemService(Context.LOCATION_SERVICE);
			// try {
			// gps_enabled = lm
			// .isProviderEnabled(LocationManager.GPS_PROVIDER);
			// } catch (Exception ex) {
			// }
			// try {
			// network_enabled = lm
			// .isProviderEnabled(LocationManager.NETWORK_PROVIDER);
			// } catch (Exception ex) {
			// }

			// don't start listeners if no provider is
			// enabled
			// if (gps_enabled || network_enabled) {
			textView_Location.setVisibility(View.GONE);
			// }

			Button button_Ok = (Button) dialog.findViewById(R.id.button_Ok);
			button_Ok.setTypeface(Reguler);
			// if button is clicked, close the custom dialog
			button_Ok.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
					// boolean gps_enabled = false;
					// boolean network_enabled = false;
					// LocationManager lm = (LocationManager) context
					// .getSystemService(Context.LOCATION_SERVICE);
					// try {
					// gps_enabled = lm
					// .isProviderEnabled(LocationManager.GPS_PROVIDER);
					// } catch (Exception ex) {
					// }
					// try {
					// network_enabled = lm
					// .isProviderEnabled(LocationManager.NETWORK_PROVIDER);
					// } catch (Exception ex) {
					// }
					if (type && (rechargeType != Constants.WALLET_PAYMENT)
							&& (rechargeType != Constants.SUBSCRIBE_DEAL)) {
						// int days = (int) ((System.currentTimeMillis() -
						// Utility
						// .getDailyFreeBieTime(context,
						// SHAREDPREFERENCE_FREEBIES_TIME)) / (1000 * 60 * 60 *
						// 24));
						// if (days < 1) {
						// Intent intent = new Intent(context,
						// MainActivity.class);
						// intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
						// | Intent.FLAG_ACTIVITY_NEW_TASK);
						// ((Activity) context).startActivity(intent);
						// } else {
						/*
						 * ReClaimGiftDataSource reClaimGiftDataSource = new
						 * ReClaimGiftDataSource( context); try {
						 * reClaimGiftDataSource.open();
						 * 
						 * ReClaimGift reClaimGift = reClaimGiftDataSource
						 * .getReClaimGift(trans_id); if (reClaimGift == null) {
						 * reClaimGiftDataSource.createReClaimGift( trans_id,
						 * Long.parseLong(amount), 1, 2,
						 * System.currentTimeMillis()); } } catch (SQLException
						 * exception) { // TODO: handle exception // // //
						 * Log.e("Er ", exception.getMessage()); } catch
						 * (Exception e) { // TODO: handle exception // // //
						 * Log.e("Er ", e.getMessage()); } finally {
						 * reClaimGiftDataSource.close(); }
						 */
						Intent intent = new Intent(context, MainActivity.class);
						intent.putExtra("FROM_QUICKPAY", true);

						intent.putExtra("IS_GIFT", true);
						((Activity) context).finish();

						((Activity) context).startActivity(intent);
						// }
					} else if (rechargeType == Constants.WALLET_PAYMENT
							|| rechargeType == Constants.SUBSCRIBE_DEAL) {
						Intent intent = new Intent(context, MainActivity.class);
						intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
								| Intent.FLAG_ACTIVITY_NEW_TASK);
						intent.putExtra("FROM_QUICKPAY", false);
						intent.putExtra("IS_GIFT", false);
						context.startActivity(intent);
						((Activity) context).finish();
					} else {
						Intent intent = new Intent(context, MainActivity.class);
						intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
								| Intent.FLAG_ACTIVITY_NEW_TASK);
						((Activity) context).startActivity(intent);
						((Activity) context).finish();
					}
				}
			});

			ImageView imageView_Close = (ImageView) dialog
					.findViewById(R.id.imageView_Close);
			imageView_Close.setVisibility(View.GONE);
			imageView_Close.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					((Activity) context).startActivity(new Intent(context,
							MainActivity.class));
				}
			});

			if (type) {
				if (rechargeType == Constants.RECHARGE_DTH) {
					imageView_Operator.setImageResource(OperatorConstant
							.getOperatorResource(Integer
									.parseInt(operatorProductID)));
					textView_Mobile.setText(mobile);
					textView_Message.setText("DTH recharge");
				} else if (rechargeType == Constants.RECHARGE_MOBILE) {
					imageView_Operator.setImageResource(OperatorConstant
							.getOperatorResource(Integer
									.parseInt(operatorProductID)));
					textView_Mobile.setText(mobile);
					textView_Message.setText("Prepaid recharge");
				} else if (rechargeType == Constants.RECHARGE_DATA) {
					imageView_Operator.setImageResource(OperatorConstant
							.getOperatorResource(Integer
									.parseInt(operatorProductID)));
					textView_Mobile.setText(mobile);
					textView_Message.setText("Datacard recharge");
				} else if (rechargeType == Constants.BILL_PAYMENT) {
					imageView_Operator.setImageResource(OperatorConstant
							.getOperatorResource(Integer
									.parseInt(operatorProductID)));
					textView_Mobile.setText(mobile);
					textView_Message.setText("Postpaid bill");
					textView_Tax.setVisibility(View.VISIBLE);
					textView_Tax.setText(tax);
				} else if (rechargeType == Constants.SUBSCRIBE_DEAL) {
					imageView_Operator.setBackgroundColor(context
							.getResources().getColor(R.color.app_blue_color));
					imageView_Operator.setImageDrawable(SVGParser
							.getSVGFromResource(context.getResources(),
									R.raw.ic_gifts_line).createPictureDrawable());
					if (Build.VERSION.SDK_INT >= 11)
						imageView_Operator.setLayerType(
								View.LAYER_TYPE_SOFTWARE, null);
					textView_Mobile.setText(mobile);
					textView_Message.setText("Deal purchased");
				} else if (rechargeType == Constants.WALLET_PAYMENT) {
					imageView_Operator.setBackgroundColor(context
							.getResources().getColor(R.color.app_blue_color));
					imageView_Operator.setImageDrawable(SVGParser
							.getSVGFromResource(context.getResources(),
									R.raw.ic_wallet).createPictureDrawable());
					if (Build.VERSION.SDK_INT >= 11)
						imageView_Operator.setLayerType(
								View.LAYER_TYPE_SOFTWARE, null);
					textView_Mobile.setText(mobile);
					textView_Message.setText("Wallet topup");
					button_Ok.setText("OK");
					button_cancel.setText("Cancel");
					/*
					 * if (loyalty_points.isEmpty()) {
					 * msg_layout.setVisibility(View.GONE);
					 * 
					 * } else { msg_layout.setVisibility(View.VISIBLE); }
					 */

					// new CheckBalanceTask().execute();
				}
			} else {
				if (rechargeType == Constants.RECHARGE_DTH) {
					imageView_Operator.setImageResource(OperatorConstant
							.getOperatorResource(Integer
									.parseInt(operatorProductID)));
					textView_Mobile.setText(mobile);
					textView_Message.setText(reason);
				} else if (rechargeType == Constants.RECHARGE_MOBILE
						|| rechargeType == Constants.RECHARGE_DATA) {
					imageView_Operator.setImageResource(OperatorConstant
							.getOperatorResource(Integer
									.parseInt(operatorProductID)));
					textView_Mobile.setText(mobile);
					textView_Message.setText(reason);
				} else if (rechargeType == Constants.BILL_PAYMENT) {
					imageView_Operator.setImageResource(OperatorConstant
							.getOperatorResource(Integer
									.parseInt(operatorProductID)));
					textView_Mobile.setText(mobile);
					textView_Message.setText(reason);
					textView_Tax.setVisibility(View.VISIBLE);
					textView_Tax.setText(tax);
				} else if (rechargeType == Constants.SUBSCRIBE_DEAL) {
					imageView_Operator.setBackgroundColor(context
							.getResources().getColor(R.color.app_blue_color));
					imageView_Operator.setImageDrawable(SVGParser
							.getSVGFromResource(context.getResources(),
									R.raw.ic_gifts_line).createPictureDrawable());
					if (Build.VERSION.SDK_INT >= 11)
						imageView_Operator.setLayerType(
								View.LAYER_TYPE_SOFTWARE, null);
					textView_Mobile.setText(mobile);
					textView_Message.setText(reason);
				} else if (rechargeType == Constants.WALLET_PAYMENT) {
					imageView_Operator.setBackgroundColor(context
							.getResources().getColor(R.color.app_blue_color));
					imageView_Operator.setImageDrawable(SVGParser
							.getSVGFromResource(context.getResources(),
									R.raw.ic_wallet).createPictureDrawable());
					if (Build.VERSION.SDK_INT >= 11)
						imageView_Operator.setLayerType(
								View.LAYER_TYPE_SOFTWARE, null);
					textView_Mobile.setText(mobile);
					textView_Message.setText(reason);
					// new CheckBalanceTask().execute();
				}
			}
			// imageView_Operator.setText(operator);
			textView_Amount.setText(amount);
			dialog.show();
		} catch (Exception exception) {
		}
	}

	/** To check whether Internet connectivity available or not. */
	public static boolean isOnline(Context context) {
		try {
			ConnectivityManager connectivityManager = (ConnectivityManager) context
					.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo activeNetworkInfo = connectivityManager
					.getActiveNetworkInfo();
			return activeNetworkInfo != null && activeNetworkInfo.isConnected();
		} catch (Exception e) {
			return false;
		}
	}

	public static boolean machTemplate(String sms, String template,
			String varStart, String varEnd) {

		template = template.trim().replaceAll(varStart, "~");
		template = template.trim().replaceAll(varEnd, "~");

		String[] template_split = template.split("~");
		// String[] vars = new String[template_split.length];

		boolean ret = true;
		int start = 0;
		String log = "";
		// String[] out = new String[10];
		// out[0] = sms;

		for (int i = 0; i < template_split.length; i += 2) {
			if (template_split[i] == null) {
				if (i != 0) {
					// vars[start] = sms;
					// vars[template_split[i-1]] = sms;
				}
			} else {
				log += "Checking " + template_split[i];
				int index = sms.indexOf(template_split[i]);
				log += ":" + index + "\n";
				if (index == -1) {
					ret = false;
					break;
				} else {
					// String var = sms.substring(0, index);
					if (i != 0) {
						// vars[start] = var.trim();
						// vars[template_split[i - 1]] = var.trim();
						start++;
					}
					sms = sms.substring(index + template_split[i].length());
				}
			}
		}

		if (template_split.length == 1 && sms != template) {
			ret = false;
		}

		// if (ret) {
		// out[1] = "success";
		// out[2] = vars[0];
		// } else {
		// out[1] = "failure";
		// out[2] = vars[0];
		// }
		System.out.println(log);
		// return out[1];
		return ret;
	}

	public static void Notify(Context context, final String notificationTitle,
			final String notificationMessage, final int id, final String t_id,
			final long amount) {
		try {
			NotificationManager notificationManager = (NotificationManager) context
					.getSystemService(Context.NOTIFICATION_SERVICE);

			Notification notification = new Notification(
					R.drawable.status_notification, notificationTitle,
					System.currentTimeMillis());
			// Constants.notificationList.add(notificationMessage);
			if (Utility.getLoginFlag(context,
					Constants.SHAREDPREFERENCE_IS_LOGIN)) {
				Intent notificationIntent = new Intent(context,
						FreeBiesActivity.class);
				notificationIntent
						.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
				notificationIntent.putExtra("AMOUNT", String.valueOf(amount));
				notificationIntent.putExtra("TRANS_ID", t_id);
				PendingIntent pendingIntent = PendingIntent.getActivity(
						context, id, notificationIntent,
						PendingIntent.FLAG_UPDATE_CURRENT);

				try {
					Uri notification1 = RingtoneManager
							.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
					Ringtone r = RingtoneManager.getRingtone(
							context.getApplicationContext(), notification1);
					r.play();

					Vibrator v = (Vibrator) context
							.getSystemService(Context.VIBRATOR_SERVICE);

					// Vibrate for 1000 milliseconds
					v.vibrate(1000);
				} catch (Exception e) {
				}
				notification.setLatestEventInfo(context, notificationTitle,
						notificationMessage, pendingIntent);
				notification.flags |= Notification.FLAG_ONGOING_EVENT
						| Notification.FLAG_AUTO_CANCEL;
				notificationManager.notify(id, notification);
			}
		} catch (Exception e) {
		}
	}

	public static double distanceFrom(double lat1, double lng1, double lat2,
			double lng2) {
		// Return distance between 2 points, stored as 2 pair location;
		double R = 6371; // Radius of the earth in km
		double dLat = (lat2 - lat1) * Math.PI / 180; // deg2rad below
		double dLon = (lng2 - lng1) * Math.PI / 180;
		double a = 0.5 - Math.cos(dLat) / 2 + Math.cos(lat1 * Math.PI / 180)
				* Math.cos(lat2 * Math.PI / 180) * (1 - Math.cos(dLon)) / 2;

		return R * 2 * Math.asin(Math.sqrt(a));

	}

	// public static double CalculationByDistance(double initialLat,
	// double initialLong, double finalLat, double finalLong) {
	// /* PRE: All the input values are in radians! */
	//
	// // double theta = initialLong - finalLong;
	// // double dist = Math.sin(deg2rad(initialLat))
	// // * Math.sin(deg2rad(finalLat)) + Math.cos(deg2rad(initialLat))
	// // * Math.cos(deg2rad(finalLat)) * Math.cos(deg2rad(theta));
	// // dist = Math.acos(dist);
	// // dist = rad2deg(dist);
	// // dist = dist * 60 * 1.1515;
	// // return (dist);
	//
	// double earthRadius = 6371;
	//
	// double dLat = Math.toRadians(initialLat - finalLat);
	// double dLng = Math.toRadians(initialLong - finalLong);
	// double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
	// + Math.cos(Math.toRadians(finalLat))
	// * Math.cos(Math.toRadians(initialLat)) * Math.sin(dLng / 2)
	// * Math.sin(dLng / 2);
	// double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
	// double dist = earthRadius * c;
	// return dist;
	//
	// }
	//
	public static double deg2rad(double deg) {
		return (deg * Math.PI / 180.0);
	}

	//
	// private static double rad2deg(double rad) {
	// return (rad * 180.0 / Math.PI);
	// }

	public static void setFont(Context context,ViewGroup group) {
		Typeface Reguler = Typeface.createFromAsset(context.getAssets(),
				"Roboto-Regular.ttf");
		int count = group.getChildCount();
		View v;
		for (int i = 0; i < count; i++) {
			v = group.getChildAt(i);

			if (v instanceof TextView || v instanceof Button /* etc. */)
				((TextView) v).setTypeface(Reguler);
			else if (v instanceof ViewGroup)
				setFont(context,(ViewGroup) v);
		}
	}

	public static void changeFonts(Context context, ViewGroup root) {
		Typeface tf = Typeface.createFromAsset(context.getAssets(),
				"Roboto-Regular.ttf");
		for (int i = 0; i < root.getChildCount(); i++) {
			View v = root.getChildAt(i);
			if (v instanceof TextView) {
				((TextView) v).setTypeface(tf);
			} else if (v instanceof Button) {
				((Button) v).setTypeface(tf);
			} else if (v instanceof EditText) {
				((EditText) v).setTypeface(tf);
			} else if (v instanceof ViewGroup) {
				changeFonts(context,(ViewGroup) v);
			}
		}

	}
	
	public static void setAlpha(View view, float alpha) {
        // Set the alpha appropriately (setAlpha is API >= 11, this technique works on all API levels).
        AlphaAnimation alphaAnimation = new AlphaAnimation(alpha, alpha);
        alphaAnimation.setDuration(0);
        alphaAnimation.setFillAfter(true);
        view.startAnimation(alphaAnimation);
    }

}