//
//  TransactionHistoryService.h
//  Pay1
//
//  Created by webninjaz on 09/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "Service.h"

@class TransactionHistoryCriteria;
@class TransactionHistoryResponse;

typedef void (^transactionHistoryResponse)(NSError *error, TransactionHistoryResponse *transactionHistoryResponse);

@interface TransactionHistoryService : Service{
}

-(void)callTransactionHistoryService:(TransactionHistoryCriteria *)criteria callback:(transactionHistoryResponse)callback;


@end
