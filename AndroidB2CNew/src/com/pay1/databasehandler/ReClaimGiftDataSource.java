package com.pay1.databasehandler;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class ReClaimGiftDataSource {
	// Database fields
	private SQLiteDatabase database;
	private Pay1SQLiteHelper dbHelper;

	public ReClaimGiftDataSource(Context context) {
		dbHelper = new Pay1SQLiteHelper(context);
	}

	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
		System.out.println("Released " + SQLiteDatabase.releaseMemory());
	}

	public void createReClaimGift(String reClaimTransactionID,
			long reClaimRechargeAmount, int reClaimFlag, int reClaimCounter,
			long reClaimUptateTime) {

		ContentValues values = new ContentValues();
		values.put(Pay1SQLiteHelper.RCG_TRANS_ID, reClaimTransactionID);
		values.put(Pay1SQLiteHelper.RCG_RECHARGE_AMOUNT, reClaimRechargeAmount);
		values.put(Pay1SQLiteHelper.RCG_FLAG, reClaimFlag);
		values.put(Pay1SQLiteHelper.RCG_COUNTER, reClaimCounter);
		values.put(Pay1SQLiteHelper.RCG_LAST_UPDATE_TIME, reClaimUptateTime);

		database.insert(Pay1SQLiteHelper.TABLE_RECLAIMGIFT, null, values);
		// Cursor cursor = database.query(Pay1SQLiteHelper.TABLE_Operator,
		// null, Pay1SQLiteHelper.Operator_ID + " = " + insertId, null,
		// null, null, null);
		// cursor.moveToFirst();
		// ReClaimGift neOperator = cursorToOperator(cursor);
		// cursor.close();
		// return newOperator;
	}

	public void updateReClaimGift(int reClaimID, int reClaimFlag,
			int reClaimCounter, long reClaimUptateTime) {

		ContentValues values = new ContentValues();
		values.put(Pay1SQLiteHelper.RCG_FLAG, reClaimFlag);
		values.put(Pay1SQLiteHelper.RCG_COUNTER, reClaimCounter);
		values.put(Pay1SQLiteHelper.RCG_LAST_UPDATE_TIME, reClaimUptateTime);
		long updatetId = database.update(Pay1SQLiteHelper.TABLE_RECLAIMGIFT,
				values, Pay1SQLiteHelper.RCG_ID + " = " + reClaimID, null);
		if (updatetId > 0) {
		}
		// // // Log.i(Tag, "Monitor updated with id: " + monitor_ID);
		else {
		}
	}

	public void updateReClaimGift(String reClaimTransactionID, int reClaimFlag,
			int reClaimCounter, long reClaimUptateTime) {

		ContentValues values = new ContentValues();
		values.put(Pay1SQLiteHelper.RCG_FLAG, reClaimFlag);
		values.put(Pay1SQLiteHelper.RCG_COUNTER, reClaimCounter);
		values.put(Pay1SQLiteHelper.RCG_LAST_UPDATE_TIME, reClaimUptateTime);
		long updatetId = database.update(Pay1SQLiteHelper.TABLE_RECLAIMGIFT,
				values, Pay1SQLiteHelper.RCG_TRANS_ID + " = '"
						+ reClaimTransactionID + "'", null);
		if (updatetId > 0) {
		}
		// // // Log.i(Tag, "Monitor updated with id: " + monitor_ID);
		else {
		}
	}

	public void deleteReClaimGift() {
		database.delete(Pay1SQLiteHelper.TABLE_RECLAIMGIFT, null, null);
	}

	public void deleteReClaimGift(int reClaimID) {
		database.delete(Pay1SQLiteHelper.TABLE_RECLAIMGIFT,
				Pay1SQLiteHelper.RCG_ID + " = " + reClaimID, null);
	}

	public void deleteReClaimGift(String reClaimTransactionID) {
		database.delete(Pay1SQLiteHelper.TABLE_RECLAIMGIFT,
				Pay1SQLiteHelper.RCG_TRANS_ID + " = '" + reClaimTransactionID
						+ "'", null);
	}

	public List<ReClaimGift> getAllReClaimGift() {
		List<ReClaimGift> reClaimGifts = new ArrayList<ReClaimGift>();

		Cursor cursor = database.query(Pay1SQLiteHelper.TABLE_RECLAIMGIFT,
				null, null, null, null, null,
				Pay1SQLiteHelper.RCG_LAST_UPDATE_TIME);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			ReClaimGift reClaimGift = cursorToReClaimGift(cursor);
			reClaimGifts.add(reClaimGift);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return reClaimGifts;
	}

	public List<ReClaimGift> getAllReClaimGift(int flag) {
		List<ReClaimGift> reClaimGifts = new ArrayList<ReClaimGift>();

		Cursor cursor = database.query(Pay1SQLiteHelper.TABLE_RECLAIMGIFT,
				null, Pay1SQLiteHelper.RCG_FLAG + " = " + flag, null, null,
				null, Pay1SQLiteHelper.RCG_LAST_UPDATE_TIME);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			ReClaimGift reClaimGift = cursorToReClaimGift(cursor);
			reClaimGifts.add(reClaimGift);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return reClaimGifts;
	}

	public ReClaimGift getReClaimGift(String reClaimTransactionID, int flag) {

		Cursor cursor = database.query(Pay1SQLiteHelper.TABLE_RECLAIMGIFT,
				null, Pay1SQLiteHelper.RCG_TRANS_ID + " = '"
						+ reClaimTransactionID + "' AND "
						+ Pay1SQLiteHelper.RCG_FLAG + " = " + flag, null, null,
				null, null);

		if (cursor.getCount() > 0) {
			cursor.moveToFirst();
			ReClaimGift reClaimGift = cursorToReClaimGift(cursor);
			cursor.moveToNext();
			// make sure to close the cursor
			cursor.close();
			return reClaimGift;
		} else {
			return null;
		}
	}

	public ReClaimGift getReClaimGift(String reClaimTransactionID) {

		Cursor cursor = database.query(Pay1SQLiteHelper.TABLE_RECLAIMGIFT,
				null, Pay1SQLiteHelper.RCG_TRANS_ID + " = '"
						+ reClaimTransactionID + "'", null, null, null, null);

		if (cursor.getCount() > 0) {
			cursor.moveToFirst();
			ReClaimGift reClaimGift = cursorToReClaimGift(cursor);
			cursor.moveToNext();
			// make sure to close the cursor
			cursor.close();
			return reClaimGift;
		} else {
			return null;
		}
	}

	private ReClaimGift cursorToReClaimGift(Cursor cursor) {
		ReClaimGift reClaimGift = new ReClaimGift();
		reClaimGift.setReClaimID(cursor.getInt(0));
		reClaimGift.setReClaimTransactionID(cursor.getString(1));
		reClaimGift.setReClaimRechargeAmount(cursor.getLong(2));
		reClaimGift.setReClaimFlag(cursor.getInt(3));
		reClaimGift.setReClaimCounter(cursor.getInt(4));
		reClaimGift.setReClaimUptateTime(cursor.getLong(5));
		return reClaimGift;
	}
}
