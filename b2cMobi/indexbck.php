
<?php

if(!isset($_COOKIE["mob"])){
  
   header("Location:login.php?logout=logout");
}

require_once 'function.php';
session_start();



if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' ) {
   
   $method = $_POST["method"];
   $mobileno = $_POST["mobileno"];
   $curl_url = "http://panel.pay1.in/apis/receiveWeb/mindsarray/mindsarray/json?method=$method&mobile=$mobileno";
   $res = getCurlAjaxRespose($curl_url);
   
    $res = trim($res);
    $res = trim($res, ")");
    $res = trim($res, "(");
    $res = trim($res, ";");

    $res = substr_replace($res, "", -1);
    echo $res;
    die;
}


$service_url = "http://cdev.pay1.in/index.php/api_new/action/api/true/actiontype/get_all_operators/?";
$curl_response = getCurlRespose($service_url);
$operatorData = objectToArray($curl_response);
$arrayval = array();
foreach ($operatorData as $operatorkey => $operatorval) {
    if (is_array($operatorval)) {
        foreach ($operatorval as $operatorkey => $operatorvalue) {
            if ($operatorkey == "mobile") {
                $arrayval["Mobile"] = $operatorvalue;
            } elseif ($operatorkey == "data") {
                $arrayval["data"] = $operatorvalue;
            } elseif ($operatorkey == "dth") {
                $arrayval["dth"] = $operatorvalue;
            }
            elseif($operatorkey == "postpaid")
            {
                $arrayval["postpaid"] = $operatorvalue;
            }
        }
    }
}



$operatorarray = array();
$datalimit = array();
foreach ($arrayval["data"] as $key => $val)
{
    $operatorarray[$val["opr_code"]] = $val["id"];
    $datalimit[$val["opr_code"]] = array($val["max"],$val["min"],$val["stv"],$val["charges_slab"],$val["product_id"]);
}


$data = json_encode($operatorarray);
$datalimit = json_encode($datalimit);

$postpaidoperatorarray = array();
$postpaidlimit = array();

foreach ($arrayval["postpaid"] as $key => $val)
{
    $postpaidoperatorarray[$val["opr_code"]] = $val["id"];
    $postpaidlimit[$val["opr_code"]] = array($val["max"],$val["min"],$val["stv"],$val["charges_slab"],$val["service_tax_percent"],$val["product_id"]);
    
}

$postpaiddata = json_encode($postpaidoperatorarray);
$postpaidlimit = json_encode($postpaidlimit);

$prepaidoperatorarray = array();
$prepaidlimit = array();

foreach ($arrayval["Mobile"] as $key => $val)
{
    $prepaidoperatorarray[$val["opr_code"]] = $val["id"];
    $prepaidlimit[$val["opr_code"]] = array($val["max"],$val["min"],$val["stv"],$val["charges_slab"],$val["product_id"]);
    
}


$prepaiddata = json_encode($prepaidoperatorarray);
$prepaidlimit = json_encode($prepaidlimit);

$dthoperatorarray = array();
$dthlimit = array();

foreach ($arrayval["dth"] as $key => $val)
{
    $dthoperatorarray[$val["opr_code"]] = $val["id"];
    $dthlimit[$val["opr_code"]] = array($val["max"],$val["min"],$val["stv"],$val["charges_slab"],$val["product_id"]);
    
}



$dthdata = json_encode($dthoperatorarray);
$dthlimit = json_encode($dthlimit);

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Home || PAY1</title>
	<meta charset="utf-8">  
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,width=device-width,user-scalable=no" />
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />

	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/mobile-tablet-style.css">
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style type="text/css">
      div.scroll {
   // background-color: #00FFFF;
    width: 650px;
    height: 300px;
    overflow-y: scroll;
}
  </style>
<!--  var $closestTr = $(this).closest('tr');
    var chkbox = $closestTr.find('td:eq(0)').find(':checkbox');-->
</head>

<?php

if(($_SESSION["longitude"]=="") || ($_SESSION["latitude"]=="")){
   ?>
<body onload="getLocation();getalldeals();">
    <?php } else {  ?>
<body onload="getalldeals();">
    <?php }?>>    
<div>
<img src="images/MainPageImage.jpg" class="homebgIMG">
<!-- navigation -->
<div class="mobilemenu">
 	<p class="mobiletoggle" data-idd="#mobnav"></p>
	<a href="index.html" title="Pay1"><img src="images/logo.jpg" align="logo-image"></a>
</div>
<div class="opacitybg"></div>
<div class="leftnavi" id="mobnav">
	<ul>
		<li>
			<a href="index.html"><img src="images/logo.jpg"></a>
			<strong>Recharge</strong>
		</li>
		<li><a href="#mobile"  class="current"><span class="customicon-mobile"></span> Mobile</a></li>
		<li><a href="#dth"><span class="customicon-dth"></span> DTH</a></li>
		<li><a href="#data"><span class="customicon-dcable"></span> Data Card</a></li>
		<li class="freespace">&nbsp;</li>
		<li><a href="deals.html"><span class="customicon-deals"></span>Deals</a></li>
		<li class="bill freespace"><small>Bill</small></li>
		<li><a href="#postpaid"><span class="customicon-pmobile"></span> Postpaid mobile</a></li>
		<li><a href="#"><span class="customicon-Electricity"></span> Electricity</a></li>
	</ul>
</div>
	<!-- //close navigation -->
<div class="shiftbox">
	<div class="container">
		<div class="row">
                    <div class="col-sm-12">
                    
			<?php include 'header.php'; ?>
                    </div>
		</div>
            
            
<!--            <div id="order" style="display:none;">
                
            </div>-->
                
		<div class="row" id="mobile">
                    <form method="post" action="">
                        <input type="hidden" id="mob_flag" name="mob_flag" value="1">
                        <input type="hidden" id="mob_circle" name="mob_circle" value="">
                        <input type="hidden" id="mob_max" name="mob_max" value="">
                        <input type="hidden" id="mob_min" name="mob_min" value="">
                         <input type="text" id="mob_id" name="mob_id" value="">
			<div class="col-sm-12">
				<div class="welcome showbelow768hideUp768">
					<div class="">
						<p>Welcome to <span>Pay1</span> india's first cash recharge portal.</p>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="col-sm-12"><h2 class="mobilerecharge">Mobile Recharge</h2></div>
			<div class="clearfix"></div>
			<div class="col-sm-6">
				<div class="inputfild">
					<label>Pre-paid Mobile Number</label>
                                        <input type="text" class="form-control" name="mob_number" id="mob_number" maxlength="10" onkeypress="getoperatorvalue(this.value,'mob_provider','mob_amount');return isNumberKey(event)" placeholder="">
				</div>
                            
                            
				<div class="inputfild">
					<label>Service Provider</label>
					<select class="form-control" name="mob_provider" id="mob_provider" onchange="getplanoperatorchange('mob')">
						<option value="">Select Service Provider</option>
                                                <?php foreach($arrayval["Mobile"] as $key => $val){?>
                                                <option value="<?php echo $val["opr_code"] ?>"><?php echo $val["name"] ?></option>
                                                <?php } ?>
						
					</select>
				</div>

				<div class="inputfild">
					<label>Amount</label>
					<input type="text" class="form-control" name="mob_amount" id="mob_amount"onkeypress="return isNumberKey(event);">
                                        <span  class="" onclick="show('showmobplan');">Check Plan</span>
					<div class="checkplans" id="showmobplan">
                                              <div class="sliderowbar">
						<ul class="nav nav-tabs" role="tablist" id ="mob_plan">
						
						</ul>
                                              </div>
						<!-- Tab panes -->
						<div class="tab-content mT20"  id ="mob_plandetails">
    
						</div>
						<!-- close -->
						<div class="text-right">
							<a href="javascript:void(0)" onclick="hide('showmobplan');" title="Close">X &nbsp;</a>
						</div>						
					</div>
				</div>
<!--onclick="return validate('mob_number','mob_provider','mob_amount');"-->
				<div class="inputfild">
					<button type="button" onclick="orderclick('mob');" data-toggle="modal"  data-target=".orderconfirmation" class="btn btn-primary btn-lg btn-block">Proceed</button>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="welcome">

				</div>
			</div>
                    </form>
		</div>
            <div id="dth" class="row" style="display:none;">
                <form method="post" action="">
               <input type="hidden" name="dth_flag" id="dth_flag" value="2">
               <input type="hidden" name="dth_max" id="dth_max" value="">
                <input type="hidden" name="dth_min" id="dth_min" value="">
                <input type="text" name="dth_id" id="dth_id" value="">
                
                <div class="col-sm-12">
				<div class="welcome showbelow768hideUp768">
					<div class="">
						<p>Welcome to <span>Pay1</span> india's first cash recharge portal.</p>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="col-sm-12"><h2 class="mobilerecharge">DTH Recharge</h2></div>
			<div class="clearfix"></div>
			<div class="col-sm-6">
				<div class="inputfild">
					<label>DTH Number</label>
					<input type="text" class="form-control" name="dth_number" id="dth_number" placeholder="">
				</div>
                            
                            
				<div class="inputfild">
					<label>Service Provider</label>
					<select class="form-control" name="dth_provider" id="dth_provider" onchange="getplanoperatorchange('dth')">
						<option value="">Select Service Provider</option>
                                                <?php foreach($arrayval["dth"] as $key => $val){?>
                                                <option value="<?php echo $val["opr_code"] ?>"><?php echo $val["name"] ?></option>
                                                <?php } ?>
						
					</select>
				</div>

				<div class="inputfild">
					<label>Amount</label>
					<input type="text"  name="dth_amount" id="dth_amount" onkeypress="return isNumberKey(event)" class="form-control" placeholder="" >
                                         <span onclick="show('showdthplan');">Check Plans</span>
                                       
					<div class="checkplans" id="showdthplan">
                                            <div class="sliderowbar">
						<ul class="nav nav-tabs" id="dth_plan" role="tablist">
						 
						</ul>
                                            </div>
						<!-- Tab panes -->
						<div class="tab-content mT20" id="dth_plandetails">
						 
						</div>
						<!-- close -->
						<div class="text-right">
                                                   <a href="javascript:void(0);" onclick="hide('showdthplan');" title="Close">X &nbsp;</a>
						</div>						
					</div>
				</div>

				<div class="inputfild">
					<button type="button" data-toggle="modal" onclick="orderclick('dth');" data-target=".orderconfirmation" class="btn btn-primary btn-lg btn-block">Proceed</button>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="welcome">
					
				</div>
			</div>
                </form>
            </div>
            
            <div id="data" style="display:none;">
                <form method="post" action="">
                    <input type="text" name="data_id" id="data_id" value="">
                    <input type="hidden" name="data_max" id="data_max" value="">
                     <input type="hidden" name="data_min" id="data_min" value="">
                     <input type="hidden" id="data_flag" name="data_flag" value="3">
                      <input type="hidden" id="data_circle" name="data_circle" value="">
                
                <div class="col-sm-12">
				<div class="welcome showbelow768hideUp768">
					<div class="">
						<p>Welcome to <span>Pay1</span> india's first cash recharge portal.</p>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="col-sm-12"><h2 class="mobilerecharge">Data Card Recharge</h2></div>
			<div class="clearfix"></div>
			<div class="col-sm-6">
				<div class="inputfild">
					<label>Data Card Number</label>
                                        <input type="text" class="form-control" name="data_number" id="data_number" onkeypress="getdataoperatorvalue(this.value,'data_provider','data_amount')" placeholder="">
				</div>
                            
                            
				<div class="inputfild">
					<label>Service Provider</label>
					<select class="form-control" id="data_provider" name="data_provider" onchange="getplanoperatorchange('data')">
						<option value="">Select Service Provider</option>
                                                <?php foreach($arrayval["data"] as $key => $val){?>
                                                <option value="<?php echo $val["opr_code"] ?>"><?php echo $val["name"] ?></option>
                                                <?php } ?>
						
					</select>
				</div>

				<div class="inputfild">
					<label>Amount</label>
					<input type="text" name="data_amount" id="data_amount" class="form-control" placeholder="" onkeypress="return isNumberKey(event);">
                                        <span onclick="show('showdataplan');">check plan</span>
					<div class="checkplans" id="showdataplan">
                                            <div class="sliderowbar">
						<ul class="nav nav-tabs"  id ="data_plan" role="tablist">
						  
						</ul>
                                            </div>
						<!-- Tab panes -->
						<div class="tab-content mT20" id="data_plandetails">
						  
						</div>
						<!-- close -->
						<div class="text-right">
							<a href="javascript:void(0);" onclick="hide('showdataplan');"title="Close">X &nbsp;</a>
						</div>						
					</div>
				</div>

				<div class="inputfild">
					<button type="button" data-toggle="modal" onclick="orderclick('data');" data-target=".orderconfirmation" class="btn btn-primary btn-lg btn-block">Proceed</button>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="welcome">
					
				</div>
			</div>
            </div>
            <div class="row" id="postpaid" style="display: none;">
                    <form method="post" action="">
                        <input type="hidden" name="post_flag" id="post_flag" value="4">
                         <input type="hidden" name="post_id" id="post_id" value="">
                         <input type="hidden" name="post_min" id="post_min" value="">
                         <input type="hidden" name="post_max" id="post_max" value="">
                         <input type="hidden" name="post_charge" id="post_charge" value="">
                         <input type="hidden" name="post_servicecharge" id="post_servicecharge" value="">
			<div class="col-sm-12">
				<div class="welcome showbelow768hideUp768">
					<div class="">
						<p>Welcome to <span>Pay1</span> india's first cash recharge portal.</p>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="col-sm-12"><h2 class="mobilerecharge">Postpaid Mobile</h2></div>
			<div class="clearfix"></div>
			<div class="col-sm-6">
				<div class="inputfild">
					<label>Mobile Number</label>
                                        <input type="text" class="form-control"  maxlength="10"  id="post_number" name="post_number" onkeypress="getdataoperatorvalue(this.value,'post_provider','post_amount');return isNumberKey(event)" placeholder="">
				</div>
                            
                            
				<div class="inputfild">
					<label>Service Provider</label>
                                        <select class="form-control" name="post_provider" id="post_provider" onchange="getplanoperatorchange('post');">
						<option value="">Select Service Provider</option>
                                                <?php foreach($arrayval["postpaid"] as $key => $val){?>
                                                <option value="<?php echo $val["opr_code"] ?>"><?php echo $val["name"] ?></option>
                                                <?php } ?>
						
					</select>
				</div>

				<div class="inputfild">
					<label>Amount</label>
                                        <input type="text" class="form-control" name="post_amount" id="post_amount" onkeypress="return isNumberKey(event)">

				</div>

				<div class="inputfild">
					<button type="button" id="postpaid" onclick="orderclick('post');"  data-toggle="modal" data-target=".orderconfirmation" class="btn btn-primary btn-lg btn-block">Proceed</button>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="welcome">
					
				</div>
			</div>
                    </form>
		</div>
            </div>
	</div>

	<div class="whychoose">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="row">
						<div class="col-sm-6 text-center">
							<img src="images/why-choose.jpg">
						</div>
						<div class="col-sm-6">
							<div class="whytextbox">
								<h2>Why choose Pay1 over others?</h2>
								<p>
									Pay1 is an app that helps you make your wallet even lighter. It helps you carry rupees without actually carrying it. Bill payments and money transfer has never been that easy before. With a team focusing on delivering a safer, secured and easy experience Pay1 is one of the most powerful digital wallet one can own.
								</p>
								<a href="#">Read More &#x025B8;</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="hutpart">
		<div class="container">
			<div class="row" id ="alldeals">

				
			</div>
		</div>
	</div>

<!-- popup for order confirmation -->
<div class=""  style="display:none;" id="orderconfirmation" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
              <form method="post" action="" id="payment_form">
                  <input type="text" name="recharge_amount" id="recharge_amount" value="">
                    <input type="text" name="recharge_operator" id="recharge_operator" value="">
                    <input type="text" name="recharge_flag" id="recharge_flag" value="">
                    <input type ="text" name="recharge_number" id="recharge_number" value="">
                    <input type ="text" name="payment_option" value="" id="payment_option">
                    <input type="text" name="service_charge" id ="service_charge" value="">
                     <input type="text" name="service_charge_percent" id ="service_charge_percent" value="">
                    <input type="text" name="total_amount" id ="total_amount" value="">
		
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" data-dismiss="modal" type="button" title="Close">
					<span aria-hidden="true">×</span>
					<span class="sr-only">Close</span>
				</button>
                          
				<h4 id="mySmallModalLabel" class="modal-title">
					Order Confirmation
				</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-5">
						<small id="recharge_type"></small>
						<p id ="number"></p>
					</div>
					<div class="col-sm-3">
						<small>Amount</small>
						<p id ="amount"></p>
					</div>
					<div class="col-sm-4">
						<small id ="description"></small>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<small>Service Provider</small>
						<p id ="provider_name"></p>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12 popupdivision">
						<small>Pay now via</small>
					</div>
					<div class="col-sm-6">
						<table>
					  	<tr>
					  		<td><span class="blk-wallet"></span></td>
					  		<td >Balance<br> <strong id="balance"><?php echo ($_SESSION["wallet_balance"]>0)?$_SESSION["wallet_balance"]:0 ?></strong></td>
					  	</tr>
					  </table>
                                            <input type="button" class="common-btn mT10 btn-block"  value="Pay with wallet" id="wallet"  onclick="payment('recharge_amount','recharge_operator','recharge_flag','recharge_number',this.value)">
					  
					</div>
					<div class="col-sm-6">
						<table>
					  	<tr>
					  		<td><span class="blk-atm"></span></td>
					  		<td><small>Pay with your Debit or Credit Card</small></td>
					  	</tr>
					  </table>
                                            <input type="button" class="common-btn mT10 btn-block"    value="Payment Gateway" onclick="payment('recharge_amount','recharge_operator','recharge_flag','recharge_number',this.value)">
					  	
					  <!--</button>-->
					</div>
				</div>
<!--				<div class="row mT15">
					<div class="col-sm-12">
						<small>Have a coupon code?</small>
						<input type="text" class="form-control" placeholder="Redeem">
					</div>
				</div>-->
			</div>
		</div>
                                          </form>

	</div>
</div>


<!-- popup for order success -->
<div class="modal fade in ordersuccess"  id="paymentconfirm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" data-dismiss="modal" type="button" title="Close">
					<span aria-hidden="true">×</span>
					<span class="sr-only">Close</span>
				</button>
				
			</div>
			<div class="modal-body">
<!--                            <iframe  id ="confirmform"></iframe>-->
<!--				<div class="row">
					<div class="col-sm-12 mT20">
						<p class="ordersucHead">Transaction completed succcessfully!</p>
					</div>
					<div class="col-sm-5">
						<small>Pre-Paid Mobile No</small>
						<p>+91 9098434560</p>
					</div>
					<div class="col-sm-3">
						<small>Amount</small>
						<p>Rs. 147</p>
					</div>
					<div class="col-sm-4">
						<small>500MB 3G Plan valid for 30 days.</small>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-5">
						<small>Service Provider</small>
						<p>Reliance</p>
					</div>
					<div class="col-sm-6">
						<small>Balance</small>
						<p>10,000</p>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12 popupdivision">
						<small>&nbsp;</small>
					</div>
					<div class="col-sm-12 mT15">
						<p>If your transaction does not suceed</p>
						<button class="callaskBTN"><span class="popupicons-call"></span> Call Us</button>
						<button class="callaskBTN"><span class="popupicons-ask"></span> Ask Us</button>
						<br><br>
					</div>
				</div>-->
			</div>
		</div>
	</div>
</div>




<!-- popup for transaction faild -->
<div class="modal fade in transactionfaild" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" data-dismiss="modal" type="button" title="Close">
					<span aria-hidden="true">×</span>
					<span class="sr-only">Close</span>
				</button>
				<h4 id="mySmallModalLabel" class="modal-title">
					<span class="transfaildTXT">Failed!</span>
				</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-12 mT20">
						<p class="transfaildTXT">Transaction not completed succcessfully!</p>
					</div>
					<div class="col-sm-5">
						<small>Pre-Paid Mobile No</small>
						<p>+91 9098434560</p>
					</div>
					<div class="col-sm-3">
						<small>Amount</small>
						<p>Rs. 147</p>
					</div>
					<div class="col-sm-4">
						<small>500MB 3G Plan valid for 30 days.</small>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-5">
						<small>Service Provider</small>
						<p>Reliance</p>
					</div>
					<div class="col-sm-6">
						<small>Balance</small>
						<p>10,000</p>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12 popupdivision">
						<small>&nbsp;</small>
					</div>
					<div class="col-sm-12 mT15">
						<p>Kindly contact us for assistance</p>
						<button class="callaskBTN"><span class="popupicons-call"></span> Call Us</button>
						<button class="callaskBTN"><span class="popupicons-ask"></span> Ask Us</button>
						<br><br>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- popup for transaction error -->
<div class="modal fade in transactionerror" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" data-dismiss="modal" type="button" title="Close">
					<span aria-hidden="true">×</span>
					<span class="sr-only">Close</span>
				</button>
				<h4 id="mySmallModalLabel" class="modal-title">
					<span class="transfaildTXT">Error!</span>
				</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-12 mT50 text-center">
						<p class="transfaildTXT">Sorry! We couldn't find it.
						<br>Lorem ipsum is simply dummy text!! </p>
						<br><br><br>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<!-- footer box -->
	<footer>
		<div class="container handmobile">
			<div class="row">
				<div class="col-md-6 row">
						<div class="col-md-6">
							<h4>Mobile Recharge <span class="fooactive show-1"></span></h4>
							<ul class="alllinkshow-1">
								<li><a href="#">Airtel Online Recharge</a></li>
								<li><a href="#">Aircel Online Recharge</a></li>
								<li><a href="#">Vodafone Online Recharge</a></li>
								<li><a href="#">BSNL Online Recharge</a></li>
								<li><a href="#">Tata Docomo GSM Online Recharge</a></li>
								<li><a href="#">Idea Online Recharge</a></li>
								<li><a href="#">Indicom Walky Online Recharge</a></li>
								<li><a href="#">Loop Online Recharge</a></li>
								<li><a href="#">MTNL Delhi Online Recharge</a></li>
								<li><a href="#">Reliance CDMA Online Recharge</a></li>
								<li><a href="#">Reliance GSM Online Recharge</a></li>
								<li><a href="#">Tata Indicom Online Recharge</a></li>
								<li><a href="#">Uninor Online Recharge</a></li>
								<li><a href="#">MTS Online Recharge</a></li>
								<li><a href="#">Videocon Online Recharge</a></li>
								<li><a href="#">Virgin CDMA Online Recharge</a></li>
								<li><a href="#">Virgin GSM Online Recharge</a></li>
								<li><a href="#">Tata Docomo CDMA Online Recharge</a></li>
								<li><a href="#"></a></li>
							</ul>
						</div>
						<div class="col-md-6">
							<h4>Date Card Recharge <span class="fooactive show-2"></span></h4>
							<ul class="alllinkshow-2">
								<li><a href="#">Tata Photon Plus Recharge</a></li>
								<li><a href="#">MTS MBlaze Recharge</a></li>
								<li><a href="#">MTS MBrowse Recharge</a></li>
								<li><a href="#">Reliance NetConnect Recharge</a></li>
								<li><a href="#">Airtel Recharge</a></li>
								<li><a href="#">BSNL Recharge</a></li>
								<li><a href="#">Aircel Recharge</a></li>
								<li><a href="">MTNL Delhi Recharge</a></li>
								<li><a href="">Vodafone Recharge</a></li>
								<li><a href="">Idea Recharge</a></li>
								<li><a href="">Reliance-GSM Recharge</a></li>
								<li><a href="">MTNL Mumbai Recharge</a></li>
								<li><a href="">Tata Photon Whiz Recharge</a></li>
								<li><a href="">T24 Recharge</a></li>
								<li><a href="">Tata Docomo Recharge</a></li>
							</ul>
						</div>
					</div>
				<div class="col-md-6 row">
					<div class="col-md-6">
						<h4>DTH(TV) Recharge <span class="fooactive show-3"></span></h4>
						<ul class="alllinkshow-3">
							<li><a href="#">Airtel Digital Recharge</a></li>
							<li><a href="#">Reliance Digital TV Recharge</a></li>
							<li><a href="#">Dish TV Recharge</a></li>
							<li><a href="#">Tata Sky Recharge</a></li>
							<li><a href="#">Sun Direct Recharge</a></li>
							<li><a href="#">Videocon D2H Recharge</a></li>
						</ul>
						<h4>Postpaid <span class="fooactive show-4"></span></h4>
						<ul class="alllinkshow-4">
							<li><a href="#">Airtel Bill Recharge</a></li>
							<li><a href="#">Loop Bill Recharge</a></li>
							<li><a href="#">BSNL Bill Recharge</a></li>
							<li><a href="#">Tata Docomo GSM Bill Payment</a></li>
							<li><a href="#">Tata Dococmo Bill Payment</a></li>
							<li><a href="#">Idea Bill Payment</a></li>
							<li><a href="#">Vodafone Bill Payment</a></li>
							<li><a href="#">Reliance GSM Bill Payment</a></li>
							<li><a href="#">Reliance CDMA Bill Payment</a></li>
						</ul>
					</div>
					<div class="col-md-6">
						<h4>Dummy Copy <span class="fooactive show-5"></span></h4>
						<ul class="alllinkshow-5">
							<li><a href="#">About Us</a></li>
							<li><a href="#">FAQ</a></li>
							<li><a href="#">Support</a></li>
							<li><a href="#">Contact Us</a></li>
							<li><a href="#">Privacy Policy</a></li>
							<li><a href="#">Sitemap</a></li>
							<li><a href="#">T &amp; C</a></li>
							<li><a href="#">Blog</a></li>
							<li><a href="#">Credits</a></li>
							<li><a href="#">Security Policy</a></li>
						</ul>
						<h4>Mobile <span class="fooactive show-6"></span></h4>
						<ul class="alllinkshow-6">
							<li><a href="">Android App</a></li>
							<li><a href="">Mobile Site</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</footer>

</div><!-- //shiftbox -->
</div>
<script src="js/jquery.min.2.1.1.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    
       var loc = location.search;
        if(loc =="?mobile")
          {
              $("#dth").hide();
              $("#data").hide();
              $("#postpaid").hide();
              $("#mobile").show();
          }
          else if(loc =="?dth")
          {
              $("#mobile").hide();
              $("#data").hide();
              $("#postpaid").hide();
              $("#dth").show();
          }
          else if(loc =="?data")
          {
              $("#mobile").hide();
              $("#postpaid").hide();
              $("#dth").hide();
              $("#data").show();
          }
          else if(loc =="?postpaid")
          {
              $("#mobile").hide();
              $("#data").hide();
              $("#dth").hide();
              $("#postpaid").show();
          }

	var width = $(window).width();
	var height = $(window).height(); 
	var screenTimer = null;

	function detectScreen (){
    $(window).resize(function() {
      height = $(window).height(); 
      width = $(window).width(); 
      getScreen ();
    });

    function getScreen (){
      return {'width': getWidth () };
    }
    screenTimer = setInterval(function(){ getScreen(),50});
	}

	function getWidth(){
		$(location).attr('href');
  	var pathname = window.location;
  	if (pathname=='http://pay1.my-fbapps.info/' || pathname=='http://pay1.my-fbapps.info/index.html') {
    //if (pathname=='file:///E:/My%20Project/PAY1%20-%20Project/index.html'){
  	$('.leftnavi').css('width','220px');
  	$('.shiftbox').css('padding-left','220px');
  	$('.leftnavi ul li:first-child').css('margin-left','-10px');
  	$('.leftnavi ul li strong').css('opacity','1');

    console.log ( 'width: ' + width);
    //$('#width').text(width);
    //return width;
    if(width<=1024) {
    	$('.shiftbox').css('padding-left','0px');
    	$('.leftnavi').css('width','55px');
    	}
    else{
    	$('.shiftbox').css('padding-left','220px');
    	$('.leftnavi').css('width','220px');
    	}
  	}
	}

 	detectScreen ();
    
});



	var xx = setInterval(function(){
  var currentScroll; 
  if (document.documentElement.scrollTop)
    { currentScroll = document.documentElement.scrollTop; }
  else
    { currentScroll = document.body.scrollTop; }
  

  if (currentScroll>='300')
  {
    $('.shiftbox').css('padding-left','0px');
   	$('.leftnavi').css('width','55px');
   	$('.leftnavi ul li:first-child').css('margin-left','-150px');
   	$('.leftnavi ul li strong').css('opacity','0');
  }
  else
  {
    // $('#data').removeClass('hilite');
  }
  
   }, 100); 
   
   $("a").click(function(e) {
        e.preventDefault();
         var operatorid = $(this).attr("href");
         //alert(operatorid);
          if(operatorid =="#mobile")
          {
              $("#dth").hide();
              $("#data").hide();
              $("#postpaid").hide();
              $(operatorid).show();
          }
          else if(operatorid =="#dth")
          {
              $("#mobile").hide();
              $("#data").hide();
              $("#postpaid").hide();
              $(operatorid).show();
          }
          else if(operatorid =="#data")
          {
              $("#mobile").hide();
              $("#postpaid").hide();
              $("#dth").hide();
              $(operatorid).show();
          }
          else if(operatorid =="#postpaid")
          {
              $("#mobile").hide();
              $("#data").hide();
              $("#dth").hide();
              $(operatorid).show();
          }
        
            

        });
        
        
        function getoperatorvalue(value,opertatortype,amount)
        {
        var mobileno = value;
        var methodtype = "getMobileDetails";
        var plandetails = "getPlanDetails";
        var spitoperatortype = (opertatortype.split("_"));
        var operatorobj = JSON.parse(<?php echo json_encode($prepaiddata); ?>);
        var limit = JSON.parse(<?php echo json_encode($prepaidlimit); ?>);
        if(mobileno.length == 9){
        $.ajax({
        url: "index.php",
        type:"post",
        dataType: "json",
        data:{mobileno : value,method :methodtype},
        success:function(data)
        {
            
          if(data[0].status=="success")
          {
            $("#"+opertatortype).val(data[0].details.operator);
            $("#"+amount).focus();
            var operatorid = operatorobj[data[0].details.operator];
            var circle =  data[0].details.area;
             $("#mob_circle").val(circle);
             $("#mob_max").val(limit[data[0].details.operator][0]);
             $("#mob_min").val(limit[data[0].details.operator][1]);
             $("#mob_id").val(operatorid);
              checkplan(plandetails,operatorid,circle,spitoperatortype[0]);
          }
          else
          {
              $("#"+opertatortype).val('');
          }
            
        }
        
        });
       }
        }
        
        function getdataoperatorvalue(value,opertatortype,amount)
        {
            var spitoperatortype = (opertatortype.split("_"));
            if(spitoperatortype[0]=="post"){
                var operatorobj = JSON.parse(<?php echo json_encode($postpaiddata); ?>);
                var limit = JSON.parse(<?php echo json_encode($postpaidlimit); ?>);
            }
            else{
                var operatorobj = JSON.parse(<?php echo json_encode($data); ?>);
                var limit = JSON.parse(<?php echo json_encode($datalimit); ?>);
            }
            var mobileno = value;
            var methodtype = "getMobileDetails";
            var plandetails = "getPlanDetails";
            if(mobileno.length == 9){
            $.ajax({
            url: "index.php",
            type:"post",
            dataType: "json",
            data:{mobileno : value,method:methodtype},
            success:function(data)
            {
              if(data[0].status=="success")
              {
                 
                var operatorval = data[0].details.operator;
                $("#"+opertatortype).val(operatorval);
                $("#"+spitoperatortype[0]+"_id").val(operatorobj[operatorval]);
                $("#"+spitoperatortype[0]+"_max").val(limit[operatorval][0]);
                $("#"+spitoperatortype[0]+"_min").val(limit[operatorval][1]);
                $("#"+spitoperatortype[0]+"_charge").val(limit[operatorval][3]);
                var operatorid = operatorobj[operatorval];
                $("#"+amount).focus();
                var circle =  data[0].details.area;
                $("#data_circle").val(circle);
                //alert(circle);
                if(spitoperatortype[0]!='post'){
                checkplan(plandetails,operatorid,circle,spitoperatortype[0]);
               }
              }
              else
              {
                  $("#"+opertatortype).val('');
              }

            }
        
        });
    }
        }
        
        function checkplan(plandetails,operator,circletype,operatortype)
        {
         var plandetails = "getPlanDetails";
         var operatorid = operator;
         var circle = circletype;
         var html = "";
         var associativeArray = new Array();
         var plantype = new Array();
         $.ajax({
            url: "request.php",
            type:"post",
            dataType: "json",
            data:{plandetails:plandetails,operatorid:operatorid,circle:circle},
            success:function(data)
            {
               $.each(data, function( key, value) {
               $.each(value, function(k,v){
              if(typeof v == "object")
              {
                    $.each(v, function(pkey,pval){
                        i = 0;
                        $.each(pval.plans, function(a,b){
                            if(a!="None"){
                                 $("#"+operatortype+"_plandetails").append("no details found");
                            var classname = (i==0)?"active":"";
                            html+='<li class='+classname+'><a href=#'+a+' role="tab" data-toggle="tab">'+a+'</a></li>';
                             }
                            var newElement = {};
                            newElement[a] = b;
                            associativeArray.push(newElement);
                            plantype.push(a);
                            i++;
                        });
                        
                    });
                }
                  
               });
 
                });
              
               $("#"+operatortype+"_plan").append(html);
               var htmlbody = '';
               var htmlcontent = '';
               var tabclass = '';
               j = 0;
              $.each(plantype,function(plankey,planval){
                  if(planval!="None"){
                 var tabclass = (j== 0)? "tab-pane active scroll":"tab-pane scroll";
               htmlcontent +='<div class="'+tabclass+'"  height="100" width="300" id='+planval+'><div class=""><table class="table table-hover"><thead><tr><th>Price</th><th>Validity</th><th>Description</th></tr></thead><tbody>'
               $.each(associativeArray,function(k,v){
                    $.each(v,function(key,val){
                        if(key == planval ){ 
                             $.each(val,function(datakey,dataval){ 
                                 var planamt = "\'" + dataval.plan_amt + "\'";
                                 var planvalidity = "\'"+dataval.plan_validity +"\'";
                                 var plandesc = "\'"+dataval.plan_desc+"\'";
                                 var type = "\'"+operatortype+"\'";
                                 htmlcontent+='<tr onclick="getPlandescription('+planamt+','+planvalidity+','+plandesc+','+type+')"><td>'+dataval.plan_amt+'</td><td>'+dataval.plan_validity+'</td><td>'+dataval.plan_desc+'</td></tr>'
                                 });
                                 htmlcontent+='</tbody></table></div></div>';
                                 $("#"+operatortype+"_plandetails").append(htmlcontent);
                                 htmlcontent = '';
                        }
                      
                  });
                 
                    });
                     j++;
                     }
                     else
                     {
                      htmlcontent+="<table><tr><td colspan='3' class='tab-content mT20' width='670' align='center'><b>No Plans Found</b></td></tr></table>";
                      $("#"+operatortype+"_plandetails").append(htmlcontent);
                     }
                    
                  });
              
            }
         
        });
    }
      
     function getplanoperatorchange(type){
      
      var operatorid = $("#"+type+"_provider").val();
    //  alert(operatorid);
      var plandetails = "getPlanDetails";
      if(type=="dth"){
      var circle = "all";
      $("#dth_plan").html('');
      $("#dth_plandetails").html('');
       var limit = JSON.parse(<?php echo json_encode($dthlimit); ?>);
       var operatorobj = JSON.parse(<?php echo json_encode($dthdata); ?>);
       alert(operatorobj[operatorid]);
       $("#"+type+"_max").val(limit[operatorid][0]);
       $("#"+type+"_min").val(limit[operatorid][1]);
       $("#"+type+"_id").val(operatorobj[operatorid]);
       //alert(operatorobj[operatorid]);
       checkplan(plandetails,limit[operatorid][4],circle,type);
      }
       else if(type=="mob"){
           
            $("#mob_plan").html('');
            $("#mob_plandetails").html('');  
            var circle = $("#"+type+"_circle").val();
            var limit = JSON.parse(<?php echo json_encode($prepaidlimit); ?>);
            var operatorobj = JSON.parse(<?php echo json_encode($prepaiddata); ?>);
            $("#"+type+"_max").val(limit[operatorid][0]);
            $("#"+type+"_min").val(limit[operatorid][1]);
             $("#"+type+"_id").val(operatorobj[operatorid]);
            checkplan(plandetails,limit[operatorid][4],circle,type);
     
       }
       else if(type=="data"){
           
            $("#data_plan").html('');
            $("#data_plandetails").html('');  
            var circle = $("#"+type+"_circle").val();
            var limit = JSON.parse(<?php echo json_encode($datalimit); ?>);
             var operatorobj = JSON.parse(<?php echo json_encode($data); ?>);
            $("#"+type+"_id").val(operatorobj[operatorid]);
            $("#"+type+"_max").val(limit[operatorid][0]);
            $("#"+type+"_min").val(limit[operatorid][1]);
            checkplan(plandetails,operatorobj[operatorid],circle,type);
     
       }
       else if(type=="post")
       {
            var limit = JSON.parse(<?php echo json_encode($postpaidlimit); ?>);
            var operatorobj = JSON.parse(<?php echo json_encode($postpaiddata); ?>);
            $("#"+type+"_id").val(operatorobj[operatorid]);
            $("#"+type+"_max").val(limit[operatorid][0]);
            $("#"+type+"_min").val(limit[operatorid][1]);
            $("#"+type+"_charge").val(limit[operatorid][3]);
            $("#"+type+"_servicecharge").val(limit[operatorid][4]);
            $("#service_charge_percent").val(limit[operatorid][4]);
       }
       
      
     
     }
      
      function getPlandescription(amt,validity,desc,type)
      {
      
           var mobnumber = $("#"+type+"_number").val();
           $("#"+type+"_amount").val(amt);
           $("#number").html(mobnumber);
           $("#amount").html(amt);
           if(desc!=''){
           $("#description").html(desc);
           }
           if(type =="mob"){
               $("#recharge_type").html("Prepaid Mobile Number");
           }
           else if(type=="post"){
               $("#recharge_type").html("Postpaid Mobile Number");
           }
           else if(type=="data"){
                $("#recharge_type").html("Data Card Number");
           
           }
           else if(type=="dth"){
                  $("#recharge_type").html("DTH Number");
           }
           $("#provider_name").html($("#"+type+"_provider option:selected").text());
           $("#recharge_amount").val(amt);
           $("#recharge_flag").val($("#"+type+"_flag").val());
           $("#recharge_number").val(mobnumber);
           var rechargeoperator = $("#"+type+"_id").val();
           $("#recharge_operator").val(rechargeoperator);
                if(desc!=""){
                   // alert(type);
                    ///alert("show"+type+"plan");
                  hide("show"+type+"plan");

            }
           
           
      }
      
      function orderclick(type){
      
          var amt = $("#"+type+"_amount").val();
          var number = $("#"+type+"_number").val();
          var operator = $("#"+type+"_id").val();
          var type = type;
          var maxlimit = $("#"+type+"_max").val();
          var minlimit = $("#"+type+"_min").val();
          if(type=="post")
          {
               var charge = $("#"+type+"_charge").val();
               var servicechrg = charge.split(",");
               var mincharge = servicechrg[0].split(":");
               var maxcharge = servicechrg[1].split(":");
               var service_charge_percent = $("#service_charge_percent").val();
               
               if(parseInt(amt)>parseInt(mincharge[0]))
               {
                   var totalamt = (parseInt(amt)+parseInt(mincharge[1])+parseInt(service_charge_percent));
                   $("#service_charge").val(parseInt(mincharge[1]));
                   $("#total_amount").val(totalamt);
               }
               else
               {
                   var totalamt = (parseInt(amt)+parseInt(maxcharge[1])+parseInt(service_charge_percent));
                   $("#service_charge").val(parseInt(maxcharge[1]));
                   $("#total_amount").val(totalamt);
               }
             
          }
           if(number=='')
           {
               alert("Number can not be blank");
               $("#"+type+"_number").focus();
               $("#orderconfirmation" ).removeClass("modal fade in orderconfirmation" );
               // $("#orderconfirmation" ).hide();
               return false;
           }
           else if(operator=='')
           {
               alert("Service Provider can not be blank");
               $("#"+type+"_provider").focus();
                $("#orderconfirmation" ).removeClass("modal fade in orderconfirmation" );
                
               return false;
           }
           else if(amt=='')
           {
               alert("Amount can not be blank");
               $("#"+type+"_amount").focus();
               $("#orderconfirmation" ).removeClass( "modal fade in orderconfirmation" );
               return false;
           }
           else if((parseInt(amt)!='') && (parseInt(amt)>parseInt(maxlimit))){
               
               alert("Amount can not be greater than  "+maxlimit);
                $("#"+type+"_amount").val('');
                $("#"+type+"_amount").focus();
                $("#orderconfirmation" ).removeClass( "modal fade in orderconfirmation" );
               return false;
           }
           else if((parseInt(amt)!='') && (parseInt(amt)<parseInt(minlimit))){
               
                alert("Amount can not be less than  "+minlimit);
                $("#"+type+"_amount").val('');
                $("#"+type+"_amount").focus();
                $("#orderconfirmation" ).removeClass( "modal fade in orderconfirmation" );
               return false;
           }
           else{
              $("#orderconfirmation" ).addClass( "modal fade in orderconfirmation" );
              getPlandescription(amt,operator,"",type);
           }
          
          
      }
      
      function isNumberKey(evt){
         var charCode = (evt.which) ? evt.which : evt.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true
     }
  
//
//        $("#wallet").click(function(){
//          var balance = $("#balance").html();
//          if(balance==0){
//              alert("Not Sufficient balance in wallet");
//              return false;
//          }
//          else{
//              $("#payment_option").val("Wallet");
//               $("#payment_form").submit();
//          }
// 
//});



function payment(amt,opt,flag,number,option)
{
  
    var amt = $("#"+amt).val();
    var operator = $("#"+opt).val();
    var flag  = $("#"+flag).val();
    var number = $("#"+number).val();
    var option = option;
    
    $("#orderconfirmation").hide();
    
    if(option=="Payment Gateway")
    {
        $("#payment_option").val('online');
    }
    else
    {
         $("#payment_option").val('wallet');
    }
    var paymentmode = $("#payment_option").val();
    
    if(flag=="1" || flag=="3"){  
    var url = "http://cdev.pay1.in/index.php/api_new/action/api/true/actiontype/recharge/?";
 $.ajax({
            url: url,
            type:"GET",
            data:{mobile_number:number,
                  operator:operator,
                  flag:flag,
                  amount:amt,
                  recharge:"1",
                  paymentopt:paymentmode,
                  payment : "1",
                  res_format : "jsonp"
                 },
            timeout: 50000,
            dataType: "jsonp",
            jsonpCallback: 'callback',
            crossDomain: true,
            success:function(data){
             
                
                if(paymentmode=="wallet"){
//                localStorage.setItem("closing_balance",data.description.closing_balance);  
//                localStorage.setItem("actual_price",data.deal[0]["actual_price"]); 
//                localStorage.setItem("category",data.deal[0]["category"]);
//                localStorage.setItem("category_id",data.deal[0]["category_id"]);
//                localStorage.setItem("dealname",data.deal[0]["dealname"]);
//                localStorage.setItem("discount",data.deal[0]["discount"]);
//                localStorage.setItem("distance",data.deal[0]["distance"]);
//                localStorage.setItem("id",data.deal[0]["id"]);
//                localStorage.setItem("img_url",data.deal[0]["img_url"]);
//                localStorage.setItem("latitude",data.deal[0]["latitude"]);
//                localStorage.setItem("longitude",data.deal[0]["longitude"]);
//                localStorage.setItem("offer_price",data.deal[0]["offer_price"]);
//                localStorage.setItem("stock_sold",data.deal[0]["stock_sold"]);
//                localStorage.setItem("total_stock",data.deal[0]["total_stock"]);
//                localStorage.setItem("total_stock",data.deal[0]["total_stock"]);
//                  location.reload();

                  updatebalance();
               
               }
               
               else{
               
                localStorage.setItem("data",data.description.form_content);
                var url = "content.php?data=";
                window.location.href = encodeURI(url);
              
            }},
            error: function (xhr,error) {
              
          },  
         
            });
            }
            
            else if(flag=="2"){  
            var url = "http://cdev.pay1.in/index.php/api_new/action/api/true/actiontype/recharge/?";
            $.ajax({
            url: url,
            type:"GET",
            data:{subscriber_id:number,
                  operator:operator,
                  flag:flag,
                  amount:amt,
                  recharge:"1",
                  paymentopt:paymentmode,
                  payment : "1",
                  res_format : "jsonp"
                 },
            timeout: 50000,
            dataType: "jsonp",
            jsonpCallback: 'callback',
            crossDomain: true,
            success:function(data){
               
                localStorage.setItem("data",data.description.form_content);
                var url = "content.php?data=";
                window.location.href = encodeURI(url);
        
            },
            error: function (xhr,error) {
              
          },  
         
            });
            }
            else if(flag=="4")
            {
                var url = "http://cdev.pay1.in/index.php/api_new/action/api/true/actiontype/recharge/?";
                var service_charge = $("#service_charge").val();
                var totalamt = $("#total_amount").val();
                var service_charge_percent = $("#service_charge_percent").val();
                $.ajax({
            url: url,
            type:"GET",
            data:{mobile_number:number,
                  operator:operator,
                  flag:flag,
                  base_amount:amt,
                  service_charge:service_charge,
                  amount:totalamt,
                  payment_flag:"1",
                  service_tax:service_charge_percent,
                  billpayment : "1",
                  paymentopt:paymentmode,
                  payment:"1",
                  res_format : "jsonp"
                 },
            timeout: 50000,
            dataType: "jsonp",
            jsonpCallback: 'callback',
            crossDomain: true,
            success:function(data){
               
                localStorage.setItem("data",data.description.form_content);
                var url = "content.php?data=";
                window.location.href = encodeURI(url);
        
            },
            error: function (xhr,error) {
              
          },  
         
            });
            }
            
}

function updatebalance(){
    var url = "http://cdev.pay1.in/index.php/api_new/action/api/true/actiontype/check_bal";
       $.ajax({
            url: url,
            type:"GET",
            data:{
                  res_format : "jsonp"
                 },
            timeout: 50000,
            dataType: "jsonp",
            jsonpCallback: 'callback',
            crossDomain: true,
            success:function(data){
            if(data.status=="success"){
            $.ajax({
            url: "info.php",
            type:"POST",
            data:{
                 walletbal : data.description.account_balance,
                 },
            timeout: 50000,
            dataType: "json",
            success:function(data){
              if(data.status=="success")
              {
                  location.reload();
              }
            },
            error: function (error) {
            }
            });
  
            }
            else
            {
                $("#couponcode").html(data.description);
            }
            }
            });
}


   $(document).ready(function(){
      var url = "http://cdev.pay1.in/index.php/api_new/action/api/true/actiontype/check_bal";
      var html = "";
       $.ajax({
            url: url,
            type:"GET",
            data:{
                  res_format : "jsonp"
                 },
            timeout: 50000,
            dataType: "jsonp",
            jsonpCallback: 'callback',
            crossDomain: true,
            success:function(data){
                if(data.deal[0]['img_url']==""){
                var imageurl = "images/bighut.jpg";
                }
                else{
                    var imageurl =data.deal[0]['img_url'];
                }
                html+="<div><p class='hideonmobile'>Welcome to <span>Pay1</span> india's first cash recharge portal.</p><h5>Top Selling Deals</h5></div>"
		html+="<div class='welcomehutbox'>"
		html+="<img class='img-responsive' src="+imageurl+">"
		html+="<ul class='hutLIST clearfix'>"
		html+="<li><span>"+data.deal[0]["dealname"]+"</span></li>";
		html+="<li><del>&nbsp; Rs. "+data.deal[0]["actual_price"]+" &nbsp;</del><br><strong>Rs."+data.deal[0]["offer_price"]+"</strong></li>";						
		html+="<li><small>"+data.deal[0]["discount"]+"% Discount</small><small></small><br></li></ul>";				
		html+="</div>";
		html+="<h6>Scroll to Check Deals</h6>";
                $(".welcome").append(html);
            }

            });
            

   
       
});


function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else { 
          alert("Geolocation is not supported by this browser.");
    }
}

function showPosition(position) {
   
       var latitude = position.coords.latitude;
       var longitude = position.coords.longitude;
       var serviceurl = "http://b2c.pay1.in/index.php/api_new/action/api/true/actiontype/get_deals/?";
     
      $.ajax({
            url: "info.php",
            type:"POST",
            data:{
                 latitude : latitude,
                 longitude: longitude
                 },
            dataType:"json",
            success:function(data){
              if(data.status=="success")
              {
                 //  getalldeals();
                  location.reload();
                 

              }
            },
            error: function (error) {
            }
            });
 
}


function getalldeals()
{
      var serviceurl = "http://b2c.pay1.in/index.php/api_new/action/api/true/actiontype/get_deals/?";
      var latitude = "<?php echo $_SESSION["latitude"]; ?>";
      var longitude = "<?php echo $_SESSION["longitude"]; ?>";
      var html = "";
      
         $.ajax({
            url: serviceurl,
            type:"GET",
            data:{
                 latitude : latitude,
                 longitude: longitude,
                 res_format : "jsonp"
                 },
          
            timeout: 50000,
            dataType: "jsonp",
            jsonpCallback: 'callback',
            crossDomain: true,
            success:function(dealsdata){
              if(dealsdata.status=="success")
              {  
                 i=1;
                 $.each(dealsdata.description,function(dealkey,dealvalue){
                     if(i<=6){
                          var imageurl = dealvalue.img_url;
                          html+="<div class='col-md-4'><div class='hutbox'><img class='img-responsive' src='"+imageurl+"'>"
                          html+="<ul class='hutLIST clearfix'><li><span>"+dealvalue.dealname+"</span></li>"
			  html+="<li><del>&nbsp; Rs. "+dealvalue.actual_price+" &nbsp;</del><br><strong>Rs."+dealvalue.offer_price+"</strong></li>"
		          html+="<li><small>"+dealvalue.discount+"% Discount</small><br><small>"+dealvalue.total_stock+"Bought this</small></li></ul></div></div>"
				
                       }
              
             
                 i++;   
              });
              //console.log(html);
              $("#alldeals").append(html);
              }},
            error: function (error) {
            }
            });
    
}



</script>


<!-- <div id="data">&nbsp;</div> -->
</body>
</html>