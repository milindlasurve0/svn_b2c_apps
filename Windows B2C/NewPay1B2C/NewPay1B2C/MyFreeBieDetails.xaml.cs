﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Windows.Media.Imaging;
using System.Windows.Input;
using System.ComponentModel;
using System.Windows.Media;

namespace NewPay1B2C
	{
	public partial class MyFreeBieDetails : PhoneApplicationPage
		{
		List<string> myDealList;
		public  JArray locations;
		ProgressBarManager ProgressBarManager;
		public static string DEAL_DATA = "dealdata";
		public static List<string> addressList;
		public MyFreeBieDetails ()
			{

			InitializeComponent ();

			GoogleAnalytics.EasyTracker.GetTracker ().SendView ("My Gifts Details");
			myDealList = new List<string> ();
			ProgressIndicator prog = new ProgressIndicator ();
			ProgressBarManager = new ProgressBarManager ();
			SystemTray.SetProgressIndicator (this, prog);
			ProgressBarManager.Hook (prog);
			string data = PhoneApplicationService.Current.State[MyGifts.DEAL_DATA].ToString ();
			var jObj = (JObject)JsonConvert.DeserializeObject (data);
			string id=jObj["deal_id"].ToString();
			addressList = new List<string> ();
			var length = Constant.getUserNumber ().Length;
			var result = new String ('X', length - 4) + Constant.getUserNumber ().Substring (length - 4);

			textMobile.Text = " Mobile number: " + result;
			textCode.Text = " Voucher code: " + jObj["code"].ToString ();
			textDealsDesc.Text = jObj["deal_name"].ToString ();
			imageDeal.Source=new BitmapImage(new Uri(jObj["img_url"].ToString(),UriKind.RelativeOrAbsolute));
			getDealsTask (id);
			}

		private async void getDealsTask (string id)
			{
			ProgressBarManager.Show (13, "Please wait...", false, 10);
			// Constant.SetProgressIndicator(true);
			var values = new List<KeyValuePair<string, string>> ();
			values.Add (new KeyValuePair<string, string> ("actiontype", "get_deal_details"));
			values.Add (new KeyValuePair<string, string> ("id", id));
			values.Add (new KeyValuePair<string, string> ("latitude", HomePage.latti));
			values.Add (new KeyValuePair<string, string> ("longitude", HomePage.longi));
			values.Add (new KeyValuePair<string, string> ("api", "true"));
			FormUrlEncodedContent values1 = new FormUrlEncodedContent (values);

			Task<string> response = Constant.postParamsAndgetResponse (Constant.B2C_URL, values1);

			string result = await response;

			if (string.IsNullOrEmpty (result))
				{
				NavigationService.Navigate (new Uri ("/NoConnection.xaml?", UriKind.Relative));
				}
			else
				{
				var dict = (JObject)JsonConvert.DeserializeObject (result);
				string status = dict["status"].ToString ();

				if (status.Equals ("success"))
					{
					var description = dict["description"];
					var deal_detail = description["deal_detail"].First();
					var offer_detail = description["offer_detail"].First();
					var location_details = description["location_detail"];

					

					freeBieName.Text = offer_detail["name"].ToString ();
					freeBieSmlDesc.Text = offer_detail["short_desc"].ToString ();
					htmlString = deal_detail["content_txt"].ToString ();
					var htmlScript = "<script>function getDocHeight() { " +
			  "return document.getElementById('pageWrapper').offsetHeight;" +
			  "}" +
			  "function SendDataToPhoneApp() {" +
			  "window.external.Notify('' + getDocHeight());" +
			  "}</script>";

					string newHtml = "<html><head>" + htmlScript + "<style>@font-face {font-family: Normal;src: url('/Fonts/EncodeSansNormal-100-Thin.ttf') format('truetype')}"
																	  + "@font-face {font-family: Narrow;src: url('/Fonts/EncodeSansNarrow-500-Medium.ttf') format('truetype')}"
																	  + "body {margin: 0px;padding:45px;text-align:justify;opacity:0.5;background: linear-gradient(to bottom, #000000, #000000);}"
																	  + "h1{font-size:24px;font-family:'Normal';margin:0;padding:10px 0 5px 0;color:#3f3f40;}"
																	  + "h2{font-size:14px;font-family:'Narrow';margin:0;padding:10px 0 5px 0;color:#513D98;}"
																	  + "h3{font-size:13px;font-family:'Narrow';margin:0;padding:10px 0 5px 0;}"
																	  + "ol{margin:0 0 10px 0;padding-left:20px;}"
																	  + "li{font-size:13px;margin:0 0 2px 0;font-family:'Normal';}</style></head>" +
							"<body style=\"margin:0px;padding:0px;background-color:transparent;\" " +
							"onLoad=\"SendDataToPhoneApp()\">" +
							"<div id=\"pageWrapper\" style=\"width:100%; background-color: transparent;" +
							"\">" + htmlString + "</div></body></html>";


					webBrowser1.ApplyTemplate ();
					webBrowser1.NavigateToString (newHtml);

					webBrowser1.ScriptNotify +=
	new EventHandler<NotifyEventArgs> (wb1_ScriptNotify);
					

					
					foreach (var location in location_details)
						{
						textSmlAdrs.Text = location["address"].ToString ();
						string distance = location["distance"].ToString ();
						double dis = Convert.ToDouble (distance);
						if (dis < 1)
							{
							dis = dis * 1000;
							distance = dis.ToString ("#.##") + " \n(M)";
							}
						else
							{
							distance = dis.ToString ("#.##") + " \n(KM)";
							}
						textDis.Text = distance;
						}
					int loc = location_details.Count ();
					addressList.Add (location_details.ToString ());
					
					locationPanel.Tap += (s, e) =>
					{
						PhoneApplicationService.Current.State[Constant.SHOP_LOCATION_DATA] ="0";
						PhoneApplicationService.Current.State[Constant.REQUEST_FOR] = Constant.MYFREEBIE;
						NavigationService.Navigate (new Uri ("/DealLocator.xaml?", UriKind.RelativeOrAbsolute));
					};

					}
				else
					{
					Constant.checkForErrorCode (result);
					}
				}
			
			ProgressBarManager.Hide (13);
			}

		private void web_loaded (object sender, RoutedEventArgs e)
			{
			SolidColorBrush scb = new SolidColorBrush (Colors.Transparent);
			webBrowser1.Background = scb;
			
			}

		string htmlString;
		private void webBrowser1_Loaded (object sender, RoutedEventArgs e)
			{
			WebBrowser webBrowser1 = sender as WebBrowser;
			webBrowser1.ApplyTemplate ();
			string na = webBrowser1.Name;
			webBrowser1.NavigateToString (htmlString);
			}

		private void wb1_ScriptNotify (object sender, NotifyEventArgs e)
			{
			WebBrowser webBrowser1 = sender as WebBrowser;
			webBrowser1.Height = Convert.ToDouble (e.Value) * 0.50;
			}






		private void buttonImage_Click_Back (object sender, RoutedEventArgs e)
			{
			NavigationService.GoBack ();
			}

		}
	}