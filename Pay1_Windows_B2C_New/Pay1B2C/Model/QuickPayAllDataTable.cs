﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pay1B2C.Model
    {
    class QuickPayAllDataTable
        {

        //public string FreeBieId { get; set; }
        public string id { get; set; }
        public string flag { get; set; }
        public string name { get; set; }
        public string amount { get; set; }
        public string users_id { get; set; }
        public string stv { get; set; }
        public string operator_id { get; set; }
        public string product { get; set; }
        public string number { get; set; }
        public string delegate_flag { get; set; }
        public string missed_number { get; set; }
        public string confirmation { get; set; }
        public string operator_name { get; set; }
        public string transaction_flag { get; set; }


        public string operator_code { get; set; }
        public string datetime { get; set; }

        public QuickPayAllDataTable()
            {
            }
        public QuickPayAllDataTable(string id1, string flag1, string amount1, string users_id1, string stv1, string operator_id1, string product1, string number1, string delegate_flag1, string missed_number1, string confirmation1, string operator_name1, string transaction_flag1, string operator_code1, string datetime1, string name1)
            {
            //id flag name amount users_id max_quantity stv operator_id product number delegate_flag missed_number confirmation operator_name transaction_flag operator_code datetime name name validity max_quantity status short_desc long_desc allow location_detail freeBieUpdateTime)
            id=id1;
            flag=flag1;
            name=name1;
            amount = amount1;
            users_id = users_id1;

            stv = stv1;
            operator_id = operator_id1;

            product = product1;
            number = number1;
            delegate_flag = delegate_flag1;

            missed_number = missed_number1;
            confirmation = confirmation1;
            operator_name = operator_name1;
            transaction_flag = transaction_flag1;
            operator_code = operator_code1;
            datetime = datetime1;
            name = name1;

            }


        }
    }
