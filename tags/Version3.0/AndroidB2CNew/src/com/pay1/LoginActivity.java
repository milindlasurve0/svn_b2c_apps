package com.pay1;

import java.net.URLEncoder;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.SQLException;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.google.android.gcm.GCMRegistrar;
import com.larvalabs.svgandroid.SVGParser;
import com.pay1.constants.Constants;
import com.pay1.customviews.MyProgressDialog;
import com.pay1.databasehandler.QuickPay;
import com.pay1.databasehandler.QuickPayDataSource;
import com.pay1.requesthandler.RequestClass;
import com.pay1.servicehandler.GPSTracker;
import com.pay1.servicehandler.LoginService;
import com.pay1.utilities.EditTextValidator;
import com.pay1.utilities.MyLocation;
import com.pay1.utilities.MyLocation.LocationResult;
import com.pay1.utilities.Utility;

public class LoginActivity extends Activity {

	private static final String SCREEN_LABEL = "Login Screen";
	private EasyTracker easyTracker = null;

	private final String TAG = "Login";

	private TextView textView_Title, textView_Message, textView_TitleMobile,
			textView_TitlePin, textView_ForgotPin;
	private EditText editText_Mobile, editText_Pin;
	private ImageView imageView_Close;
	private Button button_Login;
	private Double longitude, latitude;
	private String uuid, version, manufacturer;
	private boolean flag = false;
	private QuickPayDataSource quickPayDataSource;

	// private LinearLayout linearLayout_Parent;

	@SuppressLint("NewApi")
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.login_activity);
		try {
			quickPayDataSource = new QuickPayDataSource(LoginActivity.this);
			easyTracker = EasyTracker.getInstance(LoginActivity.this);
			easyTracker.set(Fields.SCREEN_NAME, SCREEN_LABEL);

			Typeface Reguler = Typeface.createFromAsset(getAssets(),
					"Roboto-Regular.ttf");

			try {
				Bundle bundle = getIntent().getExtras();
				if (bundle != null)
					flag = bundle.getBoolean("AUTO", true);
			} catch (Exception e) {
				flag = false;
			}
			textView_Title = (TextView) findViewById(R.id.textView_Title);
			textView_Title.setTypeface(Reguler);
			textView_Title.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					finish();
				}
			});
			textView_Message = (TextView) findViewById(R.id.textView_Message);
			textView_Message.setTypeface(Reguler);
			textView_TitleMobile = (TextView) findViewById(R.id.textView_TitleMobile);
			textView_TitleMobile.setVisibility(View.GONE);
			textView_TitlePin = (TextView) findViewById(R.id.textView_TitlePin);
			textView_TitlePin.setVisibility(View.GONE);
			textView_ForgotPin = (TextView) findViewById(R.id.textView_ForgotPin);
			textView_ForgotPin.setTypeface(Reguler);
			textView_ForgotPin.setPaintFlags(textView_ForgotPin.getPaintFlags()
					| Paint.UNDERLINE_TEXT_FLAG);
			textView_ForgotPin.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent intent = new Intent(LoginActivity.this,
							ForgotPasswordActivity.class);
					startActivity(intent);
				}
			});

			editText_Mobile = (EditText) findViewById(R.id.editText_Mobile);
			editText_Mobile.setTypeface(Reguler);
			InputMethodManager imm = (InputMethodManager) LoginActivity.this
					.getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(editText_Mobile.getWindowToken(),
					InputMethodManager.HIDE_IMPLICIT_ONLY);

			editText_Pin = (EditText) findViewById(R.id.editText_Pin);
			editText_Pin.setTypeface(Reguler);

			editText_Mobile.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					// TODO Auto-generated method stub
					if (editText_Mobile.getText().toString().trim().length() != 0) {
						textView_TitleMobile.setVisibility(View.GONE);
					} else {
						textView_TitleMobile.setVisibility(View.GONE);
					}
					EditTextValidator
							.hasText(LoginActivity.this, editText_Mobile,
									Constants.ERROR_MOBILE_BLANK_FIELD);
					if (editText_Mobile.length() == 10) {
						editText_Pin.requestFocus();
						Utility.setMobileNumber(LoginActivity.this,
								Constants.SHAREDPREFERENCE_MOBILE,
								editText_Mobile.getText().toString().trim());
					}
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					// TODO Auto-generated method stub

				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

				}
			});
			editText_Mobile.setText(Utility.getMobileNumber(LoginActivity.this,
					Constants.SHAREDPREFERENCE_MOBILE) != null ? Utility
					.getMobileNumber(LoginActivity.this,
							Constants.SHAREDPREFERENCE_MOBILE) : "");

			editText_Pin.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					// TODO Auto-generated method stub
					if (editText_Pin.getText().toString().trim().length() != 0) {
						textView_TitlePin.setVisibility(View.GONE);
					} else {
						textView_TitlePin.setVisibility(View.GONE);
					}

					EditTextValidator.hasText(LoginActivity.this, editText_Pin,
							Constants.ERROR_LOGIN_PIN_BLANK_FIELD);

				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					// TODO Auto-generated method stub

				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

				}
			});

			editText_Pin
					.setOnEditorActionListener(new TextView.OnEditorActionListener() {

						@Override
						public boolean onEditorAction(TextView v, int actionId,
								KeyEvent event) {
							// TODO Auto-generated method stub
							if (actionId == EditorInfo.IME_ACTION_GO) {
								submitClick(v);
								return true;
							}
							return false;
						}
					});

			try {
				TelephonyManager tManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
				uuid = tManager.getDeviceId();
				if (uuid == null) {
					uuid = Secure.getString(
							LoginActivity.this.getContentResolver(),
							Secure.ANDROID_ID);
				}
				if (Utility.getUUID(LoginActivity.this,
						Constants.SHAREDPREFERENCE_UUID) == null)
					Utility.setUUID(LoginActivity.this,
							Constants.SHAREDPREFERENCE_UUID, uuid);
			} catch (Exception exception) {
			}

			if (Utility.getOSVersion(LoginActivity.this,
					Constants.SHAREDPREFERENCE_OS_VERSION) == null)
				Utility.setOSVersion(LoginActivity.this,
						Constants.SHAREDPREFERENCE_OS_VERSION,
						android.os.Build.VERSION.RELEASE);

			if (Utility.getOSManufacturer(LoginActivity.this,
					Constants.SHAREDPREFERENCE_OS_MANUFACTURER) == null)
				Utility.setOSManufacturer(LoginActivity.this,
						Constants.SHAREDPREFERENCE_OS_MANUFACTURER,
						android.os.Build.MANUFACTURER);

			try {
				LocationResult locationResult = new LocationResult() {
					@Override
					public void gotLocation(Location location) {
						// Got the location!
						try {
							// //// // Log.w("Location", location + "");
							if (location != null) {
								longitude = location.getLongitude();
								latitude = location.getLatitude();

								/*Utility.setCurrentLatitude(
										LoginActivity.this,
										Constants.SHAREDPREFERENCE_CURRENT_LATITUDE,
										String.valueOf(location.getLatitude()));
								Utility.setCurrentLongitude(
										LoginActivity.this,
										Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE,
										String.valueOf(location.getLongitude()));*/
								
								
								GPSTracker gps = new GPSTracker(LoginActivity.this);
								if (gps.canGetLocation()) { // gps enabled} // return boolean true/false

									gps.getLatitude(); // returns latitude
									gps.getLongitude(); // returns longitude

									Utility.setCurrentLatitude(LoginActivity.this,
											Constants.SHAREDPREFERENCE_CURRENT_LATITUDE,
											String.valueOf(gps.getLatitude()));
									Utility.setCurrentLongitude(LoginActivity.this,
											Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE,
											String.valueOf(gps.getLongitude()));
								}

								// Geocoder geocoder;
								// List<Address> addresses;
								// geocoder = new Geocoder(LoginActivity.this,
								// Locale.getDefault());
								// addresses =
								// geocoder.getFromLocation(lattitude,
								// longitude, 1);

								// String address =
								// addresses.get(0).getAddressLine(0);
								// String city =
								// addresses.get(0).getAddressLine(1);
								// String country =
								// addresses.get(0).getAddressLine(2);

								// // // Log.w("Address: ", "Add : " + address +
								// " City :"
								// + city + " Country :" + country);
							}
						} catch (Exception exception) {
						}
					}
				};
				MyLocation myLocation = new MyLocation();
				myLocation.getLocation(this, locationResult);
			} catch (Exception exception) {
			}

			button_Login = (Button) findViewById(R.id.button_Login);
			button_Login.setTypeface(Reguler);
			button_Login.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					submitClick(v);
				}
			});

			imageView_Close = (ImageView) findViewById(R.id.imageView_Close);
			imageView_Close.setImageDrawable(SVGParser.getSVGFromResource(
					getResources(), R.raw.ic_close).createPictureDrawable());
			if (Build.VERSION.SDK_INT >= 11)
				imageView_Close.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
			imageView_Close.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					finish();
				}
			});

			GCMRegistrar.checkDevice(this);
			GCMRegistrar.checkManifest(this);
			checkNotNull(Constants.SENDER_ID, "SENDER_ID");

			final String regId = GCMRegistrar.getRegistrationId(this);
			// // // Log.i(TAG, "registration id =====  " + regId);
			Utility.setGCMID(LoginActivity.this,
					Constants.SHAREDPREFERENCE_GCMID, regId);
			if (regId.equals("")) {
				GCMRegistrar.register(this, Constants.SENDER_ID);
			} else {
				// // // Log.v(TAG, "Already registered");
			}

		} catch (Exception e) {

			// System.out.println(e.getMessage());
		}
	}

	private void checkNotNull(Object reference, String name) {
		if (reference == null) {
			throw new NullPointerException("Errror");
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView().build());
		// EasyTracker.getInstance(this).activityStart(this);
		easyTracker.send(MapBuilder.createAppView().build());
	}

	@Override
	public void onStop() {
		super.onStop();
		// EasyTracker.getInstance(this).activityStop(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
	}

	private void submitClick(View v) {
		try {
			if (checkValidation(LoginActivity.this)) {
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

				/*
				 * ArrayHelper arrayHelper=new ArrayHelper(LoginActivity.this);
				 * arrayHelper.saveArray(Constants.CONATACT_JSON,
				 * Constants.getAllContacts(LoginActivity.this));
				 */
				new LoginTask().execute(editText_Mobile.getText().toString()
						.trim(), editText_Pin.getText().toString().trim(),
						Utility.getUUID(LoginActivity.this,
								Constants.SHAREDPREFERENCE_UUID), String
								.valueOf(latitude), String.valueOf(longitude),
						manufacturer, version, Utility.getGCMID(
								LoginActivity.this,
								Constants.SHAREDPREFERENCE_GCMID));
			}
		} catch (Exception e) {
		}
	}

	private boolean checkValidation(Context context) {
		boolean ret = true;

		if (!EditTextValidator.hasText(context, editText_Mobile,
				Constants.ERROR_MOBILE_BLANK_FIELD)) {
			ret = false;
			editText_Mobile.requestFocus();
			return ret;
		} else if (!EditTextValidator.isValidMobileNumber(context,
				editText_Mobile, Constants.ERROR_MOBILE_LENGTH_FIELD)) {
			ret = false;
			editText_Mobile.requestFocus();
			return ret;
		} else if (!EditTextValidator.hasText(context, editText_Pin,
				Constants.ERROR_LOGIN_PIN_BLANK_FIELD)) {
			ret = false;
			editText_Pin.requestFocus();
			return ret;
		} else if (!EditTextValidator.isValidPin(context, editText_Pin,
				Constants.ERROR_LOGIN_PIN_VALID_FIELD)) {
			ret = false;
			editText_Pin.requestFocus();
			return ret;
		} else
			return ret;
	}

	public class LoginTask extends AsyncTask<String, String, String> implements
			OnDismissListener {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			try {

				String response = RequestClass.getInstance()
						.readPay1B2CRequest(
								LoginActivity.this,
								Constants.B2C_URL + "signin/?username="
										+ URLEncoder.encode(params[0], "utf-8")
										+ "&password="
										+ URLEncoder.encode(params[1], "utf-8")
										+ "&uuid=" + params[2] + "&latitude="
										+ params[3] + "&longitude=" + params[4]
										+ "&manufacturer=" + params[5]
										+ "&os_info=" + params[6]
										+ "&gcm_reg_id=" + params[7]
										+ "&device_type=android&api_version="+Constants.API_VERSION);

				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			try {

				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONObject jsonObject = new JSONObject(replaced);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {
						Utility.setLoginFlag(LoginActivity.this,
								Constants.SHAREDPREFERENCE_IS_LOGIN, true);
						JSONObject jsonObject2 = jsonObject
								.getJSONObject("description");

						Utility.setUserName(LoginActivity.this,
								Constants.SHAREDPREFERENCE_NAME, jsonObject2
										.getString("name").trim());
						Utility.setMobileNumber(LoginActivity.this,
								Constants.SHAREDPREFERENCE_MOBILE, jsonObject2
										.getString("mobile").trim());
						Utility.setEmail(LoginActivity.this,
								Constants.SHAREDPREFERENCE_EMAIL, jsonObject2
										.getString("email").trim());
						Utility.setGender(LoginActivity.this,
								Constants.SHAREDPREFERENCE_GENDER, jsonObject2
										.getString("gender").trim());
						Utility.setUserID(LoginActivity.this,
								Constants.SHAREDPREFERENCE_USER_ID, jsonObject2
										.getString("user_id").trim());
						Utility.setBalance(LoginActivity.this,
								Constants.SHAREDPREFERENCE_BALANCE, jsonObject2
										.getString("wallet_balance").trim());
						Utility.setPin(LoginActivity.this,
								Constants.SHAREDPREFERENCE_PIN, editText_Pin
										.getText().toString().trim());
						Utility.setDOB(LoginActivity.this,
								Constants.SHAREDPREFERENCE_DOB, jsonObject2
										.getString("date_of_birth").trim());
						Utility.setLatitude(LoginActivity.this,
								Constants.SHAREDPREFERENCE_LATITUDE,
								jsonObject2.getString("latitude").trim());
						Utility.setLongitude(LoginActivity.this,
								Constants.SHAREDPREFERENCE_LONGITUDE,
								jsonObject2.getString("longitude").trim());
						Utility.setSupportNumber(LoginActivity.this,
								Constants.SHAREDPREFERENCE_SUPPORT_NUMBER,
								jsonObject2.getString("support_number").trim());
						Utility.setCookieName(LoginActivity.this,
								Constants.SHAREDPREFERENCE_COOKIE_NAME,
								jsonObject2.getString("user_val_name"));
						Utility.setCookieValue(LoginActivity.this,
								Constants.SHAREDPREFERENCE_COOKIE_VALUE,
								jsonObject2.getString("user_val"));
						Utility.setBalance(LoginActivity.this,
								 Constants.SHAREDPREFERENCE_LOYALTY_POINTS,
								 jsonObject2.getString("loyalty_points"));
						Utility.setSharedPreferences(LoginActivity.this,
								"Deals_offer",
								jsonObject2.getString("Deals_offer"));
						
						
						Utility.setMyTotalGifts(LoginActivity.this,
								jsonObject2.getString("total_gifts"));

						Utility.setMyLikes(LoginActivity.this,
								jsonObject2.getString("total_mylikes"));

						Utility.setMyTotalRedeem(LoginActivity.this,
								jsonObject2.getString("total_redeem"));

						Utility.setMyReviews(LoginActivity.this,
								jsonObject2.getString("total_myreviews"));
						JSONObject jsonObjectTemplate = jsonObject2
								.getJSONObject("ErrorMsg");
						Utility.setTemplate(
								LoginActivity.this,
								Constants.SHAREDPREFERENCE_SMS_BILLPAYMENT_FAILURE,
								jsonObjectTemplate
										.getString("BILLPAYMENT_FAILURE"));
						Utility.setTemplate(
								LoginActivity.this,
								Constants.SHAREDPREFERENCE_SMS_RECHARGE_FAILURE,
								jsonObjectTemplate
										.getString("RECHARGE_FAILURE"));
						Utility.setTemplate(
								LoginActivity.this,
								Constants.SHAREDPREFERENCE_SMS_WALLET_REFILLED_RETAILER,
								jsonObjectTemplate
										.getString("WALLET_REFILLED_RETAILER"));

						Utility.setSharedPreferences(
								LoginActivity.this,
								Constants.SHAREDPREFERENCE_SERVICE_CHARGE_PERCENT,
								jsonObject2.getString("service_charge_percent")
										.trim());

						Utility.setSharedPreferences(LoginActivity.this,
								Constants.SHAREDPREFERENCE_SERVICE_TAX_PERCENT,
								jsonObject2.getString("service_tax_percent")
										.trim());
						Utility.setFbImageId(LoginActivity.this, jsonObject2.getString("profile_image"));
						Utility.setProfileDataSource(LoginActivity.this, jsonObject2.getString("fb_data"));
						
						/*if(!jsonObject2.getString("fb_data").isEmpty()||jsonObject2.getString("fb_data").length()!=0){
						JSONObject fbJsonObject=new JSONObject(jsonObject2.getString("fb_data"));
						
						Utility.setUserName(LoginActivity.this,
								Constants.SHAREDPREFERENCE_NAME, fbJsonObject
										.getString("name").trim());
						Utility.setMobileNumber(LoginActivity.this,
								Constants.SHAREDPREFERENCE_MOBILE, jsonObject2
										.getString("mobile").trim());
						Utility.setEmail(LoginActivity.this,
								Constants.SHAREDPREFERENCE_EMAIL, fbJsonObject
										.getString("email").trim());
						Utility.setGender(LoginActivity.this,
								Constants.SHAREDPREFERENCE_GENDER, fbJsonObject
										.getString("gender").trim());
						String imageId="https://graph.facebook.com/" + fbJsonObject.getString("id")+ "/picture?type=large";
						Utility.setFbImageId(LoginActivity.this,imageId );
						
						}*/

						try {
							quickPayDataSource.open();

							List<QuickPay> list = quickPayDataSource
									.getAllQuickPay();
							if (list.size() == 0) {
								JSONArray jsonArray = jsonObject2
										.getJSONArray("recent_pay1_transactions");

								if (jsonArray.length() != 0) {
									long timestamp = System.currentTimeMillis();
									quickPayDataSource.createQuickPay(
											jsonArray, timestamp);
									// for (int i = 0; i < jsonArray.length();
									// i++) {
									//
									// JSONObject jsonObject_inner = jsonArray
									// .getJSONObject(i);
									// try {
									// quickPayDataSource
									// .createQuickPay(
									// Integer.parseInt(jsonObject_inner
									// .getString("id")),
									// Integer.parseInt(jsonObject_inner
									// .getString("flag")),
									// jsonObject_inner
									// .getString("name"),
									// Integer.parseInt(jsonObject_inner
									// .getString("amount")),
									// Integer.parseInt(jsonObject_inner
									// .getString("stv")),
									// Integer.parseInt(jsonObject_inner
									// .getString("operator_id")),
									// Integer.parseInt(jsonObject_inner
									// .getString("product")),
									// jsonObject_inner
									// .getString("number"),
									// Integer.parseInt(jsonObject_inner
									// .getString("delegate_flag")),
									// jsonObject_inner
									// .getString("missed_number"),
									// jsonObject_inner
									// .getString("operator_name"),
									// jsonObject_inner
									// .getString("operator_code"),
									// timespam,
									// jsonObject_inner
									// .getString("datetime"),
									// Integer.parseInt(jsonObject_inner
									// .getString("transaction_flag")));
									// } catch (Exception e) {
									// }
									// }
								}
							}
						} catch (SQLException exception) {
							// TODO: handle exception
							// // // Log.e("Er ", exception.getMessage());
						} catch (Exception e) {
							// TODO: handle exception
							// // // Log.e("Er ", e.getMessage());
						} finally {
							quickPayDataSource.close();
						}

						if (flag) {
							Intent i = new Intent(LoginActivity.this,
									MainActivity.class);
							i.putExtra(Constants.RECHARGE_FOR,
									Constants.RECHARGE_MOBILE);
							startActivity(i);
						}
						finish();

					} else {
						Constants.showOneButtonDialog(LoginActivity.this, TAG,
								Constants.checkCode(replaced),
								Constants.DIALOG_CLOSE);
					}

				} else {
					Intent intent = new Intent(LoginActivity.this,
							ConnectivityErrorActivity.class);
					startActivity(intent);
				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonDialog(LoginActivity.this, TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonDialog(LoginActivity.this, TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(LoginActivity.this);
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							LoginTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			LoginTask.this.cancel(true);
			dialog.cancel();
		}

	}

}