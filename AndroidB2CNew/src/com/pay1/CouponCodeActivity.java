package com.pay1;

import java.net.URLEncoder;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.larvalabs.svgandroid.SVGParser;
import com.pay1.constants.Constants;
import com.pay1.customviews.MyProgressDialog;
import com.pay1.requesthandler.RequestClass;
import com.pay1.utilities.EditTextValidator;
import com.pay1.utilities.Utility;

public class CouponCodeActivity extends Activity {

	private static final String SCREEN_LABEL = "Coupon Code Screen";
	private EasyTracker easyTracker = null;

	private final String TAG = "Coupon Code";
	private EditText editText_CouponCode;
	private TextView textView_TitleCode, textView_Text;
	private Button button_Confirm, button_Cancel;
	private ImageView imageView_Back;
	private TextView textView_Title;

	@SuppressLint("NewApi")
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.couponcode_activity);
		try {
			easyTracker = EasyTracker.getInstance(CouponCodeActivity.this);
			easyTracker.set(Fields.SCREEN_NAME, SCREEN_LABEL);

			// timestamp = System.currentTimeMillis();

			// faqDataSource = new FAQDataSource(CouponCodeActivity.this);

			
			RelativeLayout back_layout=(RelativeLayout)findViewById(R.id.back_layout);
			back_layout.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					getWindow().setSoftInputMode(
						    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
						);
					onBackPressed();
				}
			});
			
			imageView_Back = (ImageView) findViewById(R.id.imageView_Back);
			imageView_Back.setImageDrawable(SVGParser.getSVGFromResource(
					getResources(), R.raw.ic_back_old).createPictureDrawable());
			if (Build.VERSION.SDK_INT >= 11)
				imageView_Back.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
			imageView_Back.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onBackPressed();
				}
			});

			Typeface Reguler = Typeface.createFromAsset(getAssets(),
					"Roboto-Regular.ttf");
			Typeface Medium = Typeface.createFromAsset(getAssets(),
					"Roboto-Regular.ttf");

			textView_Title = (TextView) findViewById(R.id.textView_Title);
			textView_Title.setTypeface(Reguler);

			textView_Text = (TextView) findViewById(R.id.textView_Text);
			textView_Text.setTypeface(Reguler);
			textView_TitleCode = (TextView) findViewById(R.id.textView_TitleCode);
			textView_TitleCode.setTypeface(Medium);
			textView_TitleCode.setVisibility(View.GONE);

			editText_CouponCode = (EditText) findViewById(R.id.editText_CouponCode);
			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(editText_CouponCode.getWindowToken(), 0);
			editText_CouponCode.setTypeface(Reguler);
			try {
				Bundle bundle = getIntent().getExtras();
				if (bundle != null) {
					String str = bundle.getString("AMOUNT");
					editText_CouponCode.setText(str);
				}
			} catch (Exception e) {

			}
			editText_CouponCode.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					// TODO Auto-generated method stub
					if (editText_CouponCode.getText().toString().trim()
							.length() != 0) {
						textView_TitleCode.setVisibility(View.GONE);
					} else {
						textView_TitleCode.setVisibility(View.GONE);
					}
					if (editText_CouponCode.getText().toString().trim()
							.length() > 0)
						EditTextValidator.hasFragmentText(
								CouponCodeActivity.this, editText_CouponCode,
								Constants.ERROR_COUPON_BLANK_FIELD);
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					// TODO Auto-generated method stub

				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

				}
			});

			editText_CouponCode
					.setOnEditorActionListener(new TextView.OnEditorActionListener() {

						@Override
						public boolean onEditorAction(TextView v, int actionId,
								KeyEvent event) {
							// TODO Auto-generated method stub
							if (actionId == EditorInfo.IME_ACTION_GO) {
								submitClick(v);
								return true;
							}
							return false;
						}
					});

			button_Confirm = (Button) findViewById(R.id.button_Confirm);
			button_Confirm.setTypeface(Reguler);
			button_Confirm.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					submitClick(v);
				}
			});
			button_Cancel = (Button) findViewById(R.id.button_Cancel);
			button_Cancel.setTypeface(Reguler);
			button_Cancel.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					editText_CouponCode.setText("");
				}
			});

		} catch (Exception e) {

		}
		overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}

	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView().build());
		// EasyTracker.getInstance(this).activityStart(this);
		easyTracker.send(MapBuilder.createAppView().build());
	}

	@Override
	public void onStop() {
		super.onStop();
		// EasyTracker.getInstance(this).activityStop(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
	}

	private void submitClick(View v) {
		try {
			InputMethodManager imm = (InputMethodManager) CouponCodeActivity.this
					.getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

			if (EditTextValidator.hasFragmentText(CouponCodeActivity.this,
					editText_CouponCode, Constants.ERROR_COUPON_BLANK_FIELD)) {
				new WalletTopupViaCouponTask().execute(editText_CouponCode
						.getText().toString().trim());
			}
		} catch (Exception e) {
		}
	}

	public class WalletTopupViaCouponTask extends
			AsyncTask<String, String, String> implements OnDismissListener {
		private MyProgressDialog dialog;
		String url;

		@Override
		protected String doInBackground(String... params) {
			try {
				String response = RequestClass
						.getInstance()
						.readPay1B2CRequest(
								CouponCodeActivity.this,
								Constants.B2C_URL
										+ "refillwallet/?mobile_number=&amount=&vcode="
										+ URLEncoder.encode(params[0], "utf-8"));

				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onCancelled() {
			// TODO Auto-generated method stub
			super.onCancelled();
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			try {

				if (!result.startsWith("Error")) {
					// result = result.replaceAll("\\\\", "");
					JSONObject jsonObject = new JSONObject(result);

					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {
						JSONObject description = jsonObject
								.getJSONObject("description");

						Utility.setBalance(CouponCodeActivity.this,
								Constants.SHAREDPREFERENCE_BALANCE,
								description.getString("closing_balance"));

						Constants.showSuccessFailureDialog(
								CouponCodeActivity.this, "Success",
								Constants.WALLET_PAYMENT,
								Utility.getMobileNumber(
										CouponCodeActivity.this,
										Constants.SHAREDPREFERENCE_MOBILE), "",
								description.getString("closing_balance"), "",
								true, "", description
										.getString("transaction_id"),"");
					}else if(jsonObject.getString("errCode").toString().equalsIgnoreCase("201")){
						
						Constants.showOneButtonDialog(
								CouponCodeActivity.this, "Gifts",
								Constants.checkCode(result),
								Constants.DIALOG_CLOSE);
					} else {

						Constants.showOneButtonDialog(CouponCodeActivity.this,
								TAG, jsonObject.getString("description"),
								Constants.DIALOG_CLOSE);

					}
				} else {
					Intent intent = new Intent(CouponCodeActivity.this,
							ConnectivityErrorActivity.class);
					startActivity(intent);
				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonDialog(CouponCodeActivity.this, TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonDialog(CouponCodeActivity.this, TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(CouponCodeActivity.this);
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(false);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							WalletTopupViaCouponTask.this.cancel(true);
							dialog.cancel();
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			WalletTopupViaCouponTask.this.cancel(true);
			dialog.cancel();
		}

	}
}
