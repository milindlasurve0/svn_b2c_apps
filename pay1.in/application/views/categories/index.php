<?php $this->load->view('common/doctype_html');  ?>
<?php echo $this->load->view('common/header'); ?>

<!-- Write logic to load slider based on category in URL-->
<?php echo $this->load->view('categories/categoryslider'); ?>


<div class="row">
    <div class="col-lg-10 col-lg-offset-1">



        <div class="row">
            <div class="col-lg-10 col-lg-offset-2">

                <div align="center" class="col-lg-4 col-lg-offset-4">

                    <div class="form-group">

                        <div class="input-group">
                            <input type="text" class="form-control input-sm" placeholder="Search by name">
                            <span class="input-group-btn">
                                <button class="btn btn-primary btn-sm" type="button">Search!</button>
                            </span>
                        </div>

                    </div>

                </div>

            </div>
        </div>


        <div class="row">

            <div class="col-lg-12">


                <div class="col-lg-3">
                    <div  >
                        <!--                        <h3>Categories</h3>-->
                        <div class="list-group panel verticaldropdown">
                            <a href="#" class="list-group-item list-group-item" data-toggle="collapse" data-parent="#MainMenu"><b>Featured---</b></a>
                            <a href="#" class="list-group-item list-group-item" data-toggle="collapse" data-parent="#MainMenu"><b>Offers of the day----</b></a>
                            <a href="#" class="list-group-item list-group-item" data-toggle="collapse" data-parent="#MainMenu"><b>Recommended----</b></a>
                            <a href="#" class="list-group-item list-group-item" data-toggle="collapse" data-parent="#MainMenu"><b>Near you----</b></a>
                            <a  onclick="javascript:showOverlay();" href="#" class="list-group-item list-group-item" data-toggle="collapse" data-parent="#MainMenu"><b>Categories</b><i class="glyphicon glyphicon-chevron-down arrowDown" ></i></a>
<!--                            <a href="#categories" class="list-group-item list-group-item" data-toggle="collapse" data-parent="#MainMenu"><b>Categories</b><i class="glyphicon glyphicon-chevron-down arrowDown" onclick="javascript:showOverlay();"></i></a>
                            <div class="collapse" id="categories" style="margin-left: 30px;">
                               <a href="" class="list-group-item">Food</a>
                                <a href="" class="list-group-item">Learning</a>
                                <a href="" class="list-group-item">Health & Fitness</a>
                                <a href="" class="list-group-item">Fashion</a>
                                <a href="" class="list-group-item">Services</a>
                                <a href="" class="list-group-item">Entertainment</a>
                                <a href="" class="list-group-item">Online</a>
                            </div>-->

                        </div>
                    </div>

                            <div class="categoryOverlaydiv" style="display: none;">
                                    <div class="categoryOverlay"></div>
                                    <div class="categoryOverlayText">
                                        <ul class="overlaymenu">
                                            <li><a>Featured</a></li>
                                        <li><a>Offers of the day</a></li>
                                        <li><a>Recommended</a></li>
                                        <li><a>Near you</a></li>
                                        <li><a class="addclassunderline" id="overlaylink">Categories</a>
                                            <ul id="overlayUL">
                                                
                                            </ul>
                                        </li>
                                        <li><a>Locations</a></li>
                                        </ul>
                                    </div>
                            </div>

                      
                </div>



                <div class="col-lg-9">

                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="col-lg-3 col-sm-3 pull-left">
                                <h1><?php 
                                echo ucfirst(isset($CategoryName)?$CategoryName:""); ?></h1>
                            </div>
                        </div>
                    </div>

                    <div id="gift_types" class="storeCategory">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="col-md-4">                                   
                                    <a href="pulpitrock.jpg" class="thumbnail">
                                        <img class="img-responsive" src="http://placehold.it/250x150" alt="">
                                    </a>                                    
                                </div>
                                <div class="col-md-4">                                   
                                    <a href="pulpitrock.jpg" class="thumbnail">
                                        <img class="img-responsive" src="http://placehold.it/250x150" alt="">
                                    </a>                                    
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<?php echo $this->load->view('common/footer'); ?>