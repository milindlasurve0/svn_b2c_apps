﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Maps.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Device.Location;
using System.Windows.Media.Imaging;
using Newtonsoft.Json.Linq;
using Pay1B2C.UserControls;

namespace Pay1B2C.Views
    {
    public partial class GiftLocationMap: PhoneApplicationPage
        {
        JArray locationData;
        public GiftLocationMap()
            {
            InitializeComponent();
            if(PhoneApplicationService.Current.State.ContainsKey("locationData"))
                {
                locationData=(JArray)PhoneApplicationService.Current.State["locationData"];
                loadLocationList(locationData);
                }
            }

        

            protected override void OnNavigatedTo(NavigationEventArgs e)
            {
            if(NavigationContext.QueryString.ContainsKey("shopName"))
                {
                string shopName = NavigationContext.QueryString["shopName"];
                if(string.IsNullOrEmpty(shopName))
                    {
                    }
                else
                    {
                    textTitlePath.Text=shopName;
                    textName.Text=shopName;
                   
                    }
                }

                if(NavigationContext.QueryString.ContainsKey("img_logo_url"))
                    {
                     string img_logo_url = NavigationContext.QueryString["img_logo_url"];
                if(string.IsNullOrEmpty(img_logo_url))
                    {
                    }
                else
                    {
                  imgLogoUrl.Source=new BitmapImage(new Uri(img_logo_url,UriKind.RelativeOrAbsolute));
                    
                    }
                    }
                }



        private void loadLocationList(JArray locationData)
            {
            int i=0;
            foreach(JObject jObj in locationData)
                {
                ShopLocationControl shopLocationControl=new ShopLocationControl();
                shopLocationControl.textFullAddress.Text=jObj["full_address"].ToString();
                shopLocationControl.textPhone.Text=jObj["dealer_contact"].ToString();
                double lat= (double)jObj["lat"];
                double lng= (double)jObj["lng"];
                string s_lat= Constant.LoadPersistent<string>(Constant.SETTINGS_USER_LATTITUDE);
                string s_lng= Constant.LoadPersistent<string>(Constant.SETTINGS_USER_LONGITUDE);
                double dis=   Constant.distance(lat, lng, Convert.ToDouble(s_lat), Convert.ToDouble(s_lng));

                shopLocationControl.showDealerLocation.Click+=(a, b) =>
                {
                //NavigationService.Navigate(new Uri("/Views/DealShopRoute.xaml?",UriKind.RelativeOrAbsolute));
                PhoneApplicationService.Current.State["LocationData"] = jObj.ToString();
                NavigationService.Navigate(new Uri("/Views/ShopRoute.xaml?address="+shopLocationControl.textFullAddress.Text+"&shopName="+textTitlePath.Text, UriKind.RelativeOrAbsolute));
                };


                if(i==0)
                    {
                    addPushPin(lat, lng);
                    }
                i++;

                if(dis>1)
                    {
                    shopLocationControl.textDistance.Text=dis.ToString("0.00")+" KM";
                    }
                else
                    {
                    shopLocationControl.textDistance.Text=" < 1KM";
                    }

                dealLocationListBox.Items.Add(shopLocationControl);
                }
            }



        private void buttonImage_Click_Back(object sender, RoutedEventArgs e)
            {
            NavigationService.GoBack();
            

            }

        public void addPushPin(double lattitude, double longitude)
            {
            if(sampleMap.Layers.Count()>=1)
                {
                sampleMap.Layers.Clear();
                }
            Uri uri = new Uri("http://b2c.pay1.in/images/pay1retailsshop.png", UriKind.RelativeOrAbsolute);
            BitmapImage imgSource = new BitmapImage(uri);

            MapOverlay overlay = new MapOverlay
            {
                GeoCoordinate = new GeoCoordinate(lattitude, longitude),
                Content = new Image
                {

                    Source=imgSource
                    // this.image.Source = imgSource
                }
            };
            MapLayer layer = new MapLayer();
            layer.Add(overlay);

            sampleMap.Center=new GeoCoordinate(lattitude, longitude);
            sampleMap.Layers.Add(layer);
            }

        private void dealLocationListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
            {
            int index=dealLocationListBox.SelectedIndex;
            JObject jObj=(JObject)locationData[index];
            double lat= (double)jObj["lat"];
            double lng= (double)jObj["lng"];

            sampleMap.ZoomLevel=12;
            addPushPin(lat, lng);
            
            }



        }
    }