//
//  Session.h
//  ClearPathGPS
//
//  Created by Hemantech on 06/03/14.
//  Copyright (c) 2014 Hemantech. All rights reserved.
//

#import <Foundation/Foundation.h>
@class SignInResponse;
@interface Session : NSObject
@property (nonatomic, strong) SignInResponse *objSignInResponse;
@property (nonatomic, assign) BOOL isFromSearch;
@property (nonatomic, assign) BOOL isAppLaunched;
@property (nonatomic, assign) BOOL isFromAutoLogin;
@property (nonatomic, assign) BOOL isFromSlide;
//@property (nonatomic, assign) BOOL isCallQuickPay;

//@property (nonatomic, assign) BOOL isPlansFilled;

//@property (nonatomic, assign) BOOL isLogoutDone;



+(Session*) shared ;
@end

