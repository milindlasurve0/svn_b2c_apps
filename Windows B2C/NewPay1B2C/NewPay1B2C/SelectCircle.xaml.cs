﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Windows.Input;
using NewPay1B2C.UserControls;

namespace NewPay1B2C
	{
	public partial class SelectCircle : PhoneApplicationPage
		{
		string[] codeArray;

		public static string jsonCircle;
		string[] circleArray;
		string op_id, op_name;
		public SelectCircle ()
			{
			InitializeComponent ();
			GoogleAnalytics.EasyTracker.GetTracker ().SendView ("SelectCircle");
			jsonCircle = @"[
    {
        'type': 'States',
        'id': 1,
        'name': 'Andhra Pradesh',
        'code': 'AP'
    },
    {
        'type': 'States',
        'id': 2,
        'name': 'Assam',
        'code': 'AS'
    },
    {
        'type': 'States',
        'id': 3,
        'name': 'Jharkhand',
        'code': 'BR'
    },
    {
        'type': 'Metros',
        'id': 5,
        'name': 'Delhi NCR',
        'code': 'DL'
    },
    {
        'type': 'States',
        'id': 6,
        'name': 'Gujarat',
        'code': 'GJ'
    },
    {
        'type': 'States',
        'id': 7,
        'name': 'Haryana',
        'code': 'NE'
    },
    {
        'type': 'States',
        'id': 8,
        'name': 'Himachal Pradesh',
        'code': 'HP'
    },
    {
        'type': 'States',
        'id': 9,
        'name': 'Jammu & Kashmir',
        'code': 'JK'
    },
    {
        'type': 'States',
        'id': 10,
        'name': 'Karnataka',
        'code': 'KA'
    },
    {
        'type': 'States',
        'id': 11,
        'name': 'Kerala',
        'code': 'KL'
    },
    {
        'type': 'Metros',
        'id': 12,
        'name': 'Kolkata',
        'code': 'KO'
    },
    {
        'type': 'States',
        'id': 13,
        'name': 'Maharashtra',
        'code': 'MH'
    },
    {
        'type': 'States',
        'id': 14,
        'name': 'Madhya Pradesh',
        'code': 'MP'
    },
    {
        'type': 'Metros',
        'id': 15,
        'name': 'Mumbai',
        'code': 'MU'
    },
    {
        'type': 'States',
        'id': 16,
        'name': 'Tripura',
        'code': 'NE'
    },
    {
        'type': 'States',
        'id': 17,
        'name': 'Orissa',
        'code': 'OR'
    },
    {
        'type': 'States',
        'id': 18,
        'name': 'Punjab',
        'code': 'PB'
    },
    {
        'type': 'States',
        'id': 19,
        'name': 'Rajasthan',
        'code': 'RJ'
    },
    {
        'type': 'States',
        'id': 20,
        'name': 'Tamil Nadu',
        'code': 'TN'
    },
    {
        'type': 'States',
        'id': 21,
        'name': 'Uttar Pradesh (East)',
        'code': 'UE'
    },
    {
        'type': 'States',
        'id': 22,
        'name': 'Uttar Pradesh (West)',
        'code': 'UW'
    },{
        'type': 'States',
        'id': 22,
        'name': 'Uttarakhand',
        'code': 'UW'
    },
    {
        'type': 'States',
        'id': 23,
        'name': 'West Bengal',
        'code': 'WB'
    }
]";
			jsonCircle = jsonCircle.Replace ("\r", "");
			jsonCircle = jsonCircle.Replace ("\n", "");

			try
				{
				var dict = (JArray)JsonConvert.DeserializeObject (jsonCircle);
				codeArray = new string[25];
				circleArray = new string[25];
				int i = 0;
				foreach (var item in dict)
					{
					var code = item["code"];
					var name = item["name"];
					var id = item["id"];
					codeArray[i] = code.ToString ();
					circleArray[i] = name.ToString ();
					CircleListControl circleListCtrl = new CircleListControl ();
					circleListCtrl.CircleName.Text = name.ToString ();
					listBox1.Items.Add (circleListCtrl);
					i++;
					}

				}
			catch (Exception e)
				{
				Console.WriteLine (e.StackTrace);
				}
			}

		private void btn_back_MouseLeave (object sender, MouseEventArgs e)
			{
			NavigationService.Navigate (new Uri ("/NewMainPage.xaml?", UriKind.Relative));
			}
		protected override void OnNavigatedTo (NavigationEventArgs e)
			{

			if (NavigationContext.QueryString.ContainsKey ("op_id"))
				{
				op_id = NavigationContext.QueryString["op_id"];
				}

			if (NavigationContext.QueryString.ContainsKey ("op_name"))
				{
				op_name = NavigationContext.QueryString["op_name"];
				}
			}




		private void Home_Click (object sender, RoutedEventArgs e)
			{
			NavigationService.Navigate (new Uri ("/NewMainPage.xaml", UriKind.Relative));
			}


		private void Help_Click (object sender, RoutedEventArgs e)
			{
			NavigationService.Navigate (new Uri ("/Help.xaml", UriKind.Relative));
			}

		private void Login_Click (object sender, RoutedEventArgs e)
			{
			NavigationService.Navigate (new Uri ("/Login.xaml", UriKind.Relative));
			}

		private void listBox1_SelectionChanged (object sender, SelectionChangedEventArgs e)
			{
			string codeId = codeArray[listBox1.SelectedIndex];
			//NavigationService.Navigate(new Uri("/PlanNew.xaml?code=" + codeId + "&op_id=" + op_id, UriKind.Relative));
			NavigationService.Navigate (new Uri ("/PlansActivity.xaml?code=" + codeId + "&op_id=" + op_id + "&cir_name=" + circleArray[listBox1.SelectedIndex] + "&op_name=" + op_name, UriKind.Relative));
			PhoneApplicationService.Current.State["area_name"] = circleArray[listBox1.SelectedIndex];
			//PhoneApplicationService.Current.State["area"] = codeId;
			PhoneApplicationService.Current.State["area"] = codeId;
				
			NavigationService.RemoveBackEntry ();
			//NavigationService.Navigate(new Uri("/DynamicPivot.xaml", UriKind.Relative));
			}

		}

	}