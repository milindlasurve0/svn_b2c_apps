package com.pay1.customviews;

import com.pay1.MapActivity;

import android.content.Context;
import android.view.MotionEvent;
import android.widget.FrameLayout;

public class TouchableWrapper extends FrameLayout {

	public TouchableWrapper(Context context) {
		super(context);
	}

	/*@Override
	public boolean dispatchTouchEvent(MotionEvent event) {
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			MapActivity.isMoving = false;
			break;
		case MotionEvent.ACTION_MOVE:
			MapActivity.isMoving = false;
			break;
		case MotionEvent.ACTION_UP:
			MapActivity.isMoving = true;
			break;
		}
		return super.dispatchTouchEvent(event);
	}*/
	
	
	 public interface OnDragListener {
	        public void onDrag(MotionEvent motionEvent);
	    }

	    private OnDragListener mOnDragListener;

	    @Override
	    public boolean dispatchTouchEvent(MotionEvent ev) {
	        if (mOnDragListener != null) {
	            mOnDragListener.onDrag(ev);
	        }
	        return super.dispatchTouchEvent(ev);
	    }

	    public void setOnDragListener(OnDragListener mOnDragListener) {
	        this.mOnDragListener = mOnDragListener;
	    }
	
}