//
//  PlansViewController.h
//  Pay1
//
//  Created by Annapurna on 15/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "BaseViewController.h"
#import "EnumType.h"
@class PlanCustomCell;

typedef void (^OnSelectPlan)(NSString *planPrice,ServiceType serviceTypeObj);

@interface PlansViewController : BaseViewController<UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate>{
    IBOutlet UIView *threeGView;
    IBOutlet UIView *twoGView;
    IBOutlet UIView *otherView;
    IBOutlet UIView *topUpView;
    
    IBOutlet UITableView *topUpTV;
    IBOutlet UITableView *otherTV;
    IBOutlet UITableView *twoGTV;
    IBOutlet UITableView *threeGTV;
    
    IBOutlet UITableView *circleTV;
        
    IBOutlet UIButton *threeGButton;
    IBOutlet UIButton *dataTwoButton;
    IBOutlet UIButton *otherButton;
    IBOutlet UIButton *topUpButton;
    
    IBOutlet UIView *headerUnderLineView;
    
    IBOutlet UIScrollView *pageScrollView;
    
    
    
    IBOutlet PlanCustomCell *objPlanCustomCell;
    
    BOOL isCityTapped;
    
    IBOutlet UIView *blackHeaderView;
    
    NSMutableArray *circleNameArrayList;
    
    NSMutableArray *planValidityArrayList;
    NSMutableArray *planDescArrayList;
    NSMutableArray *planAmountArrayList;
    
    IBOutlet UILabel *cityTxtLabel;
    
    
    IBOutlet UIView *cityView;
    
    DataSelectedType objDataSelectedType;
    
    IBOutlet UILabel *planNotFoundLabel;
    
    
}

@property (nonatomic, strong) NSString *selectOperatorName;
@property (nonatomic, strong) NSString *selectOperatorID;
@property (nonatomic, strong) NSString *selectCirName;
@property (nonatomic, strong) NSString *selectCirID;

@property (nonatomic, assign) BOOL isShowPlanDetails;
@property (nonatomic, assign) ServiceType objServiceType;

@property (nonatomic, copy) OnSelectPlan  onSelectPlan;



@end
