package com.pay1.databasehandler;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import com.pay1.constants.Constants;
import com.pay1.utilities.Utility;

public class SyncDatabaseForFreeGifts {

	private SQLiteDatabase database;
	private Pay1SyncDatabaseHelperGifts dbHelper;
	private Context dbContext;

	

	public SyncDatabaseForFreeGifts(Context context) {
		dbHelper = new Pay1SyncDatabaseHelperGifts(context);
		dbContext = context;
		
		
	}

	public boolean cloneDatabase(String inputDBName, String outputDBName) {
		return dbHelper.cloneDatabase(inputDBName, outputDBName);
	}

	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
		System.out.println("Released " + SQLiteDatabase.releaseMemory());
	}

	public void createFreeBies(String response, long timestamp) {
		try {
			open();
			String gift = "INSERT INTO "
					+ Pay1GiftsSQLiteHelper.TABLE_GIFTS
					+ " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";

			String location = "INSERT INTO "
					+ Pay1GiftsSQLiteHelper.TABLE_LOCATION
					+ " VALUES (?,?,?,?,?,?,?,?,?,?,?);";
			SQLiteStatement insert_gift = database.compileStatement(gift);
			SQLiteStatement insert_location = database
					.compileStatement(location);
			database.beginTransaction();

			JSONObject jsonObject = new JSONObject(response);
			String status = jsonObject.getString("status");
			if (status.equalsIgnoreCase("success")) {
				JSONArray jsonArrayDesc = jsonObject
						.getJSONArray("description");
				for (int i = 0; i < jsonArrayDesc.length(); i++) {
					try {
						JSONObject jsonObjectOffer = jsonArrayDesc
								.getJSONObject(i).getJSONObject("offer_detail");

						String deal_id = jsonArrayDesc.getJSONObject(i)
								.getString("id");

						String deal_name = jsonArrayDesc.getJSONObject(i)
								.getString("dealname");
						String catagory = jsonArrayDesc.getJSONObject(i)
								.getString("category");
						String catagory_id = jsonArrayDesc.getJSONObject(i)
								.getString("category_id");
						String url = jsonArrayDesc.getJSONObject(i).getString(
								"img_url");
						String logo = jsonArrayDesc.getJSONObject(i).getString(
								"logo_url");
						String offer_id = jsonObjectOffer.getString("id");
						String offer_name = jsonObjectOffer.getString("name");
						String desc = jsonObjectOffer.getString("offer_desc");
						String min_amt = jsonObjectOffer
								.getString("min_amount");
						String dealer_contact = jsonArrayDesc.getJSONObject(i)
								.getString("dealer_contact");
						String validity = jsonObjectOffer.getString("validity");

						String offer_lat = "0", offer_lng = "0", offer_add = "", offer_area = "", offer_city = "", offer_state = "";

						JSONArray jsonArrayLocation = jsonArrayDesc
								.getJSONObject(i).getJSONArray(
										"location_detail");

						for (int j = 0; j < jsonArrayLocation.length(); j++) {
							JSONObject jsonObjectInner = jsonArrayLocation
									.getJSONObject(j);

							String loc_lat = jsonObjectInner.getString("lat");
							String loc_lng = jsonObjectInner.getString("lng");
							String loc_add = jsonObjectInner
									.getString("address");
							String loc_area = jsonObjectInner.getString("area");
							String loc_city = jsonObjectInner.getString("city");
							String loc_state = jsonObjectInner
									.getString("state");

							if (j == 0) {
								offer_lat = loc_lat;
								offer_lng = loc_lng;
								offer_add = loc_add;
								offer_area = loc_area;
								offer_city = loc_city;
								offer_state = loc_state;
							}

							insert_location.bindLong(2,
									Integer.parseInt(offer_id));
							insert_location.bindDouble(3,
									Double.parseDouble(loc_lat));
							insert_location.bindDouble(4,
									Double.parseDouble(loc_lng));
							insert_location.bindString(5, loc_add);
							insert_location.bindString(6, loc_area);
							insert_location.bindString(7, loc_city);
							insert_location.bindString(8, loc_state);
							insert_location.bindLong(9, 0);
							insert_location.bindDouble(10, 0);
							insert_location.bindLong(11, timestamp);

							insert_location.execute();
							insert_location.clearBindings();
						}

						insert_gift.bindLong(2, Integer.parseInt(deal_id));
						insert_gift.bindString(3, deal_name);
						insert_gift.bindString(4, url);
						insert_gift.bindString(5, catagory);
						insert_gift.bindLong(6, Integer.parseInt(catagory_id));
						insert_gift
								.bindDouble(7, Double.parseDouble(offer_lat));
						insert_gift
								.bindDouble(8, Double.parseDouble(offer_lng));
						insert_gift.bindString(9, offer_add);
						insert_gift.bindString(10, offer_area);
						insert_gift.bindString(11, offer_city);
						insert_gift.bindString(12, offer_state);
						insert_gift.bindLong(13, 0);
						insert_gift.bindLong(14, jsonArrayLocation.length());
						insert_gift.bindLong(15, Integer.parseInt(offer_id));
						insert_gift.bindString(16, offer_name);
						insert_gift.bindString(17, validity);
						insert_gift.bindString(18, desc);
						insert_gift.bindDouble(19, Double.parseDouble(min_amt));
						insert_gift.bindLong(20, timestamp);
						insert_gift.bindLong(21, 0);
						insert_gift.bindString(22, logo);
						insert_gift.bindString(23, dealer_contact);

						insert_gift.execute();
						insert_gift.clearBindings();
					} catch (Exception e) {
					}
				}
			}

		} catch (JSONException je) {
			// Log.d("Er ", "row inserted   " + je.getMessage());
			// je.printStackTrace();
		} catch (SQLException exception) {
			// Log.d("Er ", "row inserted   " + exception.getMessage());
			// TODO: handle exception
			// // // Log.e("Er ", exception.getMessage());
		} catch (Exception e) {
			// Log.d("Er ", "row inserted   " + e.getMessage());
			// TODO: handle exception
			// // // Log.e("Er ", e.getMessage());
		} finally {
			database.setTransactionSuccessful();
			database.endTransaction();
			close();
		}
	}

	public void createFreeBies(JSONArray jsonArrayAlldeals, long timestamp) {
		try {
			open();
			String gift = "INSERT INTO "
					+ Pay1GiftsSQLiteHelper.TABLE_GIFTS
					+ " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";

			String location = "INSERT INTO "
					+ Pay1GiftsSQLiteHelper.TABLE_LOCATION
					+ " VALUES (?,?,?,?,?,?,?,?,?,?,?);";
			SQLiteStatement insert_gift = database.compileStatement(gift);
			SQLiteStatement insert_location = database
					.compileStatement(location);
			database.beginTransaction();

			for (int i = 0; i < jsonArrayAlldeals.length(); i++) {
				Log.d("Inside createFreeBies", "updateFreeGiftData ");
				Log.d("update Data", "update Data "+jsonArrayAlldeals.getJSONObject(i));
				String deal_id = jsonArrayAlldeals.getJSONObject(i).getString(
						"id");
				String offer_id = jsonArrayAlldeals.getJSONObject(i).getString(
						"of_id");
				try {
					String claimedGifts=Utility.getSharedPreferences(dbContext, "claimedGifts");
					if(claimedGifts.contains(deal_id)){
					Log.e("inside claims", "Inside claims");
					}else{
						deleteFreeBies(Integer.parseInt(deal_id));
						database.delete(Pay1GiftsSQLiteHelper.TABLE_LOCATION,
								Pay1GiftsSQLiteHelper.LOCATION_DEAL_ID + " = "
										+ Integer.parseInt(offer_id), null);
						Log.e("Outside claims", "Outside claims");
					}
				} catch (Exception e) {
					continue;
				}
				String deal_name = jsonArrayAlldeals.getJSONObject(i)
						.getString("name");
				String url = jsonArrayAlldeals.getJSONObject(i).getString(
						"i_url");
				String logo = jsonArrayAlldeals.getJSONObject(i).getString(
						"logo_url");
				String catagory = jsonArrayAlldeals.getJSONObject(i).getString(
						"cat");
				if(catagory.equalsIgnoreCase("null")){
					catagory="0";
				}
				String catagory_id = jsonArrayAlldeals.getJSONObject(i)
						.getString("cat_id");
				if(catagory_id.equalsIgnoreCase("null")){
					catagory_id="0";
				}
				String min_amt = jsonArrayAlldeals.getJSONObject(i).getString(
						"min");
				String dealer_contact = jsonArrayAlldeals.getJSONObject(i)
						.getString("dealer_contact");
				String offer_name = jsonArrayAlldeals.getJSONObject(i)
						.getString("of_name");
				String validity = jsonArrayAlldeals.getJSONObject(i).getString(
						"valid");
				String desc = jsonArrayAlldeals.getJSONObject(i).getString(
						"offer_desc");
				String price = jsonArrayAlldeals.getJSONObject(i).getString(
						"price");
				String by_voucher = jsonArrayAlldeals.getJSONObject(i).getString(
						"by_voucher");
				String offer_price = jsonArrayAlldeals.getJSONObject(i).getString(
						"offer_price");

				String offer_lat = "0", offer_lng = "0", offer_add = "", offer_area = "", offer_city = "", offer_state = "";

				JSONArray jsonArrayLocation = jsonArrayAlldeals
						.getJSONObject(i).getJSONArray("locs");

				for (int j = 0; j < jsonArrayLocation.length(); j++) {
					JSONObject jsonObjectInner = jsonArrayLocation
							.getJSONObject(j);

					String loc_lat = jsonObjectInner.getString("lat");
					String loc_lng = jsonObjectInner.getString("lng");
					String loc_add = jsonObjectInner.getString("addr");
					String loc_area = jsonObjectInner.getString("area");
					String loc_city = jsonObjectInner.getString("city");
					String loc_state = jsonObjectInner.getString("state");

					if (j == 0) {
						offer_lat = loc_lat;
						offer_lng = loc_lng;
						offer_add = loc_add;
						offer_area = loc_area;
						offer_city = loc_city;
						offer_state = loc_state;
					}

					insert_location.bindLong(2, Integer.parseInt(offer_id));
					insert_location.bindDouble(3, Double.parseDouble(loc_lat));
					insert_location.bindDouble(4, Double.parseDouble(loc_lng));
					insert_location.bindString(5, loc_add);
					insert_location.bindString(6, loc_area);
					insert_location.bindString(7, loc_city);
					insert_location.bindString(8, loc_state);
					insert_location.bindLong(9, 0);
					insert_location.bindDouble(10, 0);
					insert_location.bindLong(11, timestamp);

					insert_location.execute();
					insert_location.clearBindings();
				}

				insert_gift.bindLong(2, Integer.parseInt(deal_id));
				insert_gift.bindString(3, deal_name);
				insert_gift.bindString(4, url);
				insert_gift.bindString(5, catagory);
				insert_gift.bindLong(6, Integer.parseInt(catagory_id));
				insert_gift.bindDouble(7, Double.parseDouble(offer_lat));
				insert_gift.bindDouble(8, Double.parseDouble(offer_lng));
				insert_gift.bindString(9, offer_add);
				insert_gift.bindString(10, offer_area);
				insert_gift.bindString(11, offer_city);
				insert_gift.bindString(12, offer_state);
				insert_gift.bindLong(13, 0);
				insert_gift.bindLong(14, jsonArrayLocation.length());
				insert_gift.bindLong(15, Integer.parseInt(offer_id));
				insert_gift.bindString(16, offer_name);
				insert_gift.bindString(17, validity);
				insert_gift.bindString(18, desc);
				insert_gift.bindDouble(19, Double.parseDouble(min_amt));
				insert_gift.bindLong(20, timestamp);
				insert_gift.bindLong(21, 0);
				insert_gift.bindString(22, logo);
				insert_gift.bindString(23, dealer_contact);
				Log.d("Data Inserted1", "Data Inserted1 "+deal_name);
				insert_gift.bindLong(24,Integer.parseInt(price));
				Log.d("Data Inserted1", "Data Inserted1 price "+price);
				insert_gift.bindLong(26,Integer.parseInt(offer_price));
				Log.d("Data Inserted1", "Data Inserted1 offer_price "+offer_price);
				insert_gift.bindLong(25,Integer.parseInt(by_voucher));
				Log.d("Data Inserted1", "Data Inserted1 by_voucher "+by_voucher);
				Log.d("Data Inserted2", "Data Inserted2 "+deal_name);
				insert_gift.execute();
				insert_gift.clearBindings();

				Log.d("Data Inserted", "Data Inserted "+deal_name);
			}

		} catch (JSONException je) {
			Log.d("Er1 ", "row inserted   " + je.getMessage());
			je.printStackTrace();
			// je.printStackTrace();
		} catch (SQLException exception) {
			Log.d("Er2 ", "row inserted   " + exception.getMessage());
			exception.printStackTrace();
			// TODO: handle exception
			// // // Log.e("Er ", exception.getMessage());
		} catch (Exception e) {
			Log.d("Er3 ", "row inserted   " + e.getMessage());
			e.printStackTrace();
			// TODO: handle exception
			// // // Log.e("Er ", e.getMessage());
		} finally {
			database.setTransactionSuccessful();
			database.endTransaction();
			close();
		}
	}

	public void createFreeBies(int freebieDealID, String freebieDealName,
			String freebieURL, String freebieCategory, int freebieCategoryID,
			double freebieLat, double freebieLng, String freebieAddress,
			String freebieArea, String freebieCity, String freebieState,
			int freebiePin, int freebieLocationCount, int freebieOfferID,
			String freebieOfferName, String freebieValidity,
			String freebieShortDesc, int freebieMinAmount,
			long freebieUptateTime, int freebieLike, String freebieLogoURL,
			String freebieDealerMobile,int price) {

		ContentValues values = new ContentValues();
		values.put(Pay1GiftsSQLiteHelper.FG_DEAL_ID, freebieDealID);
		values.put(Pay1GiftsSQLiteHelper.FG_DEAL_NAME, freebieDealName);
		values.put(Pay1GiftsSQLiteHelper.FG_URL, freebieURL);
		values.put(Pay1GiftsSQLiteHelper.FG_CATAGORY, freebieCategory);
		values.put(Pay1GiftsSQLiteHelper.FG_CATAGORY_ID, freebieCategoryID);
		values.put(Pay1GiftsSQLiteHelper.FG_LAT, freebieLat);
		values.put(Pay1GiftsSQLiteHelper.FG_LNG, freebieLng);
		values.put(Pay1GiftsSQLiteHelper.FG_ADDRESS, freebieAddress);
		values.put(Pay1GiftsSQLiteHelper.FG_AREA, freebieArea);
		values.put(Pay1GiftsSQLiteHelper.FG_CITY, freebieCity);
		values.put(Pay1GiftsSQLiteHelper.FG_STATE, freebieState);
		values.put(Pay1GiftsSQLiteHelper.FG_PIN, freebiePin);
		values.put(Pay1GiftsSQLiteHelper.FG_LOCATION_COUNT,
				freebieLocationCount);
		values.put(Pay1GiftsSQLiteHelper.FG_OFFER_ID, freebieOfferID);
		values.put(Pay1GiftsSQLiteHelper.FG_OFFER_NAME, freebieOfferName);
		values.put(Pay1GiftsSQLiteHelper.FG_VALIDITY, freebieValidity);
		values.put(Pay1GiftsSQLiteHelper.FG_DESC, freebieShortDesc);
		values.put(Pay1GiftsSQLiteHelper.FG_MIN_AMOUNT, freebieMinAmount);
		values.put(Pay1GiftsSQLiteHelper.FG_UPDATED_TIME, freebieUptateTime);
		values.put(Pay1GiftsSQLiteHelper.FG_LIKE, freebieLike);
		values.put(Pay1GiftsSQLiteHelper.FG_LOGO_URL, freebieLogoURL);
		values.put(Pay1GiftsSQLiteHelper.FG_DEALER_CONTACT, freebieDealerMobile);

		database.insert(Pay1GiftsSQLiteHelper.TABLE_GIFTS, null, values);
		// Cursor cursor = database.query(Pay1GiftsSQLiteHelper.TABLE_FreeBies,
		// null, Pay1GiftsSQLiteHelper.FreeBies_ID + " = " + insertId, null,
		// null, null, null);
		// cursor.moveToFirst();
		// FreeBies neFreeBies = cursorToFreeBies(cursor);
		// cursor.close();
		// return newFreeBies;
	}

	public void updateFreeBies(int freebieLike, String offerIds) {
		ContentValues values = new ContentValues();
		values.put(Pay1GiftsSQLiteHelper.FG_LIKE, freebieLike);
		database.update(Pay1GiftsSQLiteHelper.TABLE_GIFTS, values,
				Pay1GiftsSQLiteHelper.FG_OFFER_ID + " In ( " + offerIds + " )",
				null);
	}

	public void deleteFreeBies() {
		database.delete(Pay1GiftsSQLiteHelper.TABLE_GIFTS, null, null);
	}

	public void deleteFreeBies(int deal_id) {
		database.delete(Pay1GiftsSQLiteHelper.TABLE_GIFTS,
				Pay1GiftsSQLiteHelper.FG_DEAL_ID + " = " + deal_id, null);
		Log.d("after Delete", "aafter delete");
	}

	public void deleteFreeBies(String offer_id) {
		
		database.delete(Pay1GiftsSQLiteHelper.TABLE_GIFTS,
				Pay1GiftsSQLiteHelper.FG_OFFER_ID + " In ( " + offer_id + " )",
				null);
	}

	public List<FreeBies> getAllFreeBies() {
		List<FreeBies> freebies = new ArrayList<FreeBies>();

		Cursor cursor = database.query(Pay1GiftsSQLiteHelper.TABLE_GIFTS, null,
				null, null, null, null, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			FreeBies freebie = cursorToFreeBies(cursor);
			freebies.add(freebie);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return freebies;
	}

	public List<FreeBies> getAllFreeBies(String order) {

		List<FreeBies> freebies = new ArrayList<FreeBies>();

		Cursor cursor;
		if (order != "") {
			String[] orders = order.split(",");

			StringBuffer sb = new StringBuffer();
			sb.append(" case ").append(Pay1GiftsSQLiteHelper.FG_OFFER_ID)
					.append(" when ").append(orders[0]).append(" then ")
					.append("1");
			for (int i = 0; i < orders.length; i++) {
				if (i != 0)
					sb.append(" when ").append(orders[i]).append(" then ")
							.append(i + 1);
			}
			sb.append(" end ");
			cursor = database.query(Pay1GiftsSQLiteHelper.TABLE_GIFTS, null,
					Pay1GiftsSQLiteHelper.FG_OFFER_ID + " In (" + order + ")",
					null, null, null, String.valueOf(sb));
		} else {
			cursor = database.query(Pay1GiftsSQLiteHelper.TABLE_GIFTS, null,
					null, null, null, null, Pay1GiftsSQLiteHelper.FG_OFFER_ID
							+ " desc");
		}

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			FreeBies freebie = cursorToFreeBies(cursor);
			freebies.add(freebie);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return freebies;
	}

	public List<FreeBies> getAllNearFreeBies(double lat, double lng, int dis) {

		List<FreeBies> freebies = new ArrayList<FreeBies>();

		// Cursor cursor = database.rawQuery("SELECT d.* FROM "
		// + "(SELECT *,(3956 * 2 * ASIN(SQRT( POW(SIN((" + lat
		// + " - abs(" + Pay1GiftsSQLiteHelper.FG_LAT
		// + "))*pi()/180/2),2) " + " + COS(" + lat
		// + " *pi()/180 ) * COS(abs(" + Pay1GiftsSQLiteHelper.FG_LAT
		// + ")*pi()/180) * POW(SIN((" + lng + "-"
		// + Pay1GiftsSQLiteHelper.FG_LNG + ")"
		// + " * pi()/180/2),2)))) as distance " + " FROM "
		// + Pay1GiftsSQLiteHelper.TABLE_GIFTS
		// + ") as d WHERE d.distance > " + dis + " order by d.distance ",
		// null);

		Cursor cursor = database.query(Pay1GiftsSQLiteHelper.TABLE_GIFTS, null,
				null, null, null, null, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			FreeBies freebie = cursorToFreeBies(cursor);
			double d = Constants.distanceFrom(lat, lng,
					freebie.getFreebieLat(), freebie.getFreebieLng());
			System.out.println("Distance " + d);
			if (d < dis)
				freebies.add(freebie);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return freebies;
	}

	public List<FreeBies> getAllLikeFreeBies(int like) {

		List<FreeBies> freebies = new ArrayList<FreeBies>();

		Cursor cursor = database.query(Pay1GiftsSQLiteHelper.TABLE_GIFTS, null,
				Pay1GiftsSQLiteHelper.FG_LIKE + " = " + like, null, null, null,
				Pay1GiftsSQLiteHelper.FG_UPDATED_TIME + " desc");

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			FreeBies freebie = cursorToFreeBies(cursor);
			freebies.add(freebie);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return freebies;
	}

	public List<FreeBies> getAllSearchFreeBies(String query) {

		List<FreeBies> freebies = new ArrayList<FreeBies>();

		Cursor cursor = database.query(Pay1GiftsSQLiteHelper.TABLE_GIFTS, null,
				Pay1GiftsSQLiteHelper.FG_OFFER_NAME + " LIKE  '%" + query
						+ "%' OR " + Pay1GiftsSQLiteHelper.FG_DEAL_NAME
						+ " LIKE  '%" + query + "%' OR "
						+ Pay1GiftsSQLiteHelper.FG_DESC + " LIKE  '%" + query
						+ "%' OR " + Pay1GiftsSQLiteHelper.FG_ADDRESS
						+ " LIKE  '%" + query + "%' OR "
						+ Pay1GiftsSQLiteHelper.FG_CITY + " LIKE  '%" + query
						+ "%' OR " + Pay1GiftsSQLiteHelper.FG_AREA
						+ " LIKE  '%" + query + "%' OR "
						+ Pay1GiftsSQLiteHelper.FG_STATE + " LIKE  '%" + query
						+ "%' ", null, null, null,
				Pay1GiftsSQLiteHelper.FG_UPDATED_TIME + " desc");

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			FreeBies freebie = cursorToFreeBies(cursor);
			freebies.add(freebie);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return freebies;
	}

	public int getAllFreeBiesCount(String expired, String claimed) {

		Cursor cursor = database.query(Pay1GiftsSQLiteHelper.TABLE_GIFTS, null,
				Pay1GiftsSQLiteHelper.FG_OFFER_ID + " Not In (" + expired
						+ ") AND " + Pay1GiftsSQLiteHelper.FG_OFFER_ID
						+ " Not In (" + claimed + ")", null, null, null, null);

		return cursor.getCount();
	}

	public List<FreeBies> getAllFreeBies(String order, String claimed) {

		List<FreeBies> freebies = new ArrayList<FreeBies>();

		Cursor cursor;
		if (order != "") {
			String[] orders = order.split(",");

			StringBuffer sb = new StringBuffer();
			sb.append(" case ").append(Pay1GiftsSQLiteHelper.FG_OFFER_ID)
					.append(" when ").append(orders[0]).append(" then ")
					.append("1");
			for (int i = 0; i < orders.length; i++) {
				if (i != 0)
					sb.append(" when ").append(orders[i]).append(" then ")
							.append(i + 1);
			}
			sb.append(" end ");
			cursor = database.query(Pay1GiftsSQLiteHelper.TABLE_GIFTS, null,
					Pay1GiftsSQLiteHelper.FG_OFFER_ID + " In (" + order
							+ ") AND " + Pay1GiftsSQLiteHelper.FG_OFFER_ID
							+ " Not In (" + claimed + ")", null, null, null,
					String.valueOf(sb));
		} else {
			cursor = database.query(Pay1GiftsSQLiteHelper.TABLE_GIFTS, null,
					null, null, null, null, Pay1GiftsSQLiteHelper.FG_OFFER_ID
							+ " desc");
		}

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			FreeBies freebie = cursorToFreeBies(cursor);
			freebies.add(freebie);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return freebies;
	}

	public FreeBies getFreeBie(int deal_id) {

		Cursor cursor = database.query(Pay1GiftsSQLiteHelper.TABLE_GIFTS, null,
				Pay1GiftsSQLiteHelper.FG_DEAL_ID + " = " + deal_id, null, null,
				null, null);

		if (cursor.getCount() > 0) {
			cursor.moveToFirst();
			FreeBies freeBies = cursorToFreeBies(cursor);
			cursor.moveToNext();
			// make sure to close the cursor
			cursor.close();
			return freeBies;
		} else {
			return null;
		}
	}

	public FreeBies getTopFreeBie() {

		Cursor cursor = database.query(Pay1GiftsSQLiteHelper.TABLE_GIFTS, null,
				null, null, null, null, Pay1GiftsSQLiteHelper.FG_UPDATED_TIME
						+ " desc limit 1");

		if (cursor.getCount() > 0) {
			cursor.moveToFirst();
			FreeBies freeBies = cursorToFreeBies(cursor);
			cursor.moveToNext();
			// make sure to close the cursor
			cursor.close();
			return freeBies;
		} else {
			return null;
		}
	}

	private FreeBies cursorToFreeBies(Cursor cursor) {
		FreeBies freeBies = new FreeBies();
		freeBies.setFreebieID(cursor.getInt(0));
		freeBies.setFreebieDealID(cursor.getInt(1));
		freeBies.setFreebieDealName(cursor.getString(2));
		freeBies.setFreebieURL(cursor.getString(3));
		freeBies.setFreebieCategory(cursor.getString(4));
		freeBies.setFreebieCategoryID(cursor.getInt(5));
		freeBies.setFreebieLat(cursor.getDouble(6));
		freeBies.setFreebieLng(cursor.getDouble(7));
		freeBies.setFreebieAddress(cursor.getString(8));
		freeBies.setFreebieArea(cursor.getString(9));
		freeBies.setFreebieCity(cursor.getString(10));
		freeBies.setFreebieState(cursor.getString(11));
		freeBies.setFreebiePin(cursor.getInt(12));
		freeBies.setFreebieLocationCount(cursor.getInt(13));
		freeBies.setFreebieOfferID(cursor.getInt(14));
		freeBies.setFreebieOfferName(cursor.getString(15));
		freeBies.setFreebieValidity(cursor.getString(16));
		freeBies.setFreebieShortDesc(cursor.getString(17));
		freeBies.setFreebieMinAmount(cursor.getInt(18));
		freeBies.setFreebieUptateTime(cursor.getLong(19));
		freeBies.setFreebieLike(cursor.getInt(20));
		freeBies.setFreebieLogoURL(cursor.getString(21));
		freeBies.setFreebieDealerMobile(cursor.getString(22));
		freeBies.setPrice(cursor.getInt(23));
		freeBies.setFreeBieOfferPrice(cursor.getInt(25));
		freeBies.setFreeBieByVoucher(cursor.getInt(24));
		
		return freeBies;
	}

	public void createFreeBiesLocation(int locationDealID, double locationLat,
			double locationLng, String locationAddress, String locationArea,
			String locationCity, String locationState, int locationPin,
			double locationDistance, long locationUpdatedTime) {

		ContentValues values = new ContentValues();
		values.put(Pay1GiftsSQLiteHelper.LOCATION_DEAL_ID, locationDealID);
		values.put(Pay1GiftsSQLiteHelper.LOCATION_LAT, locationLat);
		values.put(Pay1GiftsSQLiteHelper.LOCATION_LNG, locationLng);
		values.put(Pay1GiftsSQLiteHelper.LOCATION_ADDRESS, locationAddress);
		values.put(Pay1GiftsSQLiteHelper.LOCATION_AREA, locationArea);
		values.put(Pay1GiftsSQLiteHelper.LOCATION_CITY, locationCity);
		values.put(Pay1GiftsSQLiteHelper.LOCATION_STATE, locationState);
		values.put(Pay1GiftsSQLiteHelper.LOCATION_PIN, locationPin);
		values.put(Pay1GiftsSQLiteHelper.LOCATION_DISTANCE, locationDistance);
		values.put(Pay1GiftsSQLiteHelper.LOCATION_UPDATED_TIME,
				locationUpdatedTime);

		database.insert(Pay1GiftsSQLiteHelper.TABLE_LOCATION, null, values);
		// Cursor cursor =
		// database.query(Pay1GiftsSQLiteHelper.TABLE_FreeBiesLocation,
		// null, Pay1GiftsSQLiteHelper.FreeBiesLocation_ID + " = " + insertId,
		// null,
		// null, null, null);
		// cursor.moveToFirst();
		// FreeBiesLocation neFreeBiesLocation =
		// cursorToFreeBiesLocation(cursor);
		// cursor.close();
		// return newFreeBiesLocation;
	}

	public void deleteFreeBiesLocation() {
		database.delete(Pay1GiftsSQLiteHelper.TABLE_LOCATION, null, null);
	}

	public void deleteFreeBiesLocation(int offer_ID) {
		database.delete(Pay1GiftsSQLiteHelper.TABLE_LOCATION,
				Pay1GiftsSQLiteHelper.LOCATION_DEAL_ID + " = " + offer_ID, null);
	}

	public void deleteFreeBiesLocation(String offer_ID) {
		database.delete(Pay1GiftsSQLiteHelper.TABLE_LOCATION,
				Pay1GiftsSQLiteHelper.LOCATION_DEAL_ID + " In ( " + offer_ID
						+ " )", null);
	}

	public List<FreeBiesLocation> getAllFreeBiesLocation() {
		List<FreeBiesLocation> FreeBiesLocation = new ArrayList<FreeBiesLocation>();

		Cursor cursor = database.query(Pay1GiftsSQLiteHelper.TABLE_LOCATION,
				null, null, null, null, null, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			FreeBiesLocation freebie = cursorToFreeBiesLocation(cursor);
			FreeBiesLocation.add(freebie);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return FreeBiesLocation;
	}

	public List<FreeBiesLocation> getAllFreeBiesLocation(int offer_id) {
		List<FreeBiesLocation> FreeBiesLocation = new ArrayList<FreeBiesLocation>();

		Cursor cursor = database.query(Pay1GiftsSQLiteHelper.TABLE_LOCATION,
				null,
				Pay1GiftsSQLiteHelper.LOCATION_DEAL_ID + " = " + offer_id,
				null, null, null, Pay1GiftsSQLiteHelper.LOCATION_ID);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			FreeBiesLocation freebie = cursorToFreeBiesLocation(cursor);
			FreeBiesLocation.add(freebie);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return FreeBiesLocation;
	}

	public FreeBiesLocation getFreeBiesLocation() {

		Cursor cursor = database.query(Pay1GiftsSQLiteHelper.TABLE_LOCATION,
				null, null, null, null, null,
				Pay1GiftsSQLiteHelper.LOCATION_UPDATED_TIME + " desc limit 1");

		if (cursor.getCount() > 0) {
			cursor.moveToFirst();
			FreeBiesLocation FreeBiesLocation = cursorToFreeBiesLocation(cursor);
			cursor.moveToNext();
			// make sure to close the cursor
			cursor.close();
			return FreeBiesLocation;
		} else {
			return null;
		}
	}

	private FreeBiesLocation cursorToFreeBiesLocation(Cursor cursor) {
		FreeBiesLocation freeBiesLocation = new FreeBiesLocation();
		freeBiesLocation.setLocationID(cursor.getInt(0));
		freeBiesLocation.setLocationDealID(cursor.getInt(1));
		freeBiesLocation.setLocationLat(cursor.getDouble(2));
		freeBiesLocation.setLocationLng(cursor.getDouble(3));
		freeBiesLocation.setLocationAddress(cursor.getString(4));
		freeBiesLocation.setLocationArea(cursor.getString(5));
		freeBiesLocation.setLocationCity(cursor.getString(6));
		freeBiesLocation.setLocationState(cursor.getString(7));
		freeBiesLocation.setLocationPin(cursor.getInt(8));
		freeBiesLocation.setLocationDistance(cursor.getDouble(9));
		freeBiesLocation.setLocationUpdatedTime(cursor.getLong(10));
		return freeBiesLocation;
	}

}
