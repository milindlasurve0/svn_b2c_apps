package com.pay1.customviews;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.pay1.MyFreeBiesDetailsActivity;
import com.pay1.R;
import com.pay1.databasehandler.FreeBies;
import com.squareup.picasso.Picasso;

public class GiftTopCellLayout extends LinearLayout {

	Context myContext;
	ArrayList<FreeBies> data = new ArrayList<FreeBies>();
	Typeface Reguler;

	public GiftTopCellLayout(Context context) {
		super(context);
		myContext = context;
		Reguler = Typeface.createFromAsset(context.getAssets(),
				"EncodeSansNormal-400-Regular.ttf");
	}

	public GiftTopCellLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		myContext = context;
		Reguler = Typeface.createFromAsset(context.getAssets(),
				"EncodeSansNormal-400-Regular.ttf");
	}

	@SuppressLint("NewApi")
	public GiftTopCellLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		myContext = context;
		Reguler = Typeface.createFromAsset(context.getAssets(),
				"EncodeSansNormal-400-Regular.ttf");
	}

	public void add(FreeBies item) {
		int newIdx = data.size();
		data.add(item);
		addView(getView(newIdx));
	}

	private View getView(final int position) {
		LayoutInflater inflater = (LayoutInflater) myContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		final Context contextThemeWrapper = new ContextThemeWrapper(myContext,
				R.style.StyledIndicators);
		inflater = inflater.cloneInContext(contextThemeWrapper);
		View view = inflater.inflate(R.layout.gift_top_cell_adapter, null,
				false);
		try {
			TextView textView_OfferName = (TextView) view
					.findViewById(R.id.textView_OfferName);
			textView_OfferName
					.setText(data.get(position).getFreebieOfferName());
			textView_OfferName.setTypeface(Reguler, Typeface.BOLD);

			ImageView imageView_Photo = (ImageView) view
					.findViewById(R.id.imageView_Photo);
			// Bitmap bm = null;
			// if (position < itemList.size()) {
			// bm = decodeSampledBitmapFromUri(itemList.get(position)
			// .getFreebieURL(), 60, 60);
			// }
			// imageView_Photo.setImageBitmap(getRoundedCornerBitmap(bm, 10));
			try {
				Picasso.with(myContext)
						.load(data.get(position).getFreebieURL())
						.placeholder(R.drawable.deal_default).fit()
						.into(imageView_Photo);
			} catch (Exception e) {
			}

			view.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					try {
						FreeBies freeBie = data.get(position);
						Intent intent = new Intent(myContext,
								MyFreeBiesDetailsActivity.class);
						intent.putExtra("GIFT", freeBie);
						intent.putExtra("IS_BUY", 0);
						myContext.startActivity(intent);
					} catch (Exception e) {
					}
				}
			});
		} catch (Exception e) {
		}
		return view;
	}

	public Bitmap decodeSampledBitmapFromUri(String path, int reqWidth,
			int reqHeight) {
		Bitmap bm = null;

		// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(path, options);

		// Calculate inSampleSize
		options.inSampleSize = calculateInSampleSize(options, reqWidth,
				reqHeight);

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;
		bm = BitmapFactory.decodeFile(path, options);

		return bm;
	}

	public int calculateInSampleSize(

	BitmapFactory.Options options, int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {
			if (width > height) {
				inSampleSize = Math.round((float) height / (float) reqHeight);
			} else {
				inSampleSize = Math.round((float) width / (float) reqWidth);
			}
		}

		return inSampleSize;
	}

	public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, int pixels) {
		Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
				bitmap.getHeight(), Config.ARGB_8888);
		Canvas canvas = new Canvas(output);

		final int color = 0xff424242;
		final Paint paint = new Paint();
		final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
		final RectF rectF = new RectF(rect);
		final float roundPx = pixels;

		paint.setAntiAlias(true);
		canvas.drawARGB(0, 0, 0, 0);
		paint.setColor(color);
		canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

		paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
		canvas.drawBitmap(bitmap, rect, rect, paint);

		return output;
	}
}
