package com.pay1.customviews;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.pay1.R;

public class AppRater {
	private final static String APP_TITLE = "Pay1";
	private final static String APP_PNAME = "com.pay1";

	private final static int DAYS_UNTIL_PROMPT = 3;
	private final static int LAUNCHES_UNTIL_PROMPT = 7;

	public static void app_launched(Context mContext) {
		SharedPreferences prefs = mContext.getSharedPreferences("apprater", 0);
		if (prefs.getBoolean("dontshowagain", false)) {
			return;
		}

		SharedPreferences.Editor editor = prefs.edit();

		// Increment launch counter
		long launch_count = prefs.getLong("launch_count", 0) + 1;
		editor.putLong("launch_count", launch_count);

		// Get date of first launch
		Long date_firstLaunch = prefs.getLong("date_firstlaunch", 0);
		if (date_firstLaunch == 0) {
			date_firstLaunch = System.currentTimeMillis();
			editor.putLong("date_firstlaunch", date_firstLaunch);
		}

		// Wait at least n days before opening
		if (launch_count >= LAUNCHES_UNTIL_PROMPT) {
			if (System.currentTimeMillis() >= date_firstLaunch
					+ (DAYS_UNTIL_PROMPT * 24 * 60 * 60 * 1000)) {
				showConfirmationDialog(mContext, editor);
			}
		}

		editor.commit();
	}

	// public static void showRateDialog(final Context mContext,
	// final SharedPreferences.Editor editor) {
	// final Dialog dialog = new Dialog(mContext);
	// dialog.setTitle("Rate " + APP_TITLE);
	//
	// LinearLayout ll = new LinearLayout(mContext);
	// ll.setOrientation(LinearLayout.VERTICAL);
	//
	// TextView tv = new TextView(mContext);
	// tv.setText("If you enjoy using " + APP_TITLE
	// + ", please take a moment to rate it. Thanks for your support!");
	// tv.setWidth(240);
	// tv.setPadding(4, 0, 4, 10);
	// ll.addView(tv);
	//
	// Button b1 = new Button(mContext);
	// b1.setText("Rate " + APP_TITLE);
	// b1.setOnClickListener(new View.OnClickListener() {
	// public void onClick(View v) {
	// mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri
	// .parse("market://details?id=" + APP_PNAME)));
	// dialog.dismiss();
	// }
	// });
	// ll.addView(b1);
	//
	// Button b2 = new Button(mContext);
	// b2.setText("Remind me later");
	// b2.setOnClickListener(new View.OnClickListener() {
	// public void onClick(View v) {
	// dialog.dismiss();
	// }
	// });
	// ll.addView(b2);
	//
	// Button b3 = new Button(mContext);
	// b3.setText("No, thanks");
	// b3.setOnClickListener(new View.OnClickListener() {
	// public void onClick(View v) {
	// if (editor != null) {
	// editor.putBoolean("dontshowagain", true);
	// editor.commit();
	// }
	// dialog.dismiss();
	// }
	// });
	// ll.addView(b3);
	//
	// dialog.setContentView(ll);
	// dialog.show();
	// }

	public static void showConfirmationDialog(final Context context,
			final SharedPreferences.Editor editor) {
		try {

			final Dialog dialog = new Dialog(context);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			// dialog.getWindow().setBackgroundDrawableResource(
			// android.R.color.transparent);
			dialog.setContentView(R.layout.dialog_two_button);
			dialog.setCancelable(false);
			dialog.setCanceledOnTouchOutside(false);
			// dialog.setTitle(null);

			Typeface Reguler = Typeface.createFromAsset(context.getAssets(),
					"EncodeSansNormal-400-Regular.ttf");

			Typeface Medium = Typeface.createFromAsset(context.getAssets(),
					"EncodeSansNarrow-500-Medium.ttf");

			// set the custom dialog components - text, image and button
			TextView textView_Title = (TextView) dialog
					.findViewById(R.id.textView_Title);
			textView_Title.setText("Rate " + APP_TITLE);
			textView_Title.setTypeface(Reguler);

			TextView textView_Message = (TextView) dialog
					.findViewById(R.id.textView_Message);
			textView_Message
					.setText("If you enjoy using "
							+ APP_TITLE
							+ ", please take a moment to rate it. Thanks for your support!");
			textView_Message.setTypeface(Medium);

			Button button_Ok = (Button) dialog.findViewById(R.id.button_Ok);
			button_Ok.setTypeface(Reguler);
			button_Ok.setText("Rate Now");
			// if button is clicked, close the custom dialog
			button_Ok.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
					context.startActivity(new Intent(Intent.ACTION_VIEW, Uri
							.parse("market://details?id=" + APP_PNAME)));
				}
			});

			Button button_Cancel = (Button) dialog
					.findViewById(R.id.button_Cancel);
			button_Cancel.setTypeface(Reguler);
			button_Cancel.setText("Rate Later");
			// if button is clicked, close the custom dialog
			button_Cancel.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
				}
			});

			// ImageView imageView_Close = (ImageView) dialog
			// .findViewById(R.id.imageView_Close);
			// imageView_Close.setOnClickListener(new View.OnClickListener() {
			//
			// @Override
			// public void onClick(View v) {
			// // TODO Auto-generated method stub
			// dialog.dismiss();
			// }
			// });
			dialog.show();

		} catch (Exception exception) {
		}
	}
}