<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Payment_model extends CI_Model {
    
    public function __construct()
	{
		$this->load->database();
	}
        
    public function get_banklist($slug = FALSE)
        {
            $query = $this->db->get_where('banklist', array('toshow' => 1));
            return $query->result_array();
               
        }
        
        public function get_transaction($id)
        {
            $query = $this->db->get_where('transactions', array('trans_id' => $id));
            if($query->num_rows){
            $result = $query->result_array();
            return $result[0]['users_id']; 
            }
            else{
                return "0";
            }
               
        }
        
        public function get_walletbalance($id){
            $query = $this->db->get_where('wallets', array('users_id' => $id));
            if($query->num_rows){
            $result = $query->result_array();
            return $result[0]['account_balance']; 
            }
            else{
                return "0";
            }
        }
}