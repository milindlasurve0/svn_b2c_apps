﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Imaging;

namespace NewPay1B2C.UtilityClass
    {
    class OperatorConstant
        {
        public static String operatorImageName = "";
        public static BitmapImage getOperatorResource (int op_code)
            {
           
            switch (op_code)
                {

                case 1:
                case 44:
                    operatorImageName = "m_aircel";
                    break;

                case 2:
                case 45:
                case 42:
                    operatorImageName = "m_airtel";
                    break;
                case 3:
                case 46:
                case 38:
                    operatorImageName = "m_bsnl";
                    break;
                case 4:
                case 47:
                case 39:
                    operatorImageName = "m_idea";
                    break;
                case 5:
                case 37:
                    operatorImageName = "m_loop";
                    break;
                case 6:
                case 49:
                    operatorImageName = "m_mts";
                    break;
                case 7:
                case 50:
                case 43:
                    operatorImageName = "m_reliance_cdma";
                    break;
                case 8:
                    operatorImageName = "m_reliance";
                    break;
                case 9:
                case 51:
                case 36:
                    operatorImageName = "m_docomo";
                    break;
                case 10:
                case 40:
                    operatorImageName = "m_indicom";
                    break;
                case 11:
                    operatorImageName = "m_uninor";
                    break;
                case 12:
                    operatorImageName = "m_videocon";
                    break;
                case 15:
                case 52:
                case 41:
                    operatorImageName = "m_vodafone";
                    break;
                case 30:
                case 48:
                    operatorImageName = "m_mtnl";
                    break;
                case 16:
                    operatorImageName = "m_airtel";
                    break;
                case 17:
                    operatorImageName = "d_bigtv";
                    break;
                case 18:
                    operatorImageName = "d_dishtv";
                    break;
                case 19:
                    operatorImageName = "d_sundirect";
                    break;
                case 20:
                    operatorImageName = "d_tatasky";
                    break;
                case 21:
                    operatorImageName = "d_videocon";
                    break;
                default:
                    operatorImageName = "m_loop";
                    break;

                    
                }

            return new BitmapImage (new Uri ("/Pay1Images/" + operatorImageName + ".png", UriKind.RelativeOrAbsolute));
            }

		public static Uri getOperatorResourcePath (int op_code)
			{

			switch (op_code)
				{

				case 1:
				case 44:
					operatorImageName = "m_aircel";
					break;

				case 2:
				case 45:
				case 42:
					operatorImageName = "m_airtel";
					break;
				case 3:
				case 46:
				case 38:
					operatorImageName = "m_bsnl";
					break;
				case 4:
				case 47:
				case 39:
					operatorImageName = "m_idea";
					break;
				case 5:
				case 37:
					operatorImageName = "m_loop";
					break;
				case 6:
				case 49:
					operatorImageName = "m_mts";
					break;
				case 7:
				case 50:
				case 43:
					operatorImageName = "m_reliance_cdma";
					break;
				case 8:
					operatorImageName = "m_reliance";
					break;
				case 9:
				case 51:
				case 36:
					operatorImageName = "m_docomo";
					break;
				case 10:
				case 40:
					operatorImageName = "m_indicom";
					break;
				case 11:
					operatorImageName = "m_uninor";
					break;
				case 12:
					operatorImageName = "m_videocon";
					break;
				case 15:
				case 52:
				case 41:
					operatorImageName = "m_vodafone";
					break;
				case 30:
				case 48:
					operatorImageName = "m_mtnl";
					break;
				case 16:
					operatorImageName = "m_airtel";
					break;
				case 17:
					operatorImageName = "d_bigtv";
					break;
				case 18:
					operatorImageName = "d_dishtv";
					break;
				case 19:
					operatorImageName = "d_sundirect";
					break;
				case 20:
					operatorImageName = "d_tatasky";
					break;
				case 21:
					operatorImageName = "d_videocon";
					break;
				default:
					operatorImageName = "m_loop";
					break;


				}

			return new Uri ("/Pay1Images/" + operatorImageName + ".png", UriKind.RelativeOrAbsolute);
			}

        }
    }
