//
//  BaseViewController.h
//  Pay1
//
//  Created by Annapurna on 04/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#import "UIImageView+WebCache.h"
#import <GoogleMaps/GoogleMaps.h>
#import "Session.h"
#import "SignInResponse.h"
#import "Constant.h"
#import "SWRevealViewController.h"
#import "AppDelegate.h"

@class OfferView;
@class MBProgressHUD;
@class NoNetworkView;
@class CustomAlertView;
@class TransactionHistoryView;
@class DatePickerView;

@interface BaseViewController : UIViewController<UIGestureRecognizerDelegate>{
    UIButton *offerButton;
  //  BOOL isOfferButtonClicked;
    IBOutlet OfferView *objOfferView;
    MBProgressHUD *HUD;
    
    NoNetworkView *objNoNetworkView;
    CustomAlertView *objCustomAlertView;
    TransactionHistoryView *transactionAlertView;
    DatePickerView *datePickerView;

   // UIImageView *bgImgView;
   // UIView *dimView;
    
    UIView *  coverBaseView ;

    SWRevealViewController *revealViewController;
    

}

@property(nonatomic, copy) void (^onRetryClick)();
@property(nonatomic, copy) void (^onDoneClickOfCustomAlert)();
@property(nonatomic, copy) void (^onSubmitClick)();
@property (nonatomic, copy) void (^onDatePickerSetClick)();
@property (nonatomic, copy) void (^onDatePickerCancelClick)();




-(void)setPlaceHolderColor:(UITextField *)textField;
-(void)setViewOfButton:(UIButton *)button;
-(void)setConfirmVIewOfButton:(UIButton *)button;
-(void)hideNavBackButton;
-(UIButton *)setImageOnHeader:(NSString *)imageName isRight:(BOOL)isRight;
-(void)addOfferButtonOnAllView;
-(UIButton *) setNaviationButtonWithImage:(NSString *)imageName selector:(SEL) selector isLeft:(BOOL)isLeft;
-(void)setTwoNavigationButtonsWithImages:(NSString*)firstImage secondImage:(NSString*)secondImage thirdImage:(NSString*)thirdImage isLeft:(BOOL)isLeft selector1:(SEL) selector1 selector2:(SEL) selector2 selector3:(SEL) selector3 isFromGift:(BOOL)isFromGift;




-(void)showHUDOnView;
-(void)hideHUDOnView;

-(void)showNoNetworkView;
//-(void)showCustomAlertView;
-(void)setTextOnLabel:(UILabel *)textLabel fontSize:(CGFloat)fontsize;
//-(void)showUserNamePasswordAlertView;
-(void)setBackgroundImageOnNavigationBar;
-(void)setNavBarTintColor;
-(void)setUIForErrorViews:(UIView *)errorView;
-(void)setNavBarText:(NSString *)text;

-(void)setRoundedCornerForImageView:(UIImageView *)imgView;

-(UIButton *) setNaviationButtonWithText:(NSString *)text selector:(SEL) selector isLeft:(BOOL)isLeft isReduceSize:(BOOL)isReduceSize isMyFont:(CGFloat)isMyFont;
-(NSString *)giveLogoImageAccordingToOperator:(NSString *)opName;

-(UIView *)onAddDimViewOnSuperView:(UIView *)myView ;

-(void)addSpaceBetweenTextFieldOriginAndText:(UITextField *)textField;



@end
