//
//  GiftDetailCriteria.h
//  Pay1
//
//  Created by Annapurna on 15/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GiftDetailCriteria : NSObject

@property (nonatomic, strong) NSString *giftDealID;
@property (nonatomic, strong) NSString *userLat;
@property (nonatomic, strong) NSString *userLong;

@end
