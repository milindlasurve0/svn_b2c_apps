//
//  Service.m
//  Pay1
//
//  Created by Annapurna on 11/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "Service.h"

@implementation Service



//NSString *const kBaseURL = @"http://b2c.pay1.in/index.php/api_new/action/api/true/actiontype/";
NSString *const kBaseURL =  @"http://b2c.pay1.in/index.php/api_new/action/api/true/actiontype/"; //@"http://cdev.pay1.in/index.php/api_new/action/api/true/actiontype/"; //@"http://174.129.181.27/index.php/api_new/action/api/true/actiontype/";// @"http://cdev.pay1.in/index.php/api_new/action/api/true/actiontype/"; //@"http://174.129.181.27/index.php/api_new/action/api/true/actiontype/";

NSString *const kCreateUserURL = @"create_user/";
NSString *const kSignInURL = @"signin/api/true/";
NSString *const kUpdatePasswordURL = @"update_password/";
NSString *const kGetOperatorURL = @"get_all_operators/";
NSString *const kRechargeURL = @"recharge/";
NSString *const kRefillWalletURL = @"online_walletrefill/";
NSString *const kRedeemCouponURL = @"refillwallet/";
NSString *const kGiftServiceURL = @"GetAllofferTypes/"; //@"GetAllGifts_WEB/";
NSString *const kGiftDetailURL = @"get_deal_details/"; //@"GetGiftDetails_WEB/";
NSString *const kFAQRequestURL = @"get_faq/";
NSString *const kWalletRequestURL = @"wallet_history/";
NSString *const kSignOutURL = @"signout/";
NSString *const kGiftCategoryURL = @"GetAllOfferCategory/";
NSString *const kGetMobileDataURL = @"json";
NSString *const kGetUpdatedDealResponseURL = @"GetupdatedDeal/";
NSString *const kGetMyDealURL = @"get_my_deal/";
NSString *const kMyLikeURL = @"MyLikes/";
NSString *const kPurchaseDealURL = @"purchase_deal/";
NSString *const kRedeemURL = @"Redeem_Freebie/"; //@"redeem_voucher/";
NSString *const kCoinHistoryURL = @"LoyaltyPointsHistory/";
NSString *const kProfileUpdateURL = @"update_profile/";
NSString *const kTransactionHistoryURL = @"purchase_history/";
NSString *const kUserReviewURL = @"SetOfferReview/";
NSString *const kForgotPasswordURL = @"forgotpwd/";
NSString *const kQuickPayURL = @"get_quickpaylist/";
NSString *const kEditPayURL = @"edit_quickpay/";
NSString *const kStoreLocationURL = @"json/";
NSString *const kPostPaidRechargeURL = @"billpayment/";


NSString *const kMobilePlanBaseURL = @"http://panel.pay1.in/apis/receiveWeb/mindsarray/mindsarray/";


-(BOOL)handleNullResponse:(RequestData *)request{
    if(request.responseData == [NSNull null] || request.responseData == NULL || request.responseData == nil || request.responseData == Nil){
    return TRUE;
    }
    else
    return FALSE;
}

-(void) printResponse:(id)response forRequest:(NSString *) request {
    NSLog(@"-------%@:%@",request, response);
}
-(void) handleNetworkError:(NSError *)error {
    NSLog(@"#NoNetworkFound");
}

-(void) fillDefaultData:(NSMutableDictionary *) dic {
    
    [dic setObject:@"3" forKey:@"api_version"];
}



@end
