package com.pay1;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.larvalabs.svgandroid.SVGParser;
import com.pay1.constants.Constants;
import com.pay1.customviews.DatePickerDailog;
import com.pay1.customviews.MyProgressDialog;
import com.pay1.requesthandler.RequestClass;
import com.pay1.utilities.EditTextValidator;
import com.pay1.utilities.RoundedImageView;
import com.pay1.utilities.Utility;

public class ProfileActivity extends Activity {

	private static final String SCREEN_LABEL = "Profile Screen";
	private EasyTracker easyTracker = null;
	private final String TAG = "My Profile";
	private TextView textView_TitleName, textView_TitleDOB,
			textView_TitleGender, textView_TitleEmail, textView_TitleNumber,
			textView_TitlePin;
	private TextView textView_Name, textView_DOB, textView_Gender,
			textView_Email, textView_Mobile, textView_Pin;
	private EditText editText_Name, editText_Email, editText_Mobile;
	private LinearLayout linearLayout_EditName, linearLayout_EditDOB,
			linearLayout_EditGender, linearLayout_EditEmail,
			linearLayout_EditMobile, linearLayout_EditPin;
	private ImageView imageView_EditName, imageView_EditDOB,
			imageView_EditGender, imageView_EditEmail, imageView_EditMobile,
			imageView_EditPin;
	RoundedImageView imageView_profile;
	private RelativeLayout relativeLayout_Name, relativeLayout_Gender,
			relativeLayout_Email;
	private ImageView imageView_NameOk, imageView_NameCancel, imageView_DOBOk,
			imageView_DOBCancel, imageView_GenderOk, imageView_GenderCancel,
			imageView_EmailOk, imageView_EmailCancel;
	private Button button_Male, button_Female;
	Button buttonSaveAndEdit;
	private LinearLayout dobAction;
	private ImageView imageView_Back;
	String imgeInBase64, imageFBUrl = "";
	private TextView textView_Title;

	private RadioGroup radioGroup_Gender;
	private RadioButton radio_Male, radio_Female;
	private RadioButton radioSexButton;
	String dataSource = "0";
	int selected_id = -1;
	private int mYear;
	private int mMonth;
	private int mDay;
	StringBuilder builder;
	static final int DATE_DIALOG_ID = 0;
	static final int UPDATE_EMAIL = 1;
	static final int CHANGE_PIN = 2;
	String months[] = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug",
			"Sep", "Oct", "Nov", "Dec" };
	String fbData;
	private Calendar dateandtime;
	String maleFemale ="m";
	@SuppressLint("NewApi")
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.profile_activity);
		try {
			easyTracker = EasyTracker.getInstance(ProfileActivity.this);
			easyTracker.set(Fields.SCREEN_NAME, SCREEN_LABEL);

			// timestamp = System.currentTimeMillis();

			// faqDataSource = new FAQDataSource(ProfileActivity.this);
			radioGroup_Gender = (RadioGroup) findViewById(R.id.radioGroup_Gender);
			radio_Male = (RadioButton) findViewById(R.id.radio_Male);

			radio_Female = (RadioButton) findViewById(R.id.radio_Female);

		RelativeLayout back_layout = (RelativeLayout) findViewById(R.id.back_layout);
			back_layout.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onBackPressed();
				}
			});

			imageView_profile = (RoundedImageView) findViewById(R.id.imageView_profile);
			imageView_profile.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (buttonSaveAndEdit.getText().toString()
							.equalsIgnoreCase("SAVE")) {
						Intent galleryIntent = new Intent(
								Intent.ACTION_PICK,
								android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
						// Start the Intent
						startActivityForResult(galleryIntent, 100);
					}

					/*
					 * Intent intent = new Intent(); intent.setType("image/*");
					 * intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
					 * intent.setAction(Intent.ACTION_GET_CONTENT);
					 * startActivityForResult
					 * (Intent.createChooser(intent,"Select Picture"), 1);
					 */
				}
			});

			radioGroup_Gender
					.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

						@Override
						public void onCheckedChanged(RadioGroup group,
								int checkedId) {
							// TODO Auto-generated method stub
							/*int selectedId = radioGroup_Gender
									.getCheckedRadioButtonId();

							// find the radiobutton by returned id
							radio_Male = (RadioButton) findViewById(selectedId);
							radio_Female = (RadioButton) findViewById(selectedId);

							if (selectedId == radio_Male.getId()) {

								textView_Gender.setText("Male");

							} else if (selectedId == radio_Female.getId()) {
								textView_Gender.setText("Female");

							}
							*/
							  // get selected radio button from radioGroup
							int selectedId = radioGroup_Gender.getCheckedRadioButtonId();
				 
							// find the radiobutton by returned id
						        radioSexButton = (RadioButton) findViewById(selectedId);
				 
							/*Toast.makeText(ProfileActivity.this,
								radioSexButton.getText(), Toast.LENGTH_SHORT).show();*/
							textView_Gender.setText(radioSexButton.getText());
							//updateGender();
							maleFemale=radioSexButton.getText().toString();

						}
					});

			if (Utility.getFbImage64(ProfileActivity.this).isEmpty()) {

			} else {
				imageView_profile.setImageBitmap(Constants.decodeBase64(Utility
						.getFbImage64(ProfileActivity.this)));
			}

			imageView_Back = (ImageView) findViewById(R.id.imageView_Back);
			imageView_Back.setImageDrawable(SVGParser.getSVGFromResource(
					getResources(), R.raw.ic_back_old).createPictureDrawable());
			if (Build.VERSION.SDK_INT >= 11)
				imageView_Back.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
			imageView_Back.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onBackPressed();
				}
			});

			Typeface Reguler = Typeface.createFromAsset(getAssets(),
					"Roboto-Regular.ttf");
			Typeface Medium = Typeface.createFromAsset(
					ProfileActivity.this.getAssets(), "Roboto-Regular.ttf");
			radio_Female.setTypeface(Reguler);
			radio_Male.setTypeface(Reguler);
			textView_Title = (TextView) findViewById(R.id.textView_Title);
			textView_Title.setText(TAG);
			textView_Title.setTypeface(Reguler);

			dateandtime = Calendar.getInstance();

			Date dt = null;
			try {
				String d = Utility
						.getDOB(ProfileActivity.this,
								Constants.SHAREDPREFERENCE_DOB).toString()
						.trim().replaceAll("-", "/");

				dt = new Date(d);
			} catch (Exception e) {
				dt = new Date();
			}
			final Calendar c = Calendar.getInstance();
			c.setTime(dt);

			dateandtime.setTime(dt);
			mYear = c.get(Calendar.YEAR);
			mMonth = c.get(Calendar.MONTH);
			mDay = c.get(Calendar.DAY_OF_MONTH);

			textView_TitleName = (TextView) findViewById(R.id.textView_TitleName);
			textView_TitleName.setTypeface(Reguler);
			textView_TitleDOB = (TextView) findViewById(R.id.textView_TitleDOB);
			textView_TitleDOB.setTypeface(Reguler);
			textView_TitleGender = (TextView) findViewById(R.id.textView_TitleGender);
			textView_TitleGender.setTypeface(Reguler);
			textView_TitleEmail = (TextView) findViewById(R.id.textView_TitleEmail);
			textView_TitleEmail.setTypeface(Reguler);
			textView_TitleNumber = (TextView) findViewById(R.id.textView_TitleNumber);
			textView_TitleNumber.setTypeface(Reguler);
			textView_TitlePin = (TextView) findViewById(R.id.textView_TitlePin);
			textView_TitlePin.setTypeface(Reguler);

			textView_Name = (TextView) findViewById(R.id.textView_Name);
			textView_Name.setTypeface(Medium);
			textView_DOB = (TextView) findViewById(R.id.textView_DOB);
			textView_DOB.setTypeface(Medium);
			builder = new StringBuilder(
					new SimpleDateFormat("yyyy-MM-dd").format(c.getTime()));
			textView_DOB.setText(new SimpleDateFormat("dd MMM yyyy")
					.format(dateandtime.getTime()));
			textView_Gender = (TextView) findViewById(R.id.textView_Gender);
			textView_Gender.setTypeface(Medium);
			textView_Email = (TextView) findViewById(R.id.textView_Email);
			textView_Email.setTypeface(Medium);
			textView_Email.setText(Utility.getEmail(ProfileActivity.this,
					Constants.SHAREDPREFERENCE_EMAIL));
			textView_Mobile = (TextView) findViewById(R.id.textView_Mobile);
			textView_Mobile.setTypeface(Medium);
			textView_Mobile.setText(Utility.getMobileNumber(
					ProfileActivity.this, Constants.SHAREDPREFERENCE_MOBILE));
			textView_Pin = (TextView) findViewById(R.id.textView_Pin);
			textView_Pin.setTypeface(Medium);
			textView_Pin.setText(Utility.getPin(ProfileActivity.this,
					Constants.SHAREDPREFERENCE_PIN));

			Drawable drawable_ok = SVGParser.getSVGFromResource(getResources(),
					R.raw.successful).createPictureDrawable();
			Drawable drawable_cancel = SVGParser.getSVGFromResource(
					getResources(), R.raw.unsuccessful).createPictureDrawable();

			relativeLayout_Name = (RelativeLayout) findViewById(R.id.relativeLayout_Name);
			relativeLayout_Name.setVisibility(View.GONE);
			imageView_NameOk = (ImageView) findViewById(R.id.imageView_NameOk);
			imageView_NameOk.setImageDrawable(drawable_ok);
			imageView_NameCancel = (ImageView) findViewById(R.id.imageView_NameCancel);
			imageView_NameCancel.setImageDrawable(drawable_cancel);

			dobAction = (LinearLayout) findViewById(R.id.dobAction);
			dobAction.setVisibility(View.GONE);
			imageView_DOBOk = (ImageView) findViewById(R.id.imageView_DOBOk);
			imageView_DOBOk.setImageDrawable(drawable_ok);
			imageView_DOBCancel = (ImageView) findViewById(R.id.imageView_DOBCancel);
			imageView_DOBCancel.setImageDrawable(drawable_cancel);

			relativeLayout_Gender = (RelativeLayout) findViewById(R.id.relativeLayout_Gender);
			relativeLayout_Gender.setVisibility(View.GONE);
			imageView_GenderOk = (ImageView) findViewById(R.id.imageView_GenderOk);
			imageView_GenderOk.setImageDrawable(drawable_ok);
			imageView_GenderCancel = (ImageView) findViewById(R.id.imageView_GenderCancel);
			imageView_GenderCancel.setImageDrawable(drawable_cancel);

			button_Male = (Button) findViewById(R.id.button_Male);
			button_Male.setTypeface(Medium);
			button_Female = (Button) findViewById(R.id.button_Female);
			button_Female.setTypeface(Medium);
			buttonSaveAndEdit = (Button) findViewById(R.id.buttonSaveAndEdit);
			buttonSaveAndEdit.setTypeface(Medium);
			relativeLayout_Email = (RelativeLayout) findViewById(R.id.relativeLayout_Email);
			relativeLayout_Email.setVisibility(View.GONE);
			imageView_EmailOk = (ImageView) findViewById(R.id.imageView_EmailOk);
			imageView_EmailOk.setImageDrawable(drawable_ok);
			imageView_EmailCancel = (ImageView) findViewById(R.id.imageView_EmailCancel);
			imageView_EmailCancel.setImageDrawable(drawable_cancel);

			editText_Name = (EditText) findViewById(R.id.editText_Name);
			editText_Name.setTypeface(Medium);
			editText_Name.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					// TODO Auto-generated method stub
					if (editText_Name.getText().toString().trim().length() > 0)
						EditTextValidator
								.hasFragmentText(ProfileActivity.this,
										editText_Name,
										Constants.ERROR_NAME_BLANK_FIELD);
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					// TODO Auto-generated method stub

				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

				}
			});
			editText_Email = (EditText) findViewById(R.id.editText_Email);
			editText_Email.setTypeface(Medium);
			editText_Mobile = (EditText) findViewById(R.id.editText_Mobile);
			editText_Mobile.setTypeface(Medium);
			editText_Mobile.setVisibility(View.GONE);

			linearLayout_EditName = (LinearLayout) findViewById(R.id.linearLayout_EditName);
			linearLayout_EditDOB = (LinearLayout) findViewById(R.id.linearLayout_EditDOB);
			linearLayout_EditGender = (LinearLayout) findViewById(R.id.linearLayout_EditGender);
			linearLayout_EditEmail = (LinearLayout) findViewById(R.id.linearLayout_EditEmail);
			linearLayout_EditMobile = (LinearLayout) findViewById(R.id.linearLayout_EditMobile);
			linearLayout_EditPin = (LinearLayout) findViewById(R.id.linearLayout_EditPin);

			/*
			 * Drawable drawable_edit = SVGParser.getSVGFromResource(
			 * getResources(), R.raw.ic_edit).createPictureDrawable();
			 */
			/*
			 * Drawable drawable_edit = getResources().getDrawable(
			 * R.drawable.ic_edit);
			 */

			Drawable drawable_edit = SVGParser.getSVGFromResource(
					getResources(), R.raw.ic_edit).createPictureDrawable();
			imageView_EditName = (ImageView) findViewById(R.id.imageView_EditName);
			imageView_EditName.setImageDrawable(drawable_edit);
			imageView_EditDOB = (ImageView) findViewById(R.id.imageView_EditDOB);
			Drawable drawable_dob = SVGParser.getSVGFromResource(
					getResources(), R.raw.ic_calender).createPictureDrawable();
			imageView_EditDOB.setImageDrawable(drawable_dob);
			imageView_EditGender = (ImageView) findViewById(R.id.imageView_EditGender);
			imageView_EditGender.setImageDrawable(drawable_edit);
			imageView_EditEmail = (ImageView) findViewById(R.id.imageView_EditEmail);
			imageView_EditEmail.setImageDrawable(drawable_edit);
			imageView_EditMobile = (ImageView) findViewById(R.id.imageView_EditMobile);
			imageView_EditMobile.setImageDrawable(drawable_edit);
			linearLayout_EditMobile.setVisibility(View.GONE);
			imageView_EditPin = (ImageView) findViewById(R.id.imageView_EditPin);
			imageView_EditPin.setImageDrawable(drawable_edit);
			if (Build.VERSION.SDK_INT >= 11) {
				imageView_EditName.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
				imageView_EditDOB.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
				imageView_EditGender.setLayerType(View.LAYER_TYPE_SOFTWARE,
						null);
				imageView_EditEmail
						.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
				imageView_EditMobile.setLayerType(View.LAYER_TYPE_SOFTWARE,
						null);
				imageView_EditPin.setLayerType(View.LAYER_TYPE_SOFTWARE, null);

				imageView_NameOk.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
				imageView_NameCancel.setLayerType(View.LAYER_TYPE_SOFTWARE,
						null);
				imageView_DOBOk.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
				imageView_DOBCancel
						.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
				imageView_GenderOk.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
				imageView_GenderCancel.setLayerType(View.LAYER_TYPE_SOFTWARE,
						null);
				imageView_EmailOk.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
				imageView_EmailCancel.setLayerType(View.LAYER_TYPE_SOFTWARE,
						null);
			}

			linearLayout_EditPin.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					/*relativeLayout_Name.setVisibility(View.GONE);
					textView_Name.setVisibility(View.VISIBLE);
					linearLayout_EditName.setVisibility(View.GONE);

					dobAction.setVisibility(View.GONE);
					linearLayout_EditDOB.setVisibility(View.GONE);

					relativeLayout_Gender.setVisibility(View.GONE);
					textView_Gender.setVisibility(View.VISIBLE);
					linearLayout_EditGender.setVisibility(View.VISIBLE);

					relativeLayout_Email.setVisibility(View.GONE);
					textView_Email.setVisibility(View.VISIBLE);
					linearLayout_EditEmail.setVisibility(View.VISIBLE);*/
					
					/*relativeLayout_Name.setVisibility(View.GONE);
					textView_Name.setVisibility(View.VISIBLE);
					linearLayout_EditName.setVisibility(View.GONE);

					dobAction.setVisibility(View.GONE);
					linearLayout_EditDOB.setVisibility(View.VISIBLE);

					relativeLayout_Gender.setVisibility(View.GONE);
					textView_Gender.setVisibility(View.VISIBLE);
					linearLayout_EditGender.setVisibility(View.GONE);

					relativeLayout_Email.setVisibility(View.GONE);
					textView_Email.setVisibility(View.VISIBLE);
					linearLayout_EditEmail.setVisibility(View.GONE);
					imageView_EditPin.setVisibility(View.GONE);*/
					

					new ForgotPasswordTask().execute(Utility.getMobileNumber(
							ProfileActivity.this,
							Constants.SHAREDPREFERENCE_MOBILE));
				}
			});

			updateName();
			updateDOB();
			updateGender();
			// updateEmail();

			linearLayout_EditEmail
					.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub

							relativeLayout_Name.setVisibility(View.GONE);
							textView_Name.setVisibility(View.VISIBLE);
							linearLayout_EditName.setVisibility(View.GONE);

							dobAction.setVisibility(View.GONE);
							linearLayout_EditDOB.setVisibility(View.GONE);

							relativeLayout_Gender.setVisibility(View.GONE);
							textView_Gender.setVisibility(View.VISIBLE);
							linearLayout_EditGender.setVisibility(View.VISIBLE);

							relativeLayout_Email.setVisibility(View.GONE);
							textView_Email.setVisibility(View.VISIBLE);
							linearLayout_EditEmail.setVisibility(View.GONE);

							Intent intent = new Intent(ProfileActivity.this,
									ChangeEmailActivity.class);
							intent.putExtra("EMAIL", textView_Email.getText()
									.toString().trim());
							startActivityForResult(intent, UPDATE_EMAIL);
						}
					});

			Bundle bundle = getIntent().getExtras();
			if (bundle != null) {
				fbData = bundle.getString("fbData");
				// Response response=(Response)
				// getIntent().getSerializableExtra("MyClass");
				dataSource = "1";
				updateLayoutForFb(fbData);

			}

			buttonSaveAndEdit.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					if (buttonSaveAndEdit.getText().toString()
							.equalsIgnoreCase("SAVE")) {
						
						
						if(maleFemale.startsWith("m")||maleFemale.startsWith("M")){
							maleFemale="m";
						}else{
							maleFemale="f";
						}

						new UpdateFullProfileTask().execute(editText_Name
								.getText().toString(), builder.toString(),
								maleFemale,
								editText_Email.getText().toString(), "",
								imageFBUrl);
					} else {
						updateLayoutNormal();
					}
				}
			});

			if (buttonSaveAndEdit.getText().toString().equalsIgnoreCase("SAVE")) {
				linearLayout_EditDOB.setVisibility(View.VISIBLE);
				linearLayout_EditPin.setVisibility(View.VISIBLE);
			} else {
				linearLayout_EditDOB.setVisibility(View.GONE);
				linearLayout_EditPin.setVisibility(View.GONE);
			}

		} catch (Exception e) {
		}
		overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}

	protected void updateLayoutNormal() {
		// TODO Auto-generated method stub
		
		
		buttonSaveAndEdit.setText("SAVE");
		editText_Name.setText(textView_Name.getText().toString().trim());
		relativeLayout_Name.setVisibility(View.VISIBLE);
		textView_Name.setVisibility(View.GONE);
		linearLayout_EditName.setVisibility(View.GONE);

		dobAction.setVisibility(View.GONE);
		linearLayout_EditDOB.setVisibility(View.VISIBLE);

		relativeLayout_Gender.setVisibility(View.VISIBLE);
		textView_Gender.setVisibility(View.GONE);
		linearLayout_EditGender.setVisibility(View.GONE);

		relativeLayout_Email.setVisibility(View.VISIBLE);
		textView_Email.setVisibility(View.GONE);
		editText_Email.setText(textView_Email.getText().toString().trim());
		linearLayout_EditEmail.setVisibility(View.GONE);
		imageView_EditPin.setVisibility(View.VISIBLE);
		String s  = Utility.getGender(ProfileActivity.this,
					Constants.SHAREDPREFERENCE_GENDER);
		
		if (buttonSaveAndEdit.getText().toString().equalsIgnoreCase("SAVE")) {
			linearLayout_EditDOB.setVisibility(View.VISIBLE);
			linearLayout_EditPin.setVisibility(View.VISIBLE);
		} else {
			linearLayout_EditDOB.setVisibility(View.GONE);
			linearLayout_EditPin.setVisibility(View.GONE);
		}
		
		if (s != null && s.startsWith("m")) {
			/*
			 * selected_id = 0;
			 * button_Male.setBackgroundResource(R.drawable.button_login_selected
			 * );
			 * button_Male.setTextColor(getResources().getColor(R.color.White));
			 * button_Female
			 * .setBackgroundResource(R.drawable.button_signup_selected);
			 * button_Female.setTextColor(getResources().getColor(
			 * R.color.app_blue_color));
			 */
			radio_Male.setChecked(true);
		} else {
			/*
			 * selected_id = 1; button_Female
			 * .setBackgroundResource(R.drawable.button_login_selected);
			 * button_Female
			 * .setTextColor(getResources().getColor(R.color.White));
			 * button_Male
			 * .setBackgroundResource(R.drawable.button_signup_selected);
			 * button_Male.setTextColor(getResources().getColor(
			 * R.color.app_blue_color));
			 */
			radio_Female.setChecked(true);
		}
	}

	private void updateLayoutForFb(String fbData) {
		// TODO Auto-generated method stub
		try {
			if (!fbData.isEmpty() || fbData.length() != 0) {
				buttonSaveAndEdit.setText("SAVE");
				JSONObject jsonObject = new JSONObject(fbData);
				new LoadFBImageTask().execute("https://graph.facebook.com/"
						+ jsonObject.getString("id") + "/picture?type=large");
				// Utility.setFbImageId(ProfileActivity.this,"https://graph.facebook.com/"
				// + jsonObject.getString("id")+ "/picture?type=large");
				imageFBUrl = "https://graph.facebook.com/"
						+ jsonObject.getString("id") + "/picture?type=large";
				editText_Name.setText(jsonObject.getString("name"));
				relativeLayout_Name.setVisibility(View.VISIBLE);
				textView_Name.setVisibility(View.GONE);
				linearLayout_EditName.setVisibility(View.GONE);

				dobAction.setVisibility(View.GONE);
				linearLayout_EditDOB.setVisibility(View.VISIBLE);

				relativeLayout_Gender.setVisibility(View.VISIBLE);
				textView_Gender.setVisibility(View.GONE);
				linearLayout_EditGender.setVisibility(View.GONE);
				imageView_EditPin.setVisibility(View.VISIBLE);
				relativeLayout_Email.setVisibility(View.VISIBLE);
				textView_Email.setVisibility(View.GONE);
				editText_Email.setText(jsonObject.getString("email")/*
																	 * textView_Email
																	 * .
																	 * getText()
																	 * .
																	 * toString(
																	 * ).trim()
																	 */);
				
				
				String s=jsonObject.getString("gender");
				if (s != null && s.startsWith("m")) {
					/*
					 * selected_id = 0;
					 * button_Male.setBackgroundResource(R.drawable.button_login_selected
					 * );
					 * button_Male.setTextColor(getResources().getColor(R.color.White));
					 * button_Female
					 * .setBackgroundResource(R.drawable.button_signup_selected);
					 * button_Female.setTextColor(getResources().getColor(
					 * R.color.app_blue_color));
					 */
					maleFemale="male";
					radio_Male.setChecked(true);
				} else {
					/*
					 * selected_id = 1; button_Female
					 * .setBackgroundResource(R.drawable.button_login_selected);
					 * button_Female
					 * .setTextColor(getResources().getColor(R.color.White));
					 * button_Male
					 * .setBackgroundResource(R.drawable.button_signup_selected);
					 * button_Male.setTextColor(getResources().getColor(
					 * R.color.app_blue_color));
					 */
					maleFemale="female";
					radio_Female.setChecked(true);
				}
				
				/*if(maleFemale.startsWith("m")||maleFemale.startsWith("M")){
					maleFemale="m";
				}else{
					maleFemale="f";
				}*/
				if (buttonSaveAndEdit.getText().toString().equalsIgnoreCase("SAVE")) {
					linearLayout_EditDOB.setVisibility(View.VISIBLE);
					linearLayout_EditPin.setVisibility(View.VISIBLE);
				} else {
					linearLayout_EditDOB.setVisibility(View.GONE);
					linearLayout_EditPin.setVisibility(View.GONE);
				}
				linearLayout_EditEmail.setVisibility(View.GONE);
			}
		} catch (JSONException je) {

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView().build());
		// EasyTracker.getInstance(this).activityStart(this);
		easyTracker.send(MapBuilder.createAppView().build());
	}

	@Override
	public void onStop() {
		super.onStop();
		// EasyTracker.getInstance(this).activityStop(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}
	private void hideKeyboard() {
	    InputMethodManager imm = (InputMethodManager) 
	        getSystemService(Context.INPUT_METHOD_SERVICE);
	    imm.hideSoftInputFromWindow(editText_Email.getWindowToken(), 0);
	    imm.hideSoftInputFromWindow(editText_Mobile.getWindowToken(), 0);
	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		
		hideKeyboard();
		if (buttonSaveAndEdit.getText().toString().equalsIgnoreCase("EDIT")) {
			overridePendingTransition(R.anim.slide_in_left,
					R.anim.slide_out_right);
		} else {
			relativeLayout_Name.setVisibility(View.GONE);
			textView_Name.setVisibility(View.VISIBLE);
			linearLayout_EditName.setVisibility(View.GONE);

			dobAction.setVisibility(View.GONE);
			linearLayout_EditDOB.setVisibility(View.VISIBLE);

			relativeLayout_Gender.setVisibility(View.GONE);
			textView_Gender.setVisibility(View.VISIBLE);
			linearLayout_EditGender.setVisibility(View.GONE);

			relativeLayout_Email.setVisibility(View.GONE);
			textView_Email.setVisibility(View.VISIBLE);
			linearLayout_EditEmail.setVisibility(View.GONE);
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		try {
			if (resultCode == Activity.RESULT_OK) {

				try {
					// When an Image is picked
					if (requestCode == 100 && resultCode == RESULT_OK
							&& null != data) {
						// Get the Image from data

						Uri selectedImage = data.getData();
						String[] filePathColumn = { MediaStore.Images.Media.DATA };

						// Get the cursor
						Cursor cursor = getContentResolver()
								.query(selectedImage, filePathColumn, null,
										null, null);
						// Move to first row
						cursor.moveToFirst();

						int columnIndex = cursor
								.getColumnIndex(filePathColumn[0]);
						String imgDecodableString = cursor
								.getString(columnIndex);
						cursor.close();

						imageView_profile.setImageBitmap(BitmapFactory
								.decodeFile(imgDecodableString));
						dataSource = "0";
						// Utility.setFbImageId(ProfileActivity.this,"");
						imgeInBase64 = Constants.encodeTobase64(BitmapFactory
								.decodeFile(imgDecodableString));
						/*
						 * Utility.setFbImage64(ProfileActivity.this,
						 * imgeInBase64);
						 */

						/*
						 * new LoadImageTask().execute(Constants
						 * .encodeTobase64(BitmapFactory
						 * .decodeFile(imgDecodableString)));
						 */
						/*
						 * String
						 * imgeInBase64=Constants.encodeTobase64(BitmapFactory
						 * .decodeFile(imgDecodableString));
						 * Utility.setFbImage64(ProfileActivity.this,
						 * imgeInBase64);
						 * imageView_profile.setImageBitmap(Constants
						 * .decodeBase64
						 * (Utility.getFbImage64(ProfileActivity.this)));
						 */
					} else {
						Toast.makeText(this, "You haven't picked Image",
								Toast.LENGTH_LONG).show();
					}
				} catch (Exception e) {
					Toast.makeText(this, "Something went wrong",
							Toast.LENGTH_LONG).show();
				}

				if (requestCode == UPDATE_EMAIL) {
					textView_Email.setText(data.getExtras().getString("EMAIL"));
				}
				if (requestCode == CHANGE_PIN) {
					textView_Pin.setText(data.getExtras().getString("PIN"));
				}
			}

		} catch (Exception e) {
		}
	}

	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DATE_DIALOG_ID:
			DatePickerDialog dialog = new DatePickerDialog(
					ProfileActivity.this, mDateSetListener, mYear, mMonth, mDay);

			return dialog;

		}
		return null;
	}

	private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {

		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			mYear = year;
			mMonth = monthOfYear;
			mDay = dayOfMonth;
			updateDisplay();
		}
	};

	private void updateDisplay() {

		String d = mDay < 10 ? "0" + mDay : "" + mDay;
		String m = (mMonth + 1) < 10 ? "0" + (mMonth + 1) : "" + (mMonth + 1);
		builder = new StringBuilder().append(mYear).append("-").append(m)
				.append("-").append(d);
		textView_DOB.setText(d + "-" + months[mMonth] + "-" + mYear);

	}

	private void updateName() {
		textView_Name.setText(Utility.getUserName(ProfileActivity.this,
				Constants.SHAREDPREFERENCE_NAME));
		imageView_NameOk.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (EditTextValidator.hasFragmentText(ProfileActivity.this,
						editText_Name, Constants.ERROR_NAME_BLANK_FIELD)) {

					InputMethodManager imm = (InputMethodManager) ProfileActivity.this
							.getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

					textView_Name.setText(editText_Name.getText().toString()
							.trim());
					String str = textView_Gender.getText().toString().trim()
							.equalsIgnoreCase("Male") ? "m" : "f";
					new UpdateProfileTask().execute(editText_Name.getText()
							.toString().trim(), builder.toString().trim(), str);
				} else {
					editText_Name.requestFocus();
				}
			}
		});
		imageView_NameCancel.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				InputMethodManager imm = (InputMethodManager) ProfileActivity.this
						.getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

				relativeLayout_Name.setVisibility(View.GONE);
				textView_Name.setVisibility(View.VISIBLE);
				linearLayout_EditName.setVisibility(View.GONE);
			}
		});
		linearLayout_EditName.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				editText_Name
						.setText(textView_Name.getText().toString().trim());
				relativeLayout_Name.setVisibility(View.VISIBLE);
				textView_Name.setVisibility(View.GONE);
				linearLayout_EditName.setVisibility(View.GONE);

				dobAction.setVisibility(View.GONE);
				linearLayout_EditDOB.setVisibility(View.GONE);

				relativeLayout_Gender.setVisibility(View.GONE);
				textView_Gender.setVisibility(View.VISIBLE);
				linearLayout_EditGender.setVisibility(View.VISIBLE);

				relativeLayout_Email.setVisibility(View.GONE);
				textView_Email.setVisibility(View.VISIBLE);
				linearLayout_EditEmail.setVisibility(View.GONE);
			}
		});
	}

	private void updateDOB() {
		// updateDisplay();
		imageView_DOBOk.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				String str = textView_Gender.getText().toString().trim()
						.equalsIgnoreCase("Male") ? "m" : "f";
				new UpdateProfileTask().execute(editText_Name.getText()
						.toString().trim(), builder.toString().trim(), str);
			}
		});
		imageView_DOBCancel.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dobAction.setVisibility(View.GONE);
				linearLayout_EditDOB.setVisibility(View.GONE);
			}
		});
		linearLayout_EditDOB.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// showDialog(DATE_DIALOG_ID);

				DatePickerDailog dp = new DatePickerDailog(
						ProfileActivity.this, dateandtime,
						new DatePickerDailog.DatePickerListner() {

							@Override
							public void OnDoneButton(Dialog datedialog,
									Calendar c) {
								datedialog.dismiss();
								dateandtime.set(Calendar.YEAR,
										c.get(Calendar.YEAR));
								dateandtime.set(Calendar.MONTH,
										c.get(Calendar.MONTH));
								dateandtime.set(Calendar.DAY_OF_MONTH,
										c.get(Calendar.DAY_OF_MONTH));
								// ((Button) arg0)
								// .setText(new SimpleDateFormat(
								// "MMMM dd yyyy").format(c
								// .getTime()));

								// String d = mDay < 10 ? "0" + mDay : ""
								// + mDay;
								// String m = (mMonth + 1) < 10 ? "0"
								// + (mMonth + 1) : "" + (mMonth + 1);
								// builder = new
								// StringBuilder().append(mYear)
								// .append("-").append(m).append("-")
								// .append(d);
								builder = new StringBuilder(
										new SimpleDateFormat("yyyy-MM-dd")
												.format(dateandtime.getTime()));
								textView_DOB.setText(new SimpleDateFormat(
										"dd MMM yyyy").format(dateandtime
										.getTime()));
							}

							@Override
							public void OnCancelButton(Dialog datedialog) {
								// TODO Auto-generated method stub
								datedialog.dismiss();
							}
						});
				dp.show();
				/*dobAction.setVisibility(View.GONE);
				linearLayout_EditDOB.setVisibility(View.VISIBLE);

				relativeLayout_Name.setVisibility(View.GONE);
				textView_Name.setVisibility(View.VISIBLE);
				linearLayout_EditName.setVisibility(View.GONE);

				relativeLayout_Gender.setVisibility(View.GONE);
				textView_Gender.setVisibility(View.VISIBLE);
				linearLayout_EditGender.setVisibility(View.GONE);

				relativeLayout_Email.setVisibility(View.GONE);
				textView_Email.setVisibility(View.VISIBLE);
				linearLayout_EditEmail.setVisibility(View.GONE);*/

			}
		});

	}

	private void updateGender() {
		textView_Gender.setText("");
		String s = "";
		try {
			s = Utility.getGender(ProfileActivity.this,
					Constants.SHAREDPREFERENCE_GENDER);
			String p = "";
			if (s.startsWith("m"))
				p = "Male";
			else if (s.startsWith("f"))
				p = "Female";
			else
				p = "";
			textView_Gender.setText(p);
		} catch (Exception e) {
		}

		imageView_GenderOk.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {/*
										 * // TODO Auto-generated method stub if
										 * (selected_id == -1) {
										 * Constants.showOneButtonDialog
										 * (ProfileActivity.this, TAG,
										 * "Please select gender.",
										 * Constants.DIALOG_CLOSE); } else {
										 * String str = selected_id == 0 ? "m" :
										 * "f";
										 * textView_Gender.setText(selected_id
										 * == 0 ? "Male" : "Female"); new
										 * UpdateProfileTask
										 * ().execute(textView_Name.getText()
										 * .toString().trim(),
										 * builder.toString().trim(), str); }
										 */
			}
		});
		imageView_GenderCancel.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				relativeLayout_Gender.setVisibility(View.GONE);
				textView_Gender.setVisibility(View.VISIBLE);
				linearLayout_EditGender.setVisibility(View.VISIBLE);
			}
		});
		linearLayout_EditGender.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				relativeLayout_Gender.setVisibility(View.VISIBLE);
				textView_Gender.setVisibility(View.GONE);
				linearLayout_EditGender.setVisibility(View.GONE);

				relativeLayout_Name.setVisibility(View.GONE);
				textView_Name.setVisibility(View.VISIBLE);
				linearLayout_EditName.setVisibility(View.GONE);

				dobAction.setVisibility(View.GONE);
				linearLayout_EditDOB.setVisibility(View.GONE);

				relativeLayout_Email.setVisibility(View.GONE);
				textView_Email.setVisibility(View.VISIBLE);
				linearLayout_EditEmail.setVisibility(View.GONE);
			}
		});
		button_Male = (Button) findViewById(R.id.button_Male);
		button_Male.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				selected_id = 0;
				button_Male
						.setBackgroundResource(R.drawable.button_login_selected);
				button_Male
						.setTextColor(getResources().getColor(R.color.White));
				button_Female
						.setBackgroundResource(R.drawable.button_signup_selected);
				button_Female.setTextColor(getResources().getColor(
						R.color.app_blue_color));
			}
		});

		button_Female = (Button) findViewById(R.id.button_Female);
		button_Female.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {/*
										 * // TODO Auto-generated method stub
										 * selected_id = 1; button_Female
										 * .setBackgroundResource
										 * (R.drawable.button_login_selected);
										 * button_Female
										 * .setTextColor(getResources
										 * ().getColor( R.color.White));
										 * button_Male
										 * .setBackgroundResource(R.drawable
										 * .button_signup_selected);
										 * button_Male.
										 * setTextColor(getResources().getColor(
										 * R.color.app_blue_color));
										 */
			}
		});

		if (s != null && s.startsWith("m")) {
			/*
			 * selected_id = 0;
			 * button_Male.setBackgroundResource(R.drawable.button_login_selected
			 * );
			 * button_Male.setTextColor(getResources().getColor(R.color.White));
			 * button_Female
			 * .setBackgroundResource(R.drawable.button_signup_selected);
			 * button_Female.setTextColor(getResources().getColor(
			 * R.color.app_blue_color));
			 */
			radio_Male.setChecked(true);
		} else {
			/*
			 * selected_id = 1; button_Female
			 * .setBackgroundResource(R.drawable.button_login_selected);
			 * button_Female
			 * .setTextColor(getResources().getColor(R.color.White));
			 * button_Male
			 * .setBackgroundResource(R.drawable.button_signup_selected);
			 * button_Male.setTextColor(getResources().getColor(
			 * R.color.app_blue_color));
			 */
			radio_Female.setChecked(true);
		}
	}

	private void updateEmail() {
		textView_Email.setText(Utility.getEmail(ProfileActivity.this,
				Constants.SHAREDPREFERENCE_EMAIL));
		imageView_EmailOk.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (EditTextValidator.hasFragmentText(ProfileActivity.this,
						editText_Email, Constants.ERROR_EMAIL_BLANK_FIELD)
						&& EditTextValidator.isFragmentEmailAddress(
								ProfileActivity.this, editText_Email,
								Constants.ERROR_EMAIL_VALID_FIELD)) {

					InputMethodManager imm = (InputMethodManager) ProfileActivity.this
							.getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

					textView_Email.setText(editText_Email.getText().toString()
							.trim());
					String str = textView_Gender.getText().toString().trim()
							.equalsIgnoreCase("Male") ? "m" : "f";
					new UpdateProfileTask().execute(textView_Name.getText()
							.toString().trim(), builder.toString().trim(), str,
							editText_Email.getText().toString().trim());
				} else {
					editText_Email.requestFocus();
				}
			}
		});
		imageView_EmailCancel.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				InputMethodManager imm = (InputMethodManager) ProfileActivity.this
						.getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

				relativeLayout_Email.setVisibility(View.GONE);
				textView_Email.setVisibility(View.VISIBLE);
				linearLayout_EditEmail.setVisibility(View.GONE);
			}
		});
		linearLayout_EditEmail.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				editText_Email.setText(textView_Email.getText().toString()
						.trim());
				relativeLayout_Email.setVisibility(View.VISIBLE);
				textView_Email.setVisibility(View.GONE);
				linearLayout_EditEmail.setVisibility(View.GONE);

				relativeLayout_Name.setVisibility(View.GONE);
				textView_Name.setVisibility(View.VISIBLE);
				linearLayout_EditName.setVisibility(View.GONE);

				dobAction.setVisibility(View.GONE);
				linearLayout_EditDOB.setVisibility(View.GONE);

				relativeLayout_Gender.setVisibility(View.GONE);
				textView_Gender.setVisibility(View.VISIBLE);
				linearLayout_EditGender.setVisibility(View.VISIBLE);
			}
		});
	}

	public class UpdateFullProfileTask extends
			AsyncTask<String, String, String> implements OnDismissListener {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {

			try {
				ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();
				listValuePair.add(new BasicNameValuePair("name", params[0]));
				listValuePair.add(new BasicNameValuePair("date_of_birth",
						params[1]));
				listValuePair.add(new BasicNameValuePair("gender", params[2]));
				listValuePair.add(new BasicNameValuePair("email", params[3]));

				if (dataSource.equalsIgnoreCase("0")) {
					listValuePair.add(new BasicNameValuePair("code",
							imgeInBase64/* params[4] */));
					listValuePair.add(new BasicNameValuePair("f_url", ""));
					listValuePair.add(new BasicNameValuePair("fb_data", "2"));
				} else if (dataSource.equalsIgnoreCase("1")) {
					listValuePair.add(new BasicNameValuePair("code", ""));
					listValuePair
							.add(new BasicNameValuePair("f_url", params[5]));
					listValuePair.add(new BasicNameValuePair("fb_data",
							dataSource));
				}
				listValuePair.add(new BasicNameValuePair("api_version",
						Constants.API_VERSION));
				String response = RequestClass.getInstance()
						.readPay1B2CRequest(ProfileActivity.this,
								Constants.B2C_URL + "update_profile",
								listValuePair);
				listValuePair.clear();
				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			try {

				if (!result.startsWith("Error")) {

					JSONObject jsonObject = new JSONObject(result);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {

						// JSONObject jsonObject2 = jsonObject
						// .getJSONObject("description");
						relativeLayout_Name.setVisibility(View.GONE);
						textView_Name.setVisibility(View.VISIBLE);
						linearLayout_EditName.setVisibility(View.GONE);

						dobAction.setVisibility(View.GONE);
						//linearLayout_EditDOB.setVisibility(View.VISIBLE);
						linearLayout_EditDOB.setVisibility(View.GONE);
						relativeLayout_Gender.setVisibility(View.GONE);
						textView_Gender.setVisibility(View.VISIBLE);
						linearLayout_EditGender.setVisibility(View.GONE);

						relativeLayout_Email.setVisibility(View.GONE);
						textView_Email.setVisibility(View.VISIBLE);
						linearLayout_EditEmail.setVisibility(View.GONE);
						imageView_EditPin.setVisibility(View.GONE);
						JSONObject profileData = jsonObject
								.getJSONObject("profileData");
						Utility.setUserName(ProfileActivity.this,
								Constants.SHAREDPREFERENCE_NAME,
								profileData.getString("name"));
						Utility.setMobileNumber(ProfileActivity.this,
								Constants.SHAREDPREFERENCE_MOBILE,
								profileData.getString("mobile"));
						Utility.setEmail(ProfileActivity.this,
								Constants.SHAREDPREFERENCE_EMAIL,
								profileData.getString("email"));
					/*	Utility.setGender(ProfileActivity.this,
								Constants.SHAREDPREFERENCE_GENDER,
								profileData.getString("gender").trim()
										.startsWith("M") ? "m" : "f");*/
						
						Utility.setGender(ProfileActivity.this,
								Constants.SHAREDPREFERENCE_GENDER, profileData
										.getString("gender").trim());
						Utility.setPin(ProfileActivity.this,
								Constants.SHAREDPREFERENCE_PIN, textView_Pin
										.getText().toString().trim());
						Utility.setDOB(ProfileActivity.this,
								Constants.SHAREDPREFERENCE_DOB,
								profileData.getString("date_of_birth"));
						Utility.setFbImageId(ProfileActivity.this,
								profileData.getString("image"));
						new LoadImageTask().execute(profileData
								.getString("image"));
						Utility.setProfileDataSource(ProfileActivity.this,
								profileData.getString("fb_data"));
						Constants.showOneButtonDialog(ProfileActivity.this,
								TAG, "Profile updated successfully.",
								Constants.DIALOG_CLOSE);
						buttonSaveAndEdit.setText("EDIT");
						updateName();
						updateDOB();
						updateGender();
						updateEmail();

					} else {
						Constants.showOneButtonDialog(ProfileActivity.this,
								TAG, Constants.checkCode(result),
								Constants.DIALOG_CLOSE);
					}

				} else {
					Intent intent = new Intent(ProfileActivity.this,
							ConnectivityErrorActivity.class);
					startActivity(intent);
				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonDialog(ProfileActivity.this, TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonDialog(ProfileActivity.this, TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(ProfileActivity.this);
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(false);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							UpdateFullProfileTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			UpdateFullProfileTask.this.cancel(true);
			dialog.cancel();
		}

	}

	public class UpdateProfileTask extends AsyncTask<String, String, String>
			implements OnDismissListener {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			try {
				String response = RequestClass
						.getInstance()
						.readPay1B2CRequest(
								ProfileActivity.this,
								Constants.B2C_URL + "update_profile/?name="
										+ URLEncoder.encode(params[0], "utf-8")
										+ "&date_of_birth="
										+ URLEncoder.encode(params[1], "utf-8")
										+ "&gender="
										+ URLEncoder.encode(params[2], "utf-8"));

				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			try {

				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONObject jsonObject = new JSONObject(replaced);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {

						// JSONObject jsonObject2 = jsonObject
						// .getJSONObject("description");
						relativeLayout_Name.setVisibility(View.GONE);
						textView_Name.setVisibility(View.VISIBLE);
						linearLayout_EditName.setVisibility(View.GONE);

						dobAction.setVisibility(View.GONE);
						linearLayout_EditDOB.setVisibility(View.GONE);

						relativeLayout_Gender.setVisibility(View.GONE);
						textView_Gender.setVisibility(View.VISIBLE);
						linearLayout_EditGender.setVisibility(View.GONE);

						relativeLayout_Email.setVisibility(View.GONE);
						textView_Email.setVisibility(View.VISIBLE);
						linearLayout_EditEmail.setVisibility(View.GONE);

						Utility.setUserName(ProfileActivity.this,
								Constants.SHAREDPREFERENCE_NAME, textView_Name
										.getText().toString().trim());
						Utility.setMobileNumber(ProfileActivity.this,
								Constants.SHAREDPREFERENCE_MOBILE,
								textView_Mobile.getText().toString().trim());
						Utility.setEmail(ProfileActivity.this,
								Constants.SHAREDPREFERENCE_EMAIL,
								textView_Email.getText().toString().trim());
						Utility.setGender(ProfileActivity.this,
								Constants.SHAREDPREFERENCE_GENDER,
								textView_Gender.getText().toString().trim()
										.startsWith("M") ? "m" : "f");
						Utility.setPin(ProfileActivity.this,
								Constants.SHAREDPREFERENCE_PIN, textView_Pin
										.getText().toString().trim());
						Utility.setDOB(ProfileActivity.this,
								Constants.SHAREDPREFERENCE_DOB, builder
										.toString().trim());
						Constants.showOneButtonDialog(ProfileActivity.this,
								TAG, "Profile updated successfully.",
								Constants.DIALOG_CLOSE);

					} else {
						Constants.showOneButtonDialog(ProfileActivity.this,
								TAG, Constants.checkCode(replaced),
								Constants.DIALOG_CLOSE);
					}

				} else {
					Intent intent = new Intent(ProfileActivity.this,
							ConnectivityErrorActivity.class);
					startActivity(intent);
				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonDialog(ProfileActivity.this, TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonDialog(ProfileActivity.this, TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(ProfileActivity.this);
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(false);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							UpdateProfileTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			UpdateProfileTask.this.cancel(true);
			dialog.cancel();
		}

	}

	public class LoadFBImageTask extends AsyncTask<String, String, Bitmap> {

		@Override
		protected Bitmap doInBackground(String... params) {

			Bitmap bitmap = null;
			URL imageURL = null;
			try {
				imageURL = new URL(params[0]);

				bitmap = BitmapFactory.decodeStream(imageURL.openConnection()
						.getInputStream());
				Log.d("dd", "fff");
				return bitmap;
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Log.d("dd", "fff");

			return bitmap;
		}

		@Override
		protected void onPostExecute(Bitmap result) {

			super.onPostExecute(result);
			try {
				if (result != null) {
					imageView_profile.setImageBitmap(result);
					String imgeInBase64 = Constants.encodeTobase64(result);
					// Utility.setFbImage64(ProfileActivity.this, imgeInBase64);

				}
			} catch (Exception e) {
				// TODO: handle exception
				// Constants.showCustomToast(MainActivity.this,
				// result);
			}
			// notifyDataSetChanged();

		}

		protected void onPreExecute() {

			super.onPreExecute();
		}
	}

	public class LoadImageTask extends AsyncTask<String, String, Bitmap> {

		@Override
		protected Bitmap doInBackground(String... params) {

			Bitmap bitmap = null;
			URL imageURL = null;
			try {
				imageURL = new URL(params[0]);

				bitmap = BitmapFactory.decodeStream(imageURL.openConnection()
						.getInputStream());
				Log.d("dd", "fff");
				return bitmap;
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Log.d("dd", "fff");

			return bitmap;
		}

		@Override
		protected void onPostExecute(Bitmap result) {

			super.onPostExecute(result);
			try {
				if (result != null) {
					imageView_profile.setImageBitmap(result);
					String imgeInBase64 = Constants.encodeTobase64(result);
					Utility.setFbImage64(ProfileActivity.this, imgeInBase64);

				}
			} catch (Exception e) {
				// TODO: handle exception
				// Constants.showCustomToast(MainActivity.this,
				// result);
			}
			// notifyDataSetChanged();

		}

		protected void onPreExecute() {

			super.onPreExecute();
		}
	}

	public class ForgotPasswordTask extends AsyncTask<String, String, String>
			implements OnDismissListener {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			try {

				String response = RequestClass.getInstance()
						.readPay1B2CRequest(
								ProfileActivity.this,
								Constants.B2C_URL + "forgotpwd/?user_name="
										+ URLEncoder.encode(params[0], "utf-8")
										+ "&vtype=1");

				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			try {

				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONObject jsonObject = new JSONObject(replaced);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {

						Intent returnIntent = new Intent(ProfileActivity.this,
								ChangePinActivity.class);
						returnIntent.putExtra("OTP",
								jsonObject.getString("otp"));
						returnIntent.putExtra("IS_FLAG", false);
						
						startActivity(returnIntent);

					} else {
						Constants.showOneButtonDialog(ProfileActivity.this,
								TAG, Constants.checkCode(replaced),
								Constants.DIALOG_CLOSE_FORGOTPIN);
					}

				} else {
					Intent intent = new Intent(ProfileActivity.this,
							ConnectivityErrorActivity.class);
					startActivity(intent);

				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonDialog(ProfileActivity.this, TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonDialog(ProfileActivity.this, TAG,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(ProfileActivity.this);
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							ForgotPasswordTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			ForgotPasswordTask.this.cancel(true);
			dialog.cancel();
		}

	}
}
