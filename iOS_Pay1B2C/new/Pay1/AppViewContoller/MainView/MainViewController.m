//
//  MainViewController.m
//  Pay1
//
//  Created by Annapurna on 04/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//http://stackoverflow.com/questions/16272216/uicollection-view-adjust-cell-size-for-screen-size

#import "MainViewController.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "OperatorListViewController.h"
#import "DBManager.h"
#import "PlansViewController.h"
#import "QuickCustomCell.h"
#import "RechargeCriteria.h"
#import "RechargeService.h"
#import "RechargeResponse.h"
#import "SignInResponse.h"
#import "RechargeTransactionViewController.h"
#import "GetOperatorService.h"
#import "CoinViewController.h"
#import "TransactionCancelAlertView.h"
#import "GiftsViewController.h"
#import "HomeViewController.h"
#import "PostPaidConfirmView.h"

@interface MainViewController ()

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    postPaidInfoLabel.hidden = TRUE;
    
    prepaidButton.selected = TRUE;
    postPaidButton.selected = FALSE;
    
    
    [self addSpaceBetweenTextFieldOriginAndText:errorTxtField];
    [self addSpaceBetweenTextFieldOriginAndText:errorTxtFieldDTH];
    [self addSpaceBetweenTextFieldOriginAndText:errorTxtFieldDC];
    
    
    
    maxOperatorLenght = 10;
    
    [self hideNavBackButton];
    
    isFromWallet = TRUE;
    
  //  isDTHError = TRUE;
    
    errorTxtField.hidden = TRUE;
    dotImageView.hidden = TRUE;
    
    errorTxtFieldDC.hidden = TRUE;
    errorTxtFieldDTH.hidden = TRUE;
    
    dotImageViewDC.hidden = TRUE;
    dotImageViewDTH.hidden = TRUE;
    
    [quickCV removeFromSuperview];
    
    isMobileSwitchClicked = FALSE;
    
    mobileSwitchButton.hidden = TRUE;
    typeLabel.hidden = TRUE;
    
    quickPayLogoArray = [[NSMutableArray alloc]init];
    quickPayNumArray = [[NSMutableArray alloc]init];
    quickPayAmountArray = [[NSMutableArray alloc]init];
    quickPayOperatorName = [[NSMutableArray alloc]init];
    quickPayFlagArray = [[NSMutableArray alloc]init];
    quickPaySTVArray = [[NSMutableArray alloc]init];
    quickPayOperatorCode = [[NSMutableArray alloc]init];
    quickPayDateArray = [[NSMutableArray alloc]init];
    
    isNumberEntered = FALSE;
    isSTVFound = FALSE;
    
    checkPlanButtonDataCard.hidden = TRUE;
    checkPlanButtonDTH.hidden = TRUE;
    checkPlanButtonMobile.hidden = TRUE;
    
    quickRechargeBtn.frame = CGRectMake(5, -53, 150, 30);
    mobileBtn.frame = CGRectMake(quickRechargeBtn.frame.origin.x + quickRechargeBtn.frame.size.width + 5, -53, 88, 30);
    dthBtn.frame = CGRectMake(mobileBtn.frame.origin.x + mobileBtn.frame.size.width  -10 , -53, 88, 30);
    dataCardBtn.frame = CGRectMake(dthBtn.frame.origin.x + dthBtn.frame.size.width , -53, 88, 30);
    
    
    NSLog(@"FrameWidth :%f",self.view.frame.size.width);
    
    NSLog(@"#ScrollView Sizes :%f %f",self.view.frame.size.width,self.view.frame.size.height);
    
    [self setFrameForTxtField];
    
    [self setConfirmVIewOfButton:payButtonMobile];
    [self setConfirmVIewOfButton:payButtonDTH];
    [self setConfirmVIewOfButton:payButtonDC];
    
    [self setViewOfButton:locateShopButton];
    [self setViewOfButton:payViaDC];
    [self setViewOfButton:payViaDTH];
    
    
    [operatorTxtFieldDTH resignFirstResponder];
    
    if([Constant getQuickPayRecord] == TRUE){
        [self callQuickPayService];
    }
    else{
        [self getQuickPayDataFromDB];
    }

    

}

-(void)viewWillAppear:(BOOL)animated{
    
    
    NSLog(@"Selected Type :%d",objServiceType);
    
    
    
    if([Constant getQuickPayRecord] == TRUE){
    [self callQuickPayService];
    }
    else
    [quickCV reloadData];
  
    
    
    [self setTwoNavigationButtonsWithImages:@"money_sliding_menu.png" secondImage:@"coin_sling_menu.png" thirdImage:@"ic_search.png" isLeft:FALSE selector1:@selector(moneyButtonClicked:) selector2:@selector(coinButtonClicked:) selector3:@selector(searchBtnClicked:) isFromGift:FALSE];
    
    NSLog(@"#ScrollView :%f %f  %lu",headerScrollView.contentOffset.x , headerScrollView.contentOffset.y,(unsigned long)quickPayLogoArray.count);
    
    [headerScrollView setContentOffset:CGPointMake(headerScrollView.contentOffset.x,headerScrollView.contentOffset.y) animated:YES];
    

}


-(void)callQuickPayService{
    
    [quickPayLogoArray removeAllObjects];
    [quickPayNumArray removeAllObjects];
    [quickPayAmountArray removeAllObjects];
    [quickPayOperatorName removeAllObjects];
    [quickPayFlagArray removeAllObjects];
    [quickPaySTVArray removeAllObjects];
    [quickPayOperatorCode removeAllObjects];
    [quickPayDateArray removeAllObjects];
    
       
    GetOperatorService *objService = [[GetOperatorService alloc]init];
    [objService callQuickPayService:^(NSError *error, QuickPayResponse *objQuickResponse) {
    if(error)return ;
    else{
    [self getQuickPayDataFromDB];
    }
    }];
}


-(void)getQuickPayDataFromDB{
    
    DBManager *  objDBManager = [[DBManager alloc]initWithDatabaseFilename:@"quickPayDB.sql"];
    
     //NSString *query = [NSString stringWithFormat:@"select * from plandatatable WHERE circle_id=\"%@\" AND plan_type=\"%@\" AND operator_id=\"%@\"  ORDER BY plan_amount ASC",_selectCirID,planTypeStr,_selectOperatorID];
    
   // NSString *query = [NSString stringWithFormat:@"select * from QuickTable ORDER BY quickPayTransactionDateTime ASC"];
    
    NSString *query = [NSString stringWithFormat:@"select * from QuickTable"];


    
  //  NSLog(@"#QuickPayData :%@ %lu",[objDBManager loadDataFromDB:query],(unsigned long)[objDBManager loadDataFromDB:query].count);
    
    if([[NSArray alloc] initWithArray:[objDBManager loadDataFromDB:query]].count >0){
        
    isQuickPayFound = TRUE;
    objServiceType = kServiceTypeQuickRecharge;        
    [quickRechargeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
  
    [noQuickPayImageView removeFromSuperview];
    [noQuickPayLabel removeFromSuperview];
        
    NSArray *theArray = [[NSArray alloc] initWithArray:[objDBManager loadDataFromDB:query]];
        
    NSInteger indexOfLogoURL = [objDBManager.arrColumnNames indexOfObject:@"quickPayOperatorName"];
    for(int i=0;i<theArray.count;i++){
    [quickPayLogoArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfLogoURL]];
    }
        
  //  NSLog(@"#QuickPayData :%@ %lu",quickPayLogoArray,(unsigned long)quickPayLogoArray.count);

        
    NSInteger indexOfAmountName = [objDBManager.arrColumnNames indexOfObject:@"quickPayAmount"];
    for(int i=0;i<theArray.count;i++){
    [quickPayAmountArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfAmountName]];
    }
        
    NSInteger indexOfNumber = [objDBManager.arrColumnNames indexOfObject:@"quickPayMobileNumber"];
    for(int i=0;i<theArray.count;i++){
    [quickPayNumArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfNumber]];
    }
        
    NSInteger indexOfOperatorName = [objDBManager.arrColumnNames indexOfObject:@"quickPayOperatorID"];
    for(int i=0;i<theArray.count;i++){
    [quickPayOperatorName addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfOperatorName]];
    }
        
    NSInteger indexOfOperatorSTV = [objDBManager.arrColumnNames indexOfObject:@"quickPaySTV"];
    for(int i=0;i<theArray.count;i++){
    [quickPaySTVArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfOperatorSTV]];
    }
        
    NSInteger indexOfOperatorCode = [objDBManager.arrColumnNames indexOfObject:@"quickPayOperatorCode"];
    for(int i=0;i<theArray.count;i++){
    [quickPayOperatorCode addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfOperatorCode]];
    }
        
        
        
    NSInteger indexOfQuickFlag = [objDBManager.arrColumnNames indexOfObject:@"quickPayFlag"];
    for(int i=0;i<theArray.count;i++){
    [quickPayFlagArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfQuickFlag]];
    }
        
    NSInteger indexOfQuickDate = [objDBManager.arrColumnNames indexOfObject:@"quickPayTransactionDateTime"];
    for(int i=0;i<theArray.count;i++){
    [quickPayDateArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfQuickDate]];
    }
        
        
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
   // quickCV = [[UICollectionView alloc] initWithFrame:quickRechargeView.frame collectionViewLayout:layout];
        
    quickCV = [[UICollectionView alloc] initWithFrame:CGRectMake(5, 5, [UIScreen mainScreen].bounds.size.width - 10, quickRechargeView.frame.size.height) collectionViewLayout:layout];

        
    [quickCV setDataSource:self];
    [quickCV setDelegate:self];
        
 //   [quickCV registerClass:[QuickCustomCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
        
    [quickCV registerNib:[UINib nibWithNibName:@"QuickCustomCell" bundle:[NSBundle mainBundle]]forCellWithReuseIdentifier:@"QuickCustomCell"];
        
    quickCV.backgroundColor = [UIColor clearColor];
        
    [quickRechargeView addSubview:quickCV];
      
        
        
    
        
    [quickCV reloadData];
        

        
    [self setPageControlView:4];


    }
    
    else{
        
    [pageScrollView setContentOffset:CGPointMake(375, 0) animated:YES];

   
    if([Session shared].isAppLaunched == TRUE){
    isNumberEntered = TRUE;
    mobileNumTxtField.text = [Constant getUserPhoneNumber];
    [self findMobileOperatorFromNumber:[Constant getUserPhoneNumber] opServiceType:kServiceTypeMobile];
    }
        
    prepaidButton.selected = TRUE;
    postPaidButton.selected = FALSE;
    objServiceType = kServiceTypeMobile;
    isQuickPayFound = FALSE;
        
    [mobileBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        
    quickRechargeView.frame = CGRectMake(-375, 0, 375, 533);
    mobileView.frame = CGRectMake(0, 0, 375, 533);
    dthView.frame = CGRectMake(375, 0, 375, 533);
    dataCardView.frame = CGRectMake(750, 0, 375, 533);
        
        
        
    underLineView.frame = CGRectMake(mobileBtn.frame.origin.x+5, -7, mobileBtn.frame.size.width-15, 2);
        
        
    [self setPageControlView:3];
        
        
       

   }
    
   // NSLog(@"OperatorArrayQucikPay :%@",quickPayLogoArray);
  
}

#pragma mark - CollectionView Methods

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return quickPayLogoArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
   // QuickCustomCell * objQuickCustomCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
  //  YourCustomClass *cell = (YourCustomClass *)[collectionView dequeueReusableCellWithReuseIdentifier:@"CustomCell" forIndexPath:indexPath];
    
   QuickCustomCell*  objQuickCustomCell = (QuickCustomCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"QuickCustomCell" forIndexPath:indexPath];

    [objQuickCustomCell autoresizingMask];
    
//    NSLog(@"Quick Cell Num :%@",[quickPayNumArray objectAtIndex:indexPath.row]);
    
    objQuickCustomCell.numLabel.text = [quickPayNumArray objectAtIndex:indexPath.row];
    
    
    if([[quickPayFlagArray objectAtIndex:indexPath.row] intValue] == 4){
        objQuickCustomCell.amountLanel.text =  [Constant convertRechargeDateTime:[quickPayDateArray objectAtIndex:indexPath.row]];
   
    }
    else{
    objQuickCustomCell.amountLanel.text =  [NSString stringWithFormat:@"Rs. %@",[quickPayAmountArray objectAtIndex:indexPath.row]];
    }
    
    objQuickCustomCell.layer.borderColor = [UIColor darkGrayColor].CGColor;
    objQuickCustomCell.layer.borderWidth = 1;
    
    objQuickCustomCell.logoImageView.layer.borderColor = [UIColor darkGrayColor].CGColor;
    objQuickCustomCell.logoImageView.layer.borderWidth = 1;

    [self setRoundedCornerForImageView:objQuickCustomCell.logoImageView];
    
   // NSLog(@"theArray :%@",quickPayLogoArray);
    
    NSString *logo = [self giveLogoImageAccordingToOperator:[quickPayLogoArray objectAtIndex:indexPath.row]];
  //  objQuickCustomCell.logoImageView.image = [UIImage imageNamed:logo];
    
    objQuickCustomCell.operatorLogoImageView.image = [UIImage imageNamed:logo];


    return objQuickCustomCell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"#SelectedCell is :%@",[quickPayOperatorName objectAtIndex:indexPath.row]);
    
   // NSString *amountStr = [NSString stringWithFormat:@"Recharge the transaction of Rs. %@ on %@",[quickPayAmountArray objectAtIndex:indexPath.row],[quickPayNumArray objectAtIndex:indexPath.row]];
    
    quickMobileNum = [quickPayNumArray objectAtIndex:indexPath.row];
    quickAmount = [quickPayAmountArray objectAtIndex:indexPath.row];
    quickOperatorID = [quickPayOperatorName objectAtIndex: indexPath.row];
    quickSTV = [quickPaySTVArray objectAtIndex:indexPath.row];
    quickFlag = [quickPayFlagArray objectAtIndex:indexPath.row];
    quickOperatorName = [quickPayLogoArray objectAtIndex:indexPath.row];
    
    coverView =  [self onAddDimViewOnSuperView:self.view];
    
    if([quickFlag intValue] == 4 || [quickFlag intValue] == 5){
    [self getOperatorIDFromProductIDForQuickRechargePostPaid:quickOperatorID];
    PostPaidConfirmView * postPaidView;
        
    if([UIScreen mainScreen].bounds.size.height == 480){
    postPaidView  = [[PostPaidConfirmView alloc]initWithFrame:CGRectMake(10, self.view.frame.origin.y + 50, 270 , 410)];
            
    }
    if([UIScreen mainScreen].bounds.size.height == 568){
    postPaidView  = [[PostPaidConfirmView alloc]initWithFrame:CGRectMake(10, self.view.frame.origin.y + 80, 270 , 410)];
            
    }
        
        
    else{
    postPaidView  = [[PostPaidConfirmView alloc]initWithFrame:CGRectMake(20, self.view.frame.origin.y + 120, self.view.frame.size.width - 20 , 410)];
            
    }
        
        [postPaidView PutDataOnCancelAlert:quickMobileNum chargeSlab:serviceChargeSlab chargePercent:serviceTaxPercent chargeAmount:serviceChargeAmount];
        
        
        
    [coverView addSubview:postPaidView];
        
        [postPaidView setOnCancelClick:^{
            [coverView removeFromSuperview];
        }];
        
        [postPaidView setOnCheckClick:^(BOOL isType) {
            if(isType)
                isFromWallet = TRUE;
            if(!isType)
                isFromWallet = FALSE;
        }];
        
        
        [postPaidView setOnProceedClick:^(NSString *netAmount,NSString *netBaseAmount,NSString *netCharge,NSString *netPercent){
            quickAmount = netAmount;
            serviceChargeAmount = netCharge;
            serviceTaxPercent = netPercent;
            quickPayBaseAmount = netBaseAmount;
            
            [self confirmCLicked:self];
        }];

        

    }
    
    else
    [coverView addSubview:[self addTransactionCancelView:quickMobileNum mobAmount:quickAmount mobOperator:quickOperatorName]];

}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(150, 70);
}


-(void)getOperatorIDFromProductIDForQuickRechargePostPaid:(NSString *)productID{
    DBManager *  objDBManager = [[DBManager alloc]initWithDatabaseFilename:@"operatorDB.sql"];
    
   
    
    //  NSString *query = [NSString stringWithFormat:@"select OPERATOR_ID,OPERATOR_STV,OPERATOR_MIN,OPERATOR_MAX from OPERATOR WHERE OPERATOR_PRODUCT_ID=\"%@\" AND OPERATOR_KEY=\"%@\"",productID,serviceType];
    
    NSString *query = [NSString stringWithFormat:@"select OPERATOR_CHARGES_SLAB,OPERATOR_SERVICE_CHARGE_AMOUNT,OPERATOR_SERVICE_TAX_PERCENT from OPERATOR WHERE OPERATOR_ID=\"%@\"AND OPERATOR_KEY=\"%@\"",productID,@"PostPaid"];
    
    
    NSLog(@"OperatorQuery :%@",query);
    
    
    NSArray *theOpArray = [[NSArray alloc]initWithArray:[objDBManager loadDataFromDB:query]];
    
    
    NSLog(@"DetailOpearatorData :%@",theOpArray);
    
    
    if([objDBManager loadDataFromDB:query].count >0){
        
        serviceChargeSlab = [[[objDBManager loadDataFromDB:query] objectAtIndex:0] objectAtIndex:0];
        serviceChargeAmount = [[[objDBManager loadDataFromDB:query] objectAtIndex:0] objectAtIndex:1];
        serviceTaxPercent = [[[objDBManager loadDataFromDB:query] objectAtIndex:0] objectAtIndex:2];
        
        
        
        
       
    }
    
    
}


-(IBAction)quickRechargeClicked:(id)sender{
    
    [self resignTextFields];

    
    int contentOffSet = pageScrollView.contentOffset.x ;
    
    NSLog(@"#TheContentOffSet is QuickRecharge:%d",contentOffSet);
    
    
   /* if((objServiceType == kServiceTypeDataCard && contentOffSet == 1125) || (objServiceType == kServiceTypeDTH && contentOffSet == 750) || (objServiceType == kServiceTypeMobile && contentOffSet == 375)){
    [pageScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    }
    else if((objServiceType == kServiceTypeDataCard && contentOffSet == 750) || (objServiceType == kServiceTypeDTH && contentOffSet == 375) || (objServiceType == kServiceTypeMobile && contentOffSet == 0)){
    [pageScrollView setContentOffset:CGPointMake(- 375, 0) animated:YES];
    }*/
    
    if(isQuickPayFound){
        [pageScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    }
    else{
        [pageScrollView setContentOffset:CGPointMake(-375, 0) animated:YES];
    }
   
    objServiceType = kServiceTypeQuickRecharge;
    
    [self setTextColorAccrdingToClick:kServiceTypeQuickRecharge];
    
}

-(IBAction)mobileClicked:(id)sender{
    [self resignTextFields];

    
    int contentOffSet = pageScrollView.contentOffset.x ;
    
    NSLog(@"#TheContentOffSet is Mobile:%d",contentOffSet);
    
    if(isQuickPayFound){
    [pageScrollView setContentOffset:CGPointMake(375, 0) animated:YES];
    }
    else{
    [pageScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    }

    
    
  /*  if((objServiceType == kServiceTypeDataCard && contentOffSet == 1125) || (objServiceType == kServiceTypeDTH && contentOffSet == 750) || (objServiceType == kServiceTypeQuickRecharge && contentOffSet == 0)){
    [pageScrollView setContentOffset:CGPointMake(375, 0) animated:YES];
    }
    else if((objServiceType == kServiceTypeDataCard && contentOffSet == 750) || (objServiceType == kServiceTypeDTH && contentOffSet == 375) || (objServiceType == kServiceTypeQuickRecharge && contentOffSet == -375)){
    [pageScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    }*/
    
    if(prepaidButton.selected)
    objServiceType = kServiceTypeMobile;
    else if(postPaidButton.selected)
    objServiceType = kServiceTypePostPaid;

    
    [self setTextColorAccrdingToClick:kServiceTypeMobile];
   
   
}

-(IBAction)dthClicked:(id)sender{
    
    [self resignTextFields];

    
    int contentOffSet = pageScrollView.contentOffset.x ;
    
    NSLog(@"#TheContentOffSet is DTH:%d",contentOffSet);
    
    
   /* if((objServiceType == kServiceTypeDataCard && contentOffSet == 1125) || (objServiceType == kServiceTypeQuickRecharge && contentOffSet == 0) || (objServiceType == kServiceTypeMobile && contentOffSet == 375) || (objServiceType == kServiceTypePostPaid && contentOffSet == 375)){
        [pageScrollView setContentOffset:CGPointMake(750, 0) animated:YES];
    }
    else if((objServiceType == kServiceTypeDataCard && contentOffSet == 750) || (objServiceType == kServiceTypeQuickRecharge && contentOffSet == -375) || (objServiceType == kServiceTypeMobile && contentOffSet == 0) || ((objServiceType == kServiceTypePostPaid && contentOffSet == 0))){
        [pageScrollView setContentOffset:CGPointMake(375, 0) animated:YES];
    }*/
    
    if(isQuickPayFound){
        [pageScrollView setContentOffset:CGPointMake(750, 0) animated:YES];
    }
    else{
        [pageScrollView setContentOffset:CGPointMake(375, 0) animated:YES];
    }
    
    
    objServiceType = kServiceTypeDTH;
    
    [self setTextColorAccrdingToClick:kServiceTypeDTH];

}

-(IBAction)dataCardClicked:(id)sender{
    
    [self resignTextFields];

    
    int contentOffSet = pageScrollView.contentOffset.x ;
    
    NSLog(@"#TheContentOffSet is DataCard:%d",contentOffSet);
    
    
  /*  if((objServiceType == kServiceTypeQuickRecharge && contentOffSet == 0) || (objServiceType == kServiceTypeDTH && contentOffSet == 750) || (objServiceType == kServiceTypeMobile && contentOffSet == 375)){
        [pageScrollView setContentOffset:CGPointMake(1125, 0) animated:YES];
    }
    else if((objServiceType == kServiceTypeQuickRecharge && contentOffSet == -375) || (objServiceType == kServiceTypeDTH && contentOffSet == 375) || (objServiceType == kServiceTypeMobile && contentOffSet == 0)){
        [pageScrollView setContentOffset:CGPointMake(750, 0) animated:YES];
    }*/
    
    if(isQuickPayFound){
        [pageScrollView setContentOffset:CGPointMake(1125, 0) animated:YES];
    }
    else{
        [pageScrollView setContentOffset:CGPointMake(750, 0) animated:YES];
    }
    
    objServiceType = kServiceTypeDataCard;
    
    [self setTextColorAccrdingToClick:kServiceTypeDataCard];
}

-(IBAction)mobileSwitchClicked:(id)sender{
    
    if(isMobileSwitchClicked == FALSE){
    [mobileSwitchButton setBackgroundImage:[UIImage imageNamed:@"special.png"] forState:UIControlStateNormal];
    mobileSTV = @"1";
    }
    if(isMobileSwitchClicked == TRUE){
    [mobileSwitchButton setBackgroundImage:[UIImage imageNamed:@"normal.png"] forState:UIControlStateNormal];
    mobileSTV = @"0";

    }
    
    isMobileSwitchClicked = !isMobileSwitchClicked;
}

-(void)setPageControlView:(int)count{
    
    headerScrollView.pagingEnabled = YES;
    
    pageScrollView.pagingEnabled = YES;
    pageScrollView.showsHorizontalScrollIndicator = NO;
    pageScrollView.delegate = self;
    [pageScrollView setContentSize:CGSizeMake(pageScrollView.frame.size.width * count, pageScrollView.frame.size.height)];
    
    headerScrollView.contentSize = CGSizeMake(optionView.frame.size.width + 160, headerScrollView.frame.origin.y +  -80);
}

-(void)setFrameForTxtField{
    
    errorBlackViewMobile.hidden = TRUE;
    errorBlackViewDTH.hidden = TRUE;
    errorBlackViewDC.hidden = TRUE;
    
    
    mobileNumTxtField.frame = CGRectMake(25, 94, self.view.frame.size.width -150, 35);
    
    
    contactsButton.frame = CGRectMake(mobileNumTxtField.frame.origin.x + mobileNumTxtField.frame.size.width + 60, 94, 30, 30);
    
    
    mobileSepView.frame = CGRectMake(20, mobileNumTxtField.frame.origin.y + mobileNumTxtField.frame.size.height + 5, self.view.frame.size.width - 40, 1);
    dotImageView.frame = CGRectMake(self.view.frame.size.width - 40, 96, 25, 25);
    errorTxtField.frame = CGRectMake((mobileSepView.frame.origin.x + mobileSepView.frame.size.width ) / 2 - 30, 138, self.view.frame.size.width - 140, 30);
    
    operatorTxtField.frame = CGRectMake(25, mobileNumTxtField.frame.origin.y + mobileNumTxtField.frame.size.height+ 25  , self.view.frame.size.width - 40, 35);
    rightArrowButton.frame = CGRectMake(operatorTxtField.frame.origin.x + operatorTxtField.frame.size.width - 20, operatorSepView.frame.origin.y - 20, 10, 10);

    operatorSepView.frame = CGRectMake(20, operatorTxtField.frame.origin.y + operatorTxtField.frame.size.height + 5, self.view.frame.size.width - 40, 1);

    
    if(isSTVFound == TRUE){
    typeLabel.frame = CGRectMake(25, 210, 50, 21);
    amountTxtField.frame = CGRectMake(25, operatorTxtField.frame.origin.y + operatorTxtField.frame.size.height+ 50, self.view.frame.size.width - 30, 35);
    amountSepView.frame = CGRectMake(20, amountTxtField.frame.origin.y + amountTxtField.frame.size.height+ 5, self.view.frame.size.width - 40, 1);
    mobileSwitchButton.frame = CGRectMake(typeLabel.frame.origin.x + typeLabel.frame.size.width + 10, 210, 50, 20);
        
    }
    else{
    amountTxtField.frame = CGRectMake(25, operatorTxtField.frame.origin.y + operatorTxtField.frame.size.height+ 25, self.view.frame.size.width - 30, 35);
    amountSepView.frame = CGRectMake(20, amountTxtField.frame.origin.y + amountTxtField.frame.size.height + 5, self.view.frame.size.width - 40, 1);
        
    }
    
    
    checkPlanButtonMobile.frame = CGRectMake(self.view.frame.size.width - 120, amountSepView.frame.origin.y - 40, 80, 30);
    payButtonMobile.frame = CGRectMake(17, amountSepView.frame.origin.y + amountSepView.frame.size.height + 60, self.view.frame.size.width - 40, 50);
    postPaidInfoLabel.frame = CGRectMake(20, amountSepView.frame.origin.y + amountSepView.frame.size.height+ 5, self.view.frame.size.width - 40, 40);

    
    operatorTxtFieldDTH.frame = CGRectMake(25, 40, self.view.frame.size.width - 40, 35);
    
    operatorSepViewDTH.frame = CGRectMake(20, operatorTxtFieldDTH.frame.origin.y + operatorTxtFieldDTH.frame.size.height + 5, self.view.frame.size.width - 40, 1);
    rightArrowButtonDTH.frame = CGRectMake(operatorTxtFieldDTH.frame.origin.x + operatorTxtFieldDTH.frame.size.width - 20, operatorSepViewDTH.frame.origin.y - 20, 10, 10);
    
    dotImageViewDTH.frame = CGRectMake(self.view.frame.size.width - 60, 40, 25, 25);
    errorTxtFieldDTH.frame = CGRectMake((operatorSepViewDTH.frame.origin.x + operatorSepViewDTH.frame.size.width ) / 2 - 30, operatorSepViewDTH.frame.origin.y + operatorSepViewDTH.frame.size.height + 5, self.view.frame.size.width - 140, 30);
    

    subscriberNumTxtFieldDTH.frame = CGRectMake(25, operatorTxtFieldDTH.frame.origin.y + operatorTxtFieldDTH.frame.size.height + 25, self.view.frame.size.width - 30, 35);
    subscriberSepViewDTH.frame = CGRectMake(20, subscriberNumTxtFieldDTH.frame.origin.y + subscriberNumTxtFieldDTH.frame.size.height + 5, self.view.frame.size.width - 40, 1);
    


    amountTxtFieldDTH.frame = CGRectMake(25, subscriberNumTxtFieldDTH.frame.origin.y + subscriberNumTxtFieldDTH.frame.size.height + 25, self.view.frame.size.width - 30, 35);
    amountSepViewDTH.frame = CGRectMake(20, amountTxtFieldDTH.frame.origin.y + amountTxtFieldDTH.frame.size.height + 5, self.view.frame.size.width - 40, 1);
    
    checkPlanButtonDTH.frame = CGRectMake(self.view.frame.size.width - 120, amountSepViewDTH.frame.origin.y - 40, 80, 30);

    
    payButtonDTH.frame = CGRectMake(17, amountSepViewDTH.frame.origin.y + amountSepViewDTH.frame.size.height + 50, self.view.frame.size.width - 40, 50);
    
    
    mobileNumTxtFieldDC.frame = CGRectMake(25, 40, self.view.frame.size.width -40, 35);
    mobileSepViewDC.frame = CGRectMake(20, mobileNumTxtFieldDC.frame.origin.y + mobileNumTxtFieldDC.frame.size.height + 5, self.view.frame.size.width - 40, 1);

    operatorTxtFieldDC.frame = CGRectMake(25, mobileNumTxtFieldDC.frame.origin.y + mobileNumTxtFieldDC.frame.size.height + 25, self.view.frame.size.width - 40, 35);
    operatorSepViewDC.frame = CGRectMake(20, operatorTxtFieldDC.frame.origin.y + operatorTxtFieldDC.frame.size.height + 5, self.view.frame.size.width - 40, 1);

    amountTxtFieldDC.frame = CGRectMake(25, operatorTxtFieldDC.frame.origin.y + operatorTxtFieldDC.frame.size.height + 25, self.view.frame.size.width - 30, 35);
    amountSepViewDC.frame = CGRectMake(20, amountTxtFieldDC.frame.origin.y + amountTxtFieldDC.frame.size.height + 5, self.view.frame.size.width - 40, 1);

    rightArrowButtonDC.frame = CGRectMake(operatorTxtFieldDC.frame.origin.x + operatorTxtFieldDC.frame.size.width - 20, operatorSepViewDC.frame.origin.y - 20, 10, 10);
    
    
    
    
    prepaidButton.frame = CGRectMake(self.view.frame.origin.x + 20,23  , 30,30);
    prepaidLabel.frame = CGRectMake(prepaidButton.frame.origin.x + prepaidButton.frame.size.width + 10, 25, 100, 21);
    
    postPaidLabel.frame = CGRectMake(self.view.frame.size.width - 100 , 25, 100, 21);
    postPaidButton.frame = CGRectMake(postPaidLabel.frame.origin.x - 40,23  , 30,30);
    
    checkPlanButtonDataCard.frame = CGRectMake(self.view.frame.size.width - 120, operatorTxtFieldDC.frame.origin.y + operatorTxtFieldDC.frame.size.height+ 25, 80, 30);

    
    payButtonDC.frame = CGRectMake(17, amountSepViewDC.frame.origin.y + amountSepViewDC.frame.size.height + 50, self.view.frame.size.width - 40, 50);
    
}


- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    [self resignTextFields];
    
    scrollContentOffset = scrollView.contentOffset.x;
   
    
    if(scrollView == pageScrollView){
    if(isQuickPayFound == TRUE){
        
    if(scrollContentOffset == 0){
    objServiceType = kServiceTypeQuickRecharge;
    [self setTextColorAccrdingToClick:kServiceTypeQuickRecharge];
    }
        
    if(scrollContentOffset == 375){
    if(postPaidButton.selected){
    objServiceType = kServiceTypePostPaid;
    [self setTextColorAccrdingToClick:kServiceTypePostPaid];
    }
        
    if(prepaidButton.selected){
    objServiceType = kServiceTypeMobile;
    [self setTextColorAccrdingToClick:kServiceTypeMobile];
    }
        
    }
        
    if(scrollContentOffset == 750){
    objServiceType = kServiceTypeDTH;
    [self setTextColorAccrdingToClick:kServiceTypeDTH];
    }
        
    if(scrollContentOffset == 1125){
    objServiceType = kServiceTypeDataCard;
    [self setTextColorAccrdingToClick:kServiceTypeDataCard];
    }
        
    }
    
        
        
    else{
        
    if(scrollContentOffset == 0 && objServiceType == kServiceTypeMobile){
  //  objServiceType = kServiceTypeMobile;
        
    if(isMovingDirection){
    isMovingDirection = FALSE;
    [pageScrollView setContentOffset:CGPointMake(- 375, 0) animated:YES];
    [self setTextColorAccrdingToClick:kServiceTypeQuickRecharge];
    }
        
        
    else{
    if(postPaidButton.selected){
    objServiceType = kServiceTypePostPaid;
    [self setTextColorAccrdingToClick:kServiceTypePostPaid];
    }
        
    if(prepaidButton.selected){
    objServiceType = kServiceTypeMobile;
    [self setTextColorAccrdingToClick:kServiceTypeMobile];
    }
    }
    }
     
   else if(scrollContentOffset == 0 && objServiceType == kServiceTypeDTH){
    objServiceType = kServiceTypeMobile;
    [self setTextColorAccrdingToClick:kServiceTypeMobile];
    }
        
        
        
   else if(scrollContentOffset == 375){
    objServiceType = kServiceTypeDTH;
    [self setTextColorAccrdingToClick:kServiceTypeDTH];
    }
        
        
   else if(scrollContentOffset == 750){
    objServiceType = kServiceTypeDataCard;
    [self setTextColorAccrdingToClick:kServiceTypeDataCard];
    }
        
        
   else if(scrollContentOffset == -375){
    objServiceType = kServiceTypeQuickRecharge;
    [pageScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    [self setTextColorAccrdingToClick:kServiceTypeQuickRecharge];
    }
        
    }
    }
    
    
}

-(void)setTextColorAccrdingToClick:(ServiceType)isClickedType{
    
    

    if(isClickedType == kServiceTypeMobile || isClickedType == kServiceTypePostPaid){
        
    [mobileBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [quickRechargeBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [dthBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [dataCardBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    underLineView.frame = CGRectMake(mobileBtn.frame.origin.x+10, -28, mobileBtn.frame.size.width-20, 2);
        
    if([Session shared].isAppLaunched == TRUE){
    isNumberEntered = TRUE;
    mobileNumTxtField.text = [Constant getUserPhoneNumber];
    [self findMobileOperatorFromNumber:[Constant getUserPhoneNumber] opServiceType:kServiceTypeMobile];
    }
        
    if(isMovingDirection){
    [headerScrollView setContentOffset:CGPointMake(0,-64) animated:YES];
    }


        
    }
    
    
    if(isClickedType == kServiceTypeDTH){
     

        
    [dthBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [mobileBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [quickRechargeBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [dataCardBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        
        
    NSLog(@"#ScrollView :%f %f",headerScrollView.contentOffset.x , headerScrollView.contentOffset.y);

    [headerScrollView setContentOffset:CGPointMake(100,-64) animated:YES];

    underLineView.frame = CGRectMake(dthBtn.frame.origin.x+25, -28, dthBtn.frame.size.width-50, 2);
        
    }
    
    if(isClickedType == kServiceTypeDataCard){

   
    [dataCardBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [quickRechargeBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [mobileBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [dthBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        
   [headerScrollView setContentOffset:CGPointMake(100,-64) animated:YES];
        
    underLineView.frame = CGRectMake(dataCardBtn.frame.origin.x, -28, dataCardBtn.frame.size.width, 2);
        
    }
    
    if(isClickedType == kServiceTypeQuickRecharge){

        
    [mobileBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [quickRechargeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [dthBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [dataCardBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    underLineView.frame = CGRectMake(quickRechargeBtn.frame.origin.x, -28, quickRechargeBtn.frame.size.width, 2);
        
  
    }
    
    
}



#pragma Recharge Service

-(IBAction)proceedMobileClick:(id)sender{
    
    [self resignTextFields];
    
    if(objServiceType == kServiceTypeMobile || objServiceType == kServiceTypePostPaid){
        

   
    if(mobileNumTxtField.text.length == 0 || mobileNumTxtField.text.length < 10){
    [self showTheErrorView:TRUE textValue:@"Please enter mobile number"];
    mobileSepView.backgroundColor = [UIColor redColor];
    return;
    }
        
        if(operatorTxtField.text.length == 0 ){
         //   [self showTheErrorView:TRUE textValue:@"Please select operator"];
            operatorSepView.backgroundColor = [UIColor redColor];
            
            [self showAlertWithText:@"MOBILE" textMessage:@"Please Select Operator." firstButtonTitle:@"DONE" secondButtonTitle:nil];

            return;
        }
        
        
   else if(amountTxtField.text.length == 0){

   amountSepView.backgroundColor = [UIColor redColor];
   checkPlanButtonMobile.hidden = TRUE;
        
   dotImageView.frame = CGRectMake(self.view.frame.size.width - 40, amountSepView.frame.origin.y - 35, 25, 25);
   errorTxtField.frame = CGRectMake((amountSepView.frame.origin.x + amountSepView.frame.size.width ) / 2 - 30, amountTxtField.frame.origin.y + amountTxtField.frame.size.height + 8, self.view.frame.size.width - 140, 30);
        
    [self showTheErrorView:TRUE textValue:@"Please enter amount"];
    return;
    }
        
   else if([amountTxtField.text intValue] < [mobileMinAmount intValue] || [amountTxtField.text intValue] > [mobileMaxAmount intValue]){
       
       NSLog(@"#Min and MAax Amount :%@ %@",mobileMinAmount,mobileMaxAmount);
            
    amountSepView.backgroundColor = [UIColor redColor];
    checkPlanButtonMobile.hidden = TRUE;
            
    dotImageView.frame = CGRectMake(self.view.frame.size.width - 40, amountSepView.frame.origin.y - 35, 25, 25);
   
       
    errorBlackViewMobile.frame = CGRectMake(amountSepView.frame.origin.x + 30, amountTxtField.frame.origin.y + amountTxtField.frame.size.height + 8, amountSepView.frame.origin.x + amountSepView.frame.size.width - 50, 50);
       
       
    errorLabelMobile.text =  [NSString stringWithFormat:@"Please enter amount between range \n%@ to %@",mobileMinAmount,mobileMaxAmount];
       
    errorBlackViewMobile.hidden = FALSE;

    return;
    }
        
      else  if(mobileNumTxtField.text.length == 10 && objServiceType == kServiceTypeMobile && ![mobileNumTxtField.text hasPrefix:@"9"] && ![mobileNumTxtField.text hasPrefix:@"8"] && ![mobileNumTxtField.text hasPrefix:@"7"]){
            NSString * firstLetter = [mobileNumTxtField.text substringToIndex:1];
            
            if(([firstLetter isEqualToString:@"0"] || [firstLetter isEqualToString:@"1"] || [firstLetter isEqualToString:@"2"] || [firstLetter isEqualToString:@"3"] || [firstLetter isEqualToString:@"4"] || [firstLetter isEqualToString:@"5"] || [firstLetter isEqualToString:@"6"])){
                
                [self showAlertWithText:@"MOBILE" textMessage:@"Please Enter valid mobile number." firstButtonTitle:@"DONE" secondButtonTitle:nil];
                return ;
            }
        }
        
        
        
        
    else{
    [self showConfirmationViewToRecharge:mobileOperatorName];
    }
        
        
    }
    
    if(objServiceType == kServiceTypeDTH){
    
    NSArray *array = [dthOperatorPrefix componentsSeparatedByString:@","];
        
    NSString *subIndex = [array objectAtIndex:0];
        
    NSLog(@"#ArrayObjectAtindex0 :%@ %@ %@",[array objectAtIndex:0],subIndex,dthOperatorLength);
        
        
    if(operatorTxtFieldDTH.text.length == 0){
        
    dotImageViewDTH.hidden = FALSE;
    errorTxtFieldDTH.hidden = FALSE;
        
    operatorSepViewDTH.backgroundColor = [UIColor redColor];
        
    dotImageViewDTH.frame = CGRectMake(self.view.frame.size.width - 40, operatorSepViewDTH.frame.origin.y - 35, 25, 25);
    errorTxtFieldDTH.frame = CGRectMake((operatorSepViewDTH.frame.origin.x + operatorSepViewDTH.frame.size.width ) / 2 - 30, operatorTxtFieldDTH.frame.origin.y + operatorTxtFieldDTH.frame.size.height + 8, self.view.frame.size.width - 140, 30);
        
    [self showTheErrorView:TRUE textValue:@"Please select operator"];
        
        return;
    }
        
        
        
   else if(subscriberNumTxtFieldDTH.text.length == 0){
       
       
    dotImageViewDTH.hidden = FALSE;
    errorTxtFieldDTH.hidden = FALSE;
       
    subscriberSepViewDTH.backgroundColor = [UIColor redColor];
       
    dotImageViewDTH.frame = CGRectMake(self.view.frame.size.width - 40, subscriberSepViewDTH.frame.origin.y - 35, 25, 25);
       
    errorTxtFieldDTH.frame = CGRectMake((subscriberSepViewDTH.frame.origin.x + subscriberSepViewDTH.frame.size.width ) / 2 - 30, subscriberNumTxtFieldDTH.frame.origin.y + subscriberNumTxtFieldDTH.frame.size.height + 8, self.view.frame.size.width - 140, 30);
       
    [self showTheErrorView:TRUE textValue:@"Please enter Subscriber ID"];

    return;
    }
        
   else if([operatorTxtFieldDTH.text isEqual:@"Dish TV DTH"] && [[subscriberNumTxtFieldDTH.text substringToIndex:1] intValue] != 4 ){
       
       //else if(![dthOperatorLength isEqualToString:@"0"] && ){
       
       NSString *name;
       if([operatorTxtFieldDTH.text isEqual:@"Airtel DTH"] || [operatorTxtFieldDTH.text isEqual:@"Videocon DTH"]){
           name = @"Customer ID (CID)";
       }
       
       if([operatorTxtFieldDTH.text isEqual:@"Sun TV DTH"] || [operatorTxtFieldDTH.text isEqual:@"Big TV DTH"]){
           name = @"Smart card (SC)";
       }
       
       if([operatorTxtFieldDTH.text isEqual:@"Tata Sky DTH"]){
           name = @"Subscriber ID (SID)";
       }
       
       if([operatorTxtFieldDTH.text isEqual:@"Dish TV DTH"]){
           name = @"Viewing card (VC)";
       }
       
       
       NSLog(@"#DTHError :%@",name);
       
       NSString *dthErrorMessage = [NSString stringWithFormat:@"Your %@  starts with %@ and is %@ digits long.",name,@"0",dthOperatorLength];
       
       NSLog(@"#dthErrorMessage :%@",dthErrorMessage);
       
       
       [self showAlertWithText:@"DTH" textMessage:dthErrorMessage firstButtonTitle:nil secondButtonTitle:@"DONE"];
       
       
       return;
   }
        

     
        
        
   else  if(![[subscriberNumTxtFieldDTH.text substringToIndex:1] isEqualToString:[array objectAtIndex:0]] && subIndex.length > 0 && subscriberNumTxtFieldDTH.text.length == 1){
       NSString *dthErrorMessage = [NSString stringWithFormat:@"Your %@ starts with %@ and is\n %@ digits long.",subscriberNumTxtFieldDTH.placeholder,dthOperatorPrefix,dthOperatorLength];
       [self showAlertWithText:@"DTH" textMessage:dthErrorMessage firstButtonTitle:nil secondButtonTitle:@"DONE"];
       return;
   }
    
      
       
   // else if(subIndex.length > 0 ){
  else  if(![[subscriberNumTxtFieldDTH.text substringToIndex:2] isEqualToString:[array objectAtIndex:0]] && subIndex.length > 0 && subscriberNumTxtFieldDTH.text.length > 1){
    NSString *dthErrorMessage = [NSString stringWithFormat:@"Your %@ starts with %@ and is\n %@ digits long.",subscriberNumTxtFieldDTH.placeholder,dthOperatorPrefix,dthOperatorLength];
    [self showAlertWithText:@"DTH" textMessage:dthErrorMessage firstButtonTitle:nil secondButtonTitle:@"DONE"];
    return;
    }
  
        
    
   //}
        
        
   
   else if(amountTxtFieldDTH.text.length == 0){
        
    dotImageViewDTH.hidden = FALSE;
    errorTxtFieldDTH.hidden = FALSE;
            
    amountSepViewDTH.backgroundColor = [UIColor redColor];
        
    dotImageViewDTH.frame = CGRectMake(self.view.frame.size.width - 40, amountSepViewDTH.frame.origin.y - 35, 25, 25);
       
        errorTxtFieldDTH.frame = CGRectMake((amountSepViewDTH.frame.origin.x + amountSepViewDTH.frame.size.width ) / 2 - 30, amountTxtFieldDTH.frame.origin.y + amountTxtFieldDTH.frame.size.height + 8, self.view.frame.size.width - 140, 30);
            
    [self showTheErrorView:TRUE textValue:@"Please enter amount"];
            
    }
        
  else  if([amountTxtFieldDTH.text intValue] < [dthMinAmount intValue] || [amountTxtFieldDTH.text intValue] > [dthMaxAmount intValue]){
            
    amountSepViewDTH.backgroundColor = [UIColor redColor];
        
    dotImageViewDTH.frame = CGRectMake(self.view.frame.size.width - 40, amountSepViewDTH.frame.origin.y - 35, 25, 25);
      
        dotImageViewDTH.hidden = FALSE;
      
    errorBlackViewDTH.hidden = FALSE;
            
    NSString *rangeOfAmount = [NSString stringWithFormat:@"Please enter amount between range %@ to %@",dthMinAmount,dthMaxAmount];
  
      
      
    errorBlackViewDTH.frame = CGRectMake(amountSepViewDC.frame.origin.x + 30, amountTxtFieldDTH.frame.origin.y + amountTxtFieldDTH.frame.size.height + 8, amountSepViewDC.frame.origin.x + amountSepViewDC.frame.size.width - 50, 50);
      
    errorLabelDTH.text = rangeOfAmount;
      
    }
        
        
        
        
    else{
        [self showConfirmationViewToRecharge:operatorTxtFieldDTH.text];
    }
        
        
    }
    
    if(objServiceType == kServiceTypeDataCard){
        
        NSLog(@"#DatacardMin andMax :%@ %@",dataCardMinAmount,dataCardMaxAmount);

        
        
    if(mobileNumTxtFieldDC.text.length == 0 || mobileNumTxtFieldDC.text.length < 10){
    dotImageViewDC.hidden = FALSE;
    errorTxtFieldDC.hidden = FALSE;
        
    dotImageViewDC.frame = CGRectMake(self.view.frame.size.width - 40, mobileSepViewDC.frame.origin.y - 35, 25, 25);
    errorTxtFieldDC.frame = CGRectMake(operatorTxtFieldDC.frame.origin.x + 35, mobileSepViewDC.frame.origin.y + mobileSepViewDC.frame.size.height, self.view.frame.size.width - 85, 30);
        
        
      //  errorTxtFieldDTH.frame = CGRectMake((mobileSepViewDC.frame.origin.x + mobileSepViewDC.frame.size.width ) / 2 - 30, mobileNumTxtFieldDC.frame.origin.y + mobileNumTxtFieldDC.frame.size.height + 8, self.view.frame.size.width - 140, 30);
        
        
      //  errorTxtFieldDTH.frame = CGRectMake((amountSepViewDTH.frame.origin.x + amountSepViewDTH.frame.size.width ) / 2 - 30, amountTxtFieldDTH.frame.origin.y + amountTxtFieldDTH.frame.size.height + 8, self.view.frame.size.width - 140, 30);

        
    [self showTheErrorView:TRUE textValue:@"Please enter valid datacard number"];
    mobileSepViewDC.backgroundColor = [UIColor redColor];
        
        return;
    }
        
        
        if(operatorTxtFieldDC.text.length == 0 ){
            //   [self showTheErrorView:TRUE textValue:@"Please select operator"];
            operatorSepViewDC.backgroundColor = [UIColor redColor];
            
            [self showAlertWithText:@"Data Card" textMessage:@"Please Select Operator." firstButtonTitle:@"DONE" secondButtonTitle:nil];
            
            return;
        }
        
   else if(amountTxtFieldDC.text.length == 0){
        
    dotImageViewDC.hidden = FALSE;
    errorTxtFieldDC.hidden = FALSE;
            
    amountSepViewDC.backgroundColor = [UIColor redColor];
            
    dotImageViewDC.frame = CGRectMake(self.view.frame.size.width - 40, amountSepViewDC.frame.origin.y - 35, 25, 25);
    errorTxtFieldDC.frame = CGRectMake((amountSepViewDTH.frame.origin.x + amountSepViewDTH.frame.size.width ) / 2 - 30, amountSepViewDC.frame.origin.y + amountSepViewDC.frame.size.height, self.view.frame.size.width - 140, 30);
       
    [self showTheErrorView:TRUE textValue:@"Please enter amount"];
       
       return;
            
    }
        
   else if([amountTxtFieldDC.text intValue] < [dataCardMinAmount intValue] || [amountTxtFieldDC.text intValue] > [dataCardMaxAmount intValue]){
       
       
    amountSepViewDC.backgroundColor = [UIColor redColor];
        
        dotImageViewDC.hidden = FALSE;
       
    dotImageViewDC.frame = CGRectMake(self.view.frame.size.width - 40, amountSepViewDC.frame.origin.y - 35, 25, 25);
       
       NSString *rangeOfAmount = [NSString stringWithFormat:@"Please enter amount between range %@ to %@",dataCardMinAmount,dataCardMaxAmount];
       
       errorBlackViewDC.hidden = FALSE;
       
       
       errorBlackViewDC.frame = CGRectMake(amountSepViewDC.frame.origin.x + 30, amountTxtFieldDC.frame.origin.y + amountTxtFieldDC.frame.size.height + 8, amountSepViewDC.frame.origin.x + amountSepViewDC.frame.size.width - 50, 50);
       
       errorLabelDC.text = rangeOfAmount;
       
       return;
            
    }
       
        if(mobileNumTxtFieldDC.text.length == 10 && objServiceType == kServiceTypeDataCard && ![mobileNumTxtFieldDC.text hasPrefix:@"9"] && ![mobileNumTxtFieldDC.text hasPrefix:@"8"] && ![mobileNumTxtFieldDC.text hasPrefix:@"7"]){
            NSString * firstLetter = [mobileNumTxtFieldDC.text substringToIndex:1];
            
            if( ([firstLetter isEqualToString:@"0"] || [firstLetter isEqualToString:@"1"] || [firstLetter isEqualToString:@"2"] || [firstLetter isEqualToString:@"3"] || [firstLetter isEqualToString:@"4"] || [firstLetter isEqualToString:@"5"] || [firstLetter isEqualToString:@"6"])){
                [self showAlertWithText:@"DATA CARD" textMessage:@"Please Enter valid datacard number." firstButtonTitle:@"DONE" secondButtonTitle:nil];
                
                return ;
            }
        }

        
    else{
        [self showConfirmationViewToRecharge:operatorTxtFieldDC.text];
    }
     
        
        
        
    
    }

    
 //
}


-(void)callRechargeService{
    [self showHUDOnView];
    RechargeService *objRechargeService = [[RechargeService alloc]init];
    [objRechargeService callRechargeService:self.objCriteria callback:^(NSError *error, RechargeResponse *objRechargeResponse) {
    if(error)return ;
    else{
    [self hideHUDOnView];
    if([objRechargeResponse.status isEqualToString:@"success"])
    if(isFromWallet == TRUE){
    [self callQuickPayService];
        
    if([[Constant getUserWalletBalance] intValue] > [amountForRecharge intValue]){
    [self showUIForWalletSuccessRecharge:objRechargeResponse.operatorName theMobNum:numberForRecharge theAmount:amountForRecharge];
        
        
    if([objRechargeResponse.loyaltyBalance isEqual:[NSNull null]]){
    [Constant saveUserWalletBalance:@"0"];
    }
    else{
    [Constant saveUserWalletBalance:objRechargeResponse.closingBalance];
    }
        
    [Constant saveUserCoinBalance:objRechargeResponse.loyaltyBalance];
    
        
   [self setTwoNavigationButtonsWithImages:@"money_sliding_menu.png" secondImage:@"coin_sling_menu.png" thirdImage:@"magnifying_glass.png" isLeft:FALSE selector1:@selector(moneyButtonClicked:) selector2:@selector(coinButtonClicked:) selector3:@selector(searchBtnClicked:) isFromGift:FALSE];
    }
    else
    [self navigateToRechargeTransactionView:objRechargeResponse];
       
        
    }
    else
    [self navigateToRechargeTransactionView:objRechargeResponse];
        
        
    else{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error!" message:objRechargeResponse.descMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    }
    }
    }];
}

-(RechargeCriteria *)objCriteria{
    RechargeCriteria *criteria = [[RechargeCriteria alloc]init];
    
    NSLog(@"#BoolWalletValue :%d",isFromWallet);
    
    if(objServiceType == kServiceTypeMobile || objServiceType == kServiceTypePostPaid){
    criteria.userMobileNumber = mobileNumTxtField.text;
        
    if(objServiceType == kServiceTypeMobile){
    criteria.flag = @"1";
    criteria.serviceType = kServiceTypeMobile;
    criteria.stv = mobileSTV;
    criteria.amount = amountTxtField.text;
        
        


    }
        
    if(objServiceType == kServiceTypePostPaid){
    criteria.flag = @"4";
    criteria.serviceType = kServiceTypePostPaid;
    criteria.baseAmount =  amountTxtField.text; //[NSString stringWithFormat:@"%d",rechargeAmount];
    criteria.serviceCharge = serviceChargeAmount; //@"10";
    criteria.serviceTax = serviceTaxPercent;

    criteria.amount = amountForPostPaidRecharge;
    //criteria.serviceTax =
        
    }
        
        
    if(isFromWallet){
    criteria.paymentOption = @"Wallet";
    criteria.partial = @"1";
        
    if([criteria.amount intValue]> [[Constant getUserWalletBalance] intValue]){
    [Constant saveUserWalletBalance:@"0"];
    }

        
    }
    else{
    criteria.paymentOption = @"online";
    criteria.partial = @"";

    }
        
    criteria.operatorID = mobileOperatorID;
        
    operatorNameForRecharge =  operatorTxtField.text; //objRechargeResponse.operatorName;

        
    
    }
    
    
    
    else if(objServiceType == kServiceTypeDTH){
    criteria.userSubscriberID = subscriberNumTxtFieldDTH.text;
    criteria.flag = @"2";
    criteria.amount = amountTxtFieldDTH.text;
        
     
    criteria.operatorID = dthOperatorID;
        
    if(isFromWallet){
    criteria.paymentOption = @"Wallet";
    criteria.partial = @"1";
        
    if([criteria.amount intValue]> [[Constant getUserWalletBalance] intValue]){
    [Constant saveUserWalletBalance:@"0"];
    }
    }
    else{
    criteria.paymentOption = @"online";
    criteria.partial = @"";

    }
 
        
    criteria.stv = @""; //dthSTV;
    criteria.serviceType = kServiceTypeDTH;
        operatorNameForRecharge =  operatorTxtFieldDTH.text;

    }
    else if(objServiceType == kServiceTypeDataCard){
    criteria.userMobileNumber = mobileNumTxtFieldDC.text;
    criteria.flag = @"3";
    criteria.amount = amountTxtFieldDC.text;
        
   
    criteria.operatorID = dataCardOperatorID;
        
    if(isFromWallet){
    criteria.paymentOption = @"Wallet";
    criteria.partial = @"1";
        
    if([criteria.amount intValue]> [[Constant getUserWalletBalance] intValue]){
    [Constant saveUserWalletBalance:@"0"];
    }

    }
    else{
    criteria.paymentOption = @"online";
    criteria.partial = @"";

    }
        
    criteria.serviceType = kServiceTypeDataCard;
        
    operatorNameForRecharge =  operatorTxtFieldDC.text;


    }
    
    else if(objServiceType == kServiceTypeQuickRecharge){
    criteria.userMobileNumber = quickMobileNum;
    criteria.flag = quickFlag;
    criteria.amount = quickAmount;
        
    if([criteria.flag intValue] == 4 || [criteria.flag intValue] == 5){
    criteria.baseAmount =  quickPayBaseAmount; //[NSString stringWithFormat:@"%d",rechargeAmount];
    criteria.serviceCharge = serviceChargeAmount; //@"10";
    criteria.serviceTax = serviceTaxPercent;
        
    }
        
    if(isFromWallet){
    criteria.paymentOption = @"Wallet";
    criteria.partial = @"1";
        
    if([criteria.amount intValue]> [[Constant getUserWalletBalance] intValue]){
    [Constant saveUserWalletBalance:@"0"];
    }

    }
    else{
    criteria.paymentOption = @"online";
    criteria.partial = @"";
    }
        
    criteria.stv = quickSTV;
    criteria.operatorID = quickOperatorID;
    criteria.serviceType = kServiceTypeQuickRecharge;
        
    operatorNameForRecharge =  quickOperatorName;
    numberForRecharge = quickMobileNum;

    }
    
    
     amountForRecharge = criteria.amount;
    
    if(objServiceType == kServiceTypeDataCard || objServiceType == kServiceTypeMobile || objServiceType == kServiceTypePostPaid)
    numberForRecharge = criteria.userMobileNumber;
    else if(objServiceType == kServiceTypeDTH)
    numberForRecharge = criteria.userSubscriberID;
    else
    numberForRecharge = quickMobileNum;
   
    
    
    flagForRecharge = criteria.flag;
    
    NSLog(@"#FlagForRecharge :%@ %@ %@",flagForRecharge,criteria.flag,postPaidInfoLabel.text);
    
    return criteria;
}


-(void)navigateToRechargeTransactionView:(RechargeResponse *)rechResponse{
    RechargeTransactionViewController *objRechargeTVC = [self.storyboard instantiateViewControllerWithIdentifier:@"rechargeTransactionView"];
    objRechargeTVC.objRechargeResponse = rechResponse;
    
    objRechargeTVC.amountToRecharge = amountForRecharge;
    objRechargeTVC.numberToRecharge = numberForRecharge;
    objRechargeTVC.operatorName = operatorNameForRecharge;
    
    objRechargeTVC.postPaidLabelText = postPaidInfoLabel.text;
    
    objRechargeTVC.typeStr = [self getTypeFromOperatorType];
    
    
    objRechargeTVC.isComingFrom = FALSE;
    objRechargeTVC.isComingFromGift = FALSE;
    objRechargeTVC.hidesBottomBarWhenPushed = TRUE;
    [self.navigationController pushViewController:objRechargeTVC animated:YES];
}


#pragma mark - Button Actions

-(IBAction)prepaidClicked:(id)sender{
    
    if(amountTxtField.text.length > 0){
        amountTxtField.text = nil;
    }
    
    postPaidInfoLabel.hidden = TRUE;
    
    prepaidButton.selected = TRUE;
    postPaidButton.selected = FALSE;
    
    if(operatorTxtField.text.length > 0){
    checkPlanButtonMobile.hidden = FALSE;
    }
    else
    checkPlanButtonMobile.hidden = TRUE;

    


    objServiceType = kServiceTypeMobile;
    [prepaidButton setBackgroundImage:[UIImage imageNamed:@"green_check_button.png"] forState:UIControlStateNormal];
    [postPaidButton setBackgroundImage:[UIImage imageNamed:@"green_uncheck_button.png"] forState:UIControlStateNormal];
    
    
    if(errorTxtField.hidden == FALSE || dotImageView.hidden == FALSE){
    [self showTheErrorView:FALSE textValue:@""];
    }
    
    
    if(mobileNumTxtField.text.length == 10)
    [self findMobileOperatorFromNumber:mobileNumTxtField.text opServiceType:kServiceTypeMobile];
    else if(mobileNumTxtField.text.length < 10 || operatorTxtField.text.length >0){
    operatorTxtField.text = @"";
    }

}

-(IBAction)postPaidClicked:(id)sender{
    objServiceType = kServiceTypePostPaid;
    
    if(amountTxtField.text.length > 0){
        amountTxtField.text = nil;
    }
    
    postPaidButton.selected = TRUE;
    prepaidButton.selected = FALSE;
    
    checkPlanButtonMobile.hidden = TRUE;
    
    [prepaidButton setBackgroundImage:[UIImage imageNamed:@"green_uncheck_button.png"] forState:UIControlStateNormal];
    [postPaidButton setBackgroundImage:[UIImage imageNamed:@"green_check_button.png"] forState:UIControlStateNormal];
    
    if(typeLabel.hidden == FALSE || mobileSwitchButton.hidden == FALSE){
    mobileSwitchButton.hidden = TRUE;
    typeLabel.hidden = TRUE;
        
    isSTVFound = FALSE;
        
    [self setFrameForTxtField];
    }
    
    if(errorTxtField.hidden == FALSE || dotImageView.hidden == FALSE){
    [self showTheErrorView:FALSE textValue:@""];
    }
    
  //  NSLog(@"#TrxtField Length :%lu %@",(unsigned long)mobileNumTxtField.text.length,mobileNumTxtField.text);
    
    if(mobileNumTxtField.text.length == 10)
    [self findMobileOperatorFromNumber:mobileNumTxtField.text opServiceType:kServiceTypePostPaid];
    else if(mobileNumTxtField.text.length < 10 || operatorTxtField.text.length >0){
        operatorTxtField.text = @"";
    }
        
}

-(BOOL)dfShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return TRUE;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
  //  [self resignTextFields];
    
    if(textField == operatorTxtField){
    if(objServiceType == kServiceTypeMobile)
    [self moveToOperatorListView:kServiceTypeMobile];
    else if(objServiceType == kServiceTypePostPaid)
    [self moveToOperatorListView:kServiceTypePostPaid];

    return NO;
    }
    
    if(textField == operatorTxtFieldDC){
    [self moveToOperatorListView:kServiceTypeDataCard];
    return NO;

    }
    
    if(textField == operatorTxtFieldDTH){
    [self moveToOperatorListView:kServiceTypeDTH];
    return NO;

    }
    
    else
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
   // NSLog(@"#MobileTextFieldLength :%d %d %d",textField.text.length,mobileNumTxtField.text.length,string.length);
    
    NSString *editedText = [textField.text stringByReplacingCharactersInRange:range withString:string];
    NSLog(@"LENGTH %lu %d",(unsigned long)editedText.length,maxOperatorLenght);
    

    
    if(editedText.length == 0){
    textField.text = @"";
    }
    
    if(textField.text.length > 0){
    [self showTheErrorView:FALSE textValue:@"Please enter mobile number"];
    mobileSepView.backgroundColor = [UIColor lightGrayColor];
    }
    
    if(operatorTxtField.text.length > 0 && textField == mobileNumTxtField){
        operatorTxtField.text = @"";
    }
    
    if(operatorTxtFieldDC.text.length > 0 && textField == mobileNumTxtFieldDC){
        operatorTxtField.text = @"";
    }

    

    if(editedText.length == 10 && textField == mobileNumTxtField && objServiceType == kServiceTypeMobile){
    NSString * firstLetter = [mobileNumTxtField.text substringToIndex:1];

    if(([firstLetter isEqualToString:@"0"] || [firstLetter isEqualToString:@"1"] || [firstLetter isEqualToString:@"2"] || [firstLetter isEqualToString:@"3"] || [firstLetter isEqualToString:@"4"] || [firstLetter isEqualToString:@"5"] || [firstLetter isEqualToString:@"6"])){
        
    [self showAlertWithText:@"MOBILE" textMessage:@"Please Enter valid mobile number." firstButtonTitle:@"DONE" secondButtonTitle:nil];
    return TRUE;
    }
    }
    
    if(editedText.length == 10 && textField == mobileNumTxtFieldDC){
    NSString * firstLetter = [mobileNumTxtFieldDC.text substringToIndex:1];

    if( ([firstLetter isEqualToString:@"0"] || [firstLetter isEqualToString:@"1"] || [firstLetter isEqualToString:@"2"] || [firstLetter isEqualToString:@"3"] || [firstLetter isEqualToString:@"4"] || [firstLetter isEqualToString:@"5"] || [firstLetter isEqualToString:@"6"])){
        [self showAlertWithText:@"DATA CARD" textMessage:@"Please Enter valid datacard number." firstButtonTitle:@"DONE" secondButtonTitle:nil];

        return TRUE;
    }
     }
    
    if(editedText.length > 0 && textField == amountTxtField && objServiceType == kServiceTypePostPaid){
    postPaidInfoLabel.hidden = FALSE;
        
    NSLog(@"#TaxPecent :%@ %@ %@",serviceChargeAmount,serviceChargeSlab,serviceTaxPercent);
        
    NSArray *items0 = [serviceChargeSlab componentsSeparatedByString:@","];
        
        NSLog(@"items0 :%@",items0);
        
        
        
        NSArray *items1 = [[items0 objectAtIndex:0] componentsSeparatedByString:@":"];
        
        NSArray *items2 = [[items0 objectAtIndex:1] componentsSeparatedByString:@":"];


        NSLog(@"items1 :%@",items1);
        NSLog(@"items2 :%@",items2);
        
        NSString *firstPrice = [items1 objectAtIndex:0];
        NSString *secondPrice = [items1 objectAtIndex:1];
        
        NSString *thirdPrice = [items2 objectAtIndex:0];
        NSString *fourthPrice = [items2 objectAtIndex:1];

        NSLog(@"#CheckFirstText :%@",[editedText substringFromIndex:0]);
        
        NSString *firstAmountStr = [editedText substringFromIndex:0];
        
     //   if(editedText substringFromIndex:)

        if([firstAmountStr intValue]<=[firstPrice intValue]){
         postPaidInfoLabel.text = [NSString stringWithFormat:@"(%d = %@ + %@ service charge inclusive of all taxes.)",[editedText intValue],editedText,@"0"];
            
            int amountPostPaid = [editedText intValue] + 0;

            
            amountForPostPaidRecharge = [NSString stringWithFormat:@"%d",[editedText intValue] + amountPostPaid];
        }
        else if([firstAmountStr intValue] > [firstPrice intValue] && [firstAmountStr intValue]<= [thirdPrice intValue] ){
         postPaidInfoLabel.text = [NSString stringWithFormat:@"(%d = %@ + %d service charge inclusive of all taxes.)",[editedText intValue]+[secondPrice intValue],editedText,[secondPrice intValue]];
            
            int amountPostPaid = [editedText intValue] + [secondPrice intValue];
            
            amountForPostPaidRecharge = [NSString stringWithFormat:@"%d",amountPostPaid];

        }
        else{
        postPaidInfoLabel.text = [NSString stringWithFormat:@"(%d = %@ + %d service charge inclusive of all taxes.)",[editedText intValue]+[fourthPrice intValue],editedText,[fourthPrice intValue]];
            
            int amountPostPaid = [editedText intValue] + [fourthPrice intValue];

            
            amountForPostPaidRecharge = [NSString stringWithFormat:@"%d",amountPostPaid];

        }
        
        
        NSLog(@"#AmountForPostPaidRecharge :%@",amountForPostPaidRecharge);
   
        
   // rechargeAmount = [editedText intValue]+ 10;
    }
    
    if(editedText.length == 10 && textField == mobileNumTxtField){
    isNumberEntered = TRUE;
    if(objServiceType == kServiceTypeMobile)
    [self findMobileOperatorFromNumber:mobileNumTxtField.text opServiceType:kServiceTypeMobile];
    if(objServiceType == kServiceTypePostPaid)
    [self findMobileOperatorFromNumber:mobileNumTxtField.text opServiceType:kServiceTypePostPaid];
        

    return TRUE;
    }
    
    if(editedText.length == 10 && textField == mobileNumTxtFieldDC){
    isNumberEntered = TRUE;
    [self findMobileOperatorFromNumber:mobileNumTxtFieldDC.text opServiceType:kServiceTypeDataCard];
    return TRUE;
    }
    
    if(editedText.length == 9 && textField == mobileNumTxtField){
    if(typeLabel.hidden == FALSE || mobileSwitchButton.hidden == FALSE){
    isSTVFound = FALSE;
    mobileSwitchButton.hidden = TRUE;
    typeLabel.hidden = TRUE;
    [self setFrameForTxtField];
    }
    operatorTxtField.text = @"";
    checkPlanButtonMobile.hidden = TRUE;
    return TRUE;
    }
    
    if(editedText.length == 9 && textField == mobileNumTxtFieldDC){
    operatorTxtFieldDC.text = @"";
    checkPlanButtonDataCard.hidden = TRUE;
    return TRUE;
    }
    
    
    if(textField == amountTxtField){
    dotImageView.hidden = TRUE;

    amountSepView.backgroundColor = [UIColor lightGrayColor];
    errorTxtField.hidden = TRUE;
        
    return TRUE;
    }
    
    if(textField == amountTxtFieldDTH){
    dotImageViewDTH.hidden = TRUE;

    amountSepViewDTH.backgroundColor = [UIColor lightGrayColor];
    errorTxtFieldDTH.hidden = TRUE;
        
    return TRUE;
    }
    
    if(textField == amountTxtFieldDC){
    dotImageViewDC.hidden = TRUE;
    amountSepViewDC.backgroundColor = [UIColor lightGrayColor];
    errorTxtFieldDC.hidden = TRUE;
        
    return TRUE;
    }
    
    if(maxOperatorLenght > 0){
        
    if (editedText.length > maxOperatorLenght)
    {
        return NO;
    }
        
   
        
  
    NSUInteger newLength = [editedText length] + [string length] - range.length;
    NSLog(@"#NewLength :%lu",(unsigned long)newLength);
    return newLength <= maxOperatorLenght + 1;
    }
   
    
    else
    {
        return TRUE;
    }
   

    
   // return TRUE;
}


-(void)findMobileOperatorFromNumber:(NSString *)enteredMobileNumber opServiceType:(ServiceType)opServiceType{
    DBManager *  objDBManager = [[DBManager alloc]initWithDatabaseFilename:@"mobiledatadb.sql"];

    NSString *mobNum = [enteredMobileNumber substringToIndex:4]; //in case string is less than 4 characters long.
    
    NSString *query = [NSString stringWithFormat:@"select * from NumberAndCircleTable WHERE nac_mobile_number=\"%@\"",mobNum];
    
    NSArray *theArray = [[NSArray alloc]initWithArray:[objDBManager loadDataFromDB:query]];
    
    NSLog(@"Data :%@ %lu %@",[objDBManager loadDataFromDB:query],(unsigned long)[objDBManager loadDataFromDB:query].count,theArray);
    
   

    
    if([objDBManager loadDataFromDB:query].count > 0){
    
    if(objServiceType == kServiceTypeMobile || objServiceType == kServiceTypePostPaid){
        
    if(objServiceType == kServiceTypeMobile){
        
    DBManager * dbManager = [[DBManager alloc]initWithDatabaseFilename:@"operatorDB.sql"];
        
    NSString *queryOperator = [NSString stringWithFormat:@"select * from operator WHERE OPERATOR_KEY=\"%@\" and OPERATOR_CODE=\"%@\"",@"Mobile",[[[objDBManager loadDataFromDB:query] objectAtIndex:0] objectAtIndex:3]];
        
        
  //  NSLog(@"#OperatorQuery :%@",queryOperator);
        
    NSArray *theOpArray = [[NSArray alloc]initWithArray:[dbManager loadDataFromDB:queryOperator]];
        
   // NSLog(@"Data :%@ %lu %@",[dbManager loadDataFromDB:queryOperator],(unsigned long)[dbManager loadDataFromDB:queryOperator].count,theOpArray);
        
    if(theOpArray.count>0){
    operatorTxtField.text = [[[[objDBManager loadDataFromDB:query] objectAtIndex:0] objectAtIndex:4] capitalizedString];
        
    if([[[[objDBManager loadDataFromDB:query] objectAtIndex:0] objectAtIndex:4] isEqualToString:@"CELLONE GSM"])
    operatorTxtField.text = @"BSNL";
        
    if([[[[objDBManager loadDataFromDB:query] objectAtIndex:0] objectAtIndex:4] isEqualToString:@"DATACOM/Videocon"])
    operatorTxtField.text = @"Videocon";
        
    if([[[[objDBManager loadDataFromDB:query] objectAtIndex:0] objectAtIndex:4] isEqualToString:@"DOLPHIN"])
    operatorTxtField.text = @"MTNL";
        
        
    if([[[[objDBManager loadDataFromDB:query] objectAtIndex:0] objectAtIndex:4] isEqualToString:@"MTS CDMA"])
    operatorTxtField.text = @"MTS";
        
        
       
    checkPlanButtonMobile.hidden = FALSE;
        
    mobileOperatorName = operatorTxtField.text;
//        [amountTxtField becomeFirstResponder];

    
        
    mobileCircleName = [[[objDBManager loadDataFromDB:query] objectAtIndex:0] objectAtIndex:2];
    mobilePlanOPID = [[[objDBManager loadDataFromDB:query] objectAtIndex:0] objectAtIndex:5];
    mobileCircleID = [[[objDBManager loadDataFromDB:query] objectAtIndex:0] objectAtIndex:1];
    [self getOperatorIDFromProductID:mobilePlanOPID];
        
    }
        
       /* UIResponder* nextResponder = [operatorTxtField.superview viewWithTag:(operatorTxtField.tag + 1)];
        if (nextResponder) {
            [nextResponder becomeFirstResponder];
            
            // Your code for checking it.
        }*/
   
    }
    
    if(objServiceType == kServiceTypePostPaid){
    checkPlanButtonMobile.hidden = TRUE;
        
    DBManager * dbManager = [[DBManager alloc]initWithDatabaseFilename:@"operatorDB.sql"];
        
    NSString *queryOperator = [NSString stringWithFormat:@"select * from operator WHERE OPERATOR_KEY=\"%@\" and OPERATOR_CODE=\"%@\"",@"PostPaid",[[[objDBManager loadDataFromDB:query] objectAtIndex:0] objectAtIndex:3]];
        
        
   // NSLog(@"#OperatorQuery :%@",queryOperator);
        
    NSArray *theOpArray = [[NSArray alloc]initWithArray:[dbManager loadDataFromDB:queryOperator]];
        
  //  NSLog(@"Data :%@ %lu %@",[dbManager loadDataFromDB:queryOperator],(unsigned long)[dbManager loadDataFromDB:queryOperator].count,theOpArray);
        
   
    if(theOpArray.count > 0){
        
    operatorTxtField.text = [[[dbManager loadDataFromDB:queryOperator] objectAtIndex:0] objectAtIndex:5]; //[NSString stringWithFormat:@"%@ %@",[newString capitalizedString],@"Postpaid"];

        
    mobileCircleName = [[[objDBManager loadDataFromDB:query] objectAtIndex:0] objectAtIndex:2];
    mobilePlanOPID = [[[dbManager loadDataFromDB:queryOperator] objectAtIndex:0] objectAtIndex:4];
    mobileCircleID = [[[objDBManager loadDataFromDB:query] objectAtIndex:0] objectAtIndex:1];
    [self getOperatorIDFromProductID:mobilePlanOPID];
        
   // operatorTxtField.text = [NSString stringWithFormat:@"%@ %@",[newString capitalizedString],@"Postpaid"];
    }
        
    mobileOperatorName = operatorTxtField.text;
    }
        

    }
    
    if(objServiceType == kServiceTypeDataCard){
    checkPlanButtonDataCard.hidden = FALSE;
   
    DBManager * dbManager = [[DBManager alloc]initWithDatabaseFilename:@"operatorDB.sql"];
        
   // NSString *queryOperator = [NSString stringWithFormat:@"select * from operator WHERE OPERATOR_KEY=\"%@\" and OPERATOR_PRODUCT_ID=\"%@\"",@"DataCard",[[[objDBManager loadDataFromDB:query] objectAtIndex:0] objectAtIndex:5]];
        
        NSString *queryOperator = [NSString stringWithFormat:@"select * from operator WHERE OPERATOR_KEY=\"%@\" and OPERATOR_CODE=\"%@\"",@"DataCard",[[[objDBManager loadDataFromDB:query] objectAtIndex:0] objectAtIndex:3]];

        
        
 //  NSLog(@"#OperatorQuery :%@",queryOperator);
    
    NSArray *theOpArray = [[NSArray alloc]initWithArray:[dbManager loadDataFromDB:queryOperator]];
        
 //   NSLog(@"Data :%@ %lu %@",[dbManager loadDataFromDB:queryOperator],(unsigned long)[dbManager loadDataFromDB:queryOperator].count,theOpArray);
        
    if(theOpArray.count>0){
    operatorTxtFieldDC.text = [[[[objDBManager loadDataFromDB:query] objectAtIndex:0] objectAtIndex:4] capitalizedString];
    dataCardOperatorName = operatorTxtFieldDC.text;
        
    dataCardCircleName = [[[objDBManager loadDataFromDB:query] objectAtIndex:0] objectAtIndex:2];
    dataCardPlanOPID = [[[objDBManager loadDataFromDB:query] objectAtIndex:0] objectAtIndex:5];
    NSString *opCode = [[[objDBManager loadDataFromDB:query] objectAtIndex:0] objectAtIndex:3];

    dataCardCircleID = [[[objDBManager loadDataFromDB:query] objectAtIndex:0] objectAtIndex:1];
        
    [self getOperatorIDForDataCrdProductID:opCode];


    }
    
    }
    }
    
   // [operatorTxtField nextResponder];
}

-(void)getOperatorIDFromProductID:(NSString *)productID{
    DBManager *  objDBManager = [[DBManager alloc]initWithDatabaseFilename:@"operatorDB.sql"];
    NSString *serviceType ;
    
   if(objServiceType == kServiceTypeMobile)
    serviceType = @"Mobile";
   else if(objServiceType == kServiceTypePostPaid)
    serviceType = @"PostPaid";
    
  //  NSString *query = [NSString stringWithFormat:@"select OPERATOR_ID,OPERATOR_STV,OPERATOR_MIN,OPERATOR_MAX from OPERATOR WHERE OPERATOR_PRODUCT_ID=\"%@\" AND OPERATOR_KEY=\"%@\"",productID,serviceType];
    
     NSString *query = [NSString stringWithFormat:@"select OPERATOR_ID,OPERATOR_STV,OPERATOR_MIN,OPERATOR_MAX,OPERATOR_CHARGES_SLAB,OPERATOR_SERVICE_CHARGE_AMOUNT,OPERATOR_SERVICE_TAX_PERCENT from OPERATOR WHERE OPERATOR_PRODUCT_ID=\"%@\" AND OPERATOR_KEY=\"%@\"",productID,serviceType];
    
    
    NSArray *theOpArray = [[NSArray alloc]initWithArray:[objDBManager loadDataFromDB:query]];

    
    NSLog(@"DetailOpearatorData :%@",theOpArray);
    

    if([objDBManager loadDataFromDB:query].count >0){
    mobileOperatorID = [[[objDBManager loadDataFromDB:query] objectAtIndex:0] objectAtIndex:0];
    mobileSTV = [[[objDBManager loadDataFromDB:query] objectAtIndex:0] objectAtIndex:1];
    mobileMinAmount = [[[objDBManager loadDataFromDB:query] objectAtIndex:0] objectAtIndex:2];
    mobileMaxAmount = [[[objDBManager loadDataFromDB:query] objectAtIndex:0] objectAtIndex:3];
        
    serviceChargeSlab = [[[objDBManager loadDataFromDB:query] objectAtIndex:0] objectAtIndex:4];
    serviceChargeAmount = [[[objDBManager loadDataFromDB:query] objectAtIndex:0] objectAtIndex:5];
    serviceTaxPercent = [[[objDBManager loadDataFromDB:query] objectAtIndex:0] objectAtIndex:6];

        
        
    
    if(objServiceType == kServiceTypeMobile){
    if([mobileSTV isEqualToString:@"1"]) {
    isSTVFound = TRUE;
    typeLabel.hidden = FALSE;
    mobileSwitchButton.hidden = FALSE;
    mobileSTV = @"0";
    [self setFrameForTxtField];
    }
    else{
    NSLog(@"#InElse>>>>>>>>>>>");
    isSTVFound = FALSE;
    mobileSTV = @"0";
    mobileSwitchButton.hidden = TRUE;
    typeLabel.hidden = TRUE;
    [self setFrameForTxtField];

    }
    }
    }
    

}


-(void)getOperatorIDForDataCrdProductID:(NSString *)productID{
    DBManager *  objDBManager = [[DBManager alloc]initWithDatabaseFilename:@"operatorDB.sql"];
    
    NSString *query = [NSString stringWithFormat:@"select OPERATOR_ID,OPERATOR_STV,OPERATOR_MIN,OPERATOR_MAX from OPERATOR WHERE OPERATOR_CODE=\"%@\" AND OPERATOR_KEY=\"%@\"",productID,@"DataCard"];
    
    NSLog(@"#DataCardQuery :%@",query);
    
    
    NSArray *theArray = [[NSArray alloc]initWithArray:[objDBManager loadDataFromDB:query]];
    
    NSLog(@"#DataCard Data :%@ %@",[objDBManager loadDataFromDB:query],theArray);


    
    if([objDBManager loadDataFromDB:query].count >0){
    dataCardOperatorID = [[[objDBManager loadDataFromDB:query] objectAtIndex:0] objectAtIndex:0];
   // dataCardSTV = [[[objDBManager loadDataFromDB:query] objectAtIndex:0] objectAtIndex:1];
    dataCardMinAmount = [[[objDBManager loadDataFromDB:query] objectAtIndex:0] objectAtIndex:2];
    dataCardMaxAmount = [[[objDBManager loadDataFromDB:query] objectAtIndex:0] objectAtIndex:3];
    }
    
    NSLog(@"SQLiteDataCard :%@ %@ %@",dataCardMaxAmount,dataCardMinAmount,dataCardOperatorID);
}

#pragma mark - Operator Info

-(IBAction)operatorInfoClicked:(id)sender{

    if(objServiceType == kServiceTypeMobile)
    [self moveToOperatorListView:kServiceTypeMobile];
    if(objServiceType == kServiceTypePostPaid)
    [self moveToOperatorListView:kServiceTypePostPaid];
}


-(IBAction)DTHInfoClicked:(id)sender{
  [self moveToOperatorListView:kServiceTypeDTH];
}

-(IBAction)DataCardinfoClicked:(id)sender{
    [self moveToOperatorListView:kServiceTypeDataCard];
}




#pragma mark - Address Book Methods

-(IBAction)getContactNumber:(id)sender{
    _addressBookController = [[ABPeoplePickerNavigationController alloc] init];
    [_addressBookController setPeoplePickerDelegate:self];
    [self presentViewController:_addressBookController animated:YES completion:nil];
}

- (void)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker didSelectPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier {
    
    NSString *contactName = CFBridgingRelease(ABRecordCopyCompositeName(person));
    NSLog(@"PhoneNumber :%@",[NSString stringWithFormat:@"%@", contactName ? contactName : @"No Name"]);
    
    ABMultiValueRef phoneRecord = ABRecordCopyValue(person, kABPersonPhoneProperty);
    CFStringRef phoneNumber = ABMultiValueCopyValueAtIndex(phoneRecord, identifier);
    
    NSLog(@"PhoneNumber :%@",(__bridge_transfer NSString *)phoneNumber);
    
    
    ABMultiValueRef email = ABRecordCopyValue(person, kABPersonEmailProperty);
    CFStringRef emailField = ABMultiValueCopyValueAtIndex(email, 0);
    NSLog(@"emailField :%@", (__bridge_transfer NSString *)emailField);
    
   
    
   // mobileNumTxtField.text =  [(__bridge_transfer NSString *)phoneNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    
    NSString *numOfContact = (__bridge_transfer NSString *)phoneNumber;

    
    if ([numOfContact hasPrefix:@"0"] && [numOfContact length] > 1) {
    numOfContact = [numOfContact substringFromIndex:1];
        
      numOfContact  = [self removeSpecialCharecterFromPhoneNumber:numOfContact];
        
       
        
        NSLog(@"numOfContact 0:%@",numOfContact);


        mobileNumTxtField.text =  numOfContact;
        
        if(objServiceType == kServiceTypeMobile)
            [self findMobileOperatorFromNumber:mobileNumTxtField.text opServiceType:kServiceTypeMobile];
        if(objServiceType == kServiceTypePostPaid)
            [self findMobileOperatorFromNumber:mobileNumTxtField.text opServiceType:kServiceTypePostPaid];



    }
    

    else if ([numOfContact hasPrefix:@"+91"] && [numOfContact length] > 1) {
        
        numOfContact = [numOfContact substringFromIndex:4];
        numOfContact  = [self removeSpecialCharecterFromPhoneNumber:numOfContact];

       // numOfContact = [numOfContact stringByReplacingOccurrencesOfString:@"-" withString:@""];
       // numOfContact = [numOfContact stringByReplacingOccurrencesOfString:@" " withString:@""];

        NSLog(@"numOfContact%@",numOfContact);



        mobileNumTxtField.text = numOfContact;
        
        if(objServiceType == kServiceTypeMobile)
            [self findMobileOperatorFromNumber:mobileNumTxtField.text opServiceType:kServiceTypeMobile];
        if(objServiceType == kServiceTypePostPaid)
            [self findMobileOperatorFromNumber:mobileNumTxtField.text opServiceType:kServiceTypePostPaid];

        
    }
    else{
        numOfContact  = [self removeSpecialCharecterFromPhoneNumber:numOfContact];

        mobileNumTxtField.text =  numOfContact;//[numOfContact stringByReplacingOccurrencesOfString:@"-" withString:@""];
        NSLog(@"#MobileNum :%@",mobileNumTxtField.text);
        if(objServiceType == kServiceTypeMobile)
            [self findMobileOperatorFromNumber:mobileNumTxtField.text opServiceType:kServiceTypeMobile];
        if(objServiceType == kServiceTypePostPaid)
            [self findMobileOperatorFromNumber:mobileNumTxtField.text opServiceType:kServiceTypePostPaid];


    }
        
        
   
  
    
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker{
    [_addressBookController dismissViewControllerAnimated:YES completion:nil];
}

-(void)searchBtnClicked:(id)sender{
    NSLog(@"#Search");
}

-(IBAction)checkPlanClicked:(id)sender{
    [self resignTextFields];

    PlansViewController *planVC = [self.storyboard instantiateViewControllerWithIdentifier:@"planViewC"];
    
    
    planVC.isShowPlanDetails = isNumberEntered;
    
    
    
    if(objServiceType == kServiceTypeMobile){
    planVC.objServiceType = kServiceTypeMobile;
    planVC.selectOperatorName = mobileOperatorName;
    planVC.selectOperatorID = mobilePlanOPID;
    planVC.selectCirID = mobileCircleID;
    planVC.selectCirName = mobileCircleName;


    }
    if(objServiceType == kServiceTypePostPaid){
    planVC.objServiceType = kServiceTypePostPaid;
    planVC.selectOperatorName = mobileOperatorName;
    planVC.selectOperatorID = mobilePlanOPID;
    planVC.selectCirID = mobileCircleID;
    planVC.selectCirName = mobileCircleName;

    }
    if(objServiceType == kServiceTypeDTH){
    planVC.objServiceType = kServiceTypeDTH;
    planVC.selectCirID = @"all";
    planVC.selectOperatorName = operatorTxtFieldDTH.text;
    planVC.selectOperatorID = dthPlanOPID;
    }
    if(objServiceType == kServiceTypeDataCard){
    planVC.objServiceType = kServiceTypeDataCard;
    planVC.selectOperatorName = dataCardOperatorName;
    planVC.selectOperatorID = dataCardPlanOPID;
    planVC.selectCirID = dataCardCircleID;
    planVC.selectCirName = dataCardCircleName;
    }
    
    [planVC setOnSelectPlan:^(NSString *thePlanAmount,ServiceType serType){
    if(serType == kServiceTypeMobile || serType == kServiceTypePostPaid){
    amountTxtField.text = thePlanAmount;
    }
        
    if(serType == kServiceTypeDataCard){
    amountTxtFieldDC.text = thePlanAmount;
    }
        
    if(serType == kServiceTypeDTH ){
    amountTxtFieldDTH.text = thePlanAmount;
    }
        
        
    }];

    planVC.hidesBottomBarWhenPushed = TRUE;

    [self.navigationController pushViewController:planVC animated:YES];
}

-(void)moveToOperatorListView:(ServiceType)serviceTypeObj{

    OperatorListViewController *operatorListVC = [self.storyboard instantiateViewControllerWithIdentifier:@"OperatorListView"];
    operatorListVC.operatorType = serviceTypeObj;
    [self resignTextFields];
    
    [operatorListVC setOnOperatorSelectClick:^(NSString *theOPName, NSString *thePlanOperatorID,NSString *theOPID,NSString *theOPSTV,NSString *theOPMinAmount,NSString *theOPMaxAmount,NSString *theOPCode,NSString *theOPLength,NSString *theOPPrefix,NSString *theOPChargeSlab,NSString *theOPChargeAmount,NSString *theOPTaxPercent){
        
        operatorSepView.backgroundColor = [UIColor lightGrayColor];
        operatorSepViewDC.backgroundColor = [UIColor lightGrayColor];

        
    if(objServiceType == kServiceTypeMobile){
    mobileOperatorName = theOPName;
    mobilePlanOPID = thePlanOperatorID;
    mobileOperatorID = theOPID;
    mobileMinAmount = theOPMinAmount;
    mobileMaxAmount = theOPMaxAmount;
    mobileSTV = theOPSTV;
    operatorTxtField.text = theOPName;
        
    checkPlanButtonMobile.hidden = FALSE;
        
    if([mobileSTV isEqualToString:@"1"]){
    typeLabel.hidden = FALSE;
    mobileSwitchButton.hidden = FALSE;
    isSTVFound = TRUE;
    mobileSTV = @"0";
                
    }
            
    
    else{
    isSTVFound = FALSE;
    mobileSTV = @"0";
    mobileSwitchButton.hidden = TRUE;
    typeLabel.hidden = TRUE;
    }
    
    [self setFrameForTxtField];
        
    
    }
        
    if(objServiceType == kServiceTypePostPaid){
    mobileOperatorName = theOPName;
    mobilePlanOPID = thePlanOperatorID;
    mobileOperatorID = theOPID;
    mobileMinAmount = theOPMinAmount;
    mobileMaxAmount = theOPMaxAmount;
        
    serviceChargeSlab = theOPChargeSlab;
    serviceChargeAmount = theOPChargeAmount;
    serviceTaxPercent = theOPTaxPercent;

        
        
  //  mobileSTV = theOPSTV;
    operatorTxtField.text = theOPName;
    checkPlanButtonMobile.hidden = TRUE;
            
        
    }
        
        
        
        
    if(objServiceType == kServiceTypeDTH){
        
    maxOperatorLenght = [theOPLength intValue];
        
        NSLog(@"#DTHMAxValue :%d",maxOperatorLenght);
        
    operatorTxtFieldDTH.text = theOPName;
        dthOperatorID = theOPID; //thePlanOperatorID;
        dthPlanOPID = thePlanOperatorID;
    dthMinAmount = theOPMinAmount;
    dthMaxAmount = theOPMaxAmount;
  //  dthSTV = theOPSTV;
    checkPlanButtonDTH.hidden = FALSE;
    isNumberEntered = TRUE;
        
    dthOperatorLength = theOPLength;
    dthOperatorPrefix = theOPPrefix;
        
    if(errorTxtFieldDTH.hidden == FALSE || dotImageViewDTH.hidden == FALSE){
    errorTxtFieldDTH.hidden = TRUE;
    dotImageViewDTH.hidden = TRUE;
    }
        
    [self setPlaceHolderForDTHOperator:theOPName];
    }
        
        
    if(objServiceType == kServiceTypeDataCard ){
    operatorTxtFieldDC.text = theOPName;
    dataCardOperatorName = theOPName;
    dataCardPlanOPID = thePlanOperatorID;
    dataCardOperatorID = theOPID;
    dataCardMinAmount = theOPMinAmount;
    dataCardMaxAmount = theOPMaxAmount;
  //  dataCardSTV = theOPSTV;
    checkPlanButtonDataCard.hidden = FALSE;
    }
        
    
    
    }];
    
    operatorListVC.hidesBottomBarWhenPushed = TRUE;
    [self.navigationController pushViewController:operatorListVC animated:YES];
}

-(void)setPlaceHolderForDTHOperator:(NSString *)dthOperatorName{
    if([dthOperatorName isEqual:@"Airtel DTH"] || [dthOperatorName isEqual:@"Videocon DTH"]){
    subscriberNumTxtFieldDTH.placeholder = @"Customer ID";
    }
    
    if([dthOperatorName isEqual:@"Sun TV DTH"] || [dthOperatorName isEqual:@"Big TV DTH"]){
    subscriberNumTxtFieldDTH.placeholder = @"Smart card No.";
    }
    
    if([dthOperatorName isEqual:@"Tata Sky DTH"]){
    subscriberNumTxtFieldDTH.placeholder = @"Subscriber ID";
    }
    
    if([dthOperatorName isEqual:@"Dish TV DTH"]){
    subscriberNumTxtFieldDTH.placeholder = @"Viewing card No.";
    }
}

-(void)setOperatorNameForCertainOperator:(NSString *)operatorName{
    if(objServiceType == kServiceTypeMobile || objServiceType == kServiceTypePostPaid){
    operatorTxtField.placeholder = operatorName;
    }
    
    if(objServiceType == kServiceTypeDataCard){
    operatorTxtFieldDC.placeholder =  operatorName;
    }
    
}



-(void)resignTextFields{
    
    if(objServiceType == kServiceTypeMobile || objServiceType == kServiceTypePostPaid){
    [operatorTxtField resignFirstResponder];
    [mobileNumTxtField resignFirstResponder];
    [amountTxtField resignFirstResponder];
    }
    
    if(objServiceType == kServiceTypeDTH){
    [operatorTxtFieldDTH resignFirstResponder];
    [subscriberNumTxtFieldDTH resignFirstResponder];
    [amountTxtFieldDTH resignFirstResponder];
    }
    
    
    if(objServiceType == kServiceTypeDataCard){
    [mobileNumTxtFieldDC resignFirstResponder];
    [operatorTxtFieldDC resignFirstResponder];
    [amountTxtFieldDC resignFirstResponder];

    }
}

-(void)moneyButtonClicked:(id)sender{
    NSLog(@"#MoneyClicked");
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate.tabBarController setSelectedIndex:3];
   
}
-(void)coinButtonClicked:(id)sender{
    NSLog(@"#CoinClicked");
    CoinViewController *coinVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CoinView"];
    coinVC.hidesBottomBarWhenPushed = TRUE;
    [self.navigationController pushViewController:coinVC animated:YES];
}

#pragma mark - Wallet Success Recharge


-(void)showUIForWalletSuccessRecharge:(NSString *)theOpName theMobNum:(NSString *)theMobNum theAmount:(NSString *)theAmount{
    
    coverView = [self onAddDimViewOnSuperView:self.view];

    TransactionCancelAlertView * transAlertView;
    
    if([UIScreen mainScreen].bounds.size.height == 480){
    transAlertView  = [[TransactionCancelAlertView alloc]initWithFrame:CGRectMake(10, self.view.frame.origin.y + 70, 270 , 410)];

    }
    if([UIScreen mainScreen].bounds.size.height == 568){
        transAlertView  = [[TransactionCancelAlertView alloc]initWithFrame:CGRectMake(10, self.view.frame.origin.y + 110, 270 , 410)];
        
    }

    
    else{
    transAlertView  = [[TransactionCancelAlertView alloc]initWithFrame:CGRectMake(20, self.view.frame.origin.y + 120, self.view.frame.size.width - 20 , 410)];

    }
    
    [coverView addSubview:transAlertView];
    
    NSString *typeStr = [self getTypeFromOperatorType];
    
    [transAlertView PutDataOnCancelAlert: [self giveLogoImageAccordingToOperator:theOpName] phoneNumber:theMobNum amount:theAmount type:[typeStr uppercaseString]];
    
    [transAlertView setOnAwesomeClick:^{
    [coverView removeFromSuperview];
    }];
}

-(void)showConfirmationViewToRecharge:(NSString *)selectOperator{
    
    NSString *selectMbileNumber;
    NSString *selectAmountNumber;
    
    if(objServiceType == kServiceTypeMobile || objServiceType == kServiceTypePostPaid){
    selectMbileNumber = mobileNumTxtField.text;
    selectAmountNumber = amountTxtField.text;
    }
    
    if(objServiceType == kServiceTypeDTH){
    selectMbileNumber = subscriberNumTxtFieldDTH.text;
    selectAmountNumber = amountTxtFieldDTH.text;
    }
    
    if(objServiceType == kServiceTypeDataCard){
    selectMbileNumber = mobileNumTxtFieldDC.text;
    selectAmountNumber = amountTxtFieldDC.text;
    }
    
    
    
    coverView =  [self onAddDimViewOnSuperView:self.view];
    
    [coverView addSubview:[self addTransactionCancelView:selectMbileNumber mobAmount:selectAmountNumber mobOperator:selectOperator]];
    
}

-(UIView *)addTransactionCancelView:(NSString *)mobNum mobAmount:(NSString *)mobAmount mobOperator:(NSString *)mobOperator{
    
    NSLog(@"ConfirmRechargeView :%@ %@ %@ %@",mobNum,mobAmount,mobOperator,[Session shared].objSignInResponse.userWalletBalance);
    
    UIView * confirmView;
    
    //if(self.view.frame.size.width == 320){
  //  if([[Session shared].objSignInResponse.userWalletBalance intValue] > 0){
    if([[Constant getUserWalletBalance] intValue] > 0){

    confirmView = [[UIView alloc]initWithFrame:CGRectMake(10, 150, 300, 250)];
    }
    else{
    if(objServiceType == kServiceTypePostPaid)
    confirmView = [[UIView alloc]initWithFrame:CGRectMake(10, 150, 300, 235)];
    else
    confirmView = [[UIView alloc]initWithFrame:CGRectMake(10, 150, 300, 195)];

    }
   // }
    
   /* else{
    if([[Session shared].objSignInResponse.userWalletBalance intValue] > 0){
    confirmView = [[UIView alloc]initWithFrame:CGRectMake(30, 150, 300, 250)];
    }
    else{
    confirmView = [[UIView alloc]initWithFrame:CGRectMake(30, 150, 300, 150)];
    }
    }*/
    
    
    confirmView.backgroundColor = [UIColor whiteColor];
    UILabel *txtLabel = [[UILabel alloc]initWithFrame:CGRectMake(70, 10, 150, 21)];
    txtLabel.text = @"CONFIRMATION";
    txtLabel.font = [UIFont fontWithName:@"OpenSans-SemiBold" size:16.0];
    txtLabel.textColor = [UIColor darkGrayColor];


    txtLabel.textAlignment = NSTextAlignmentCenter;
    
    [confirmView addSubview:txtLabel];
    
    UILabel *txtMsgLabel = [[UILabel alloc]initWithFrame:CGRectMake(20, txtLabel.frame.origin.y + txtLabel.frame.size.height + 10, confirmView.frame.size.width - 10, 21)];
    
    if(objServiceType == kServiceTypePostPaid)
    txtMsgLabel.text = @"Are you sure you want to pay bill?";
    else
    txtMsgLabel.text = @"Are you sure you want to recharge?";
    
    txtMsgLabel.textColor = [UIColor darkGrayColor];
    txtMsgLabel.font = [UIFont fontWithName:@"OpenSans-SemiBold" size:15.0];


    [confirmView addSubview:txtMsgLabel];
    
    
    UIView *detailInfoView = [[UIView alloc]initWithFrame:CGRectMake(50, txtMsgLabel.frame.origin.y + txtMsgLabel.frame.size.height + 10, 190, 60)];
    
    
    UIImageView *logoImageView = [[UIImageView alloc]initWithFrame:CGRectMake(5, 5, 50, 50)];
    
    [self setRoundedCornerForImageView:logoImageView];
    
    logoImageView.layer.borderColor = [UIColor grayColor].CGColor;
    logoImageView.layer.borderWidth = 1;
    
    
   
    
    
    UIImageView *operatorLogoImageView = [[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 30, 30)];
    operatorLogoImageView.image = [UIImage imageNamed:[self giveLogoImageAccordingToOperator:[mobOperator capitalizedString]]];
    [logoImageView addSubview:operatorLogoImageView];
    
    
    
    detailInfoView.layer.borderColor = [UIColor grayColor].CGColor;
    detailInfoView.layer.borderWidth = 1;
    
    [detailInfoView addSubview:logoImageView];
    
    UIView *horiSepView = [[UIView alloc]initWithFrame:CGRectMake(logoImageView.frame.origin.x + logoImageView.frame.size.width + 5, logoImageView.frame.origin.y + logoImageView.frame.size.height/2, 115 , 1)];
    horiSepView.backgroundColor = [UIColor lightGrayColor];
    [detailInfoView addSubview:horiSepView];
    
    UILabel *numLabel = [[UILabel alloc]initWithFrame:CGRectMake(logoImageView.frame.origin.x + logoImageView.frame.size.width + 10, 5, 100, 21)];
    numLabel.text = mobNum;
    numLabel.font = [UIFont fontWithName:@"Open Sans" size:15];
    numLabel.textAlignment = NSTextAlignmentCenter;

    [detailInfoView addSubview:numLabel];

    UILabel *rsLabel = [[UILabel alloc]initWithFrame:CGRectMake(logoImageView.frame.origin.x + logoImageView.frame.size.width + 30, horiSepView.frame.origin.y + horiSepView.frame.size.height + 5, 20, 21)];
    rsLabel.text = @"Rs.";
    rsLabel.font = [UIFont fontWithName:@"Open Sans" size:13];
   
    
    
    UILabel *amountLabel = [[UILabel alloc]initWithFrame:CGRectMake(logoImageView.frame.origin.x + logoImageView.frame.size.width + 10, horiSepView.frame.origin.y + horiSepView.frame.size.height + 5, 100, 21)];
    
    if(objServiceType == kServiceTypePostPaid)
    amountLabel.text = [NSString stringWithFormat:@"%@",amountForPostPaidRecharge];

    else
    amountLabel.text = [NSString stringWithFormat:@"%@",mobAmount];
    
    
    amountLabel.font = [UIFont fontWithName:@"Open Sans" size:15];
    amountLabel.textAlignment = NSTextAlignmentCenter;
    
    
  
    
  

    
    
    [detailInfoView addSubview:rsLabel];
    
    [detailInfoView addSubview:amountLabel];
    
    
    [confirmView addSubview:detailInfoView];
    
    
    UILabel *postPaidTxtLabel;
    if(objServiceType == kServiceTypePostPaid){
    postPaidTxtLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, detailInfoView.frame.origin.y + detailInfoView.frame.size.height, confirmView.frame.size.width - 20, 40)];
    postPaidTxtLabel.text = postPaidInfoLabel.text;

    [self setTextOnLabel:postPaidTxtLabel fontSize:13.0];
    [confirmView addSubview:postPaidTxtLabel];
    }
    
    
    UILabel *walletTextLabel;
    
  //  if([[Session shared].objSignInResponse.userWalletBalance intValue] > 0){
    if([[Constant getUserWalletBalance] intValue] > 0){

    isFromWallet = TRUE;
    checkButton = [UIButton buttonWithType:UIButtonTypeCustom];
    checkButton.frame = CGRectMake(detailInfoView.frame.origin.x + 10, detailInfoView.frame.origin.y + detailInfoView.frame.size.height + 35, 20, 20);
    [checkButton setBackgroundImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateNormal];
        
    [checkButton addTarget:self action:@selector(checkButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    
    walletTextLabel = [[UILabel alloc]initWithFrame:CGRectMake(checkButton.frame.origin.x + checkButton.frame.size.width + 10,  detailInfoView.frame.origin.y + detailInfoView.frame.size.height + 35, 200, 21)];
    walletTextLabel.text = @"USE WALLET BALANCE";
    walletTextLabel.font = [UIFont fontWithName:@"Open Sans" size:16.0];
    
    [confirmView addSubview:checkButton];
    [confirmView addSubview:walletTextLabel];
    }
    else{
    isFromWallet = FALSE;
    }
    
    UIView *horizontalSepView;
    
    
    // if([[Session shared].objSignInResponse.userWalletBalance intValue] > 0){
    if([[Constant getUserWalletBalance] intValue] > 0){

    horizontalSepView = [[UIView alloc]initWithFrame:CGRectMake(0, walletTextLabel.frame.origin.y + walletTextLabel.frame.size.height + 15, confirmView.frame.origin.x + confirmView.frame.size.width - 8, 1)];
     }
    
     else{
         if(objServiceType == kServiceTypePostPaid)
       horizontalSepView = [[UIView alloc]initWithFrame:CGRectMake(0, postPaidTxtLabel.frame.origin.y + postPaidTxtLabel.frame.size.height + 15, confirmView.frame.origin.x + confirmView.frame.size.width - 8, 1)];
         else
            horizontalSepView = [[UIView alloc]initWithFrame:CGRectMake(0, detailInfoView.frame.origin.y + detailInfoView.frame.size.height + 15, confirmView.frame.origin.x + confirmView.frame.size.width - 8, 1)];
     }
    
   
    horizontalSepView.backgroundColor = [UIColor lightGrayColor];
    [confirmView addSubview:horizontalSepView];
    
    
  //  NSLog(@"#Coordinates :%f %f",horizontalSepView.frame.origin.y,horizontalSepView.frame.size.height);
    
    UIButton *confirmButton = [UIButton buttonWithType:UIButtonTypeCustom];
    confirmButton.frame = CGRectMake(0, horizontalSepView.frame.origin.y + horizontalSepView.frame.size.height , (confirmView.frame.origin.x + confirmView.frame.size.width ) /2 -1,  confirmView.frame.size.height - (horizontalSepView.frame.origin.y + horizontalSepView.frame.size.height));
    [confirmButton setTitle:@"CONFIRM" forState:UIControlStateNormal];
    confirmButton.titleLabel.font = [UIFont fontWithName:@"OpenSans-SemiBold" size:17];
    [confirmButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [confirmButton addTarget:self action:@selector(confirmCLicked:) forControlEvents:UIControlEventTouchUpInside];
    [confirmView addSubview:confirmButton];
    
    UIView *verticalSepView = [[UIView alloc]initWithFrame:CGRectMake(confirmButton.frame.origin.x + confirmButton.frame.size.width, horizontalSepView.frame.origin.y + horizontalSepView.frame.size.height , 1, confirmView.frame.size.height - (horizontalSepView.frame.origin.y + horizontalSepView.frame.size.height))];
    verticalSepView.backgroundColor = [UIColor lightGrayColor];
    [confirmView addSubview:verticalSepView];
    
    
    UIButton *cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelButton.frame = CGRectMake(confirmButton.frame.origin.x + confirmButton.frame.size.width + 2, horizontalSepView.frame.origin.y + horizontalSepView.frame.size.height , (confirmView.frame.origin.x + confirmView.frame.size.width ) - (verticalSepView.frame.origin.y + verticalSepView.frame.size.width), confirmView.frame.size.height - (horizontalSepView.frame.origin.y + horizontalSepView.frame.size.height));
    [cancelButton setTitle:@"CANCEL" forState:UIControlStateNormal];

    
    cancelButton.titleLabel.font = [UIFont fontWithName:@"OpenSans-SemiBold" size:17];
    [cancelButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(cancelCLicked:) forControlEvents:UIControlEventTouchUpInside];
    [confirmView addSubview:cancelButton];
    
    return confirmView;
}

-(void)confirmCLicked:(id)sender{
    NSLog(@"#ConfirmClick");
    [coverView removeFromSuperview];
    
    
    [self callRechargeService];
}

-(void)cancelCLicked:(id)sender{
    NSLog(@"#CancelClick");
    
    [coverView removeFromSuperview];
    
}

-(void)showTheErrorView:(BOOL)isShow textValue:(NSString *)textValue{

    
    if((objServiceType == kServiceTypeMobile || objServiceType == kServiceTypePostPaid) && isShow == TRUE){
        
    dotImageView.hidden = FALSE;
    errorTxtField.hidden = FALSE;
    contactsButton.hidden = TRUE;
        
    errorTxtField.text = textValue;
    errorTxtField.textColor = [UIColor whiteColor];
    errorTxtField.font = [UIFont systemFontOfSize:13];
    }
    
   
    if(isShow == FALSE && (objServiceType == kServiceTypeMobile || objServiceType == kServiceTypePostPaid)){
    dotImageView.hidden = TRUE;
    errorTxtField.hidden = TRUE;
    contactsButton.hidden = FALSE;
    errorBlackViewMobile.hidden = TRUE;
        
    operatorSepView.backgroundColor = [UIColor lightGrayColor];
    mobileSepView.backgroundColor = [UIColor lightGrayColor];
    [amountSepView setBackgroundColor:[UIColor lightGrayColor]];
        
    }
    
    
    if(objServiceType == kServiceTypeDTH  && isShow == TRUE){
        
    dotImageViewDTH.hidden = FALSE;
    errorTxtFieldDTH.hidden = FALSE;
        
    errorTxtFieldDTH.text = textValue;
    errorTxtFieldDTH.textColor = [UIColor whiteColor];
    errorTxtFieldDTH.font = [UIFont systemFontOfSize:13];
    }
    
    
    if(isShow == FALSE && objServiceType == kServiceTypeDTH){
    dotImageViewDTH.hidden = TRUE;
    errorTxtFieldDTH.hidden = TRUE;
    errorBlackViewDTH.hidden = TRUE;
    subscriberSepViewDTH.backgroundColor = [UIColor lightGrayColor];
    operatorSepViewDTH.backgroundColor = [UIColor lightGrayColor];
    [amountSepViewDTH setBackgroundColor:[UIColor lightGrayColor]];
    }
    
    
    
    if(objServiceType == kServiceTypeDataCard && isShow == TRUE){
        
    dotImageViewDC.hidden = FALSE;
    errorTxtFieldDC.hidden = FALSE;
        
    errorTxtFieldDC.text = textValue;
    errorTxtFieldDC.textColor = [UIColor whiteColor];
    errorTxtFieldDC.font = [UIFont systemFontOfSize:13];
    }
    
    
    if(isShow == FALSE && objServiceType == kServiceTypeDataCard){
    dotImageViewDC.hidden = TRUE;
    errorTxtFieldDC.hidden = TRUE;
    errorBlackViewDC.hidden = TRUE;
        
    operatorSepViewDC.backgroundColor = [UIColor lightGrayColor];
    mobileSepViewDC.backgroundColor = [UIColor lightGrayColor];
    [amountSepViewDC setBackgroundColor:[UIColor lightGrayColor]];
    }
    
}

-(void)checkButtonTapped:(id)sender{
    NSLog(@"#CheckTapped");
    
    if(isFromWallet == TRUE){
    [checkButton setBackgroundImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
    }
    
    if(isFromWallet == FALSE){
    [checkButton setBackgroundImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateNormal];
    }
    
    isFromWallet = !isFromWallet;
}

-(void)showAlertWithText:(NSString *)title textMessage:(NSString *)textMessage firstButtonTitle:(NSString *)firstButtonTitle secondButtonTitle:(NSString *)secondButtonTitle{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title  message:textMessage delegate:self cancelButtonTitle:firstButtonTitle otherButtonTitles:secondButtonTitle, nil];
    [alert show];
}


-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    pointNow = scrollView.contentOffset;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    if (scrollView.contentOffset.x<pointNow.x) {
       // [self resignTextFields];

       // NSLog(@"LEFT");
        isMovingDirection = TRUE;
    }
    
    else if (scrollView.contentOffset.x>pointNow.x) {
     //   [self resignTextFields];

       // NSLog(@"RIGHT");
        isMovingDirection = FALSE;
    }
}

- (void)scrollRectToVisible:(CGRect)rect animated:(BOOL)animated{
  //  headerScrollView
    
    
}// scroll so rect is just visible (nearest edges). nothing if rect completely visible

-(NSString *)getTypeFromOperatorType{
    NSString *typeStr;
    if(objServiceType == kServiceTypeMobile){
    typeStr = @"Prepaid Recharge";
    }
    
    if(objServiceType == kServiceTypePostPaid)
    typeStr = @"Postpaid Recharge";
    
    
    else if(objServiceType == kServiceTypeDTH)
    typeStr = @"DTH Recharge";
    
    else if(objServiceType == kServiceTypeDataCard)
    typeStr = @"Datacard Recharge";
    
    else if(objServiceType == kServiceTypeQuickRecharge){
    if([flagForRecharge intValue] == 1)
    typeStr = @"Prepaid Recharge";
    if([flagForRecharge intValue] == 2)
    typeStr = @"DTH Recharge";
        
    if([flagForRecharge intValue] == 3)
    typeStr = @"Datacard Recharge";
        
    if([flagForRecharge intValue] == 4)
    typeStr = @"Postpaid Recharge";
        
    }
    return typeStr;
}
-(NSString *)removeSpecialCharecterFromPhoneNumber:(NSString *)phNum{
    NSMutableString *strippedString = [NSMutableString stringWithCapacity:phNum.length];
    
    NSScanner *scanner = [NSScanner scannerWithString:phNum];
    
    NSCharacterSet *numbers = [NSCharacterSet characterSetWithCharactersInString:@"1234567890"];
    
    while ([scanner isAtEnd] == NO) {
    NSString *buffer;
    if ([scanner scanCharactersFromSet:numbers intoString:&buffer]) {
    [strippedString appendString:buffer];
            
    } else {
    [scanner setScanLocation:([scanner scanLocation] + 1)];
    }
    }
    
    NSLog(@"%@", strippedString);
    
    return strippedString;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
