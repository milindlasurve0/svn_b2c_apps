﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace Pay1B2C.UserControls
    {
    public partial class TopBackBar: UserControl
        {
        public TopBackBar()
            {
            InitializeComponent();
            }

        private void ButtonImage_Click(object sender, RoutedEventArgs e)
            {
            (Application.Current.RootVisual as PhoneApplicationFrame).RemoveBackEntry();
            }
        }
    }