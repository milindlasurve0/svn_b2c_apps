package com.pay1.utilities;

import java.util.regex.Pattern;

import android.content.Context;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.widget.EditText;
import android.widget.TextView;

import com.pay1.R;

public class EditTextValidator {

	// Regular Expression
	// you can change the expression based on your need
	private static final String EMAIL_REGEX = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	private static final String PHONE_REGEX = "\\d{3}-\\d{7}";

	// Error Messages
	private static final String PHONE_MSG = "###-#######";

	// call this method when you need to check email validation
	public static boolean isEmailAddress(Context context, EditText editText,
			String errorMessage) {

		if (isValid(editText, EMAIL_REGEX, errorMessage, true)) {
			editText.setBackgroundDrawable(context.getResources().getDrawable(
					R.drawable.edittext_valid_background_selector));
			editText.setError(null);
			editText.setPadding(15, 5, 15, 5);
			editText.setTextSize(24);
			editText.setTextColor(context.getResources()
					.getColor(R.color.Black));
			return true;

		} else {
			int ecolor = R.color.black;
			ForegroundColorSpan fgcspan = new ForegroundColorSpan(ecolor);
			SpannableStringBuilder ssbuilder = new SpannableStringBuilder(
					errorMessage);
			ssbuilder.setSpan(fgcspan, 0, errorMessage.length(), 0);
			editText.setError(ssbuilder);
			editText.setBackgroundDrawable(context.getResources().getDrawable(
					R.drawable.edittext_error_background_selector));
			editText.setPadding(15, 5, 15, 5);
			editText.setTextSize(24);
			return false;
		}
	}

	public static boolean isEmailAddressLogin(Context context,
			EditText editText, String errorMessage) {

		if (isValid(editText, EMAIL_REGEX, errorMessage, true)) {
			editText.setBackgroundDrawable(context.getResources().getDrawable(
					R.drawable.edittext_valid_background_selector_login));
			editText.setError(null);
			editText.setPadding(15, 5, 15, 5);
			editText.setTextSize(24);
			editText.setTextColor(context.getResources()
					.getColor(R.color.Black));
			return true;

		} else {
			int ecolor = R.color.black;
			ForegroundColorSpan fgcspan = new ForegroundColorSpan(ecolor);
			SpannableStringBuilder ssbuilder = new SpannableStringBuilder(
					errorMessage);
			ssbuilder.setSpan(fgcspan, 0, errorMessage.length(), 0);
			editText.setError(ssbuilder);
			editText.setBackgroundDrawable(context.getResources().getDrawable(
					R.drawable.edittext_error_background_selector_login));
			editText.setPadding(15, 5, 15, 5);
			editText.setTextSize(24);
			return false;
		}
	}

	// call this method when you need to check phone number validation
	public static boolean isPhoneNumber(EditText editText, boolean required) {
		return isValid(editText, PHONE_REGEX, PHONE_MSG, required);
	}

	// return true if the input field is valid, based on the parameter passed
	public static boolean isValid(EditText editText, String regex,
			String errorMessage, boolean required) {

		String text = editText.getText().toString().trim();
		// pattern doesn't match so returning false
		if (!Pattern.matches(regex, text)) {
			return false;
		}

		return true;
	}

	// check the input field has any text or not
	// return true if it contains text otherwise false
	public static boolean hasText(Context context, EditText editText,
			String errorMessage) {
		String text = editText.getText().toString().trim();

		if (text.length() == 0) {

			int ecolor = R.color.black;
			ForegroundColorSpan fgcspan = new ForegroundColorSpan(ecolor);
			SpannableStringBuilder ssbuilder = new SpannableStringBuilder(
					errorMessage);
			ssbuilder.setSpan(fgcspan, 0, errorMessage.length(), 0);
			editText.setError(ssbuilder);
			editText.setBackgroundDrawable(context.getResources().getDrawable(
					R.drawable.edittext_error_background_selector));

			editText.setPadding(15, 5, 15, 5);
			editText.setTextSize(24);

			return false;
		} else {
			editText.setBackgroundDrawable(context.getResources().getDrawable(
					R.drawable.edittext_valid_background_selector));
			editText.setError(null);
			editText.setPadding(15, 5, 15, 5);
			editText.setTextSize(24);
			editText.setTextColor(context.getResources()
					.getColor(R.color.Black));

			return true;
		}

	}

	public static boolean hasTextLogin(Context context, EditText editText,
			String errorMessage) {
		String text = editText.getText().toString().trim();

		if (text.length() == 0) {

			int ecolor = R.color.black;
			ForegroundColorSpan fgcspan = new ForegroundColorSpan(ecolor);
			SpannableStringBuilder ssbuilder = new SpannableStringBuilder(
					errorMessage);
			ssbuilder.setSpan(fgcspan, 0, errorMessage.length(), 0);
			editText.setError(ssbuilder);
			editText.setBackgroundDrawable(context.getResources().getDrawable(
					R.drawable.edittext_error_background_selector_login));

			editText.setPadding(15, 5, 15, 5);
			editText.setTextSize(24);

			return false;
		} else {
			editText.setBackgroundDrawable(context.getResources().getDrawable(
					R.drawable.edittext_valid_background_selector_login));
			editText.setError(null);
			editText.setPadding(15, 5, 15, 5);
			editText.setTextSize(24);
			editText.setTextColor(context.getResources()
					.getColor(R.color.Black));

			return true;
		}

	}

	public static boolean hasText(Context context, TextView editText,
			String errorMessage) {
		String text = editText.getText().toString().trim();

		if (text.length() == 0) {

			int ecolor = R.color.black;
			ForegroundColorSpan fgcspan = new ForegroundColorSpan(ecolor);
			SpannableStringBuilder ssbuilder = new SpannableStringBuilder(
					errorMessage);
			ssbuilder.setSpan(fgcspan, 0, errorMessage.length(), 0);
			editText.setError(ssbuilder);
			editText.setBackgroundDrawable(context.getResources().getDrawable(
					R.drawable.edittext_error_background_selector));

			editText.setPadding(15, 5, 15, 5);
			editText.setTextSize(24);

			return false;
		} else {
			editText.setBackgroundDrawable(context.getResources().getDrawable(
					R.drawable.edittext_valid_background_selector));
			editText.setError(null);
			editText.setPadding(15, 5, 15, 5);
			editText.setTextSize(24);
			editText.setTextColor(context.getResources()
					.getColor(R.color.Black));

			return true;
		}

	}

	// check the input mobile has length 10 character text or not
	// return true if it contains text otherwise false
	public static boolean isValidMobileNumber(Context context,
			EditText editText, String errorMessage) {
		String text = editText.getText().toString().trim();

		if (text.length() != 10) {
			int ecolor = R.color.black;
			ForegroundColorSpan fgcspan = new ForegroundColorSpan(ecolor);
			SpannableStringBuilder ssbuilder = new SpannableStringBuilder(
					errorMessage);
			ssbuilder.setSpan(fgcspan, 0, errorMessage.length(), 0);
			editText.setError(ssbuilder);
			editText.setBackgroundDrawable(context.getResources().getDrawable(
					R.drawable.edittext_error_background_selector));
			editText.setPadding(15, 5, 15, 5);
			editText.setTextSize(24);
			return false;
		} else {
			editText.setBackgroundDrawable(context.getResources().getDrawable(
					R.drawable.edittext_valid_background_selector));
			editText.setError(null);
			editText.setPadding(15, 5, 15, 5);
			editText.setTextSize(24);
			editText.setTextColor(context.getResources()
					.getColor(R.color.Black));
			return true;
		}

	}

	public static boolean isValidMobileNumberLogin(Context context,
			EditText editText, String errorMessage) {
		String text = editText.getText().toString().trim();

		if (text.length() != 10) {
			int ecolor = R.color.black;
			ForegroundColorSpan fgcspan = new ForegroundColorSpan(ecolor);
			SpannableStringBuilder ssbuilder = new SpannableStringBuilder(
					errorMessage);
			ssbuilder.setSpan(fgcspan, 0, errorMessage.length(), 0);
			editText.setError(ssbuilder);
			editText.setBackgroundDrawable(context.getResources().getDrawable(
					R.drawable.edittext_error_background_selector_login));
			editText.setPadding(15, 5, 15, 5);
			editText.setTextSize(24);
			return false;
		} else {
			editText.setBackgroundDrawable(context.getResources().getDrawable(
					R.drawable.edittext_valid_background_selector_login));
			editText.setError(null);
			editText.setPadding(15, 5, 15, 5);
			editText.setTextSize(24);
			editText.setTextColor(context.getResources()
					.getColor(R.color.Black));
			return true;
		}

	}

	// check the input mobile has length 10 character text or not
	// return true if it contains text otherwise false
	public static boolean isValidAmount(Context context, EditText editText,
			String errorMessage) {
		String text = editText.getText().toString().trim();

		if (text.equalsIgnoreCase("0")) {
			int ecolor = R.color.black;
			ForegroundColorSpan fgcspan = new ForegroundColorSpan(ecolor);
			SpannableStringBuilder ssbuilder = new SpannableStringBuilder(
					errorMessage);
			ssbuilder.setSpan(fgcspan, 0, errorMessage.length(), 0);
			editText.setError(ssbuilder);
			editText.setBackgroundDrawable(context.getResources().getDrawable(
					R.drawable.edittext_error_background_selector));
			editText.setPadding(15, 5, 15, 5);
			editText.setTextSize(24);
			return false;
		} else {
			editText.setBackgroundDrawable(context.getResources().getDrawable(
					R.drawable.edittext_valid_background_selector));
			editText.setError(null);
			editText.setPadding(15, 5, 15, 5);
			editText.setTextSize(24);
			editText.setTextColor(context.getResources()
					.getColor(R.color.Black));
			return true;
		}

	}

	// check the input mobile has length 10 character text or not
	// return true if it contains text otherwise false
	public static boolean isValidAmount(Context context, EditText editText,
			String errorMessage, double min, double max) {
		double amount = 0;
		try {
			amount = Integer.parseInt(editText.getText().toString().trim());
		} catch (Exception e) {
			amount = 0;
		}
		if (amount > min || amount < max) {
			int ecolor = R.color.black;
			ForegroundColorSpan fgcspan = new ForegroundColorSpan(ecolor);
			SpannableStringBuilder ssbuilder = new SpannableStringBuilder(
					errorMessage);
			ssbuilder.setSpan(fgcspan, 0, errorMessage.length(), 0);
			editText.setError(ssbuilder);
			editText.setBackgroundDrawable(context.getResources().getDrawable(
					R.drawable.edittext_error_background_selector));
			editText.setPadding(15, 5, 15, 5);
			editText.setTextSize(24);
			return false;
		} else {
			editText.setBackgroundDrawable(context.getResources().getDrawable(
					R.drawable.edittext_valid_background_selector));
			editText.setError(null);
			editText.setPadding(15, 5, 15, 5);
			editText.setTextSize(24);
			editText.setTextColor(context.getResources()
					.getColor(R.color.Black));
			return true;
		}

	}

	public static boolean isValidPin(Context context, EditText editText,
			String errorMessage) {
		String text = editText.getText().toString().trim();

		if (text.length() < 4) {
			int ecolor = R.color.black;
			ForegroundColorSpan fgcspan = new ForegroundColorSpan(ecolor);
			SpannableStringBuilder ssbuilder = new SpannableStringBuilder(
					errorMessage);
			ssbuilder.setSpan(fgcspan, 0, errorMessage.length(), 0);
			editText.setError(ssbuilder);
			editText.setBackgroundDrawable(context.getResources().getDrawable(
					R.drawable.edittext_error_background_selector));
			editText.setPadding(15, 5, 15, 5);
			editText.setTextSize(24);
			return false;
		} else {
			editText.setBackgroundDrawable(context.getResources().getDrawable(
					R.drawable.edittext_valid_background_selector));
			editText.setError(null);
			editText.setPadding(15, 5, 15, 5);
			editText.setTextSize(24);
			editText.setTextColor(context.getResources()
					.getColor(R.color.Black));
			return true;
		}

	}

	public static boolean isValidPinLogin(Context context, EditText editText,
			String errorMessage) {
		String text = editText.getText().toString().trim();

		if (text.length() < 4) {
			int ecolor = R.color.black;
			ForegroundColorSpan fgcspan = new ForegroundColorSpan(ecolor);
			SpannableStringBuilder ssbuilder = new SpannableStringBuilder(
					errorMessage);
			ssbuilder.setSpan(fgcspan, 0, errorMessage.length(), 0);
			editText.setError(ssbuilder);
			editText.setBackgroundDrawable(context.getResources().getDrawable(
					R.drawable.edittext_error_background_selector_login));
			editText.setPadding(15, 5, 15, 5);
			editText.setTextSize(24);
			return false;
		} else {
			editText.setBackgroundDrawable(context.getResources().getDrawable(
					R.drawable.edittext_valid_background_selector_login));
			editText.setError(null);
			editText.setPadding(15, 5, 15, 5);
			editText.setTextSize(24);
			editText.setTextColor(context.getResources()
					.getColor(R.color.Black));
			return true;
		}

	}

}