﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Input;
using System.Windows.Media;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Net.Http;
using Microsoft.Phone.Notification;
using System.Collections.ObjectModel;
using System.Device.Location;
using System.ComponentModel;
using Microsoft.Phone.Tasks;

namespace NewPay1B2C
	{
	public partial class RegistrationLayout : PhoneApplicationPage
		{
		ProgressBarManager ProgressBarManager;
		GeoCoordinateWatcher watcher;
		 public static string latti, longi;
		// public static string currentAddress;
		public RegistrationLayout ()
			{
			InitializeComponent ();


			GoogleAnalytics.EasyTracker.GetTracker ().SendView ("RegistrationLayout");
			ProgressIndicator prog = new ProgressIndicator ();
			ProgressBarManager = new ProgressBarManager ();
			SystemTray.SetProgressIndicator (this, prog);
			ProgressBarManager.Hook (prog);
			getPushUrl ();

			}


		private void StartBackGroundWOrker ()
			{
			BackgroundWorker backroungWorker = new BackgroundWorker ();
			backroungWorker.DoWork += new DoWorkEventHandler (backroungWorker_DoWork);
			//backroungWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(backroungWorker_RunWorkerCompleted);

			backroungWorker.RunWorkerAsync ();
			}
		private void backroungWorker_DoWork (object sender, DoWorkEventArgs e)
			{

			if (watcher == null)
				{
				watcher = new GeoCoordinateWatcher (GeoPositionAccuracy.High); // Use high accuracy. 
				watcher.MovementThreshold = 20; // Use MovementThreshold to ignore noise in the signal. 
				watcher.StatusChanged += new EventHandler<GeoPositionStatusChangedEventArgs> (watcher_StatusChanged);
				if (watcher.Permission == GeoPositionPermission.Denied)
					{
					//Location services is disable on the phone. Show a message to the user.
					MessageBox.Show ("Your location is not enabled.");

					}
				else
					{
					watcher.Start ();
					}
				}
			}


		private void watcher_StatusChanged (object sender, GeoPositionStatusChangedEventArgs e)
			{
			if (e.Status == GeoPositionStatus.Ready)
				{
				// Use the Position property of the GeoCoordinateWatcher object to get the current location. 
				GeoCoordinate co = watcher.Position.Location;
				latti = co.Latitude.ToString ("0.000");
				longi = co.Longitude.ToString ("0.000");
				Constant.SavePersistent (Constant.SETTINGS_USER_LATTITUDE, latti);
				Constant.SavePersistent (Constant.SETTINGS_USER_LONGITUDE, longi);
				
				watcher.Stop ();
				}
			}



		void textMouseEnter (object sender, MouseEventArgs e)
			{

			NavigationService.Navigate (new Uri ("/TermsAndCondition.xaml?", UriKind.RelativeOrAbsolute));
			PhoneApplicationService.Current.State["isTermsAndCondition"] = "1";
			PhoneApplicationService.Current.State[Constant.IS_PROFILE] = true;
			}


		void textPrivacyMouseEnter (object sender, MouseEventArgs e)
			{
			NavigationService.Navigate (new Uri ("/TermsAndCondition.xaml?", UriKind.RelativeOrAbsolute));
			PhoneApplicationService.Current.State["isTermsAndCondition"] = "2";
			PhoneApplicationService.Current.State[Constant.IS_PROFILE] = true;
			}

		private void btnRegister_Click (object sender, RoutedEventArgs e)
			{
			String textMobile = textMobileNumber.Text;
			if (textMobile.Length == 0)
				{
				MessageBox.Show ("Please enter mobile number");
				textMobileNumber.BorderBrush = new SolidColorBrush (Colors.Red);
				}
			else
				{
				if (textMobile.Length != 10)
					{
					MessageBox.Show ("Please enter correct mobile number");
					textMobileNumber.BorderBrush = new SolidColorBrush (Colors.Red);
					}
				else
					{
					RegisterTask (textMobile);
					}
				}
			}

		private async void RegisterTask (string textMobile)
			{
			PhoneApplicationService.Current.State["loginCall"] = 2;
			ProgressBarManager.Show (1, "Please wait...", false, 10);
			var values = new List<KeyValuePair<string, string>>
                        { 
                        new KeyValuePair<string, string>("actiontype", "create_user"),
                        new KeyValuePair<string, string>("mobile_number",textMobile),
                       new KeyValuePair<string, string>("api","true"),
                        };

			FormUrlEncodedContent values1 = new FormUrlEncodedContent (values);
			Task<string> response = Constant.postParamsAndgetResponse (Constant.B2C_URL, values1);
			string result = await response;
			if (string.IsNullOrEmpty (result))
				{
				NavigationService.Navigate (new Uri ("/NoConnection.xaml?", UriKind.Relative));
				}
			else
				{
				var dict = (JObject)JsonConvert.DeserializeObject (result);
				string status = dict["status"].ToString ();
				if (status.Equals ("success"))
					{
					Constant.SavePersistent (Constant.SETTINGS_IS_NEW, true);
					var description = dict["description"];
					Constant.setOtpHash (description["otp"].ToString ());
					Constant.setUserNumber (textMobile);
					Constant.setLastTransaction (description["recent_pay1_transactions"].ToString ());
					Constant.setbalance (description["balance"].ToString ());
					NavigationService.Navigate (new Uri ("/EnterPin.xaml?", UriKind.RelativeOrAbsolute));
					NavigationService.RemoveBackEntry ();
					}
				else if (status.Equals ("failure"))
					{
					String errCode = dict["errCode"].ToString ();
					var description = dict["description"];
					if (errCode.Equals ("202"))
						{
						Constant.SavePersistent (Constant.SETTINGS_IS_NEW, false);
						Constant.setOtpHash (description["otp"].ToString ());
						Constant.setUserNumber (textMobile);
						Constant.setLastTransaction (description["recent_pay1_transactions"].ToString ());
						Constant.setbalance (description["balance"].ToString ());
						NavigationService.Navigate (new Uri ("/EnterPin.xaml?", UriKind.RelativeOrAbsolute));
						NavigationService.RemoveBackEntry ();
						}
					else
						{
						}
					}
				}
			ProgressBarManager.Hide (1);
			}


		string channelName = "Pay1B2cChannel";

		HttpNotificationChannel pushChannel;
		public static string MPNSString;
		public static Collection<Uri> allowedDomains =
		  new Collection<Uri> { 
                                                new Uri("http://b2c.pay1.in"), 
                                                new Uri("http://cdev.pay1.in") 
                                              };


		private void getPushUrl ()
			{
			try
				{
				pushChannel = HttpNotificationChannel.Find (channelName);

				if (pushChannel == null)
					{
					pushChannel = new HttpNotificationChannel (channelName);

					pushChannel.ChannelUriUpdated += new EventHandler<NotificationChannelUriEventArgs> (PushChannel_ChannelUriUpdated);
					pushChannel.ErrorOccurred += new EventHandler<NotificationChannelErrorEventArgs> (PushChannel_ErrorOccurred);
					pushChannel.ShellToastNotificationReceived += new EventHandler<NotificationEventArgs> (PushChannel_ShellToastNotificationReceived);
					pushChannel.Open ();



					//pushChannel.BindToShellTile();

					pushChannel.BindToShellToast ();
					//pushChannel.BindToShellTile(allowedDomains);

					}
				else
					{
					pushChannel.ChannelUriUpdated += new EventHandler<NotificationChannelUriEventArgs> (PushChannel_ChannelUriUpdated);
					pushChannel.ErrorOccurred += new EventHandler<NotificationChannelErrorEventArgs> (PushChannel_ErrorOccurred);

					pushChannel.ShellToastNotificationReceived += new EventHandler<NotificationEventArgs> (PushChannel_ShellToastNotificationReceived);

					pushChannel.BindToShellTile (allowedDomains);
					System.Diagnostics.Debug.WriteLine (pushChannel.ChannelUri.ToString ());
					/* MessageBox.Show(String.Format("Channel Uri is {0}",
						 pushChannel.ChannelUri.ToString()));*/

					MPNSString = pushChannel.ChannelUri.ToString ();
					Constant.SavePersistent (Constant.NOTIFICATION_URL, MPNSString);
					// MessageBox.Show(MPNSString);

					}
				}
			catch (Exception e)
				{
				//Splash_Screen();
				//Console.WriteLine("dfghf");
				}
			}



		void PushChannel_ChannelUriUpdated (object sender, NotificationChannelUriEventArgs e)
			{
			Dispatcher.BeginInvoke (() =>
			{
				System.Diagnostics.Debug.WriteLine (e.ChannelUri.ToString ());

				MPNSString = e.ChannelUri.ToString ();
				Constant.SavePersistent (Constant.NOTIFICATION_URL, MPNSString);
				//MessageBox.Show(MPNSString);
			});
			}


		void PushChannel_ErrorOccurred (object sender, NotificationChannelErrorEventArgs e)
			{
			// Error handling logic for your particular application would be here.
			Dispatcher.BeginInvoke (() =>
				MessageBox.Show (String.Format ("A push notification {0} error occurred.  {1} ({2}) {3}",
					e.ErrorType, e.Message, e.ErrorCode, e.ErrorAdditionalData))
					);
			}

		void PushChannel_ShellToastNotificationReceived (object sender, NotificationEventArgs e)
			{
			Dispatcher.BeginInvoke (() => MessageBox.Show (e.Collection["wp:Text1"] + "\n" + e.Collection["wp:Text2"]));
			string Text1 = e.Collection["wp:Text1"];
			string Text2 = e.Collection["wp:Text2"];
			string Text3 = e.Collection["wp:Text3"];
			string Text4 = e.Collection["wp:Text4"];
			if (Text4.Equals ("1"))
				{
				}
			}



		}
	}