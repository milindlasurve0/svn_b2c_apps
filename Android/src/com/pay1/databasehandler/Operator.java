package com.pay1.databasehandler;

public class Operator {

	private int oID;
	private int operatorID;
	private String operatorCode;
	private int operatorProductID;
	private String operatorName;
	private int operatorServiceID;
	private int operatorFlag;
	private int operatorDelegate;
	private int operatorSTV;
	private int operatorType;
	private long operatorUptateTime;
	private int operatorMaxAmount;
	private int operatorMinAmount;
	private String operatorChargeSlab;
	private int operatorServiceChargeAmount;
	private int operatorServiceChargePercent;
	private int operatorServiceTaxPercent;

	public int getOID() {
		return oID;
	}

	public void setOID(int oID) {
		this.oID = oID;
	}

	public int getOperatorID() {
		return operatorID;
	}

	public void setOperatorID(int operatorID) {
		this.operatorID = operatorID;
	}

	public String getOperatorCode() {
		return operatorCode;
	}

	public void setOperatorCode(String operatorCode) {
		this.operatorCode = operatorCode;
	}

	public int getOperatorProductID() {
		return operatorProductID;
	}

	public void setOperatorProductID(int operatorProductID) {
		this.operatorProductID = operatorProductID;
	}

	public String getOperatorName() {
		return operatorName;
	}

	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}

	public int getOperatorServiceID() {
		return operatorServiceID;
	}

	public void setOperatorServiceID(int operatorServiceID) {
		this.operatorServiceID = operatorServiceID;
	}

	public int getOperatorFlag() {
		return operatorFlag;
	}

	public void setOperatorFlag(int operatorFlag) {
		this.operatorFlag = operatorFlag;
	}

	public int getOperatorDelegate() {
		return operatorDelegate;
	}

	public void setOperatorDelegate(int operatorDelegate) {
		this.operatorDelegate = operatorDelegate;
	}

	public int getOperatorSTV() {
		return operatorSTV;
	}

	public void setOperatorSTV(int operatorSTV) {
		this.operatorSTV = operatorSTV;
	}

	public int getOperatorType() {
		return operatorType;
	}

	public void setOperatorType(int operatorType) {
		this.operatorType = operatorType;
	}

	public long getOperatorUptateTime() {
		return operatorUptateTime;
	}

	public void setOperatorUptateTime(long operatorUptateTime) {
		this.operatorUptateTime = operatorUptateTime;
	}

	public int getOperatorMaxAmount() {
		return operatorMaxAmount;
	}

	public void setOperatorMaxAmount(int operatorMaxAmount) {
		this.operatorMaxAmount = operatorMaxAmount;
	}

	public int getOperatorMinAmount() {
		return operatorMinAmount;
	}

	public void setOperatorMinAmount(int operatorMinAmount) {
		this.operatorMinAmount = operatorMinAmount;
	}

	public String getOperatorChargeSlab() {
		return operatorChargeSlab;
	}

	public void setOperatorChargeSlab(String operatorChargeSlab) {
		this.operatorChargeSlab = operatorChargeSlab;
	}

	public int getOperatorServiceChargeAmount() {
		return operatorServiceChargeAmount;
	}

	public void setOperatorServiceChargeAmount(int operatorServiceChargeAmount) {
		this.operatorServiceChargeAmount = operatorServiceChargeAmount;
	}

	public int getOperatorServiceChargePercent() {
		return operatorServiceChargePercent;
	}

	public void setOperatorServiceChargePercent(int operatorServiceChargePercent) {
		this.operatorServiceChargePercent = operatorServiceChargePercent;
	}

	public int getOperatorServiceTaxPercent() {
		return operatorServiceTaxPercent;
	}

	public void setOperatorServiceTaxPercent(int operatorServiceTaxPercent) {
		this.operatorServiceTaxPercent = operatorServiceTaxPercent;
	}

}
