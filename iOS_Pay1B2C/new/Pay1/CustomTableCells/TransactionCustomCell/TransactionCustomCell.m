//
//  TransactionCustomCell.m
//  Pay1
//
//  Created by webninjaz on 09/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "TransactionCustomCell.h"

@implementation TransactionCustomCell



- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
    NSArray *tblView=[[NSBundle mainBundle]loadNibNamed:@"TransactionCustomCell" owner:self options:nil];
    self=[tblView objectAtIndex:0];
    }
    return self;
}

- (void)awakeFromNib {
    
   
    
    
    if([UIScreen mainScreen].bounds.size.width > 320){
    _transactionCorrectImageView.frame = CGRectMake([UIScreen mainScreen].bounds.size.width - 45, 23, 35, 35);
    _transactionMoneyLabel.frame = CGRectMake(_transactionCorrectImageView.frame.origin.x - 70, 50, 32, 21);
    }

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
