﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Pay1B2C.UserControls;
using Microsoft.Phone.Tasks;

namespace Pay1B2C.Views
    {
    public partial class AccountAndSupport: PhoneApplicationPage
        {
        List<string> profileList;
        ProgressBarManager ProgressBarManager;
        public AccountAndSupport()
            {
            InitializeComponent();
            Constant.laodNavigationBar(TitlePanel, LayoutRoot, canvas, "Account & Support");

            GoogleAnalytics.EasyTracker.GetTracker().SendView("User  Account And Support");
            PhoneApplicationService.Current.State[Constant.IS_PROFILE] = true;
            //Constant.laodNavigationBar (TitlePanel, Container, ListSideBar);
            profileList = new List<string>();
            loadProfileList();

            ProgressIndicator prog = new ProgressIndicator();
            ProgressBarManager = new ProgressBarManager();
            SystemTray.SetProgressIndicator(this, prog);
            ProgressBarManager.Hook(prog);
            }

        private void loadProfileList()
            {
            profileList.Add("My Profile");
            profileList.Add("Support");
            profileList.Add("FAQs");
            profileList.Add("Privacy Policy");
            profileList.Add("Terms & Condition");
            profileList.Add("Rate Us");

            foreach(string item in profileList)
                {
                ProfileUserControl profileItem = new ProfileUserControl();
                profileItem.textProfileItem.Text = item;
                listProfile.Items.Add(profileItem);
                }
            }


        private void profileListSelection(object sender, EventArgs e)
            {
            if(listProfile.SelectedIndex == -1) return;

            switch(listProfile.SelectedIndex)
                {
                case 0: (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri("/Views/Profile.xaml?", UriKind.RelativeOrAbsolute));
                    break;
                case 1: (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri("/Views/Support.xaml?", UriKind.RelativeOrAbsolute));
                    break;
                case 2: (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri("/Views/TermsAndCondition.xaml?", UriKind.RelativeOrAbsolute));
                    PhoneApplicationService.Current.State["isTermsAndCondition"] = 3;

                    break;
                case 3:
                    (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri("/Views/TermsAndCondition.xaml?", UriKind.RelativeOrAbsolute));
                    PhoneApplicationService.Current.State["isTermsAndCondition"] = 2;

                    break;
                case 4: (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri("/Views/TermsAndCondition.xaml?", UriKind.RelativeOrAbsolute));
                    PhoneApplicationService.Current.State["isTermsAndCondition"] = 1;

                    break;
                case 5: if(MessageBox.Show("Rate this app?", "Pay1", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
                        {

                        var a = new MarketplaceReviewTask();
                        a.Show();
                        }
                    break;
                }
            }

        private async void LogOutTask()
            {
            ProgressBarManager.Show(18, "Please wait...", false, 10);
            var values = new List<KeyValuePair<string, string>>();
            values.Add(new KeyValuePair<string, string>("actiontype", "signout"));

            values.Add(new KeyValuePair<string, string>("api", "true"));
            FormUrlEncodedContent values1 = new FormUrlEncodedContent(values);
            Task<string> response = Constant.postParamsAndgetResponse(Constant.B2C_URL, values1);

            string result = await response;
            if(string.IsNullOrEmpty(result))
                {
                NavigationService.Navigate(new Uri("/NoConnection.xaml?", UriKind.Relative));
                }
            else
                {
                var dict = (JObject)JsonConvert.DeserializeObject(result);
                string status = dict["status"].ToString();
                var description = dict["description"];
                if(status.Equals("success"))
                    {
                    // MessageBox.Show(description.ToString());
                    Constant.SavePersistent(Constant.SETTINGS_IS_LOGIN, false);
                    Constant.SavePersistent(Constant.SETTINGS_USER_WALLET_BALANCE, "0.00");
                    /*NavigationService.Navigate(new Uri("/HomePage.xaml?", UriKind.Relative));
                    NavigationService.RemoveBackEntry();*/
                    Constant.SavePersistent("cookieName", "111");
                    Constant.SavePersistent("cookieValue", "111");
                    NavigationService.Navigate(new Uri("/Login.xaml?", UriKind.Relative));
                    while(this.NavigationService.BackStack.Count() > 0)
                        {
                        this.NavigationService.RemoveBackEntry();
                        }
                    }
                else if(status.Equals("failure"))
                    {
                    MessageBox.Show(description.ToString());
                    }
                }
            ProgressBarManager.Hide(18);
            }
        }
    }