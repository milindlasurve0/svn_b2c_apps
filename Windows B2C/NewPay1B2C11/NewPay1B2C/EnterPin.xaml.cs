﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Threading;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using SQLiteClient;

using NewPay1B2C.Model;
using Microsoft.Phone.Info;
using System.ComponentModel;
using System.Collections.ObjectModel;

namespace NewPay1B2C
	{
	public partial class EnterPin : PhoneApplicationPage
		{
		ProgressBarManager ProgressBarManager;
		SQLiteConnection db = null;
		List<OperatorDB> queryResult = null;
		public EnterPin ()
			{
			InitializeComponent ();
			timer1.Interval = new TimeSpan (0, 0, 0, 1);
			timer1.Tick += new EventHandler (timer1_Tick);

			//CreateDatabase ();
			GoogleAnalytics.EasyTracker.GetTracker ().SendView ("EnterPin");
			ProgressIndicator prog = new ProgressIndicator ();
			ProgressBarManager = new ProgressBarManager ();
			SystemTray.SetProgressIndicator (this, prog);
			ProgressBarManager.Hook (prog);
			btnOtp.Tap += btnRegister_Click;
			btnResendOtp.Tap += btnResendOtp_Click;
			
			textUserMobileNumber.Text = Constant.getUserNumber ();
			if (Constant.LoadPersistent<Boolean> (Constant.SETTINGS_IS_NEW))
				{
				textPinHint.Text = "OTP";
				Countdown.Visibility = Visibility.Visible;
				timer1.Start ();
				btnOtp.Content = "Verify";
				textNewOrOldMsg.Text = "You will receive one time passward (OTP) via SMS. Kindly enter below";
				}
			else
				{
				textPinHint.Text = "Password";
				Countdown.Visibility = Visibility.Collapsed;
				btnResendOtp.Visibility = Visibility.Visible;
				btnResendOtp.Text = "Forgot Password";
				btnOtp.Content = "Login";
				textNewOrOldMsg.Text = "Kindly enter your password below";
				}
			
				StartLoadingData ();
				textPinHint.MouseEnter += (s, e) =>
				{
					textPinHint.Visibility = Visibility.Collapsed;
					textOTP.Focus ();
				};
			}


		private void StartLoadingData ()
			{
			BackgroundWorker backroungWorker = new BackgroundWorker ();
			backroungWorker.DoWork += new DoWorkEventHandler (backroungWorker_DoWork);
			//backroungWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(backroungWorker_RunWorkerCompleted);

			backroungWorker.RunWorkerAsync ();
			}

		private void passGotFocus (object sender, RoutedEventArgs e)
			{
			textPinHint.Visibility = Visibility.Collapsed;

			}
		private void paasLostFocus (object sender, RoutedEventArgs e)
			{
			PasswordBox tb = (PasswordBox)sender;
			//tb.BorderBrush = new SolidColorBrush(Colors.Transparent);
			if (tb.Password.Length > 0)
				{
				textPinHint.Visibility = Visibility.Collapsed;
				}
			else
				{
				textPinHint.Visibility = Visibility.Visible;
				}
			}
        


		private void backroungWorker_DoWork (object sender, DoWorkEventArgs e)
			{
            //if (Constant.checkInternetConnection ())
            //    {
            //    //getDealsTask (RegistrationLayout.latti, RegistrationLayout.longi);
            //    GetAndSaveOperator ();
            //    //getAllCircles ();
            //    }
            //else
            //    {
            //    MessageBox.Show ("No internet connection. \n Please check your connection.");
            //    }
						
			}



	/*	private async void getAllCircles ()
			{
			DateTime lastStorageCircle = Constant.LoadPersistent<DateTime> (Constant.CIRCLE_INSERT_TIME);
			// Use current time
			string format = "yyyy-MM-dd HH:mm:ss";    // Use this format
			Console.WriteLine (lastStorageCircle.ToString (format));
			String lastUrl = Constant.B2B_URL+"method=getMobileDetails&mobile=all&timestamp=" +lastStorageCircle.ToString (format);
			var httpClient = new HttpClient (new HttpClientHandler ());
			HttpResponseMessage response = await httpClient.PostAsync (lastUrl, null);
			response.EnsureSuccessStatusCode ();
			long cl = (long)response.Content.Headers.ContentLength;
			var responseString = await response.Content.ReadAsStringAsync ();
			int count = 0;
			try
				{
				db.Open ();


				string output = responseString.Remove (responseString.Length - 1, 1);
				string output1 = output.Remove (output.Length - 1, 1);
				string output2 = output1.Remove (0, 1);
				string output3 = output2.Remove (0, 1);
				string output4 = output3.Remove (output3.Length - 1, 1);
				NumberAndCircle numberAndCircle = new NumberAndCircle ();
				var dict = (JObject)JsonConvert.DeserializeObject (output4);
				string status = dict["status"].ToString ();
				int inseted = 0;
				var details = dict["details"];
				foreach (var item in details)
					{
					inseted = inseted + 1;
					numberAndCircle.area = item["area"].ToString ();
					numberAndCircle.area_name = item["area_name"].ToString ();
					numberAndCircle.number = item["number"].ToString ();
					numberAndCircle.numberOperator = item["operator"].ToString ();
					numberAndCircle.opr_name = item["opr_name"].ToString ();
					numberAndCircle.product_id = item["product_id"].ToString ();
					try
						{

						string strInsert = " Insert into NumberAndCircle (area_name,area,opr_name,numberOperator,number,product_id) values (@area_name,@area,@opr_name,@numberOperator,@number,@product_id)";
						int rec = (Application.Current as App).dbh.Insert<NumberAndCircle> (numberAndCircle, strInsert);

						}
					catch (Exception ex)
						{
						string ms = ex.Message;
						continue;
						}
					}

				Constant.SavePersistent (Constant.CIRCLE_INSERT_TIME, DateTime.Now);
				System.Diagnostics.Debug.WriteLine ("Inserted  Circle  " + inseted);

				}
			catch (JsonException je)
				{
				}
			}

			   */
	
		DispatcherTimer timer1 = new DispatcherTimer ();

		int tik = 178;
		void timer1_Tick (object sender, EventArgs e)
			{
			int hours = (int)(tik / 3600);
			int minutes = (int)(tik / 60) % 60;
			int seconds = (int)tik % 60;
			string sec, min;
			if (seconds < 10)
				{
				sec = "0" + seconds;
				}
			else
				{
				sec = "" + seconds;
				}
			if (minutes < 10)
				{
				min = "0" + minutes;
				}
			else
				{
				min = "" + minutes;
				}

			string res = min + ":" + sec;

			Countdown.Text = res + "";
			if (tik > 0)
				tik--;
			else
				{

				Countdown.Text = "00:00";
				Countdown.Text = "Looks like there is some problem in sending SMS, try re-sending it.";
				btnResendOtp.Visibility = Visibility.Visible;
				}
			}

		private void btnRegister_Click (object sender, RoutedEventArgs e)
			{
			String strOtp = textOTP.Password;
			String hashNumber = Constant.EncodeString (Constant.getUserNumber ());
			String hashOtp = Constant.EncodeString (strOtp);
			String serverOtp = Constant.getOtpHash ();
			if ((hashNumber + hashOtp).Equals (serverOtp))
				{
				Constant.setOtp (strOtp);
				Constant.SavePersistent (Constant.SETTINGS_IS_FIRST, true);
				if (Constant.LoadPersistent<Boolean> (Constant.SETTINGS_IS_NEW))
					{
					UpdatePasswordTask (strOtp);
					}
				else
					{
					autoLogin (strOtp);
					}
				Constant.SavePersistent ("Prefilled", true);
				string str_select = "SELECT * from QuickPayAllDataTable";
				ObservableCollection<QuickPayAllDataTable> freeBieList = (Application.Current as App).dbh.SelectObservableCollection<QuickPayAllDataTable> (str_select);
				int totalTransaction = freeBieList.Count;
				PhoneApplicationService.Current.State["data"] = "2";
				if (totalTransaction != 0)
					{
					NavigationService.Navigate (new Uri ("/HomePage.xaml?", UriKind.RelativeOrAbsolute));
					}
				else 
					{
					NavigationService.Navigate (new Uri ("/RechargeAndBills.xaml?", UriKind.RelativeOrAbsolute));
					}
				NavigationService.RemoveBackEntry ();
				}
			else
				{
				MessageBox.Show ("Invalid  Password");
				}
			}

		private async void UpdatePasswordTask (string strOtp)
			{
			var values = new List<KeyValuePair<string, string>>
                        { 
                        new KeyValuePair<string, string>("actiontype", "update_password"),
                        new KeyValuePair<string, string>("mobile_number",Constant.getUserNumber()),
                        new KeyValuePair<string, string>("password", strOtp),
                        new KeyValuePair<string, string>("confirm_password",strOtp),
                        new KeyValuePair<string, string>("code", strOtp),
                        
                       new KeyValuePair<string, string>("api","true"),
                        };

			FormUrlEncodedContent values1 = new FormUrlEncodedContent (values);
			Task<string> response = Constant.postParamsAndgetResponse (Constant.B2C_URL, values1);
			string result = await response;
			if (string.IsNullOrEmpty (result))
				{
				NavigationService.Navigate (new Uri ("/NoConnection.xaml?", UriKind.Relative));
				}
			else
				{
				var dict = (JObject)JsonConvert.DeserializeObject (result);
				string status = dict["status"].ToString ();
				if (status.Equals ("success"))
					{
					autoLogin (strOtp);
					}
				else
					{
					Constant.checkForErrorCode (result);
					}
				} 
			}


		private async void autoLogin (String strOtp)
			{
			ProgressBarManager.Show (11, "Please wait...", false, 10);
			PhoneApplicationService.Current.State["loginCall"] = 1;
			var values = new List<KeyValuePair<string, string>>
                        { 
                        new KeyValuePair<string, string>("actiontype", "signin"),
                        new KeyValuePair<string, string>("username",Constant.getUserNumber()),
                        new KeyValuePair<string, string>("password",strOtp),
                        new KeyValuePair<string, string>("uuid", Constant.getUUID()),
                        new KeyValuePair<string, string>("latitude",Constant.LoadPersistent<string>(Constant.SETTINGS_USER_LATTITUDE)),
                        new KeyValuePair<string, string>("longitude",Constant.LoadPersistent<string>(Constant.SETTINGS_USER_LONGITUDE)),
                        new KeyValuePair<string, string>("device_type","windows"),
                        new KeyValuePair<string, string>("manufacturer",DeviceStatus.DeviceManufacturer),
                        new KeyValuePair<string, string>("os_info",Environment.OSVersion.Version.ToString()),
                        new KeyValuePair<string, string>("gcm_reg_id",Constant.LoadPersistent<string>(Constant.NOTIFICATION_URL)),
                        new KeyValuePair<string, string>("api","true"),
                        };

			FormUrlEncodedContent values1 = new FormUrlEncodedContent (values);
			Task<string> response = Constant.postParamsAndgetResponse (Constant.B2C_URL, values1);

			string result = await response;
			if (string.IsNullOrEmpty (result))
				{
				NavigationService.Navigate (new Uri ("/NoConnection.xaml?", UriKind.Relative));
				}
			else
				{
				var dict = (JObject)JsonConvert.DeserializeObject (result);
				string status = dict["status"].ToString ();
				if (status.Equals ("success"))
					{
					var description = dict["description"];
					Constant.SavePersistent (Constant.SETTINGS_USER_NAME, description["name"].ToString ());
					Constant.SavePersistent (Constant.SETTINGS_USER_PIN, strOtp);
					Constant.setUserNumber (description["mobile"].ToString ());
					Constant.SavePersistent (Constant.SETTINGS_USER_GENDER, description["gender"].ToString ());
					Constant.SavePersistent (Constant.SETTINGS_USER_EMAIL, description["email"].ToString ());
					Constant.SavePersistent (Constant.SETTINGS_USER_ID, description["user_id"].ToString ());
					Constant.SavePersistent (Constant.SETTINGS_USER_DOB, description["date_of_birth"].ToString ());
					Constant.setbalance (description["wallet_balance"].ToString ());
					Constant.SavePersistent (Constant.SETTINGS_USER_LATTITUDE, description["latitude"].ToString ());
					Constant.SavePersistent (Constant.SETTINGS_USER_LONGITUDE, description["longitude"].ToString ());
					Constant.SavePersistent ("session_id", description["session_id"].ToString ());
					Constant.SavePersistent ("session_name", description["session_name"].ToString ());
					Constant.SavePersistent (Constant.SETTINGS_USER_SUPPORT_NUMBER, description["support_number"].ToString ());
					Constant.SavePersistent (Constant.SETTINGS_IS_LOGIN, true);
					//NavigationService.Navigate (new Uri ("/HomePage.xaml?", UriKind.RelativeOrAbsolute));

					//NavigationService.RemoveBackEntry ();
					}
				else
					{
					Constant.checkForErrorCode (result);
					}

				}
			ProgressBarManager.Hide (11);
			}




		private async void RegisterTask ()
			{
			// ProgressBarManager.Show(11, "Please wait...", false, 10);
			var values = new List<KeyValuePair<string, string>>
                        { 
                        new KeyValuePair<string, string>("actiontype", "create_user"),
                        new KeyValuePair<string, string>("mobile_number",Constant.getUserNumber()),
                       new KeyValuePair<string, string>("api","true"),
                        };

			FormUrlEncodedContent values1 = new FormUrlEncodedContent (values);
			Task<string> response = Constant.postParamsAndgetResponse (Constant.B2C_URL, values1);
			string result = await response;
			if (string.IsNullOrEmpty (result))
				{
				NavigationService.Navigate (new Uri ("/NoConnection.xaml?", UriKind.Relative));
				}
			else
				{
				var dict = (JObject)JsonConvert.DeserializeObject (result);
				string status = dict["status"].ToString ();
				if (status.Equals ("success"))
					{
					var description = dict["description"];
					Constant.setOtpHash (description["otp"].ToString ());
					Constant.setUserNumber (Constant.getUserNumber ());
					Constant.setbalance (description["balance"].ToString ());
					NavigationService.Navigate (new Uri ("/EnterPin.xaml?", UriKind.RelativeOrAbsolute));
					NavigationService.RemoveBackEntry ();
					}
				else if (status.Equals ("failure"))
					{
					String errCode = dict["errCode"].ToString ();
					var description = dict["description"];
					if (errCode.Equals ("202"))
						{
						Constant.setOtpHash (description["otp"].ToString ());
						Constant.setUserNumber (Constant.getUserNumber ());
						Constant.setbalance (description["balance"].ToString ());
						NavigationService.Navigate (new Uri ("/EnterPin.xaml?", UriKind.RelativeOrAbsolute));
						NavigationService.RemoveBackEntry ();
						}
					else
						{
						}
					}
				}
			}

		private void btnResendOtp_Click (object sender, RoutedEventArgs e)
			{
			
			if (Constant.LoadPersistent<Boolean> (Constant.SETTINGS_IS_NEW))
				{
				RegisterTask ();
				}
			else
				{
				ForgotPasswordTask ();
				}

			}

		private async void ForgotPasswordTask ()
			{
			var values = new List<KeyValuePair<string, string>>
                        { 
                        new KeyValuePair<string, string>("actiontype", "forgotpwd"),
                        new KeyValuePair<string, string>("user_name",Constant.getUserNumber()),
                        new KeyValuePair<string, string>("vtype", "1"),
						new KeyValuePair<string, string>("api","true"),
                        };

			FormUrlEncodedContent values1 = new FormUrlEncodedContent (values);
			Task<string> response = Constant.postParamsAndgetResponse (Constant.B2C_URL, values1);
			string result = await response;
			if (string.IsNullOrEmpty (result))
				{
				NavigationService.Navigate (new Uri ("/NoConnection.xaml?", UriKind.RelativeOrAbsolute));
				}
			else
				{
				var dict = (JObject)JsonConvert.DeserializeObject (result);
				string status = dict["status"].ToString ();
				if (status.Equals ("success"))
					{
					PhoneApplicationService.Current.State["updateFromLogin"] = 1;
					NavigationService.Navigate (new Uri ("/UpdatePassword.xaml?", UriKind.RelativeOrAbsolute));
					}
				else
					{
					Constant.checkForErrorCode (result);
					}
				}
			}


		private void editEmailClick (object sender, RoutedEventArgs e)
			{
			NavigationService.Navigate (new Uri ("/RegistrationLayout.xaml?", UriKind.RelativeOrAbsolute));
			NavigationService.RemoveBackEntry ();
			}

		}
	}