package com.pay1.model;

public class GiftItem {

	private int offerID;
	private String offerName;
	private String url;
	private int loyaltyPoints;

	public GiftItem() {
	}

	public GiftItem(int offerID, String offerName, String url, int loyaltyPoints) {
		this.offerID = offerID;
		this.offerName = offerName;
		this.url = url;
		this.loyaltyPoints = loyaltyPoints;
	}

	/**
	 * @return the offerID
	 */
	public final int getOfferID() {
		return offerID;
	}

	/**
	 * @param offerID
	 *            the offerID to set
	 */
	public final void setOfferID(int offerID) {
		this.offerID = offerID;
	}

	/**
	 * @return the offerName
	 */
	public final String getOfferName() {
		return offerName;
	}

	/**
	 * @param offerName
	 *            the offerName to set
	 */
	public final void setOfferName(String offerName) {
		this.offerName = offerName;
	}

	/**
	 * @return the url
	 */
	public final String getUrl() {
		return url;
	}

	/**
	 * @param url
	 *            the url to set
	 */
	public final void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the loyaltyPoints
	 */
	public final int getLoyaltyPoints() {
		return loyaltyPoints;
	}

	/**
	 * @param loyaltyPoints
	 *            the loyaltyPoints to set
	 */
	public final void setLoyaltyPoints(int loyaltyPoints) {
		this.loyaltyPoints = loyaltyPoints;
	}

}
