
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="Get through the payment gateway to pass your successful transactions. Pay1 is a convenient app for Mobile, DTH, Data Card recharge & bill payment. ">
      <meta name="author" content="">
      <title>Payments | Pay1 </title>
      <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
      <link rel="stylesheet" href="/assets/css/style.css">
      <link rel="stylesheet" href="/assets/css/homepage.css">
      <link rel="stylesheet" href="/assets/css/navbar.css">
      <link rel="stylesheet" href="/assets/css/footer.css">
      <link rel="stylesheet" href="/assets/css/normalize.css">
      <link rel="stylesheet" href="/assets/css/media_query.css">
      <link rel="stylesheet" href="/assets/css/font-awesome.min.css">
      <link rel="stylesheet" href="/assets/css/easing.css">
      <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
      <style>body{background-color:#d1d1d1;}.bg_white{background-color:#fff; padding:10px;}</style>
   </head>
   <body>
       <input type="hidden" id="balance" value="<?php echo isset($parameters['balance']) ? $parameters['balance']: ""; ?>">
      <div class="header-section">
         <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
               <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed " data-toggle="collapse" data-target="#navigation" aria-expanded="false">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  </button>
                  <a class="navbar-brand" href="/home"><img src="/assets/images/icons/logo_pay1.png"></a>
               </div>
               <div class="collapse navbar-collapse pull-right" id="navigation">
                  <ul class="nav navbar-nav">
                     <li><a href="#">Recharge/Pay bills</a></li>
                     <li><a href="#">Gift Stores</a></li>
                     <li class="active-nav"><a href="#">Redeem Gift Coin</a></li>
                     <li><a href="#">Logout</a></li>
                  </ul>
               </div>
            </div>
         </nav>
      </div>
      <div class="clearfix" style="clear:both;"></div>
      <div class="container margin-top-50">
      <div class="row margin-top-50">
      <div class="col-md-6 col-md-offeset-3 col-md-push-4 bg_white">
         <div class="text-center"><h3>Payment <?php if(isset($parameters['status']) && $parameters['status']=='success'){ ?>Successful <?php } else{ ?>Failed !<?php } ?></h3></div>
         <br>
         <br>
         <table class="table1 table-responsive1 margin-top-20 borderless" style="margin-left:75px;margin-right:25px;width: 100%">
             <tr>
                 <td style="width: 50%">
                     <label style="font-size:14px;color:#ed1c24;font-weight:500">NUMBER</label><br><label id="trans_mobile" style="font-size:18px;color:#333333;font-weight:500"></label>
                 </td>
                 <td style="width: 50%">
                     <label style="font-size:14px;color:#ed1c24;font-weight:500">AMOUNT</label><br>Rs. <label style="font-size:18px;color:#333333;font-weight:500" id="transaction_amount"><?php echo $transaction['description'][0]['transaction_amount']; ?></label>
                 </td>
             </tr>
             <tr>
                 <td style="width: 50%">
                     <label style="font-size:14px;color:#ed1c24;font-weight:500">TRANSACTION ID</label><br><lbel style="font-size:18px;color:#333333;font-weight:500"><?php echo $parameters['txnid']; ?></label>
                 </td>
                 <td>
                     <label style="font-size:14px;color:#ed1c24;font-weight:500">PAYMENT MODE</label><br><label id="paymentmode" style="font-size:18px;color:#333333;font-weight:500"><?php echo $transaction['description'][0]['transaction_mode']; ?></label>
                 </td>
             </tr>
             <tr>
                 <td style="width: 50%">
                     <label style="font-size:14px;color:#ed1c24;font-weight:500">SERVICE</label><br><label id="service_provider" style="font-size:18px;color:#333333;font-weight:500"><?php echo $transaction['description'][0]['trans_category']; ?></label>
                 </td>
                 <td>
                     <label style="font-size:14px;color:#ed1c24;font-weight:500">PAY1 WALLET BALANCE</label><br>Rs. <label style="font-size:18px;color:#333333;font-weight:500"><?php echo isset($parameters['balance']) ? $parameters['balance']: "0"; ?></label>
                 </td>
             </tr>
             <tr>
                 <td style="width: 50%">
                     <label style="font-size:14px;color:#ed1c24;font-weight:500">DATE</label><br><label style="font-size:18px;color:#333333;font-weight:500"><?php echo date('Y-m-d'); ?></label>
                 </td>
                 <td>
                     <label style="font-size:14px;color:#ed1c24;font-weight:500">TIME</label><br><label style="font-size:18px;color:#333333;font-weight:500"><?php echo date("H:i:s",strtotime($transaction['description'][0]['update_datetime']));  ?></label>
                 </td>
             </tr>
         </table>
                <br>
                <br>
            <table class="table1 table-responsive1 margin-top-20 borderless" style="width:80%;margin-left:75px;margin-right:25px;padding-bottom: 15px">
                <tr>
                    <td style="width: 50%;text-align: left">
                        <a href="/profile" class="btn-save" style="border-radius:5px">Payment History</a> 
                    </td>
                    <td style="width: 50%;text-align: right" >
                        <a href="/" class="btn-save" style="border-radius:5px">Home</a> 
                    </td>
                </tr>
            </table>
<!--                <div style="padding-left: 70px;"><h5>To check your payment history. click <a href="/profile" class="btn-save">HERE</a> or </h5><p><h5>go back to <a href="/" class="btn-save">HOME</a></h5></p></div>
               -->
         
         </div>
      </div>
   </div>
</div>
     
      <script src="/assets/js/jquery.min.js"></script>
      <script src="/js/recharge.js"></script>
     <script type="text/javascript"  src="<?php echo base_url('js/jstorage.js'); ?>"></script>
      <script>
           $(document).ready(function(){ 
                var mobnumber = localStorage.getItem('number');
                if(mobnumber!=''){	
                 var bal = $("#balance").val();
                 $("#trans_mobile").html(localStorage.getItem('number'));
//                 $("#transaction_amount").html(localStorage.getItem('amount'));
                 //$("#service_provider").html(localStorage.getItem('provider'));
//                 $("#paymentmode").html(localStorage.getItem('paymentmode'));
                 var balance = {'wallet_balance' :bal};
                 b2c.core.updateprofile(balance);

                 localStorage.setItem('number','');
           } });
        
      </script>
   
    
   </body>
</html>