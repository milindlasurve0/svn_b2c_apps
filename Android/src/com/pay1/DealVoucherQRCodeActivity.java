package com.pay1;

import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.pay1.constants.Constants;
import com.pay1.utilities.Contents;
import com.pay1.utilities.QRCodeEncoder;

public class DealVoucherQRCodeActivity extends AppBaseActivity {

	private static final String SCREEN_LABEL = "Deal QRCode Screen";
	private EasyTracker easyTracker = null;
	// private String LOG_TAG = "GenerateQRCode";

	private ImageView imageView_Back;
	private TextView textView_Title, textView_TitleCode, textView_Code,
			textView_TitleID, textView_ID, textView_TitleDate, textView_Date,
			textView_TitleQuantity, textView_Quantity;
	private String code, ID, expity, quantity = "";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.deal_voucher_qrcode_activity);
		try {
			easyTracker = EasyTracker
					.getInstance(DealVoucherQRCodeActivity.this);
			easyTracker.set(Fields.SCREEN_NAME, SCREEN_LABEL);

			Bundle bundle = getIntent().getExtras();
			if (bundle != null) {
				code = bundle.getString("CODE");
				ID = bundle.getString("ID");
				expity = bundle.getString("EXPIRY");
				quantity = bundle.getString("QUANTITY");
			}
			registerBaseActivityReceiver();

			Typeface Reguler = Typeface.createFromAsset(getAssets(),
					"EncodeSansNormal-400-Regular.ttf");

			textView_Title = (TextView) findViewById(R.id.textView_Title);
			textView_Title.setTypeface(Reguler);

			textView_TitleCode = (TextView) findViewById(R.id.textView_TitleCode);
			textView_TitleCode.setTypeface(Reguler);
			textView_Code = (TextView) findViewById(R.id.textView_Code);
			textView_Code.setTypeface(Reguler);
			textView_Code.setText(code);
			textView_TitleID = (TextView) findViewById(R.id.textView_TitleID);
			textView_TitleID.setTypeface(Reguler);
			textView_ID = (TextView) findViewById(R.id.textView_ID);
			textView_ID.setTypeface(Reguler);
			textView_ID.setText(ID);
			textView_TitleDate = (TextView) findViewById(R.id.textView_TitleDate);
			textView_TitleDate.setTypeface(Reguler);
			textView_Date = (TextView) findViewById(R.id.textView_Date);
			textView_Date.setTypeface(Reguler);
			textView_Date.setText(expity);
			textView_TitleQuantity = (TextView) findViewById(R.id.textView_TitleQuantity);
			textView_TitleQuantity.setTypeface(Reguler);
			textView_Quantity = (TextView) findViewById(R.id.textView_Quantity);
			textView_Quantity.setTypeface(Reguler);
			textView_Quantity.setText(quantity);

			imageView_Back = (ImageView) findViewById(R.id.imageView_Back);
			imageView_Back.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onBackPressed();
				}
			});

			QRCodeEncoder qrCodeEncoder = new QRCodeEncoder(code, null,
					Contents.Type.TEXT, BarcodeFormat.QR_CODE.toString(), 300);
			try {
				Bitmap bitmap = qrCodeEncoder.encodeAsBitmap();
				ImageView myImage = (ImageView) findViewById(R.id.imageView_QRCode);
				myImage.setImageBitmap(bitmap);

			} catch (WriterException e) {
				e.printStackTrace();
			}

		} catch (Exception e) {
		}
		overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}

	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView().build());
		// EasyTracker.getInstance(this).activityStart(this);
		easyTracker.send(MapBuilder.createAppView().build());
	}

	@Override
	public void onStop() {
		super.onStop();
		// EasyTracker.getInstance(this).activityStop(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		// Constants.showNavigationBar(MyDealActivity.this);
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unRegisterBaseActivityReceiver();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (Constants.isExpanded) {
			Constants.collapsView(this);
		} else {
			super.onBackPressed();
			overridePendingTransition(R.anim.slide_in_left,
					R.anim.slide_out_right);
		}
	}

}