package com.pay1.servicehandler;

import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.IntentService;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.pay1.constants.Constants;
import com.pay1.databasehandler.NumbberWithCircleDataSource;
import com.pay1.databasehandler.Pay1SQLiteHelper;
import com.pay1.requesthandler.RequestClass;
import com.pay1.utilities.Utility;

public class CircleIntentService extends IntentService {
	NumbberWithCircleDataSource circleDataSource;
	private SQLiteDatabase database;
	private Pay1SQLiteHelper dbHelper;
	private String jString = "";

	public CircleIntentService() {
		super("CircleService");
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		dbHelper = new Pay1SQLiteHelper(CircleIntentService.this);
		database = dbHelper.getWritableDatabase();
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub

		circleDataSource = new NumbberWithCircleDataSource(
				CircleIntentService.this);
		// new SyncCircleDataTask().execute();
		// Log.d("Plan", "Plan Start Circle Thread Time loop");
		callApi();
	}

	public void callApi() {

		new Thread(new Runnable() {
			public void run() {

				// HttpClient httpclient = new DefaultHttpClient();
				// // Log.d(TAG,HomeView.webserviceUrl+"applogout");
				String time = Utility.getSharedPreferences(
						CircleIntentService.this, Constants.LAST_CODE_UPDATED) == null ? ""
						: Utility.getSharedPreferences(
								CircleIntentService.this,
								Constants.LAST_CODE_UPDATED);

				if (time != "") {
					try {
						DateFormat dateFormat = new SimpleDateFormat(
								"yyyy-MM-dd HH:mm:ss");
						Date date = new Date(Long.parseLong(time));
						time = dateFormat.format(date);
					} catch (Exception e) {
					}
				}
				/*
				 * HttpPost httppost = new HttpPost(Constants.API_URL +
				 * "method=getMobileDetails&mobile=all&timestamp" + time);
				 */
				// Log.d("Plan", "Plan Start Circle Start Thread Time " +
				// jString);
				try {
					jString = RequestClass
							.getInstance()
							.readPay1B2BRequest(
									CircleIntentService.this,
									Constants.B2B_URL
											+ "method=getMobileDetails&mobile=all&timestamp="
											+ URLEncoder.encode(time, "utf-8"));
					// Log.d("Plan", "Plan Start Circle Time " + jString);
					// Log.d("URL", Constants.B2B_URL
					// + "method=getMobileDetails&mobile=all&timestamp="
					// + time);

				} catch (Exception e) {
					// e.printStackTrace();
					// Log.d("Plan", "Plan  IOException " + e);
					jString = "Error";
				}

				if (jString.startsWith("Error"))
					jString = Constants.loadJSONFromAsset(
							CircleIntentService.this, "operatorcode.json");
				try {
					parseResponse(jString);
				} catch (Exception e) {
					// e.printStackTrace();
				} finally {
					circleDataSource.close();
				}
			}

			private void parseResponse(String response) {
				// TODO Auto-generated method stub
				try {
					DateFormat dateFormat = new SimpleDateFormat(
							"yyyy-MM-dd HH:mm:ss");
					Date date = new Date();
					String timeStamp = dateFormat.format(date);
					long t = System.currentTimeMillis();
					// Log.d("Plan", "Plan Start Circle Start Thread Time loop"
					// + timeStamp);

					circleDataSource.open();
					String sql = "INSERT INTO "
							+ "NumberAndCircleTable"
							+ " VALUES (?,?,?,?,?,?,?,?,?);";
					SQLiteStatement statement = database.compileStatement(sql);
					database.beginTransaction();

					if (!response.startsWith("Error")) {
						String replaced = response.replace("(", "")
								.replace(")", "").replace(";", "");
						// // Log.d("Circle  ", "Plan  "+response);
						replaced = replaced.substring(1, replaced.length() - 1);
						/* JSONArray array = new JSONArray(replaced); */
						JSONObject jsonObject = new JSONObject(replaced);
						String status = jsonObject.getString("status");
						if (status.equalsIgnoreCase("success")) {
							String details = jsonObject.getString("details");
							JSONArray detailsArray = new JSONArray(details);
							for (int i = 0; i < detailsArray.length(); i++) {
								JSONObject object = detailsArray
										.getJSONObject(i);
								String areaName = object.getString("area_name");
								String areaCode = object.getString("area");
								String operatorName = object
										.getString("opr_name");
								String operatorCode = object
										.getString("operator");
								String operatorId = object
										.getString("product_id");
								String number = object.getString("number");
								try {
									circleDataSource.deleteCircle(number);
								} catch (Exception e) {
								}

								// statement.bindString(1, "1");
								statement.bindString(2, areaCode);
								statement.bindString(3, areaName);
								statement.bindString(4, operatorCode);
								statement.bindString(5, operatorName);
								statement.bindString(6, operatorId);
								statement.bindString(7, number);
								statement.bindString(8, timeStamp);
								statement.bindLong(9, t);

								statement.execute();

								/*
								 * circleDataSource
								 * .createNumberAndCircleList(areaName,
								 * areaCode, operatorName, operatorCode,
								 * operatorId, number, timeStamp);
								 */
							}

						}

					}

					// String currentDateandTime = dateFormat.format(new
					// Date());
					// Log.d("Plan", "Plan Start Circle End Thread Time loop"
					// + currentDateandTime);
					database.setTransactionSuccessful();
					database.endTransaction();
					Utility.setSharedPreferences(CircleIntentService.this,
							Constants.LAST_CODE_UPDATED,
							String.valueOf(System.currentTimeMillis()));
				} catch (JSONException je) {
					// Log.d("NAC", "row inserted   " + je.getMessage());
					// je.printStackTrace();
				} catch (SQLException exception) {
					// Log.d("NAC", "row inserted   " + exception.getMessage());
					// TODO: handle exception
					// // // Log.e("Er ", exception.getMessage());
				} catch (Exception e) {
					// Log.d("NAC", "row inserted   " + e.getMessage());
					// TODO: handle exception
					// // // Log.e("Er ", e.getMessage());
				} finally {
					try {
						circleDataSource.close();
					} catch (SQLException see) {
						// Log.d("NAC", "row inserted   " + see.getMessage());
					}
					stopSelf();
				}
			}
		}).start();
	}
}
