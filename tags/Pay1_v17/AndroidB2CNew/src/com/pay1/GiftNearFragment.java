package com.pay1;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.database.SQLException;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.pay1.adapterhandler.GiftGridAdapter;
import com.pay1.constants.Constants;
import com.pay1.databasehandler.FreeBies;
import com.pay1.databasehandler.FreeBiesDataSource;
import com.pay1.utilities.Utility;

public class GiftNearFragment extends Fragment implements ConnectionCallbacks,
		OnConnectionFailedListener {

	private GridView gridView_Gifts;
	private GiftGridAdapter adapter;
	private ArrayList<FreeBies> data;
	private FreeBiesDataSource freeBiesDataSource;

	private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;

	private Location mLastLocation;

	// Google client to interact with Google API
	private GoogleApiClient mGoogleApiClient;

	// boolean flag to toggle periodic location updates
	private boolean mRequestingLocationUpdates = false;

	private LocationRequest mLocationRequest;

	// Location updates intervals in sec
	private static int UPDATE_INTERVAL = 10000; // 10 sec
	private static int FATEST_INTERVAL = 5000; // 5 sec
	private static int DISPLACEMENT = 0; // 10 meters

	public static GiftNearFragment newInstance() {
		GiftNearFragment fragment = new GiftNearFragment();
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// if ((savedInstanceState != null)
		// && savedInstanceState.containsKey(KEY_CONTENT)) {
		// mContent = savedInstanceState.getString(KEY_CONTENT);
		// }
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		final View view = inflater.inflate(R.layout.gift_grid_fragment,
				container, false);
		try {

			// First we need to check availability of play services
			if (checkPlayServices()) {

				// Building the GoogleApi client
				buildGoogleApiClient();
			}

			freeBiesDataSource = new FreeBiesDataSource(getActivity());

			data = new ArrayList<FreeBies>();
			gridView_Gifts = (GridView) view.findViewById(R.id.gridView_Gifts);

			// String ExternalStorageDirectoryPath = Environment
			// .getExternalStorageDirectory().getAbsolutePath();
			//
			// String targetPath = ExternalStorageDirectoryPath + "/Pay1Cache/";
			//
			// File targetDirector = new File(targetPath);
			//
			// File[] files = targetDirector.listFiles();
			// int i = 1;
			// for (File file : files) {
			// items.add(new GiftItem(i, "Gift " + i, file.getAbsolutePath(),
			// i * 10));
			// i++;
			// }

			try {
				freeBiesDataSource.open();
				String lat = Utility.getCurrentLatitude(getActivity(),
						Constants.SHAREDPREFERENCE_CURRENT_LATITUDE);
				String lng = Utility.getCurrentLongitude(getActivity(),
						Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE);

				if (lat != "" && lng != "") {
					List<FreeBies> list = freeBiesDataSource
							.getAllNearFreeBies(Double.parseDouble(lat),
									Double.parseDouble(lng), 2);

					int len = list.size();
					if (len != 0) {

						for (FreeBies freebie : list)
							data.add(freebie);

					} else {
						// new FreeBiesTask()
						// .execute(
						// Utility.getCurrentLatitude(
						// getActivity(),
						// Constants.SHAREDPREFERENCE_CURRENT_LATITUDE),
						// Utility.getCurrentLongitude(
						// getActivity(),
						// Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE));
						// pager.setVisibility(View.GONE);

					}
				}

			} catch (SQLException exception) {
				// TODO: handle exception
				Log.e("Er ", exception.getMessage());
			} catch (Exception e) {
				// TODO: handle exception
				// Log.e("Er ", e.getMessage());
			} finally {
				freeBiesDataSource.close();
			}
			adapter = new GiftGridAdapter(getActivity(), data, false, true);
			// items.clear();
			gridView_Gifts.setAdapter(adapter);
			gridView_Gifts
					.setOnItemClickListener(new AdapterView.OnItemClickListener() {

						@Override
						public void onItemClick(AdapterView<?> parent,
								View view, int position, long id) {
							// TODO Auto-generated method stub
							try {
								FreeBies freeBie = data.get(position);
								Intent intent = new Intent(getActivity(),
										MyFreeBiesDetailsActivity.class);
								intent.putExtra("GIFT", freeBie);
								intent.putExtra("IS_BUY", 0);
								startActivity(intent);
							} catch (Exception e) {
							}
						}
					});
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return view;
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		// outState.putString(KEY_CONTENT, mContent);
	}

	@Override
	public void onStart() {
		super.onStart();
		if (mGoogleApiClient != null) {
			mGoogleApiClient.connect();
		}
	}

	@Override
	public void onResume() {
		super.onResume();

		checkPlayServices();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.Fragment#onDestroy()
	 */
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	/**
	 * Method to display the location on UI
	 * */
	private void displayLocation() {

		mLastLocation = LocationServices.FusedLocationApi
				.getLastLocation(mGoogleApiClient);

		if (mLastLocation != null) {
			double latitude = mLastLocation.getLatitude();
			double longitude = mLastLocation.getLongitude();

			Utility.setCurrentLatitude(getActivity(),
					Constants.SHAREDPREFERENCE_CURRENT_LATITUDE,
					String.valueOf(latitude));
			Utility.setCurrentLongitude(getActivity(),
					Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE,
					String.valueOf(longitude));

			Log.e("NEAR", latitude + ", " + longitude);

			try {
				freeBiesDataSource.open();
				String lat = Utility.getCurrentLatitude(getActivity(),
						Constants.SHAREDPREFERENCE_CURRENT_LATITUDE);
				String lng = Utility.getCurrentLongitude(getActivity(),
						Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE);

				if (lat != "" && lng != "") {
					data.clear();
					List<FreeBies> list = freeBiesDataSource
							.getAllNearFreeBies(Double.parseDouble(lat),
									Double.parseDouble(lng), 2);

					int len = list.size();
					if (len != 0) {

						for (FreeBies freebie : list)
							data.add(freebie);

					} else {
						// new FreeBiesTask()
						// .execute(
						// Utility.getCurrentLatitude(
						// getActivity(),
						// Constants.SHAREDPREFERENCE_CURRENT_LATITUDE),
						// Utility.getCurrentLongitude(
						// getActivity(),
						// Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE));
						// pager.setVisibility(View.GONE);

					}
				}
				getActivity().runOnUiThread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						adapter.notifyDataSetChanged();
					}
				});
			} catch (SQLException exception) {
				// TODO: handle exception
				Log.e("Er ", exception.getMessage());
			} catch (Exception e) {
				// TODO: handle exception
				// Log.e("Er ", e.getMessage());
			} finally {
				freeBiesDataSource.close();
			}

		} else {

			Log.e("NEAR",
					"(Couldn't get the location. Make sure location is enabled on the device)");
		}
	}

	/**
	 * Creating location request object
	 * */
	protected void createLocationRequest() {
		mLocationRequest = new LocationRequest();
		mLocationRequest.setInterval(UPDATE_INTERVAL);
		mLocationRequest.setFastestInterval(FATEST_INTERVAL);
		mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
		mLocationRequest.setSmallestDisplacement(DISPLACEMENT);
	}

	/**
	 * Creating google api client object
	 * */
	protected synchronized void buildGoogleApiClient() {
		mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
				.addConnectionCallbacks(this)
				.addOnConnectionFailedListener(this)
				.addApi(LocationServices.API).build();
	}

	/**
	 * Method to verify google play services on the device
	 * */
	private boolean checkPlayServices() {
		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(getActivity());
		if (resultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
				GooglePlayServicesUtil.getErrorDialog(resultCode,
						getActivity(), PLAY_SERVICES_RESOLUTION_REQUEST).show();
			} else {
				Toast.makeText(getActivity(), "This device is not supported.",
						Toast.LENGTH_LONG).show();

			}
			return false;
		}
		return true;
	}

	@Override
	public void onConnectionFailed(ConnectionResult arg0) {
		// TODO Auto-generated method stub
		Log.i("NEAR", "Connection failed: ConnectionResult.getErrorCode() = "
				+ arg0.getErrorCode());
	}

	@Override
	public void onConnected(Bundle arg0) {
		// TODO Auto-generated method stub
		displayLocation();
	}

	@Override
	public void onConnectionSuspended(int arg0) {
		// TODO Auto-generated method stub
		mGoogleApiClient.connect();
	}

	
}
