//
//  RechargeService.m
//  Pay1
//
//  Created by Annapurna on 15/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "RechargeService.h"
#import "RechargeCriteria.h"
#import "RechargeResponse.h"
#import "EnumType.h"
#import "Constant.h"
@implementation RechargeService
-(void)callRechargeService:(RechargeCriteria *)criteria callback:(rechargeResponse)callback{
    NSMutableDictionary *postData = [[NSMutableDictionary alloc]init];
    
    if(criteria.serviceType == kServiceTypeMobile){
    [postData setObject:criteria.userMobileNumber forKey:@"mobile_number"];
    [postData setObject:criteria.stv forKey:@"stv"];
    }
    
    if(criteria.serviceType == kServiceTypePostPaid ){
    [postData setObject:criteria.userMobileNumber forKey:@"mobile_number"];
    [postData setObject:@"1" forKey:@"billpayment"];
    [postData setObject:@"1" forKey:@"payment_flag"];
    [postData setObject:criteria.baseAmount forKey:@"base_amount"];
    [postData setObject:criteria.serviceCharge forKey:@"service_charge"];
    [postData setObject:criteria.serviceTax forKey:@"service_tax"];
    }
    
    
    
    if(criteria.serviceType == kServiceTypeDataCard)
    [postData setObject:criteria.userMobileNumber forKey:@"mobile_number"];

    
    if(criteria.serviceType == kServiceTypeDTH)
    [postData setObject:criteria.userSubscriberID forKey:@"subscriber_id"];
    
    if(criteria.serviceType == kServiceTypeQuickRecharge){
    if([criteria.flag isEqualToString:@"1"]){
    [postData setObject:criteria.userMobileNumber forKey:@"mobile_number"];
    [postData setObject:criteria.stv forKey:@"stv"];
    }
        
    else if([criteria.flag isEqualToString:@"3"]){
    [postData setObject:criteria.userMobileNumber forKey:@"mobile_number"];
    }
   else if([criteria.flag isEqualToString:@"2"]){
    [postData setObject:criteria.userSubscriberID forKey:@"subscriber_id"];
    }
    else{
    [postData setObject:criteria.userMobileNumber forKey:@"mobile_number"];
    [postData setObject:@"1" forKey:@"billpayment"];
    [postData setObject:@"1" forKey:@"payment_flag"];
    [postData setObject:criteria.baseAmount forKey:@"base_amount"];
    [postData setObject:criteria.serviceCharge forKey:@"service_charge"];
    [postData setObject:criteria.serviceTax forKey:@"service_tax"];
    }
        
    }
    
    
    
    
    [postData setObject:criteria.operatorID forKey:@"operator"];
    [postData setObject:criteria.flag forKey:@"flag"];
    [postData setObject:criteria.amount forKey:@"amount"];
    [postData setObject:@"1" forKey:@"recharge"];
    [postData setObject:criteria.paymentOption forKey:@"paymentopt"];
    [postData setObject:@"1" forKey:@"payment"];
    [postData setObject:criteria.partial forKey:@"partial"];

    [self fillDefaultData:postData];
    
    NSLog(@"#RechargePostData :%@",postData);
    
    RequestData *requestData = [[RequestData alloc]init];
    requestData.baseURL = kBaseURL;
    if(criteria.serviceType == kServiceTypePostPaid)
    requestData.webServiceURL = kPostPaidRechargeURL;
    else
    requestData.webServiceURL = kRechargeURL;
    
    requestData.postData = postData;
    requestData.showLoader = FALSE;
    
    requestData.requestSuccessBlock = ^(RequestData *request) {
    [self printResponse:request.responseData forRequest:@"Recharge Response"];
    if ([self handleNullResponse:request])
    return;
        
    callback(nil, [self parseRechargeResponse:request]);
    };
    
    
    requestData.requestFailureBlock = ^(RequestData *request) {
    [self handleNetworkError:request.responseData];
    callback(request.responseData, nil);
    };
    
    [[ServiceManager sharedInstance] makeRequest:requestData];

}

-(RechargeResponse *)parseRechargeResponse:(RequestData *)request{
    RechargeResponse *objRechargeResponse = [[RechargeResponse alloc]init];
    
    objRechargeResponse.errorCode = [request.responseData objectForKey:@"errCode"];
    objRechargeResponse.status = [request.responseData objectForKey:@"status"];
    
    
    if([objRechargeResponse.status isEqualToString:@"success"]){
    objRechargeResponse.dealArrayList = [request.responseData valueForKey:@"deal"];
    NSMutableDictionary *dealDic = [request.responseData objectForKey:@"dealDetails"];
    
    objRechargeResponse.allDeals = [dealDic objectForKey:@"Alldeals"];
    objRechargeResponse.claimedGifts = [dealDic objectForKey:@"claimedGifts"];
    objRechargeResponse.offerDetails = [dealDic objectForKey:@"offer_details"];

    NSMutableDictionary *descDic = [request.responseData objectForKey:@"description"];
        
    if([descDic objectForKey:@"form_content"]){
    objRechargeResponse.formContent = [descDic objectForKey:@"form_content"];
    objRechargeResponse.txnID = [descDic objectForKey:@"txnid"];

    }
    else{
    objRechargeResponse.closingBalance = [descDic objectForKey:@"closing_balance"];
    objRechargeResponse.expiry = [descDic objectForKey:@"expiry"];
    objRechargeResponse.loyaltyBalance = [descDic objectForKey:@"loyalty_balance"];
    objRechargeResponse.loyaltyPoints = [descDic objectForKey:@"loyalty_points"];
    objRechargeResponse.onlineForm = [descDic objectForKey:@"online_form"];
    objRechargeResponse.operatorName = [descDic objectForKey:@"operator_name"];
    objRechargeResponse.pin = [descDic objectForKey:@"pin"];
    objRechargeResponse.txnID = [descDic objectForKey:@"transaction_id"];

    }
        
        
        
        
        
    }
    
    else if([objRechargeResponse.status isEqualToString:@"failure"]){
    objRechargeResponse.descMessage = [request.responseData objectForKey:@"description"];
    }
    
    

    
    return objRechargeResponse;
}

#pragma mark - Online Wallet Refill

-(void)callOnlineWalletRefillService:(NSString *)amountToRefill callback:(onlineRefillResponse)callback{
    NSMutableDictionary *postData = [[NSMutableDictionary alloc]init];
    
    [postData setObject:amountToRefill forKey:@"amount"];
    [self fillDefaultData:postData];
    
    NSLog(@"#RechargePostData :%@",postData);
    
    RequestData *requestData = [[RequestData alloc]init];
    requestData.baseURL = kBaseURL;
    requestData.webServiceURL = kRefillWalletURL;
    requestData.postData = postData;
    requestData.showLoader = FALSE;
    
    requestData.requestSuccessBlock = ^(RequestData *request) {
    [self printResponse:request.responseData forRequest:@"Recharge Response"];
    if ([self handleNullResponse:request])
    return;
        
    callback(nil, [self parseRefillWalletResponse:request]);
    };
    
    
    requestData.requestFailureBlock = ^(RequestData *request) {
    [self handleNetworkError:request.responseData];
    callback(request.responseData, nil);
    };
    
    [[ServiceManager sharedInstance] makeRequest:requestData];
}

-(RechargeResponse *)parseRefillWalletResponse:(RequestData *)request{
    RechargeResponse *objRechargeResponse = [[RechargeResponse alloc]init];
    
    objRechargeResponse.errorCode = [request.responseData objectForKey:@"errCode"];
    objRechargeResponse.status = [request.responseData objectForKey:@"status"];
    
    NSLog(@"#Status :%@",objRechargeResponse.status);
    
    if([objRechargeResponse.status isEqualToString:@"success"]){
    NSMutableDictionary *descDic = [request.responseData objectForKey:@"description"];
    objRechargeResponse.formContent = [descDic objectForKey:@"form_content"];
    objRechargeResponse.txnID = [descDic objectForKey:@"txnid"];
    }
    if([objRechargeResponse.status isEqualToString:@"failure"]){
    objRechargeResponse.descMessage = [request.responseData objectForKey:@"description"];
    }
    
   
    
    
    return objRechargeResponse;
}

#pragma mark - Redeem Coupon Service

-(void)callCouponRedeemService:(NSString *)voucherCode callback:(couponRedeemResponse)callback{
    NSMutableDictionary *postData = [[NSMutableDictionary alloc]init];
    [postData setObject:[Constant getUserPhoneNumber] forKey:@"mobile_number"];
    [postData setObject:voucherCode forKey:@"vcode"];
    [self fillDefaultData:postData];
    
    NSLog(@"#RechargePostData :%@",postData);
    
    RequestData *requestData = [[RequestData alloc]init];
    requestData.baseURL = kBaseURL;
    requestData.webServiceURL = kRedeemCouponURL;
    requestData.postData = postData;
    requestData.showLoader = FALSE;
    
    requestData.requestSuccessBlock = ^(RequestData *request) {
    [self printResponse:request.responseData forRequest:@"Recharge Response"];
    if ([self handleNullResponse:request])
    return;
        
    callback(nil, [self parseRedeemCouponResponse:request]);
    };
    
    
    requestData.requestFailureBlock = ^(RequestData *request) {
    [self handleNetworkError:request.responseData];
    callback(request.responseData, nil);
    };
    
    [[ServiceManager sharedInstance] makeRequest:requestData];
}

-(RechargeResponse *)parseRedeemCouponResponse:(RequestData *)request{
    RechargeResponse *objRechargeResponse = [[RechargeResponse alloc]init];
    
    objRechargeResponse.errorCode = [request.responseData objectForKey:@"errCode"];
    objRechargeResponse.status = [request.responseData objectForKey:@"status"];
    
    if([objRechargeResponse.status isEqualToString:@"success"]){
    NSMutableDictionary *descDic = [request.responseData objectForKey:@"description"];
    objRechargeResponse.closingBalance = [descDic objectForKey:@"closing_balance"];
    objRechargeResponse.txnID = [descDic objectForKey:@"transaction_id"];
    }
    
    if([objRechargeResponse.status isEqualToString:@"failure"]){
    objRechargeResponse.descMessage = [request.responseData objectForKey:@"description"];
    }
    
    
    
    return objRechargeResponse;
}


@end
