//
//  ProfileUpdateService.m
//  Pay1
//
//  Created by webninjaz on 11/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "ProfileUpdateService.h"
#import "ProfileUpdateCriteria.h"
//#import "ProfileUpdateResponse.h"
#import "SignInResponse.h"
@implementation ProfileUpdateService

-(void)callProfileUpdateService:(ProfileUpdateCriteria *)profileUpdateCriteria callback:(profileUpdateResponse)callback{
    
    NSMutableDictionary *postData = [[NSMutableDictionary alloc] init];
    if(profileUpdateCriteria.profileUpdateName)
    [postData setObject:profileUpdateCriteria.profileUpdateName forKey:@"name"];
    
    if(profileUpdateCriteria.profileUpdateEmail)
    [postData setObject:profileUpdateCriteria.profileUpdateEmail forKey:@"email"];
    
    if(profileUpdateCriteria.profileUpdateGender)
    [postData setObject:profileUpdateCriteria.profileUpdateGender forKey:@"gender"];
    
    if(profileUpdateCriteria.profileUpdateDateOfBirth)
    [postData setObject:profileUpdateCriteria.profileUpdateDateOfBirth forKey:@"date_of_birth"];
    
    if(profileUpdateCriteria.profileUpdateUDID)
    [postData setObject:profileUpdateCriteria.profileUpdateUDID forKey:@"uuid"];
    
    if(profileUpdateCriteria.profileUpdateDeviceType)
    [postData setObject:profileUpdateCriteria.profileUpdateDeviceType forKey:@"device_type"];
    
    if(profileUpdateCriteria.profileUpdatePassword)
    [postData setObject:profileUpdateCriteria.profileUpdatePassword forKey:@"password"];
   
    
    
    NSLog(@"PostData for Update Password :%@",postData);
    
    RequestData *requestData = [[RequestData alloc]init];
    requestData.baseURL = kBaseURL;
    requestData.webServiceURL = kProfileUpdateURL;
    requestData.postData = postData;
    requestData.showLoader = FALSE;
    
    requestData.requestSuccessBlock = ^(RequestData *request) {
    [self printResponse:request.responseData forRequest:@"Profile Update Response"];
    if ([self handleNullResponse:request])
    return;
        
    callback(nil, [self parseProfileUpdateResponse:request]);
    };
    
    requestData.requestFailureBlock = ^(RequestData *request) {
        [self handleNetworkError:request.responseData];
        callback(request.responseData, nil);
    };
    
    [[ServiceManager sharedInstance] makeRequest:requestData];
}

-(SignInResponse *)parseProfileUpdateResponse:(RequestData *)request{
    SignInResponse *objProfileUpdateResponse = [[SignInResponse alloc]init];
    
    objProfileUpdateResponse.status = [request.responseData objectForKey:@"status"];
    objProfileUpdateResponse.errorCode = [request.responseData objectForKey:@"errCode"];
    objProfileUpdateResponse.descMessage = [request.responseData objectForKey:@"description"];

    
    NSMutableDictionary *descriptionDic = [request.responseData objectForKey:@"profileData"];
    objProfileUpdateResponse.userAPIVersion = [descriptionDic objectForKey:@"api_version"];
    objProfileUpdateResponse.created = [descriptionDic objectForKey:@"created"];
    objProfileUpdateResponse.userDOB = [descriptionDic objectForKey:@"date_of_birth"];
    objProfileUpdateResponse.deviceType = [descriptionDic objectForKey:@"device_type"];
    objProfileUpdateResponse.userEmail = [descriptionDic objectForKey:@"email"];
    objProfileUpdateResponse.userFBData = [descriptionDic objectForKey:@"fb_data"];
    objProfileUpdateResponse.gcmRegID = [descriptionDic objectForKey:@"gcm_reg_id"];
    objProfileUpdateResponse.userGender = [descriptionDic objectForKey:@"gender"];
    objProfileUpdateResponse.uID = [descriptionDic objectForKey:@"id"];
    objProfileUpdateResponse.profileImageURL = [descriptionDic objectForKey:@"image"];
    objProfileUpdateResponse.userLat = [descriptionDic objectForKey:@"latitude"];
    objProfileUpdateResponse.userLong = [descriptionDic objectForKey:@"longitude"];
    objProfileUpdateResponse.locationSRC = [descriptionDic objectForKey:@"location_src"];
    objProfileUpdateResponse.userManufacturer = [descriptionDic objectForKey:@"manufacturer"];
    objProfileUpdateResponse.userMobileNum = [descriptionDic objectForKey:@"mobile"];
    objProfileUpdateResponse.userName = [descriptionDic objectForKey:@"name"];
    objProfileUpdateResponse.userOSInfo = [descriptionDic objectForKey:@"os_info"];
    objProfileUpdateResponse.updated = [descriptionDic objectForKey:@"updated"];
    objProfileUpdateResponse.userID = [descriptionDic objectForKey:@"user_id"];
    objProfileUpdateResponse.userType = [descriptionDic objectForKey:@"user_type"];
    objProfileUpdateResponse.userDeviceID = [descriptionDic objectForKey:@"uuid"];
    objProfileUpdateResponse.userISGenuine = [descriptionDic objectForKey:@"is_genuine"];
    return objProfileUpdateResponse;
}


@end
