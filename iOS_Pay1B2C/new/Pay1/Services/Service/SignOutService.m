//
//  SignOutService.m
//  Pay1
//
//  Created by webninjaz on 03/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "SignOutService.h"
#import "SignOutResponse.h"

@implementation SignOutService

-(void)callSignOutService:(signOutResponse)callback{
    RequestData *requestData = [[RequestData alloc]init];
    requestData.baseURL = kBaseURL;
    requestData.webServiceURL = kSignOutURL;
    requestData.showLoader = FALSE;
    
    requestData.requestSuccessBlock = ^(RequestData *request) {
        
        [self printResponse:request.responseData forRequest:@"GetSignOut Response"];
        if ([self handleNullResponse:request])
            return;
        
        callback(nil, [self parseSignOutResponse:request]);
    };
    
    
    requestData.requestFailureBlock = ^(RequestData *request) {
        [self handleNetworkError:request.responseData];
        callback(request.responseData, nil);
    };
    
    [[ServiceManager sharedInstance] makeRequest:requestData];

}


-(SignOutResponse *)parseSignOutResponse:(RequestData *)request{
    SignOutResponse *signOutResponse = [[SignOutResponse alloc]init];
    signOutResponse.status = [request.responseData objectForKey:@"status"];
    NSLog(@"The status is%@",signOutResponse.status);
    signOutResponse.errCode = [request.responseData objectForKey:@"errCode"];
    NSLog(@"The error code is%@",signOutResponse.errCode);
    signOutResponse.descriptionSignOutResponse =  [request.responseData objectForKey:@"description"];
    NSLog(@"The sign out response is%@",signOutResponse.descriptionSignOutResponse);
    
    return signOutResponse;
}

@end
