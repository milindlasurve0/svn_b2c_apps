//
//  StoreLocationCriteria.h
//  Pay1
//
//  Created by Annapurna on 10/08/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StoreLocationCriteria : NSObject

@property (nonatomic, strong) NSString *userLat;
@property (nonatomic, strong) NSString *userLong;
@property (nonatomic, strong) NSString *distance;
@property (nonatomic, strong) NSString *userMobNum;




@end
