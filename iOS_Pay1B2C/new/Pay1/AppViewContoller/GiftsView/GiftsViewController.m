//
//  GiftsViewController.m
//  Pay1
//
//  Created by Annapurna on 16/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//http://www.objc.io/issues/4-core-data/core-data-fetch-requests/

#import "GiftsViewController.h"
#import "GiftsService.h"
#import "GiftResponse.h"
#import "GiftCustomCell.h"
#import "GiftList.h"
#import "GiftDetailViewController.h"
#import "GiftCategoryService.h"
#import "GiftCategoryResponse.h"
#import "GiftCategoryCustomCell.h"
#import "GiftCategoryList.h"
#import "GiftCriteria.h"
#import "DBManager.h"
#import "MyLikesCell.h"
#import "GetMyDealService.h"
#import "GetMyDealResponse.h"
#import "MyGiftsCell.h"
#import "GetMyDealList.h"
#import "TopOfferViewController.h"
#import "CurrentLocation.h"
#import <math.h>
#import "MyLikeService.h"
#import "MyLikeResponse.h"
#import "CoinViewController.h"
@interface GiftsViewController ()

@end

@implementation GiftsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"#WeAreInGiftsViewPage");
    [self setPageView];
    [self hideNavBackButton];
    
    
    nearYouTV.hidden = TRUE;

    
    
    if([UIScreen mainScreen].bounds.size.width == 320){
    giftTV.frame = CGRectMake(0, 0, self.view.frame.size.width, pageScrollView.frame.size.height - 80);
    nearYouTV.frame = CGRectMake(0, 0, 375, pageScrollView.frame.size.height - 100);
    categoryTV.frame = CGRectMake(0, 0, self.view.frame.size.width, pageScrollView.frame.size.height - 100);
    myGiftsTV.frame = CGRectMake(0, 0, 375, pageScrollView.frame.size.height - 100);
    myLikesTV.frame = CGRectMake(0, 0, 375, pageScrollView.frame.size.height - 100);
    }
    
    
   

    
    
    UIView *imageScrollView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 180)];
    
    giftScrollView =  [[UIScrollView alloc]initWithFrame:CGRectMake(0, 2, self.view.frame.size.width, 160)];
    giftScrollView.pagingEnabled = YES;

    UIView *grayView = [[UIView alloc]initWithFrame:CGRectMake(0, imageScrollView.frame.origin.y + imageScrollView.frame.size.height - 2 , self.view.frame.size.width, 1)];
    
    
    grayView.backgroundColor = [UIColor grayColor];
    [imageScrollView addSubview:grayView];
    [imageScrollView addSubview:giftScrollView];
    giftTV.tableHeaderView = imageScrollView;
    
    UIView *giftBottomView = [[UIView alloc]initWithFrame:CGRectMake(0, giftTV.frame.origin.y + giftTV.frame.size.height - 5, giftTV.frame.size.width, 5)];
    giftTV.tableFooterView = giftBottomView;
    
    UIView *categoryBottomView = [[UIView alloc]initWithFrame:CGRectMake(0, categoryTV.frame.origin.y + categoryTV.frame.size.height - 5, giftTV.frame.size.width, 5)];
    categoryTV.tableFooterView = categoryBottomView;
    
    [giftTV removeFromSuperview];
    
    giftSelectType = kSelectedTypeGift;

    
 
    
    NSLog(@"FrameWidth :%f",self.view.frame.size.width);
    

    
    
    
    checkFromWhere = FALSE;
  
  
}

-(void)viewWillAppear:(BOOL)animated{
    
    isLoadedOnce = TRUE;

    
    NSLog(@"#SecrollContentOffset :%f %f",pageScrollView.contentOffset.x,pageScrollView.contentOffset.y);
    
    [self setTwoNavigationButtonsWithImages:@"money_sliding_menu.png" secondImage:@"coin_sling_menu.png" thirdImage:@"ic_search.png" isLeft:FALSE selector1:@selector(moneyButtonClicked:) selector2:@selector(coinButtonClicked:) selector3:@selector(searchBtnClicked:) isFromGift:TRUE];
    
    if([Session shared].isFromSearch == TRUE){
    [self showSearchBarOnTheView];
    }
    
    if(pageScrollView.contentOffset.x == 0.0){
    if(checkFromWhere == FALSE){
    giftSelectType = kSelectedTypeGift;
        
        
    likesArray = [[NSMutableArray alloc]init];
        
    
    offerIDSArray = [[NSMutableArray alloc]init];
    distanceArray = [[NSMutableArray alloc]init];
        
    giftDataArray = [[NSMutableArray alloc]init];
    myGiftDataArray = [[NSMutableArray alloc]init];
    myLikesDataArray = [[NSMutableArray alloc]init];
    nearYouGiftArray = [[NSMutableArray alloc]init];
    giftCategoryArray = [[NSMutableArray alloc]init];
        
    imageURLArray = [[NSMutableArray alloc]init];
    shopNameArray = [[NSMutableArray alloc]init];
    areaArray = [[NSMutableArray alloc]init];
    offerArray = [[NSMutableArray alloc]init];
    priceArray = [[NSMutableArray alloc]init];
        
    dealIDArrayList = [[NSMutableArray alloc]init];
    offerIDArrayList = [[NSMutableArray alloc]init];

        
    shopLatArray = [[NSMutableArray alloc]init];
    shopLongArray = [[NSMutableArray alloc]init];
       
        
    [self callGiftAPIService];

    }
    }
    
}

-(void)callGiftAPIService{
    NSLog(@"#TheGiftAPI Is Called");
    
    [self showHUDOnView];
    [imageURLArray removeAllObjects];
    [shopNameArray removeAllObjects];
    [areaArray removeAllObjects];
    [offerArray removeAllObjects];
    [priceArray removeAllObjects];
    
    [dealIDArrayList removeAllObjects];
    [offerIDArrayList removeAllObjects];

    

    GiftsService *giftService = [[GiftsService alloc]init];
    [giftService callGiftService:self.criteria callback:^(NSError *error, GiftResponse *objGiftResponse) {
    if(error)return ;
    else{
   // NSLog(@"objGiftResponse :%@",objGiftResponse.giftArrayList);
   [self fetchValueFromDBForGifts:objGiftResponse.giftArrayList];

    giftDataArray = objGiftResponse.giftArrayList;
        
    firstIndexGiftArray =  [giftDataArray objectAtIndex:0];
        
    [giftDataArray removeObjectAtIndex:0];
   // NSLog(@"giftDataArray :%@",giftDataArray);

   [pageScrollView addSubview:giftTV];
        
    [giftTV reloadData];
    [self hideHUDOnView];
        
    }
 
    }];
}

-(GiftCriteria *)criteria{
    GiftCriteria *objCriteria = [[GiftCriteria alloc]init];
    objCriteria.next = @"0";
    objCriteria.type = @"0";
    
    return objCriteria;
}

-(void)fetchValueFromDBForGifts:(NSMutableArray *)dataArray{
    GiftList *objGiftList = [dataArray objectAtIndex:0];
    NSLog(@"GiftData :%@ %@",objGiftList.name,objGiftList.details);
   NSArray * offerIDArray= [objGiftList.details componentsSeparatedByString:@","];

    [self fetchDataFromDB:offerIDArray];
}

-(void)fetchDataFromDB:(NSArray *)giftOfferIDArray{
    
    NSString *query;
    NSMutableArray *offerArrayList = [[NSArray arrayWithArray:giftOfferIDArray] mutableCopy];
    DBManager *dbManager = [[DBManager alloc]initWithDatabaseFilename:@"dbgiftdata.sql"];
    for(int i=0;i<offerArrayList.count;i++){
    NSString *offserValue = [offerArrayList objectAtIndex:i];
        
  //  NSLog(@"offserValue :%@",offserValue);
    query = [NSString stringWithFormat:@"select * from GIFTTABLE WHERE FREEBIEOFFERID=\"%@\"",offserValue];
        
    NSArray *theArray = [[NSArray alloc] initWithArray:[dbManager loadDataFromDB:query]];
        
    NSInteger indexOfImageURL = [dbManager.arrColumnNames indexOfObject:@"FREEBIEURL"];
    for(int i=0;i<theArray.count;i++){
    [imageURLArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfImageURL]];
    }
    NSInteger indexOfShopNameURL = [dbManager.arrColumnNames indexOfObject:@"FREEBIEDEALNAME"];
    for(int i=0;i<theArray.count;i++){
    [shopNameArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfShopNameURL]];
    }
    NSInteger indexOfAreaURL = [dbManager.arrColumnNames indexOfObject:@"FREEBIEAREA"];
    for(int i=0;i<theArray.count;i++){
    [areaArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfAreaURL]];
    }
    NSInteger indexOfOfferURL = [dbManager.arrColumnNames indexOfObject:@"FREEBIESHORTDESC"];
    for(int i=0;i<theArray.count;i++){
    [offerArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfOfferURL]];
    }
    NSInteger indexOfPriceURL = [dbManager.arrColumnNames indexOfObject:@"FREEBIEMINAMOUNT"];
    for(int i=0;i<theArray.count;i++){
    [priceArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfPriceURL]];
    }
        
    NSInteger indexOfDealIDURL = [dbManager.arrColumnNames indexOfObject:@"FREEBIEDEALID"];
    for(int i=0;i<theArray.count;i++){
    [dealIDArrayList addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfDealIDURL]];
    }
        
    NSInteger indexOfOfferIDURL = [dbManager.arrColumnNames indexOfObject:@"FREEBIEOFFERID"];
    for(int i=0;i<theArray.count;i++){
    [offerIDArrayList addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfOfferIDURL]];
    }
        
    }
    
 //   NSLog(@"ImageURLValue is :%@",imageURLArray);
    
    [self showGiftFirstDataOnImageView];
    
}

-(void)showGiftFirstDataOnImageView{
    giftScrollView.contentSize = CGSizeMake(giftScrollView.frame.size.width * imageURLArray.count, 125);

    NSLog(@"ImageURLCount is :%lu",(unsigned long)imageURLArray.count);
    CGFloat x = 0;
    
    for(int i=0;i<imageURLArray.count;i++){
    NSString *imageURL   = [imageURLArray objectAtIndex:i];
  //  NSLog(@"imageURL :%@",imageURL);
    
        
   UIImageView *  giftImgView = [[UIImageView alloc]initWithFrame:CGRectMake(x + 10 , 10, giftScrollView.frame.size.width - 20, giftScrollView.frame.size.height - 10)];
   giftImgView.userInteractionEnabled = TRUE;
        
        UITapGestureRecognizer *imgTapGes = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(grayImageTapped:)];
        imgTapGes.delegate = self;
        giftImgView.tag = i;
        [giftImgView addGestureRecognizer:imgTapGes];
     
        
   giftImgView.tintColor = [UIColor blackColor];
        
   UIView *maskImageView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, giftImgView.frame.size.width, giftImgView.frame.size.height)];
    maskImageView.backgroundColor = [UIColor colorWithRed:49/255.0 green:49/255.0 blue:49/255.0 alpha:.2];
        
      

        
    [giftImgView setImageWithURL:[NSURL URLWithString:imageURL] placeholderImage:[UIImage imageNamed:@"deal_default.png"]completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
    UIImage *newImage = [UIImage imageWithImage:image  scaledToSize:CGSizeMake(320,212)];
    giftImgView.image = newImage;
    }usingActivityIndicatorStyle:(UIActivityIndicatorViewStyle)UIActivityIndicatorViewStyleGray];
        
    UILabel *giftOfferLabel = [[UILabel alloc]initWithFrame:CGRectMake(30, 30, giftImgView.frame.size.width - 30, 50)];
    giftOfferLabel.text = [offerArray objectAtIndex:i];
    giftOfferLabel.textColor = [UIColor whiteColor];
    giftOfferLabel.font = [UIFont systemFontOfSize:22];
    giftOfferLabel.textAlignment = NSTextAlignmentCenter;
    giftOfferLabel.lineBreakMode = NSLineBreakByWordWrapping;
    giftOfferLabel.numberOfLines = 3;
    [giftOfferLabel sizeToFit];
        
    UILabel *giftShopLabel = [[UILabel alloc]initWithFrame:CGRectMake(30, giftOfferLabel.frame.origin.y + giftOfferLabel.frame.size.height + 10, giftImgView.frame.size.width - 30, 21)];
    NSString *name = [[shopNameArray objectAtIndex:i] stringByReplacingOccurrencesOfString:@"%27" withString:@"'"];

    giftShopLabel.text = name; //[shopNameArray objectAtIndex:i];
        
    giftShopLabel.textColor = [UIColor whiteColor];
    giftShopLabel.font = [UIFont systemFontOfSize:15];
    giftShopLabel.textAlignment = NSTextAlignmentCenter;
       
    [maskImageView addSubview:giftOfferLabel];
    [maskImageView addSubview:giftShopLabel];
    [giftImgView addSubview:maskImageView];
    [giftScrollView addSubview:giftImgView];
        
  //  NSLog(@"X is :%f %f",x,giftImgView.frame.size.width);
        
    x+=self.view.frame.size.width ;
    }
    imageTimer = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(change:)  userInfo:nil repeats:YES];
    
}

-(void)grayImageTapped:(UITapGestureRecognizer *)sender{
    NSLog(@"ImageTapped %ld",(long)sender.view.tag);
    
    NSString *dealID = [dealIDArrayList objectAtIndex:sender.view.tag];
    NSString *offerID = [offerIDArrayList objectAtIndex:sender.view.tag];
    NSString *shopTxt = [shopNameArray objectAtIndex:sender.view.tag];

    NSLog(@"ImageTapped %@ %@ %@",dealID,offerID,shopTxt);

    [self navigateToProductDetailView:dealID offID:offerID shopText:shopTxt];
    
}

-(void)callGetMyDealServiceBlock{
   // [myGiftDataArray removeAllObjects];
    [self showHUDOnView];
    GetMyDealService *service = [[GetMyDealService alloc]init];
    [service callGetMyDealService:^(NSError *error, GetMyDealResponse *objGetMyDealResponse) {
    if(error)return ;
    else{
        
    if(objGetMyDealResponse.myDealResponseArray.count > 0){
    myGiftDataArray = objGetMyDealResponse.myDealResponseArray;
    [Constant saveMyGiftsList:objGetMyDealResponse.myDealResponseArray];
    if([[Constant getMyGiftsCount] integerValue]!= objGetMyDealResponse.myDealResponseArray.count){
    [myGiftsTV reloadData];
    [Constant saveMyGiftsCount:[NSString stringWithFormat:@"%lu",(unsigned long)objGetMyDealResponse.myDealResponseArray.count]];
    noMyGiftsImgView.hidden = TRUE;
    noMyGiftsLabel.hidden = TRUE;
      
        
    }
    }
    [self hideHUDOnView];

    }
    }];
}

-(void)callGiftCategoryService{
    [self showHUDOnView];
    GiftCategoryService *service = [[GiftCategoryService alloc]init];
    [service callGiftCategoryService:self.criteria callback:^(NSError *error, GiftCategoryResponse *objGiftCatResponse) {
    if(error)return ;
    else{
    giftCategoryArray = objGiftCatResponse.giftCatDataArray;
    
        
    [categoryTV reloadData];
    [self hideHUDOnView];

            
  //  [categoryTV reloadData];
    }

    }];
}

-(void)fetchDataFromDBForMyLikes{
    
    [myLikesDataArray removeAllObjects];
    
    DBManager *dbManager = [[DBManager alloc]initWithDatabaseFilename:@"dbgiftdata.sql"];
    
    NSString * likeQuery = [NSString stringWithFormat:@"SELECT FREEBIEOFFERID FROM GIFTTABLE WHERE FREEBIELIKE=\"%d\"",1];
   // NSLog(@"MYLIKES :%@ %lu",[dbManager loadDataFromDB:likeQuery],(unsigned long)[dbManager loadDataFromDB:likeQuery].count);
    
    
    
    for(NSArray *arr in [dbManager loadDataFromDB:likeQuery]){
    [myLikesDataArray addObject:[arr objectAtIndex:0]];
    }

    NSArray *myLikeArray = [[Session shared].objSignInResponse.offerLikes componentsSeparatedByString:@","];
    
    if(myLikeArray.count > 0){
    for(int i=0;i<myLikeArray.count;i++){
    [myLikesDataArray addObject:[myLikeArray objectAtIndex:i]];
     }
     }
    
 //   NSLog(@"array :%@",myLikesDataArray);
    
    NSMutableArray *discardedItems = [NSMutableArray array];

    
    for(NSString *Str in myLikesDataArray){
    NSString * likeQuery = [NSString stringWithFormat:@"SELECT * FROM GIFTTABLE WHERE FREEBIEOFFERID=\"%@\"",Str];
    if([dbManager loadDataFromDB:likeQuery].count == 0){
    [discardedItems addObject:Str];
    }
    //NSLog(@"MYLIKES :%@ %lu %@",myLikesDataArray,(unsigned long)[dbManager loadDataFromDB:likeQuery].count,discardedItems);

    }
    [myLikesDataArray removeObjectsInArray:discardedItems];

   // NSLog(@"#ArrayCount :%@ %lu",myLikesDataArray,(unsigned long)myLikesDataArray.count);

    
    if(myLikesDataArray.count >0){
    noMyLikesImgView.hidden = TRUE;
    noMyLikesLabel.hidden = TRUE;
    }
        
 
   // offer_likes" = "886,885,747,765,638";


    
  //  NSLog(@"#OfferLikes is :%@",[Session shared].objSignInResponse.offerLikes);
    
    [myLikesTV reloadData];
}

-(void)fetchAllDataFromDBForNearYou{
    
    
    if(![CLLocationManager locationServicesEnabled]){
        
        NSLog(@"Location Services Enabled");
        
        
        
        if([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied){
            UIAlertView *   alert = [[UIAlertView alloc] initWithTitle:@"App Permission Denied"
                                                               message:@"To re-enable, please go to Settings and turn on Location Service for this app."
                                                              delegate:nil
                                                     cancelButtonTitle:@"OK"
                                                     otherButtonTitles:nil];
            
            alert.delegate = self;
            [alert show];
            
        }
    }
    
    else{
    [nearYouGiftArray removeAllObjects];
    [shopLatArray removeAllObjects];
    [shopLongArray removeAllObjects];
    [offerIDSArray removeAllObjects];
    [distanceArray removeAllObjects];
    
   DBManager *dbManager = [[DBManager alloc]initWithDatabaseFilename:@"dbgiftdata.sql"];

   NSString* query = [NSString stringWithFormat:@"SELECT * FROM GIFTTABLE"];
    
    NSArray *theArray = [[NSArray alloc] initWithArray:[dbManager loadDataFromDB:query]];
    
  //  NSLog(@"#TheArrayCount :%d",theArray.count);
    
    NSInteger indexOfLatURL = [dbManager.arrColumnNames indexOfObject:@"FREEBIELAT"];
    for(int i=0;i<theArray.count;i++){
    [shopLatArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfLatURL]];
    }

    NSInteger indexOfLongURL = [dbManager.arrColumnNames indexOfObject:@"FREEBIELNG"];
    for(int i=0;i<theArray.count;i++){
    [shopLongArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfLongURL]];
    }
    
    NSInteger indexOfOfferIDURL = [dbManager.arrColumnNames indexOfObject:@"FREEBIEOFFERID"];
    for(int i=0;i<theArray.count;i++){
    [offerIDSArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfOfferIDURL]];
    }
    
   // NSLog(@"ShopLatArray :%d",shopLatArray.count);
    
    
    for(int i=0;i<offerIDSArray.count;i++){
        
    CLLocation *location = [CurrentLocation sharedLocation].currentLocation;
        
   
   // CLLocation *location1 = [[CLLocation alloc] initWithLatitude:19.183911 longitude:72.83177];
   CLLocation *location1 = [[CLLocation alloc] initWithLatitude:location.coordinate.latitude longitude:location.coordinate.longitude];

    CLLocation *location2 = [[CLLocation alloc] initWithLatitude:[[shopLatArray objectAtIndex:i] floatValue] longitude:[[shopLongArray objectAtIndex:i] floatValue]];
        
    float distanceVal = [location1 distanceFromLocation:location2]/1000;
        
    if(distanceVal <= 15){
    distanceDic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:[offerIDSArray objectAtIndex:i],@"OffID", [NSNumber numberWithFloat:distanceVal],@"distance",nil];
    
    [distanceArray addObject:distanceDic];
    }
   // [distanceDic setObject:[offerIDSArray objectAtIndex:i] forKey:[NSNumber numberWithFloat:distanceVal]];
        
     
    }
    
    //  NSLog(@"distanceVal is :%@ ",distanceDic);

    if(distanceArray.count >0){
    NSArray *array = [NSArray arrayWithArray:[distanceArray mutableCopy]];
    
    NSSortDescriptor *sortDescriptor;
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"distance" ascending:YES];
    
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    array = [array sortedArrayUsingDescriptors:sortDescriptors];
    
    NSLog(@"#TheArrayis :%@",array);
    
    
    nearYouGiftArray = [array mutableCopy];
        
 //   NSLog(@"#NearYouGiftArray :%@",nearYouGiftArray);
        
    noGiftNearYouImgView.hidden = FALSE;
    noGiftNearYouLabel.hidden = TRUE;
        
    noGiftNearYouImgView.hidden = TRUE;
    noGiftNearYouLabel.hidden = TRUE;
        
    nearYouTV.hidden = FALSE;
        
    [self hideHUDOnView];

    
    [nearYouTV reloadData];
    }
    
    else{
    [self hideHUDOnView];
    }
    }
}


#pragma BUtton Actions

-(IBAction)giftClicked:(id)sender{
    [giftDataArray removeAllObjects];
    
   // int contentOffSet = pageScrollView.contentOffset.x ;

    
   /*if((giftSelectType == kSelectedTypeMyLikes && contentOffSet == 1500) || (giftSelectType == kSelectedTypeMyGift && contentOffSet == 1125) || (giftSelectType == kSelectedTypeCategories && contentOffSet == 750) ||  (giftSelectType == kSelectedTypeNearYou && contentOffSet == 375)){
    [pageScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    }*/
    
    [pageScrollView setContentOffset:CGPointMake(0, 0) animated:YES];

    
    
    giftSelectType = kSelectedTypeGift;
    [self setTextColorAccrdingToClick:kSelectedTypeGift];
    [self callGiftAPIService];
    
}

-(IBAction)nearYouClicked:(id)sender{
    
   // int contentOffSet = pageScrollView.contentOffset.x ;

    
   /* if((giftSelectType == kSelectedTypeMyLikes && contentOffSet == 1500) || (giftSelectType == kSelectedTypeMyGift && contentOffSet == 1125) || (giftSelectType == kSelectedTypeCategories && contentOffSet == 750) ||  (giftSelectType == kSelectedTypeGift && contentOffSet == 0)){
        [pageScrollView setContentOffset:CGPointMake(375, 0) animated:YES];
    }*/
    
    [pageScrollView setContentOffset:CGPointMake(375, 0) animated:YES];

    
    giftSelectType = kSelectedTypeNearYou;
    [self setTextColorAccrdingToClick:kSelectedTypeNearYou];
}

-(IBAction)categoriesClicked:(id)sender{
    
  //  int contentOffSet = pageScrollView.contentOffset.x ;
    
    
   /* if((giftSelectType == kSelectedTypeMyLikes && contentOffSet == 1500) || (giftSelectType == kSelectedTypeMyGift && contentOffSet == 1125) || (giftSelectType == kSelectedTypeGift && contentOffSet == 0) ||  (giftSelectType == kSelectedTypeNearYou && contentOffSet == 375)){
        [pageScrollView setContentOffset:CGPointMake(750, 0) animated:YES];
    }*/
    [pageScrollView setContentOffset:CGPointMake(750, 0) animated:YES];

    
    
    giftSelectType = kSelectedTypeCategories;
    [self setTextColorAccrdingToClick:kSelectedTypeCategories];
}

-(IBAction)myGiftsCLicked:(id)sender{
    
    
  //  int contentOffSet = pageScrollView.contentOffset.x ;
    
    /*if((giftSelectType == kSelectedTypeMyLikes && contentOffSet == 1500) || (giftSelectType == kSelectedTypeGift && contentOffSet == 0) || (giftSelectType == kSelectedTypeCategories && contentOffSet == 750) ||  (giftSelectType == kSelectedTypeNearYou && contentOffSet == 375)){
        [pageScrollView setContentOffset:CGPointMake(1125, 0) animated:YES];
    }*/
    
    [pageScrollView setContentOffset:CGPointMake(1125, 0) animated:YES];

    
    giftSelectType = kSelectedTypeMyGift;
    [self setTextColorAccrdingToClick:kSelectedTypeMyGift];
}

-(IBAction)myLikesClicked:(id)sender{
    
 //   int contentOffSet = pageScrollView.contentOffset.x ;

    
   /* if((giftSelectType == kSelectedTypeGift && contentOffSet == 0) || (giftSelectType == kSelectedTypeNearYou && contentOffSet == 375) || (giftSelectType == kSelectedTypeCategories && contentOffSet == 750) ||  (giftSelectType == kSelectedTypeMyGift && contentOffSet == 1125)){
    }*/
    [pageScrollView setContentOffset:CGPointMake(1500, 0) animated:YES];

    
    
    giftSelectType = kSelectedTypeMyLikes;
    [self setTextColorAccrdingToClick:kSelectedTypeMyLikes];
}



-(void)moneyButtonClicked:(id)sender{
    NSLog(@"#MoneyClicked");
    
    AppDelegate *appDelegate =  (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [appDelegate.tabBarController setSelectedIndex:3];

}
-(void)coinButtonClicked:(id)sender{
    NSLog(@"#CoinClicked");
    
    CoinViewController *coinVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CoinView"];
    [Session shared].isFromSlide = FALSE;
    coinVC.hidesBottomBarWhenPushed = TRUE;
    [self.navigationController pushViewController:coinVC animated:YES];
}

-(void)searchBtnClicked:(id)sender{
    NSLog(@"#SearchClicked");

    [self showSearchBarOnTheView];

}

#pragma  mark - Set PageView.

-(void)setPageView{
    
   // pageScrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height + 100);
    
    pageScrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height -280);

    
    headerView.frame = CGRectMake(0, 64, self.view.frame.size.width, 50);
    
    
    giftButton.frame = CGRectMake(10,-50, 50, 30);
    nearYouButton.frame = CGRectMake(giftButton.frame.origin.x + giftButton.frame.size.width + 5,-50,  100, 30);
    categoryButton.frame = CGRectMake(nearYouButton.frame.origin.x + nearYouButton.frame.size.width + 5,-50,100, 30);
    myGiftButton.frame = CGRectMake(categoryButton.frame.origin.x + categoryButton.frame.size.width + 5,-50,  100, 30);
    myLikesButton.frame = CGRectMake(myGiftButton.frame.origin.x + myGiftButton.frame.size.width + 5,-50,  100, 30);
    
    underLineView.frame = CGRectMake(10,giftButton.frame.origin.y + giftButton.frame.size.height, giftButton.frame.size.width, 2);

    headerScrollView.contentSize = CGSizeMake(headerView.frame.size.width + 200, -80);
    
    pageScrollView.pagingEnabled = YES;
    pageScrollView.showsHorizontalScrollIndicator = NO;
    pageScrollView.delegate = self;
  //  [pageScrollView setContentSize:CGSizeMake(pageScrollView.frame.size.width * 5, pageScrollView.frame.size.height)];

    [pageScrollView setContentSize:CGSizeMake(pageScrollView.frame.size.width * 5, pageScrollView.frame.size.height)];

}

#pragma mark - UITableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(giftSelectType == kSelectedTypeGift)
    return giftDataArray.count;
    else if(giftSelectType == kSelectedTypeNearYou)
    return nearYouGiftArray.count;
    else if(giftSelectType == kSelectedTypeCategories)
    return giftCategoryArray.count;
    else if(giftSelectType == kSelectedTypeMyGift)
    return myGiftDataArray.count;
    else if(giftSelectType == kSelectedTypeMyLikes)
    return myLikesDataArray.count;
    
    else
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier1 = @"GiftCustomCell";
    static NSString *CellIdentifier2 = @"GiftCategoryCustomCell";
    static NSString *CellIdentifier4 = @"MyGiftsCustomCell";
    static NSString *CellIdentifier5 = @"MyLikesCustomCell";

    __block GiftsViewController *vc = self;

    
    if(giftSelectType == kSelectedTypeGift){
    objGiftCell=(GiftCustomCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
        
    if(objGiftCell==nil){
    [[NSBundle mainBundle] loadNibNamed:@"GiftCustomCell" owner:self options:nil];
            
    }
        
        
    GiftList *objGiftList = [giftDataArray objectAtIndex:indexPath.row];
    NSLog(@"objGiftList :%@",objGiftList.giftID);
    [objGiftCell setOnImageViewClick:^(NSString* dealID,NSString *offerID,NSString *shopTxt) {
    
    [vc navigateToProductDetailView:dealID offID:offerID shopText:shopTxt];
    }];
        
    [objGiftCell setOnSeeAllClick:^(NSString *gOfferName, NSString *gOfferID,NSString *gOffVoucher,NSInteger seeTagValue) {
     __block GiftList *objGiftList = nil;
     objGiftList = [giftDataArray objectAtIndex:seeTagValue];
    [vc navigateToGiftViewAllView:gOfferName offID:gOfferID offVoucer:gOffVoucher allOffID:objGiftList.giftID isFrom:FALSE];

    }];
       
        
        
        
    [objGiftCell loadGiftDataOnTableView:objGiftList rowIndex:indexPath.row];
    NSLog(@"GiftCells");
    return objGiftCell;
    }
    
    
    
    if(giftSelectType == kSelectedTypeCategories){
    objGiftCatCustomCell=(GiftCategoryCustomCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier2];
        
    if(objGiftCatCustomCell==nil){
    [[NSBundle mainBundle] loadNibNamed:@"GiftCategoryCustomCell" owner:self options:nil];
            
    }
    GiftCategoryList *objGiftCatList = [giftCategoryArray objectAtIndex:indexPath.row];
    [objGiftCatCustomCell setOnImageViewClick:^(NSString* dealID,NSString *offerID,NSString *shopTxt) {
    [vc navigateToProductDetailView:dealID offID:offerID shopText:shopTxt];
    }];
        
    [objGiftCatCustomCell loadGiftDataOnTableView:objGiftCatList rowIndex:indexPath.row];
       
        

        
    [objGiftCatCustomCell setOnSeeAllButtonClick:^(NSString *gOfferName, NSString *gOfferID,NSString *giftEVoucher,NSInteger seeTagValue) {
    __block GiftCategoryList *objGiftCatList = nil;
     objGiftCatList = [giftCategoryArray objectAtIndex:seeTagValue];

    [vc navigateToGiftViewAllView:gOfferName offID:gOfferID offVoucer:giftEVoucher allOffID:objGiftCatList.catID isFrom:TRUE];
    }];
    return objGiftCatCustomCell;
    }
    
    
    
    
    if(giftSelectType == kSelectedTypeMyGift){
    objMyGiftCell=(MyGiftsCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier4];
        
    if(objMyGiftCell==nil){
    [[NSBundle mainBundle] loadNibNamed:@"MyGiftsCell" owner:self options:nil];
            
    }
    GetMyDealList *objMyDealList = [myGiftDataArray objectAtIndex:indexPath.row];
    [objMyGiftCell showMyGiftDeal:objMyDealList];
    return objMyGiftCell;
    }
    
    
    
    if(giftSelectType == kSelectedTypeMyLikes){
    objMyLikesCell=(MyLikesCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier5];
        
    if(objMyLikesCell==nil){
    [[NSBundle mainBundle] loadNibNamed:@"MyLikesCell" owner:self options:nil];
    }
    NSString *myLikeValue = [myLikesDataArray objectAtIndex:indexPath.row];
    
    [objMyLikesCell fetchMyLikesDataFromDb:myLikeValue];
    [objMyLikesCell setOnTableRowClick:^(NSString* dealID,NSString *offerID,NSString *shopNameTxt) {
    [vc navigateToProductDetailView:dealID offID:offerID shopText:shopNameTxt];
    }];
       
    objMyLikesCell.heartButton.hidden = FALSE;
    objMyLikesCell.heartBGButton.hidden = FALSE;

    
    for(int i=0;i<myLikesDataArray.count;i++){
    if(objMyLikesCell.tag == [[myLikesDataArray objectAtIndex:i] integerValue]){
    NSLog(@"++++++");
    [objMyLikesCell.heartButton setBackgroundImage:[UIImage imageNamed:@"red_heart.png"] forState:UIControlStateNormal];
                
    }
            
    }

    [objMyLikesCell setOnHeartClick:^{
    [vc callMyLikeService:indexPath.row];
    }];
        
    return objMyLikesCell;
    }
    
    if(giftSelectType == kSelectedTypeNearYou){
    objMyLikesCell=(MyLikesCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier5];
        
    if(objMyLikesCell==nil){
    [[NSBundle mainBundle] loadNibNamed:@"MyLikesCell" owner:self options:nil];
    }
    NSString *myLikeValue = [[nearYouGiftArray objectAtIndex:indexPath.row]objectForKey:@"OffID"];
        
    objMyLikesCell.heartButton.hidden = TRUE;
    objMyLikesCell.heartBGButton.hidden = TRUE;
        
        
    [objMyLikesCell fetchMyLikesDataFromDb:myLikeValue];
    [objMyLikesCell setOnTableRowClick:^(NSString* dealID,NSString *offerID,NSString *shopNameTxt) {
    [vc navigateToProductDetailView:dealID offID:offerID shopText:shopNameTxt];
    }];
        
    return objMyLikesCell;
    }

    
    return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView == categoryTV){
    return 241;
    }
    if(tableView == giftTV){
    return 240;
    }
    else
    return 260;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:TRUE];
    if(tableView == myGiftsTV){
    GetMyDealList *objMyDealList = [myGiftDataArray objectAtIndex:indexPath.row];
    NSLog(@"#MyGiftsGiftID :%@",objMyDealList.dealID);
        
    GiftDetailViewController *giftDetailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"giftDetailViewC"];
    giftDetailVC.giftID = objMyDealList.dealID;
    giftDetailVC.giftOfferID =  objMyDealList.offerID;//[offerIDArray objectAtIndex:indexPath.row];
    giftDetailVC.shopName = objMyDealList.dealName;
    giftDetailVC.objMyGiftDealList = objMyDealList;
    giftDetailVC.giftType = kSelectedTypeMyGift;
    checkFromWhere = TRUE;
    giftDetailVC.hidesBottomBarWhenPushed = TRUE;
    [self.navigationController pushViewController:giftDetailVC animated:YES];
    }

}

#pragma mark - Navigation Methods

-(void)navigateToProductDetailView:(NSString *)theDealID offID:(NSString *)offID shopText:(NSString *)shopText{
    GiftDetailViewController *giftDetailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"giftDetailViewC"];
    giftDetailVC.giftID = theDealID;
    giftDetailVC.giftOfferID = offID;
    NSLog(@"Values :%@ %@ %@",theDealID,offID,shopText);
    giftDetailVC.shopName = shopText;
    checkFromWhere = TRUE;
    giftDetailVC.hidesBottomBarWhenPushed = TRUE;

    [self.navigationController pushViewController:giftDetailVC animated:YES];
}

-(void)navigateToGiftViewAllView:(NSString *)offName offID:(NSString *)offID offVoucer:(NSString *)offVoucher allOffID:(NSString *)allOffID isFrom:(BOOL)isFrom{
    TopOfferViewController *topOfferVC = [self.storyboard instantiateViewControllerWithIdentifier:@"topOfferViewC"];
    topOfferVC.giftOfferName = offName;
    topOfferVC.giftOfferID = offID;
    topOfferVC.giftOfferVoucher = offVoucher;
    topOfferVC.giftOfferTextID = allOffID;
    checkFromWhere = TRUE;
    [Session shared].isFromSearch = FALSE;
    topOfferVC.isComingFrom = isFrom;
    
    topOfferVC.hidesBottomBarWhenPushed = TRUE;
    NSLog(@"Values :%@ %@ %@ %@",offID,offName,offVoucher,allOffID);
    [self.navigationController pushViewController:topOfferVC animated:YES];
}

#pragma mark -  ScrollView

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    int contentOffset = pageScrollView.contentOffset.x;
    
 //   NSLog(@"#Scroll :%d",contentOffset);
    if(scrollView == pageScrollView){
    if(contentOffset == 0){
    NSLog(@"GiftArrayCount :%@",giftDataArray);
      
    giftSelectType = kSelectedTypeGift;
    [self setTextColorAccrdingToClick:kSelectedTypeGift];
    }
        
    if(contentOffset == 375){
        
    giftSelectType = kSelectedTypeNearYou;
    [self setTextColorAccrdingToClick:kSelectedTypeNearYou];
    }
        
    if(contentOffset == 750){
    giftSelectType = kSelectedTypeCategories;
    [self setTextColorAccrdingToClick:kSelectedTypeCategories];
    }
        
    if(contentOffset == 1125){
    giftSelectType = kSelectedTypeMyGift;
    [self setTextColorAccrdingToClick:kSelectedTypeMyGift];
    }
        
    if(contentOffset == 1500){
    giftSelectType = kSelectedTypeMyLikes;
   [self setTextColorAccrdingToClick:kSelectedTypeMyLikes];
   }
}
}





-(void)change:(NSTimer *)timer{
    static int count=0;
    count+=1;
  //  NSLog(@"Count is :%d %d",count,imageURLArray.count);
    
    if(count==imageURLArray.count){
    count = 0;
    CGRect page = CGRectMake(self.view.frame.size.width*count, 0, self.view.frame.size.width, 200);
    [giftScrollView scrollRectToVisible:page animated:YES];
    }
    else{
    CGRect page = CGRectMake(self.view.frame.size.width*count, 0, self.view.frame.size.width, 200);
    [giftScrollView scrollRectToVisible:page animated:YES];
    }
}

-(void)showSearchBarOnTheView{
    if([Session shared].isFromSearch == FALSE){
   
    coverView =  [self onAddDimViewOnSuperView:self.view];
    UITapGestureRecognizer *gesRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(viewTapped:)];
    gesRecognizer.numberOfTapsRequired = 1;
    [coverView addGestureRecognizer:gesRecognizer];
        
    self.navigationController.navigationBar.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.4];
    }
    
    theSearchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(30, 0, self.view.frame.size.width - 50, 50)];
    theSearchBar.delegate = self;
    theSearchBar.backgroundColor = [UIColor whiteColor];
    if([Session shared].isFromSearch == TRUE){
    theSearchBar.text = searchBarText;
    }
    else
    theSearchBar.placeholder = @"Search";
    
    [theSearchBar becomeFirstResponder];
    [self.navigationController.navigationBar addSubview:theSearchBar];
}

#pragma mark 

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
   // [searchBar removeFromSuperview];
    searchBarText = searchBar.text;
    TopOfferViewController *topOfferVC = [self.storyboard instantiateViewControllerWithIdentifier:@"topOfferViewC"];
    topOfferVC.searchKeyWord = theSearchBar.text;
    [Session shared].isFromSearch = TRUE;
    topOfferVC.hidesBottomBarWhenPushed = TRUE;
    [self.navigationController pushViewController:topOfferVC animated:YES];
    
    [searchBar resignFirstResponder];
    
}

-(void)callMyLikeService:(NSInteger )index{
    MyLikeService *service = [[MyLikeService alloc]init];
    
    NSString *myLikeValue = [myLikesDataArray objectAtIndex:index];
    
    NSLog(@"#LikeUnLike :%@",myLikeValue);
    
    
    [service callMyLikeService:myLikeValue callback:^(NSError *error, MyLikeResponse *likeResponse) {
    if(error)return ;
    else{
    if(likeResponse.likeStatus == TRUE){
    NSLog(@"Check It's Like");
    [self updateLikeContentInDB:index likeValue:@"1"];
    }
    if(likeResponse.likeStatus == FALSE){
    NSLog(@"Check It's UnLike");
    [self updateLikeContentInDB:index likeValue:@"0"];
    }
    else
    NSLog(@"Error!");
    }
    }];
}

-(void)updateLikeContentInDB:(NSInteger)index likeValue:(NSString *)likeValue{
  DBManager*  dbManager = [[DBManager alloc] initWithDatabaseFilename:@"dbgiftdata.sql"];

    NSString *myLikeValue = [myLikesDataArray objectAtIndex:index];

    NSLog(@"#LikeUnLike :%@",myLikeValue);
    
    
    NSString *query = [NSString stringWithFormat:@"UPDATE GIFTTABLE SET FREEBIELIKE=\"%d\" WHERE FREEBIEOFFERID=\"%@\"",[likeValue intValue],[myLikesDataArray objectAtIndex:index]];
    NSLog(@"#QueryLikeUnLike :%@",query);
    [dbManager executeQuery:query];
    
    if (dbManager.affectedRows != 0) {
    NSLog(@"Query was executed successfully. Affected rows = %d", dbManager.affectedRows);
        //[delegate editingInfoWasFinished];
    }
    else{
    NSLog(@"Could not execute the query.");
    }
    
 


    if(giftSelectType == kSelectedTypeMyLikes){
    dispatch_async(dispatch_get_main_queue(), ^{
    [self fetchDataFromDBForMyLikes];
    });
    }
    
   /* if(giftSelectType == kSelectedTypeNearYou){
    dispatch_async(dispatch_get_main_queue(), ^{
            
    DBManager *dbManager = [[DBManager alloc]initWithDatabaseFilename:@"giftsdb.sql"];
            
    NSString * likeQuery = [NSString stringWithFormat:@"SELECT FREEBIEOFFERID FROM GIFTSTABLE WHERE FREEBIELIKE=\"%d\"",1];
    NSLog(@"MYLIKES :%@",[dbManager loadDataFromDB:likeQuery]);
            
            
            
    for(NSArray *arr in [dbManager loadDataFromDB:likeQuery]){
    NSLog(@"array :%@",[arr objectAtIndex:0]);
    [giftDataArray addObject:[arr objectAtIndex:0]];
                
    if(giftDataArray.count >0){
                   
    }
                
                
    }

            
            
    [nearYouTV reloadData];
            
    });
    }*/
}


-(void)viewWillDisappear:(BOOL)animated{
    [self invalidateTimerBlock];
}

-(void)invalidateTimerBlock{
    [imageTimer invalidate];
    imageTimer = nil;
}

-(void)setTextColorAccrdingToClick:(BOOL)isClickedType{
   
    
    if(giftSelectType == kSelectedTypeGift){
    [ headerScrollView setContentOffset:CGPointMake(headerScrollView.contentOffset.x, -64) animated:YES];

        
   // isGiftClickedType = FALSE;
    [myLikesButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [myGiftButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [categoryButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [nearYouButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [giftButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    underLineView.frame = CGRectMake(giftButton.frame.origin.x - 2,giftButton.frame.origin.y + giftButton.frame.size.height, giftButton.frame.size.width - 2, 2);
    
    }
    
    if(giftSelectType == kSelectedTypeNearYou){
    [self invalidateTimerBlock];
        
    [ headerScrollView setContentOffset:CGPointMake(headerScrollView.contentOffset.x, -64) animated:YES];

        
  //  isGiftClickedType = FALSE;
        
    [nearYouButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [myGiftButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [categoryButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [giftButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [myLikesButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        
    underLineView.frame = CGRectMake(nearYouButton.frame.origin.x -5,nearYouButton.frame.origin.y + nearYouButton.frame.size.height, nearYouButton.frame.size.width - 5, 2);
    
    [self fetchAllDataFromDBForNearYou];

    }
    
    if(giftSelectType == kSelectedTypeCategories){
        
    NSLog(@"#HeaderScrollview %f %f",headerScrollView.contentOffset.x,headerScrollView.contentOffset.y);
        
    if(isGiftClickedType)
    [ headerScrollView setContentOffset:CGPointMake(0, -64) animated:YES];
    if(!isGiftClickedType)
    [ headerScrollView setContentOffset:CGPointMake(150, -64) animated:YES];
        
  //  [ headerScrollView setContentOffset:CGPointMake(headerScrollView.contentOffset.x, -64) animated:YES];


        

    [myLikesButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [myGiftButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [categoryButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [nearYouButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [myGiftButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        
        
    underLineView.frame = CGRectMake(categoryButton.frame.origin.x - 2,categoryButton.frame.origin.y + categoryButton.frame.size.height, categoryButton.frame.size.width -2, 2);
        
    if(isLoadedOnce == TRUE){
    isLoadedOnce = FALSE;
    [self callGiftCategoryService];

    }
        
    }
    
    if(giftSelectType == kSelectedTypeMyGift){
        
  //  isGiftClickedType = TRUE;
        
    [ headerScrollView setContentOffset:CGPointMake(headerScrollView.contentOffset.x, -64) animated:YES];


    [myLikesButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [myGiftButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [categoryButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [nearYouButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [giftButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        
        
    underLineView.frame = CGRectMake(myGiftButton.frame.origin.x - 2,myGiftButton.frame.origin.y + myGiftButton.frame.size.height, myGiftButton.frame.size.width -2, 2);
        
    if([Constant getMyGiftsList].count > 0){
    myGiftDataArray = [[Constant getMyGiftsList] mutableCopy];
    [myGiftsTV reloadData];
            
    noMyGiftsImgView.hidden = TRUE;
    noMyGiftsLabel.hidden = TRUE;
    }
    [self callGetMyDealServiceBlock];

    }
    
    if(giftSelectType == kSelectedTypeMyLikes){
        
       // NSLog(@"#ScrollView :%f %f",headerScrollView.contentOffset.x , headerScrollView.contentOffset.y);
        [ headerScrollView setContentOffset:CGPointMake(headerScrollView.contentOffset.x, -64) animated:YES];

        
   // isGiftClickedType = TRUE;
        
    [myLikesDataArray removeAllObjects];
        
        



    [myLikesButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [myGiftButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [categoryButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [nearYouButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [giftButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];

    underLineView.frame = CGRectMake(myLikesButton.frame.origin.x,myLikesButton.frame.origin.y + myLikesButton.frame.size.height, myLikesButton.frame.size.width, 2);
    [self fetchDataFromDBForMyLikes];
 
    }
    
    
}

/*
- (void)revealController:(SWRevealViewController *)revealController willMoveToPosition:(FrontViewPosition)position
{
    if (position == FrontViewPositionRight) {
    NSLog(@"Done Right");
        
    coverView =  [self onAddDimViewOnSuperView:self.view];
        
    [self.view setUserInteractionEnabled:FALSE];
        
    [self.view addSubview:coverView];
    }
    
    if (position == FrontViewPositionLeft) {
    [self.view setUserInteractionEnabled:TRUE];
        
    [coverView removeFromSuperview];
    }
    
}
*/

-(void)viewTapped:(UITapGestureRecognizer *)recognzier{
    [Session shared].isFromSearch = FALSE;
    [theSearchBar removeFromSuperview];
    [theSearchBar resignFirstResponder];
    [coverView removeFromSuperview];
}


-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    pointNow = scrollView.contentOffset;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView.contentOffset.x<pointNow.x) {
        // NSLog(@"LEFT");
        isGiftClickedType = TRUE;
    }
    
    else if (scrollView.contentOffset.x>pointNow.x) {
        // NSLog(@"RIGHT");
        isGiftClickedType = FALSE;
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
