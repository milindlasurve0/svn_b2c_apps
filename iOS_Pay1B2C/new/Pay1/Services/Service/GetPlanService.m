//
//  GetPlanService.m
//  Pay1
//
//  Created by Annapurna on 05/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "GetPlanService.h"
#import "GetPlanResponse.h"
#import "PlanList.h"
#import "DBManager.h"
#import "GetMobileDataList.h"
#import "GetMobileResponse.h"
#import "Constant.h"
@implementation GetPlanService
-(void)callGetPlanService:(getPlanResponse)callback{
    NSMutableDictionary *postData = [[NSMutableDictionary alloc]init];
    [postData setObject:@"getPlanDetails" forKey:@"method"];
    [postData setObject:@"all" forKey:@"operator"];
    [postData setObject:@"all" forKey:@"circle"];
    
    NSLog(@"#PostDataForPlans :%@",postData);
    
    
    RequestData *requestData = [[RequestData alloc]init];
    requestData.baseURL = kMobilePlanBaseURL;
    requestData.webServiceURL = kGetMobileDataURL;
    requestData.postData = postData;
    requestData.showLoader = FALSE;
    
    requestData.requestSuccessBlock = ^(RequestData *request) {
        
    [self printResponse:request.responseData forRequest:@"GetPlan Response"];
    if ([self handleNullResponse:request])
    return;
        
        
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        // Add Hud Here. and remove it when its starts insertion.
        
        
    callback(nil, [self parseGetPlanResponse:request]);
    });
        
  //  callback(nil, [self parseGetPlanResponse:request]);
    };
    
    
    requestData.requestFailureBlock = ^(RequestData *request) {
    [self handleNetworkError:request.responseData];
    callback(request.responseData, nil);
    };
    
    [[ServiceManager sharedInstance] makeRequest:requestData];


}

-(GetPlanResponse *)parseGetPlanResponse:(RequestData *)request{
    
    
    DBManager *  objDBManager = [[DBManager alloc]initWithDatabaseFilename:@"plansdb.sql"];
    NSString *query;
    
    
   /* query = [NSString stringWithFormat:@"delete from plandatatable"];
    
    NSLog(@"#Query :%@",query);
    [objDBManager executeQuery:query];
    
    if (objDBManager.affectedRows != 0) {
        //  NSLog(@"Query was executed successfully.");
    }
    else{
        //  NSLog(@"Could not execute the query.");
    }*/
    
    
      
    GetPlanResponse *planResponse = [[GetPlanResponse alloc]init];
    NSArray *planArray = request.responseData;
    for(NSMutableDictionary *dic in planArray){
    planResponse.getPlansDic = dic;
        
    for(NSMutableDictionary *dataDic in [dic allValues]){

    planResponse.productCode = [dataDic objectForKey:@"prod_code_pay1"];
    planResponse.operatorName = [dataDic objectForKey:@"opr_name"];
    NSMutableDictionary *circleDic = [dataDic objectForKey:@"circles"];
              
    for(NSMutableDictionary *circleDataDic in [circleDic allValues])
    {
    PlanList *objPlanList = [[PlanList alloc]init];
    objPlanList.planCircleID = [circleDataDic objectForKey:@"circle_id"];
    objPlanList.planCircleName = [circleDataDic objectForKey:@"circle_name"];
        
    NSMutableDictionary *planDic  = [circleDataDic objectForKey:@"plans"];
        
        
    if([planDic valueForKey:@"Data/2G"]){
    NSMutableArray* twoGDataArray = [planDic valueForKey:@"Data/2G"];
    for(NSMutableDictionary *twoGDic in twoGDataArray){
    objPlanList.planType = @"Data/2G";
    objPlanList.planAmount = [twoGDic objectForKey:@"plan_amt"];
    objPlanList.planValidity = [twoGDic objectForKey:@"plan_validity"];
        
    NSString *dataDesc  = [[twoGDic objectForKey:@"plan_desc"] stringByReplacingOccurrencesOfString:@"\'" withString:@""];
    objPlanList.planDescription = dataDesc;
    objPlanList.planFlag = [twoGDic objectForKey:@"show_flag"];
   // NSLog(@"#Data/2G-Data :%@ %@ %@ %@",objPlanList.planAmount,objPlanList.planValidity,objPlanList.planDescription,objPlanList.planFlag);
        
        
    query = [NSString stringWithFormat:@"insert into tempplantable values(null,'%@','%@','%@','%@','%@','%ld','%@','%@','%@')",planResponse.productCode,planResponse.operatorName,objPlanList.planCircleID,objPlanList.planCircleName,objPlanList.planType,(long)[objPlanList.planAmount integerValue],objPlanList.planValidity,objPlanList.planDescription,objPlanList.planFlag];
        
   // NSLog(@"#Query 2G:%@",query);
    [objDBManager executeQuery:query];
        
    if (objDBManager.affectedRows != 0) {
   // NSLog(@"Query 2G was executed successfully.");
    }
    else{
    //NSLog(@"Could 2G not execute the query.");
    }
    
    }
    //[planResponse.allPlanDataArray addObject:objPlanList];
    }
                
    if([planDic valueForKey:@"Other"]){
    NSMutableArray* otherDataArray = [planDic valueForKey:@"Other"];
    for(NSMutableDictionary *otherDic in otherDataArray){
    objPlanList.planType = @"Other";
    objPlanList.planAmount = [otherDic objectForKey:@"plan_amt"];

    objPlanList.planValidity = [otherDic objectForKey:@"plan_validity"];
        
    NSString *otherDesc  = [[otherDic objectForKey:@"plan_desc"] stringByReplacingOccurrencesOfString:@"\'" withString:@""];
    
    objPlanList.planDescription = otherDesc;
    objPlanList.planFlag = [otherDic objectForKey:@"show_flag"];
  //  NSLog(@"#Other-Data :%@ %@ %@ %@",objPlanList.planAmount,objPlanList.planValidity,objPlanList.planDescription,objPlanList.planFlag);
        
    query = [NSString stringWithFormat:@"insert into tempplantable values(null,'%@','%@','%@','%@','%@','%ld','%@','%@','%@')",planResponse.productCode,planResponse.operatorName,objPlanList.planCircleID,objPlanList.planCircleName,objPlanList.planType,(long)[objPlanList.planAmount integerValue],objPlanList.planValidity,objPlanList.planDescription,objPlanList.planFlag];
        
    // NSLog(@"#Query Other:%@",query);
    [objDBManager executeQuery:query];
        
    if (objDBManager.affectedRows != 0) {
   // NSLog(@"Query Other was executed successfully.");
    }
    else{
   // NSLog(@"Could Other not execute the query.");
    }
        
    }
    
    //[planResponse.allPlanDataArray addObject:objPlanList];
    }
                
    if([planDic valueForKey:@"Topup"]){
    NSMutableArray* topupDataArray = [planDic valueForKey:@"Topup"];
    for(NSMutableDictionary *topUpDic in topupDataArray){
    objPlanList.planType = @"Topup";
    objPlanList.planAmount = [topUpDic objectForKey:@"plan_amt"];

    objPlanList.planValidity = [topUpDic objectForKey:@"plan_validity"];
        
    NSString *topupDesc  = [[topUpDic objectForKey:@"plan_desc"] stringByReplacingOccurrencesOfString:@"\'" withString:@""];
    
    objPlanList.planDescription = topupDesc;
    objPlanList.planFlag = [topUpDic objectForKey:@"show_flag"];
   // NSLog(@"#Topup-Data :%@ %@ %@ %@",objPlanList.planAmount,objPlanList.planValidity,objPlanList.planDescription,objPlanList.planFlag);
        
    query = [NSString stringWithFormat:@"insert into tempplantable values(null,'%@','%@','%@','%@','%@','%ld','%@','%@','%@')",planResponse.productCode,planResponse.operatorName,objPlanList.planCircleID,objPlanList.planCircleName,objPlanList.planType,(long)[objPlanList.planAmount integerValue],objPlanList.planValidity,objPlanList.planDescription,objPlanList.planFlag];
        
    // NSLog(@"#Query Topup:%@",query);
    [objDBManager executeQuery:query];
        
    if (objDBManager.affectedRows != 0) {
   // NSLog(@"Query Topup was executed successfully.");
    }
    else{
   // NSLog(@"Could Topup not execute the query.");
    }
        
    }
       // [planResponse.allPlanDataArray addObject:objPlanList];
    }
        
    if([planDic valueForKey:@"Topup-Plans"]){
    NSMutableArray* topupDataArray = [planDic valueForKey:@"Topup-Plans"];
    for(NSMutableDictionary *topUpDic in topupDataArray){
    objPlanList.planType = @"Topup-Plans";
    objPlanList.planAmount = [topUpDic objectForKey:@"plan_amt"];

    objPlanList.planValidity = [topUpDic objectForKey:@"plan_validity"];
        
     NSString *topupDesc  = [[topUpDic objectForKey:@"plan_desc"] stringByReplacingOccurrencesOfString:@"\'" withString:@""];

    objPlanList.planDescription = topupDesc;
    objPlanList.planFlag = [topUpDic objectForKey:@"show_flag"];
  //  NSLog(@"#Topup-PlansData :%@ %@ %@ %@",objPlanList.planAmount,objPlanList.planValidity,objPlanList.planDescription,objPlanList.planFlag);
        
    query = [NSString stringWithFormat:@"insert into tempplantable values(null,'%@','%@','%@','%@','%@','%ld','%@','%@','%@')",planResponse.productCode,planResponse.operatorName,objPlanList.planCircleID,objPlanList.planCircleName,objPlanList.planType,(long)[objPlanList.planAmount integerValue],objPlanList.planValidity,objPlanList.planDescription,objPlanList.planFlag];
        
  //  NSLog(@"#Query TopupPlans:%@",query);
    [objDBManager executeQuery:query];
        
    if (objDBManager.affectedRows != 0) {
   // NSLog(@"Query TopupPlans was executed successfully.");
    }
    else{
   // NSLog(@"Could TopupPlans not execute the query.");
    }
        
    }
    
   // [planResponse.allPlanDataArray addObject:objPlanList];
    }
                
    if([planDic valueForKey:@"3G"]){
    NSMutableArray* threeGDataArray = [planDic valueForKey:@"3G"];
    for(NSMutableDictionary *threeGDic in threeGDataArray){
    objPlanList.planType = @"3G";
    objPlanList.planAmount = [threeGDic objectForKey:@"plan_amt"];
    objPlanList.planValidity = [threeGDic objectForKey:@"plan_validity"];
    NSString *threeGDesc  = [[threeGDic objectForKey:@"plan_desc"] stringByReplacingOccurrencesOfString:@"\'" withString:@""];
    objPlanList.planDescription = threeGDesc;
    objPlanList.planFlag = [threeGDic objectForKey:@"show_flag"];
  //  NSLog(@"#OtherData :%@ %@ %@ %@",objPlanList.planAmount,objPlanList.planValidity,objPlanList.planDescription,objPlanList.planFlag);
        
    query = [NSString stringWithFormat:@"insert into tempplantable values(null,'%@','%@','%@','%@','%@','%ld','%@','%@','%@')",planResponse.productCode,planResponse.operatorName,objPlanList.planCircleID,objPlanList.planCircleName,objPlanList.planType,(long)[objPlanList.planAmount integerValue],objPlanList.planValidity,objPlanList.planDescription,objPlanList.planFlag];
        
  //  NSLog(@"#Query 3G:%@",query);
    [objDBManager executeQuery:query];
        
    if (objDBManager.affectedRows != 0) {
  //  NSLog(@"Query 3G was executed successfully.");
    }
    else{
   // NSLog(@"Could 3G not execute the query.");
    }
        
    }
    
    //[planResponse.allPlanDataArray addObject:objPlanList];
    }
        
     //   NSLog(@"#Arraycount :%lu",(unsigned long)planResponse.allPlanDataArray.count);
     

    }
        
    }
    }
    
  
    return planResponse;
}

#pragma mark - Mobile

-(void)callGetMobileService:(NSString *)systemTime callback:(getMobileResponse)callback{
    NSMutableDictionary *postData = [[NSMutableDictionary alloc]init];
    [postData setObject:@"getMobileDetails" forKey:@"method"];
    [postData setObject:@"all" forKey:@"operator"];
    [postData setObject:@"all" forKey:@"circle"];
    [postData setObject:systemTime forKey:@"timestamp"];
    
    NSLog(@"#PostDataForMobileDetail:%@",postData);
    
    RequestData *requestData = [[RequestData alloc]init];
    requestData.baseURL = kMobilePlanBaseURL;
    requestData.webServiceURL = kGetMobileDataURL;
    requestData.postData = postData;
    requestData.showLoader = FALSE;
    
    requestData.requestSuccessBlock = ^(RequestData *request) {
        
    [self printResponse:request.responseData forRequest:@"GetMobile Response"];
    if ([self handleNullResponse:request])
    return;
        
    callback(nil, [self parseGetMobileResponse:request]);
    };
    
    
    requestData.requestFailureBlock = ^(RequestData *request) {
    [self handleNetworkError:request.responseData];
    callback(request.responseData, nil);
    };
    
    [[ServiceManager sharedInstance] makeRequest:requestData];
}

-(GetMobileResponse *)parseGetMobileResponse:(RequestData *)request{
    
    
    GetMobileResponse *objResponse = [[GetMobileResponse alloc]init];
    NSMutableArray *array = request.responseData;
    
    for(NSMutableDictionary *dic in array){
    objResponse.status = [dic objectForKey:@"status"];
     
    if([objResponse.status isEqualToString:@"success"]){
            
 
        
    NSMutableArray *detailArray = [dic valueForKey:@"details"];
    if(detailArray.count > 0){
    for(NSMutableDictionary *detailDic in detailArray){
    GetMobileDataList *objMobileDataList = [[GetMobileDataList alloc]init];
        
    objMobileDataList.areaName = [detailDic objectForKey:@"area_name"];
    objMobileDataList.area = [detailDic objectForKey:@"area"];
    objMobileDataList.operatorName = [detailDic objectForKey:@"opr_name"];
    objMobileDataList.operatorID = [detailDic objectForKey:@"operator"];
    objMobileDataList.productID = [detailDic objectForKey:@"product_id"];
    objMobileDataList.startNumber = [detailDic objectForKey:@"number"];
    [objResponse.detailsArray addObject:objMobileDataList];
    }
      [self saveMobileDetailsInDB:objResponse];
    }
        
    
    }
    else{
        NSLog(@"#Do Nothing For Mobile Details");
    }

    }
    
    return objResponse;
}

-(void)saveMobileDetailsInDB:(GetMobileResponse *)mobResponse{
    DBManager* objMobDBManager = [[DBManager alloc]initWithDatabaseFilename:@"mobiledatadb.sql"];
    NSString *query;
    for(int i=0;i<mobResponse.detailsArray.count;i++){
    GetMobileDataList *objDataList = [mobResponse.detailsArray objectAtIndex:i];
    query = [NSString stringWithFormat:@"insert into NumberAndCircleTable values(null,'%@','%@','%@','%@','%@','%@','%@')",objDataList.area,objDataList.areaName,objDataList.operatorID,objDataList.operatorName,objDataList.productID,objDataList.startNumber,[Constant currentSystemDateTime]];
    [objMobDBManager executeQuery:query];
        
    }
    
    if (objMobDBManager.affectedRows != 0) {
        NSLog(@"Query was executed successfully.");
    }
    else{
        NSLog(@"Could not execute the query.");
    }
    
}

@end
