package com.pay1.adapterhandler;

import java.util.ArrayList;
import java.util.WeakHashMap;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pay1.R;
import com.pay1.imagehandler.ImageLoader;

public class MyFreeBiesAdapter extends BaseAdapter {

	private Context context;
	private ArrayList<WeakHashMap<String, String>> data;
	private ImageLoader imageLoader;
	private Typeface Reguler;

	public MyFreeBiesAdapter(Context context,
			ArrayList<WeakHashMap<String, String>> data) {
		this.context = context;
		this.data = data;
		imageLoader = new ImageLoader(context);
		Reguler = Typeface.createFromAsset(context.getAssets(),
				"EncodeSansNormal-400-Regular.ttf");
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		try {

			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.my_freebies_adapter, null);

			RelativeLayout relativeLayout_Freebies;
			ImageView imageView_Image;
			TextView textView_DealName, textView_Coupon, textView_Expity, textView_TitleCoupon, textView_TitleExpiry;
			ProgressBar progressBar1;

			relativeLayout_Freebies = (RelativeLayout) convertView
					.findViewById(R.id.relativeLayout_Freebies);
			imageView_Image = (ImageView) convertView
					.findViewById(R.id.imageView_Image);

			textView_DealName = (TextView) convertView
					.findViewById(R.id.textView_DealName);
			textView_DealName.setTypeface(Reguler);
			textView_Coupon = (TextView) convertView
					.findViewById(R.id.textView_Coupon);
			textView_Coupon.setTypeface(Reguler);
			textView_Expity = (TextView) convertView
					.findViewById(R.id.textView_Expity);
			textView_Expity.setTypeface(Reguler);
			textView_TitleCoupon = (TextView) convertView
					.findViewById(R.id.textView_TitleCoupon);
			textView_TitleCoupon.setTypeface(Reguler);
			textView_TitleExpiry = (TextView) convertView
					.findViewById(R.id.textView_TitleExpiry);
			textView_TitleExpiry.setTypeface(Reguler);
			progressBar1 = (ProgressBar) convertView
					.findViewById(R.id.progressBar1);

			relativeLayout_Freebies
					.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							try {
								// System.out.println("Position " +
								// position);
//								WeakHashMap<String, String> map = new WeakHashMap<String, String>();
//								map = data.get(position);
//								Intent intent = new Intent(context,
//										MyFreeBiesDetailsActivity.class);
//								intent.putExtra("IS_BUY", 0);
//								intent.putExtra("URL",
//										map.get(MyFreeBiesFragment.URL));
//								intent.putExtra("DEAL_ID",
//										map.get(MyFreeBiesFragment.DEAL_ID));
//								intent.putExtra("DEAL_NAME",
//										map.get(MyFreeBiesFragment.DEAL_NAME));
//								intent.putExtra("OFFER_NAME",
//										map.get(MyFreeBiesFragment.OFFER_NAME));
//								intent.putExtra("SHORT_DESC",
//										map.get(MyFreeBiesFragment.SHORT_DESC));
//								intent.putExtra("VOUCHER", map
//										.get(MyFreeBiesFragment.VOUCHER_CODE));
//								intent.putExtra("EXPIRY",
//										map.get(MyFreeBiesFragment.EXPIRY));
//								intent.putExtra(
//										"COUPON_STATUS_ID",
//										map.get(MyFreeBiesFragment.COUPON_STATUS_ID));
//
//								context.startActivity(intent);
							} catch (Exception e) {
							}
						}
					});
//			WeakHashMap<String, String> map = new WeakHashMap<String, String>();
//			map = data.get(position);
//			imageLoader.getDealImage(map.get(MyFreeBiesFragment.URL),
//					imageView_Image, progressBar1);
//			textView_DealName.setText(map.get(MyFreeBiesFragment.DEAL_NAME));
//			textView_Coupon.setText(map.get(MyFreeBiesFragment.COUPON_STATUS));
//			textView_Expity.setText(map.get(MyFreeBiesFragment.EXPIRY));
//			if (map.get(MyFreeBiesFragment.COUPON_STATUS_ID).equalsIgnoreCase(
//					"1"))
//				textView_Coupon.setTextColor(context.getResources().getColor(
//						R.color.Green));
//			convertView.setClickable(false);

		} catch (Exception e) {
		}
		return convertView;
	}

	// private class ViewHolder {
	// RelativeLayout relativeLayout_Freebies;
	// ImageView imageView_Image;
	// TextView textView_DealName, textView_Coupon, textView_Expity,
	// textView_TitleCoupon, textView_TitleExpiry;
	// ProgressBar progressBar1;
	// }

}