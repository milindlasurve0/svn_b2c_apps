//
//  CoinViewController.m
//  Pay1
//
//  Created by webninjaz on 01/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "CoinViewController.h"
#import "CoinHistoryService.h"
#import "CoinHistoryCriteria.h"
#import "CoinHistoryResponse.h"
#import "CoinTableViewCell.h"
#import "CoinHistoryList.h"
#import "HomeViewController.h"
@interface CoinViewController ()

@end

@implementation CoinViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    coinTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

   
    
    [self setNaviationButtonWithText:@"Gift Coin History" selector:@selector(backClicked:) isLeft:TRUE isReduceSize:TRUE isMyFont:18.0];
     [coinTableView removeFromSuperview];
    
    /*if([UIScreen mainScreen].bounds.size.width == 320){
    coinHistoryImageView.frame = CGRectMake([UIScreen mainScreen].bounds.size.width /2 -50, [UIScreen mainScreen].bounds.size.height/2 -100, 100, 100);
  //  coinTableView.frame = CGRectMake(0, 64, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height - 50);
    }*/
    
     [self showHUDOnView];
     [self callCoinHistoryServiceBlock];
}

-(void)callCoinHistoryServiceBlock{
    CoinHistoryService *coinHistoryService = [[CoinHistoryService alloc]init];
    [coinHistoryService callCoinHistoryService:^(NSError *error, CoinHistoryResponse *objCoinHistoryResponse) {
    NSLog(@"The wallet response is%@",objCoinHistoryResponse.descriptionCoinHistoryResponse);
        
    if([objCoinHistoryResponse.statusCoinHistoryResponse isEqualToString:@"failure"]){
    [self hideHUDOnView];

    NSLog(@"The Status get error");
    [coinTableView removeFromSuperview];
            
    }
    
    else{
        [self hideHUDOnView];

    NSLog(@"The status is Success");
    self.coinArray = objCoinHistoryResponse.coinHistoryResponse;
  //  NSLog(@"The transaction History response is%@",self.coinArray);
    [coinHistoryImageView removeFromSuperview];
    [coinHistoryLabel removeFromSuperview];
    [self.view addSubview:coinTableView];
    [coinTableView reloadData];
    }
        
    }];
    
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.coinArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"CoinTableViewCell";
    
    coinTableViewCell=(CoinTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(coinTableViewCell==nil){
        [[NSBundle mainBundle] loadNibNamed:@"CoinTableViewCell" owner:self options:nil];
    }
    
    CoinHistoryList *coinData = [self.coinArray objectAtIndex:indexPath.row];
    coinTableViewCell.dateTimeLabel.text = [Constant convertTheDateFormat:coinData.coinTransactionDateTime]; //[Constant convertTheDateFormat:@"2015-04-18 13:45:27"]; //[Constant convertTheDateFormat:coinData.coinTransactionDateTime];//coinData.coinTransactionDateTime;
    coinTableViewCell.transactionIDLabel.text = [NSString stringWithFormat:@"TRANSACTION ID: %@",coinData.coinTransactionID];
    
    if([coinData.coinRedeemStatus isEqualToString:@"1"]){
    coinTableViewCell.coinTransactionImageView.image = [UIImage imageNamed:@"coin_decrement.png"];
    coinTableViewCell.coinTransactionLabel.text = [NSString stringWithFormat:@"%@ coins redeemed",coinData.coinCoinsValue]; //coinData.coinName;
    coinTableViewCell.giftTxtLabel.text = [NSString stringWithFormat:@"Grabbed gift \"%@\"",coinData.coinName];
    }
    
    if([coinData.coinRedeemStatus isEqualToString:@"2"]){
    coinTableViewCell.coinTransactionImageView.image = [UIImage imageNamed:@"coin_decrement.png"];
    coinTableViewCell.coinTransactionLabel.text = [NSString stringWithFormat:@"%@ coins reversed",coinData.coinCoinsValue]; //coinData.coinName;
    coinTableViewCell.giftTxtLabel.text = [NSString stringWithFormat:@"Grabbed gift \"%@\"",coinData.coinName];
    }
    
    if([coinData.coinRedeemStatus isEqualToString:@"3"]){
    coinTableViewCell.coinTransactionImageView.image = [UIImage imageNamed:@"coin_added_history.png"];
    coinTableViewCell.coinTransactionLabel.text = [NSString stringWithFormat:@"%@ coins added",coinData.coinCoinsValue]; //coinData.coinName;
    if([coinData.coinRechargeType isEqualToString:@"pay1shop"]){
    NSMutableString *mu = [NSMutableString stringWithString:coinData.coinRechargeType];
    [mu insertString:@" " atIndex:4];
    coinTableViewCell.giftTxtLabel.text = [NSString stringWithFormat:@"%@ of Rs.%@\n(%@)",[mu mutableCopy],coinData.coinTransactionAmount,coinData.coinName];
    }
    else
    coinTableViewCell.giftTxtLabel.text = [NSString stringWithFormat:@"%@ of Rs.%@\n(%@)",coinData.coinRechargeType,coinData.coinTransactionAmount,coinData.coinName];
    }
    
    if([coinData.coinRedeemStatus isEqualToString:@"4"]){
    coinTableViewCell.coinTransactionImageView.image = [UIImage imageNamed:@"coin_decrement.png"];
    coinTableViewCell.coinTransactionLabel.text = [NSString stringWithFormat:@"%@ coins reversed",coinData.coinTransactionAmount]; //coinData.coinName;
    coinTableViewCell.giftTxtLabel.text = [NSString stringWithFormat:@"%@",coinData.coinOperator];
    }
    
    if([coinData.coinRedeemStatus isEqualToString:@"5"]){
    coinTableViewCell.coinTransactionImageView.image = [UIImage imageNamed:@"coin_added_history.png"];
    coinTableViewCell.coinTransactionLabel.text = [NSString stringWithFormat:@"%@ coins added",coinData.coinCoinsValue]; //coinData.coinName;
    coinTableViewCell.giftTxtLabel.text = [NSString stringWithFormat:@"%@",coinData.coinOperator];
    }
    
    
  //  coinTableViewCell.coinTransactionImageView.image = [UIImage imageNamed:@"coin_added_history.png"];
    
    
    coinTableViewCell.selectionStyle = UITableViewCellSelectionStyleNone;


    
    NSLog(@"The coin data is%@",coinData.coinName);
    
    
    return coinTableViewCell;
}




- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath;
{
    return 135;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(void)backClicked:(id)sender{
    
[self.navigationController popViewControllerAnimated:YES];
  }
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
