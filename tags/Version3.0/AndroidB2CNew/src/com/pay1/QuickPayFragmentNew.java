package com.pay1;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.database.SQLException;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.larvalabs.svgandroid.SVG;
import com.larvalabs.svgandroid.SVGParser;
import com.pay1.constants.Constants;
import com.pay1.customviews.GiftCellHomeLayout;
import com.pay1.customviews.GiftImageSpecialOffersLayout;
import com.pay1.databasehandler.FreeBies;
import com.pay1.databasehandler.FreeBiesDataSource;
import com.pay1.databasehandler.FreeBiesLocationDataSource;
import com.pay1.databasehandler.FreeBiesOrderDataSource;
import com.pay1.databasehandler.QuickPay;
import com.pay1.databasehandler.QuickPayDataSource;
import com.pay1.requesthandler.RequestClass;
import com.pay1.utilities.RoundedTransformation;
import com.pay1.utilities.Utility;
import com.squareup.picasso.Picasso;

@SuppressLint("NewApi")
public class QuickPayFragmentNew extends Fragment implements
		OnSharedPreferenceChangeListener {
	private static final String SCREEN_LABEL = "Quick Pay Screen";
	private EasyTracker easyTracker = null;
	LinearLayout linearLayout_Main, empty_layout;
	Typeface Medium, Regular;
	Typeface SemiBold;
	// TextView textViewBalance, textViewGiftCoins;
	private FreeBiesDataSource freeBiesDataSource;
	private FreeBiesLocationDataSource freeBiesLocationDataSource;
	private FreeBiesOrderDataSource freeBiesOrderDataSource;
	// Button buttonAddMoney, buttonPocketAGift, buttonnearMe;
	/*
	 * ImageView imageViewRecharge, imageViewNearMe,
	 * imageViewBalance,imageViewBalance1,
	 * imageViewCoins,imageViewCoins1,imageViewBoard;
	 */
	ImageView imageViewGift, imageViewRecharge, imageViewNearYou,
			imageViewInfo;
	/*
	 * LinearLayout layout_recharge, layout_add_money, layout_gift,
	 * layout_near_me,buttonRecharge;
	 */
	private QuickPayDataSource quickPayDataSource;
	LinearLayout layout_recharge, layout_gift;
	RelativeLayout layout_near_me;
	ListView listViewGift;

	public static QuickPayFragmentNew newInstance() {
		QuickPayFragmentNew fragment = new QuickPayFragmentNew();
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	/*
	 * public void updateBalance(Context mContext) { DecimalFormat decimalFormat
	 * = new DecimalFormat("0.00"); // Constants.SHAREDPREFERENCE_LOYALTY_POINTS
	 * String bal = Utility.getBalance(mContext,
	 * Constants.SHAREDPREFERENCE_BALANCE); bal = bal == null ? "0" : bal;
	 * textViewBalance.setText(decimalFormat.format(Double .parseDouble(bal)));
	 * textViewBalance.setTypeface(Medium); String loyalityPoints =
	 * Utility.getBalance(getActivity(),
	 * Constants.SHAREDPREFERENCE_LOYALTY_POINTS);
	 * textViewGiftCoins.setText(loyalityPoints); }
	 */

	@SuppressLint("NewApi")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.quickpay_fragment_new, container,
				false);
		try {
			Constants.setFont(getActivity(), (ViewGroup) view);
			easyTracker = EasyTracker.getInstance(getActivity());
			easyTracker.set(Fields.SCREEN_NAME, SCREEN_LABEL);

			SemiBold = Typeface.createFromAsset(getActivity().getAssets(),
					"OpenSans-Semibold.ttf");

			Medium = Typeface.createFromAsset(getActivity().getAssets(),
					"Roboto-Light.ttf");
			Regular = Typeface.createFromAsset(getActivity().getAssets(),
					"Roboto-Regular.ttf");
			quickPayDataSource = new QuickPayDataSource(getActivity());
			freeBiesDataSource = new FreeBiesDataSource(getActivity());
			freeBiesLocationDataSource = new FreeBiesLocationDataSource(
					getActivity());
			freeBiesOrderDataSource = new FreeBiesOrderDataSource(getActivity());
			linearLayout_Main = (LinearLayout) view
					.findViewById(R.id.gift_layout);
			listViewGift = (ListView) view.findViewById(R.id.listViewGift);
			layout_recharge = (LinearLayout) view
					.findViewById(R.id.layout_recharge);
			TextView textWelcome = (TextView) view
					.findViewById(R.id.textWelcome);
			textWelcome.setTypeface(SemiBold);
			layout_recharge.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					MainActivity.changeView(2, getActivity());
				}
			});

			layout_gift = (LinearLayout) view.findViewById(R.id.layout_gift);

			layout_near_me = (RelativeLayout) view
					.findViewById(R.id.layout_near_me);

			layout_near_me.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent intent = new Intent(getActivity(), MapActivity.class);
					getActivity().startActivity(intent);
				}
			});

			layout_gift.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					MainActivity.changeView(3, getActivity());
				}
			});

			imageViewInfo = (ImageView) view.findViewById(R.id.imageViewInfo);

			imageViewRecharge = (ImageView) view
					.findViewById(R.id.imageViewRecharge);
			imageViewGift = (ImageView) view.findViewById(R.id.imageViewGift);
			imageViewNearYou = (ImageView) view
					.findViewById(R.id.imageViewNearYou);

			imageViewRecharge.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					MainActivity.changeView(2, getActivity());
				}
			});

			imageViewGift.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					MainActivity.changeView(3, getActivity());
				}
			});

			SVG svgImageGift = SVGParser.getSVGFromResource(getActivity()
					.getResources(), R.raw.ic_homescreen_gift);
			Drawable svgImageViewGift = svgImageGift.createPictureDrawable();
			imageViewGift.setImageDrawable(svgImageViewGift);
			SVG svgImageNearYou = SVGParser.getSVGFromResource(getActivity()
					.getResources(), R.raw.ic_home_locate);
			/*
			 * Drawable svgimageViewNearYou = svgImageNearYou
			 * .createPictureDrawable();
			 * imageViewNearYou.setImageDrawable(svgimageViewNearYou);
			 */
			SVG svgRecharge1 = SVGParser.getSVGFromResource(getActivity()
					.getResources(), R.raw.ic_homescreen_recharge);
			Drawable rechargeDrawable = svgRecharge1.createPictureDrawable();

			imageViewRecharge.setImageDrawable(rechargeDrawable);

			SVG svgInfo = SVGParser.getSVGFromResource(getActivity()
					.getResources(), R.raw.ic_home_info);
			Drawable infoDrawable = svgInfo.createPictureDrawable();

			imageViewInfo.setImageDrawable(infoDrawable);

			// showFreeBies();
			showFreeGiftList();

			if (Utility.getRefreshRequired(getActivity(),
					Constants.SHAREDPREFERENCE_REFESH_QUICKPAY))
				new GetQuickPayList().execute();

		} catch (Exception e) {
			Log.d("qqq", "qwe");
		}

		return view;

	}

	private void showFreeGiftList() {
		// TODO Auto-generated method stub
		try {

			freeBiesDataSource.open();
			String offerJson = Utility.getSharedPreferences(getActivity(),
					"Deals_offer");

			JSONObject jsonObjectDescription = new JSONObject(offerJson);

			final JSONArray jsonArrayOffer = jsonObjectDescription
					.getJSONArray("offer_details");

			for (int i = 0; i < jsonArrayOffer.length(); i++) {
				final String catName = jsonArrayOffer.getJSONObject(i)
						.getString("name");
				final String catID = jsonArrayOffer.getJSONObject(i).getString(
						"id");

				final String layoutType = jsonArrayOffer.getJSONObject(i)
						.getString("featured");

				if (layoutType.equalsIgnoreCase("1")) {

				} else {

					RelativeLayout relativeLayout = new RelativeLayout(
							getActivity());
					relativeLayout.setPadding(15, 5, 15, 0);
					{
						RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
								LayoutParams.WRAP_CONTENT,
								LayoutParams.WRAP_CONTENT);
						// Align bottom-right, and add
						// bottom-margin
						params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
						params.setMargins(0, 15, 0, 0);
						TextView textView_Catagory = new TextView(getActivity());
						textView_Catagory.setText(catName);
						textView_Catagory.setTypeface(Regular);
						// textView_Catagory.setAllCaps(true);

						textView_Catagory.setLayoutParams(params);
						relativeLayout.addView(textView_Catagory);
					}
					{
						RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
								LayoutParams.WRAP_CONTENT,
								LayoutParams.WRAP_CONTENT);
						// Align bottom-right, and add
						// bottom-margin
						params.setMargins(0, 15, 0, 0);
						params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
						TextView textView = new TextView(getActivity());
						textView.setText("SEE ALL ");
						// textView.setAllCaps(true);
						textView.setCompoundDrawablesWithIntrinsicBounds(0, 0,
								R.drawable.ic_next, 0);
						textView.setTextSize(12);
						Constants.setAlpha(textView, 0.6f);
						textView.setTypeface(Regular);
						textView.setLayoutParams(params);

						relativeLayout.addView(textView);
					}
					relativeLayout
							.setOnClickListener(new View.OnClickListener() {

								@Override
								public void onClick(View v) {
									// TODO Auto-generated
									// method stub
									Intent intent = new Intent(getActivity(),
											GiftGridActivity.class);
									intent.putExtra("NAME", catName);
									intent.putExtra("TYPE", catID);
									startActivity(intent);
								}
							});
					linearLayout_Main.addView(relativeLayout);

					String str = jsonArrayOffer.getJSONObject(i).getString(
							"details");
					final List<FreeBies> list = freeBiesDataSource
							.getAllFreeBies(str);
					/*
					 * GiftHomePageAdapter adapter=new
					 * GiftHomePageAdapter(getActivity(), list);
					 * listViewGift.setAdapter(adapter);
					 */

					for (int j = 0; j < list.size(); j++) {
						try {

							View row = null;
							if (row == null) {
								LayoutInflater inflater = (getActivity())
										.getLayoutInflater();
								row = inflater.inflate(
										R.layout.gift_cell_home_adapter, null);
								row.setTag((int) j);

								ImageView imageView_Photo = (ImageView) row
										.findViewById(R.id.imageView_Photo);
								TextView textView_OfferDesc = (TextView) row
										.findViewById(R.id.textView_OfferDesc);
								textView_OfferDesc.setText(list.get(j)
										.getFreebieShortDesc());
								try {
									Picasso.with(getActivity())
											.load(list.get(j).getFreebieURL())
											.placeholder(
													R.drawable.deal_default)
											.transform(
													new RoundedTransformation(
															0, 0))
											.into(imageView_Photo);
								} catch (Exception e) {
									System.out.println(e.getMessage());
								}
								row.setOnClickListener(new OnClickListener() {

									@Override
									public void onClick(View v) {
										// TODO Auto-generated method stub
										try {
											int tag = (Integer) v.getTag();
											FreeBies freeBie = list.get(tag);
											Intent intent = new Intent(
													getActivity(),
													MyFreeBiesDetailsActivity.class);
											intent.putExtra("GIFT", freeBie);
											intent.putExtra("IS_BUY", 0);
											getActivity().startActivity(intent);
										} catch (Exception e) {
										}
									}
								});
								Constants.setFont(getActivity(),
										(ViewGroup) row);
								linearLayout_Main.addView(row);
							}
						} catch (Exception e) {
						}
					}
				}
			}
		} catch (JSONException je) {
			Log.d("fgsdf", "dh");
		} catch (Exception e) {
			Log.d("fgsdf", "dh");
		} finally {
			freeBiesDataSource.close();
		}

	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		// MainActivity.changeView(20, getActivity());
	}

	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView().build());
		// EasyTracker.getInstance(this).activityStart(this);
		easyTracker.send(MapBuilder.createAppView().build());
	}

	@Override
	public void onStop() {
		super.onStop();
		// EasyTracker.getInstance(this).activityStop(this);
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		// outState.putString(KEY_CONTENT, mContent);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.Fragment#onDestroy()
	 */
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		// Intent intent = new Intent(getActivity(), MainActivity.class);
		// intent.putExtra("FROM_QUICKPAY", false);
		// intent.putExtra("IS_GIFT", false);
		// intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
		// | Intent.FLAG_ACTIVITY_NEW_TASK);
		// getActivity().startActivity(intent);
		// getActivity().finish();
	}

	private void showFreeBies() {
		String offerJson = Utility.getSharedPreferences(getActivity(),
				"Deals_offer");

		try {
			JSONObject jsonObjectDescription = new JSONObject(offerJson);

			final JSONArray jsonArrayOffer = jsonObjectDescription
					.getJSONArray("offer_details");

			for (int i = 0; i < jsonArrayOffer.length(); i++) {
				final String catName = jsonArrayOffer.getJSONObject(i)
						.getString("name");
				final String catID = jsonArrayOffer.getJSONObject(i).getString(
						"id");

				final String layoutType = jsonArrayOffer.getJSONObject(i)
						.getString("featured");

				if (layoutType.equalsIgnoreCase("1")) {
					RelativeLayout relativeLayout = new RelativeLayout(
							getActivity());
					relativeLayout.setPadding(15, 15, 15, 5);
					{
						RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
								LayoutParams.WRAP_CONTENT,
								LayoutParams.WRAP_CONTENT);
						// Align bottom-right, and add
						// bottom-margin
						params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);

						TextView textView_Catagory = new TextView(getActivity());
						textView_Catagory.setText(catName);
						// textView_Catagory.setAllCaps(true);
						textView_Catagory.setLayoutParams(params);
						relativeLayout.addView(textView_Catagory);
					}
					{
						RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
								LayoutParams.WRAP_CONTENT,
								LayoutParams.WRAP_CONTENT);
						// Align bottom-right, and add
						// bottom-margin
						params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
						TextView textView = new TextView(getActivity());
						textView.setText("SEE ALL ");
						// textView.setAllCaps(true);
						// textView.setAlpha(0.6f);
						Constants.setAlpha(textView, 0.6f);
						textView.setCompoundDrawablesWithIntrinsicBounds(0, 0,
								R.drawable.ic_next, 0);
						textView.setTextSize(12);
						textView.setTypeface(Medium);
						textView.setLayoutParams(params);

						relativeLayout.addView(textView);
					}
					relativeLayout
							.setOnClickListener(new View.OnClickListener() {

								@Override
								public void onClick(View v) {
									// TODO Auto-generated
									// method stub
									Intent intent = new Intent(getActivity(),
											GiftGridActivity.class);
									intent.putExtra("NAME", catName);
									intent.putExtra("TYPE", catID);
									startActivity(intent);
								}
							});
					linearLayout_Main.addView(relativeLayout);

					HorizontalScrollView hs = new HorizontalScrollView(
							getActivity());
					GiftImageSpecialOffersLayout giftCell = new GiftImageSpecialOffersLayout(
							getActivity());
					hs.addView(giftCell);
					linearLayout_Main.addView(hs);

					View v = new View(getActivity());
					LayoutParams layoutParams = new LayoutParams(
							LayoutParams.FILL_PARENT, 1);
					v.setLayoutParams(layoutParams);
					v.setBackgroundColor(getResources().getColor(
							R.color.LightGrey));
					v.setPadding(15, 0, 15, 0);
					linearLayout_Main.addView(v);

					// myHorizontalLayout = (MyHorizontalLayout)
					// view
					// .findViewById(R.id.mygallery);

					try {
						freeBiesDataSource.open();

						String str = jsonArrayOffer.getJSONObject(i).getString(
								"details");
						List<FreeBies> list = freeBiesDataSource
								.getAllFreeBies(str);
						int len = list.size();
						if (len != 0) {
							for (FreeBies freebie : list) {
								giftCell.add(freebie);
							}
							Log.d("Aftre for", "Aftre for");

						} else {
							// new FreeBiesTask()
							// .execute(
							// Utility.getCurrentLatitude(
							// getActivity(),
							// Constants.SHAREDPREFERENCE_CURRENT_LATITUDE),
							// Utility.getCurrentLongitude(
							// getActivity(),
							// Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE));
							// pager.setVisibility(View.GONE);

						}
					} catch (SQLException exception) {
						// TODO: handle exception
						// // // Log.e("Er ",
						// exception.getMessage());
					} catch (Exception e) {
						// TODO: handle exception
						// Log.e("Er ", e.getMessage());
					} finally {
						freeBiesDataSource.close();
					}
				} else {
					RelativeLayout relativeLayout = new RelativeLayout(
							getActivity());
					relativeLayout.setPadding(15, 5, 15, 0);
					{
						RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
								LayoutParams.WRAP_CONTENT,
								LayoutParams.WRAP_CONTENT);
						// Align bottom-right, and add
						// bottom-margin
						params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
						params.setMargins(0, 15, 0, 0);
						TextView textView_Catagory = new TextView(getActivity());
						textView_Catagory.setText(catName);
						textView_Catagory.setTypeface(Regular);
						// textView_Catagory.setAllCaps(true);

						textView_Catagory.setLayoutParams(params);
						relativeLayout.addView(textView_Catagory);
					}
					{
						RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
								LayoutParams.WRAP_CONTENT,
								LayoutParams.WRAP_CONTENT);
						// Align bottom-right, and add
						// bottom-margin
						params.setMargins(0, 15, 0, 0);
						params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
						TextView textView = new TextView(getActivity());
						textView.setText("SEE ALL ");
						// textView.setAllCaps(true);
						textView.setCompoundDrawablesWithIntrinsicBounds(0, 0,
								R.drawable.ic_next, 0);
						textView.setTextSize(12);
						// textView.setAlpha(0.6f);
						Constants.setAlpha(textView, 0.6f);
						textView.setTypeface(Regular);
						textView.setLayoutParams(params);

						relativeLayout.addView(textView);
					}
					relativeLayout
							.setOnClickListener(new View.OnClickListener() {

								@Override
								public void onClick(View v) {
									// TODO Auto-generated
									// method stub
									Intent intent = new Intent(getActivity(),
											GiftGridActivity.class);
									intent.putExtra("NAME", catName);
									intent.putExtra("TYPE", catID);
									startActivity(intent);
								}
							});
					linearLayout_Main.addView(relativeLayout);

					ScrollView hs = new ScrollView(getActivity());/*
																 * hs = new
																 * HorizontalScrollView
																 * (
																 * getActivity(
																 * ));
																 */
					GiftCellHomeLayout giftCell = new GiftCellHomeLayout(
							getActivity());
					hs.addView(giftCell);
					linearLayout_Main.addView(hs);

					View v = new View(getActivity());
					LayoutParams layoutParams = new LayoutParams(
							LayoutParams.FILL_PARENT, 1);
					v.setLayoutParams(layoutParams);
					v.setBackgroundColor(getResources().getColor(
							R.color.LightGrey));
					v.setPadding(15, 0, 15, 0);
					linearLayout_Main.addView(v);

					// myHorizontalLayout = (MyHorizontalLayout)
					// view
					// .findViewById(R.id.mygallery);

					try {
						freeBiesDataSource.open();

						String str = jsonArrayOffer.getJSONObject(i).getString(
								"details");
						List<FreeBies> list = freeBiesDataSource
								.getAllFreeBies(str);
						int len = list.size();
						if (len != 0) {
							for (FreeBies freebie : list) {
								giftCell.add(freebie);
							}
							Log.d("Aftre for", "Aftre for");

						} else {

						}
					} catch (SQLException exception) {
						// TODO: handle exception
						// // // Log.e("Er ",
						// exception.getMessage());
					} catch (Exception e) {
						// TODO: handle exception
						// Log.e("Er ", e.getMessage());
					} finally {
						freeBiesDataSource.close();
					}
				}

			}
		} catch (SQLException se) {

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public class GetQuickPayList extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String response = RequestClass.getInstance().readPay1B2CRequest(
					getActivity(), Constants.B2C_URL + "get_quickpaylist/?");
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			if (!result.startsWith("Error")) {

				String status;
				try {
					JSONObject jsonObject = new JSONObject(result);
					status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {

						try {
							quickPayDataSource.open();

							JSONArray description = jsonObject
									.getJSONArray("description");

							if (description.length() != 0) {
								quickPayDataSource.deleteQuickPay();
								// textView_Title.setVisibility(View.VISIBLE);
								long timestamp = System.currentTimeMillis();
								quickPayDataSource.createQuickPay(description,
										timestamp);

								List<QuickPay> quickpays = quickPayDataSource
										.getQuickPayByLimit(7);
								int size = quickpays.size();
								if (size != 0) {/*
												 * data.clear();
												 * textView_Title.setVisibility
												 * (View.VISIBLE); if (size > 6)
												 * textView_LoadMore
												 * .setVisibility(View.VISIBLE);
												 * else textView_LoadMore
												 * .setVisibility(View.GONE);
												 * int i = 0; for (QuickPay
												 * quickpay : quickpays) { if (i
												 * == 6) break; i++;
												 * WeakHashMap<String, String>
												 * map = new WeakHashMap<String,
												 * String>(); map.put(OPERATOR,
												 * "" + quickpay
												 * .getQuickPayProductID());
												 * map.put(MOBILE,
												 * quickpay.getQuickPayMobile
												 * ()); map.put(FLAG, "" +
												 * quickpay.getQuickPayFlag());
												 * if
												 * (quickpay.getQuickPayFlag()
												 * == Constants.BILL_PAYMENT) {
												 * 
												 * SimpleDateFormat dateFormat =
												 * new SimpleDateFormat(
												 * "dd-MMM"); String dt = "";
												 * try { String str = quickpay
												 * .getQuickPayTransactionDate()
												 * .replaceAll("-", "/"); Date
												 * date = new Date(str);
												 * 
												 * dt = dateFormat.format(date);
												 * } catch (Exception e) { }
												 * map.put(AMOUNT, dt); } else
												 * map.put(AMOUNT, "" + quickpay
												 * .getQuickPayAmount());
												 * map.put(ID, "" +
												 * quickpay.getQuickPayID());
												 * data.add(map); }
												 */
								}
							}
						} catch (SQLException exception) {
							// TODO: handle exception
							// // // Log.e("Er ", exception.getMessage());
						} catch (Exception e) {
							// TODO: handle exception
							// // // Log.e("Er ", e.getMessage());
						} finally {
							quickPayDataSource.close();
						}
						// adapter.notifyDataSetChanged();
						try {
							Utility.setRefreshRequired(getActivity(),
									Constants.SHAREDPREFERENCE_REFESH_QUICKPAY,
									false);
						} catch (Exception e) {
						}
					} else {
					}
				} catch (JSONException e) {
				}
			}
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences arg0, String arg1) {
		// TODO Auto-generated method stub
		Log.d("aaa", "ads");
	}

}
