function initialize() {
        var mapCanvas = document.getElementById('map-canvas1');
       var styles = {
			stylers: [
			  { gamma:0.5 },
			  { lightness: 40 }
			]
		  };
		  var styledMap = new google.maps.StyledMapType(styles,
			{name: "Styled Map"});
		var mapOptions = {
          center: new google.maps.LatLng(19.1839311,72.8318963),
          zoom: 16,
          mapTypeControlOptions: {
			mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
			}
        };
        var map = new google.maps.Map(mapCanvas, mapOptions)
		 map.mapTypes.set('map_style', styledMap);
		map.setMapTypeId('map_style');
      }
      google.maps.event.addDomListener(window, 'load', initialize);

  

