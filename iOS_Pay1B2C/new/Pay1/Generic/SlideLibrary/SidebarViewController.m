//
//  SidebarViewController.m
//  SidebarDemo
//
//  Created by Simon on 29/6/13.
//  Copyright (c) 2013 Appcoda. All rights reserved.
//

#import "SidebarViewController.h"
#import "SWRevealViewController.h"
#import "GiftsViewController.h"
#import "Constant.h"
#import "ViewController.h"
#import "SignOutService.h"
#import "SignOutResponse.h"
#import "Session.h"
#import "SignInResponse.h"
#import "ProfileViewController.h"
#import "UIImage+Utility.h"
#import "CoinViewController.h"
#import "WalletDetailViewController.h"

#import "ViewController.h"
#import "MissedCallViewController.h"
#import "HomeViewController.h"
#import "AccountsAndSupportViewController.h"
@interface SidebarViewController ()

@end

@implementation SidebarViewController {
    NSArray *menuItems;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
        
   // [self.view addSubview:appDelegate.tabBarController.view];
    
    isFBCalled = FALSE;
    isfoundFBData = FALSE;
    
    HUD = [[MBProgressHUD alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    HUD.labelText = @"Loading...";
    HUD.dimBackground = YES;
    [self.view addSubview:HUD];

    menuItems = @[@"userInfoTC",@"myMoneyTC",@"homeTC", @"rechargeTC", @"giftTC", @"historyTC", @"missedTC", @"accountTC", @"logoutTC"];
    self.tableView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    NSLog(@"#ImageURL :%@",[Session shared].objSignInResponse.profileImageURL);
    
    loginview = [[FBLoginView alloc]init];
    loginview.frame = CGRectMake(100, 10, 200, 200);
    
    
    
    loginview.delegate = self;
    loginview.readPermissions = @[@"public_profile", @"email",@"user_friends"];
    [self.view addSubview:loginview];
    
    for (id loginObject in loginview.subviews)
    {
    if ([loginObject isKindOfClass:[UIButton class]])
    {
    UIButton *btn = loginObject;
            
    btn.frame = CGRectMake(0, 0, 100, 100);
    UILabel *nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(90, btn.frame.origin.y + btn.frame.size.height + 10, self.view.frame.size.width - 50, 21)];
    nameLabel.textColor = [UIColor whiteColor];
    nameLabel.font = [UIFont fontWithName:@"Open Sans" size:17];
    nameLabel.text = [Session shared].objSignInResponse.userName;
    [self.view addSubview:nameLabel];
            
  //  if([Session shared].objSignInResponse.profileImageURL.length == 0 && [Constant getUserImage:[Session shared].objSignInResponse.userID] == nil){
    if([Session shared].objSignInResponse.profileImageURL.length == 0){
    [btn setBackgroundImage:[UIImage imageNamed:@"facebook_connect.png"] forState:UIControlStateNormal];
    [nameLabel removeFromSuperview];
    }
    else if([Session shared].objSignInResponse.profileImageURL.length > 0){
    btn.layer.cornerRadius = btn.frame.size.height/2;
    btn.clipsToBounds = YES;
    btn.layer.masksToBounds = YES;
    btn.layer.borderWidth = 2;
    btn.layer.borderColor = [UIColor whiteColor].CGColor;
                
    [btn.imageView setImageWithURL:[NSURL URLWithString:[Session shared].objSignInResponse.profileImageURL] placeholderImage:[UIImage imageNamed:@"white.png"]completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
                    UIImage *newImage = [UIImage imageWithImage:image  scaledToSize:CGSizeMake(320,212)];
                    [btn setBackgroundImage:newImage forState:UIControlStateNormal];
    }usingActivityIndicatorStyle:(UIActivityIndicatorViewStyle)UIActivityIndicatorViewStyleGray];
                
                
    }
    else{
    UIImage * loginImage = [Constant getUserImage:[Session shared].objSignInResponse.userID];
    [btn setBackgroundImage:loginImage forState:UIControlStateNormal];
    }
    }
    if ([loginObject isKindOfClass:[UILabel class]])
    {
    [loginObject removeFromSuperview];
    }
        
    }
    
    
    
    
  //  [self loginViewShowingLoggedOutUser:loginview];

    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [menuItems count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = [menuItems objectAtIndex:indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.separatorInset = UIEdgeInsetsZero;
    
   // loginview = (FBLoginView *)[cell viewWithTag:200];
    
    
    
    UIView *bgView = (UIView *)[cell viewWithTag:201];
    bgView.backgroundColor = [UIColor colorWithRed:237/255.0 green:237/255.0 blue:237/255.0 alpha:0.3];
    
    
    
    
    walletView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width - 185, 50)];
    walletView.backgroundColor = [UIColor blackColor];
    
    
    UIButton *walletButton = [UIButton buttonWithType:UIButtonTypeCustom];
    walletButton.frame = CGRectMake(15, 15, 30, 20);
    [walletButton setBackgroundImage:[UIImage imageNamed:@"money_sliding_menu.png"] forState:UIControlStateNormal];
    
    
    UILabel *walletLabel = [[UILabel alloc]initWithFrame:CGRectMake(walletButton.frame.origin.x + walletButton.frame.size.width + 2, 15, 84, 21)];
    walletLabel.text = [NSString stringWithFormat:@"Rs %@",[Session shared].objSignInResponse.userWalletBalance];
    walletLabel.textColor = [UIColor whiteColor];
    
    
    [walletView addSubview:walletLabel];
    [walletView addSubview:walletButton];
    
    UITapGestureRecognizer *walletTapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(walletClicked:)];
    walletTapGesture.delegate = self;
    [walletView addGestureRecognizer:walletTapGesture];
    
    [bgView addSubview:walletView];
    
    UIView *sepView = [[UIView alloc]initWithFrame:CGRectMake((self.view.frame.size.width - 185) + 1, 0, 10, 50)];
    sepView.backgroundColor = [UIColor redColor];
    [bgView addSubview:sepView];

    
    coinView = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width - 185, 0, self.view.frame.size.width - 175, 50)];
    coinView.backgroundColor = [UIColor blackColor];
    
    UITapGestureRecognizer *coinTapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(coinClicked:)];
    coinTapGesture.delegate = self;
    [coinView addGestureRecognizer:coinTapGesture];
    

    UIButton *coinButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [coinButton setBackgroundImage:[UIImage imageNamed:@"coin_sling_menu.png"] forState:UIControlStateNormal];
    coinButton.frame = CGRectMake(15, 15, 25, 25);

    
    UILabel *coinLabel = [[UILabel alloc]initWithFrame:CGRectMake(coinButton.frame.origin.x + coinButton.frame.size.width + 2, 15, 84, 21)];
    coinLabel.text = [Session shared].objSignInResponse.userLoyaltyPoints; //@"100";
    coinLabel.textColor = [UIColor whiteColor];
    
    [coinView addSubview:coinLabel];
    [coinView addSubview:coinButton];
    
    [bgView addSubview:coinView];
    
    
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
   
      return cell;
}


- (void) prepareForSegue: (UIStoryboardSegue *) segue sender: (id) sender
{
    // Set the title of navigation bar by using the menu items
   /* NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    UINavigationController *destViewController = (UINavigationController*)segue.destinationViewController;
    destViewController.title = [[menuItems objectAtIndex:indexPath.row] capitalizedString];*/
    
    // Set the photo if it navigates to the PhotoView
    if ([segue.identifier isEqualToString:@"showGifts"]) {
       // GiftsViewController *giftsVC = (GiftsViewController*)segue.destinationViewController;
    }
    
    if ([segue.identifier isEqualToString:@"AccountAndSupportView"]) {
        
        AccountsAndSupportViewController *destViewController = segue.destinationViewController;
        
        // Hide bottom tab bar in the detail view
        destViewController.hidesBottomBarWhenPushed = YES;
    }
    
    
    
    if ( [segue isKindOfClass: [SWRevealViewControllerSegue class]] ) {
    SWRevealViewControllerSegue *swSegue = (SWRevealViewControllerSegue*) segue;
        
    swSegue.performBlock = ^(SWRevealViewControllerSegue* rvc_segue, UIViewController* svc, UIViewController* dvc) {
            
    UINavigationController* navController = (UINavigationController*)self.revealViewController.frontViewController;
    [navController setViewControllers: @[dvc] animated: NO ];
    [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
    };
        
    }
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
  //  NSLog(@"#RowIndex :%ld",(long)indexPath.row);
    
    if (indexPath.row == 8) {
    NSLog(@"Logout is clicked");
        
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Pay1"message: @"Are you sure you want to logout?"delegate: self cancelButtonTitle:@"Ok" otherButtonTitles:@"Cancel",nil];
        
    [alert show];
    }
   /*  if (indexPath.row == 7) {
    NSLog(@"Missed Call clicked");
        
    MissedCallViewController *homeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"missed"];
    UINavigationController* navController = (UINavigationController*)self.revealViewController.frontViewController;
    [navController setViewControllers:@[homeVC] animated: YES ];
    [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
    [self.revealViewController setHidesBottomBarWhenPushed:TRUE];
    [self.revealViewController.navigationController pushViewController:navController animated:YES];
    }*/

}


-(void)callSignOutService{
    [self showHUDOnView];
    SignOutService *signOutService = [[SignOutService alloc]init];
    [signOutService callSignOutService:^(NSError *error, SignOutResponse *signOutResponse) {
    if(error)return ;
    else{
    if([signOutResponse.status isEqualToString:@"success"]){
    NSLog(@"we get success from server");
    NSLog(@"The sign out response is%@",signOutResponse.status);
    [Constant saveUserPhoneNumber:@""];
    [Constant saveUserPassword:@""];
  //  [Constant saveUserDataMD5:@""];
        
        [self hideHUDOnView];
        
   // [self clearUserQuickPayDataFromSQLite];
    if([Session shared].isFromAutoLogin == TRUE){
    [Session shared].isFromAutoLogin = FALSE;
    [Session shared].isFromSlide = TRUE;
        
    ViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"viewC"];
    UINavigationController* navController = (UINavigationController*)self.revealViewController.frontViewController;
    [navController setViewControllers:@[vc] animated: YES ];
    [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];

    [self.revealViewController.navigationController pushViewController:navController animated:YES];
    }
    else
    [self.revealViewController.navigationController popToRootViewControllerAnimated:YES];

    }
    }
    }];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
    NSLog(@"user pressed Button Indexed 0");
    [self callSignOutService];
    }
    else
    {
    NSLog(@"user pressed Button Indexed 1");
    }
    
}

-(void)loginViewShowingLoggedInUser:(FBLoginView *)loginView{
    NSLog(@"You are logged in ");
}

-(void)loginViewFetchedUserInfo:(FBLoginView *)loginView user:(id<FBGraphUser>)user{
    NSLog(@"Info %@", user);
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=normal",[user objectForKey:@"id"]]];
    NSLog(@"User URL is :%@",url);
    
    if(isFBCalled == FALSE){
    isFBCalled = TRUE;
        

    [Session shared].objSignInResponse.userName = [NSString stringWithFormat:@"%@",[user objectForKey:@"name"]];
    [Session shared].objSignInResponse.userEmail = [NSString stringWithFormat:@"%@",[user objectForKey:@"email"]];
    [Session shared].objSignInResponse.userGender = [NSString stringWithFormat:@"%@",[user objectForKey:@"gender"]];
    [Session shared].objSignInResponse.profileImageURL = [url absoluteString];
     
        
    
   // [self navigateToProfileView];

    }
}

-(void)loginViewShowingLoggedOutUser:(FBLoginView *)loginView{
    NSLog(@"You are logged out ");

}


-(void)loginView:(FBLoginView *)loginView handleError:(NSError *)error{
    NSLog(@"%@", [error localizedDescription]);
}



-(void)navigateToProfileView{
    ProfileViewController *profileVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
    profileVC.isFromFB = TRUE;
    UINavigationController* navController = (UINavigationController*)self.revealViewController.frontViewController;
    [navController setViewControllers:@[profileVC] animated: YES ];
    [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
    
    [self.revealViewController.navigationController pushViewController:navController animated:YES];
}

-(void)walletClicked:(UITapGestureRecognizer*)sender{
    NSLog(@"#WalletClicked");
    [Session shared].isFromSlide = TRUE;
    WalletDetailViewController *walletDetailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"walletDetailViewC"];
    UINavigationController* navController = (UINavigationController*)self.revealViewController.frontViewController;
    [navController setViewControllers:@[walletDetailVC] animated: YES ];
    [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];

    [self.revealViewController.navigationController pushViewController:navController animated:YES];
    

    
  /*  NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    UINavigationController *destViewController = (UINavigationController*)segue.destinationViewController;
    
    
    
    if ( [segue isKindOfClass: [SWRevealViewControllerSegue class]] ) {
        SWRevealViewControllerSegue *swSegue = (SWRevealViewControllerSegue*) segue;
        
        swSegue.performBlock = ^(SWRevealViewControllerSegue* rvc_segue, UIViewController* svc, UIViewController* dvc) {
            
            UINavigationController* navController = (UINavigationController*)self.revealViewController.frontViewController;
            [navController setViewControllers: @[dvc] animated: NO ];
            [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
        };
        
    }*/
}

-(void)coinClicked:(UITapGestureRecognizer *)sender{
    NSLog(@"#CoinClicked");
    [Session shared].isFromSlide = TRUE;

    CoinViewController *coinVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CoinView"];
    UINavigationController* navController = (UINavigationController*)self.revealViewController.frontViewController;
    [navController setViewControllers:@[coinVC] animated: YES ];
    [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
    
    [self.revealViewController.navigationController pushViewController:navController animated:YES];

}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == 0){
        return 210;
    }
    else{
        return 40;
    }
    
}
-(void)showHUDOnView{
    [HUD show:YES];
}
-(void)hideHUDOnView{
    [HUD hide:YES];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    loginview.delegate=nil;
}



@end
