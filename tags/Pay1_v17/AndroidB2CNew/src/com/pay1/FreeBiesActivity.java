package com.pay1;

import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.WeakHashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.SQLException;
import android.graphics.Typeface;
import android.location.LocationManager;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.pay1.adapterhandler.FreeBiesPagerAdapter;
import com.pay1.constants.Constants;
import com.pay1.customviews.MyProgressDialog;
import com.pay1.databasehandler.FreeBies;
import com.pay1.databasehandler.FreeBiesDataSource;
import com.pay1.databasehandler.FreeBiesLocation;
import com.pay1.databasehandler.FreeBiesLocationDataSource;
import com.pay1.databasehandler.FreeBiesOrder;
import com.pay1.databasehandler.FreeBiesOrderDataSource;
import com.pay1.databasehandler.ReClaimGift;
import com.pay1.databasehandler.ReClaimGiftDataSource;
import com.pay1.requesthandler.RequestClass;
import com.pay1.utilities.Utility;

public class FreeBiesActivity extends FragmentActivity {

	private static final String SCREEN_LABEL = "Freebies On Recharge Screen";
	// private final String TAG = "Freebies";
	private ViewPager pager;
	private FreeBiesPagerAdapter adapter;
	// private ImageView imageView_Back;
	private TextView textView_CALL, textView_MSG;
	private LinearLayout footer, relativeLayout_EmptyView;
	private TextView textView_Back, textView_Title, textView_NoData,
			textView_Shop;

	private ArrayList<WeakHashMap<String, String>> data;
	private ArrayList<WeakHashMap<String, String>> data_location;
	private Set<Integer> deal_id_hash;

	public static final String DEAL_ID = "id";
	public static final String DEAL_NAME = "dealname";
	public static final String URL = "img_url";
	public static final String CATAGORY = "category";
	public static final String CATAGORY_ID = "category_id";
	public static final String LATITUDE = "latitude";
	public static final String LONGITUDE = "longitude";
	public static final String ADDRESS = "address";
	public static final String AREA = "area";
	public static final String CITY = "city";
	public static final String STATE = "state";
	public static final String PIN = "pin";
	public static final String DISTANCE = "distance";
	public static final String LOCATION_COUNT = "location_count";
	public static final String OFFER_ID = "o_id";
	public static final String OFFER_NAME = "name";
	public static final String VALIDITY = "validity";
	public static final String SHORT_DESC = "offer_desc";
	public static final String MIN_AMOUNT = "min_amount";

	private EasyTracker easyTracker = null;

	int recharge_amount = 0;
	String trans_id = "";
	private FreeBiesDataSource freeBiesDataSource;
	private FreeBiesLocationDataSource freeBiesLocationDataSource;
	private ReClaimGiftDataSource reClaimGiftDataSource;
	private FreeBiesOrderDataSource freeBiesOrderDataSource;
	boolean check_flag = false;
	int page = 1;
	int selected_item = 0;
	int direction = 0;

	@SuppressLint("NewApi")
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.freebies_activity);
		try {

			freeBiesDataSource = new FreeBiesDataSource(FreeBiesActivity.this);
			freeBiesLocationDataSource = new FreeBiesLocationDataSource(
					FreeBiesActivity.this);
			reClaimGiftDataSource = new ReClaimGiftDataSource(
					FreeBiesActivity.this);
			freeBiesOrderDataSource = new FreeBiesOrderDataSource(
					FreeBiesActivity.this);

			Utility.setBackRequired(FreeBiesActivity.this,
					Constants.SHAREDPREFERENCE_BACK_LOCATION, true);

			footer = (LinearLayout) findViewById(R.id.footer);
			// try {
			// if (getIntent().getBooleanExtra("FROM_RECHARGE", false))
			// footer.setVisibility(View.VISIBLE);
			// else
			// footer.setVisibility(View.GONE);
			// } catch (Exception e) {
			footer.setVisibility(View.GONE);
			// }

			Typeface Reguler = Typeface.createFromAsset(getAssets(),
					"EncodeSansNormal-400-Regular.ttf");

			relativeLayout_EmptyView = (LinearLayout) findViewById(R.id.relativeLayout_EmptyView);

			textView_Title = (TextView) findViewById(R.id.textView_Title);
			textView_Title.setTypeface(Reguler);
			textView_Back = (TextView) findViewById(R.id.textView_Back);
			textView_Back.setTypeface(Reguler);
			textView_Back.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onBackPressed();
				}
			});

			textView_NoData = (TextView) findViewById(R.id.textView_NoData);
			textView_NoData.setTypeface(Reguler);
			textView_Shop = (TextView) findViewById(R.id.textView_Shop);
			textView_Shop.setTypeface(Reguler);
			textView_Shop.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent intent = new Intent(FreeBiesActivity.this,
							FreeBiesActivity.class);
					intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
							| Intent.FLAG_ACTIVITY_NEW_TASK);
					intent.putExtra("FROM_QUICKPAY", true);
					intent.putExtra("IS_GIFT", false);
					startActivity(intent);
					finish();
				}
			});

			// imageView_Back = (ImageView) findViewById(R.id.imageView_Back);
			// imageView_Back.setImageDrawable(SVGParser.getSVGFromResource(
			// getResources(), R.raw.ic_back).createPictureDrawable());
			// if (Build.VERSION.SDK_INT >= 11)
			// imageView_Back.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
			// imageView_Back.setOnClickListener(new View.OnClickListener() {
			//
			// @Override
			// public void onClick(View v) {
			// // TODO Auto-generated method stub
			// onBackPressed();
			// }
			// });

			textView_CALL = (TextView) findViewById(R.id.textView_CALL);
			textView_CALL.setTypeface(Reguler);
			textView_CALL.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent callIntent = new Intent(Intent.ACTION_CALL);
					// callIntent.setData(Uri.parse("tel:+912267242266"));
					callIntent.setData(Uri.parse("tel:"
							+ Utility.getSupportNumber(FreeBiesActivity.this,
									Constants.SHAREDPREFERENCE_SUPPORT_NUMBER)));
					startActivity(callIntent);
				}
			});

			try {

				// textView_CALL.setText("If your transaction of Rs."
				// + getIntent().getStringExtra("AMOUNT")
				// + " on "
				// + getIntent().getStringExtra("MOBILE")
				// + " does not succeed, give a missed call on "
				// + Utility.getSupportNumber(FreeBiesActivity.this,
				// Constants.SHAREDPREFERENCE_SUPPORT_NUMBER)
				// + ".");
				recharge_amount = Integer.parseInt(getIntent().getStringExtra(
						"AMOUNT"));
				trans_id = getIntent().getStringExtra("TRANS_ID");
			} catch (Exception e) {
				textView_CALL.setVisibility(View.GONE);
				recharge_amount = 0;
				trans_id = "";
			}

			try {
				reClaimGiftDataSource.open();

				ReClaimGift reClaimGift = reClaimGiftDataSource
						.getReClaimGift(trans_id);
				// if (reClaimGift == null) {
				// reClaimGiftDataSource.createReClaimGift(trans_id,
				// recharge_amount, 1, 2, System.currentTimeMillis());
				// } else
				if (reClaimGift != null) {
					if (reClaimGift.getReClaimCounter() == 3) {
						reClaimGiftDataSource.updateReClaimGift(
								reClaimGift.getReClaimID(), 1, 2,
								System.currentTimeMillis());
					} else if (reClaimGift.getReClaimCounter() == 2) {
						check_flag = true;
						reClaimGiftDataSource.updateReClaimGift(
								reClaimGift.getReClaimID(), 1, 1,
								System.currentTimeMillis());
					} else if (reClaimGift.getReClaimCounter() == 1) {
						check_flag = false;
						reClaimGiftDataSource.updateReClaimGift(
								reClaimGift.getReClaimID(), 0, 0,
								System.currentTimeMillis());
						Constants
								.showOneButtonDialog(
										FreeBiesActivity.this,
										"Free Gifts",
										"You forgot to grab your free gift.\nExplore the unlimited excitement, grab a gift now!!!",
										Constants.DIALOG_CLOSE);
						reClaimGiftDataSource.deleteReClaimGift(reClaimGift
								.getReClaimID());
					} else {
						reClaimGiftDataSource.deleteReClaimGift(reClaimGift
								.getReClaimID());
					}
				}
			} catch (SQLException exception) {
				// TODO: handle exception
				// // // Log.e("Er ", exception.getMessage());
			} catch (Exception e) {
				// TODO: handle exception
				// // // Log.e("Er ", e.getMessage());
			} finally {
				reClaimGiftDataSource.close();
			}
			// try {
			// LayoutInflater inflater = getLayoutInflater();
			//
			// // Call toast.xml file for toast layout
			// View toastRoot = inflater.inflate(R.layout.custom_toast, null);
			//
			// Toast toast = new Toast(FreeBiesActivity.this);
			// TextView textView = (TextView) toastRoot
			// .findViewById(R.id.textView_Message);
			// textView.setTypeface(Reguler);
			// textView.setText("Last recharge complaint - Missed call to "
			// + Utility.getSupportNumber(FreeBiesActivity.this,
			// Constants.SHAREDPREFERENCE_SUPPORT_NUMBER)
			// + ".");
			// toast.setView(toastRoot);
			// toast.setGravity(Gravity.BOTTOM, 0, 0);
			// toast.setDuration(Toast.LENGTH_LONG);
			// toast.show();
			// } catch (Exception e) {
			// // System.out.println("ms " + e.getMessage());
			// }

			textView_MSG = (TextView) findViewById(R.id.textView_MSG);
			textView_MSG.setTypeface(Reguler);

			pager = (ViewPager) findViewById(R.id.pager);

			easyTracker = EasyTracker.getInstance(FreeBiesActivity.this);
			easyTracker.set(Fields.SCREEN_NAME, SCREEN_LABEL);

			data = new ArrayList<WeakHashMap<String, String>>();
			data_location = new ArrayList<WeakHashMap<String, String>>();
			deal_id_hash = new HashSet<Integer>();

			adapter = new FreeBiesPagerAdapter(
					this.getSupportFragmentManager(), 0, recharge_amount,
					trans_id, true, data, data_location);
			pager.setAdapter(adapter);
			pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

				@Override
				public void onPageSelected(int arg0) {
					// TODO Auto-generated method stub
					selected_item = arg0;
					int c = pager.getCurrentItem();
					if (c > direction && c == (data.size() - 2)) {
						new FreeBiesTask().execute(
								Utility.getCurrentLatitude(
										FreeBiesActivity.this,
										Constants.SHAREDPREFERENCE_CURRENT_LATITUDE),
								Utility.getCurrentLongitude(
										FreeBiesActivity.this,
										Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE),
								Utility.getMobileNumber(FreeBiesActivity.this,
										Constants.SHAREDPREFERENCE_MOBILE),
								Utility.getSharedPreferences(
										FreeBiesActivity.this,
										Constants.LAST_GIFT_UPDATED));
					}
					direction = c;
				}

				@Override
				public void onPageScrolled(int arg0, float arg1, int arg2) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onPageScrollStateChanged(int arg0) {
					// TODO Auto-generated method stub

				}
			});
			// try {
			//
			// String lat = Utility.getCurrentLatitude(FreeBiesActivity.this,
			// Constants.SHAREDPREFERENCE_CURRENT_LATITUDE);
			// String lng = Utility.getCurrentLongitude(FreeBiesActivity.this,
			// Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE);
			// if (lat != null && lng != null) {
			// new DealTask().execute(lat, lng);
			// } else {
			// new DealTask().execute(Utility.getLatitude(
			// FreeBiesActivity.this,
			// Constants.SHAREDPREFERENCE_LATITUDE), Utility
			// .getLongitude(FreeBiesActivity.this,
			// Constants.SHAREDPREFERENCE_LONGITUDE));
			//
			// }
			// } catch (Exception exception) {
			// new DealTask().execute(null, null);
			// }

			try {
				freeBiesDataSource.open();
				freeBiesLocationDataSource.open();
				freeBiesOrderDataSource.open();

				FreeBiesOrder order = freeBiesOrderDataSource
						.getFreeBiesOrder();

				List<FreeBies> freebies = freeBiesDataSource.getAllFreeBies(
						order != null ? order.getOrderData() : "", Utility
								.getOrder(FreeBiesActivity.this,
										Constants.SHAREDPREFERENCE_GIFT));
				int len = freebies.size();
				if (len != 0) {

					data.clear();
					data_location.clear();

					for (FreeBies freebie : freebies							) {

						// if (jsonArrayDesc.getJSONObject(i).has("allow"))
						// {
						// if (jsonArrayDesc.getJSONObject(i)
						// .getString("allow")
						// .equalsIgnoreCase("0"))
						// continue;
						// }
						// if (recharge_amount < Integer
						// .parseInt(jsonObjectOffer
						// .getString("min_amount"))) {
						// // textView_Title.setText("");
						// textView_NoData
						// .setText("You missed out on a free gift! Recharge of minimum Rs. "
						// + jsonObjectOffer
						// .getString("min_amount")
						// + " and win a free gift.");
						// continue;
						// }
						int id = freebie.getFreebieDealID();
						if (!deal_id_hash.contains(id)) {
							WeakHashMap<String, String> map = new WeakHashMap<String, String>();
							deal_id_hash.add(id);
							map.put(DEAL_ID, String.valueOf(id));
							map.put(DEAL_NAME, freebie.getFreebieDealName());
							map.put(CATAGORY, freebie.getFreebieCategory());
							map.put(CATAGORY_ID, String.valueOf(freebie
									.getFreebieCategoryID()));
							// map.put(LATITUDE, jsonArrayDesc.getJSONObject(i)
							// .getString("latitude"));
							// map.put(LONGITUDE, jsonArrayDesc.getJSONObject(i)
							// .getString("longitude"));

							map.put(URL, freebie.getFreebieURL());

							map.put(OFFER_ID, String.valueOf(freebie
									.getFreebieOfferID()));
							map.put(OFFER_NAME, freebie
									.getFreebieOfferName());
							map.put(SHORT_DESC, freebie
									.getFreebieShortDesc());
							map.put(MIN_AMOUNT, String.valueOf(freebie
									.getFreebieMinAmount()));
							map.put(VALIDITY, freebie.getFreebieValidity());
							map.put(LOCATION_COUNT, String.valueOf(freebie
									.getFreebieLocationCount()));

							List<FreeBiesLocation> locations = freeBiesLocationDataSource
									.getAllFreeBiesLocation(freebie
											.getFreebieOfferID());

							int j=0;
							for (FreeBiesLocation location : locations) {

								WeakHashMap<String, String> map1 = new WeakHashMap<String, String>();
								map1.put(DEAL_ID, String.valueOf(location.getLocationDealID()));
								double latitude = location
										.getLocationLat();
								double longitude = location
										.getLocationLng();
								map1.put(LATITUDE, String.valueOf(latitude));
								map1.put(LONGITUDE, String.valueOf(longitude));
								map1.put(ADDRESS, location
										.getLocationAddress());
								map1.put(CITY, location
										.getLocationCity());
								map1.put(STATE, location
										.getLocationState());
								String lat = Utility
										.getCurrentLatitude(
												FreeBiesActivity.this,
												Constants.SHAREDPREFERENCE_CURRENT_LATITUDE);
								String lng = Utility
										.getCurrentLongitude(
												FreeBiesActivity.this,
												Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE);
								String dis = "0";
								if (lat != "" && lng != "")
									dis = String.valueOf(Constants
											.distanceFrom(
													Double.parseDouble(lat),
													Double.parseDouble(lng),
													latitude, longitude));
								map1.put(DISTANCE, dis);

								data_location.add(map1);

								if (j == 0) {
									map.put(ADDRESS, location
											.getLocationAddress());
									map.put(DISTANCE, dis);
									++j;
								}
							}
							data.add(map);
							//
							// if (i == 2)
							// break;
						}
						if (data.size() == 0) {
							pager.setVisibility(View.GONE);
							relativeLayout_EmptyView
									.setVisibility(View.VISIBLE);
						} else {
							// Collections.sort(data,
							// new Comparator<WeakHashMap<String, String>>() {
							//
							// @Override
							// public int compare(
							// WeakHashMap<String, String> lhs,
							// WeakHashMap<String, String> rhs) {
							// // TODO Auto-generated
							// // method stub
							// int l, r;
							// try {
							// l = Integer.parseInt(lhs
							//
							// .get(FreeBiesListFragment.MIN_AMOUNT));
							// r = Integer.parseInt(rhs
							// .get(FreeBiesListFragment.MIN_AMOUNT));
							// } catch (Exception e) {
							// l = 0;
							// r = 0;
							// }
							// return Integer.valueOf(l).compareTo(
							// Integer.valueOf(r));
							// }
							// });
							int cc = freeBiesDataSource
									.getAllFreeBiesCount(
											Utility.getOrder(
													FreeBiesActivity.this,
													Constants.SHAREDPREFERENCE_GIFT_EXPIRED),
											Utility.getOrder(
													FreeBiesActivity.this,
													Constants.SHAREDPREFERENCE_GIFT));
							adapter = new FreeBiesPagerAdapter(
									FreeBiesActivity.this
											.getSupportFragmentManager(),
									cc, recharge_amount, trans_id, true, data,
									data_location);
							pager.setAdapter(adapter);
							pager.setVisibility(View.VISIBLE);
							relativeLayout_EmptyView.setVisibility(View.GONE);
						}
					}
				} else {
					// new FreeBiesTask()
					// .execute(
					// Utility.getCurrentLatitude(
					// FreeBiesActivity.this,
					// Constants.SHAREDPREFERENCE_CURRENT_LATITUDE),
					// Utility.getCurrentLongitude(
					// FreeBiesActivity.this,
					// Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE));
					pager.setVisibility(View.GONE);
					relativeLayout_EmptyView.setVisibility(View.VISIBLE);
				}
			} catch (SQLException exception) {
				// TODO: handle exception
				// // // Log.e("Er ", exception.getMessage());
			} catch (Exception e) {
				// TODO: handle exception
				// // // Log.e("Er ", e.getMessage());
			} finally {
				freeBiesDataSource.close();
				freeBiesLocationDataSource.close();
				freeBiesOrderDataSource.close();
			}

			if (!isLocationOn()) {
				showTwoButtonDialog(
						FreeBiesActivity.this,
						"Free gifts",
						"Turn on your location service on the phone to locate nearby free gifts.",
						Constants.DIALOG_CLOSE_LOCATION);
			}
		} catch (Exception e) {
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView().build());
		// EasyTracker.getInstance(this).activityStart(this);
		easyTracker.send(MapBuilder.createAppView().build());
	}

	@Override
	public void onStop() {
		super.onStop();
		// EasyTracker.getInstance(this).activityStop(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		if (Utility.getBackRequired(FreeBiesActivity.this,
				Constants.SHAREDPREFERENCE_BACK_LOCATION))
			if (isLocationOn()) {
				Utility.setBackRequired(FreeBiesActivity.this,
						Constants.SHAREDPREFERENCE_BACK_LOCATION, false);
				page = 0;
				new FreeBiesTask().execute(Utility.getCurrentLatitude(
						FreeBiesActivity.this,
						Constants.SHAREDPREFERENCE_CURRENT_LATITUDE), Utility
						.getCurrentLongitude(FreeBiesActivity.this,
								Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE),
						Utility.getMobileNumber(FreeBiesActivity.this,
								Constants.SHAREDPREFERENCE_MOBILE), Utility
								.getSharedPreferences(FreeBiesActivity.this,
										Constants.LAST_GIFT_UPDATED));
			}
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		try {
			data.clear();
			data_location.clear();
			deal_id_hash.clear();
			pager.setAdapter(null);
		} catch (Exception e) {
		}
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// super.onBackPressed();
		if (FreeBiesPagerFragment.flag == 1) {
			FreeBiesPagerFragment.flag = 0;
			adapter.notifyDataSetChanged();
		} else {
			if (check_flag)
				showTwoButtonDialog(FreeBiesActivity.this, "Free Gifts",
						"Grab a free gift?");
			else {
				Utility.setSelectedGift(FreeBiesActivity.this,
						Constants.SHAREDPREFERENCE_SELECTED_GIFT, "");
				Intent intent = new Intent(FreeBiesActivity.this,
						MainActivity.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
						| Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
				FreeBiesActivity.this.finish();
			}
		}
	}

	public void showTwoButtonDialog(final Context context, String Title,
			String message) {
		try {

			final Dialog dialog = new Dialog(context);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			// dialog.getWindow().setBackgroundDrawableResource(
			// android.R.color.transparent);
			dialog.setContentView(R.layout.dialog_two_button);
			dialog.setCancelable(false);
			dialog.setCanceledOnTouchOutside(false);
			// dialog.setTitle(null);

			Typeface Reguler = Typeface.createFromAsset(context.getAssets(),
					"EncodeSansNormal-400-Regular.ttf");

			Typeface Medium = Typeface.createFromAsset(context.getAssets(),
					"EncodeSansNarrow-500-Medium.ttf");

			// set the custom dialog components - text, image and button
			TextView textView_Title = (TextView) dialog
					.findViewById(R.id.textView_Title);
			textView_Title.setText(Title);
			textView_Title.setTypeface(Reguler);

			TextView textView_Message = (TextView) dialog
					.findViewById(R.id.textView_Message);
			textView_Message.setText(message);
			textView_Message.setTypeface(Medium);

			Button button_Ok = (Button) dialog.findViewById(R.id.button_Ok);
			button_Ok.setTypeface(Reguler);
			// if button is clicked, close the custom dialog
			button_Ok.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
				}
			});

			Button button_Cancel = (Button) dialog
					.findViewById(R.id.button_Cancel);
			button_Cancel.setTypeface(Reguler);
			// if button is clicked, close the custom dialog
			button_Cancel.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
					Intent intent = new Intent(FreeBiesActivity.this,
							MainActivity.class);
					intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
							| Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(intent);
					FreeBiesActivity.this.finish();
				}
			});

			button_Ok.setText("Now");
			button_Cancel.setText("Later");

			dialog.show();

		} catch (Exception exception) {
		}
	}

	private boolean isLocationOn() {

		try {

			boolean gps_enabled = false;
			boolean network_enabled = false;
			LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
			try {
				gps_enabled = lm
						.isProviderEnabled(LocationManager.GPS_PROVIDER);
			} catch (Exception ex) {
			}
			try {
				network_enabled = lm
						.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
			} catch (Exception ex) {
			}

			// don't start listeners if no provider is
			// enabled
			boolean hasGPS = getPackageManager().hasSystemFeature(
					PackageManager.FEATURE_LOCATION_GPS);
			WifiManager wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
			if (!(network_enabled && wifi.isWifiEnabled())
					&& (hasGPS && !gps_enabled)) {
				return false;
			} else if (gps_enabled || network_enabled) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return true;
		}

	}

	public void showTwoButtonDialog(final Context context, String Title,
			String message, final int Type) {
		try {

			final Dialog dialog = new Dialog(context);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			// dialog.getWindow().setBackgroundDrawableResource(
			// android.R.color.transparent);
			dialog.setContentView(R.layout.dialog_two_button);
			dialog.setCancelable(false);
			dialog.setCanceledOnTouchOutside(false);
			// dialog.setTitle(null);

			Typeface Reguler = Typeface.createFromAsset(context.getAssets(),
					"EncodeSansNormal-400-Regular.ttf");

			Typeface Medium = Typeface.createFromAsset(context.getAssets(),
					"EncodeSansNarrow-500-Medium.ttf");

			// set the custom dialog components - text, image and button
			TextView textView_Title = (TextView) dialog
					.findViewById(R.id.textView_Title);
			textView_Title.setText(Title);
			textView_Title.setTypeface(Reguler);

			TextView textView_Message = (TextView) dialog
					.findViewById(R.id.textView_Message);
			textView_Message.setText(message);
			textView_Message.setTypeface(Medium);

			Button button_Ok = (Button) dialog.findViewById(R.id.button_Ok);
			button_Ok.setTypeface(Reguler);
			// if button is clicked, close the custom dialog
			button_Ok.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
					Utility.setBackRequired(FreeBiesActivity.this,
							Constants.SHAREDPREFERENCE_BACK_LOCATION, true);
					Intent viewIntent = new Intent(
							Settings.ACTION_LOCATION_SOURCE_SETTINGS);
					context.startActivity(viewIntent);
				}
			});

			Button button_Cancel = (Button) dialog
					.findViewById(R.id.button_Cancel);
			button_Cancel.setTypeface(Reguler);
			// if button is clicked, close the custom dialog
			button_Cancel.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
				}
			});

			button_Ok.setText("Ok");
			button_Cancel.setText("Not now");

			dialog.show();

		} catch (Exception exception) {
		}
	}

	public class FreeBiesTask extends AsyncTask<String, String, String>
			implements OnDismissListener {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			try {

				String time = params[3] == null ? "" : params[3];

				if (time != "") {
					try {
						DateFormat dateFormat = new SimpleDateFormat(
								"yyyy-MM-dd HH:mm:ss");
						Date date = new Date(Long.parseLong(time));
						time = dateFormat.format(date);
					} catch (Exception e) {
					}
				}
				String response = RequestClass.getInstance()
						.readPay1B2CRequest(
								FreeBiesActivity.this,
								Constants.B2C_URL + "GetupdatedDeal/?latitude="
										+ params[0] + "&longitude=" + params[1]
										+ "&user_mobile=" + params[2]
										+ "&updatedTime="
										+ URLEncoder.encode(time, "utf-8")
										+ "&next=" + page);

				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				if (this.dialog.isShowing()) {
					this.dialog.dismiss();
				}
			} catch (Exception e) {
			}
			boolean flag = false;
			try {
				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");
					JSONObject jsonObject = new JSONObject(replaced);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {

						try {

							freeBiesDataSource.open();
							freeBiesLocationDataSource.open();
							freeBiesOrderDataSource.open();

							JSONObject jsonObjectDesc = jsonObject
									.getJSONObject("description");
							freeBiesOrderDataSource.deleteFreeBiesOrder();
							String od = jsonObjectDesc
									.getString("offer_details");

							if (!od.equalsIgnoreCase("")) {
								freeBiesOrderDataSource.createFreeBiesOrder(
										(page + 1), od);
								flag = true;
							} else
								flag = false;

							Utility.setOrder(FreeBiesActivity.this,
									Constants.SHAREDPREFERENCE_GIFT,
									jsonObjectDesc.getString("claimedGifts"));
							Utility.setOrder(FreeBiesActivity.this,
									Constants.SHAREDPREFERENCE_GIFT_EXPIRED,
									jsonObjectDesc.getString("ExpiredOffers"));
							try {
								freeBiesDataSource
										.deleteFreeBies(jsonObjectDesc
												.getString("ExpiredOffers"));
								freeBiesLocationDataSource
										.deleteFreeBiesLocation(jsonObjectDesc
												.getString("ExpiredOffers"));
							} catch (Exception e) {

							}

							JSONArray jsonArrayAlldeals = jsonObjectDesc
									.getJSONArray("Alldeals");
							long timestamp = System.currentTimeMillis();

							freeBiesDataSource.createFreeBies(
									jsonArrayAlldeals, timestamp);
							// for (int i = 0; i < jsonArrayAlldeals.length();
							// i++) {
							//
							// String deal_id = jsonArrayAlldeals
							// .getJSONObject(i).getString("id");
							//
							// String offer_id = jsonArrayAlldeals
							// .getJSONObject(i).getString("of_id");
							// try {
							// freeBiesDataSource.deleteFreeBies(Integer
							// .parseInt(deal_id));
							// freeBiesLocationDataSource
							// .deleteFreeBiesLocation(Integer
							// .parseInt(offer_id));
							// } catch (Exception e) {
							// continue;
							// }
							//
							// String deal_name = jsonArrayAlldeals
							// .getJSONObject(i).getString("name");
							// String url = jsonArrayAlldeals.getJSONObject(i)
							// .getString("i_url");
							// String catagory = jsonArrayAlldeals
							// .getJSONObject(i).getString("cat");
							// String catagory_id = jsonArrayAlldeals
							// .getJSONObject(i).getString("cat_id");
							// String min_amt = jsonArrayAlldeals
							// .getJSONObject(i).getString("min");
							// String offer_name = jsonArrayAlldeals
							// .getJSONObject(i).getString("of_name");
							// String validity = jsonArrayAlldeals
							// .getJSONObject(i).getString("valid");
							// String desc = jsonArrayAlldeals
							// .getJSONObject(i).getString("desc");
							// String offer_lat = "0", offer_lng = "0",
							// offer_add = "", offer_area = "", offer_city = "",
							// offer_state = "";
							//
							// JSONArray jsonArrayLocation = jsonArrayAlldeals
							// .getJSONObject(i).getJSONArray("locs");
							//
							// for (int j = 0; j < jsonArrayLocation.length();
							// j++) {
							// JSONObject jsonObjectInner = jsonArrayLocation
							// .getJSONObject(j);
							// WeakHashMap<String, String> map1 = new
							// WeakHashMap<String, String>();
							//
							// String loc_lat = jsonObjectInner
							// .getString("lat");
							// String loc_lng = jsonObjectInner
							// .getString("lng");
							// String loc_add = jsonObjectInner
							// .getString("addr");
							// String loc_area = jsonObjectInner
							// .getString("area");
							// String loc_city = jsonObjectInner
							// .getString("city");
							// String loc_state = jsonObjectInner
							// .getString("state");
							//
							// if (j == 0) {
							// offer_lat = loc_lat;
							// offer_lng = loc_lng;
							// offer_add = loc_add;
							// offer_area = loc_area;
							// offer_city = loc_city;
							// offer_state = loc_state;
							// }
							//
							// freeBiesLocationDataSource
							// .createFreeBiesLocation(
							// Integer.parseInt(offer_id),
							// Double.parseDouble(loc_lat),
							// Double.parseDouble(loc_lng),
							// loc_add, loc_area,
							// loc_city, loc_state, 0, 0,
							// timestamp);
							// }
							// freeBiesDataSource.createFreeBies(
							// Integer.parseInt(deal_id), deal_name,
							// url, catagory,
							// Integer.parseInt(catagory_id),
							// Double.parseDouble(offer_lat),
							// Double.parseDouble(offer_lng),
							// offer_add, offer_area, offer_city,
							// offer_state, 0,
							// jsonArrayLocation.length(),
							// Integer.parseInt(offer_id), offer_name,
							// validity, desc,
							// Integer.parseInt(min_amt), timestamp);
							// }

							Utility.setSharedPreferences(FreeBiesActivity.this,
									Constants.LAST_GIFT_UPDATED,
									String.valueOf(System.currentTimeMillis()));
						} catch (SQLException exception) {
							// TODO: handle exception
							// Log.e("Er ", exception.getMessage());
						} catch (Exception e) {
							// TODO: handle exception
							// Log.e("Er ", e.getMessage());
						} finally {
							freeBiesDataSource.close();
							freeBiesLocationDataSource.close();
							freeBiesOrderDataSource.close();
						}

					}
				}

				if (flag) {
					try {
						freeBiesDataSource.open();
						freeBiesLocationDataSource.open();
						freeBiesOrderDataSource.open();

						FreeBiesOrder order = freeBiesOrderDataSource
								.getFreeBiesOrder((page + 1));

						List<FreeBies> freebies = freeBiesDataSource
								.getAllFreeBies(
										order != null ? order.getOrderData()
												: "",
										Utility.getOrder(FreeBiesActivity.this,
												Constants.SHAREDPREFERENCE_GIFT));

						int len = freebies.size();
						if (len != 0) {

							if (page == 0) {
								data.clear();
								data_location.clear();
								deal_id_hash.clear();
							}
							++page;
							for (FreeBies freebie : freebies) {

								// if
								// (jsonArrayDesc.getJSONObject(i).has("allow"))
								// {
								// if (jsonArrayDesc.getJSONObject(i)
								// .getString("allow")
								// .equalsIgnoreCase("0"))
								// continue;
								// }
								// if (recharge_amount < Integer
								// .parseInt(jsonObjectOffer
								// .getString("min_amount"))) {
								// // textView_Title.setText("");
								// textView_NoData
								// .setText("You missed out on a free gift! Recharge of minimum Rs. "
								// + jsonObjectOffer
								// .getString("min_amount")
								// + " and win a free gift.");
								// continue;
								// }

								int id = freebie.getFreebieDealID();
								if (!deal_id_hash.contains(id)) {
									deal_id_hash.add(id);
									WeakHashMap<String, String> map = new WeakHashMap<String, String>();

									map.put(DEAL_ID, String.valueOf(freebie
											.getFreebieDealID()));
									map.put(DEAL_NAME, freebie
											.getFreebieDealName());
									map.put(CATAGORY, freebie
											.getFreebieCategory());
									map.put(CATAGORY_ID, String.valueOf(freebie.getFreebieCategoryID()));
									// map.put(LATITUDE,
									// jsonArrayDesc.getJSONObject(i)
									// .getString("latitude"));
									// map.put(LONGITUDE,
									// jsonArrayDesc.getJSONObject(i)
									// .getString("longitude"));

									map.put(URL, freebie.getFreebieURL());

									map.put(OFFER_ID, String.valueOf(freebie.getFreebieOfferID()));
									map.put(OFFER_NAME, freebie
											.getFreebieOfferName());
									map.put(SHORT_DESC, freebie
											.getFreebieShortDesc());
									map.put(MIN_AMOUNT, String.valueOf(freebie.getFreebieMinAmount()));
									map.put(VALIDITY, freebie
											.getFreebieValidity());
									map.put(LOCATION_COUNT, String.valueOf(freebie.getFreebieLocationCount()));

									List<FreeBiesLocation> locations = freeBiesLocationDataSource
											.getAllFreeBiesLocation(freebie
													.getFreebieOfferID());

									int j=0;
									for (FreeBiesLocation location : locations) {

										WeakHashMap<String, String> map1 = new WeakHashMap<String, String>();
										map1.put(DEAL_ID, String
												.valueOf(location
														.getLocationDealID()));
										double latitude = location
												.getLocationLat();
										double longitude = location
												.getLocationLng();
										map1.put(LATITUDE,
												String.valueOf(latitude));
										map1.put(LONGITUDE,
												String.valueOf(longitude));
										map1.put(ADDRESS, location
												.getLocationAddress());
										map1.put(CITY, location
												.getLocationCity());
										map1.put(STATE, location
												.getLocationState());
										String lat = Utility
												.getCurrentLatitude(
														FreeBiesActivity.this,
														Constants.SHAREDPREFERENCE_CURRENT_LATITUDE);
										String lng = Utility
												.getCurrentLongitude(
														FreeBiesActivity.this,
														Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE);
										String dis = "0";
										if (lat != "" && lng != "")
											dis = String
													.valueOf(Constants.distanceFrom(
															Double.parseDouble(lat),
															Double.parseDouble(lng),
															latitude, longitude));
										map1.put(DISTANCE, dis);

										data_location.add(map1);

										if (j == 0) {
											map.put(ADDRESS, location
													.getLocationAddress());
											map.put(DISTANCE, dis);
											++j;
										}
									}
									data.add(map);
								}

								// if (i == 2)
								// break;
							}
							if (data.size() == 0) {
								pager.setVisibility(View.GONE);
								relativeLayout_EmptyView
										.setVisibility(View.VISIBLE);
							} else {
								// Collections.sort(data,
								// new Comparator<WeakHashMap<String, String>>()
								// {
								//
								// @Override
								// public int compare(
								// WeakHashMap<String, String> lhs,
								// WeakHashMap<String, String> rhs) {
								// // TODO Auto-generated
								// // method stub
								// int l, r;
								// try {
								// l = Integer.parseInt(lhs
								//
								// .get(FreeBiesListFragment.MIN_AMOUNT));
								// r = Integer.parseInt(rhs
								// .get(FreeBiesListFragment.MIN_AMOUNT));
								// } catch (Exception e) {
								// l = 0;
								// r = 0;
								// }
								// return Integer.valueOf(l).compareTo(
								// Integer.valueOf(r));
								// }
								// });
								int cc = freeBiesDataSource
										.getAllFreeBiesCount(
												Utility.getOrder(
														FreeBiesActivity.this,
														Constants.SHAREDPREFERENCE_GIFT_EXPIRED),
												Utility.getOrder(
														FreeBiesActivity.this,
														Constants.SHAREDPREFERENCE_GIFT));
								adapter = new FreeBiesPagerAdapter(
										FreeBiesActivity.this
												.getSupportFragmentManager(),
										cc, recharge_amount, trans_id, true,
										data, data_location);
								pager.setAdapter(adapter);
								pager.setVisibility(View.VISIBLE);
								relativeLayout_EmptyView
										.setVisibility(View.GONE);

							}

						} else {
							// new FreeBiesTask()
							// .execute(
							// Utility.getCurrentLatitude(
							// getActivity(),
							// Constants.SHAREDPREFERENCE_CURRENT_LATITUDE),
							// Utility.getCurrentLongitude(
							// getActivity(),
							// Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE));
							pager.setVisibility(View.GONE);
							relativeLayout_EmptyView
									.setVisibility(View.VISIBLE);

						}
					} catch (SQLException exception) {
						// TODO: handle exception
						// // // Log.e("Er ", exception.getMessage());
					} catch (Exception e) {
						// TODO: handle exception
						// // // Log.e("Er ", e.getMessage());
					} finally {
						freeBiesDataSource.close();
						freeBiesLocationDataSource.close();
						freeBiesOrderDataSource.close();
					}
				} else {
					adapter = new FreeBiesPagerAdapter(
							FreeBiesActivity.this.getSupportFragmentManager(),
							data.size(), recharge_amount, trans_id, true, data,
							data_location);
					pager.setAdapter(adapter);
					pager.setVisibility(View.VISIBLE);
				}
			} catch (JSONException e) {
				// e.printStackTrace();

			} catch (Exception e) {
				// TODO: handle exception
			}
			pager.setCurrentItem(selected_item);
		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(FreeBiesActivity.this);
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							dialog.dismiss();
							FreeBiesTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			dialog.dismiss();
			FreeBiesTask.this.cancel(true);
		}
	}

}