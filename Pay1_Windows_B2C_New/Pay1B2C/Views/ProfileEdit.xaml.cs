﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;
using Pay1B2C.Utilities;
using System.Globalization;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.IO;
using System.Windows.Media.Imaging;

namespace Pay1B2C.Views
    {
    public partial class ProfileEdit: PhoneApplicationPage
        {
        ProgressBarManager ProgressBarManager;
        PhotoChooserTask photoChooserTask;
        private DatePickerCustom datePicker;
        string maleFemale="m";
        string base64_code="";

       string fb_data="1";
       String f_url="";

        public ProfileEdit()
            {
            InitializeComponent();

            ProgressIndicator prog = new ProgressIndicator();
            ProgressBarManager = new ProgressBarManager();
            SystemTray.SetProgressIndicator(this, prog);
            ProgressBarManager.Hook(prog);
            this.Loaded+=(a, b) =>
            {
                pageLoaded();
            };

            textMobile.Text=Constant.getUserNumber();
            string pinn = Constant.LoadPersistent<string>(Constant.SETTINGS_USER_PIN);
            string samplePin = "";
            for(int i = 1;i <= pinn.Length;i++)
                {
                samplePin = samplePin + "•";
                }
            textProfilePin.Text = samplePin;



            string ima=Constant.getUserimage();
            if(string.IsNullOrEmpty(Constant.getUserimage()))
                {
                userImagePhoto.Source=new BitmapImage(new Uri("/Images/ic_pay1_launcher.png", UriKind.RelativeOrAbsolute));
                }
            else
                {

                userImagePhoto.Source=new BitmapImage(new Uri(Constant.getUserimage(), UriKind.RelativeOrAbsolute));
                }


            textDOB.Text=Constant.LoadPersistent<string>(Constant.SETTINGS_USER_DOB);
            textName.Text=Constant.getUserName();
            textEmail.Text=Constant.getEmail();

            userImagePhoto.Tap+=(a, b) =>
            {
                photoChooserTask = new PhotoChooserTask();
                photoChooserTask.Completed += new EventHandler<PhotoResult>(photoChooserTask_Completed);
                photoChooserTask.Show();
            };
            }

        private void pageLoaded()
            {
            if(this.datePicker == null)
                {
                this.datePicker = new DatePickerCustom();
                this.datePicker.IsTabStop = false;
                this.datePicker.MaxHeight = 0;
                string dob=Constant.LoadPersistent<string>(Constant.SETTINGS_USER_DOB);

                if(dob.Equals("0000-00-00"))
                    {
                    }
                else
                    {
                    DateTime time = DateTime.Parse(dob);
                    string sampleTime = time.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                    this.datePicker.Value = Convert.ToDateTime(sampleTime);//"2/3/2010");

                    }

                this.datePicker.ValueChanged += new EventHandler<DateTimeValueChangedEventArgs>(datePicker_ValueChanged);


                LayoutRoot.Children.Add(this.datePicker);

                }
            }

        private void datePicker_ValueChanged(object sender, DateTimeValueChangedEventArgs e)
            {
            string dd = this.datePicker.ValueString;
            string[] date = dd.Split('-');
            string day = date[0];
            string mon = date[1];
            string year = date[2];
            DateTime time = DateTime.Parse(dd);
            string formattedDate = time.ToString("yyyy-MM-dd");

            textDOB.Text=formattedDate;
            }

        private void radioButtons_CheckedChanged(object sender, EventArgs e)
            {

            RadioButton radioButton = sender as RadioButton;
            //oper=opDb;

            if(maleRadioButton.IsChecked==true)
                {
                maleFemale="m";

                }
            else if(femaleRadioButton.IsChecked==true)
                {
                maleFemale="f";

                }
            }
        private void photoChooserTask_Completed(object sender, PhotoResult e)
            {
            if(e.TaskResult == TaskResult.OK)
                {
               // MessageBox.Show(e.ChosenPhoto.Length.ToString());

                //Code to display the photo on the page in an image control named myImage.
                System.Windows.Media.Imaging.BitmapImage bmp = new System.Windows.Media.Imaging.BitmapImage();
                bmp.SetSource(e.ChosenPhoto);
                base64_code=Constant.ConvertImageToBase64(bmp);
                fb_data="2";
                f_url="";
                userImagePhoto.Source = bmp;


                byte[] bytearray = null;

                using(MemoryStream ms = new MemoryStream())
                    {
                    if(userImagePhoto.Source == null)
                        {

                        }
                    else
                        {
                        WriteableBitmap wbitmp = new WriteableBitmap((BitmapImage)userImagePhoto.Source);

                        wbitmp.SaveJpeg(ms, 46, 38, 0, 100);
                        bytearray = ms.ToArray();
                        }
                    }
                string str = Convert.ToBase64String(bytearray);

                base64_code=str;

                }
            }


      
               
        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
            {
            NavigationService.Navigate(new Uri("/Views/Profile.xaml?", UriKind.RelativeOrAbsolute));
            NavigationService.RemoveBackEntry();
            base.OnBackKeyPress(e);
            }



        private void ButtonImage_Click_Dob(object sender, RoutedEventArgs e)
            {
            datePicker.ClickTemplateButton();
            }


        private void ButtonImage_Click_EditPin(object sender, RoutedEventArgs e)
            {
            updatePinTask();
            }

        private async void updatePinTask()
            {
            ProgressBarManager.Show(19, "Please wait...", false, 10);
          

            var values = new List<KeyValuePair<string, string>>();
            values.Add(new KeyValuePair<string, string>("actiontype", "forgotpwd"));
            values.Add(new KeyValuePair<string, string>("user_name",Constant.getUserNumber()));
            values.Add(new KeyValuePair<string, string>("api_version", Constant.API_VERSION));
            values.Add(new KeyValuePair<string, string>("vtype", "1"));
            values.Add(new KeyValuePair<string, string>("api", "true"));
            FormUrlEncodedContent values1 = new FormUrlEncodedContent(values);
            Task<string> response = Constant.postParamsAndgetResponse(Constant.B2C_URL, values1);

            string result = await response;
            if(string.IsNullOrEmpty(result))
                {
                NavigationService.Navigate(new Uri("/Views/NoConnection.xaml?", UriKind.RelativeOrAbsolute));
                }
            else
                {
                var dict = (JObject)JsonConvert.DeserializeObject(result);
                string status = dict["status"].ToString();
                if(status.Equals("success"))
                    {
                    PhoneApplicationService.Current.State["updateFromLogin"] = 2;
                    MessageBox.Show("OTP has been sent to your registered Pay1 number");
                    NavigationService.Navigate(new Uri("/Views/UpdatePassword.xaml?", UriKind.RelativeOrAbsolute));
                    }
                else
                    {
                    Constant.checkForErrorCode(result);
                    }
                }
            ProgressBarManager.Hide(19);
            }




        private void ButtonImage_Click(object sender, RoutedEventArgs e)
            {
            NavigationService.Navigate(new Uri("/Views/Profile.xaml?", UriKind.RelativeOrAbsolute));
            NavigationService.RemoveBackEntry();
            }

            private void textEditAndSave_Click(object sender, RoutedEventArgs e)
                {
                updateFullProfile();
                }

            private async void updateFullProfile()
                {
                ProgressBarManager.Show(18, "Please Wait", false, 10);

                var values = new List<KeyValuePair<string, string>>();
                values.Add(new KeyValuePair<string, string>("actiontype", "update_profile"));
                values.Add(new KeyValuePair<string, string>("date_of_birth", textDOB.Text));
                values.Add(new KeyValuePair<string, string>("name", textName.Text));

                values.Add(new KeyValuePair<string, string>("gender", maleFemale));
                values.Add(new KeyValuePair<string, string>("email", textEmail.Text));

                values.Add(new KeyValuePair<string, string>("code", base64_code));
                values.Add(new KeyValuePair<string, string>("f_url", f_url));
                values.Add(new KeyValuePair<string, string>("fb_data", fb_data));
              



                values.Add(new KeyValuePair<string, string>("api_version", Constant.API_VERSION));
                values.Add(new KeyValuePair<string, string>("api", "true"));



                FormUrlEncodedContent values1 = new FormUrlEncodedContent(values);
                Task<string> response = Constant.postParamsAndgetResponse(Constant.B2C_URL, values1);

                string result = await response;
                if(string.IsNullOrEmpty(result))
                    {
                    NavigationService.Navigate(new Uri("/Views/NoConnection.xaml?", UriKind.RelativeOrAbsolute));
                    }
                else
                    {
                    var dict = (JObject)JsonConvert.DeserializeObject(result);
                    string status = dict["status"].ToString();
                    if(status.Equals("success"))
                        {
                        var profileData=dict["profileData"];
                        Constant.setUserNumber(profileData["mobile"].ToString());
                        Constant.SavePersistent(Constant.SETTINGS_USER_PIN, Constant.LoadPersistent<String>(Constant.SETTINGS_USER_PIN));
                        Constant.SavePersistent(Constant.SETTINGS_USER_GENDER, profileData["gender"].ToString());
                        Constant.SavePersistent(Constant.SETTINGS_USER_DOB, profileData["date_of_birth"].ToString());
                        Constant.SavePersistent(Constant.SETTINGS_USER_EMAIL, profileData["email"].ToString());
                        Constant.setEmail(profileData["email"].ToString());
                        Constant.setUserName(profileData["name"].ToString());
                        Constant.setUserimage(profileData["image"].ToString());
                        MessageBox.Show("Profile updated successfully.");
                        NavigationService.Navigate(new Uri("/Views/Profile.xaml?", UriKind.RelativeOrAbsolute));
                        NavigationService.RemoveBackEntry();
                        }
                    else
                        {
                        Constant.checkForErrorCode(result);
                        }
                    }

                ProgressBarManager.Hide(18);
                }
        }
    }