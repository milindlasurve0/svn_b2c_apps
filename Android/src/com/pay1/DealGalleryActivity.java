package com.pay1;

import java.util.ArrayList;
import java.util.WeakHashMap;

import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.pay1.adapterhandler.DealGalleryAdapter;
import com.pay1.constants.Constants;

public class DealGalleryActivity extends AppBaseActivity {

	private static final String SCREEN_LABEL = "Deal Gallery Screen";
	private EasyTracker easyTracker = null;
	// private final String TAG = "Deal Gallery";

	private ListView listView_Gallery;
	private TextView textView_Title;
	private ImageView imageView_Back;
	private ArrayList<WeakHashMap<String, String>> data_gallery;
	private DealGalleryAdapter adapter;

	// private ImageView imageView_Gal;
	// private ImageLoader imageLoader;
	// private Accordion linearLayout_Gal;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.deal_gallery_activity);
		try {
			easyTracker = EasyTracker.getInstance(DealGalleryActivity.this);
			easyTracker.set(Fields.SCREEN_NAME, SCREEN_LABEL);

			Typeface Reguler = Typeface.createFromAsset(getAssets(),
					"EncodeSansNormal-400-Regular.ttf");

			textView_Title = (TextView) findViewById(R.id.textView_Title);
			textView_Title.setTypeface(Reguler);

			imageView_Back = (ImageView) findViewById(R.id.imageView_Back);
			imageView_Back.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onBackPressed();
				}
			});

			try {
				data_gallery = new ArrayList<WeakHashMap<String, String>>(
						(ArrayList<WeakHashMap<String, String>>) getIntent()
								.getSerializableExtra("GALLERY"));
			} catch (Exception e) {
				data_gallery = new ArrayList<WeakHashMap<String, String>>();
			}

			// linearLayout_Gal = (Accordion)
			// findViewById(R.id.linearLayout_Gal);
			//
			// imageLoader = new ImageLoader(DealGalleryActivity.this);
			// for (int i = 0; i < data_gallery.size(); i++) {
			// imageView_Gal = new ImageView(DealGalleryActivity.this);
			// LinearLayout.LayoutParams params = new LayoutParams(
			// LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			// imageView_Gal.setLayoutParams(params);
			// imageView_Gal.setScaleType(ScaleType.CENTER);
			// linearLayout_Gal.addView(imageView_Gal);
			// imageLoader
			// .getDealImage(
			// data_gallery.get(i).get(
			// DealDetailsActivity.GALARY_URL),
			// imageView_Gal);
			// }

			listView_Gallery = (ListView) findViewById(R.id.listView_Gallery);
			adapter = new DealGalleryAdapter(DealGalleryActivity.this,
					data_gallery);
			listView_Gallery.setAdapter(adapter);
			listView_Gallery.setClickable(false);

		} catch (Exception exception) {
			// Log.e(TAG, exception.getMessage());
		}
		overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}

	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView().build());
		// EasyTracker.getInstance(this).activityStart(this);
		easyTracker.send(MapBuilder.createAppView().build());
	}

	@Override
	public void onStop() {
		super.onStop();
		// EasyTracker.getInstance(this).activityStop(this);
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (Constants.isExpanded) {
			Constants.collapsView(this);
		} else {
			super.onBackPressed();
			overridePendingTransition(R.anim.slide_in_left,
					R.anim.slide_out_right);
		}
	}

}