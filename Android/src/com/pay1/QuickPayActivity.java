package com.pay1;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Dialog;

import com.pay1.customviews.MyProgressDialog;

import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fortysevendeg.swipelistview.BaseSwipeListViewListener;
import com.fortysevendeg.swipelistview.SwipeListView;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.pay1.adapterhandler.ItemAdapter;
import com.pay1.constants.Constants;
import com.pay1.requesthandler.RequestClass;

public class QuickPayActivity extends AppBaseActivity {

	private static final String SCREEN_LABEL = "Quick Pay Screen";
	private EasyTracker easyTracker = null;
	private final String TAG = "Quick Pay";

	private ImageView imageView_Add, imageView_Back, imageView_Mobile,
			imageView_DTH, imageView_DataCard;
	private SwipeListView swipelistview;
	ItemAdapter adapter;
	private ArrayList<JSONObject> data;
	private TextView textView_NoData, textView_Shop, textView_Title,
			textView_DTH, textView_Mobile, textView_Data;
	private RelativeLayout relativeLayout_EmptyView;
	SwipeDetector swipeDetector;
	int tag;
	ImageView imageQuickInfo;
	public static final String NAME = "name";
	public static final String NUMBER = "number";
	public static final String AMOUNT = "amount";
	Button swipe_delete;

	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sample_quick);
		try {
			easyTracker = EasyTracker.getInstance(QuickPayActivity.this);
			easyTracker.set(Fields.SCREEN_NAME, SCREEN_LABEL);
			registerBaseActivityReceiver();

			Typeface Reguler = Typeface.createFromAsset(getAssets(),
					"EncodeSansNormal-400-Regular.ttf");

			textView_Title = (TextView) findViewById(R.id.textView_Title);
			textView_Title.setTypeface(Reguler);
			textView_NoData = (TextView) findViewById(R.id.textView_NoData);
			textView_NoData.setTypeface(Reguler);
			textView_Shop = (TextView) findViewById(R.id.textView_Shop);
			textView_Shop.setTypeface(Reguler);
			textView_DTH = (TextView) findViewById(R.id.textView_DTH);
			textView_DTH.setTypeface(Reguler);
			textView_Mobile = (TextView) findViewById(R.id.textView_Mobile);
			textView_Mobile.setTypeface(Reguler);
			textView_Data = (TextView) findViewById(R.id.textView_Data);
			textView_Data.setTypeface(Reguler);

			swipelistview = (SwipeListView) findViewById(R.id.example_swipe_lv_list);
			// swipelistview.setSwipeMode(SwipeListView.SWIPE_MODE_RIGHT);
			swipeDetector = new SwipeDetector();
			imageQuickInfo = (ImageView) findViewById(R.id.imageQuickInfo);
			imageQuickInfo.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent intent = new Intent(QuickPayActivity.this,
							QuickPayInfoActivity.class);
					startActivity(intent);
				}
			});
			imageView_Back = (ImageView) findViewById(R.id.imageView_Back);
			imageView_Back.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onBackPressed();
				}
			});

			imageView_Mobile = (ImageView) findViewById(R.id.imageView_Mobile);
			imageView_Mobile.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent intent = new Intent(QuickPayActivity.this,
							RechargeMobileActivity.class);
					intent.putExtra(Constants.RECHARGE_FOR,
							Constants.RECHARGE_MOBILE);
					startActivity(intent);
					finish();
				}
			});

			imageView_DTH = (ImageView) findViewById(R.id.imageView_DTH);
			imageView_DTH.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent intent = new Intent(QuickPayActivity.this,
							RechargeDthActivity.class);
					intent.putExtra(Constants.RECHARGE_FOR,
							Constants.RECHARGE_DTH);
					startActivity(intent);
					finish();
				}
			});

			imageView_DataCard = (ImageView) findViewById(R.id.imageView_DataCard);
			imageView_DataCard.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent intent = new Intent(QuickPayActivity.this,
							RechargeMobileActivity.class);
					intent.putExtra(Constants.RECHARGE_FOR,
							Constants.RECHARGE_DATA);
					startActivity(intent);
					finish();
				}
			});

			imageView_Add = (ImageView) findViewById(R.id.imageView_Add);

			relativeLayout_EmptyView = (RelativeLayout) findViewById(R.id.relativeLayout_EmptyView);
			imageView_Add.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

				}
			});
			new GetQuickPayList().execute();

			data = new ArrayList<JSONObject>();
			swipelistview.setEmptyView(relativeLayout_EmptyView);

			adapter = new ItemAdapter(this, R.layout.custom_row_new, data,
					listener);
			// These are the swipe listview settings. you can change these
			// setting as your requirement
			swipelistview.setSwipeMode(SwipeListView.SWIPE_MODE_LEFT);
			// swipelistview.setSwipeActionLeft(SwipeListView.SWIPE_ACTION_REVEAL);
			swipelistview
					.setSwipeActionRight(SwipeListView.SWIPE_ACTION_REVEAL);
			swipelistview.setOffsetLeft(convertDpToPixel(100f));
			swipelistview.setAnimationTime(500);
			swipelistview.setSwipeOpenOnLongPress(true);
			swipelistview.setAdapter(adapter);

			/*
			 * swipelistview.setOnTouchListener(swipeDetector);
			 * swipelistview.setOnItemClickListener(listener);
			 */

			swipelistview
					.setSwipeListViewListener(new BaseSwipeListViewListener() {
						@Override
						public void onOpened(int position, boolean toRight) {
							Log.d("daf", "sdg");
							tag = position;

						}

						@Override
						public void onClosed(int position, boolean fromRight) {
							// Toast.makeText(QuickPayActivity.this,
							// "Test 1111   "+fromRight,
							// Toast.LENGTH_SHORT).show();
							Log.d("daf", "sdg");
						}

						@Override
						public void onListChanged() {
							Log.d("daf", "sdg");
						}

						@Override
						public void onMove(int position, float x) {
							Log.d("daf", "sdg");
						}

						@Override
						public void onStartOpen(int position, int action,
								boolean right) {
							swipelistview.closeOpenedItems();
							Log.d("swipe", String.format(
									"onStartOpen %d - action %d", position,
									action));
							/*
							 * if(right){ Toast.makeText(QuickPayActivity.this,
							 * "Test "+right, Toast.LENGTH_SHORT).show(); }else{
							 * 
							 * }
							 */
						}

						@Override
						public void onStartClose(int position, boolean right) {
							Log.d("swipe",
									String.format("onStartClose %d", position));
						}

						@Override
						public void onClickFrontView(int position) {
							Log.d("swipe", String.format("onClickFrontView %d",
									position));
							swipelistview.closeOpenedItems();
							// swipelistview.openAnimate(position); //when you
							// touch front
							// view it will open

						}

						@Override
						public void onClickBackView(int position) {
							Log.d("swipe", String.format("onClickBackView %d",
									position));

							swipelistview.closeAnimate(position);// when you
																	// touch
																	// back view
																	// it will
																	// close
						}

						@Override
						public void onDismiss(int[] reverseSortedPositions) {
							Log.d("daf", "sdg");

						}

					});
		} catch (Exception e) {
		}
		overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

	}

	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView().build());
		// EasyTracker.getInstance(this).activityStart(this);
		easyTracker.send(MapBuilder.createAppView().build());
	}

	@Override
	public void onStop() {
		super.onStop();
		// EasyTracker.getInstance(this).activityStop(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (Constants.isExpanded) {
			Constants.collapsView(this);
		} else {
			Constants.SELECTED_MENU_INDEX = Constants.PROFILE_MENU_INDEX;
			Constants.showNavigationBar(QuickPayActivity.this);
		}
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unRegisterBaseActivityReceiver();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (Constants.isExpanded) {
			Constants.collapsView(this);
		} else {
			super.onBackPressed();
			overridePendingTransition(R.anim.slide_in_left,
					R.anim.slide_out_right);
		}
	}

	public int convertDpToPixel(float dp) {
		DisplayMetrics metrics = getResources().getDisplayMetrics();
		float px = dp * (metrics.densityDpi / 160f);
		return (int) px;
	}

	public class GetQuickPayList extends AsyncTask<String, String, String>
			implements OnDismissListener {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String url = Constants.B2C_URL + "get_quickpaylist/?";
			String response = RequestClass.getInstance().readPay1B2CRequest(
					QuickPayActivity.this, url);
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			if (!result.startsWith("Error")) {

				String status;
				try {
					JSONObject jsonObject = new JSONObject(result);
					status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {
						data.clear();
						JSONArray description = jsonObject
								.getJSONArray("description");
						for (int j = 0; j < description.length(); j++) {
							JSONObject jObject = description.getJSONObject(j);
							data.add(jObject);
						}

						adapter.notifyDataSetChanged();

					} else {
						Constants.showOneButtonDialog(QuickPayActivity.this,
								TAG, Constants.checkCode(result),
								Constants.DIALOG_CLOSE);
					}

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(QuickPayActivity.this);
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							GetQuickPayList.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			GetQuickPayList.this.cancel(true);
			dialog.cancel();
		}

	}

	private OnClickListener listener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			// new DeleteQuickTask().execute(id);
			try {
				swipelistview.closeOpenedItems();
				final Dialog dialog = new Dialog(QuickPayActivity.this);
				dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				// dialog.getWindow().setBackgroundDrawableResource(
				// android.R.color.transparent);
				dialog.setContentView(R.layout.dialog_success_two_button);
				// dialog.setTitle(null);

				Typeface Reguler = Typeface.createFromAsset(getAssets(),
						"EncodeSansNormal-400-Regular.ttf");

				Typeface Narrow = Typeface.createFromAsset(getAssets(),
						"EncodeSansNarrow-500-Medium.ttf");

				// set the custom dialog components - text, image and button
				TextView textView_Title = (TextView) dialog
						.findViewById(R.id.textView_Title);
				textView_Title.setText("Quick pay");
				textView_Title.setTypeface(Reguler);

				TextView textView_Message = (TextView) dialog
						.findViewById(R.id.textView_Message);
				textView_Message
						.setText("Are you sure you want to delete this entry?");
				textView_Message.setTypeface(Narrow);

				ImageView imageView_Ok = (ImageView) dialog
						.findViewById(R.id.imageView_Ok);
				// if button is clicked, close the
				// custom dialog
				imageView_Ok.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						dialog.dismiss();

						swipelistview.closeAnimate(tag);
						String id;
						try {
							id = data.get(tag).getString("id");
							new DeleteQuickTask().execute(id);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}
				});

				ImageView imageView_Cancel = (ImageView) dialog
						.findViewById(R.id.imageView_Cancel);
				// if button is clicked, close the
				// custom dialog
				imageView_Cancel.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						dialog.dismiss();
					}
				});

				dialog.show();

			} catch (Exception exception) {
			}
		}

	};

	public class DeleteQuickTask extends AsyncTask<String, String, String>
			implements OnDismissListener {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String url = Constants.B2C_URL + "del_quickpay/?id=" + params[0];
			String response = RequestClass.getInstance().readPay1B2CRequest(
					QuickPayActivity.this, url);
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			if (!result.startsWith("Error")) {

				String status;
				try {
					JSONObject jsonObject = new JSONObject(result);
					status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {

						Constants.showOneButtonDialog(QuickPayActivity.this,
								TAG, jsonObject.getString("description"),
								Constants.DIALOG_CLOSE);
						// swipelistview.removeViewAt(tag);
						// swipelistview.closeAnimate(tag);
						data.remove(tag);
						adapter.notifyDataSetChanged();

					} else {
						Constants.showOneButtonDialog(QuickPayActivity.this,
								TAG, Constants.checkCode(result),
								Constants.DIALOG_CLOSE);
					}

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(QuickPayActivity.this);
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							DeleteQuickTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			DeleteQuickTask.this.cancel(true);
			dialog.cancel();
		}

	}

}
