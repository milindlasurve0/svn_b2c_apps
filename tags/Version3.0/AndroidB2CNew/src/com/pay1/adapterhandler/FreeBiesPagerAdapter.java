package com.pay1.adapterhandler;

import java.util.ArrayList;
import java.util.WeakHashMap;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.pay1.FreeBiesPagerFragment;

public class FreeBiesPagerAdapter extends FragmentStatePagerAdapter

{
	ArrayList<WeakHashMap<String, String>> data, data_location;
	int count, recharge_amount;
	String trans_id;
	boolean is_FromRecharge;

	public FreeBiesPagerAdapter(FragmentManager fm, int count,
			int recharge_amount, String trans_id, boolean is_FromRecharge,
			ArrayList<WeakHashMap<String, String>> data,
			ArrayList<WeakHashMap<String, String>> data_location) {
		super(fm);
		this.recharge_amount = recharge_amount;
		this.trans_id = trans_id;
		this.is_FromRecharge = is_FromRecharge;
		this.data = data;
		this.data_location = data_location;
		this.count = count;
	}

	@Override
	public Fragment getItem(int position) {
		return FreeBiesPagerFragment.newInstance(recharge_amount, trans_id,
				count, position, is_FromRecharge, data, data_location);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * android.support.v4.view.PagerAdapter#getItemPosition(java.lang.Object)
	 */
	@Override
	public int getItemPosition(Object object) {
		// TODO Auto-generated method stub
		return POSITION_NONE;
	}

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return null;
	}

	// @Override
	// public int getIconResId(int index) {
	// return ICONS[index % ICONS.length];
	// }

	// public void setCount(int count) {
	// if (count > 0 && count <= 10) {
	// mCount = count;
	// notifyDataSetChanged();
	// }
	// }
}
