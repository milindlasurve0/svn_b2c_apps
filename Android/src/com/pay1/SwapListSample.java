package com.pay1;

/*
 * package com.pay1;
 * 
 * import java.util.ArrayList; import java.util.WeakHashMap;
 * 
 * import android.app.Activity; import android.content.Intent; import
 * android.content.res.Configuration; import android.os.Bundle; import
 * android.util.Log; import android.view.View; import android.view.ViewGroup;
 * import android.widget.ImageView; import android.widget.Toast;
 * 
 * import com.fortysevendeg.swipelistview.BaseSwipeListViewListener; import
 * com.fortysevendeg.swipelistview.SwipeListView; import
 * com.fortysevendeg.swipelistview.SwipeListViewListener; import
 * com.pay1.constants.Constants;
 * 
 * public class SwapListSample extends Activity {
 * 
 * private final String TAG = "Quick Pay";
 * 
 * private ImageView imageView_Add, imageView_Back; private SwipeListView
 * swipelistview; ItemAdapter adapter; private ArrayList<WeakHashMap<String,
 * String>> data;
 * 
 * protected void onCreate(Bundle savedInstanceState) { // TODO Auto-generated
 * method stub super.onCreate(savedInstanceState);
 * setContentView(R.layout.sample_quick); try {
 * 
 * imageView_Add = (ImageView) findViewById(R.id.imageView_Add);
 * imageView_Add.setOnClickListener(new View.OnClickListener() {
 * 
 * @Override public void onClick(View v) { // TODO Auto-generated method stub
 * Intent intent = new Intent(SwapListSample.this, AddQuickPayeeActivity.class);
 * startActivity(intent); } });
 * 
 * data = new ArrayList<WeakHashMap<String, String>>(); for (int i = 0; i < 5;
 * i++) { WeakHashMap<String, String> hashMap = new WeakHashMap<String,
 * String>(); hashMap.put("number", "8819079381"); hashMap.put("name", "Umesh");
 * data.add(hashMap); } swipelistview = (SwipeListView)
 * findViewById(R.id.example_swipe_lv_list); adapter = new ItemAdapter(this,
 * R.layout.custom_row, data); swipelistview.setAdapter(adapter);
 * 
 * } catch (Exception e) { }
 * 
 * 
 * }
 * 
 * @Override protected void onResume() { // TODO Auto-generated method stub
 * super.onResume(); Constants.showNavigationBar(SwapListSample.this); }
 * 
 * @Override protected void onPause() { // TODO Auto-generated method stub
 * super.onPause(); }
 * 
 * @Override protected void onDestroy() { // TODO Auto-generated method stub
 * super.onDestroy(); }
 * 
 * @Override public void onConfigurationChanged(Configuration newConfig) { //
 * TODO Auto-generated method stub super.onConfigurationChanged(newConfig); }
 * 
 * @Override public void onBackPressed() { // TODO Auto-generated method stub if
 * (Constants.isExpanded) { Constants.collapsView(this); } else {
 * super.onBackPressed(); } }
 * 
 * }
 */