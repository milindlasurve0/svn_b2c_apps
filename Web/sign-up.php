
<?php
session_start();
require_once 'function.php';
require_once 'left.php';
require_once 'config.php';



?>

	<!-- //close navigation -->
       
<div class="shiftbox">
	<div class="hutpart" style="height:100%;width:100%; display:block; position:absolute;">
		<div class="container">
			<div class="row mT20">
				<div class="col-md-6 col-md-offset-3">
					<div class="signlogBOX">
						<span class="btn-info">Sign Up to Begin!</span><br/>
                                                <span style="color:red;" id="errormsg"></span>
                                                <form class="form mT20" role="form" action="sign-up.php" method="post" id="submit">
						  <div class="form-group">
						    <label class="" for="">Mobile Number</label>
						    <div class="input-group">
						      <div class="input-group-addon">
						      	<span class="loginsign-mobile"></span>
						      </div>
                                                        <span style="color:red;"><?php echo $mobErr;?></span>
                                                        <input class="form-control" onkeypress="return isNumberKey(event)" maxlength="10" type="text" id="mob_number" name="mob_number">
                                                        
						    </div>
						  </div>
                                                    
<!--                                                     <div class="form-group">
						    <label class="" for="">Email Id</label>
						    <div class="input-group">
						      <div class="input-group-addon">
						      	<span class="loginsign-mobile"></span>
						      </div>
                                                        <span style="color:red;"><?php echo $emailErr;?></span>
						      <input class="form-control"  size="50" type="text" name="email" id="email">	
                                                     
						    </div>
						  </div>-->
<!--						  <div class="form-group">
						    <label class="" for="">Date of Birth</label>
						    <div class="signDOB">
						      <input class="" type="text" placeholder="DD">
						      <input class="" type="text" placeholder="MM">
						      <input class="" type="text" placeholder="YYYY">
						    </div>
						  </div>-->
						  <div class="form-group">
						  	<div><label class="" for="">Gender</label></div>
						  	<div class="btn-group" data-toggle="buttons">
								  <label class="btn btn-primary">
                                                                      <input type="radio" name="gender" value="m" id="male">Male
								  </label>
								  <label class="btn btn-primary">
                                                                      <input type="radio" name="gender" value="f" id="female">Female
								  </label>
								</div>
                                                        <span style="color:red;"><?php echo $genErr;?></span>
						  </div>
						  <div class="form-group">
						  	<label class="" for="exampleInputEmail2">Have Coupon Code?</label>
						    <div class="input-group">
						      <div class="input-group-addon">
						      	<span class="loginsign-coupon"></span>
						      </div>
						      <input class="form-control" type="password">
						    </div>
						  </div>
						  <div class="form-group">
						  	<button type="button" id="signup" class="btn btn-primary btn-lg btn-block">Sign Up</button>
                                                        <p class="logOR">Or</p>
								<a href="login.php" class="btn btn-default btn-lg btn-block">Login</a>
						  	<span class="termncond">By signing up you agree to our <a href="#">Terms &amp; Conditions.</a></span>
						  </div>
<!--                                                  <div class="form-group">
						  	<button type="button" id="login" class="btn btn-primary btn-lg btn-block">Login</button>
						  	
						  </div>-->
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div><!-- //shiftbox -->
</div>
<script src="js/jquery.min.2.1.1.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript">
    $('#signup').click(function(){
        var mobileNo = $("#mob_number").val();
      //  var email = $("#email").val();
        var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        var gender = $('input:radio[name=gender]:checked').val();
        
        
        //alert(gender);
        
        
        if(mobileNo =='')
        {
            alert("Please enter a  mobile no.");
            return false;
        }
//        else if(email=='')
//        {
//            alert("Password enter email id");
//            return false;
//        }
//        else if (!filter.test(email)) {
//        alert('Please provide a valid email address');
//        $("#emailid").focus();
//        return false;
//        }
        else if(gender=='undefined' || gender == '')
        {
            alert("Please select Gender");
            return false;
            
        }
        else
        {
           var url = "<?php echo CDEV_URL; ?>/index.php/api_new/action/api/true/actiontype/create_user/?";
            $.ajax({
            url: url,
            type:"GET",
            data:{mobile_number:mobileNo,
                  gender:gender,
                  date_of_birth:"",
                  res_format : "jsonp"
                 },
            timeout: 50000,
            dataType: "jsonp",
            jsonpCallback: 'callback',
            crossDomain: true,
            success:function(data){
                //console.log(data);
                if(data.status=="success")
                {
                    $("#errormsg").html(data.description);
                     localStorage.setItem("mob_number",mobileNo);
                     localStorage.setItem("gender",gender);
                     window.location = "update_password.php";
                }
                else
                {
                  $("#errormsg").html(data.description);  
                }

               
            },
            error: function (xhr,error) {
             },  
         
            });
        }

});

function isNumberKey(evt){
 var charCode = (evt.which) ? evt.which : evt.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true
     }
</script>
</body>
</html>