package com.pay1.adapterhandler;

import java.util.ArrayList;
import java.util.WeakHashMap;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.pay1.DealActivity;
import com.pay1.R;
import com.pay1.imagehandler.ImageLoader;

public class DealAdapter extends BaseAdapter {

	private Context context;
	private ArrayList<WeakHashMap<String, String>> data;
	private ImageLoader imageLoader;
	private Typeface Reguler, Narrow, Narrow_thin;

	public DealAdapter(Context context, ArrayList<WeakHashMap<String, String>> data) {
		this.context = context;
		this.data = data;
		imageLoader = new ImageLoader(context);
		Reguler = Typeface.createFromAsset(context.getAssets(),
				"EncodeSansNormal-400-Regular.ttf");

		Narrow = Typeface.createFromAsset(context.getAssets(),
				"EncodeSansNarrow-500-Medium.ttf");

		Narrow_thin = Typeface.createFromAsset(context.getAssets(),
				"EncodeSansNarrow-100-Thin.ttf");
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder viewHolder;
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.deal_adapter, null);

			viewHolder = new ViewHolder();
			viewHolder.imageView_Pic = (ImageView) convertView
					.findViewById(R.id.imageView_Pic);
			viewHolder.textView_DealName = (TextView) convertView
					.findViewById(R.id.textView_DealName);
			viewHolder.textView_DealName.setTypeface(Reguler);
			viewHolder.textView_Amount = (TextView) convertView
					.findViewById(R.id.textView_Amount);
			viewHolder.textView_Amount.setTypeface(Reguler);
			viewHolder.textView_OfferAmount = (TextView) convertView
					.findViewById(R.id.textView_OfferAmount);
			viewHolder.textView_OfferAmount.setTypeface(Narrow);
			viewHolder.textView_Sold = (TextView) convertView
					.findViewById(R.id.textView_Sold);
			viewHolder.textView_Sold.setTypeface(Narrow);
			viewHolder.textView_Discount = (TextView) convertView
					.findViewById(R.id.textView_Discount);
			viewHolder.textView_Discount.setTypeface(Narrow);
			convertView.setTag(viewHolder);

		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		WeakHashMap<String, String> map = new WeakHashMap<String, String>();
		map = data.get(position);
		// viewHolder.webView_Deal.loadData("<div style='border-radius: 10px;box-shadow: 0 1px 2px #888888;float: left;height: 320px;margin: 18px;width: 292px;'><div><img src='http://cdev.pay1.in/kcfinder/upload/files/1397196557324.jpg' style='border-radius:10px 10px 0px 0px; height:176px; width:292px' /></div><div class='detailsContainer' style='height: 112px;padding-left: 10px;position: absolute;width: 275px;'><div class='dealsTitleSold'><p><strong><span style='color:#A52A2A'>Rollacosta Restaurant</span></strong></p></div></div></div>",
		// "text/html", "utf-8");//.loadUrl(map.get(DealActivity.CATAGORY)
		// .toString().trim());

		// viewHolder.webView_Deal
		// .loadDataWithBaseURL(
		// null,
		// "<div style='border-radius: 10px;box-shadow: 0 1px 2px #888888;float: left;height: 320px;margin: 5px;'>"
		// + "<div>"
		// +
		// "<img src='http://cdev.pay1.in/kcfinder/upload/files/1397196557324.jpg' style='border-radius:10px 10px 0px 0px; height:176px; width:292px' />"
		// + "</div>"
		// +
		// "<div class='detailsContainer' style='height: 112px;padding-left: 10px;position: absolute;width: 275px;'>"
		// +
		// "<div class='dealsTitle' style='color: #0185C6; display: inline-block; font-size: 1.1em; font-weight: bold;  overflow: hidden; width: 100%;'>"
		// + "7 Locations: Flat 50% OFF From The Menu at Rollacosta Restaurant"
		// + "</div>"
		// + "<div class='dealsTitleSold'>"
		// +
		// "<p><strong><span style='color:#A52A2A'>Rollacosta Restaurant</span></strong></p>"
		// +
		// "<div class='price_sold' style=' color: #89B556;float: right;font-size: 1.4em;font-weight: 500;line-height: 1.1em;margin-top: 8px;padding-right: 5px;text-align: right;width: 100%;'>"
		// + "<span style='color:#A9A9A9'>"
		// + "<s>Rs29.00</s>"
		// + "</span> Rs29.00"
		// + "</div>" + "</div>" + "</div>" + "</div>",
		// "text/html", "UTF-8", null);

		imageLoader.getDealImage(map.get(DealActivity.URL),
				viewHolder.imageView_Pic);
		viewHolder.textView_DealName.setText(map.get(DealActivity.DEAL_NAME));
		viewHolder.textView_Amount.setText("Rs. "
				+ map.get(DealActivity.ACTUAL_PRICE));
		viewHolder.textView_Amount.setPaintFlags(viewHolder.textView_Amount
				.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
		viewHolder.textView_OfferAmount.setText("Rs. "
				+ map.get(DealActivity.OFFER_PRICE));
		viewHolder.textView_Sold.setText(map.get(DealActivity.STOCK_SOLD)
				+ " Bought");
		viewHolder.textView_Discount.setText(map.get(DealActivity.DISCOUNT)
				+ "% Discount");

		double actual_amt = 0;
		try {
			actual_amt = Double.parseDouble(map.get(DealActivity.ACTUAL_PRICE));
		} catch (Exception e) {
			actual_amt = 0;
		}

		if (actual_amt == 0) {
			viewHolder.textView_Amount.setVisibility(View.GONE);
			viewHolder.textView_Discount.setVisibility(View.GONE);
		} else {
			viewHolder.textView_Amount.setVisibility(View.VISIBLE);
			viewHolder.textView_Discount.setVisibility(View.VISIBLE);
		}

		return convertView;
	}

	private class ViewHolder {
		ImageView imageView_Pic;
		TextView textView_DealName, textView_Amount, textView_OfferAmount,
				textView_Sold, textView_Discount;
	}
}
