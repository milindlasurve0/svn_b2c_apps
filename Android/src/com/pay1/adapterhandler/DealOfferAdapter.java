package com.pay1.adapterhandler;

import java.util.ArrayList;
import java.util.WeakHashMap;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.pay1.R;
import com.pay1.DealOfferActivity;

public class DealOfferAdapter extends BaseAdapter {

	private Context context;
	private ArrayList<WeakHashMap<String, String>> data;
	private Typeface Reguler, Narrow, Narrow_thin;

	public DealOfferAdapter(Context context,
			ArrayList<WeakHashMap<String, String>> data) {
		this.context = context;
		this.data = data;

		Reguler = Typeface.createFromAsset(context.getAssets(),
				"EncodeSansNormal-400-Regular.ttf");

		Narrow = Typeface.createFromAsset(context.getAssets(),
				"EncodeSansNarrow-500-Medium.ttf");

		Narrow_thin = Typeface.createFromAsset(context.getAssets(),
				"EncodeSansNarrow-100-Thin.ttf");
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder viewHolder;
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.deal_offer_adapter, null);
			viewHolder = new ViewHolder();
			viewHolder.textView_OfferName = (TextView) convertView
					.findViewById(R.id.textView_OfferName);
			viewHolder.textView_OfferName.setTypeface(Reguler);
			viewHolder.textView_Amount = (TextView) convertView
					.findViewById(R.id.textView_Amount);
			viewHolder.textView_Amount.setTypeface(Reguler);
			viewHolder.textView_OfferAmount = (TextView) convertView
					.findViewById(R.id.textView_OfferAmount);
			viewHolder.textView_OfferAmount.setTypeface(Reguler);
			viewHolder.textView_Sold = (TextView) convertView
					.findViewById(R.id.textView_Sold);
			viewHolder.textView_Sold.setTypeface(Reguler);
			viewHolder.textView_Discount = (TextView) convertView
					.findViewById(R.id.textView_Discount);
			viewHolder.textView_Discount.setTypeface(Reguler);
			convertView.setTag(viewHolder);

		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		WeakHashMap<String, String> map = new WeakHashMap<String, String>();
		map = data.get(position);

		viewHolder.textView_OfferName.setText(map
				.get(DealOfferActivity.OFFER_NAME));
		viewHolder.textView_Amount.setText("Rs. "
				+ map.get(DealOfferActivity.ACTUAL_PRICE));
		viewHolder.textView_Amount.setPaintFlags(viewHolder.textView_Amount
				.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
		viewHolder.textView_OfferAmount.setText("Rs. "
				+ map.get(DealOfferActivity.OFFER_PRICE));
		viewHolder.textView_Sold.setText(map.get(DealOfferActivity.STOCK_SOLD)
				+ " Bought");
		viewHolder.textView_Discount.setText(map
				.get(DealOfferActivity.DISCOUNT) + "% Discount");

		return convertView;
	}

	private class ViewHolder {
		TextView textView_OfferName, textView_Amount, textView_OfferAmount,
				textView_Sold, textView_Discount;
	}
}
