//
//  GiftCustomCell.h
//  Pay1
//
//  Created by Annapurna on 16/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#import "UIImageView+WebCache.h"
#import "UIImage+Utility.h"

@class GiftResponse;
@class GiftCategoryList;
@class GiftDetailList;
@class GiftCategoryList;
@interface GiftCategoryCustomCell : UITableViewCell<UIScrollViewDelegate>{
    
    UIImageView *giftImgView;
    UILabel *offerTitleLabel;
    UILabel *productLabel;
    UILabel *shopName;
    UILabel *addressLabel;
    UILabel *pointLabel;
    IBOutlet UILabel *titleLabel;
    
    IBOutlet UIScrollView *giftCellScrollView;
    
    UIButton *pointBtn;
    UIButton *addressBtn;
    
    
    NSArray *offerIDArray;
    
    NSMutableArray *offerIDArrayList;
    
    NSMutableArray *imageURLArray;
    NSMutableArray *shopNameArray;
    NSMutableArray *areaArray;
    NSMutableArray *offerArray;
    NSMutableArray *priceArray;
    NSMutableArray *offerPriceArray;
    NSMutableArray *offerVoucherArray;


    
    IBOutlet UIView *seeAllButton;
    
    IBOutlet UIView *sepView;
    
    NSMutableArray *dealArray;
    NSMutableArray *offerIDList;
    
    IBOutlet UIView *firstSepView;
    
    IBOutlet UIView *secondSepView;
    
    UILabel *offerPriceLabel;

    
}

@property(nonatomic, copy) void (^onImageViewClick)(NSString* dealID,NSString *offerID,NSString *shopName);

@property(nonatomic, copy) void (^onSeeAllButtonClick)(NSString *offerTitle,NSString *offerID,NSString *fromEVoucher,NSInteger seeAllGiftID);


//-(void)loadGiftDataOnTableView:(GiftList *)giftList objGiftDetailList:(GiftDetailList *)giftDetailList;

//-(void)loadGiftDataOnTableView:(GiftCategoryList *)giftList;

-(void)loadGiftDataOnTableView:(GiftCategoryList *)giftList rowIndex:(NSInteger)rowIndex;

//-(void)loadGiftDataOnTableView:(GiftList *)giftList indexRow:(NSInteger)indexRow;


@end
