<?php $this->load->view('common/doctype_html');  ?>
<?php $this->load->view('common/header');
$userData = getUserDetails();


?>
<div id="transaction_form">
	
</div>
<div class="row" style="margin-top: 60px;">
	
		
		<div class="col-lg-8 col-lg-offset-2">
        <div class="col-md-12">
            
            
            <div class="tabbable">
                <ul class="nav nav-pills nav-stacked col-md-3">
                    <li class="active"><a href="#map_canvas" data-toggle="tab">Locate Pay1 Store</a></li>
                    <li><a href="#onlinepayment" data-toggle="tab">Online Payment</a></li>
                    <li><a href="#redeemvoucher" data-toggle="tab">Redeem Voucher</a></li>
                    
                </ul>
                
                <div class="tab-content col-md-9">
                    <div class="tab-pane active" id="map_canvas" style="height:350px; width:870px;">
						
					</div>
					
                    <div class="tab-pane" id="onlinepayment">
                                                <div>
                                                    <span class="proicon-sup proicmbT"></span> <strong>Top Up Wallet</strong>
                                                </div>
						 <div>
                                                    <form class="form-horizontal mT20" role="form">
                                                        <div class="form-group">
                                                            <div class="col-sm-7">
                                                                
                                                                <input type="text" name="amount" placeholder="Enter Amount" id="amount" class="form-control mT10">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-sm-8">
                                                                <input type="button" value="Make Payment" id="Wallettopup" class="btn btn-success payment_btn">
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
					</div>
                    <div class="tab-pane" id="redeemvoucher" >
                                    
                                            
                                                <div class="">
                                                    <span class="prohead"></span> <strong>Reedem Voucher</strong>
                                                </div>
                                                <div>
                                                    <span style="color:red" id="couponcode"></span>
                                                    <form class="form-horizontal mT20" role="form">
                                                        <div class="form-group">
                                                            <div class="col-sm-7">
                                                                <input type="text" id="coupon_code" placeholder="Enter Coupon Code" name="coupon_code" class="form-control mT10">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-sm-8">
                                                                <input type="button" value="Reedem" id="redeemcode" class="btn btn-success payment_btn">
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            
                                        
						
					</div>
                    <div class="tab-pane" id="giftcoin">
						
					</div>
                    <div class="tab-pane" id="e">
					</div>
                </div>
                
            </div>
			<div class="loader" style="display:none;">
						<center>
							<img class="loading-image" src="/images/bx_loader.gif" alt="loading..">
						</center>
					</div>
              
        </div>


    </div>
    
    </div>
    


<?php $this->load->view('common/footer'); ?>

<script>

$("#Wallettopup").click(function(){
	b2c.core.Wallettopup();
});

$("#redeemcode").click(function(){
	b2c.core.redeemvoucher();
});
</script>
<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
        <script>
       $(function() {
		     var map;
             var elevator;
             var marker;
             var infowindow = new google.maps.InfoWindow();
			 var lat = localStorage.getItem('latitude');
			 var lng = localStorage.getItem('longitude');
			 if(lat==null && lng == null ){
		     getLocation();
		     } else {
				 var lat = localStorage.getItem('latitude');
			     var lng = localStorage.getItem('longitude');
			 }
             var myOptions = {
                    zoom: 14,
                    center: new google.maps.LatLng(lat,lng),
                    mapTypeControl: false,
                    navigationControlOptions: {style: google.maps.NavigationControlStyle.SMALL},
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
               };
        map = new google.maps.Map($('#map_canvas')[0], myOptions);
	    var serviceurl = "https://panel.pay1.in/apis/receiveWeb/mindsarray/mindsarray/json?method=getNearByRetailer&lat="+lat+"&lng="+lng;
        $.ajax({
            url: serviceurl,
            type: "POST",
            dataType: "jsonp",
			jsonp: 'root',
            success: function(data) {
                $.each(data,function(key,value){
					$.each(value,function(k,v){
						 var latlng = new google.maps.LatLng(v.t.latitude,v.t.longitude);
						marker = new google.maps.Marker({
                            position: latlng,
                            map: map,
                            title: v.t.address

                        });
                        google.maps.event.addListener(marker, 'click', function() {
                            infowindow.setContent(v.t.shopname + "<br/>" +v.t.area_name + "<br/>" +v.t.city_name);
                            infowindow.open(map, this);
                        });
					});
				});
            },
            beforeSend: function() {
                $('.loader').show();
            },
            complete: function() {
                $('.loader').hide();
            },
            error: function(xhr, error) {
                console.log(xhr);
                console.log(error);
            }
        });
           	
                });
				</script>
				
        