<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "home";
$route['404_override'] = '';
//$route['categories/(:any)']="categories/index/$1";
//$route['categories/(:any)/(:num)']="giftcategory/showCategory/$1/$2";
$route['home/index/(:any)']="home/index/$1";    
$route['gift-store'] = "giftstore/index";
$route['gift-store/giftdetails/(:any)/success']="giftstore/success";
$route['gift-store/(:num)']="giftstore/index/$1";
$route['categories']="giftcategory/index";
$route['categories-(:any)-cid-(:num)'] = "giftstore/gifttypes/$2";
//$route['gifts-(:any)-(:num)_success']="giftstore/success";
$route['gifts-(:any)-(:any)'] = "giftstore/giftdetails/$2";
//$route['categories/(:num)']="giftcategory/index/$1";
//$route['categories/categorytypes/(:num)']="giftcategory/categorytypes/$1";

$route['terms-and-conditions'] = "terms/index";
$route['gifts/(:num)']="gifts/index/$1";
$route['privacy-policy']="footer/privacy_policy";
$route['about-us']="footer/about_us";
$route['sitemap']="footer/sitemap";
$route['contact-us'] = "contact/index";
$route['about-me'] = "profile/index";
$route['my-gifts']="giftstore/mygifts";
$route['my-likes']="giftstore/mylikes";
$route['my-gifts-(:any)-(:any)'] = "giftstore/mygiftdetails/$2";
$route['near-you/(:any)']="giftstore/near_you/$1";
$route['(:any)-cid-(:num)']="giftcategory/categorytypes/$2";
$route['careers']="career/index";

//$route['recharge-(:any)-(:any)-(:num)-(:any)'] = "/home/index/";

// Test using http://web.pay1.in/recharge-vodafone-mobile-online/pid-3205

$route['recharge-(:any)-(:any)-online']="home/index/$1";

/* End of file routes.php */
/* Location: ./application/config/routes.php */
