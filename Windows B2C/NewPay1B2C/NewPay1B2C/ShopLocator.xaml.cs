﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

using System.Windows.Media;

using System.Collections.ObjectModel;

using System.Windows.Input;
using System.Windows.Media.Imaging;
using NewPay1B2C.UserControls;
using System.Device.Location;

namespace NewPay1B2C
    {
   
        public partial class ShopLocator : PhoneApplicationPage
    {
        string firstLat, firstLong, firstAddress;
        ProgressBarManager ProgressBarManager;
        List<string> shopList;
        public ShopLocator()
        {
            InitializeComponent();
			
            Constant.laodNavigationBar(TitlePanel, LayoutRoot, canvas,"Cash Topup");
            GoogleAnalytics.EasyTracker.GetTracker().SendView("ShopLocator");
            ProgressIndicator prog = new ProgressIndicator();
            ProgressBarManager = new ProgressBarManager();
            SystemTray.SetProgressIndicator(this, prog);
            ProgressBarManager.Hook(prog);
            
            shopList = new List<string>();
           
            getwalletTask(HomePage.latti,HomePage.longi);

        }

        private async void getwalletTask(string lattitude, string longitude1)
        {
            ProgressBarManager.Show(27, "Please wait...", false, 10);
            Task<string> response = Constant.postParamsAndgetResponse(Constant.B2B_URL + "method=getNearByRetailer&lat="
                                        + lattitude + "&lng=" + longitude1);
           

            string result = await response;
            if (string.IsNullOrEmpty(result))
            {
                NavigationService.Navigate(new Uri("/NoConnection.xaml?", UriKind.Relative));
            }
            else
            {
                string output = result.Remove(result.Length - 1, 1);
                string output1 = output.Remove(output.Length - 1, 1);
                string output2 = output1.Remove(0, 1);
                string output3 = output2.Remove(0, 1);
                string output4 = output3.Remove(output3.Length - 1, 1);
                var dict = (JArray)JsonConvert.DeserializeObject(output4);
                bool isFirst = true;
                foreach (var shops in dict)
                {
                    var shopsJobject = shops;

                    var shopsDetails = shopsJobject["t"];
                    shopList.Add(shopsDetails.ToString());
                    string id = shopsDetails["id"].ToString();
                    string mobile = shopsDetails["mobile"].ToString();
                    string shopname = shopsDetails["shopname"].ToString();
                    string sale = shopsDetails["sale"].ToString();

                    string latitude = shopsDetails["latitude"].ToString();
                    string longitude = shopsDetails["longitude"].ToString();

                    string pin = shopsDetails["pin"].ToString();
                    string area_name = shopsDetails["area_name"].ToString();
                    string city_name = shopsDetails["city_name"].ToString();
                    string state_name = shopsDetails["state_name"].ToString();
                    string distance = shopsDetails["D"].ToString();
                    string user_id = shopsDetails["user_id"].ToString();

                    ShopUserControl shopControl = new ShopUserControl();
                    shopControl.textShopAddress.Text = area_name;
                    double dis = Convert.ToDouble(distance);
                    if (dis < 1)
                    {
                        dis = dis * 1000;
                        distance = dis.ToString("#.##") + " M";
                    }
                    else
                    {
                        distance = dis.ToString("#.##") + " KM";
                    }

                    shopControl.textShopDistance.Text = distance;
                    shopControl.textShopname.Text = shopname;

                    shopListBox.Items.Add(shopControl);
                    if (isFirst)
                    {
                        firstLat = latitude;
                        firstLong = longitude;
                        firstAddress = shopname + "\n " + area_name + "\n " + city_name + "\n" + mobile;
                        isFirst = false;
                    }
                   // loadAllShopsOnMap(latitude, longitude, shopname + "\n " + area_name + "\n " + city_name + "\n " + mobile);
                }

               // loadAllShopsOnMap(firstLat, firstLong, firstAddress);
            }
            ProgressBarManager.Hide(27);
        }

     /*   void pushPin_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            var push = sender as Pushpin;

            Border brodr = (Border)push.Content;
            if (brodr.Visibility == Visibility.Visible)
            {
                brodr.Visibility = System.Windows.Visibility.Collapsed;
            }
            else
            {
                brodr.Visibility = System.Windows.Visibility.Visible;
            }

            //stop the event from going to the parent map control
            e.Handled = true;
        }
*/

        

        private void buttonImage_Click_Back(object sender, RoutedEventArgs e)
        {
            NavigationService.GoBack(); 
        }

        private void shopListBoxChanged(object sender, SelectionChangedEventArgs e)
        {
            int index = shopListBox.SelectedIndex;
            string ads = shopList[index];
            PhoneApplicationService.Current.State[Constant.SHOP_DATA] = shopList[index];
            NavigationService.Navigate(new Uri("/ShopInfo.xaml?", UriKind.RelativeOrAbsolute));
           
            
        }


    }
}