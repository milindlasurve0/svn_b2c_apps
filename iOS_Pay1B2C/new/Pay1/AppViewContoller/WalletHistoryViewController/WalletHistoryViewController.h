//
//  WalletHistoryViewController.h
//  Pay1
//
//  Created by webninjaz on 19/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "BaseViewController.h"

@class WalletHistoryService;
@class WalletHistoryCriteria;
@class WalletHistoryResponse;
@class TransactionHistoryView;
@class WalletHistoryCustomCell;

@interface WalletHistoryViewController : BaseViewController<UITableViewDelegate,UITableViewDataSource>{
    
    IBOutlet UITableView *walletHistoryTableView;
    IBOutlet UILabel *walletHistoryLabel;
    IBOutlet UIImageView *walletHistoryImageView;
    IBOutlet WalletHistoryCustomCell *walletHistoryCustomCell;
   
    
    NSMutableArray *walletContentArray;
    
    UIView *bottomView;
    
    int walletValue;
    UIButton *bottomButton;
}


@end
