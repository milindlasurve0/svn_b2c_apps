﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace NewPay1B2C
    {
    public partial class PurchaseDetails : PhoneApplicationPage
        {
        public PurchaseDetails ()
            {
            InitializeComponent ();
          
            GoogleAnalytics.EasyTracker.GetTracker ().SendView ("PurchaseDetails");
            string PurchaseData = PhoneApplicationService.Current.State["PurchaseData"].ToString ();
            var dict = (JObject)JsonConvert.DeserializeObject (PurchaseData);
            string status = dict["status"].ToString ();
            if (status.Equals ("2"))
                {
                textStatus.Text = "Success";
                textStatus.Foreground = new SolidColorBrush (Colors.Green);
                }
            else if (status.Equals ("1"))
                {

                textStatus.Text = "In process";
                textStatus.Foreground = new SolidColorBrush (Colors.Blue);
                }
            else
                {
                textStatus.Text = "Failure";
                textStatus.Foreground = new SolidColorBrush (Colors.Red);
                }
            string service_id = dict["service_id"].ToString ();
            if (service_id.Equals ("4"))
                {
                textTaxInfo.Visibility = Visibility.Visible;
                int transaction_amount = Convert.ToInt16 (dict["transaction_amount"].ToString ());
                int base_amount = Convert.ToInt16 (dict["base_amount"].ToString ());
                textTaxInfo.Text = "( " + transaction_amount + " = " + base_amount + " + " + (transaction_amount - base_amount) + " service charge inclusive of all taxes)";
                }
            else
                {
                textTaxInfo.Visibility = Visibility.Collapsed;
                }

            textAmout.Text = "Rs. " + dict["transaction_amount"].ToString ();

            if (string.IsNullOrEmpty (dict["operator_name"].ToString ()))
                {
                textOperator.Text = "";
                }
            else
                {
                textOperator.Text = "( " + dict["operator_name"].ToString () + " )";
                }
            textContact.Text = Constant.getContactName (dict["number"].ToString ());
            textNumber.Text = dict["number"].ToString ();
            textTransactionId.Text = dict["transaction_id"].ToString ();
            textPaymentType.Text = dict["transaction_mode"].ToString ();
            textDate.Text = dict["trans_datetime"].ToString ();
            textPaymentMode.Text = dict["payment_mode"].ToString ();


            }

        private void buttonImage_Click_Back (object sender, RoutedEventArgs e)
            {

            NavigationService.GoBack ();
            }

        }
    }