﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Pay1B2C.ViewModel;
using Pay1B2C.Model;
using Pay1B2C.UserControls;
using System.Windows.Media.Imaging;

namespace Pay1B2C.Views
    {
    public partial class AllOffersByType: PhoneApplicationPage
        {
        ProgressBarManager ProgressBarManager;
        public AllOffersByType()
            {
            InitializeComponent();
            ProgressIndicator prog = new ProgressIndicator();
            ProgressBarManager = new ProgressBarManager();
            SystemTray.SetProgressIndicator(this, prog);
            ProgressBarManager.Hook(prog);
          
            }



       


        protected async override void OnNavigatedTo(NavigationEventArgs e)
            {
            if(NavigationContext.QueryString.ContainsKey("catName"))
                {
                string catName = NavigationContext.QueryString["catName"];
                if(string.IsNullOrEmpty(catName))
                    {
                    }
                else
                    {
                    textTitle.Text=catName;
                    if(NavigationContext.QueryString.ContainsKey("catId"))
                        {
                        string catId = NavigationContext.QueryString["catId"];
                        if(string.IsNullOrEmpty(catId))
                            {
                            }
                        else
                            {
                            if(NavigationContext.QueryString.ContainsKey("catType"))
                                {
                                string catType = NavigationContext.QueryString["catType"];
                                if(catType=="0")
                                    {
                                    await Task.Run(() => loadFreeBies(catId));
                                    }
                                else
                                    {
                                    await Task.Run(() => loadFreeBiesCategories(catId));
                                    }
                                }
                            else
                                {
                               
                                }
                            }
                        }
                    }
                }
            }

        private async void loadFreeBies(string catId)
            {
           // + "GetAllOfferCategory/?type="


            ProgressBarManager.Show(20, "Please wait...", false, 10);

            var values = new List<KeyValuePair<string, string>>();
            values.Add(new KeyValuePair<string, string>("actiontype", "GetAllOfferTypes"));
            values.Add(new KeyValuePair<string, string>("type", catId));
           
            values.Add(new KeyValuePair<string, string>("next", "-1"));
            values.Add(new KeyValuePair<string, string>("api_version", Constant.API_VERSION));
            values.Add(new KeyValuePair<string, string>("api", "true"));
            FormUrlEncodedContent values1 = new FormUrlEncodedContent(values);
            Task<string> response = Constant.postParamsAndgetResponse(Constant.B2C_URL, values1);


            string result = await response;
            if(string.IsNullOrEmpty(result))
                {
                NavigationService.Navigate(new Uri("/Views/NoConnection.xaml?", UriKind.Relative));
                }
            else
                {
                var dict = (JObject)JsonConvert.DeserializeObject(result);
                string status = dict["status"].ToString();

                if(status.Equals("success"))
                    {
                    var desc=dict["description"]["offer_details"];
                    foreach(var obj in desc)
                        {
                        // String description =[0];
                        this.Dispatcher.BeginInvoke(() =>
                        {
                        insertDataForGifts(obj["details"].ToString());


                        });
                        }

                    var offer_likes=dict["description"]["offer_likes"];
                    string[] offer_likes_str=offer_likes.ToString().Split(',');
                            GiftDataHelper gdh=new GiftDataHelper();
                            foreach(string ids in offer_likes_str)
                                {
                                gdh.updateGiftForLikeByOfferId(Convert.ToInt16(ids.ToString()),1);
                                }

            
                    }
                }

            ProgressBarManager.Hide(20);
          
										
            }



        private async void loadFreeBiesCategories(string catId)
            {
            // + "GetAllOfferCategory/?type="


            ProgressBarManager.Show(20, "Please wait...", false, 10);

            var values = new List<KeyValuePair<string, string>>();
            values.Add(new KeyValuePair<string, string>("actiontype", "GetAllOfferCategory"));
            values.Add(new KeyValuePair<string, string>("type", catId));

            values.Add(new KeyValuePair<string, string>("next", "-1"));
            values.Add(new KeyValuePair<string, string>("api_version", Constant.API_VERSION));
            values.Add(new KeyValuePair<string, string>("api", "true"));
            FormUrlEncodedContent values1 = new FormUrlEncodedContent(values);
            Task<string> response = Constant.postParamsAndgetResponse(Constant.B2C_URL, values1);


            string result = await response;
            if(string.IsNullOrEmpty(result))
                {
                NavigationService.Navigate(new Uri("/Views/NoConnection.xaml?", UriKind.Relative));
                }
            else
                {
                var dict = (JObject)JsonConvert.DeserializeObject(result);
                string status = dict["status"].ToString();

                if(status.Equals("success"))
                    {
                    var desc=dict["description"];
                    foreach(var obj in desc)
                        {
                       
                        this.Dispatcher.BeginInvoke(() =>
                        {
                        insertDataForGifts(obj["details"].ToString());


                        });
                        }

               

                    }
                }

            ProgressBarManager.Hide(20);


            }



        private void insertDataForGifts(String Alldeals)
            {
            try
                {
                GiftDataHelper gdh=new GiftDataHelper();
                List<GiftsTable> gifts=    gdh.ReadGiftByOfferIds(Alldeals);
                if(gifts.Count!=0)
                    {
                    emptyViewPanel.Visibility=Visibility.Collapsed;
                    allOfferListBox.Visibility=Visibility.Visible;
                    textMessage.Text="";
                    foreach(GiftsTable gift in gifts)
                        {
                        GiftListLayout giftLayout=new GiftListLayout();
                        giftLayout.offerImage.Source=new BitmapImage(new Uri(gift.freebieURL));
                        giftLayout.offerImageLogo.Source=new BitmapImage(new Uri(gift.freebieLogoURL));
                        giftLayout.offerDealname.Text=gift.freebieDealName;
                        giftLayout.offerText.Text=gift.freebieShortDesc;
                        giftLayout.textLocation.Text=gift.freebieArea+": ";
                        giftLayout.expiredGiftGrid.Visibility=Visibility.Collapsed;
                        giftLayout.textCoin.Text=gift.freebieMinAmount.ToString();
                        LocationTable locationTable=   gdh.getNearestLocationFromLocationTable(Constant.LoadPersistent<double>(Constant.SETTINGS_USER_LATTITUDE_DOUBLE), Constant.LoadPersistent<double>(Constant.SETTINGS_USER_LONGITUDE_DOUBLE), gift.freebieOfferID);


                        if(gift.freebieLike==0)
                            {
                            giftLayout.imgLike.Source=new BitmapImage(new Uri("/Images/ic_unlike.png", UriKind.RelativeOrAbsolute));
                            }
                        else
                            {
                            giftLayout.imgLike.Source=new BitmapImage(new Uri("/Images/ic_like.png", UriKind.RelativeOrAbsolute));
                            }
                        //  giftLayout.likeRadioButton.IsChecked=gift.freebieLike!=0;

                        giftLayout.imgLike.MouseEnter+=(a, b) =>
                        {
                        if(gift.freebieLike==0)
                                {
                                giftLayout.imgLike.Source=new BitmapImage(new Uri("/Images/ic_like.png", UriKind.RelativeOrAbsolute));
                                updateLikeDealById(gift.freebieOfferID, gdh, 1);
                                }
                            else
                                {
                                giftLayout.imgLike.Source=new BitmapImage(new Uri("/Images/ic_unlike.png", UriKind.RelativeOrAbsolute));
                                updateLikeDealById(gift.freebieOfferID, gdh, 0);
                                }


                        };



                        giftLayout.mainPanel.Tap+=(a, b) =>
                        {
                            (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri("/Views/GiftDetailsPage.xaml?offerId="+gift.freebieOfferID+"&fromMyDeal=0", UriKind.RelativeOrAbsolute));
                        };
                        
                        if(locationTable!=null)
                            {
                            giftLayout.textLocation.Text=locationTable.locationArea;
                            }
                        else
                            {
                            giftLayout.textLocation.Text=gift.freebieArea;
                            }

                        if(gift.by_voucher.Equals(0))
                            {
                            giftLayout.rupeePanel.Visibility=Visibility.Collapsed;
                         
                            }
                        else
                            {
                           
                            giftLayout.textRupee.Text=gift.offer_price.ToString();
                            giftLayout.rupeePanel.Visibility=Visibility.Visible;

                            }
                        giftLayout.locationPanel.Visibility=Visibility.Collapsed;
                        allOfferListBox.Items.Add(giftLayout);
                        }
                    }
                else
                    {
                    allOfferListBox.Visibility=Visibility.Collapsed;
                    emptyViewPanel.Visibility=Visibility.Visible;
                    textMessage.Text="PLEASE MAKE ANY TRANSACTION TO GET STARTED.";
                    }
                }
            catch(Exception exc)
                {
                }

            }



        private async void updateLikeDealById(int offerId, GiftDataHelper gdh, int setLike)
            {


            var values = new List<KeyValuePair<string, string>>();
            values.Add(new KeyValuePair<string, string>("actiontype", "MyLikes"));
            values.Add(new KeyValuePair<string, string>("id", ""+offerId));
            values.Add(new KeyValuePair<string, string>("api_version", Constant.API_VERSION));

            values.Add(new KeyValuePair<string, string>("api", "true"));
            FormUrlEncodedContent values1 = new FormUrlEncodedContent(values);
            Task<string> response = Constant.postParamsAndgetResponse(Constant.B2C_URL, values1);


            string result = await response;
            if(string.IsNullOrEmpty(result))
                {
                NavigationService.Navigate(new Uri("/Views/NoConnection.xaml?", UriKind.Relative));
                }
            else
                {
                var dict = (JObject)JsonConvert.DeserializeObject(result);
                string status = dict["status"].ToString();

                if(status.Equals("success"))
                    {
                    gdh.updateGiftForLikeByOfferId(offerId, setLike);


                    }
                }


            }

        private void ButtonImage_Click(object sender, RoutedEventArgs e)
            {
            NavigationService.GoBack();
            }
        }
    }