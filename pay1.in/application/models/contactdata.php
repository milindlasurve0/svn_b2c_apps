<?php

class Contactdata extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    function insert_contact($data=NULL)
    {   
        $data = array(
            'name' => $data['name'] ,
            'phone' => $data['phone'] ,
            'email' => $data['email'],
            'issue' => $data['issue'],
            'feedback' => $data['feedback'],
        );
        $this->db->insert('contact_web', $data); 
    }
}