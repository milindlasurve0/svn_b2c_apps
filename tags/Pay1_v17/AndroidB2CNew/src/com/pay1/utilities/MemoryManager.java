package com.pay1.utilities;

import java.io.File;
import java.lang.reflect.Method;

import android.app.ActivityManager;
import android.app.ActivityManager.MemoryInfo;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.os.StatFs;

public class MemoryManager {

	public static void clearApplicationData(Context context) {
		File cache = context.getCacheDir();
		Long total = cache.getTotalSpace();
		Long used = cache.getUsableSpace();
		System.out.println("TAG " + "Total " + total + " Used  " + used
				+ " Avail " + (total - used));

		// if (used >= 1000000) {
		System.out.println("TAG " + "Clearing....");
		File appDir = new File(cache.getParent());
		if (appDir.exists()) {
			String[] children = appDir.list();
			for (String s : children) {
				if (!s.equals("lib") && !s.equals("shared_prefs")
						&& !s.equals("files") && !s.equals("databases")) {
					deleteDir(new File(appDir, s));
					System.out.println("TAG " + "File /data/data/APP_PACKAGE/"
							+ s + " DELETED *******************");
				}
			}
		}
		System.out.println("TAG " + "Done");
		File cache1 = context.getCacheDir();
		Long total1 = cache.getTotalSpace();
		Long used1 = cache.getUsableSpace();
		System.out.println("TAG " + "Total " + total1 + " Used  " + used1
				+ " Avail " + (total1 - used1));
		// }
	}

	public static boolean deleteDir(File dir) {
		if (dir != null && dir.isDirectory()) {
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++) {
				boolean success = deleteDir(new File(dir, children[i]));
				if (!success) {
					return false;
				}
			}
		}

		return dir.delete();
	}

	public static long getFreeMemory() {
		File path = Environment.getDataDirectory();
		StatFs stat = new StatFs(path.getPath());
		long blockSize = stat.getBlockSize();
		long availableBlocks = stat.getAvailableBlocks();
		return availableBlocks * blockSize;
	}

	public static long getTotalMemory() {
		File path = Environment.getDataDirectory();
		StatFs stat = new StatFs(path.getPath());
		long blockSize = stat.getBlockSize();
		long availableBlocks = stat.getBlockCount();
		return availableBlocks * blockSize;
	}

	public static boolean isMemoryLow(Context context) {

		ActivityManager actManager = (ActivityManager) context
				.getSystemService(Context.ACTIVITY_SERVICE);
		MemoryInfo memInfo = new ActivityManager.MemoryInfo();

		// Retrieving available memory
		actManager.getMemoryInfo(memInfo);

		// Avail memory is the memory left on your system.
		// Dividing by 1048576L will give you the
		// amount of megabytes.
		long availableMegs = memInfo.availMem / 1048576L;

		// memory is alerted if device has less than 10 megabytes
		if (availableMegs < 50)
			return true;
		else
			return false;
	}

	public static void clearCache(Context context) {
		try {
			PackageManager pm = context.getPackageManager();
			// Get all methods on the PackageManager
			Method[] methods = pm.getClass().getDeclaredMethods();
			for (Method m : methods) {
				if (m.getName().equals("freeStorage")) {
					// Found the method I want to use
					try {
						// Request for 8GB of free space
						long desiredFreeStorage = 20 * 1024 * 1024 * 1024;
						m.invoke(pm, desiredFreeStorage, null);
						System.out.println("Mem " + "cleared");
					} catch (Exception e) {
						// Method invocation failed. Could be a permission
						// problem
					}
					break;
				}
			}
		} catch (Exception e) {
		}
	}

}
