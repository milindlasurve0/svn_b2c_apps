package com.pay1;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.WeakHashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.SQLException;
import android.graphics.Typeface;
import android.location.LocationManager;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.MapBuilder;
import com.larvalabs.svgandroid.SVGParser;
import com.pay1.constants.Constants;
import com.pay1.customviews.MyProgressDialog;
import com.pay1.databasehandler.ReClaimGift;
import com.pay1.databasehandler.ReClaimGiftDataSource;
import com.pay1.imagehandler.ImageLoader;
import com.pay1.requesthandler.RequestClass;
import com.pay1.utilities.Utility;
import com.squareup.picasso.Picasso;

public class FreeBiesPagerFragment extends Fragment {

	public static final String DEAL_ID = "id";
	public static final String LATITUDE = "latitude";
	public static final String LONGITUDE = "longitude";
	public static final String ADDRESS = "address";
	public static final String CITY = "city";
	public static final String STATE = "state";
	public static int flag = 0;

	String trans_id, offer_id, offer_name, short_desc, deal_id, deal_name,
			min_amount, img_url;
	ArrayList<WeakHashMap<String, String>> data, data_location;
	private ImageLoader imageLoader;
	int count, position, recharge_amount;
	boolean is_FromRecharge;
	private WebView webView_Details;
	private ImageView imageView_Image;
	private TextView textView_DealName, textView_OfferName,
			textView_OfferDescription, textView_Count, textView_Distance,
			textView_Unit, textView_Address, textView_Info, textView_Timer,
			textView_LocationCount;
	private RelativeLayout relativeLayout_Locate;
	private Button button_Confirm;
	private ProgressBar progressBar1;
	private WeakHashMap<String, String> map;
	private ReClaimGiftDataSource reClaimGiftDataSource;

	public static FreeBiesPagerFragment newInstance(int recharge_amount,
			String trans_id, int count, int position, boolean is_FromRecharge,
			ArrayList<WeakHashMap<String, String>> data,
			ArrayList<WeakHashMap<String, String>> data_location) {
		FreeBiesPagerFragment fragment = new FreeBiesPagerFragment();
		fragment.recharge_amount = recharge_amount;
		fragment.trans_id = trans_id;
		fragment.count = count;
		fragment.position = position;
		fragment.is_FromRecharge = is_FromRecharge;
		fragment.data = data;
		fragment.data_location = data_location;
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if ((savedInstanceState != null)
				&& savedInstanceState.containsKey("AMOUNT")
				&& savedInstanceState.containsKey("TRANS_ID")
				&& savedInstanceState.containsKey("COUNT")
				&& savedInstanceState.containsKey("POSITION")
				&& savedInstanceState.containsKey("FLAG")
				&& savedInstanceState.containsKey("LOCATIONS")
				&& savedInstanceState.containsKey("DATA")) {
			recharge_amount = savedInstanceState.getInt("AMOUNT");
			trans_id = savedInstanceState.getString("TRANS_ID");
			count = savedInstanceState.getInt("COUNT");
			position = savedInstanceState.getInt("POSITION");
			is_FromRecharge = savedInstanceState.getBoolean("FLAG", false);

			try {
				data = new ArrayList<WeakHashMap<String, String>>(
						(ArrayList<WeakHashMap<String, String>>) savedInstanceState
								.getSerializable("DATA"));
				data_location = new ArrayList<WeakHashMap<String, String>>(
						(ArrayList<WeakHashMap<String, String>>) savedInstanceState
								.getSerializable("LOCATIONS"));
			} catch (Exception e) {
				data = new ArrayList<WeakHashMap<String, String>>();
				data_location = new ArrayList<WeakHashMap<String, String>>();
			}
		}
	}

	@SuppressLint("NewApi")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View viewLayout = inflater.inflate(R.layout.freebies_pager_adapter,
				container, false);
		try {
			reClaimGiftDataSource = new ReClaimGiftDataSource(getActivity());
			Typeface Reguler = Typeface.createFromAsset(getActivity()
					.getAssets(), "Roboto-Regular.ttf");

			map = new WeakHashMap<String, String>();
			map = data.get(position);
			offer_id = map.get(FreeBiesListFragment.OFFER_ID);
			offer_name = map.get(FreeBiesListFragment.OFFER_NAME);
			short_desc = map.get(FreeBiesListFragment.SHORT_DESC);
			deal_id = map.get(FreeBiesListFragment.DEAL_ID);
			deal_name = map.get(FreeBiesListFragment.DEAL_NAME);
			min_amount = map.get(FreeBiesListFragment.MIN_AMOUNT);
			img_url = map.get(FreeBiesListFragment.URL);

			progressBar1 = (ProgressBar) viewLayout
					.findViewById(R.id.progressBar1);
			progressBar1.setVisibility(View.GONE);

			webView_Details = (WebView) viewLayout
					.findViewById(R.id.webView_Details);
			webView_Details.setVisibility(View.GONE);
			flag = 0;
			webView_Details.setLongClickable(false);

			if (flag == 0)
				webView_Details.setVisibility(View.GONE);
			webView_Details.setBackgroundColor(0x00000000);
			if (Build.VERSION.SDK_INT >= 11)
				webView_Details.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);

			imageView_Image = (ImageView) viewLayout
					.findViewById(R.id.imageView_Image);
			// imageLoader = new ImageLoader(getActivity());
			// imageLoader.getDealImage(img_url, imageView_Image, progressBar1);
			try {
				Picasso.with(getActivity()).load(img_url)
						.placeholder(R.drawable.deal_default)
						.into(imageView_Image);
			} catch (Exception e) {
			}

			imageView_Image.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					// WeakHashMap<String, String> map = new WeakHashMap<String,
					// String>();
					// map = data.get(position);
					// Intent intent = new Intent(getActivity(),
					// MyFreeBiesDetailsActivity.class);
					// intent.putExtra("IS_BUY", 0);
					// intent.putExtra("URL", img_url);
					// intent.putExtra("DEAL_ID", deal_id);
					// intent.putExtra("DEAL_NAME", deal_name);
					// intent.putExtra("ACTUAL_PRICE",
					// map.get(MyFreeBiesFragment.ACTUAL_PRICE));
					// intent.putExtra("OFFER_PRICE",
					// map.get(MyFreeBiesFragment.OFFER_PRICE));
					// intent.putExtra("STOCK_SOLD",
					// map.get(MyFreeBiesFragment.STOCK_SOLD));
					// intent.putExtra("DISCOUNT",
					// map.get(MyFreeBiesFragment.DISCOUNT));
					// intent.putExtra("VOUCHER",
					// map.get(MyFreeBiesFragment.VOUCHER_CODE));
					//
					// getActivity().startActivity(intent);
					if (webView_Details.getVisibility() == View.GONE) {
						EasyTracker easyTracker = EasyTracker
								.getInstance(getActivity());
						// easyTracker.sendEvent("ui_action", "button_press",
						// "play_button", opt_value);
						easyTracker.send(MapBuilder.createEvent("Free Gift", // Event
																				// category
																				// (required)
								"Free Gift Desctription", // Event action
															// (required)
								deal_name, // Event label
								null) // Event value
								.build());
						new DealDetailsTask().execute(
								deal_id,
								Utility.getCurrentLatitude(
										getActivity(),
										Constants.SHAREDPREFERENCE_CURRENT_LATITUDE),
								Utility.getCurrentLongitude(
										getActivity(),
										Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE));
					} else {
						webView_Details.setVisibility(View.GONE);
					}
				}
			});

			ImageView imageView_Previous = (ImageView) viewLayout
					.findViewById(R.id.imageView_Previous);
		/*	imageView_Previous.setImageDrawable(SVGParser.getSVGFromResource(
					getActivity().getResources(), R.raw.ic_pre)
					.createPictureDrawable());*/
			ImageView imageView_Next = (ImageView) viewLayout
					.findViewById(R.id.imageView_Next);
			/*imageView_Next.setImageDrawable(SVGParser.getSVGFromResource(
					getActivity().getResources(), R.raw.ic_nex)
					.createPictureDrawable());*/

			if (Build.VERSION.SDK_INT >= 11) {
				imageView_Next.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
				imageView_Previous.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
			}

			textView_DealName = (TextView) viewLayout
					.findViewById(R.id.textView_DealName);
			textView_DealName.setTypeface(Reguler);

			textView_Timer = (TextView) viewLayout
					.findViewById(R.id.textView_Timer);
			textView_Timer.setTypeface(Reguler);
			textView_Timer.setText("");

			String validity = map.get(FreeBiesListFragment.VALIDITY);
			long timestamp = 0;
			Date d_validity = null;
			Date d_current = new Date();
			SimpleDateFormat dateFormat = new SimpleDateFormat(
					"yyyy-MM-dd HH:mm:ss");
			try {

				d_validity = dateFormat.parse(validity);

				timestamp = d_validity.getTime();
			} catch (Exception e) {
				d_validity = new Date();
			}
			timestamp = d_validity.getTime() - System.currentTimeMillis();
			int rem = daysBetween(d_current, d_validity);
			if (rem > 1 && rem < 5)
				textView_Timer.setText(rem + " days left");
			else if (rem == 1)
				textView_Timer.setText(rem + " day left");
			else if (rem == 0) {
				final SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
				new CountDownTimer(timestamp, 1000) {

					@Override
					public void onTick(long millisUntilFinished) {
						// TODO Auto-generated method stub
						textView_Timer.setText(df.format(millisUntilFinished));
					}

					@Override
					public void onFinish() {
						// TODO Auto-generated method stub
						textView_Timer.setText("");
					}
				}.start();
			}

			textView_OfferName = (TextView) viewLayout
					.findViewById(R.id.textView_OfferName);
			textView_OfferName.setTypeface(Reguler);

			textView_LocationCount = (TextView) viewLayout
					.findViewById(R.id.textView_LocationCount);
			textView_LocationCount.setTypeface(Reguler);
			String count_ = map.get(FreeBiesListFragment.LOCATION_COUNT);
			int cnt = 0;
			try {
				cnt = Integer.parseInt(count_);
			} catch (Exception e) {
				cnt = 0;
			}
			if (cnt - 1 < 1)
				textView_LocationCount.setVisibility(View.INVISIBLE);
			else if (cnt - 1 == 1) {
				textView_LocationCount.setVisibility(View.VISIBLE);
				textView_LocationCount.setText("+" + (cnt - 1) + " location");
			} else {
				textView_LocationCount.setVisibility(View.VISIBLE);
				textView_LocationCount.setText("+" + (cnt - 1) + " locations");
			}

			textView_OfferDescription = (TextView) viewLayout
					.findViewById(R.id.textView_OfferDescription);
			textView_OfferDescription.setTypeface(Reguler);

			textView_Count = (TextView) viewLayout
					.findViewById(R.id.textView_Count);
			textView_Count.setTypeface(Reguler);

			textView_Distance = (TextView) viewLayout
					.findViewById(R.id.textView_Distance);
			textView_Distance.setTypeface(Reguler);

			textView_Unit = (TextView) viewLayout
					.findViewById(R.id.textView_Unit);
			textView_Unit.setTypeface(Reguler);

			textView_Address = (TextView) viewLayout
					.findViewById(R.id.textView_Address);
			textView_Address.setTypeface(Reguler);

			textView_Info = (TextView) viewLayout
					.findViewById(R.id.textView_Info);
			textView_Info.setTypeface(Reguler);

			relativeLayout_Locate = (RelativeLayout) viewLayout
					.findViewById(R.id.relativeLayout_Locate);
			relativeLayout_Locate
					.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							boolean gps_enabled = false;
							boolean network_enabled = false;
							LocationManager lm = (LocationManager) getActivity()
									.getSystemService(Context.LOCATION_SERVICE);
							try {
								gps_enabled = lm
										.isProviderEnabled(LocationManager.GPS_PROVIDER);
							} catch (Exception ex) {
							}
							try {
								network_enabled = lm
										.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
							} catch (Exception ex) {
							}

							// don't start listeners if no provider is
							// enabled
							boolean hasGPS = getActivity()
									.getPackageManager()
									.hasSystemFeature(
											PackageManager.FEATURE_LOCATION_GPS);
							WifiManager wifi = (WifiManager) getActivity()
									.getSystemService(Context.WIFI_SERVICE);
							if (!(network_enabled && wifi.isWifiEnabled())
									&& (hasGPS && !gps_enabled)) {

								showTwoButtonDialog(getActivity(),
										"Shop Locator",
										"Turn on your location service on the phone to locate "
												+ textView_DealName.getText()
														.toString().trim()
												+ ".",
										Constants.DIALOG_CLOSE_LOCATION);
							} else if (gps_enabled || network_enabled) {
								boolean show_route = false;

								if (wifi.isWifiEnabled())
									show_route = true;
								else if (network_enabled && gps_enabled)
									show_route = true;
								try {

									ArrayList<WeakHashMap<String, String>> temp = new ArrayList<WeakHashMap<String, String>>();

									for (int j = 0; j < data_location.size(); j++) {
										WeakHashMap<String, String> map1 = new WeakHashMap<String, String>();
										WeakHashMap<String, String> map = new WeakHashMap<String, String>(
												data_location.get(j));
										if (Integer.parseInt(map.get(DEAL_ID)) == Integer
												.parseInt(offer_id)) {
											map1.put(LATITUDE,
													map.get(LATITUDE));
											map1.put(LONGITUDE,
													map.get(LONGITUDE));
											map1.put(ADDRESS, map.get(ADDRESS));
											map1.put(CITY, map.get(CITY));
											map1.put(STATE, map.get(STATE));

											temp.add(map1);
										}
									}
									if (temp.size() > 1) {
										Intent intent = new Intent(
												getActivity(),
												FreeBiesLocationActivity.class);
										intent.putExtra("ROUTE", show_route);
										intent.putExtra("DEAL",
												textView_DealName.getText()
														.toString().trim());
										intent.putExtra("ID", deal_id);
										intent.putExtra("FROM_DETAILS", false);
										intent.putExtra("LOCATIONS",
												data_location);
										getActivity().startActivity(intent);
									} else {
										Intent intent = new Intent(
												getActivity(),
												FreeBiesLocatorActivity.class);
										WeakHashMap<String, String> map = new WeakHashMap<String, String>(
												temp.get(0));
										intent.putExtra("ROUTE", show_route);
										intent.putExtra("DEAL",
												textView_DealName.getText()
														.toString().trim());
										intent.putExtra(
												"LATITUDE",
												map.get(FreeBiesListFragment.LATITUDE));
										intent.putExtra(
												"LONGITUDE",
												map.get(FreeBiesListFragment.LONGITUDE));
										intent.putExtra(
												"ADDRESS",
												map.get(FreeBiesListFragment.ADDRESS));
										intent.putExtra("CITY", map
												.get(FreeBiesListFragment.CITY));
										intent.putExtra(
												"STATE",
												map.get(FreeBiesListFragment.STATE));
										startActivity(intent);
									}
								} catch (Exception e) {
									Intent intent = new Intent(getActivity(),
											FreeBiesLocationActivity.class);
									intent.putExtra("ROUTE", show_route);
									intent.putExtra("DEAL", textView_DealName
											.getText().toString().trim());
									intent.putExtra("ID", deal_id);
									intent.putExtra("FROM_DETAILS", false);
									intent.putExtra("LOCATIONS", data_location);
									getActivity().startActivity(intent);
								}
							} else {
								showTwoButtonDialog(getActivity(),
										"Shop Locator",
										"Turn on your location service on the phone to locate "
												+ textView_DealName.getText()
														.toString().trim()
												+ ".",
										Constants.DIALOG_CLOSE_LOCATION);
							}

						}
					});

			button_Confirm = (Button) viewLayout
					.findViewById(R.id.button_Confirm);
			button_Confirm.setTypeface(Reguler);
			button_Confirm.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (is_FromRecharge) {
						int min = 0;
						try {
							min = Integer.parseInt(min_amount);
						} catch (Exception e) {
							min = 0;
						}
						if (recharge_amount >= min) {
							showTwoButtonDialog(getActivity(),
									"Free Gift Confirmation",
									"Do you want to grab " + offer_name
											+ " as your free gift ?");
						} else {
							// Toast.makeText(
							// getActivity(),
							// "To avail this free gift, recharge for min Rs."
							// + min_amount, Toast.LENGTH_LONG)
							// .show();
							try {
								LayoutInflater inflater = getActivity()
										.getLayoutInflater();

								// Call toast.xml file for toast layout
								View toastRoot = inflater.inflate(
										R.layout.custom_toast, null);

								Toast toast = new Toast(getActivity());
								Typeface Reguler = Typeface.createFromAsset(
										getActivity().getAssets(),
										"Roboto-Regular.ttf");
								TextView textView = (TextView) toastRoot
										.findViewById(R.id.textView_Message);
								textView.setTypeface(Reguler);
								textView.setText("To avail this free gift, recharge for min Rs."
										+ min_amount); // Set layout to toast
								toast.setView(toastRoot);
								toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
								toast.setDuration(Toast.LENGTH_LONG);
								toast.show();
							} catch (Exception e) {
								// System.out.println("ms " + e.getMessage());
							}

						}

					} else if (button_Confirm.getText().toString().trim()
							.equalsIgnoreCase("Grabbed")) {
						try {
							LayoutInflater inflater = getActivity()
									.getLayoutInflater();

							// Call toast.xml file for toast layout
							View toastRoot = inflater.inflate(
									R.layout.custom_toast, null);

							Toast toast = new Toast(getActivity());
							Typeface Reguler = Typeface.createFromAsset(
									getActivity().getAssets(),
									"Roboto-Regular.ttf");
							TextView textView = (TextView) toastRoot
									.findViewById(R.id.textView_Message);
							textView.setTypeface(Reguler);
							textView.setText("You have already grabbed this free gift.");
							toast.setView(toastRoot);
							toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
							toast.setDuration(Toast.LENGTH_LONG);
							toast.show();
						} catch (Exception e) {
							// System.out.println("ms " + e.getMessage());
						}

					} else {
						try {
							Utility.setSelectedGift(getActivity(),
									Constants.SHAREDPREFERENCE_SELECTED_GIFT,
									offer_id);
							Intent intent = new Intent(getActivity(),
									MainActivity.class);
							intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
									| Intent.FLAG_ACTIVITY_NEW_TASK);
							intent.putExtra("FROM_QUICKPAY", true);
							intent.putExtra("IS_GIFT", false);
							getActivity().startActivity(intent);
							getActivity().finish();
						} catch (Exception e) {
							Intent intent = new Intent(getActivity(),
									MainActivity.class);
							intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
									| Intent.FLAG_ACTIVITY_NEW_TASK);
							intent.putExtra("FROM_QUICKPAY", true);
							intent.putExtra("IS_GIFT", false);
							getActivity().startActivity(intent);
							getActivity().finish();
						}
					}
				}
			});

			int min = 0;
			try {
				min = Integer.parseInt(min_amount);
			} catch (Exception e) {
				min = 0;
			}
			if (recharge_amount >= min) {
				textView_Info.setVisibility(View.GONE);
			} else {
				textView_Info
						.setText("Valid for min Rs." + min + "+ recharge.");
				textView_Info.setVisibility(View.VISIBLE);
			}

			textView_OfferName.setText(offer_name);
			textView_OfferDescription.setText(short_desc);
			textView_DealName.setText(deal_name);
			if (count != 0)
				textView_Count.setText((position + 1) + " of " + count);
			else
				textView_Count.setVisibility(View.INVISIBLE);
			// textView_Count.setText("" + (position + 1));
			if (count == 0) {
				imageView_Previous.setVisibility(View.INVISIBLE);
				imageView_Next.setVisibility(View.INVISIBLE);
			} else if (position == 0 && (position == (count - 1))) {
				imageView_Previous.setVisibility(View.INVISIBLE);
				imageView_Next.setVisibility(View.INVISIBLE);
			} else if (position == 0) {
				imageView_Previous.setVisibility(View.INVISIBLE);
				imageView_Next.setVisibility(View.VISIBLE);
			} else if (position == (count - 1)) {
				imageView_Previous.setVisibility(View.VISIBLE);
				imageView_Next.setVisibility(View.INVISIBLE);
			}

			if (is_FromRecharge) {
				button_Confirm.setText("Grab this for free");
			} else if (Utility.getOrder(getActivity(),
					Constants.SHAREDPREFERENCE_GIFT).contains(offer_id)) {
				button_Confirm.setText("Grabbed");
				button_Confirm
						.setBackgroundResource(R.drawable.button_background_selector_secondry);
			} else {
				button_Confirm.setText("Recharge now");
			}

			textView_Address.setText(map.get(FreeBiesListFragment.ADDRESS));
			double dis = 0;
			DecimalFormat decimalFormat = new DecimalFormat("0.0");
			try {
				dis = Double
						.parseDouble(map.get(FreeBiesListFragment.DISTANCE));
				if (dis == 0) {
					textView_Distance.setVisibility(View.INVISIBLE);
					textView_Unit.setVisibility(View.INVISIBLE);
				} else if (dis < 1) {
					dis = dis * 1000;
					textView_Distance.setText(decimalFormat.format(dis));
					textView_Unit.setText("(M)");
					textView_Distance.setVisibility(View.VISIBLE);
					textView_Unit.setVisibility(View.VISIBLE);
				} else {
					textView_Distance.setText(decimalFormat.format(dis));
					textView_Unit.setText("(KM)");
					textView_Distance.setVisibility(View.VISIBLE);
					textView_Unit.setVisibility(View.VISIBLE);
				}
			} catch (Exception e) {
			}
			// for (int j = 0; j < data_location.size(); j++) {
			// WeakHashMap<String, String> map_location = new
			// WeakHashMap<String, String>(
			// data_location.get(j));
			// if (Integer
			// .parseInt(map_location.get(FreeBiesListFragment.DEAL_ID)) ==
			// Integer
			// .parseInt(deal_id)) {
			// textView_Address.setText(map_location
			// .get(FreeBiesListFragment.ADDRESS));
			// double dis = 0;
			// DecimalFormat decimalFormat = new DecimalFormat("0.0");
			// try {
			// dis = Double.parseDouble(map_location
			// .get(FreeBiesListFragment.DISTANCE));
			// // if (dis < 1) {
			// // // dis = dis * 1000;
			// // textView_Distance
			// // .setText(decimalFormat.format(dis));
			// // textView_Unit.setText("(M)");
			// // } else {
			// textView_Distance.setText(decimalFormat.format(dis));
			// textView_Unit.setText("(KM)");
			// // }
			// } catch (Exception e) {
			// }
			// break;
			// }
			// }

		} catch (Exception e) {
			// System.out.println("Re " + e.getMessage());
		}

		return viewLayout;
	}

	private int daysBetween(Date d1, Date d2) {
		return (int) ((d2.getTime() - d1.getTime()) / (1000 * 60 * 60 * 24));
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt("AMOUNT", recharge_amount);
		outState.putString("TRANS_ID", trans_id);
		outState.putInt("COUNT", count);
		outState.putInt("POSITION", position);
		outState.putBoolean("FLAG", is_FromRecharge);
		outState.putString(FreeBiesListFragment.OFFER_ID, offer_id);
		outState.putString(FreeBiesListFragment.OFFER_NAME, offer_name);
		outState.putString(FreeBiesListFragment.SHORT_DESC, short_desc);
		outState.putString(FreeBiesListFragment.DEAL_ID, deal_id);
		outState.putString(FreeBiesListFragment.DEAL_NAME, deal_name);
		outState.putString(FreeBiesListFragment.MIN_AMOUNT, min_amount);
		outState.putString(FreeBiesListFragment.URL, img_url);
		outState.putSerializable("DATA", data);
		outState.putSerializable("LOCATIONS", data_location);
	}

	public void showTwoButtonDialog(final Context context, String Title,
			String message) {
		try {

			final Dialog dialog = new Dialog(context);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			// dialog.getWindow().setBackgroundDrawableResource(
			// android.R.color.transparent);
			dialog.setContentView(R.layout.dialog_two_button);
			dialog.setCancelable(false);
			dialog.setCanceledOnTouchOutside(false);
			// dialog.setTitle(null);

			Typeface Reguler = Typeface.createFromAsset(context.getAssets(),
					"Roboto-Regular.ttf");

			Typeface Medium = Typeface.createFromAsset(context.getAssets(),
					"Roboto-Regular.ttf");

			// set the custom dialog components - text, image and button
			TextView textView_Title = (TextView) dialog
					.findViewById(R.id.textView_Title);
			textView_Title.setText(Title);
			textView_Title.setTypeface(Reguler);

			TextView textView_Message = (TextView) dialog
					.findViewById(R.id.textView_Message);
			textView_Message.setText(message);
			textView_Message.setTypeface(Medium);

			Button button_Ok = (Button) dialog.findViewById(R.id.button_Ok);
			button_Ok.setTypeface(Reguler);
			button_Ok.setText("Yes");
			// if button is clicked, close the custom dialog
			button_Ok.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
					new PurchaseDealTask().execute(deal_id, offer_id, "", "0",
							"1", trans_id);
				}
			});

			Button button_Cancel = (Button) dialog
					.findViewById(R.id.button_Cancel);
			button_Cancel.setTypeface(Reguler);
			button_Cancel.setText("No");
			// if button is clicked, close the custom dialog
			button_Cancel.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
				}
			});

			dialog.show();

		} catch (Exception exception) {
		}
	}

	public class PurchaseDealTask extends AsyncTask<String, String, String>
			implements OnDismissListener {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {

			String response = RequestClass.getInstance().readPay1B2CRequest(
					getActivity(),
					Constants.B2C_URL + "purchase_deal/?deal_id=" + params[0]
							+ "&offer_id=" + params[1] + "&coupon_id="
							+ params[2] + "&paymentopt=wallet" + "&amount="
							+ params[3] + "&quantity=" + params[4]
							+ "&api_version=3&trans_category=deal&freebie=true&ref_id="
							+ params[5]);

			return response;
		}

		@Override
		protected void onCancelled() {
			// TODO Auto-generated method stub
			super.onCancelled();
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			try {

				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");
					JSONObject jsonObject = new JSONObject(replaced);

					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {
						Utility.setDailyFreeBieTime(getActivity(),
								Constants.SHAREDPREFERENCE_FREEBIES_TIME,
								System.currentTimeMillis());
						// JSONObject description = jsonObject
						// .getJSONObject("description");
						try {
							reClaimGiftDataSource.open();

							ReClaimGift reClaimGift = reClaimGiftDataSource
									.getReClaimGift(trans_id);
							if (reClaimGift != null) {
								reClaimGiftDataSource
										.deleteReClaimGift(reClaimGift
												.getReClaimID());
							} else {
								reClaimGiftDataSource.deleteReClaimGift();
							}
						} catch (SQLException exception) {
							// TODO: handle exception
							// // // Log.e("Er ", exception.getMessage());
						} catch (Exception e) {
							// TODO: handle exception
							// // // Log.e("Er ", e.getMessage());
						} finally {
							reClaimGiftDataSource.close();
						}
						showConfirmationDialog(getActivity());
					} else {
						Constants.showOneButtonDialog(getActivity(),
								"Free Gift", Constants.checkCode(replaced),
								Constants.DIALOG_CLOSE);
					}
				} else {
					Intent intent = new Intent(getActivity(),
							ConnectivityErrorActivity.class);
					startActivity(intent);
				}
			} catch (Exception e) {
				Constants.showOneButtonDialog(getActivity(), "Free Gift",
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(getActivity());
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(false);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							PurchaseDealTask.this.cancel(true);
							dialog.cancel();
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			PurchaseDealTask.this.cancel(true);
			dialog.cancel();
		}

	}

	@SuppressLint("NewApi")
	public void showConfirmationDialog(final Context context) {
		try {

			final Dialog dialog = new Dialog(context);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			// dialog.getWindow().setBackgroundDrawableResource(
			// android.R.color.transparent);
			dialog.setContentView(R.layout.dialog_confirmation_freebies);
			dialog.setCancelable(false);
			dialog.setCanceledOnTouchOutside(false);
			// dialog.setTitle(null);

			Typeface Reguler = Typeface.createFromAsset(context.getAssets(),
					"Roboto-Regular.ttf");

			Typeface Medium = Typeface.createFromAsset(context.getAssets(),
					"Roboto-Regular.ttf");

			// set the custom dialog components - text, image and button
			TextView textView_Title = (TextView) dialog
					.findViewById(R.id.textView_Title);
			textView_Title.setText("Success");
			textView_Title.setTypeface(Reguler);

			TextView textView_Message = (TextView) dialog
					.findViewById(R.id.textView_Message);
			textView_Message
					.setText("You have successfully grabbed your free gift.");
			textView_Message.setTypeface(Medium);

			TextView textView_DealName = (TextView) dialog
					.findViewById(R.id.textView_DealName);
			textView_DealName.setText(offer_name);
			textView_DealName.setTypeface(Medium);

			TextView textView_Address = (TextView) dialog
					.findViewById(R.id.textView_Address);
			// for (int j = 0; j < data_location.size(); j++) {
			// WeakHashMap<String, String> map = new WeakHashMap<String,
			// String>(
			// data_location.get(j));
			// if (Integer.parseInt(map.get(FreeBiesListFragment.DEAL_ID)) ==
			// Integer
			// .parseInt(deal_id)) {
			// textView_Address.setText(map.get(FreeBiesListFragment.ADDRESS));
			// }
			// }
			textView_Address.setText(deal_name);
			textView_Address.setTypeface(Medium);

			ImageView imageView_Image = (ImageView) dialog
					.findViewById(R.id.imageView_Image);
			// imageLoader.getDealImage(img_url, imageView_Image, progressBar1);
			try {
				Picasso.with(getActivity()).load(img_url)
						.placeholder(R.drawable.deal_default)
						.into(imageView_Image);
			} catch (Exception e) {
			}

			Button button_Confirm = (Button) dialog
					.findViewById(R.id.button_Confirm);
			button_Confirm.setTypeface(Reguler);
			// if button is clicked, close the custom dialog
			button_Confirm.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
					Intent intent = new Intent(getActivity(),
							MainActivity.class);
					intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
							| Intent.FLAG_ACTIVITY_NEW_TASK);
					intent.putExtra("IS_MYGIFT", true);
					getActivity().startActivity(intent);
					getActivity().finish();
				}
			});

			dialog.show();

		} catch (Exception exception) {
		}
	}

	public class DealDetailsTask extends AsyncTask<String, String, String>
			implements OnDismissListener {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			try {
				String response = RequestClass.getInstance()
						.readPay1B2CRequest(
								getActivity(),
								Constants.B2C_URL + "get_deal_details/?id="
										+ params[0] + "&latitude=" + params[1]
										+ "&api_version=3&longitude=" + params[2]);

				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			try {

				if (!result.startsWith("Error")) {
					// String replaced = result.replace("(", "").replace(")",
					// "")
					// .replace(";", "");

					JSONObject jsonObject = new JSONObject(result);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {

						JSONObject jsonObject_Desc = jsonObject
								.getJSONObject("description");

						JSONArray jsonArrayDetails = jsonObject_Desc
								.getJSONArray("deal_detail");

						for (int i = 0; i < jsonArrayDetails.length(); i++) {
							JSONObject jsonObjectInner = jsonArrayDetails
									.getJSONObject(i);

							String str = jsonObjectInner.getString(
									"content_txt").trim();
							webView_Details
									.loadDataWithBaseURL(
											null,
											"<html><head><style>@font-face {font-family: Normal;src: url(\"file:///android_asset/Roboto-Regular.ttf\")}"
													+ "@font-face {font-family: Narrow;src: url(\"file:///android_asset/Roboto-Regular.ttf\")}"
													+ "body {margin: 0px 5px;padding:0px;color:#6E6E70;}"
													+ "h1{font-size:16px;font-family:\"Normal\";margin:0;padding:10px 0 5px 0;color:#3F3F40;}"
													+ "h2{font-size:14px;font-family:\"Narrow\";margin:0;padding:10px 0 5px 0;color:#513D98;}"
													+ "ul{margin:0 0 10px 0;padding-left:20px;}"
													+ "li{font-size:14px;margin:0 0 2px 0;font-family:\"Normal\";}</style></head><body>"
													+ str + "</body></html>",
											"text/html", "UTF-8", null);

						}
						webView_Details.setVisibility(View.VISIBLE);
						flag = 1;
						// webView_Details.requestFocus();
					} else {
						Constants.showOneButtonDialog(getActivity(),
								"Free Gifts", Constants.checkCode(result),
								Constants.DIALOG_CLOSE);
					}

				} else {
					// Constants.showOneButtonDialog(DealDetailsActivity.this,
					// TAG, Constants.ERROR_INTERNET,
					// Constants.DIALOG_CLOSE);
					Intent intent = new Intent(getActivity(),
							ConnectivityErrorActivity.class);
					startActivity(intent);

				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonDialog(getActivity(), "Free Gifts",
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonDialog(getActivity(), "Free Gifts",
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
						Constants.DIALOG_CLOSE);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(getActivity());
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							DealDetailsTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			DealDetailsTask.this.cancel(true);
			dialog.cancel();
		}

	}

	public void showTwoButtonDialog(final Context context, String Title,
			String message, final int Type) {
		try {

			final Dialog dialog = new Dialog(context);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			// dialog.getWindow().setBackgroundDrawableResource(
			// android.R.color.transparent);
			dialog.setContentView(R.layout.dialog_two_button);
			dialog.setCancelable(false);
			dialog.setCanceledOnTouchOutside(false);
			// dialog.setTitle(null);

			Typeface Reguler = Typeface.createFromAsset(context.getAssets(),
					"Roboto-Regular.ttf");

			Typeface Medium = Typeface.createFromAsset(context.getAssets(),
					"Roboto-Regular.ttf");

			// set the custom dialog components - text, image and button
			TextView textView_Title = (TextView) dialog
					.findViewById(R.id.textView_Title);
			textView_Title.setText(Title);
			textView_Title.setTypeface(Reguler);

			TextView textView_Message = (TextView) dialog
					.findViewById(R.id.textView_Message);
			textView_Message.setText(message);
			textView_Message.setTypeface(Medium);

			Button button_Ok = (Button) dialog.findViewById(R.id.button_Ok);
			button_Ok.setTypeface(Reguler);
			// if button is clicked, close the custom dialog
			button_Ok.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();

					Intent viewIntent = new Intent(
							Settings.ACTION_LOCATION_SOURCE_SETTINGS);
					context.startActivity(viewIntent);
				}
			});

			Button button_Cancel = (Button) dialog
					.findViewById(R.id.button_Cancel);
			button_Cancel.setTypeface(Reguler);
			// if button is clicked, close the custom dialog
			button_Cancel.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();

					// don't start listeners if no provider is
					// enabled
					boolean show_route = false;
					boolean gps_enabled = false;
					boolean network_enabled = false;
					LocationManager lm = (LocationManager) getActivity()
							.getSystemService(Context.LOCATION_SERVICE);
					try {
						gps_enabled = lm
								.isProviderEnabled(LocationManager.GPS_PROVIDER);
					} catch (Exception ex) {
					}
					try {
						network_enabled = lm
								.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
					} catch (Exception ex) {
					}
					WifiManager wifi = (WifiManager) getActivity()
							.getSystemService(Context.WIFI_SERVICE);
					if (wifi.isWifiEnabled())
						show_route = true;
					else if (network_enabled && gps_enabled)
						show_route = true;
					else if (network_enabled)
						show_route = true;
					try {

						ArrayList<WeakHashMap<String, String>> temp = new ArrayList<WeakHashMap<String, String>>();

						for (int j = 0; j < data_location.size(); j++) {
							WeakHashMap<String, String> map1 = new WeakHashMap<String, String>();
							WeakHashMap<String, String> map = new WeakHashMap<String, String>(
									data_location.get(j));
							if (Integer.parseInt(map.get(DEAL_ID)) == Integer
									.parseInt(offer_id)) {
								map1.put(LATITUDE, map.get(LATITUDE));
								map1.put(LONGITUDE, map.get(LONGITUDE));
								map1.put(ADDRESS, map.get(ADDRESS));
								map1.put(CITY, map.get(CITY));
								map1.put(STATE, map.get(STATE));

								temp.add(map1);
							}
						}
						if (temp.size() > 1) {
							Intent intent = new Intent(getActivity(),
									FreeBiesLocationActivity.class);
							intent.putExtra("ROUTE", show_route);
							intent.putExtra("DEAL", textView_DealName.getText()
									.toString().trim());
							intent.putExtra("ID", deal_id);
							intent.putExtra("FROM_DETAILS", true);
							intent.putExtra("LOCATIONS", data_location);
							startActivity(intent);
						} else {
							Intent intent = new Intent(getActivity(),
									FreeBiesLocatorActivity.class);
							WeakHashMap<String, String> map = new WeakHashMap<String, String>(
									temp.get(0));

							intent.putExtra("ROUTE", show_route);
							intent.putExtra("DEAL", textView_DealName.getText()
									.toString().trim());
							intent.putExtra("LATITUDE",
									map.get(FreeBiesListFragment.LATITUDE));
							intent.putExtra("LONGITUDE",
									map.get(FreeBiesListFragment.LONGITUDE));
							intent.putExtra("ADDRESS",
									map.get(FreeBiesListFragment.ADDRESS));
							intent.putExtra("CITY",
									map.get(FreeBiesListFragment.CITY));
							intent.putExtra("STATE",
									map.get(FreeBiesListFragment.STATE));
							startActivity(intent);
						}
					} catch (Exception e) {
						Intent intent = new Intent(getActivity(),
								FreeBiesLocationActivity.class);
						intent.putExtra("ROUTE", show_route);
						intent.putExtra("DEAL", textView_DealName.getText()
								.toString().trim());
						intent.putExtra("ID", deal_id);
						intent.putExtra("FROM_DETAILS", true);
						intent.putExtra("LOCATIONS", data_location);
						startActivity(intent);
					}

				}
			});

			button_Ok.setText("Ok");
			button_Cancel.setText("Not now");

			dialog.show();

		} catch (Exception exception) {
		}
	}
}
