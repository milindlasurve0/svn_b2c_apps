//
//  FAQDataList.h
//  Pay1
//
//  Created by webninjaz on 16/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FAQDataList : NSObject

@property (nonatomic, strong) NSString *faqDataID;
@property (nonatomic, strong) NSString *faqDataQuestion;
@property (nonatomic, strong) NSString *faqDataAnswer;
@property (nonatomic, strong) NSString *faqDataVisible;
@property (nonatomic, strong) NSMutableArray *descriptionDataArray;

@end
