<?php

class Careerdata extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    function insert_career($data=NULL)
    {   
        $data = array(
            'name' => $data['name'] ,
            'phone' => $data['phone'] ,
            'email' => $data['email'],
            'file' => $data['file'],
            'message' => $data['message'],
            'job_title'=> $data['job_title'],
        );
        $this->db->insert('career', $data); 
    }
}