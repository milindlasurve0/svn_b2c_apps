package com.pay1;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import com.pay1.customviews.MyProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.pay1.constants.Constants;
import com.pay1.requesthandler.RequestClass;
import com.pay1.utilities.EditTextValidator;
import com.pay1.utilities.Utility;

public class EditQuickPayActivity extends AppBaseActivity {

	LinearLayout toggleLayout, stvLayout, linearLayout_Acknowledgement;
	// Button btnContacts;
	/*
	 * TextView textViewOperator, textViewSpecial, textViewMob, textViewName,
	 * textViewAmount, textViewMiss, textViewAck;
	 */
	TextView textViewAmount, textViewSpecial, textViewMissCallNumber;
	ToggleButton toggleMissCall, toggleAck, toggleSTV;
	ImageView imageView_Ok, imageView_Cancel, imageView_Back, imageDelete;
	TextView editRechargeMobile, editName;
	EditText editAmount;// , editOperator;
	String stv = "0", mobNumber;
	String pay1OpId, flag, delegate_flag, missed_number;
	String operator_id, id, operatorName = null;
	String isMisscall = "0", isAcknldg = "0";
	Typeface Normal, Reguler, Medium;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.edit_layout);
		try {

			registerBaseActivityReceiver();

			findViewById();

			Normal = Typeface.createFromAsset(getAssets(),
					"EncodeSansNormal-100-Thin.ttf");
			Reguler = Typeface.createFromAsset(getAssets(),
					"EncodeSansNormal-400-Regular.ttf");
			Medium = Typeface.createFromAsset(getAssets(),
					"EncodeSansNarrow-500-Medium.ttf");
			textViewAmount.setTypeface(Medium);
			textViewSpecial.setTypeface(Medium);
			textViewMissCallNumber.setTypeface(Medium);
			/*
			 * textViewOperator.setTypeface(Medium);
			 * textViewMob.setTypeface(Medium);
			 * textViewName.setTypeface(Medium);
			 * textViewAmount.setTypeface(Medium);
			 * textViewMiss.setTypeface(Medium);
			 * textViewAck.setTypeface(Medium);
			 */

			String edit_data = getIntent().getExtras().getString(
					Constants.EDIT_DATA);
			try {
				JSONObject editJsonObject = new JSONObject(edit_data);
				stv = editJsonObject.getString("stv");
				editName.setTypeface(Normal);
				editAmount.setTypeface(Normal);
				editRechargeMobile.setTypeface(Medium);
				checkForSTV(stv);
				editName.setText(editJsonObject.getString("name"));
				editName.setEnabled(false);
				delegate_flag = editJsonObject.getString("delegate_flag");
				missed_number = editJsonObject.getString("missed_number");
				if (delegate_flag.equalsIgnoreCase("1")) {
					textViewMissCallNumber.setVisibility(View.VISIBLE);
					textViewMissCallNumber.setText("Miss Call No. "
							+ missed_number);
				}
				editAmount.setText(editJsonObject.getString("amount"));

				operator_id = editJsonObject.getString("operator_id");
				operatorName = editJsonObject.getString("operator_name");
				id = editJsonObject.getString("id");

				editRechargeMobile.setEnabled(false);

				if (editJsonObject.getString("delegate_flag").equalsIgnoreCase(
						"1")) {
					toggleMissCall.setChecked(true);
				} else {
					toggleMissCall.setChecked(false);
				}

				if (editJsonObject.getString("confirmation").equalsIgnoreCase(
						"1")) {
					toggleAck.setChecked(true);
				} else {
					toggleAck.setChecked(false);
				}

				flag = editJsonObject.getString("flag");

				if (flag.equalsIgnoreCase("1")) {
					// operatorArray = arrayHelper.getArray("operatorArray");
				} else if (flag.equalsIgnoreCase("2")) {
					// operatorArray = arrayHelper.getArray("dthOperatorArray");
					// operatorArray = RechargeMobileActivity.dthOperatorArray;
					// textViewMob.setText("SUBSCRIBER ID");
					toggleLayout.setVisibility(View.GONE);
				} else if (flag.equalsIgnoreCase("3")) {
					// operatorArray =
					// arrayHelper.getArray("dataOperatorArray");
					// operatorArray = RechargeMobileActivity.dataOperatorArray;
				} else if (flag.equalsIgnoreCase("4")) {
					// operatorArray =
					// arrayHelper.getArray("billOperatorArray");
					// operatorArray = RechargeMobileActivity.billOperatorArray;
					toggleLayout.setVisibility(View.GONE);
				}
				mobNumber = editJsonObject.getString("number");
				editRechargeMobile.setText(mobNumber + "( " + operatorName
						+ " )");

			} catch (JSONException je) {
				je.printStackTrace();
			}

			imageView_Back.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					finish();
				}
			});
			editAmount.setEnabled(true);

			editRechargeMobile.setEnabled(false);

			imageDelete.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					// new DeleteQuickTask().execute(id);
					try {

						final Dialog dialog = new Dialog(
								EditQuickPayActivity.this);
						dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
						// dialog.getWindow().setBackgroundDrawableResource(
						// android.R.color.transparent);
						dialog.setContentView(R.layout.dialog_success_two_button);
						// dialog.setTitle(null);

						// set the custom dialog components
						// - text, image and button
						TextView textView_Title = (TextView) dialog
								.findViewById(R.id.textView_Title);
						textView_Title.setText("Quick pay");

						TextView textView_Message = (TextView) dialog
								.findViewById(R.id.textView_Message);
						textView_Message
								.setText("Are you sure you want to delete this entry?");

						ImageView imageView_Ok = (ImageView) dialog
								.findViewById(R.id.imageView_Ok);
						// if button is clicked, close the
						// custom dialog
						imageView_Ok
								.setOnClickListener(new View.OnClickListener() {
									@Override
									public void onClick(View v) {
										dialog.dismiss();
										new DeleteQuickTask().execute(id);
									}
								});

						ImageView imageView_Cancel = (ImageView) dialog
								.findViewById(R.id.imageView_Cancel);
						// if button is clicked, close the
						// custom dialog
						imageView_Cancel
								.setOnClickListener(new View.OnClickListener() {
									@Override
									public void onClick(View v) {
										dialog.dismiss();
									}
								});

						dialog.show();

					} catch (Exception exception) {
					}
				}
			});
			toggleSTV.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (toggleSTV.isChecked()) {

						stv = "1";
					} else {

						stv = "0";
					}
				}
			});
			imageView_Ok.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (Utility.getLoginFlag(EditQuickPayActivity.this,
							Constants.SHAREDPREFERENCE_IS_LOGIN)) {

						if (!checkValidation(EditQuickPayActivity.this)) {

						} else {

							new AddQuickPayTask().execute(editName.getText()
									.toString().trim(), mobNumber, editAmount
									.getText().toString().trim(), operator_id,
									flag, isMisscall, isAcknldg, id, stv);
						}
					}
				}
			});

			toggleAck.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (toggleAck.isChecked()) {

						isAcknldg = "1";
					} else {

						isAcknldg = "0";
					}
				}
			});

			toggleMissCall.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (toggleMissCall.isChecked()) {

						isMisscall = "1";
					} else {

						isMisscall = "0";
					}
				}
			});
		} catch (Exception e) {
		}
	}

	protected void checkForSTV(String pay1OpId2) {

		if (pay1OpId2.equalsIgnoreCase("1")) {
			stvLayout.setVisibility(View.VISIBLE);
			textViewSpecial.setVisibility(View.VISIBLE);
			if (stv.equalsIgnoreCase("1")) {
				toggleSTV.setChecked(true);
			} else {
				toggleSTV.setChecked(false);
			}
		} else {
			textViewSpecial.setVisibility(View.GONE);
			stvLayout.setVisibility(View.GONE);
		}
	}

	public class AddQuickPayTask extends AsyncTask<String, String, String>
			implements OnDismissListener {
		private MyProgressDialog dialog;
		String url;

		@Override
		protected String doInBackground(String... params) {
			ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();
			// listValuePair.add(new BasicNameValuePair("actiontype",
			// "recharge"));

			listValuePair.add(new BasicNameValuePair("name", params[0]));

			listValuePair.add(new BasicNameValuePair("number", params[1]));
			listValuePair.add(new BasicNameValuePair("amount", params[2]));
			listValuePair.add(new BasicNameValuePair("operator_id", params[3]));
			listValuePair.add(new BasicNameValuePair("flag", params[4]));
			listValuePair
					.add(new BasicNameValuePair("delegate_flag", params[5]));
			listValuePair
					.add(new BasicNameValuePair("confirmation", params[6]));
			listValuePair.add(new BasicNameValuePair("id", params[7]));
			listValuePair.add(new BasicNameValuePair("stv", params[8]));

			url = Constants.B2C_URL + "edit_quickpay";
			String response = RequestClass.getInstance().readPay1B2CRequest(
					EditQuickPayActivity.this, url, listValuePair);

			return response;
		}

		@Override
		protected void onCancelled() {
			// TODO Auto-generated method stub
			super.onCancelled();
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			try {

				if (!result.startsWith("Error")) {

					JSONObject jsonObject = new JSONObject(result);

					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {
						JSONObject description = jsonObject
								.getJSONObject("description");
						String missed_number = description
								.getString("missed_number");
						String msg = description.getString("msg");
						String msgString = "";
						if (missed_number.equalsIgnoreCase("null")
								|| missed_number.equalsIgnoreCase("")) {
							msgString = msg;// +
											// "\nTo recharge this number give misscall to "+
											// missed_number + ".";
						} else {
							msgString = msg
									+ "\nTo recharge this number give misscall to "
									+ missed_number + ".";
						}
						Constants.showOneButtonDialog(
								EditQuickPayActivity.this, "Add Quick Pay",
								msgString, Constants.DIALOG_CLOSE_ORDER);

					} else {
						Constants.showOneButtonDialog(
								EditQuickPayActivity.this, "Add Quick Pay",
								jsonObject.getString("description"),
								Constants.DIALOG_CLOSE_ORDER);
					}

				} else {
					// Constants.showOneButtonDialog(EditQuickPayActivity.this,
					// "Add Quick Pay", Constants.ERROR_INTERNET,
					// Constants.DIALOG_CLOSE_ORDER);
					Intent intent = new Intent(EditQuickPayActivity.this,
							ConnectivityErrorActivity.class);
					intent.putExtra(Constants.RECHARGE_FOR,
							Constants.WALLET_PAYMENT);
					startActivity(intent);

				}
			} catch (JSONException e) {

			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(EditQuickPayActivity.this);
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(false);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							AddQuickPayTask.this.cancel(true);
							dialog.cancel();
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			AddQuickPayTask.this.cancel(true);
			dialog.cancel();
		}
	}

	private boolean checkValidation(Context context) {
		boolean ret = true;
		if (!EditTextValidator.hasText(context, editAmount,
				Constants.ERROR_AMOUNT_BLANK_FIELD)) {
			ret = false;
			editAmount.requestFocus();
			return ret;
		} else if (!EditTextValidator.isValidAmount(context, editAmount,
				Constants.ERROR_AMOUNT_VALID_FIELD)) {
			ret = false;
			editAmount.requestFocus();
			return ret;
		} else
			return ret;
	}

	private void findViewById() {
		// TODO Auto-generated method stub
		editRechargeMobile = (TextView) findViewById(R.id.editRechargeMobile);
		editName = (TextView) findViewById(R.id.editName);
		// editOperator = (EditText) findViewById(R.id.editOperator);
		// spinnerOperator = (Spinner) findViewById(R.id.spinnerOperator);
		imageDelete = (ImageView) findViewById(R.id.imageDelete);
		imageView_Back = (ImageView) findViewById(R.id.imageView_Back);
		editAmount = (EditText) findViewById(R.id.editAmount);
		toggleSTV = (ToggleButton) findViewById(R.id.toggleSTV);
		toggleMissCall = (ToggleButton) findViewById(R.id.toggleMissCall);
		toggleAck = (ToggleButton) findViewById(R.id.toggleAck);
		imageView_Ok = (ImageView) findViewById(R.id.imageView_Ok);
		toggleLayout = (LinearLayout) findViewById(R.id.toggleLayout);
		stvLayout = (LinearLayout) findViewById(R.id.stvLayout);
		textViewSpecial = (TextView) findViewById(R.id.textViewSpecial);
		textViewAmount = (TextView) findViewById(R.id.textViewAmount);
		textViewMissCallNumber = (TextView) findViewById(R.id.textViewMissCallNumber);
		/*
		 * textViewMob = (TextView) findViewById(R.id.textViewMob);
		 * textViewOperator = (TextView) findViewById(R.id.textViewOperator);
		 * textViewSpecial = (TextView) findViewById(R.id.textViewSpecial);
		 * textViewName = (TextView) findViewById(R.id.textViewName);
		 * textViewAmount = (TextView) findViewById(R.id.textViewAmount);
		 * textViewMiss = (TextView) findViewById(R.id.textViewMiss);
		 * textViewAck = (TextView) findViewById(R.id.textViewAck);
		 */
		imageView_Cancel = (ImageView) findViewById(R.id.imageView_Cancel);
		linearLayout_Acknowledgement = (LinearLayout) findViewById(R.id.linearLayout_Acknowledgement);
		linearLayout_Acknowledgement.setVisibility(View.GONE);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (Constants.isExpanded) {
			Constants.collapsView(this);
		} else {
			Constants.SELECTED_MENU_INDEX = Constants.PROFILE_MENU_INDEX;
			Constants.showNavigationBar(EditQuickPayActivity.this);
		}
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unRegisterBaseActivityReceiver();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (Constants.isExpanded) {
			Constants.collapsView(this);
		} else {
			super.onBackPressed();
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);

		if (resultCode == Activity.RESULT_OK) {

			if (requestCode == RechargeMobileActivity.PICK_CONTACT) {

				Uri contactData = data.getData();
				Cursor contactCursor = getContentResolver().query(contactData,
						new String[] { ContactsContract.Contacts._ID }, null,
						null, null);
				String id = null;
				if (contactCursor.moveToFirst()) {
					id = contactCursor.getString(contactCursor
							.getColumnIndex(ContactsContract.Contacts._ID));
				}
				contactCursor.close();
				String phoneNumber = null, displayName = null;
				Cursor phoneCursor = getContentResolver().query(
						ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
						new String[] {
								ContactsContract.CommonDataKinds.Phone.NUMBER,
								ContactsContract.PhoneLookup.DISPLAY_NAME },
						ContactsContract.CommonDataKinds.Phone.CONTACT_ID
								+ "= ? ", new String[] { id }, null);
				if (phoneCursor.moveToFirst()) {
					phoneNumber = phoneCursor
							.getString(phoneCursor
									.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
					displayName = phoneCursor
							.getString(phoneCursor
									.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
					if (phoneNumber.length() >= 10) {
						phoneNumber = phoneNumber
								.substring(phoneNumber.length() - 10,
										phoneNumber.length());
					} else {
						Toast.makeText(EditQuickPayActivity.this,
								"Please select a valid number",
								Toast.LENGTH_LONG).show();
					}
					editRechargeMobile.setText(phoneNumber);
					editName.setText(displayName);
				}
				phoneCursor.close();
			}

		}
	}

	public String setSpinnerValue() {
		String name = "";
		// try {
		//
		// ArrayList<JSONObject> array = null;
		// array = operatorArray;
		//
		// for (int i = 0; i < array.size(); i++) {
		// JSONObject obj = array.get(i);// jsonArray.getJSONObject(i);
		// String opr_code = obj.getString("id");
		// if (operator_id.equalsIgnoreCase(opr_code)) {
		// name = obj.getString("name");
		//
		// break;
		// }
		// }
		// } catch (JSONException je) {
		// return name;
		// }
		return name;
	}

	/*
	 * public int setSpinnerValue() { int k = 0; try {
	 * 
	 * ArrayList<JSONObject> array = null; array = operatorArray;
	 * 
	 * for (int i = 0; i < array.size(); i++) { JSONObject obj = array.get(i);//
	 * jsonArray.getJSONObject(i); String opr_code = obj.getString("id"); if
	 * (operator_id.equalsIgnoreCase(opr_code)) { String name =
	 * obj.getString("inamed");
	 * 
	 * Log.w("", opr_code + " " + k); k = i; break; } } } catch (JSONException
	 * je) { return k; } return k; }
	 */

	public class DeleteQuickTask extends AsyncTask<String, String, String>
			implements OnDismissListener {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String url = Constants.B2C_URL + "del_quickpay/?id=" + params[0];
			String response = RequestClass.getInstance().readPay1B2CRequest(
					EditQuickPayActivity.this, url);
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			if (!result.startsWith("Error")) {

				String status;
				try {
					JSONObject jsonObject = new JSONObject(result);
					status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {

						finish();

					} else {
						Constants.showOneButtonDialog(
								EditQuickPayActivity.this, "Quick Pay",
								jsonObject.getString("description"),
								Constants.DIALOG_CLOSE);
					}

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} else {
				Constants.showOneButtonDialog(EditQuickPayActivity.this,
						"Quick Pay", result, Constants.DIALOG_CLOSE);
			}

		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(EditQuickPayActivity.this);
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							DeleteQuickTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			DeleteQuickTask.this.cancel(true);
			dialog.cancel();
		}

	}

}
