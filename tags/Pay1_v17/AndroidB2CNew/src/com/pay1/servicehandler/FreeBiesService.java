package com.pay1.servicehandler;

import java.util.WeakHashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.IntentService;
import android.content.Intent;
import android.database.SQLException;
import android.os.AsyncTask;

import com.pay1.constants.Constants;
import com.pay1.databasehandler.FreeBiesDataSource;
import com.pay1.databasehandler.FreeBiesLocationDataSource;
import com.pay1.requesthandler.RequestClass;
import com.pay1.utilities.Utility;

public class FreeBiesService extends IntentService {

	private FreeBiesDataSource freeBiesDataSource;
	private FreeBiesLocationDataSource freeBiesLocationDataSource;

	public FreeBiesService() {
		super("FreeBiesService");
		// TODO Auto-generated constructor stub
		freeBiesDataSource = new FreeBiesDataSource(FreeBiesService.this);
		freeBiesLocationDataSource = new FreeBiesLocationDataSource(
				FreeBiesService.this);
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		new FreeBiesTask().execute(Utility.getCurrentLatitude(
				FreeBiesService.this,
				Constants.SHAREDPREFERENCE_CURRENT_LATITUDE), Utility
				.getCurrentLongitude(FreeBiesService.this,
						Constants.SHAREDPREFERENCE_CURRENT_LONGITUDE));
	}

	public class FreeBiesTask extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... params) {
			try {
				String response = RequestClass
						.getInstance()
						.readPay1B2CRequest(
								FreeBiesService.this,
								Constants.B2C_URL
										+ "Get_all_deal_data/?latitude="
										+ params[0]
										+ "&longitude="
										+ params[1]
										+ "&mobile="
										+ Utility
												.getMobileNumber(
														FreeBiesService.this,
														Constants.SHAREDPREFERENCE_MOBILE));

				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			try {
				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");
					// System.out.println(replaced);
					JSONObject jsonObject = new JSONObject(replaced);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {

						try {

							freeBiesDataSource.open();
							freeBiesLocationDataSource.open();

							freeBiesDataSource.deleteFreeBies();
							freeBiesLocationDataSource.deleteFreeBiesLocation();

							JSONArray jsonArrayDesc = jsonObject
									.getJSONArray("description");

							long timestamp = System.currentTimeMillis();
							for (int i = 0; i < jsonArrayDesc.length(); i++) {
								WeakHashMap<String, String> map = new WeakHashMap<String, String>();

								JSONObject jsonObjectOffer = jsonArrayDesc
										.getJSONObject(i).getJSONObject(
												"offer_detail");

								String deal_id = jsonArrayDesc.getJSONObject(i)
										.getString("id");
								String deal_name = jsonArrayDesc.getJSONObject(
										i).getString("dealname");
								String catagory = jsonArrayDesc
										.getJSONObject(i).getString("category");
								String catagory_id = jsonArrayDesc
										.getJSONObject(i).getString(
												"category_id");
								String url = jsonArrayDesc.getJSONObject(i)
										.getString("img_url");
								String logo = jsonArrayDesc.getJSONObject(i)
										.getString("logo_url");
								String offer_id = jsonObjectOffer
										.getString("id");
								String offer_name = jsonObjectOffer
										.getString("name");
								String desc = jsonObjectOffer
										.getString("offer_desc");
								String min_amt = jsonObjectOffer
										.getString("min_amount");
								String dealer_contact = jsonObjectOffer
										.getString("dealer_contact");
								String validity = jsonObjectOffer
										.getString("validity");

								String offer_lat = "0", offer_lng = "0", offer_add = "", offer_city = "", offer_state = "";

								JSONArray jsonArrayLocation = jsonArrayDesc
										.getJSONObject(i).getJSONArray(
												"location_detail");

								for (int j = 0; j < jsonArrayLocation.length(); j++) {
									JSONObject jsonObjectInner = jsonArrayLocation
											.getJSONObject(j);
									WeakHashMap<String, String> map1 = new WeakHashMap<String, String>();

									String loc_lat = jsonObjectInner
											.getString("lat");
									String loc_lng = jsonObjectInner
											.getString("lng");
									String loc_add = jsonObjectInner
											.getString("address");
									String loc_city = jsonObjectInner
											.getString("city");
									String loc_state = jsonObjectInner
											.getString("state");
									// String loc_distance = jsonObjectInner
									// .getString("distance");

									if (j == 0) {
										offer_lat = loc_lat;
										offer_lng = loc_lng;
										offer_add = loc_add;
										offer_city = loc_city;
										offer_state = loc_state;
									}

									freeBiesLocationDataSource
											.createFreeBiesLocation(
													Integer.parseInt(offer_id),
													Double.parseDouble(loc_lat),
													Double.parseDouble(loc_lng),
													loc_add, "", loc_city,
													loc_state, 0, 0, timestamp);
								}

								freeBiesDataSource.createFreeBies(
										Integer.parseInt(deal_id), deal_name,
										url, catagory,
										Integer.parseInt(catagory_id),
										Double.parseDouble(offer_lat),
										Double.parseDouble(offer_lng),
										offer_add, "", offer_city, offer_state,
										0, jsonArrayLocation.length(),
										Integer.parseInt(offer_id), offer_name,
										validity, desc,
										Integer.parseInt(min_amt), timestamp,
										0, logo, dealer_contact);
							}

							Utility.setSharedPreferences(FreeBiesService.this,
									Constants.LAST_GIFT_UPDATED,
									String.valueOf(System.currentTimeMillis()));
						} catch (SQLException exception) {
							// TODO: handle exception
							// Log.e("Er ", exception.getMessage());
						} catch (Exception e) {
							// TODO: handle exception
							// Log.e("Er ", e.getMessage());
						} finally {
							freeBiesDataSource.close();
							freeBiesLocationDataSource.close();
						}

					}
				}
			} catch (JSONException e) {
				// e.printStackTrace();

			} catch (Exception e) {
				// TODO: handle exception
			}
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

	}

}