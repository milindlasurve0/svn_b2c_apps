//
//  SignInResponse.m
//  Pay1
//
//  Created by Annapurna on 12/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "SignInResponse.h"

@implementation SignInResponse
-(id)init {
    self = [super init];
    if(self) {
        self.offerDetailArray = [[NSMutableArray alloc] init] ;
        self.recentPay1TransactionArray = [[NSMutableArray alloc] init] ;
    }
    return self;
}
@end
