﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Controls.Maps;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Microsoft.Phone.Controls.Maps.Platform;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Windows.Input;
using NewPay1B2C.UserControls;

namespace NewPay1B2C
    {
    public partial class DealLocator : PhoneApplicationPage
        {
        ProgressBarManager ProgressBarManager;
        string firstLat, firstLong, firstAddress;
        List<string> allLocation;
        JArray  locations1 ;
        public DealLocator ()
            {
            InitializeComponent ();


			string requestFor = PhoneApplicationService.Current.State[Constant.REQUEST_FOR].ToString();
			if (requestFor.Equals (Constant.MYFREEBIE))
				{
				allLocation = MyFreeBieDetails.addressList;
				}
			else if (requestFor.Equals (Constant.ALLFREEBIE))
				{
				allLocation = FreeBieMainLayout.addressList;
				}
           
            bool isFirst = true;
            GoogleAnalytics.EasyTracker.GetTracker ().SendView ("DealLocator");
            ProgressIndicator prog = new ProgressIndicator ();

            ProgressBarManager = new ProgressBarManager ();
            SystemTray.SetProgressIndicator (this, prog);
            ProgressBarManager.Hook (prog);
            ProgressBarManager.Show (4, "Please wait...", false, 10);
            int i =Convert.ToInt16( (string)PhoneApplicationService.Current.State[Constant.SHOP_LOCATION_DATA]);
                 locations1 = (JArray)JsonConvert.DeserializeObject (allLocation[i]);
                foreach (var locations in locations1)
                    {
                    string latitude = locations["lat"].ToString ();
                    string longitude = locations["lng"].ToString ();
                    string address = locations["address"].ToString ();
                    string city = locations["city"].ToString ();
                    string state = locations["state"].ToString ();
                    string distance = locations["distance"].ToString ();
                    if (isFirst)
                        {
                        firstLat = latitude;
                        firstLong = longitude;
                        firstAddress = address + "\n " + city + ", " + distance;
                        isFirst = false;
                        loadAllShopsOnMap (firstLat, firstLong, firstAddress);
                        }
                    DealAllLocationControl dealLocation = new DealAllLocationControl ();
                    dealLocation.showDealerLocation.Click += (s, e) =>
                    {
                        //MessageBox.Show("sdgagagdfahadshtdshdhstdhsdgh");
                        PhoneApplicationService.Current.State[DealLocator.DEAL_LOCATION_DATA] = locations.ToString ();
                        NavigationService.Navigate (new Uri ("/DealShopRoute.xaml?", UriKind.RelativeOrAbsolute));
                        NavigationService.RemoveBackEntry ();
                    };
                    //dealLocation.dealLocationPanel.MouseEnter += new MouseEventHandler(Event_Tab_MouseEnter);
                    dealLocation.textArea.Text = locations["address"].ToString ();
                    dealLocation.textCityState.Text = locations["city"] + ", " + locations["state"];
                    dealLocationListBox.Items.Add (dealLocation);
                    //loadAllShopsOnMap(latitude, longitude, address + "\n " + city + ", " + distance;);

                   
                    }
                ProgressBarManager.Hide (4);
            }

        private void dealDetailListSelection (object sender, EventArgs e)
            {
            int index = dealLocationListBox.SelectedIndex;
            var locations = (JObject)JsonConvert.DeserializeObject (locations1[index].ToString());
            string latitude = locations["lat"].ToString ();
            string longitude = locations["lng"].ToString ();
            string address = locations["address"].ToString ();
            string city = locations["city"].ToString ();
            string state = locations["state"].ToString ();
            string distance = locations["distance"].ToString ();

            loadAllShopsOnMap (latitude, longitude, address + "\n " + city + ", " + distance);
            }

        void pushPin_Tap (object sender, System.Windows.Input.GestureEventArgs e)
            {
            var push = sender as Pushpin;

            Border brodr = (Border)push.Content;
            if (brodr.Visibility == Visibility.Visible)
                {
                brodr.Visibility = System.Windows.Visibility.Collapsed;
                }
            else
                {
                brodr.Visibility = System.Windows.Visibility.Visible;
                }

            //stop the event from going to the parent map control
            e.Handled = true;
            }


        MapLayer PinLayer1;
        public static string DEAL_LOCATION_DATA = "dealLocationdata";
        private void loadAllShopsOnMap (string latiitude, string longitude, string address)
            {
            PinLayer1 = new MapLayer ();
            Pushpin MyPin = new Pushpin ();

            Border border = new Border ();
            border.BorderBrush = new SolidColorBrush (Colors.Black);
            border.Background = new SolidColorBrush (Colors.Black);
            TextBlock textblok = new TextBlock ();
            textblok.Text = address;
            border.BorderThickness = new Thickness (5);
            border.Child = textblok;
            border.Visibility = Visibility.Collapsed;
            MyPin.Content = border;


            Uri imgUri = new Uri ("Images/location.png", UriKind.RelativeOrAbsolute);
            BitmapImage imgSourceR = new BitmapImage (imgUri);
            ImageBrush imgBrush = new ImageBrush () { ImageSource = imgSourceR };


            MyPin.Background = imgBrush;
            MyPin.Tap += pushPin_Tap;
            MyPin.Location = new Location ();
            MyPin.Location.Latitude = Convert.ToDouble (latiitude);
            MyPin.Tap += pushPin_Tap;
            MyPin.Location.Longitude = Convert.ToDouble (longitude);
            //MyPin.Content = address;
            PinLayer1.Children.Add (MyPin);

            sampleMap.Children.Add (PinLayer1);

            sampleMap.Center = MyPin.Location;
            sampleMap.ZoomLevel = 15;

            }

        private void buttonImage_Click_Back (object sender, RoutedEventArgs e)
            {
            NavigationService.GoBack ();
            }


        }
    }