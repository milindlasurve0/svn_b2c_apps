//
//  ShopDetailViewController.m
//  Pay1
//
//  Created by Annapurna on 20/08/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "ShopDetailViewController.h"
#import "StoreList.h"
#import "CurrentLocation.h"
#import "CustomInfoWindow.h"
#import "MDDirectionService.h"
#import "UIImage+Utility.h"
@interface ShopDetailViewController ()

@end

@implementation ShopDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    isShopViewTapped = FALSE;
    
    [self hideNavBackButton];
    
    _coordinates = [[NSMutableArray alloc]init];

    
    _mapView.hidden = TRUE;
    _mapView.delegate = self;
    
    CLLocation *location = [CurrentLocation sharedLocation].currentLocation;
    
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:location.coordinate.latitude  longitude:location.coordinate.longitude  zoom:14.0];
    
    [_mapView animateToCameraPosition:camera];
    
    [self setNaviationButtonWithText:@"Locate cash topup store" selector:@selector(backClicked:) isLeft:TRUE isReduceSize:FALSE isMyFont: 18.0];

    
    shopHeaderView.frame = CGRectMake(0, 64, self.view.frame.size.width, 70);
    shopInfoView.frame = CGRectMake(0, 134, self.view.frame.size.width, self.view.frame.size.height - 134);
    
    UITapGestureRecognizer *tapGes = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(shopViewTapped:)];
    tapGes.delegate = self;
    [shopHeaderView addGestureRecognizer:tapGes];
    
    [self putDataOfShopOnView];
}

-(void)shopViewTapped:(UITapGestureRecognizer *)sender{
    NSLog(@"shopviewtapped");
    
    [_mapView clear];

    
    if(!isShopViewTapped){
        CATransition* transition = [CATransition animation];
        transition.duration = 0.5f;//kAnimationDuration
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromTop; //kCATransitionFromLeft;
        transition.delegate = self;
        [shopInfoView.layer addAnimation:transition forKey:nil];
        
        _mapView.hidden = FALSE;
        shopInfoView.hidden = TRUE;
        
        
        _mapView.frame = CGRectMake(0, 70, self.view.frame.size.width, self.view.frame.size.height - 140);
        shopHeaderView.frame = CGRectMake(0, _mapView.frame.origin.y + _mapView.frame.size.height, self.view.frame.size.width, 70);
        
        [self plotOnlyShopOnMap];
    }
    
    else{
        
        CATransition* transition = [CATransition animation];
        transition.duration = 0.5f;//kAnimationDuration
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromTop; //kCATransitionFromLeft;
        transition.delegate = self;
        [shopInfoView.layer addAnimation:transition forKey:nil];
        
        
        _mapView.hidden = TRUE;
        shopInfoView.hidden = FALSE;
        shopHeaderView.frame = CGRectMake(0, 64, self.view.frame.size.width, 70);

    }
    
    isShopViewTapped = !isShopViewTapped;
}

-(void)putDataOfShopOnView{
    
    shopNameheaderLabel.text = _objStoreList.storeShopName;
    
    
    shopAddressheaderLabel.text = [NSString stringWithFormat:@"%@, %@",_objStoreList.storeAreaName,_objStoreList.storeCityName];;//_objStoreList.storeAddress;
    
    
    NSString *distance = [NSString stringWithFormat:@"%@ KM",[NSString stringWithFormat:@"%.2f", [_objStoreList.storeDistance floatValue]]];

    
    shopDistanceheaderLabel.text =  distance;//_objStoreList.storeDistance;
    
    shopNameLabel.text = _objStoreList.storeShopName;
    shopAddressLabel.text = [NSString stringWithFormat:@"Address : %@",_objStoreList.storeAddress];
    
    shopAddressLabel.lineBreakMode = NSLineBreakByWordWrapping;
    shopAddressLabel.numberOfLines = 0;
    [shopAddressLabel sizeToFit];
    
  //  [self setTextOnLabel:shopAddressLabel fontSize:16.0];
    
    
   /* CGSize yourLabelSize = [shopAddressLabel.text sizeWithAttributes:@{NSFontAttributeName : [UIFont systemFontOfSize:16.0]}];
    
    CGRect newFrame = shopAddressLabel.frame;
    
    newFrame.size.height = yourLabelSize.height + 20;
    shopAddressLabel.frame = newFrame;

    shopAddress1Label.frame = CGRectMake(13, shopAddressLabel.frame.origin.y + shopAddressLabel.frame.size.height + 10, 200, 21);*/
    
    shopAddress1Label.text = [NSString stringWithFormat:@"%@, %@",_objStoreList.storeAreaName,_objStoreList.storeCityName];;
    
   // shopAddress2Label.frame = CGRectMake(13, shopAddress1Label.frame.origin.y + shopAddress1Label.frame.size.height + 10, 200, 21);

    
    shopAddress2Label.text =  [NSString stringWithFormat:@"%@, %@",_objStoreList.storeStateName,_objStoreList.storePinCode];
    
    shopNumberLabel.text = _objStoreList.storeMobileNum;
    
    
    if(![_objStoreList.storeImageURL isEqual:[NSNull null]]){
    [shopImageView setImageWithURL:[NSURL URLWithString:_objStoreList.storeImageURL] placeholderImage:[UIImage imageNamed:@"deal_default.png"]completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
        UIImage *newImage = [UIImage imageWithImage:image  scaledToSize:CGSizeMake(320,212)];
        shopImageView.image = newImage;
    }usingActivityIndicatorStyle:(UIActivityIndicatorViewStyle)UIActivityIndicatorViewStyleGray];
    }
}

-(void)plotOnlyShopOnMap{
    
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[_objStoreList.storeLat floatValue] longitude:[_objStoreList.storeLong floatValue]  zoom:15.0];
    [_mapView animateToCameraPosition:camera];
    
    
    
    CLLocationCoordinate2D shopPosition = CLLocationCoordinate2DMake([_objStoreList.storeLat floatValue],[_objStoreList.storeLong floatValue]);
    
    shopMarker = [[GMSMarker alloc]init];
    shopMarker.position = shopPosition;
    shopMarker.icon = [UIImage imageNamed:@"pay1_store.png"];
    shopMarker.title = _objStoreList.storeShopName;
    shopMarker.snippet = [NSString stringWithFormat:@"%@, %@",_objStoreList.storeAreaName,_objStoreList.storeCityName];;
    shopMarker.map = _mapView;
    
    
    _mapView.selectedMarker = shopMarker;

}


-(void)plotShopOnMap{
    
    CLLocation *location = [CurrentLocation sharedLocation].currentLocation;

    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:location.coordinate.latitude longitude:location.coordinate.longitude  zoom:15.0];
    [_mapView animateToCameraPosition:camera];

    CLLocationCoordinate2D myPosition = CLLocationCoordinate2DMake(location.coordinate.latitude,location.coordinate.longitude);

    
    myMarker = [[GMSMarker alloc]init];
    myMarker.icon = [UIImage imageNamed:@"pay1_icon.png"];
    myMarker.position = myPosition;
    myMarker.flat = NO;
    myMarker.map = _mapView;
    
    CLLocationCoordinate2D shopPosition = CLLocationCoordinate2DMake([_objStoreList.storeLat floatValue],[_objStoreList.storeLong floatValue]);
                
    shopMarker = [[GMSMarker alloc]init];
    shopMarker.position = shopPosition;
    shopMarker.icon = [UIImage imageNamed:@"pay1_store.png"];
    shopMarker.title = _objStoreList.storeShopName;
    shopMarker.snippet = [NSString stringWithFormat:@"%@, %@",_objStoreList.storeAreaName,_objStoreList.storeCityName];;
    shopMarker.map = _mapView;
    
   
    _mapView.selectedMarker = shopMarker;
    
    
    NSString *userLoc = [NSString stringWithFormat:@"%f,%f",location.coordinate.latitude,location.coordinate.longitude];
    NSString *shopLoc = [NSString stringWithFormat:@"%@,%@",_objStoreList.storeLat,_objStoreList.storeLong];
    
    [_coordinates addObject:userLoc];
    [_coordinates addObject:shopLoc];
    
    NSLog(@"#Coordinates :%@ %lu",_coordinates,(unsigned long)_coordinates.count);
    
    NSString *sensor = @"false";
    NSArray *parameters = [NSArray arrayWithObjects:sensor, _coordinates,
                           nil];
    NSArray *keys = [NSArray arrayWithObjects:@"sensor", @"waypoints", nil];
    NSDictionary *query = [NSDictionary dictionaryWithObjects:parameters
                                                      forKeys:keys];
    MDDirectionService *mds=[[MDDirectionService alloc] init];
    SEL selector = @selector(addDirections:);
    [mds setDirectionsQuery:query withSelector:selector withDelegate:self];



}

- (void)addDirections:(NSDictionary *)json {
    
    [self hideHUDOnView];
    
    //  NSLog(@"jsonDict :%@",json);
    
    NSDictionary *routes = [json objectForKey:@"routes"][0];
    
    // NSLog(@"routesDict :%@",routes);
    
    
    NSDictionary *route = [routes objectForKey:@"overview_polyline"];
    // NSLog(@"routeDict :%@",route);
    
    
    NSString *overview_route = [route objectForKey:@"points"];
    // NSLog(@"overview_route :%@",overview_route);
    
    
    GMSPath *path = [GMSPath pathFromEncodedPath:overview_route];
    GMSPolyline *polyline = [GMSPolyline polylineWithPath:path];
    polyline.strokeWidth = 3;
    polyline.strokeColor = [UIColor blueColor];
    polyline.map = _mapView;
}



- (UIView *)mapView:(GMSMapView *)mapView markerInfoWindow:(GMSMarker *)marker {
    CustomInfoWindow *view =  [[[NSBundle mainBundle] loadNibNamed:@"CustomInfoWindow" owner:self options:nil] objectAtIndex:0];
    
    
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 10,view.frame.size.width - 10, 21)];
    
    if(marker == shopMarker){
   
        
        if([_objStoreList.storeImageURL isEqual:[NSNull null]]){
            view.frame = CGRectMake(10, 0,  150, 100);

            
            titleLabel.frame  = CGRectMake(10, 10,view.frame.size.width - 10, 21);
            titleLabel.text = marker.title;
            
            
            UILabel *addressLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, titleLabel.frame.origin.y + titleLabel.frame.size.height + 5,view.frame.size.width - 10, 60)];
            addressLabel.text = marker.snippet; //postTitle; //@"dfyudfhj";
            addressLabel.textColor = [UIColor lightGrayColor];
            addressLabel.font = [UIFont fontWithName:@"Open Sans" size:13.0];
            addressLabel.numberOfLines = 0;
            [addressLabel sizeToFit];
            //  [self setTextOnLabel:addressLabel fontSize:12.0];
            
            [view addSubview:addressLabel];
        }
        
        else{
            view.frame = CGRectMake(10, 0,  [UIScreen mainScreen].bounds.size.width - 20, 100);
            
            UIImageView *shopImgView = [[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 80, 60)];
            
            [shopImgView setImageWithURL:[NSURL URLWithString:_objStoreList.storeImageURL] placeholderImage:[UIImage imageNamed:@"deal_default.png"]completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
                UIImage *newImage = [UIImage imageWithImage:image  scaledToSize:CGSizeMake(320,212)];
                shopImgView.image = newImage;
            }usingActivityIndicatorStyle:(UIActivityIndicatorViewStyle)UIActivityIndicatorViewStyleGray];
            
            [view addSubview:shopImgView];
            
            
            UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(100, 10,view.frame.size.width - 10, 21)];
            
            if(![marker.title isEqual:[NSNull null]])
                titleLabel.text = marker.title;
            
            
            
            titleLabel.textColor = [UIColor whiteColor];
            [view addSubview:titleLabel];
            
            
            UILabel *addressLabel = [[UILabel alloc]initWithFrame:CGRectMake(100, titleLabel.frame.origin.y + titleLabel.frame.size.height + 5,view.frame.size.width - 110, 21)];
            addressLabel.text = marker.snippet;
            addressLabel.textColor = [UIColor lightGrayColor];
            addressLabel.font = [UIFont fontWithName:@"Open Sans" size:13.0];
            addressLabel.numberOfLines = 0;
            [addressLabel sizeToFit];

            
            [view addSubview:addressLabel];

 
        }
        
        
   
        
    }
    if(marker == myMarker)
    {
    view.frame = CGRectMake(10, 0,  150, 80);
    titleLabel.frame =  CGRectMake(10, 10,view.frame.size.width - 10, 21);
    titleLabel.text = @"You are here.";
    }

    
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.font = [UIFont fontWithName:@"Open Sans" size:13.0];
    titleLabel.numberOfLines = 0;
    [titleLabel sizeToFit];
    [view addSubview:titleLabel];
    
  /*  if(marker == shopMarker){
    UILabel *addressLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, titleLabel.frame.origin.y + titleLabel.frame.size.height + 5,view.frame.size.width - 10, 60)];
    addressLabel.text = marker.snippet; //postTitle; //@"dfyudfhj";
    addressLabel.textColor = [UIColor lightGrayColor];
    addressLabel.font = [UIFont fontWithName:@"Open Sans" size:13.0];
    addressLabel.numberOfLines = 0;
    [addressLabel sizeToFit];
  //  [self setTextOnLabel:addressLabel fontSize:12.0];
    
    [view addSubview:addressLabel];
    }*/
    
    
    return view;
}

-(void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)marker{
    NSLog(@"TappedMarker %@",marker.title);
    
    _mapView.hidden = TRUE;
    shopInfoView.hidden = FALSE;
    shopHeaderView.frame = CGRectMake(0, 64, self.view.frame.size.width, 70);

    
    
}

-(IBAction)phoneButtonClick:(id)sender{
    NSLog(@"Phone");
    
    [Constant makePhoneCall:_objStoreList.storeMobileNum];

}

-(IBAction)addressButtonClick:(id)sender{
    NSLog(@"Address");
    
    [_mapView clear];
    
    _mapView.hidden = FALSE;
    shopInfoView.hidden = TRUE;
    
    
    _mapView.frame = CGRectMake(0, 70, self.view.frame.size.width, self.view.frame.size.height - 140);
    shopHeaderView.frame = CGRectMake(0, _mapView.frame.origin.y + _mapView.frame.size.height, self.view.frame.size.width, 70);
    
    [self plotShopOnMap];
    
    
}
-(void)backClicked:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
