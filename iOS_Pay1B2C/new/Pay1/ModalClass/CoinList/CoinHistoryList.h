//
//  CoinHistoryList.h
//  Pay1
//
//  Created by webninjaz on 09/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CoinHistoryList : NSObject

@property (nonatomic, strong) NSString *coinName;
@property (nonatomic, strong) NSString *coinCoinsValue;
@property (nonatomic, strong) NSString *coinMessage;
@property (nonatomic, strong) NSString *coinOperator;
@property (nonatomic, strong) NSString *coinRechargeType;
@property (nonatomic, strong) NSString *coinRedeemStatus;
@property (nonatomic, strong) NSString *coinTransactionCategory;
@property (nonatomic, strong) NSString *coinTransactionDateTime;
@property (nonatomic, strong) NSString *coinTransactionID;
@property (nonatomic, strong) NSString *coinTransactionAmount;


@property (nonatomic, strong) NSMutableArray *coinDataArray;


@end
