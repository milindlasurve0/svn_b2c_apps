﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Globalization;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Windows.Media;
using NewPay1B2C.UtilityClass;

namespace NewPay1B2C
    {
    public partial class Profile : PhoneApplicationPage
        {
        private DatePickerCustom datePicker;
        string genderType;
        ProgressBarManager ProgressBarManager;
        public Profile ()
            {
            InitializeComponent ();
			
            GoogleAnalytics.EasyTracker.GetTracker ().SendView ("Profile");
            Constant.laodNavigationBar(TitlePanel, LayoutRoot, canvas,"Home");
            Loaded += new RoutedEventHandler (ProfilePage_Loaded);
            updateProfileLayout ();
            ProgressIndicator prog = new ProgressIndicator ();
            ProgressBarManager = new ProgressBarManager ();
            SystemTray.SetProgressIndicator (this, prog);
            ProgressBarManager.Hook (prog);
            }

        private void updateProfileLayout ()
            {
            stackEditName.Visibility = Visibility.Collapsed;
            textProfileName.Visibility = Visibility.Visible;
            textProfileMobile.Text = Constant.getUserNumber();
            textProfileEmail.Text = Constant.LoadPersistent<string> (Constant.SETTINGS_USER_EMAIL);

            string pinn = Constant.LoadPersistent<string> (Constant.SETTINGS_USER_PIN);
            string samplePin = "";
            for (int i = 1; i <= pinn.Length; i++)
                {
                samplePin = samplePin + "•";
                }
            textProfilePin.Text = samplePin;
            //textProfilePin.IsEnabled = false;
            DateTime time = DateTime.Parse (Constant.LoadPersistent<string> (Constant.SETTINGS_USER_DOB));
            string formattedDate = time.ToString ("dd-MMM-yyyy");
            textProfileDOB.Text = formattedDate;

            textProfileName.Text = Constant.LoadPersistent<string> (Constant.SETTINGS_USER_NAME);
            string gender1 = Constant.LoadPersistent<string> (Constant.SETTINGS_USER_GENDER);
            if (gender1.Equals ("m"))
                {
                textProfileGender.Text = "Male";
                maleBtn.Background = new SolidColorBrush (Constant.ConvertStringToColor ("#09B8C4"));
                maleBtn.Foreground = new SolidColorBrush (Colors.White);
                femaleBtn.Background = new SolidColorBrush (Colors.White);
                femaleBtn.Foreground = new SolidColorBrush (Constant.ConvertStringToColor ("#503D97"));

                }
            else if (gender1.Equals ("f"))
                {
                textProfileGender.Text = "Female";
                femaleBtn.Background = new SolidColorBrush (Constant.ConvertStringToColor ("#09B8C4"));
                femaleBtn.Foreground = new SolidColorBrush (Colors.White);
                maleBtn.Background = new SolidColorBrush (Colors.White);
                maleBtn.Foreground = new SolidColorBrush (Constant.ConvertStringToColor ("#503D97"));
                }


            }

		protected override void OnBackKeyPress (System.ComponentModel.CancelEventArgs e)
			{
			//Do your work here
			base.OnBackKeyPress (e);
			NavigationService.Navigate (new Uri ("/HomePage.xaml?", UriKind.RelativeOrAbsolute));
			while (this.NavigationService.BackStack.Any ())
				{
				this.NavigationService.RemoveBackEntry ();
				}
			}

        void ProfilePage_Loaded (object sender, RoutedEventArgs e)
            {
            // create datePicker programmatically
            if (this.datePicker == null)
                {
                this.datePicker = new DatePickerCustom ();
                this.datePicker.IsTabStop = false;
                this.datePicker.MaxHeight = 0;

                this.datePicker.ValueChanged += new EventHandler<DateTimeValueChangedEventArgs> (datePicker_ValueChanged);

                LayoutRoot.Children.Add (this.datePicker);

                }
            }

        void datePicker_ValueChanged (object sender, DateTimeValueChangedEventArgs e)
            {
            // now we may use got value from datePicker

            string dd = this.datePicker.ValueString;
            string[] date = dd.Split ('-');
            string day = date[0];
            string mon = date[1];
            string year = date[2];
            DateTime time = DateTime.Parse (dd);
            string formattedDate = time.ToString ("yyyy-MM-dd");


            Task<string> dobUpdate = editDOBTask (formattedDate, Constant.LoadPersistent<string> (Constant.SETTINGS_USER_NAME), Constant.LoadPersistent<string> (Constant.SETTINGS_USER_GENDER));
            //textProfileDOB.Text = formattedDate;

            }

        private async Task<string> editDOBTask (string date_of_birth, string userName, string userGender)
            {
            ProgressBarManager.Show (17, "Please wait...", false, 10);
            var values = new List<KeyValuePair<string, string>> ();
            values.Add (new KeyValuePair<string, string> ("actiontype", "update_profile"));
            values.Add (new KeyValuePair<string, string> ("date_of_birth", date_of_birth));
            values.Add (new KeyValuePair<string, string> ("gender", userGender));
            values.Add (new KeyValuePair<string, string> ("name", userName));
            values.Add (new KeyValuePair<string, string> ("api", "true"));
            FormUrlEncodedContent values1 = new FormUrlEncodedContent (values);
            Task<string> response = Constant.postParamsAndgetResponse (Constant.B2C_URL, values1);

            string result = await response;
            if (string.IsNullOrEmpty (result))
                {
                NavigationService.Navigate (new Uri ("/NoConnection.xaml?", UriKind.Relative));
                }
            else
                {
                var dict = (JObject)JsonConvert.DeserializeObject (result);
                string status = dict["status"].ToString ();
                var description = dict["description"];
                if (status.Equals ("success"))
                    {
                    Constant.SavePersistent (Constant.SETTINGS_USER_NAME, userName);

                    Constant.SavePersistent (Constant.SETTINGS_USER_GENDER, userGender);
                    Constant.SavePersistent (Constant.SETTINGS_USER_DOB, date_of_birth);
                    MessageBox.Show (description.ToString ());
                    updateProfileLayout ();

                    }
                else
                    {
					Constant.checkForErrorCode (result); //MessageBox.Show (description.ToString ());
                    }
                }
            ProgressBarManager.Hide (17);
            return "aaa";
            }

        private void editNameClick (object sender, RoutedEventArgs e)
            {
            stackEditName.Visibility = Visibility.Visible;
            textProfileName.Visibility = Visibility.Collapsed;

            textBoxProfileName.Text = textProfileName.Text;

            }

       

        private void editDOBClick (object sender, RoutedEventArgs e)
            {

            datePicker.ClickTemplateButton ();
            }

        private void buttonImage_Click_Back (object sender, RoutedEventArgs e)
            {
            NavigationService.GoBack ();
            }
        private void buttonEditNameClick (object sender, RoutedEventArgs e)
            {
            if (textBoxProfileName.Text.Length != 0)
                {
                Task<string> dobUpdate = editDOBTask (Constant.LoadPersistent<string> (Constant.SETTINGS_USER_DOB), textBoxProfileName.Text, Constant.LoadPersistent<string> (Constant.SETTINGS_USER_GENDER));
                }
            }
        private void buttonEditDOBClick (object sender, RoutedEventArgs e)
            {
            }

        private void buttonEditDOBClickCancel (object sender, RoutedEventArgs e)
            {

            }
        private void buttonEditNameClickCancel (object sender, RoutedEventArgs e)
            {
            stackEditName.Visibility = Visibility.Collapsed;
            textProfileName.Visibility = Visibility.Visible;
            }

        private void buttonGenderClick (object sender, RoutedEventArgs e)
            {
            Task<string> dobUpdate = editDOBTask (Constant.LoadPersistent<string> (Constant.SETTINGS_USER_DOB), textBoxProfileName.Text, genderType);

            }
        private void buttonGenderClickCancel (object sender, RoutedEventArgs e)
            {
            stackEditGender.Visibility = Visibility.Collapsed;
            textProfileGender.Visibility = Visibility.Visible;
            }

        private void editGenderClick (object sender, RoutedEventArgs e)
            {
            stackEditGender.Visibility = Visibility.Visible;
            textProfileGender.Visibility = Visibility.Collapsed;
            }

        private void editEmailClick (object sender, RoutedEventArgs e)
            {
            NavigationService.Navigate (new Uri ("/UpdateEmail.xaml?", UriKind.Relative));

            NavigationService.GoBack ();
            }

        private void editPinClick (object sender, RoutedEventArgs e)
            {
			PhoneApplicationService.Current.State["updateFromLogin"] = 1;
			ForgotPasswordTask ();
            }

		private async void ForgotPasswordTask ()
			{
			var values = new List<KeyValuePair<string, string>>
                        { 
                        new KeyValuePair<string, string>("actiontype", "forgotpwd"),
                        new KeyValuePair<string, string>("user_name",Constant.getUserNumber()),
                        new KeyValuePair<string, string>("vtype", "1"),
						new KeyValuePair<string, string>("api","true"),
                        };

			FormUrlEncodedContent values1 = new FormUrlEncodedContent (values);
			Task<string> response = Constant.postParamsAndgetResponse (Constant.B2C_URL, values1);
			string result = await response;
			if (string.IsNullOrEmpty (result))
				{
				NavigationService.Navigate (new Uri ("/NoConnection.xaml?", UriKind.RelativeOrAbsolute));
				}
			else
				{
				var dict = (JObject)JsonConvert.DeserializeObject (result);
				string status = dict["status"].ToString ();
				if (status.Equals ("success"))
					{
					PhoneApplicationService.Current.State["updateFromLogin"] = 2;
					NavigationService.Navigate (new Uri ("/UpdatePassword.xaml?", UriKind.RelativeOrAbsolute));
					}
				else
					{
					Constant.checkForErrorCode (result);
					}
				}
			}

        private void Male_Click (object sender, RoutedEventArgs e)
            {
            maleBtn.Background = new SolidColorBrush (Constant.ConvertStringToColor ("#09B8C4"));
            maleBtn.Foreground = new SolidColorBrush (Colors.White);
            femaleBtn.Background = new SolidColorBrush (Colors.White);
            femaleBtn.Foreground = new SolidColorBrush (Constant.ConvertStringToColor ("#503D97"));
            genderType = "m";
            }

        private void Female_Click (object sender, RoutedEventArgs e)
            {
            femaleBtn.Background = new SolidColorBrush (Constant.ConvertStringToColor ("#09B8C4"));
            femaleBtn.Foreground = new SolidColorBrush (Colors.White);
            maleBtn.Background = new SolidColorBrush (Colors.White);
            maleBtn.Foreground = new SolidColorBrush (Constant.ConvertStringToColor ("#503D97"));
            genderType = "f";
            }
        }
    }