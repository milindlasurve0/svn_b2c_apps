package com.pay1.databasehandler;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Pay1B2BSQLiteHelper extends SQLiteOpenHelper {

	public static final String DATABASE_NAME = "pay1b2bdatabase.db";
	private static final int DATABASE_VERSION = 3;

	public static final String TABLE_PLAN = "PlanTable";

	public static final String PLAN_ID = "p_id";
	public static final String PLAN_OPERATOR_ID = "operator_id";
	public static final String PLAN_OPERATOR_NAME = "operator_name";
	public static final String PLAN_CIRCLE_ID = "circle_id";
	public static final String PLAN_CIRCLE_NAME = "circle_name";
	public static final String PLAN_TYPE = "plan_type";
	public static final String PLAN_AMOUNT = "amount";
	public static final String PLAN_VALIDITY = "validity";
	public static final String PLAN_DESCRIPTION = "description";
	public static final String PLAN_UPDATE_TIME = "time";
	public static final String PLAN_SHOW_FLAG = "show_flag";

	private final String CREATE_PLAN = "CREATE TABLE " + TABLE_PLAN + " ( "
			+ PLAN_ID + " integer primary key autoincrement, "
			+ PLAN_OPERATOR_ID + " text, " + PLAN_OPERATOR_NAME + " text, "
			+ PLAN_CIRCLE_ID + " text, " + PLAN_CIRCLE_NAME + " text, "
			+ PLAN_TYPE + " text, " + PLAN_AMOUNT + " integer, "
			+ PLAN_VALIDITY + " text, " + PLAN_DESCRIPTION + " text, "
			+ PLAN_UPDATE_TIME + " text, " + PLAN_SHOW_FLAG + " integer );";

	public Pay1B2BSQLiteHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL(CREATE_PLAN);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		//db.execSQL("DROP TABLE IF EXISTS " + TABLE_PLAN);
	}

}
