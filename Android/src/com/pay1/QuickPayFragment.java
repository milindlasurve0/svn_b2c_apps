package com.pay1;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.pay1.customviews.MyProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fortysevendeg.swipelistview.BaseSwipeListViewListener;
import com.fortysevendeg.swipelistview.SwipeListView;
import com.pay1.adapterhandler.ItemAdapter;
import com.pay1.constants.Constants;
import com.pay1.requesthandler.RequestClass;

public class QuickPayFragment extends Fragment {

	private final String TAG = "Quick Pay";

	private ImageView imageView_Add;
	private SwipeListView swipelistview;
	ItemAdapter adapter;
	private ArrayList<JSONObject> data;
	TextView textView_NoData;
	SwipeDetector swipeDetector;
	int tag;
	public static final String NAME = "name";
	public static final String NUMBER = "number";
	public static final String AMOUNT = "amount";

	public static QuickPayFragment newInstance() {
		QuickPayFragment fragment = new QuickPayFragment();
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View vi = inflater
				.inflate(R.layout.quickpay_fragment, container, false);

		try {

			swipelistview = (SwipeListView) vi
					.findViewById(R.id.example_swipe_lv_list);
			swipelistview.setSwipeMode(SwipeListView.SWIPE_MODE_RIGHT);
			swipeDetector = new SwipeDetector();

			imageView_Add = (ImageView) vi.findViewById(R.id.imageView_Add);

			imageView_Add.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

				}
			});

			new GetQuickPayList().execute();
			data = new ArrayList<JSONObject>();
			// swipelistview.setEmptyView(textView_NoData);

			adapter = new ItemAdapter(getActivity(), R.layout.custom_row_new,
					data, listener);
			// These are the swipe listview settings. you can change these
			// setting as your requirement
			swipelistview.setSwipeMode(SwipeListView.SWIPE_MODE_BOTH);
			swipelistview
					.setSwipeActionLeft(SwipeListView.SWIPE_ACTION_DISMISS);
			swipelistview
					.setSwipeActionRight(SwipeListView.SWIPE_ACTION_REVEAL);
			swipelistview.setOffsetLeft(convertDpToPixel(0f)); // left side
																// offset
			swipelistview.setOffsetRight(convertDpToPixel(80f)); // right side
																	// offset
			swipelistview.setAnimationTime(500); // Animation time
			swipelistview.setSwipeOpenOnLongPress(true); // enable or disable
															// SwipeOpenOnLongPress

			swipelistview.setAdapter(adapter);

			swipelistview.setAdapter(adapter);

			/*
			 * swipelistview.setOnTouchListener(swipeDetector);
			 * swipelistview.setOnItemClickListener(listener);
			 */

			swipelistview
					.setSwipeListViewListener(new BaseSwipeListViewListener() {
						@Override
						public void onOpened(int position, boolean toRight) {
						}

						@Override
						public void onClosed(int position, boolean fromRight) {
							// Toast.makeText(getActivity(),
							// "Test 1111   "+fromRight,
							// Toast.LENGTH_SHORT).show();
						}

						@Override
						public void onListChanged() {
						}

						@Override
						public void onMove(int position, float x) {
						}

						@Override
						public void onStartOpen(int position, int action,
								boolean right) {
							Log.d("swipe", String.format(
									"onStartOpen %d - action %d", position,
									action));
							/*
							 * if(right){ Toast.makeText(getActivity(),
							 * "Test "+right, Toast.LENGTH_SHORT).show(); }else{
							 * 
							 * }
							 */
						}

						@Override
						public void onStartClose(int position, boolean right) {
							Log.d("swipe",
									String.format("onStartClose %d", position));
						}

						@Override
						public void onClickFrontView(int position) {
							Log.d("swipe", String.format("onClickFrontView %d",
									position));

							// swipelistview.openAnimate(position); //when you
							// touch front
							// view it will open

						}

						@Override
						public void onClickBackView(int position) {
							Log.d("swipe", String.format("onClickBackView %d",
									position));

							swipelistview.closeAnimate(position);// when you
																	// touch
																	// back view
																	// it will
																	// close
						}

						@Override
						public void onDismiss(int[] reverseSortedPositions) {

						}

					});
		} catch (Exception e) {
		}

		return vi;
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
	}

	public int convertDpToPixel(float dp) {
		DisplayMetrics metrics = getResources().getDisplayMetrics();
		float px = dp * (metrics.densityDpi / 160f);
		return (int) px;
	}

	public class GetQuickPayList extends AsyncTask<String, String, String>
			implements OnDismissListener {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String url = Constants.B2C_URL + "get_quickpaylist/?";
			String response = RequestClass.getInstance().readPay1B2CRequest(
					getActivity(), url);
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			if (!result.startsWith("Error")) {

				String status;
				try {
					JSONObject jsonObject = new JSONObject(result);
					status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {
						JSONArray description = jsonObject
								.getJSONArray("description");
						for (int j = 0; j < description.length(); j++) {
							JSONObject jObject = description.getJSONObject(j);
							data.add(jObject);
						}

						adapter.notifyDataSetChanged();

					} else {
						Constants.showOneButtonDialog(getActivity(), TAG,
								Constants.checkCode(result),
								Constants.DIALOG_CLOSE);
					}

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(getActivity());
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							GetQuickPayList.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			GetQuickPayList.this.cancel(true);
			dialog.cancel();
		}

	}

	private OnClickListener listener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			tag = (Integer) v.getTag();
			swipelistview.closeAnimate(tag);
			Intent intent = new Intent(getActivity(),
					EditQuickPayActivity.class);
			intent.putExtra(Constants.EDIT_DATA, data.get(tag).toString());
			startActivity(intent);
			/*
			 * Toast.makeText(getActivity(), "" + tag, Toast.LENGTH_SHORT)
			 * .show();
			 */
			// swipelistview.closeAnimate(tag);
			/*
			 * try { final String id = data.get(tag).getString("id"); new
			 * DeleteQuickTask().execute(id); } catch (JSONException e) { //
			 * TODO Auto-generated catch block e.printStackTrace(); }
			 */

		}
	};

	public class DeleteQuickTask extends AsyncTask<String, String, String>
			implements OnDismissListener {
		private MyProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String url = Constants.B2C_URL + "del_quickpay/?id=" + params[0];
			String response = RequestClass.getInstance().readPay1B2CRequest(
					getActivity(), url);
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			if (!result.startsWith("Error")) {

				String status;
				try {
					JSONObject jsonObject = new JSONObject(result);
					status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {

						Constants.showOneButtonDialog(getActivity(),
								"Quick Pay",
								jsonObject.getString("description"),
								Constants.DIALOG_CLOSE);
						// swipelistview.removeViewAt(tag);
						swipelistview.closeAnimate(tag);
						data.remove(tag);
						adapter.notifyDataSetChanged();

					} else {
						Constants.showOneButtonDialog(getActivity(), TAG,
								Constants.checkCode(result),
								Constants.DIALOG_CLOSE);
					}

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		}

		@Override
		protected void onPreExecute() {
			dialog = new MyProgressDialog(getActivity());
			// this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							DeleteQuickTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			DeleteQuickTask.this.cancel(true);
			dialog.cancel();
		}

	}

	/*
	 * OnItemClickListener listener = new OnItemClickListener() {
	 * 
	 * @Override public void onItemClick(AdapterView<?> arg0, View arg1, int
	 * arg2, long arg3) { // arg1.startDrag(data, shadowBuilder, myLocalState,
	 * flags) if (swipeDetector.swipeDetected()) { if (swipeDetector.getAction()
	 * == Action.RL) { Toast.makeText(SampleQuick.this, "rIGHT tO lEFT",
	 * Toast.LENGTH_LONG).show(); } else { Toast.makeText(SampleQuick.this,
	 * "lEFT to rIGHT ", Toast.LENGTH_LONG).show(); } } else {
	 * Toast.makeText(SampleQuick.this, "Click", Toast.LENGTH_LONG) .show(); } }
	 * };
	 */

}
