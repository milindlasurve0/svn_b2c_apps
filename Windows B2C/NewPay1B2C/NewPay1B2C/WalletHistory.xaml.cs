﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using NewPay1B2C.UserControls;
using NewPay1B2C.UtilityClass;

namespace NewPay1B2C
    {
    public partial class WalletHistory : PhoneApplicationPage
        {
        ProgressBarManager ProgressBarManager;
        List<string> walletList;
        public WalletHistory ()
            {
            InitializeComponent ();
			 
            GoogleAnalytics.EasyTracker.GetTracker ().SendView ("Wallet History");
            Constant.laodNavigationBar (TitlePanel, LayoutRoot, canvas, "Wallet History");
           
            walletList = new List<string> ();
            ProgressIndicator prog = new ProgressIndicator ();
            ProgressBarManager = new ProgressBarManager ();
            SystemTray.SetProgressIndicator (this, prog);
            ProgressBarManager.Hook (prog);
            getwalletTask (0);


            }


		protected override void OnBackKeyPress (System.ComponentModel.CancelEventArgs e)
			{
			//Do your work here
			base.OnBackKeyPress (e);
			NavigationService.Navigate (new Uri ("/HomePage.xaml?", UriKind.RelativeOrAbsolute));
			while (this.NavigationService.BackStack.Any ())
				{
				this.NavigationService.RemoveBackEntry ();
				}
			}

        private async void getwalletTask (int page)
            {
            ProgressBarManager.Show (26, "Please wait...", false, 10);
            if (walletListBox.Items.Count > 0)
                {
                walletListBox.Items.RemoveAt (walletListBox.Items.Count - 1);
                }

            var values = new List<KeyValuePair<string, string>> ();
            values.Add (new KeyValuePair<string, string> ("actiontype", "wallet_history"));
            values.Add (new KeyValuePair<string, string> ("page", Convert.ToString (page)));
            values.Add (new KeyValuePair<string, string> ("limit", "10"));
            values.Add (new KeyValuePair<string, string> ("api", "true"));
            FormUrlEncodedContent values1 = new FormUrlEncodedContent (values);
            Task<string> response = Constant.postParamsAndgetResponse (Constant.B2C_URL, values1);

            string result = await response;
            if (string.IsNullOrEmpty (result))
                {
                NavigationService.Navigate (new Uri ("/NoConnection.xaml?", UriKind.Relative));
                }
            else
                {
                var dict = (JObject)JsonConvert.DeserializeObject (result);
                string status = dict["status"].ToString ();

				if (status.Equals ("success"))
					{
					var description = dict["description"];
					foreach (var item in description)
						{
						walletList.Add (item.ToString ());
						WalletHistoryControlxaml walletControl = new WalletHistoryControlxaml ();
						string vIn = item["transaction_amount"].ToString ();
						//decimal vOut = Convert.ToDecimal(vIn);
						double vOut = Convert.ToDouble (vIn);
						walletControl.textWalletAmount.Text = "Rs. " + vOut;
						walletControl.textWalletContactNumber.Text = item["number"].ToString ();

						DateTime time1 = DateTime.Parse (item["trans_datetime"].ToString ());
						string time = time1.ToString ("hh:mm tt");
						DateTime date = DateTime.Parse (item["trans_datetime"].ToString ());
						string formattedDate = date.ToString ("dd-MMM-yyyy");
						walletControl.textWalletDate.Text = formattedDate + " " + time;
						walletControl.textWalletClosing.Text = "Closing balance Rs. " + item["closing_bal"].ToString ();

						string service_id = item["service_id"].ToString ();

						if (service_id.Equals ("0"))
							{
							walletControl.textWalletOperator.Text = "Transaction reversed";
							}
						else if (service_id.Equals ("1"))
							{
							walletControl.textWalletOperator.Text = "Prepaid recharge";
							}
						else if (service_id.Equals ("2"))
							{
							walletControl.textWalletOperator.Text = "DTH recharge";
							}
						else if (service_id.Equals ("4"))
							{
							walletControl.textWalletOperator.Text = "Postpaid bill";
							}
						else if (service_id.Equals ("5"))
							{
							walletControl.textWalletOperator.Text = "Datacard recharge";
							}
						else if (service_id.Equals ("98"))
							{
							walletControl.textWalletOperator.Text = "Wallet topup";// item["operator_name"].ToString();
							}
						else if (service_id.Equals ("99"))
							{
							walletControl.textWalletOperator.Text = item["trans_category"].ToString ();
							}

						// String operatorImageName = OperatorConstant.getOperatorResource (Convert.ToInt16 (item["product_id"]));
						// walletControl.imageType.Source = new BitmapImage (new Uri ("/Pay1Images/" + operatorImageName + ".png", UriKind.RelativeOrAbsolute));
						walletControl.imageType.Source = OperatorConstant.getOperatorResource (Convert.ToInt16 (item["product_id"]));

						string trans_type = item["trans_type"].ToString ();
						if (trans_type.Equals ("0"))
							{
							walletControl.textWalletClosing.Foreground = new SolidColorBrush (Colors.Red);
							}
						else
							{
							walletControl.textWalletClosing.Foreground = new SolidColorBrush (Colors.Green);
							}



						walletListBox.Items.Add (walletControl);
						}
					Button bt = new Button ();
					bt.Content = "Load more history";
					bt.Foreground = new SolidColorBrush (Colors.White);
					bt.Background = new SolidColorBrush (Colors.Blue);
					bt.Width = 480;
					if ((walletListBox.Items.Count % 10) == 0)
						{
						bt.Click += (s, e) =>
						{

							getwalletTask (page + 1);
						};

						walletListBox.Items.Add (bt);
						}
					}
				else
					{
					Constant.checkForErrorCode (result);
					}
                }
            ProgressBarManager.Hide (26);
            }


        }
    }