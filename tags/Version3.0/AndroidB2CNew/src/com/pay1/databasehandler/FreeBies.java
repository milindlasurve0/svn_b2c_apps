package com.pay1.databasehandler;

import java.io.Serializable;

public class FreeBies implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int freebieID;
	private int freebieDealID;
	private String freebieDealName;
	private String freebieURL;
	private String freebieCategory;
	private int freebieCategoryID;
	private double freebieLat;
	private double freebieLng;
	private String freebieAddress;
	private String freebieArea;
	private String freebieCity;
	private String freebieState;
	private int freebiePin;
	private int freebieLocationCount;
	private int freebieOfferID;
	private String freebieOfferName;
	private String freebieValidity;
	private String freebieShortDesc;
	private int freebieMinAmount;
	private long freebieUptateTime;
	private int freebieLike;
	private String freebieCode;
	private int freebieCouponStatusID;
	private String freebieCouponStatusCode;
	private String freebieExpiry;
	private String freebieLogoURL;
	private String freebieDealerMobile;
	private int freebieRatings;
	private int price;
	private String pin;
	private int freeBieByVoucher;
	private int freeBieOfferPrice;
	/**
	 * @return the pin
	 */
	public String getPin() {
		return pin;
	}

	/**
	 * @param pin the pin to set
	 */
	public void setPin(String pin) {
		this.pin = pin;
	}

	
	
	/**
	 * @return the price
	 */
	public int getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(int price) {
		this.price = price;
	}

	/**
	 * @return the freeBieOfferPrice
	 */
	public int getFreeBieOfferPrice() {
		return freeBieOfferPrice;
	}

	/**
	 * @param freeBieOfferPrice the freeBieOfferPrice to set
	 */
	public void setFreeBieOfferPrice(int freeBieOfferPrice) {
		this.freeBieOfferPrice = freeBieOfferPrice;
	}

	/**
	 * @return the freeBieByVoucher
	 */
	public int getFreeBieByVoucher() {
		return freeBieByVoucher;
	}

	/**
	 * @param freeBieByVoucher the freeBieByVoucher to set
	 */
	public void setFreeBieByVoucher(int freeBieByVoucher) {
		this.freeBieByVoucher = freeBieByVoucher;
	}

	

	/**
	 * @return the freebieID
	 */
	public int getFreebieID() {
		return freebieID;
	}

	/**
	 * @param freebieID
	 *            the freebieID to set
	 */
	public void setFreebieID(int freebieID) {
		this.freebieID = freebieID;
	}
	
	/**
	 * @return the freebieID
	 */
	public int getprice() {
		return price;
	}

	/**
	 * @param freebieID
	 *            the freebieID to set
	 */
	public void setprice(int price) {
		this.price = price;
	}

	/**
	 * @return the freebieDealID
	 */
	public int getFreebieDealID() {
		return freebieDealID;
	}

	/**
	 * @param freebieDealID
	 *            the freebieDealID to set
	 */
	public void setFreebieDealID(int freebieDealID) {
		this.freebieDealID = freebieDealID;
	}

	/**
	 * @return the freebieDealName
	 */
	public String getFreebieDealName() {
		return freebieDealName;
	}

	/**
	 * @param freebieDealName
	 *            the freebieDealName to set
	 */
	public void setFreebieDealName(String freebieDealName) {
		this.freebieDealName = freebieDealName;
	}

	/**
	 * @return the freebieURL
	 */
	public String getFreebieURL() {
		return freebieURL;
	}

	/**
	 * @param freebieURL
	 *            the freebieURL to set
	 */
	public void setFreebieURL(String freebieURL) {
		this.freebieURL = freebieURL;
	}

	/**
	 * @return the freebieCategory
	 */
	public String getFreebieCategory() {
		return freebieCategory;
	}

	/**
	 * @param freebieCategory
	 *            the freebieCategory to set
	 */
	public void setFreebieCategory(String freebieCategory) {
		this.freebieCategory = freebieCategory;
	}

	/**
	 * @return the freebieCategoryID
	 */
	public int getFreebieCategoryID() {
		return freebieCategoryID;
	}

	/**
	 * @param freebieCategoryID
	 *            the freebieCategoryID to set
	 */
	public void setFreebieCategoryID(int freebieCategoryID) {
		this.freebieCategoryID = freebieCategoryID;
	}

	/**
	 * @return the freebieLat
	 */
	public double getFreebieLat() {
		return freebieLat;
	}

	/**
	 * @param freebieLat
	 *            the freebieLat to set
	 */
	public void setFreebieLat(double freebieLat) {
		this.freebieLat = freebieLat;
	}

	/**
	 * @return the freebieLng
	 */
	public double getFreebieLng() {
		return freebieLng;
	}

	/**
	 * @param freebieLng
	 *            the freebieLng to set
	 */
	public void setFreebieLng(double freebieLng) {
		this.freebieLng = freebieLng;
	}

	/**
	 * @return the freebieAddress
	 */
	public String getFreebieAddress() {
		return freebieAddress;
	}

	/**
	 * @param freebieAddress
	 *            the freebieAddress to set
	 */
	public void setFreebieAddress(String freebieAddress) {
		this.freebieAddress = freebieAddress;
	}

	/**
	 * @return the freebieArea
	 */
	public String getFreebieArea() {
		return freebieArea;
	}

	/**
	 * @param freebieArea
	 *            the freebieArea to set
	 */
	public void setFreebieArea(String freebieArea) {
		this.freebieArea = freebieArea;
	}

	/**
	 * @return the freebieCity
	 */
	public String getFreebieCity() {
		return freebieCity;
	}

	/**
	 * @param freebieCity
	 *            the freebieCity to set
	 */
	public void setFreebieCity(String freebieCity) {
		this.freebieCity = freebieCity;
	}

	/**
	 * @return the freebieState
	 */
	public String getFreebieState() {
		return freebieState;
	}

	/**
	 * @param freebieState
	 *            the freebieState to set
	 */
	public void setFreebieState(String freebieState) {
		this.freebieState = freebieState;
	}

	/**
	 * @return the freebiePin
	 */
	public int getFreebiePin() {
		return freebiePin;
	}

	/**
	 * @param freebiePin
	 *            the freebiePin to set
	 */
	public void setFreebiePin(int freebiePin) {
		this.freebiePin = freebiePin;
	}

	/**
	 * @return the freebieLocationCount
	 */
	public int getFreebieLocationCount() {
		return freebieLocationCount;
	}

	/**
	 * @param freebieLocationCount
	 *            the freebieLocationCount to set
	 */
	public void setFreebieLocationCount(int freebieLocationCount) {
		this.freebieLocationCount = freebieLocationCount;
	}

	/**
	 * @return the freebieOfferID
	 */
	public int getFreebieOfferID() {
		return freebieOfferID;
	}

	/**
	 * @param freebieOfferID
	 *            the freebieOfferID to set
	 */
	public void setFreebieOfferID(int freebieOfferID) {
		this.freebieOfferID = freebieOfferID;
	}

	/**
	 * @return the freebieOfferName
	 */
	public String getFreebieOfferName() {
		return freebieOfferName;
	}

	/**
	 * @param freebieOfferName
	 *            the freebieOfferName to set
	 */
	public void setFreebieOfferName(String freebieOfferName) {
		this.freebieOfferName = freebieOfferName;
	}

	/**
	 * @return the freebieValidity
	 */
	public String getFreebieValidity() {
		return freebieValidity;
	}

	/**
	 * @param freebieValidity
	 *            the freebieValidity to set
	 */
	public void setFreebieValidity(String freebieValidity) {
		this.freebieValidity = freebieValidity;
	}

	/**
	 * @return the freebieShortDesc
	 */
	public String getFreebieShortDesc() {
		return freebieShortDesc;
	}

	/**
	 * @param freebieShortDesc
	 *            the freebieShortDesc to set
	 */
	public void setFreebieShortDesc(String freebieShortDesc) {
		this.freebieShortDesc = freebieShortDesc;
	}

	/**
	 * @return the freebieMinAmount
	 */
	public int getFreebieMinAmount() {
		return freebieMinAmount;
	}

	/**
	 * @param freebieMinAmount
	 *            the freebieMinAmount to set
	 */
	public void setFreebieMinAmount(int freebieMinAmount) {
		this.freebieMinAmount = freebieMinAmount;
	}

	/**
	 * @return the freebieUptateTime
	 */
	public long getFreebieUptateTime() {
		return freebieUptateTime;
	}

	/**
	 * @param freebieUptateTime
	 *            the freebieUptateTime to set
	 */
	public void setFreebieUptateTime(long freebieUptateTime) {
		this.freebieUptateTime = freebieUptateTime;
	}

	/**
	 * @return the freebieLike
	 */
	public final int getFreebieLike() {
		return freebieLike;
	}

	/**
	 * @param freebieLike
	 *            the freebieLike to set
	 */
	public final void setFreebieLike(int freebieLike) {
		this.freebieLike = freebieLike;
	}

	/**
	 * @return the freebieCode
	 */
	public final String getFreebieCode() {
		return freebieCode;
	}

	/**
	 * @param freebieCode
	 *            the freebieCode to set
	 */
	public final void setFreebieCode(String freebieCode) {
		this.freebieCode = freebieCode;
	}

	/**
	 * @return the freebieCouponStatusID
	 */
	public final int getFreebieCouponStatusID() {
		return freebieCouponStatusID;
	}

	/**
	 * @param freebieCouponStatusID
	 *            the freebieCouponStatusID to set
	 */
	public final void setFreebieCouponStatusID(int freebieCouponStatusID) {
		this.freebieCouponStatusID = freebieCouponStatusID;
	}

	/**
	 * @return the freebieCouponStatusCode
	 */
	public final String getFreebieCouponStatusCode() {
		return freebieCouponStatusCode;
	}

	/**
	 * @param freebieCouponStatusCode
	 *            the freebieCouponStatusCode to set
	 */
	public final void setFreebieCouponStatusCode(String freebieCouponStatusCode) {
		this.freebieCouponStatusCode = freebieCouponStatusCode;
	}

	/**
	 * @return the freebieExpiry
	 */
	public final String getFreebieExpiry() {
		return freebieExpiry;
	}

	/**
	 * @param freebieExpiry
	 *            the freebieExpiry to set
	 */
	public final void setFreebieExpiry(String freebieExpiry) {
		this.freebieExpiry = freebieExpiry;
	}

	/**
	 * @return the freebieLogoURL
	 */
	public final String getFreebieLogoURL() {
		return freebieLogoURL;
	}

	/**
	 * @param freebieLogoURL
	 *            the freebieLogoURL to set
	 */
	public final void setFreebieLogoURL(String freebieLogoURL) {
		this.freebieLogoURL = freebieLogoURL;
	}

	/**
	 * @return the freebieDealerMobile
	 */
	public final String getFreebieDealerMobile() {
		return freebieDealerMobile;
	}

	/**
	 * @param freebieDealerMobile
	 *            the freebieDealerMobile to set
	 */
	public final void setFreebieDealerMobile(String freebieDealerMobile) {
		this.freebieDealerMobile = freebieDealerMobile;
	}

	/**
	 * @return the freebieRatings
	 */
	public final int getFreebieRatings() {
		return freebieRatings;
	}

	/**
	 * @param freebieRatings
	 *            the freebieRatings to set
	 */
	public final void setFreebieRatings(int freebieRatings) {
		this.freebieRatings = freebieRatings;
	}

}