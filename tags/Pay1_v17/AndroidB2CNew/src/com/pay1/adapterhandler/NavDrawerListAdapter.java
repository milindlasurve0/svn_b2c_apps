package com.pay1.adapterhandler;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.SQLException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.larvalabs.svgandroid.SVG;
import com.larvalabs.svgandroid.SVGParser;
import com.pay1.CoinHistoryActivity;
import com.pay1.GiftMainActivity;
import com.pay1.R;
import com.pay1.WalletHistoryActivity;
import com.pay1.constants.Constants;
import com.pay1.constants.SVGConstant;
import com.pay1.databasehandler.FreeBies;
import com.pay1.databasehandler.FreeBiesDataSource;
import com.pay1.model.NavDrawerItem;
import com.pay1.requesthandler.RequestClass;
import com.pay1.utilities.Utility;

public class NavDrawerListAdapter extends BaseAdapter {

	private Context context;
	private ArrayList<NavDrawerItem> navDrawerItems;
	private Typeface Reguler;
	private ProgressBar progressBar1;
	ImageView wallet_refresh_image;
	ImageView imageView_Refresh;
	ImageView fbConnectButton;
	private FreeBiesDataSource freeBiesDataSource;
	OnClickListener clickListener;
	public NavDrawerListAdapter(Context context,
			ArrayList<NavDrawerItem> navDrawerItems,OnClickListener clickListener) {
		this.context = context;
		this.navDrawerItems = navDrawerItems;
		this.clickListener = clickListener;
		Reguler = Typeface.createFromAsset(context.getAssets(),
				"EncodeSansNormal-400-Regular.ttf");
		freeBiesDataSource = new FreeBiesDataSource(context);
	}

	@Override
	public int getCount() {
		return navDrawerItems.size();
	}

	@Override
	public Object getItem(int position) {
		return navDrawerItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@SuppressLint("NewApi")
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		LayoutInflater mInflater = (LayoutInflater) context
				.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

		if (position != 0 && position < 8) {
			convertView = mInflater.inflate(R.layout.drawer_list_adapter, null);

			// RelativeLayout llparent = (RelativeLayout) convertView
			// .findViewById(R.id.parent);
			ImageView imgIcon = (ImageView) convertView.findViewById(R.id.icon);

			TextView txtTitle = (TextView) convertView.findViewById(R.id.title);
			txtTitle.setTypeface(Reguler);
			TextView txtCount = (TextView) convertView
					.findViewById(R.id.counter);
			txtCount.setTypeface(Reguler);

			ImageView imgArrow = (ImageView) convertView
					.findViewById(R.id.imgArrow);

			SVG svgRightArrow = SVGParser.getSVGFromResource(
					context.getResources(), R.raw.ic_next);
			imgArrow.setImageDrawable(svgRightArrow.createPictureDrawable());
			// imgIcon.setImageResource(navDrawerItems.get(position).getIcon());
			SVG svg = SVGParser.getSVGFromResource(context.getResources(),
					SVGConstant.getSVGResource(position));
			imgIcon.setImageDrawable(svg.createPictureDrawable());
			imgIcon.setVisibility(View.INVISIBLE);
			if (Build.VERSION.SDK_INT >= 11) {
				imgIcon.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
			}

			if (position == 7
					&& Utility.getLoginFlag(context,
							Constants.SHAREDPREFERENCE_IS_LOGIN))
				txtTitle.setText("Logout");
			else
				txtTitle.setText(navDrawerItems.get(position).getTitle());

			// displaying count
			// check whether it set visible or not
			if (navDrawerItems.get(position).getCounterVisibility()) {
				DecimalFormat decimalFormat = new DecimalFormat("0.00");

				String bal = Utility.getBalance(context,
						Constants.SHAREDPREFERENCE_BALANCE);
				bal = bal == null ? "0" : bal;
				txtCount.setText("Rs "
						+ decimalFormat.format(Double.parseDouble(bal)));
			} else {
				// hide the counter view
				txtCount.setVisibility(View.GONE);
			}

			// if (position == 6) {
			// llparent.setBackgroundColor(context.getResources().getColor(
			// R.color.list_submenu));
			// imgIcon.setVisibility(View.GONE);
			// txtTitle.setTextSize(12);
			// txtTitle.setPadding(15, 0, 0, 0);
			// } else if (position == 7 || position == 8)
			// llparent.setBackgroundColor(context.getResources().getColor(
			// R.color.list_divider));
		} else if (position == 0) {
			convertView = mInflater.inflate(R.layout.drawer_header_adapter,
					null);

			
			
			/*
			 * LinearLayout
			 * wallet_layout_main=(LinearLayout)convertView.findViewById
			 * (R.id.wallet_layout_main); AlphaAnimation alpha = new
			 * AlphaAnimation(0.2F, 0.2F); alpha.setDuration(0); // Make
			 * animation instant alpha.setFillAfter(true); // Tell it to persist
			 * after the animation ends // And then on your layout
			 * wallet_layout_main.startAnimation(alpha);
			 */
			progressBar1 = (ProgressBar) convertView
					.findViewById(R.id.progressBar1);
			progressBar1.setVisibility(View.GONE);

			TextView textName = (TextView) convertView
					.findViewById(R.id.textName);
			textName.setText(Utility.getUserName(context,
					Constants.SHAREDPREFERENCE_NAME));
			ImageView imageView_Topup = (ImageView) convertView
					.findViewById(R.id.imageView_Topup);
			ImageView imageSettings = (ImageView) convertView
					.findViewById(R.id.imageSettings);
			SVG svgSettings = SVGParser.getSVGFromResource(
					context.getResources(), R.raw.ic_setting);
			imageSettings.setImageDrawable(svgSettings.createPictureDrawable());

			fbConnectButton = (ImageView) convertView
					.findViewById(R.id.fbConnectButton);
			

			/*
			 * imageSettings.setOnClickListener(new OnClickListener() {
			 * 
			 * @Override public void onClick(View v) { // TODO Auto-generated
			 * method stub
			 * 
			 * //fragmanagerAdapter. = WalletTopupFragement.newInstance(); } });
			 */

			wallet_refresh_image = (ImageView) convertView
					.findViewById(R.id.wallet_refresh_image);
			SVG svgRefreshImage = SVGParser.getSVGFromResource(
					context.getResources(), R.raw.ic_refresh);
			wallet_refresh_image.setImageDrawable(svgRefreshImage
					.createPictureDrawable());

			
			  ImageView wallet_icon_image = (ImageView) convertView
			  .findViewById(R.id.wallet_icon_image); 

			  
			/*  SVG svgWalletImage = SVGParser.getSVGFromResource(
						context.getResources(), R.raw.ic_profile_wallet);
			  wallet_icon_image.setImageDrawable(svgWalletImage
						.createPictureDrawable());*/
			  
			ImageView coin_icon_image = (ImageView) convertView
					.findViewById(R.id.coin_icon_image);
			SVG svgCoinImage = SVGParser.getSVGFromResource(
					context.getResources(), R.raw.ic_profile_coin);
			coin_icon_image.setImageDrawable(svgCoinImage
					.createPictureDrawable());

			ImageView like_icon_image = (ImageView) convertView
					.findViewById(R.id.like_icon_image);

			imageView_Refresh = (ImageView) convertView
					.findViewById(R.id.imageView_Refresh);
			imageView_Refresh.setImageDrawable(SVGParser.getSVGFromResource(
					context.getResources(), R.raw.ic_refresh)
					.createPictureDrawable());
			imageView_Topup.setImageDrawable(SVGParser.getSVGFromResource(
					context.getResources(), R.raw.ic_wallet)
					.createPictureDrawable());
			if (Build.VERSION.SDK_INT >= 11) {
				imageView_Topup.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
				imageView_Refresh.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
			}

			TextView textView_Balance = (TextView) convertView
					.findViewById(R.id.textView_Balance);
			textView_Balance.setTypeface(Reguler);
			TextView textView_Amount = (TextView) convertView
					.findViewById(R.id.textView_Amount);
			textView_Amount.setTypeface(Reguler);
			TextView textView_Header = (TextView) convertView
					.findViewById(R.id.textView_Header);
			textView_Header.setTypeface(Reguler);
			TextView textView_loyalityPoints = (TextView) convertView
					.findViewById(R.id.textView_loyalityPoints);
			textView_loyalityPoints.setTypeface(Reguler);
			String loyalityPoints = Utility.getBalance(context,
					Constants.SHAREDPREFERENCE_LOYALTY_POINTS);
			textView_loyalityPoints.setText(loyalityPoints);

			TextView textView_mylikes = (TextView) convertView
					.findViewById(R.id.textView_mylikes);
			textView_mylikes.setTypeface(Reguler);
			textView_loyalityPoints.setTypeface(Reguler);
			String mylikes = Utility.getMyLikes(context);
			
			try {
				freeBiesDataSource.open();
			List<FreeBies> list = freeBiesDataSource.getAllLikeFreeBies(1);
			int len = list.size();
			textView_mylikes.setText(String.valueOf(len));
			} catch (SQLException exception) {
				// TODO: handle exception
				// // // Log.e("Er ", exception.getMessage());
			} catch (Exception e) {
				// TODO: handle exception
				// Log.e("Er ", e.getMessage());
			} finally {
				freeBiesDataSource.close();
			}
			/*if (Integer.parseInt(mylikes) >= 1) {
				SVG svgLikeImage = SVGParser.getSVGFromResource(
						context.getResources(), R.raw.ic_favsel);
				like_icon_image.setImageDrawable(svgLikeImage
						.createPictureDrawable());
			} else {
				SVG svgLikeImage = SVGParser.getSVGFromResource(
						context.getResources(), R.raw.ic_favsel);
				like_icon_image.setImageDrawable(svgLikeImage
						.createPictureDrawable());
			}*/

			DecimalFormat decimalFormat = new DecimalFormat("0.00");
			// Constants.SHAREDPREFERENCE_LOYALTY_POINTS
			String bal = Utility.getBalance(context,
					Constants.SHAREDPREFERENCE_BALANCE);
			bal = bal == null ? "0" : bal;
			textView_Amount.setText("Rs "
					+ decimalFormat.format(Double.parseDouble(bal)));

			wallet_refresh_image.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					new CheckBalanceTask().execute();
				}
			});

			wallet_icon_image.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent intent=new Intent(context,WalletHistoryActivity.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					v.getContext().startActivity(intent);
				}
			});
			
			coin_icon_image.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent intent=new Intent(context,CoinHistoryActivity.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					context.startActivity(intent);
				}
			});
			
			
		/*	like_icon_image.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent intent=new Intent(context,WalletHistoryActivity.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					context.startActivity(intent);
					
					android.app.FragmentManager fragmentManager =  ((Activity) context).getFragmentManager();
					android.app.FragmentTransaction fragmentTransaction = fragmentManager
							.beginTransaction();
					fragmentTransaction.replace(R.id.frame_container,GiftMainActivity.newInstance(0));
					fragmentTransaction.addToBackStack(null);
					fragmentTransaction.commit();
				}
			});*/
			
like_icon_image.setOnClickListener(clickListener);
			
			if (Utility.getFbImage64(context).isEmpty()) {
				fbConnectButton.setImageDrawable(context.getResources()
						.getDrawable(R.drawable.ic_login));
			} else {
				// fbConnectButton.setImageBitmap(Utility.getBitmap(Utility.getFbImage(context)));
				// Picasso.with(context).load("http://graph.facebook.com/1082021775156920/picture?type=large").into(fbConnectButton);
				//new LoadImageTask().execute(Utility.getFbImage(context));
				if(Utility.getFbImage64(context).isEmpty()){
					//new LoadImageTask().execute(Utility.getFbImage(context));
				}else{
				fbConnectButton.setImageBitmap(Constants.decodeBase64(Utility.getFbImage64(context)));
				}
			}
		} else {
			convertView = mInflater
					.inflate(R.layout.drawer_empty_adapter, null);
		}
		return convertView;
	}

	public class CheckBalanceTask extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... params) {

			String response = RequestClass.getInstance().readPay1B2CRequest(
					context, Constants.B2C_URL + "check_bal");
			return response;
		}

		@Override
		protected void onPostExecute(String result) {

			super.onPostExecute(result);
			try {
				// JSONArray array = new JSONArray(result);
				JSONObject jsonObject = new JSONObject(result);
				String status = jsonObject.getString("status");
				if (status.equalsIgnoreCase("SUCCESS")) {
					JSONObject description = jsonObject
							.getJSONObject("description");
					String account_balance = description
							.getString("account_balance");
					Utility.setBalance(context,
							Constants.SHAREDPREFERENCE_BALANCE,
							account_balance.trim());
				}
			} catch (JSONException e) {
				// e.printStackTrace();
				// Constants.showCustomToast(MainActivity.this,
				// result);
			} catch (Exception e) {
				// TODO: handle exception
				// Constants.showCustomToast(MainActivity.this,
				// result);
			}
			notifyDataSetChanged();
			progressBar1.setVisibility(View.GONE);
			wallet_refresh_image.setVisibility(View.VISIBLE);
		}

		protected void onPreExecute() {
			progressBar1.setVisibility(View.VISIBLE);
			wallet_refresh_image.setVisibility(View.GONE);
			// this.dialog.setMessage("Please wait.....");

			super.onPreExecute();
		}
	}

	public class LoadImageTask extends AsyncTask<String, String, Bitmap> {
		
		@Override
		protected Bitmap doInBackground(String... params) {
			
			Bitmap bitmap=null;
				URL imageURL = null;
				try {
					imageURL = new URL("https://graph.facebook.com/"
							+ params[0] + "/picture?type=large");
					//imageURL = new URL(Utility.getFbImage(context));
				
				 bitmap = BitmapFactory.decodeStream(imageURL
						.openConnection().getInputStream());
				Log.d("dd", "fff");
				return bitmap;
				} catch (MalformedURLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Log.d("dd", "fff");
			
			return bitmap;
		}

		@Override
		protected void onPostExecute(Bitmap result) {

			super.onPostExecute(result);
			try {
				if (result != null) {
					
					String imgeInBase64=Constants.encodeTobase64(result);
					Utility.setFbImage64(context, imgeInBase64);
					fbConnectButton.setImageBitmap(Constants.decodeBase64(Utility.getFbImage64(context)));
					
					//fbConnectButton.setImageBitmap(result);
				}
			} catch (Exception e) {
				// TODO: handle exception
				// Constants.showCustomToast(MainActivity.this,
				// result);
			}
		//	notifyDataSetChanged();

		}

		protected void onPreExecute() {

			super.onPreExecute();
		}
	}

	
}
