﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using Microsoft.Phone.Shell;
using System.Windows.Data;
using System.Windows;
namespace NewPay1B2C
{
    public class ProgressBarManager : INotifyPropertyChanged
    {
        private Dictionary<int, string> _clients = new Dictionary<int, string>();
        private bool _show;
        private bool _inDeterminant;
        private string _text;
        private double _value;
        public bool ShowProgress { get { return _show; } }
        public bool ProgressInDeterminant { get { return _inDeterminant; } }
        public string ProgressText { get { return _text; } }
        public Double Value { get { return _value; } }

        public event PropertyChangedEventHandler PropertyChanged;

        public void Show(int ClientID, string Text, bool IsDeterminant, double Value)
        {
            if (!_clients.ContainsKey(ClientID))
            {
                _clients.Add(ClientID, Text);
            }
            else
            {
                _clients[ClientID] = Text;
            }
            _value = Value;
            _inDeterminant = !IsDeterminant;
            updateProperties();
        }
        public void Show(int ClientID, string Text)
        {
            if (!_clients.ContainsKey(ClientID))
            {
                _clients.Add(ClientID, Text);
            }
            else
            {
                _clients[ClientID] = Text;
            }
            updateProperties();
        }
        public void Hide(int ClientId)
        {
            if (_clients.ContainsKey(ClientId))
            {
                _clients.Remove(ClientId);
            }
            updateProperties();
        }
        public void Hook(ProgressIndicator FormProgressIndicator)
        {
            #region setup ProgressIndicator
            Binding _binding;
            _binding = new Binding("ShowProgress");
            _binding.Source = this;
            _binding.Mode = BindingMode.TwoWay;
            BindingOperations.SetBinding(FormProgressIndicator, ProgressIndicator.IsVisibleProperty, _binding);
            _binding = new Binding("ProgressInDeterminant");
            _binding.Source = this;
            _binding.Mode = BindingMode.TwoWay;
            BindingOperations.SetBinding(FormProgressIndicator, ProgressIndicator.IsIndeterminateProperty, _binding);
            _binding = new Binding("ProgressText");
            _binding.Source = this;
            _binding.Mode = BindingMode.TwoWay;
            BindingOperations.SetBinding(FormProgressIndicator, ProgressIndicator.TextProperty, _binding);
            _binding = new Binding("Value");
            _binding.Source = this;
            _binding.Mode = BindingMode.TwoWay;
            BindingOperations.SetBinding(FormProgressIndicator, ProgressIndicator.ValueProperty, _binding);
            #endregion
        }
        private void updateProperties()
        {
            if (_clients.Count == 0)
            {
                _show = false;
                _text = "";
            }
            else
            {
                _show = true;
                string _m_seperator = "";
                _text = "";
                foreach (KeyValuePair<int, string> _keyvaluePair in _clients)
                {
                    _text = _text + _m_seperator + _keyvaluePair.Value;
                    _m_seperator = ", ";
                }
            }
            onPropertyChanged("ShowProgress");
            onPropertyChanged("ProgressInDeterminant");
            onPropertyChanged("ProgressText");
            onPropertyChanged("Value");
        }
        private void onPropertyChanged(string PropertyName)
        {
            Deployment.Current.Dispatcher.BeginInvoke(() =>
            {
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
                }
            });
        }
    }
}