package com.pay1;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.view.ViewPager;

import com.pay1.adapterhandler.ScreenSlidePagerAdapter;
import com.pay1.constants.Constants;

public class MainActivity extends AppBaseActivity {

	// private final String TAG = "Main";
	private ViewPager mPager;
	private ScreenSlidePagerAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_activity);
		try {

			registerBaseActivityReceiver();

			mPager = (ViewPager) findViewById(R.id.pager);
			adapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
			mPager.setAdapter(adapter);
			mPager.setCurrentItem(1);
			mPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

				@Override
				public void onPageSelected(int arg0) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onPageScrolled(int arg0, float arg1, int arg2) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onPageScrollStateChanged(int arg0) {
					// TODO Auto-generated method stub
					if (mPager.getCurrentItem() == 0)
						Constants.SELECTED_MENU_INDEX = Constants.DEAL_MENU_INDEX;
					else if (mPager.getCurrentItem() == 1)
						Constants.SELECTED_MENU_INDEX = Constants.RECHARGE_MENU_INDEX;
					else if (mPager.getCurrentItem() == 2)
						Constants.SELECTED_MENU_INDEX = Constants.RECHARGE_MENU_INDEX;
					Constants.showNavigationBar(MainActivity.this);
				}
			});
		} catch (Exception e) {
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		Constants.SELECTED_MENU_INDEX = Constants.RECHARGE_MENU_INDEX;
		Constants.showNavigationBar(MainActivity.this);
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unRegisterBaseActivityReceiver();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (Constants.isExpanded) {
			Constants.collapsView(MainActivity.this);
		} else {
			super.onBackPressed();
		}
	}

}
