package com.pay1.servicehandler;

import java.net.URLEncoder;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.IntentService;
import android.content.Intent;
import android.os.AsyncTask;

import com.pay1.RegistrationActivity;
import com.pay1.constants.Constants;
import com.pay1.requesthandler.RequestClass;
import com.pay1.utilities.Utility;

public class SignUpService extends IntentService {

	public SignUpService() {
		super("SignUpService");
		// TODO Auto-generated constructor stub

	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		new LoginConfirmTask().execute(Utility.getMobileNumber(
				SignUpService.this, Constants.SHAREDPREFERENCE_MOBILE), Utility
				.getPin(SignUpService.this, Constants.SHAREDPREFERENCE_PIN),
				Utility.getPin(SignUpService.this,
						Constants.SHAREDPREFERENCE_PIN), Utility.getPin(
						SignUpService.this, Constants.SHAREDPREFERENCE_PIN));
	}

	public class LoginConfirmTask extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... params) {
			try {
				String response = RequestClass
						.getInstance()
						.readPay1B2CRequest(
								SignUpService.this,
								Constants.B2C_URL
										+ "update_password/?mobile_number="
										+ URLEncoder.encode(params[0], "utf-8")
										+ "&current_password=&password="
										+ URLEncoder.encode(params[1], "utf-8")
										+ "&confirm_password="
										+ URLEncoder.encode(params[2], "utf-8")
										+ "&code="
										+ URLEncoder.encode(params[3], "utf-8"));

				return response;

			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			try {

				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONObject jsonObject = new JSONObject(replaced);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {

						new LoginTask()
								.execute(
										Utility.getMobileNumber(
												SignUpService.this,
												Constants.SHAREDPREFERENCE_MOBILE),
										Utility.getPin(SignUpService.this,
												Constants.SHAREDPREFERENCE_PIN),
										Utility.getUUID(SignUpService.this,
												Constants.SHAREDPREFERENCE_UUID),
										Utility.getLatitude(
												SignUpService.this,
												Constants.SHAREDPREFERENCE_LATITUDE),
										Utility.getLongitude(
												SignUpService.this,
												Constants.SHAREDPREFERENCE_LONGITUDE),
										Utility.getOSManufacturer(
												SignUpService.this,
												Constants.SHAREDPREFERENCE_OS_MANUFACTURER),
										Utility.getOSVersion(
												SignUpService.this,
												Constants.SHAREDPREFERENCE_OS_VERSION),
										Utility.getGCMID(
												SignUpService.this,
												Constants.SHAREDPREFERENCE_GCMID));

					} else {

					}

				} else {

				}
			} catch (JSONException e) {
				// e.printStackTrace();

			} catch (Exception e) {
				// TODO: handle exception

			}
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}
	}

	public class LoginTask extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... params) {
			try {

				String response = RequestClass.getInstance()
						.readPay1B2CRequest(
								SignUpService.this,
								Constants.B2C_URL + "signin/?username="
										+ URLEncoder.encode(params[0], "utf-8")
										+ "&password="
										+ URLEncoder.encode(params[1], "utf-8")
										+ "&uuid=" + params[2] + "&latitude="
										+ params[3] + "&longitude=" + params[4]
										+ "&manufacturer=" + params[5]
										+ "&os_info=" + params[6]
										+ "&gcm_reg_id=" + params[7]
										+ "&device_type=android");

				return response;
			} catch (Exception e) {
				return "Error";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			try {

				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONObject jsonObject = new JSONObject(replaced);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {
						Utility.setLoginFlag(SignUpService.this,
								Constants.SHAREDPREFERENCE_IS_LOGIN, true);
						JSONObject jsonObject2 = jsonObject
								.getJSONObject("description");

						Utility.setUserName(SignUpService.this,
								Constants.SHAREDPREFERENCE_NAME, jsonObject2
										.getString("name").trim());
						Utility.setMobileNumber(SignUpService.this,
								Constants.SHAREDPREFERENCE_MOBILE, jsonObject2
										.getString("mobile").trim());
						Utility.setEmail(SignUpService.this,
								Constants.SHAREDPREFERENCE_EMAIL, jsonObject2
										.getString("email").trim());
						Utility.setGender(SignUpService.this,
								Constants.SHAREDPREFERENCE_GENDER, jsonObject2
										.getString("gender").trim());
						Utility.setUserID(SignUpService.this,
								Constants.SHAREDPREFERENCE_USER_ID, jsonObject2
										.getString("user_id").trim());
						Utility.setBalance(SignUpService.this,
								Constants.SHAREDPREFERENCE_BALANCE, jsonObject2
										.getString("wallet_balance").trim());
						Utility.setDOB(SignUpService.this,
								Constants.SHAREDPREFERENCE_DOB, jsonObject2
										.getString("date_of_birth").trim());
						Utility.setLatitude(SignUpService.this,
								Constants.SHAREDPREFERENCE_LATITUDE,
								jsonObject2.getString("latitude").trim());
						Utility.setLongitude(SignUpService.this,
								Constants.SHAREDPREFERENCE_LONGITUDE,
								jsonObject2.getString("longitude").trim());
						Utility.setSupportNumber(SignUpService.this,
								Constants.SHAREDPREFERENCE_SUPPORT_NUMBER,
								jsonObject2.getString("support_number").trim());
						Utility.setCookieName(SignUpService.this,
								Constants.SHAREDPREFERENCE_COOKIE_NAME,
								jsonObject2.getString("session_name"));
						Utility.setCookieValue(SignUpService.this,
								Constants.SHAREDPREFERENCE_COOKIE_VALUE,
								jsonObject2.getString("session_id"));
						
						Utility.setSharedPreferences(SignUpService.this,
								"Deals_offer",
								jsonObject2.getString("Deals_offer"));
						
						
						Utility.setMyTotalGifts(SignUpService.this,
								jsonObject2.getString("total_gifts"));

						Utility.setMyLikes(SignUpService.this,
								jsonObject2.getString("total_mylikes"));

						Utility.setMyTotalRedeem(SignUpService.this,
								jsonObject2.getString("total_redeem"));

						Utility.setMyReviews(SignUpService.this,
								jsonObject2.getString("total_myreviews"));
						JSONObject jsonObjectTemplate = jsonObject2
								.getJSONObject("ErrorMsg");
						Utility.setTemplate(
								SignUpService.this,
								Constants.SHAREDPREFERENCE_SMS_BILLPAYMENT_FAILURE,
								jsonObjectTemplate
										.getString("BILLPAYMENT_FAILURE"));
						Utility.setTemplate(
								SignUpService.this,
								Constants.SHAREDPREFERENCE_SMS_RECHARGE_FAILURE,
								jsonObjectTemplate
										.getString("RECHARGE_FAILURE"));
						Utility.setTemplate(
								SignUpService.this,
								Constants.SHAREDPREFERENCE_SMS_WALLET_REFILLED_RETAILER,
								jsonObjectTemplate
										.getString("WALLET_REFILLED_RETAILER"));

						Utility.setSharedPreferences(
								SignUpService.this,
								Constants.SHAREDPREFERENCE_SERVICE_CHARGE_PERCENT,
								jsonObject2.getString("service_charge_percent")
										.trim());

						Utility.setSharedPreferences(SignUpService.this,
								Constants.SHAREDPREFERENCE_SERVICE_TAX_PERCENT,
								jsonObject2.getString("service_tax_percent")
										.trim());

					} else {

					}

				} else {
				}
			} catch (JSONException e) {
				// e.printStackTrace();

			} catch (Exception e) {
				// TODO: handle exception

			}
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}
	}

}