﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pay1B2C.Model
    {
    public class GiftsTable: INotifyPropertyChanged
        {


     
        private static long serialVersionUID = 1L;
        //private int freebieID;

        [SQLite.PrimaryKey, SQLite.AutoIncrement]
        public int freebieID
            {
            get;
            set;
            }


        public long freebieUptateTime
            {
             get { return this.getFreebieUptateTime; }

            set
                {
                if(value != this.getFreebieUptateTime)
                    {
                    this.getFreebieUptateTime = value;
                    NotifyPropertyChanged("freebieUptateTime");
                    }
                }
            }



        public double freebieLat
            {
             get { return this.getFreebieLat; }

            set
                {
                if(value != this.getFreebieLat)
                    {
                    this.getFreebieLat = value;
                    NotifyPropertyChanged("freebieLat");
                    }
                }
            }



        public double freebieLng
            {
             get { return this.getFreebieLng; }

            set
                {
                if(value != this.getFreebieLng)
                    {
                    this.getFreebieLng = value;
                    NotifyPropertyChanged("freebieLng");
                    }
                }
            }

        
        public int freebieDealID
            {
             get { return this.getFreebieDealID; }

            set
                {
                if(value != this.getFreebieDealID)
                    {
                    this.getFreebieDealID = value;
                    NotifyPropertyChanged("freebieDealID");
                    }
                }
            }

      
        //public int freebieRatings
        //    {
        //     get { return this.getFreebieRatings; }

        //    set
        //        {
        //        if(value != this.getFreebieRatings)
        //            {
        //            this.getFreebieRatings = value;
        //            NotifyPropertyChanged("freebieRatings");
        //            }
        //        }
        //    }

        public int freebieCategoryID
            {
             get { return this.getFreebieCategoryID; }

            set
                {
                if(value != this.getFreebieCategoryID)
                    {
                    this.getFreebieCategoryID = value;
                    NotifyPropertyChanged("freebieCategoryID");
                    }
                }
            }

        public int freebiePin
            {
             get { return this.getFreebiePin; }

            set
                {
                if(value != this.getFreebiePin)
                    {
                    this.getFreebiePin = value;
                    NotifyPropertyChanged("freebiePin");
                    }
                }
            }


       
        public int freebieLocationCount
            {
             get { return this.getFreebieLocationCount; }

            set
                {
                if(value != this.getFreebieLocationCount)
                    {
                    this.getFreebieLocationCount = value;
                    NotifyPropertyChanged("freebieLocationCount");
                    }
                }
            }

        public int freebieOfferID
            {
             get { return this.getFreebieOfferID; }

            set
                {
                if(value != this.getFreebieOfferID)
                    {
                    this.getFreebieOfferID = value;
                    NotifyPropertyChanged("freebieOfferID");
                    }
                }
            }

        public int freebieMinAmount
            {
             get { return this.getFreebieMinAmount; }

            set
                {
                if(value != this.getFreebieMinAmount)
                    {
                    this.getFreebieMinAmount = value;
                    NotifyPropertyChanged("freebieMinAmount");
                    }
                }
            }


        public int freebieLike
            {
             get { return this.getFreebieLike; }

            set
                {
                if(value != this.getFreebieLike)
                    {
                    this.getFreebieLike = value;
                    NotifyPropertyChanged("freebieLike");
                    }
                }
            }

    
        public string freebieAddress
            {
             get { return this.getFreebieAddress; }

            set
                {
                if(value != this.getFreebieAddress)
                    {
                    this.getFreebieAddress = value;
                    NotifyPropertyChanged("freebieAddress");
                    }
                }
            }


          public string freebieArea
            {
             get { return this.getFreebieArea; }

            set
                {
                if(value != this.getFreebieArea)
                    {
                    this.getFreebieArea = value;
                    NotifyPropertyChanged("freebieArea");
                    }
                }
            }


          public string freebieCity
            {
             get { return this.getFreebieCity; }

            set
                {
                if(value != this.getFreebieCity)
                    {
                    this.getFreebieCity = value;
                    NotifyPropertyChanged("freebieCity");
                    }
                }
            }

      
          public string freebieState
            {
             get { return this.getFreebieState; }

            set
                {
                if(value != this.getFreebieState)
                    {
                    this.getFreebieState = value;
                    NotifyPropertyChanged("freebieState");
                    }
                }
            }



          public string freebieOfferName
            {
             get { return this.getFreebieOfferName; }

            set
                {
                if(value != this.getFreebieOfferName)
                    {
                    this.getFreebieOfferName = value;
                    NotifyPropertyChanged("freebieOfferName");
                    }
                }
            }



          public string freebieValidity
            {
             get { return this.getFreebieValidity; }

            set
                {
                if(value != this.getFreebieValidity)
                    {
                    this.getFreebieValidity = value;
                    NotifyPropertyChanged("freebieValidity");
                    }
                }
            }





          public string freebieLogoURL
              {
               get { return this.getFreebieLogoURL; }

              set
                  {
                  if(value != this.getFreebieLogoURL)
                      {
                      this.getFreebieLogoURL = value;
                      NotifyPropertyChanged("freebieLogoURL");
                      }
                  }
              }





          public string freebieShortDesc
              {
               get { return this.getFreebieShortDesc; }

              set
                  {
                  if(value != this.getFreebieShortDesc)
                      {
                      this.getFreebieShortDesc = value;
                      NotifyPropertyChanged("freebieShortDesc");
                      }
                  }
              }
        
     
          public string freebieCategory
            {
             get { return this.getFreebieCategory; }

            set
                {
                if(value != this.getFreebieCategory)
                    {
                    this.getFreebieCategory = value;
                    NotifyPropertyChanged("freebieCategory");
                    }
                }
            }

          public string freebieURL
              {
               get { return this.getFreebieURL; }

              set
                  {
                  if(value != this.getFreebieURL)
                      {
                      this.getFreebieURL = value;
                      NotifyPropertyChanged("freebieURL");
                      }
                  }
              }

          public string freebieDealName
              {
               get { return this.getFreebieDealName; }

              set
                  {
                  if(value != this.getFreebieDealName)
                      {
                      this.getFreebieDealName = value;
                      NotifyPropertyChanged("freebieDealName");
                      }
                  }
              }

          public string freebieDealerMobile
              {
               get { return this.getFreebieDealerMobile; }

              set
                  {
                  if(value != this.getFreebieDealerMobile)
                      {
                      this.getFreebieDealerMobile = value;
                      NotifyPropertyChanged("freebieDealerMobile");
                      }
                  }
              }


       /*   public string pin
              {
              get { return this.getPin; }

              set
                  {
                  if(value != this.getPin)
                      {
                      this.getPin = value;
                      NotifyPropertyChanged("pin");
                      }
                  }
              }
        */

          public int price
              {
              get { return this.getPrice; }

              set
                  {
                  if(value != this.getPrice)
                      {
                      this.getPrice = value;
                      NotifyPropertyChanged("price");
                      }
                  }
              }

          public int by_voucher
              {
              get { return this.getFreeBieByVoucher; }

              set
                  {
                  if(value != this.getFreeBieByVoucher)
                      {
                      this.getFreeBieByVoucher = value;
                      NotifyPropertyChanged("by_voucher");
                      }
                  }
              }

          public int offer_price
              {
              get { return this.getFreeBieOfferPrice; }

              set
                  {
                  if(value != this.getFreeBieOfferPrice)
                      {
                      this.getFreeBieOfferPrice = value;
                      NotifyPropertyChanged("offer_price");
                      }
                  }
              }





          public GiftsTable()
        {
        }
       /*   private int price;
          private String pin;
          private int freeBieByVoucher;
          private int freeBieOfferPrice;*/

    public GiftsTable(int getFreebieDealID, string getFreebieAddress, string getFreebieArea, string getFreebieCity, int getFreebieCategoryID, double getFreebieLat, double getFreebieLng, string getFreebieState, string getFreebieOfferName, string getFreebieValidity, string getFreebieShortDesc, int getFreebiePin, int getFreebieLocationCount, int getFreebieOfferID, int getFreebieMinAmount, int getFreebieLike, string getFreebieLogoURL, string getFreebieDealerMobile, string getFreebieDealName, string getFreebieURL, string getFreebieCategory,int price, int freeBieByVoucher, int freeBieOfferPrice)
              {
              // TODO: Complete member initialization
            
              this.getFreebieDealID = getFreebieDealID;
              this.getFreebieAddress = getFreebieAddress;
              this.getFreebieArea = getFreebieArea;
              this.getFreebieCity = getFreebieCity;
              this.getFreebieCategoryID = getFreebieCategoryID;
              this.getFreebieLat = getFreebieLat;
              this.getFreebieLng = getFreebieLng;
              this.getFreebieState = getFreebieState;
              this.getFreebieOfferName = getFreebieOfferName;
              this.getFreebieValidity = getFreebieValidity;
              this.getFreebieShortDesc = getFreebieShortDesc;
              this.getFreebiePin = getFreebiePin;
              this.getFreebieLocationCount = getFreebieLocationCount;
              this.getFreebieOfferID = getFreebieOfferID;
              
              this.getFreebieMinAmount = getFreebieMinAmount;
              this.getFreebieLike = getFreebieLike;
          
              this.getFreebieLogoURL = getFreebieLogoURL;
              this.getFreebieDealerMobile = getFreebieDealerMobile;
         
              this.getFreebieDealName = getFreebieDealName;
              this.getFreebieURL = getFreebieURL;
              this.getFreebieCategory = getFreebieCategory;


             
              this.price=price;
              this.by_voucher=freeBieByVoucher;
              this.offer_price=freeBieOfferPrice;


              }


        public event PropertyChangedEventHandler PropertyChanged;


        private int getFreebieDealID;
      
        private int getFreebieCategoryID;
        private int getFreebiePin;
        private int getFreebieLocationCount;
        private int getFreebieOfferID;
        private int getFreebieMinAmount;
        private int getFreebieLike;
        private int getFreebieCouponStatusID;

        private double getFreebieLat;
        private double getFreebieLng;
        private long getFreebieUptateTime;

        private String getFreebieAddress;
        private String getFreebieArea;
        private String getFreebieCity;
        private String getFreebieState;
        private String getFreebieOfferName;
        private String getFreebieValidity;
        private String getFreebieShortDesc;
      
        private String getFreebieLogoURL;
        private String getFreebieDealerMobile;
        private String getFreebieDealName;
        private String getFreebieURL;
        private String getFreebieCategory;

        private int getPrice;
        private String getPin;
        private int getFreeBieByVoucher;
        private int getFreeBieOfferPrice;


    

        private void NotifyPropertyChanged(String info)
            {
            if(PropertyChanged != null)
                {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
                }
            }


        }
    }
