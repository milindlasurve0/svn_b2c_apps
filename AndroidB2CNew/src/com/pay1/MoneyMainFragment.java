package com.pay1;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pay1.adapterhandler.MoneyFragmentAdapter;
import com.pay1.customviews.TabPageIndicator;

public class MoneyMainFragment extends Fragment {

	public static MoneyMainFragment newInstance() {
		MoneyMainFragment fragment = new MoneyMainFragment();
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		final Context contextThemeWrapper = new ContextThemeWrapper(
				getActivity(), R.style.StyledIndicators);
		inflater = inflater.cloneInContext(contextThemeWrapper);
		View view = inflater.inflate(R.layout.money_main_fragment, container,
				false);
		try {

			MoneyFragmentAdapter adapter = new MoneyFragmentAdapter(
					getActivity().getSupportFragmentManager());

			ViewPager pager = (ViewPager) view.findViewById(R.id.pager);
			pager.setAdapter(adapter);

			TabPageIndicator indicator = (TabPageIndicator) view
					.findViewById(R.id.indicator);
			indicator
					.setBackgroundColor(getResources().getColor(R.color.White));

			indicator.setViewPager(pager);

		} catch (Exception e) {
		}
		return view;
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.Fragment#onDestroy()
	 */
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		// Intent intent = new Intent(getActivity(), MainActivity.class);
		// intent.putExtra("FROM_QUICKPAY", false);
		// intent.putExtra("IS_GIFT", false);
		// intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
		// | Intent.FLAG_ACTIVITY_NEW_TASK);
		// getActivity().startActivity(intent);
		// getActivity().finish();
	}
}
