﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Media.Animation;
using System.Windows.Media;
using NewPay1B2C.Model;
using System.Windows.Media.Imaging;
using Microsoft.Phone.Tasks;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace NewPay1B2C
    {
    public partial class WalletControl : UserControl
        {
        Storyboard storyboard;
        ProgressBarManager ProgressBarManager;
        public WalletControl ()
            {
            InitializeComponent ();

            ProgressIndicator prog = new ProgressIndicator ();
            ProgressBarManager = new ProgressBarManager ();
            SystemTray.SetProgressIndicator (this, prog);
            ProgressBarManager.Hook (prog);


            List<SideBarItem> sideItem = new List<SideBarItem> ();
            sideItem.Add (new SideBarItem ("Home", Constant.SIDE_BAR_HOME, "/Pay1Images/home.png"));
            sideItem.Add (new SideBarItem ("Recharge & Bills", Constant.SIDE_BAR_RECHARGE_N_BILLS, "/Pay1Images/recharges_bills.png"));
            sideItem.Add (new SideBarItem ("Free Gifts", Constant.SIDE_BAR_MY_GIFTS, "/Pay1Images/mygifts.png"));
            sideItem.Add (new SideBarItem ("Missed Call Recharge", Constant.SIDE_BAR_MISSED_CALL, "/Pay1Images/missedcall.png"));
            sideItem.Add (new SideBarItem ("My Gifts", Constant.SIDE_BAR_MISSED_CALL, "/Pay1Images/mygifts.png"));
            sideItem.Add (new SideBarItem ("My Transactions", Constant.SIDE_BAR_MY_TRANS, "/Pay1Images/mytransaction.png"));



            foreach (SideBarItem SideBarItem1 in sideItem)
                {
                SIdeBarUserControl SIdeBarUserControl = new SIdeBarUserControl ();
                SIdeBarUserControl.icon.Source = new BitmapImage (new Uri (SideBarItem1.ImagePath, UriKind.RelativeOrAbsolute));
                SIdeBarUserControl.iconName.Text = SideBarItem1.Name;
                listSide.Items.Add (SIdeBarUserControl);
                }

            SideBarBalanceControl SideBarBalanceControl = new SideBarBalanceControl ();
            SideBarBalanceControl.textBalance.Text = "Rs. " + Constant.getbalance ();
            listSide.Items.Add (SideBarBalanceControl);


            List<SideBarItem> sideItemWallet = new List<SideBarItem> ();
            sideItemWallet.Add (new SideBarItem ("Cash Topup", Constant.SIDE_BAR_HOME, "/Pay1Images/cash_topup.png"));
            sideItemWallet.Add (new SideBarItem ("Online Topup", Constant.SIDE_BAR_RECHARGE_N_BILLS, "/Pay1Images/online_topup.png"));
            sideItemWallet.Add (new SideBarItem ("Redeem Coupon", Constant.SIDE_BAR_MY_GIFTS, "/Pay1Images/redeem_coupon.png"));
            sideItemWallet.Add (new SideBarItem ("Wallet History", Constant.SIDE_BAR_MISSED_CALL, "/Pay1Images/wallet_history.png"));

            foreach (SideBarItem SideBarItem1 in sideItemWallet)
                {
                SIdeBarUserControl SIdeBarUserControl = new SIdeBarUserControl ();
                SIdeBarUserControl.icon.Source = new BitmapImage (new Uri (SideBarItem1.ImagePath, UriKind.RelativeOrAbsolute));
                SIdeBarUserControl.iconName.Text = SideBarItem1.Name;
                listSide.Items.Add (SIdeBarUserControl);
                }
            SideBarOtherControl SideBarOtherControl = new SideBarOtherControl ();
            listSide.Items.Add (SideBarOtherControl);

            List<SideBarItem> sideItemOthers = new List<SideBarItem> ();
            sideItemOthers.Add (new SideBarItem ("My Profile", Constant.SIDE_BAR_HOME, "/Pay1Images/myprofile.png"));
            sideItemOthers.Add (new SideBarItem ("Support", Constant.SIDE_BAR_RECHARGE_N_BILLS, "/Pay1Images/support.png"));
            sideItemOthers.Add (new SideBarItem ("FAQ", Constant.SIDE_BAR_MY_GIFTS, "/Pay1Images/faq.png"));
            sideItemOthers.Add (new SideBarItem ("Privacy Policy", Constant.SIDE_BAR_MISSED_CALL, "/Pay1Images/privacy_policy.png"));
            sideItemOthers.Add (new SideBarItem ("Terms & Conditions", Constant.SIDE_BAR_RECHARGE_N_BILLS, "/Pay1Images/terms.png"));
            sideItemOthers.Add (new SideBarItem ("Rate Us", Constant.SIDE_BAR_MY_GIFTS, "/Pay1Images/rate_us.png"));
            sideItemOthers.Add (new SideBarItem ("Log Out", Constant.SIDE_BAR_MISSED_CALL, "/Pay1Images/logout.png"));

            foreach (SideBarItem SideBarItem1 in sideItemOthers)
                {
                SIdeBarUserControl SIdeBarUserControl = new SIdeBarUserControl ();
                SIdeBarUserControl.icon.Source = new BitmapImage (new Uri (SideBarItem1.ImagePath, UriKind.RelativeOrAbsolute));
                SIdeBarUserControl.iconName.Text = SideBarItem1.Name;
                listSide.Items.Add (SIdeBarUserControl);
                }

			int selectedIndex = Constant.LoadPersistent<int> ("index");
			SIdeBarUserControl ctrl = (SIdeBarUserControl)listSide.Items.ElementAt (selectedIndex);// [3].

			ctrl.stackPa.Background = new SolidColorBrush (Constant.ConvertStringToColor ("#503D97"));
		
			
            }
		private void HookScrollViewer (object sender, RoutedEventArgs e)
			{
			int selectedIndex = Constant.LoadPersistent<int> ("index");
			var element = (FrameworkElement)sender;
			var scrollViewer = FindChildOfType<ScrollViewer> (element);

			if (scrollViewer == null)
				return;

			scrollViewer.ScrollToVerticalOffset (selectedIndex);
			}

		public static T FindChildOfType<T> (DependencyObject root) where T : class
			{
			var queue = new Queue<DependencyObject> ();
			queue.Enqueue (root);

			while (queue.Count > 0)
				{
				var current = queue.Dequeue ();
				for (int i = VisualTreeHelper.GetChildrenCount (current) - 1; 0 <= i; i--)
					{
					var child = VisualTreeHelper.GetChild (current, i);
					var typedChild = child as T;
					if (typedChild != null)
						{
						return typedChild;
						}
					queue.Enqueue (child);
					}
				}
			return null;
			}

        private void sideBarList_SelectionChanged (object sender, SelectionChangedEventArgs e)
            {

			 Constant.SavePersistent ("index", listSide.SelectedIndex);
			Constant.sideBarListHandle (listSide.SelectedIndex);
			
            }

        private async void LogOutTask ()
            {
            ProgressBarManager.Show (18, "Please wait...", false, 10);
            var values = new List<KeyValuePair<string, string>> ();
            values.Add (new KeyValuePair<string, string> ("actiontype", "signout"));

            values.Add (new KeyValuePair<string, string> ("api", "true"));
            FormUrlEncodedContent values1 = new FormUrlEncodedContent (values);
            Task<string> response = Constant.postParamsAndgetResponse (Constant.B2C_URL, values1);

            string result = await response;
            if (string.IsNullOrEmpty (result))
                {
                (Application.Current.RootVisual as PhoneApplicationFrame).Navigate (new Uri ("/NoConnection.xaml?", UriKind.Relative));
                }
            else
                {
                var dict = (JObject)JsonConvert.DeserializeObject (result);
                string status = dict["status"].ToString ();
                var description = dict["description"];
                if (status.Equals ("success"))
                    {

                    Constant.SavePersistent (Constant.SETTINGS_IS_LOGIN, false);
                    Constant.SavePersistent (Constant.SETTINGS_USER_WALLET_BALANCE, "0.00");

                    Constant.SavePersistent ("cookieName", "111");
                    Constant.SavePersistent ("cookieValue", "111");
                    (Application.Current.RootVisual as PhoneApplicationFrame).Navigate (new Uri ("/RegistrationLayout.xaml?", UriKind.Relative));
                    while ((Application.Current.RootVisual as PhoneApplicationFrame).BackStack.Count () > 0)
                        {
                        (Application.Current.RootVisual as PhoneApplicationFrame).RemoveBackEntry ();
                        }
                    }
                else if (status.Equals ("failure"))
                    {
					Constant.checkForErrorCode (result); //MessageBox.Show (description.ToString ());
                    }
                }
            ProgressBarManager.Hide (18);
            }


        private void FrameworkElement_OnLoaded (object sender, RoutedEventArgs e)
            {
            storyboard = new Storyboard ();
            var animation = new DoubleAnimation
            {
                From = 0,
                To = 100
            };

            var translatTransform = new TranslateTransform
            {
                Y = 100
            };

            var panel = (StackPanel)sender;
            panel.RenderTransform = translatTransform;

            Storyboard.SetTarget (animation, translatTransform);
            Storyboard.SetTargetProperty (animation, new PropertyPath ("Y"));
            storyboard.Children.Add (animation);

            }


        }
    }
