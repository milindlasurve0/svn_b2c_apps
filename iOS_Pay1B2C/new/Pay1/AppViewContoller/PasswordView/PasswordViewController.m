////
//  PasswordViewController.m
//  Pay1
//
//  Created by Annapurna on 04/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "PasswordViewController.h"
#import "ForgotPasswordViewController.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "MainViewController.h"
#import "CreateUserResponse.h"
#import "SignInService.h"
#import "SignInCriteria.h"
#import "UIDevice+IdentifierAddition.h"
#import "NSString+MD5Addition.h"
#import "CurrentLocation.h"
#import "UpdatePasswordService.h"
#import "UpdatePasswordViewController.h"
#import "HomeViewController.h"
#import "UpdatePasswordResponse.h"
@interface PasswordViewController ()

@end

@implementation PasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    resendSMSCount = 0;
    
    
    updatedDealsArrayList = [[NSMutableArray alloc]init];
    
    [passwordTxtLabel removeFromSuperview];
    [resendButton removeFromSuperview];
    [timeDecrementLabel removeFromSuperview];
    
   // passwordTxtField.text = @"1234";//@"3754";
    
    backGroundImgView.frame = CGRectMake(0, -20, self.view.frame.size.width, self.view.frame.size.height);
    _scrollView.frame = CGRectMake(0, 20, self.view.frame.size.width, self.view.frame.size.height);
    
    _scrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height );
    
    if([_createUserResponseObj.status isEqualToString:@"success"]){
    currMinute = 03;
    currSeconds = 00;
    
    timer=[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerFired) userInfo:nil repeats:YES];
    [forgotPswdButton removeFromSuperview];
    
    passwordTxtField.placeholder = @"OTP";
    [loginButton setTitle:@"VERIFY" forState:UIControlStateNormal];
    forgotPswdButton.hidden = TRUE;
    
    [_scrollView addSubview:passwordTxtLabel];
    [_scrollView addSubview:timeDecrementLabel];
    
    [self setDefaultUIForView:TRUE resend:FALSE];
    }
    
    passwordImgView.userInteractionEnabled = YES;
    
    if([_createUserResponseObj.status isEqualToString:@"failure"])
    {

    passwordTxtField.placeholder = @"PASSWORD";
    [self setDefaultUIForView:FALSE resend:FALSE];

    }
    
    mobileNumLabel.text = [NSString stringWithFormat:@"%@ %@",@"+91",[Constant getUserPhoneNumber]];
    
    [self removeAllErrorViewFromSuperView];

    
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    [self.navigationController setNavigationBarHidden:TRUE];
}

-(void)timerFired
{
    
    if((currMinute>0 || currSeconds>=0) && currMinute>=0)
    {
    if(currSeconds==0)
    {
    currMinute-=1;
    currSeconds=59;
    }
    else if(currSeconds>0)
    {
    currSeconds-=1;
    }
    if(currMinute>-1)
    [timeDecrementLabel setText:[NSString stringWithFormat:@"%02d:%02d",currMinute,currSeconds]];
    }
    else
    {
    
    if(resendSMSCount == 1){
    [timeDecrementLabel removeFromSuperview];
    }
       
    resendButton.hidden = FALSE;
    [timer invalidate];
    timeDecrementLabel.hidden = TRUE;
        
    [self setDefaultUIForView:TRUE resend:TRUE];

    }
    
}



-(void)setDefaultUIForView:(BOOL)isStatus resend:(BOOL)resend{
    
    loginButton.layer.cornerRadius = 5;//half of the width
    loginButton.backgroundColor = [UIColor redColor];
    
    resendButton.layer.cornerRadius = 5;
    resendButton.backgroundColor = [UIColor redColor];
    
    if([UIScreen mainScreen].bounds.size.width == 320){
    logoImageView.frame = CGRectMake(self.view.frame.origin.x + 70, 60, 180, 128);
    numberDetailView.frame = CGRectMake(self.view.frame.origin.x + 50, logoImageView.frame.origin.y + logoImageView.frame.size.height, self.view.frame.size.width - 75, 80);
    
    mobileNumLabel.textAlignment = NSTextAlignmentCenter;
    
    if(isStatus == TRUE){
   
    if(resendSMSCount >0){
    passwordTxtLabel.text = @"LOOKS LIKE THERE IS SOME PROBLEM IN SENDING SMS, TRY RE-SENDING IT.";      
    }
        
    else
    passwordTxtLabel.text = @"YOU WILL RECEIVE ONE TIME PASSWORD (OTP) VIA SMS. KINDLY ENTER BELOW";
        
    [self setTextOnLabel:passwordTxtLabel fontSize:16.0];
        
    passwordTxtLabel.frame = CGRectMake(self.view.frame.origin.x +20, numberDetailView.frame.origin.y + numberDetailView.frame.size.height + 20, self.view.frame.size.width - 5, 60);
    passwordTxtField.frame = CGRectMake(self.view.frame.origin.x +30, passwordTxtLabel.frame.origin.y + passwordTxtLabel.frame.size.height + 10, self.view.frame.size.width - 175, 45);
    }
    else{
    passwordTxtField.frame = CGRectMake(self.view.frame.origin.x +30, numberDetailView.frame.origin.y + numberDetailView.frame.size.height + 40, self.view.frame.size.width - 175, 45);
 
    }
    
    thinImageView.frame = CGRectMake(self.view.frame.origin.x +30, passwordTxtField.frame.origin.y + passwordTxtField.frame.size.height - 5, self.view.frame.size.width-70, 1);
    loginButton.frame = CGRectMake(self.view.frame.origin.x + 90, passwordTxtField.frame.origin.y + passwordTxtField.frame.size.height + 15, 130, 40);
    
    
    
    forgotPswdButton.frame = CGRectMake(self.view.frame.origin.x + 80, loginButton.frame.origin.y + loginButton.frame.size.height + 5,180, 40);
    forgotPswdButton.titleLabel.font = [UIFont systemFontOfSize:16];
    timeDecrementLabel.frame = CGRectMake(self.view.frame.origin.x + 100, loginButton.frame.origin.y + loginButton.frame.size.height + 20,100, 40);
   
    
    
    }
    if(resend == TRUE){
    [_scrollView addSubview:resendButton];

    resendButton.frame = CGRectMake(self.view.frame.origin.x + 70, loginButton.frame.origin.y + loginButton.frame.size.height + 20, 180, 21);
    timeDecrementLabel.frame = CGRectMake(self.view.frame.origin.x + 100, loginButton.frame.origin.y + loginButton.frame.size.height + 20,100, 40);
    }

    
    
    
    
}


-(IBAction)loginClicked:(id)sender{

    
    if ([passwordTxtField.text length] == 0 ) {
    [self showValidationAlertView:numberErrorImgView];
    [self showValidationAlertView:numberErrorTxtView];
    return;
    }
    
   /* if(!isNewUser){
    NSString *convertMD5 = [NSString stringWithFormat:@"%@",[Constant getUserPhoneNumber]];
    NSString *phNumMD5 = [self generateMD5:convertMD5];
    NSString *passwordMD5 = [self generateMD5:passwordTxtField.text];
        
    NSString *finalMD5 = [NSString stringWithFormat:@"%@%@",phNumMD5,passwordMD5];
    if(![_createUserResponseObj.otp isEqualToString:finalMD5]){
    NSLog(@"#Invalid Password");
        
        
    if([_createUserResponseObj.status isEqualToString:@"success"]){
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"PASSWORD" message:@"Invalid Password!" delegate:self cancelButtonTitle:@"DONE" otherButtonTitles:nil, nil];
    [alert show];
    }
    }
    else{
        [self callSignInAPI];
        
        
    }

    }
    
    if(isNewUser){
    [self callSignInAPI];
    }*/
    
    
    
    if(passwordTxtField.text.length > 0){
        NSString *convertMD5 = [NSString stringWithFormat:@"%@",[Constant getUserPhoneNumber]];
        NSString *phNumMD5 = [Constant generateMD5:convertMD5];
        NSString *passwordMD5 = [Constant generateMD5:passwordTxtField.text];
        
        NSString *finalMD5 = [NSString stringWithFormat:@"%@%@",phNumMD5,passwordMD5];
        
        NSLog(@"#Invalid Password  %@ %@",_userOTP,finalMD5);

        if(![_userOTP isEqualToString:finalMD5]){
        NSLog(@"#Invalid Password");
            
            
        if([_createUserResponseObj.status isEqualToString:@"success"]){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"OTP" message:@"Invalid OTP!" delegate:self cancelButtonTitle:@"DONE" otherButtonTitles:nil, nil];
        [alert show];
        }
            
        else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"OTP" message:@"Invalid Password" delegate:self cancelButtonTitle:@"DONE" otherButtonTitles:nil, nil];
        [alert show];
        }
            
        }
        
        
        else
        [self callSignInAPI];
    }
    
    
    
}

-(void)callSignInAPI{
    [self showHUDOnView];
    
    SignInService *signInService = [[SignInService alloc]init];
    [signInService callSignInService:self.criteria callback:^(NSError *error, SignInResponse *objSignInResponse) {
    if(error)return ;
        
    else{
    [self hideHUDOnView];
            
    if([objSignInResponse.status isEqualToString:@"success"]){
    [Constant saveUserPassword:passwordTxtField.text];
    [Constant saveUserCoinBalance:objSignInResponse.userLoyaltyPoints];
    [Constant saveUserWalletBalance:objSignInResponse.userWalletBalance];
        
        
    AppDelegate *appDelegate =  (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [appDelegate loginDone];
                
    }
    else{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Pay1" message:objSignInResponse.signinDesc delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                
    [alert show];
    }
    }
    }];
}

-(SignInCriteria *)criteria{
    SignInCriteria *objCriteria = [[SignInCriteria alloc]init];
    objCriteria.userMobNum = [Constant getUserPhoneNumber];
    objCriteria.password = passwordTxtField.text;
    
    
     NSString *strUDID = [[UIDevice currentDevice] uniqueDeviceIdentifier];
     NSLog(@"value is------>%@",strUDID);
     objCriteria.udid = strUDID;
     
     CLLocation *location = [CurrentLocation sharedLocation].currentLocation;
     objCriteria.userLat = [NSString stringWithFormat:@"%f",location.coordinate.latitude];
     objCriteria.userLong = [NSString stringWithFormat:@"%f",location.coordinate.longitude];
     
     NSLog(@"user lat and long :%@ %@ %@ %@",objCriteria.userLat,objCriteria.userLong,objCriteria.userMobNum,objCriteria.password);
     objCriteria.deviceType = @"iOS";
    
    return objCriteria;
}


- (void) prepareForSegue: (UIStoryboardSegue *) segue sender: (id) sender
{
    NSLog(@"#TheSegueisCalled");
}

-(IBAction)forgotPasswordClicked:(id)sender{
    [self callForgotPasswordServiceBlock:TRUE];
}

-(IBAction)resendSMSClicked:(id)sender{
    
    passwordTxtField.text = nil;
    
    resendButton.hidden = TRUE;
    
    [timer invalidate];
    timer = nil;
    
    timeDecrementLabel.hidden = FALSE;
    
    
    passwordTxtLabel.text = @"LOOKS LIKE THERE IS SOME PROBLEM IN SENDING SMS, TRY RE-SENDING IT.";
    
    resendSMSCount ++;
    currMinute = 03;
    currSeconds = 00;

    timer=[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerFired) userInfo:nil repeats:YES];

    [self callForgotPasswordServiceBlock:FALSE];
 
}

-(void)callForgotPasswordServiceBlock:(BOOL)isMove{
    [self showHUDOnView];

    UpdatePasswordService *objService = [[UpdatePasswordService alloc]init];
    [objService callForgotPasswordService:[Constant getUserPhoneNumber] vtype:@"1" callback:^(NSError *error, UpdatePasswordResponse *objForgotPswdRespons) {
    if(error)return ;
    else{
    [self hideHUDOnView];
    _userOTP = objForgotPswdRespons.messageOTP;
        
    if(isMove == TRUE){
    UpdatePasswordViewController *frgtPswdObj = [self.storyboard instantiateViewControllerWithIdentifier:@"updatePasswordViewC"];
    frgtPswdObj.isUserLoggedIn = FALSE;
    frgtPswdObj.pswdOTP = objForgotPswdRespons.messageOTP;
    [self.navigationController pushViewController:frgtPswdObj animated:YES];
    }
        
    }
    }];
    
}

-(IBAction)changeNumberClicked:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}



-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    // _scrollView.frame = CGRectMake(0, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height );
    _scrollView.frame = CGRectMake(0, 20, self.view.frame.size.width, self.view.frame.size.height);
    return TRUE;
}

-(void) keyboardWillShow:(NSNotification *)note{
    NSDictionary* info = [note userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [UIView animateWithDuration:0.2f animations:^{
        
    CGRect frame = passwordImgView.frame;
    frame.origin.y -= kbSize.height;
    passwordImgView.frame = frame;
        
    frame = passwordImgView.frame;
    frame.size.height -= kbSize.height;
    passwordImgView.frame = frame;
    }];
    
}

-(void) keyboardWillHide:(NSNotification *)note{
    NSDictionary* info = [note userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [UIView animateWithDuration:0.2f animations:^{
        
        CGRect frame = passwordImgView.frame;
        frame.origin.y += kbSize.height;
        passwordImgView.frame = frame;
        
        frame = _scrollView.frame;
        frame.size.height += kbSize.height;
        _scrollView.frame = frame;
    }];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    // _scrollView.frame = CGRectMake(0, self.view.frame.origin.y  - 120, self.view.frame.size.width, self.view.frame.size.height );
    
    // _scrollView.frame = CGRectMake(0, backGroundImgView.frame.origin.y  - 120, self.view.frame.size.width, self.view.frame.size.height );
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if(numberErrorTxtView.hidden == FALSE && textField.text.length > 0){
    thinImageView.backgroundColor = [UIColor lightGrayColor];
    [numberErrorTxtView removeFromSuperview];
    [numberErrorImgView removeFromSuperview];
    }
    
    return TRUE;
}

-(void)removeAllErrorViewFromSuperView{
    if([UIScreen mainScreen].bounds.size.height == 480){
    numberErrorImgView.frame = CGRectMake(260,290, 20, 20);
    numberErrorTxtView.frame = CGRectMake(60, numberErrorImgView.frame.origin.y + numberErrorImgView.frame.size.height + 20, self.view.frame.size.width - 120, 35);
    }
    
    if([UIScreen mainScreen].bounds.size.height == 568){
    numberErrorImgView.frame = CGRectMake(260,320, 20, 20);
    numberErrorTxtView.frame = CGRectMake(60, numberErrorImgView.frame.origin.y + numberErrorImgView.frame.size.height + 20, self.view.frame.size.width - 120, 35);
    }
    
    
    [self setUIForErrorViews:numberErrorTxtView];
    [numberErrorImgView removeFromSuperview];
    [numberErrorTxtView removeFromSuperview];
}
-(void)showValidationAlertView:(UIView *)errorAlertView{
    thinImageView.backgroundColor = [UIColor redColor];
    
    numberErrorImgView.frame = CGRectMake(self.view.frame.size.width - 40, thinImageView.frame.origin.y - 35, 25, 25);
    numberErrorTxtView.frame = CGRectMake(thinImageView.frame.size.width - 150, thinImageView.frame.origin.y + thinImageView.frame.size.height, 180, 30);
    errorAlertView.transform = CGAffineTransformMakeScale(0.01, 0.01);
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
    errorAlertView.transform = CGAffineTransformMakeScale(1.2, 1.2);
    [errorAlertView setAlpha:1.0];
        
    [self.scrollView addSubview:errorAlertView];
    } completion:^(BOOL finished){
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
