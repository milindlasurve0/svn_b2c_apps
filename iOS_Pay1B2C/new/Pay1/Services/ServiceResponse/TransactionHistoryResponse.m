//
//  TransactionHistoryResponse.m
//  Pay1
//
//  Created by webninjaz on 09/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "TransactionHistoryResponse.h"

@implementation TransactionHistoryResponse

-(id)init {
    self = [super init];
    if(self) {
        self.transactionHistoryResponse = [[NSMutableArray alloc] init] ;
    }
    return self;
}

@end
