//
//  WalletDetailViewController.h
//  Pay1
//
//  Created by Annapurna on 13/07/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "BaseViewController.h"

@interface WalletDetailViewController : BaseViewController{
    IBOutlet UILabel *walletTextLabel;
    
    IBOutlet UIImageView *locateShopImageView;
    IBOutlet UIImageView *cardPaymentImageView;
    IBOutlet UIImageView *redeemCouponImageView;
    
    IBOutlet UIScrollView *scrollView;
    
}

@end
