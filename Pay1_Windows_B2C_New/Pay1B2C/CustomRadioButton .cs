﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Pay1B2C
    {
    public class CustomRadioButton: RadioButton
        {
        String offbordercolor, onbordercolor, offbackground, onbackground, onforground, offforground, hint, fontfamilyvalue = null;
        int fontsize = 25;
        int thickness, fontweightvalue;

        private string textvalue = string.Empty;
        public string D_Text
            {
            get
                {
                return textvalue;
                }
            set
                {
                textvalue = value;
                Default_Text();
                }
            }
        public CustomRadioButton(String offbordercolor_, String onbordercolor_, String offbackground_, String onbackground_,
            String offforground_, String onforground_, int fontsize_, String fontfamilyvalue_, int fontweightvalue_, int thickness_, String hint_)
            {
            this.offbordercolor = offbordercolor_;
            this.onbordercolor = onbordercolor_;
            this.offbackground = offbackground_;
            this.onbackground = onbackground_;
            this.offforground = offforground_;
            this.onforground = onforground_;

            this.fontsize = fontsize_;
            this.fontfamilyvalue = fontfamilyvalue_;
            this.fontweightvalue = fontweightvalue_;

            this.thickness = thickness_;
            this.BorderThickness = new Thickness(thickness);

            this.hint=hint_;

            this.BorderBrush = new SolidColorBrush(HexColor(offbordercolor));
            this.Background = new SolidColorBrush(HexColor(offbackground));
            this.Foreground = new SolidColorBrush(HexColor(offforground));

            this.FontSize = fontsize_;
            this.FontFamily = new FontFamily(fontfamilyvalue);
            this.FontWeight = getFontWeightValue(fontweightvalue_);

            this.HorizontalAlignment = HorizontalAlignment.Left;

            this.VerticalAlignment = VerticalAlignment.Top;

            }
        private void Default_Text()
            {



            }
        protected override void OnGotFocus(RoutedEventArgs e)
            {


            this.BorderBrush = new SolidColorBrush(HexColor(onbordercolor));
            this.Background = new SolidColorBrush(HexColor(onbackground));
            this.Foreground = new SolidColorBrush(HexColor(onforground));
            // base.OnGotFocus(e);
            }
        protected override void OnLostFocus(RoutedEventArgs e)
            {

            this.BorderBrush = new SolidColorBrush(HexColor(offbordercolor));
            this.Background = new SolidColorBrush(HexColor(offbackground));
            this.Foreground = new SolidColorBrush(HexColor(offforground));
            //  base.OnLostFocus(e);
            }
        public Color HexColor(String hex)
            {
            //remove the # at the front
            hex = hex.Replace("#", "");

            byte a = 255;
            byte r = 255;
            byte g = 255;
            byte b = 255;

            int start = 0;

            //handle ARGB strings (8 characters long)
            if(hex.Length == 8)
                {
                a = byte.Parse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
                start = 2;
                }

            //convert RGB characters to bytes
            r = byte.Parse(hex.Substring(start, 2), System.Globalization.NumberStyles.HexNumber);
            g = byte.Parse(hex.Substring(start + 2, 2), System.Globalization.NumberStyles.HexNumber);
            b = byte.Parse(hex.Substring(start + 4, 2), System.Globalization.NumberStyles.HexNumber);

            return Color.FromArgb(a, r, g, b);
            }
        public FontWeight getFontWeightValue(int i)
            {
            FontWeight returnedvalue = FontWeights.Light;
            switch(i)
                {
                case 1:
                    returnedvalue = FontWeights.Bold;
                    break;
                case 2:
                    returnedvalue = FontWeights.Normal;
                    break;
                case 3:
                    returnedvalue = FontWeights.Medium;
                    break;
                case 4:
                    returnedvalue = FontWeights.Light;
                    break;


                }
            return returnedvalue;
            }
        }
    }