﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace Pay1B2C.Views
    {
    public partial class History: PhoneApplicationPage
        {
        public History()
            {
            InitializeComponent();
            Constant.laodNavigationBar(TitlePanel, LayoutRoot, canvas, "History");
            GoogleAnalytics.EasyTracker.GetTracker().SendView("History");
            }
        protected override void OnNavigatedTo(NavigationEventArgs e)
            {
            //NavigationService.Navigate(new Uri(string.Format(NavigationService.Source +
            //                            "?Refresh=true&random={0}", Guid.NewGuid())));
            Constant.laodNavigationBar(TitlePanel, LayoutRoot, canvas, "History");
            }
        private void panelLocateRetailer_Tap(object sender, System.Windows.Input.GestureEventArgs e)
            {
            NavigationService.Navigate(new Uri("/Views/TransactionHistory.xaml?", UriKind.RelativeOrAbsolute));
            }

        private void panelOnlinePayment_Tap(object sender, System.Windows.Input.GestureEventArgs e)
            {
            NavigationService.Navigate(new Uri("/Views/WalletHistory.xaml?", UriKind.RelativeOrAbsolute));
            }

        private void panelRedeemCoupon_Tap(object sender, System.Windows.Input.GestureEventArgs e)
            {
            NavigationService.Navigate(new Uri("/Views/CoinHistory.xaml?", UriKind.RelativeOrAbsolute));
            }
        }
    }