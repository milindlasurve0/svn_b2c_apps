﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace NewPay1B2C
    {
   public partial class CouponCode : PhoneApplicationPage
    {
        ProgressBarManager ProgressBarManager;
        public CouponCode()
        {
            InitializeComponent();
			
            Constant.laodNavigationBar(TitlePanel, LayoutRoot, canvas,"Coupon Code");
            GoogleAnalytics.EasyTracker.GetTracker ().SendView ("CouponCode");
            ProgressIndicator prog = new ProgressIndicator();

            ProgressBarManager = new ProgressBarManager();
            SystemTray.SetProgressIndicator(this, prog);
            ProgressBarManager.Hook(prog);
        }

		protected override void OnBackKeyPress (System.ComponentModel.CancelEventArgs e)
			{
			//Do your work here
			base.OnBackKeyPress (e);
			NavigationService.Navigate (new Uri ("/HomePage.xaml?", UriKind.RelativeOrAbsolute));
			while (this.NavigationService.BackStack.Any ())
				{
				this.NavigationService.RemoveBackEntry ();
				}
			}
        private async void taskRedeemCouponCode(string couponCode)
        {
            ProgressBarManager.Show(3, "Please wait...", false, 10);
            var values = new List<KeyValuePair<string, string>>
                        { 
                        new KeyValuePair<string, string>("actiontype","refillwallet"),
                        new KeyValuePair<string, string>("vcode",couponCode),
                        new KeyValuePair<string, string>("api","true"),
                        };

            FormUrlEncodedContent values1 = new FormUrlEncodedContent(values);
            Task<string> response = Constant.postParamsAndgetResponse(Constant.B2C_URL, values1);

            string result = await response;
            if (string.IsNullOrEmpty(result))
            {
                NavigationService.Navigate(new Uri("/NoConnection.xaml?", UriKind.Relative));
            }
            else
            {
                var dict = (JObject)JsonConvert.DeserializeObject(result);
                string status = dict["status"].ToString();
                var description = dict["description"];
                if (status.Equals("success"))
                {
                    Constant.SavePersistent(Constant.SETTINGS_USER_WALLET_BALANCE, description["closing_balance"].ToString());
                    MessageBox.Show("Wallet Updated Successfullly");
                    NavigationService.GoBack();
                }
                else
                {
				Constant.checkForErrorCode (result);// MessageBox.Show (description.ToString ());
                }
            }
            ProgressBarManager.Hide(3);
        }


        private void buttonImage_Click(object sender, RoutedEventArgs e)
        {
            string couponCode = textCoupon.Text;
            if (couponCode.Length != 0)
            {
                taskRedeemCouponCode(couponCode);
            }
            else
            {
                MessageBox.Show("Please enter correct coupon code.");
            }
        }


        private void buttonImage_Click_Cancel(object sender, RoutedEventArgs e)
        {
            NavigationService.GoBack(); 
        }

        private void buttonImage_Click_Back(object sender, RoutedEventArgs e)
        {
            NavigationService.GoBack(); 
        }


    }
}