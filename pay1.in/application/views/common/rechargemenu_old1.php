<?php
//echo "hello";
$userData = getUserDetails();
$isLoggedIn = $this->session->userdata('isLoggedIn');
$data = getOperatorDetails();
$operatorData = json_decode($data, true);
$parameters = $this->uri->uri_to_assoc();
foreach ($operatorData as $operatorkey => $operatorval) {
	if (is_array($operatorval)) {
		foreach ($operatorval as $operatorkey => $operatorvalue) {
			if ($operatorkey == "mobile") {
				$arrayval["Mobile"] = $operatorvalue;
			} elseif ($operatorkey == "data") {
				$arrayval["data"] = $operatorvalue;
			} elseif ($operatorkey == "dth") {
				$arrayval["dth"] = $operatorvalue;
			} elseif ($operatorkey == "postpaid") {
				$arrayval["postpaid"] = $operatorvalue;
			}
		}
	}
}

$operatorarray = array();
$datalimit = array();
$array = array();
foreach ($arrayval["data"] as $key => $val) {
	$operatorarray[$val["opr_code"]] = $val["id"];
	$datalimit[$val["opr_code"]] = array($val["max"], $val["min"], $val["stv"], $val["charges_slab"], $val["product_id"]);
	$array[strtolower(str_replace(" ","-",$val["name"]))] = $val["opr_code"] ;
}

$data = json_encode($operatorarray);
$datalimit = json_encode($datalimit);

$postpaidoperatorarray = array();
$postpaidlimit = array();

foreach ($arrayval["postpaid"] as $key => $val) {
	$postpaidoperatorarray[$val["opr_code"]] = $val["id"];
	$postpaidlimit[$val["opr_code"]] = array($val["max"], $val["min"], $val["stv"], $val["charges_slab"], $val["service_tax_percent"], $val["product_id"]);
	$array[strtolower(str_replace(" ","-",$val["name"]))] = $val["opr_code"] ;
}

$postpaiddata = json_encode($postpaidoperatorarray);
$postpaidlimit = json_encode($postpaidlimit);

$prepaidoperatorarray = array();
$prepaidlimit = array();

foreach ($arrayval["Mobile"] as $key => $val) {
	$prepaidoperatorarray[$val["opr_code"]] = $val["id"];
	$prepaidlimit[$val["opr_code"]] = array($val["max"], $val["min"], $val["stv"], $val["charges_slab"], $val["product_id"]);
	$array[strtolower(str_replace(" ","-",$val["name"]))] = $val["opr_code"] ;
}

$prepaiddata = json_encode($prepaidoperatorarray);
$prepaidlimit = json_encode($prepaidlimit);

$dthoperatorarray = array();
$dthlimit = array();
foreach ($arrayval["dth"] as $key => $val) {
	$dthoperatorarray[$val["opr_code"]] = $val["id"];
	$dthlimit[$val["opr_code"]] = array($val["max"], $val["min"], $val["stv"], $val["charges_slab"], $val["product_id"]);
	$array[strtolower(str_replace(" ","-",$val["name"]))] = $val["opr_code"] ;
}

$array = json_encode($array);

$dthdata = json_encode($dthoperatorarray);
$dthlimit = json_encode($dthlimit);
$getcircle = json_decode(CIRCLE, TRUE);
?>
<?php if(isset($parameters['status']) && ($parameters['status']== 'success')){ ?>
<div class="modal fade" id="ordersuccess"  tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    		<div class="modal-dialog">
      		  <div class="modal-content">
          		  <div class="modal-header order_bottomgreenline">
 					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
           		    <h4 class="modal-title" id="myModalLabel">Success</h4>
           			</div>
            <div class="modal-body">
				 
				<h4 class="modal-title login_font_title" id="myModalLabel" style="color:green;">Transactions Completed Successfully!!</h4>
            <div class="row">
				<table cellspacing="0" cellpadding="0" border="0">
					<tr>
						<td class="order_font col-sm-4 col-md-4 col-xs-12">Mobile Number</td>
						<td class="order_font col-sm-4 col-md-6 col-xs-8">Amount</td>
						<td class="order_font col-sm-6 col-md-6 col-xs-12">Wallet Balance</td>
					</tr>
					<tr>
						<td class="order_font col-sm-4 col-md-4 col-xs-12" id="trans_mobile"></td>
						<td class="order_font col-sm-4 col-md-6 col-xs-8" id="transaction_amount"></td>
						<td class="order_font col-sm-6 col-md-6 col-xs-12" id="trans_balance"></td>
					</tr>
					<tr>
						<td colspan="3">&nbsp;</td>
					</tr>
					<tr>
						<td class="order_font col-sm-4 col-md-4 col-xs-12">Service Provider</td>
						<td class="order_font col-sm-4 col-md-6 col-xs-8" colspan="2">Transaction Id</td>
						
					</tr>
					<tr>
						<td class="order_font col-sm-4 col-md-4 col-xs-12" id="service_provider"></td>
						<td class="order_font col-sm-4 col-md-6 col-xs-8" colspan="2" id="trans_id"></td>
						
					</tr>
				</table>
             </div>
        
				<!--row ends-->
              <div class="modal-header order_bottomgreenline">
             <h4>If Your Transaction does not succeed</h4>         
             </div>
               <div class="row pay_via">
             	<div class="col-md-6 col-sm-6 col-xs-12">                             
                <div class="row">                
                <div class="col-md-4 col-sm-4 col-xs-12">
                <img src="/images/offer_callic.png" height="68" width="68">
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">         
                <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">Call Us</div>
                <div class="col-md-12 col-sm-12 col-xs-12"><h4 class="h4bold" id="balance"><?php echo ($userData["walletbal"]>0)?$userData["walletbal"]:0 ?></h4></div>                                          
                </div>
                </div>
                 
                </div>
                </div>
                
                <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="row">

                <div class="col-md-4 col-sm-4 col-xs-12"><img src="/images/ic1.png" height="68" width="68"></div>
                <div class="col-md-6 col-sm-6 col-xs-12">Ask Us <br/> <b>listen@pay1.in</b></div>              
                </div>
                 
                </div>
<!--				   <div class="col-md-6 col-sm-6 col-xs-12">
					   <div class="row">
						   <div class="col-md-6 col-sm-6 col-xs-12"><button class="btn btn-lg btn-primary btn-block" type="button">check out our deals</button></div>              
					   </div>
                 
                </div>-->
             </div>
             </div>
              </div>
			
			</div>
		    </div>
<?php } else if(isset($parameters['status']) && ($parameters['status']== 'error')){ ?>

<div class="modal fade" id="ordererror" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    		<div class="modal-dialog">
      		  <div class="modal-content">
          		  <div class="modal-header order_bottomgreenline">
 					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel" style="color:red;">Failed!!!!</h4>
           			</div>
            <div class="modal-body">
				 
				<h4 class="modal-title login_font_title" id="myModalLabel" style="color:red;">Transactions failed!!</h4>
            <div class="row">
				<table cellspacing="0" cellpadding="0" border="0">
					<tr>
						<td class="order_font col-sm-4 col-md-4 col-xs-12">Mobile Number</td>
						<td class="order_font col-sm-4 col-md-6 col-xs-8">Amount</td>
						<td class="order_font col-sm-6 col-md-6 col-xs-12">Wallet Balance</td>
					</tr>
					<tr>
						<td class="order_font col-sm-4 col-md-4 col-xs-12" id="trans_mobile"></td>
						<td class="order_font col-sm-4 col-md-6 col-xs-8" id="transaction_amount"></td>
						<td class="order_font col-sm-6 col-md-6 col-xs-12" id="trans_balance"></td>
					</tr>
					<tr>
						<td colspan="3">&nbsp;</td>
					</tr>
					<tr>
						<td class="order_font col-sm-4 col-md-4 col-xs-12">Service Provider</td>
						<td class="order_font col-sm-4 col-md-6 col-xs-8" colspan="2">Transaction Id</td>
						
					</tr>
					<tr>
						<td class="order_font col-sm-4 col-md-4 col-xs-12" id="service_provider"></td>
						<td class="order_font col-sm-4 col-md-6 col-xs-8" colspan="2" id="trans_id"></td>
						
					</tr>
				</table>
             </div>
        
				<!--row ends-->
              <div class="modal-header order_bottomgreenline">
             <h4>If Your Transaction does not succeed</h4>         
             </div>
               <div class="row pay_via">
             	<div class="col-md-6 col-sm-6 col-xs-12">                             
                <div class="row">                
                <div class="col-md-4 col-sm-4 col-xs-12">
                <img src="/images/offer_callic.png" height="68" width="68">
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">         
                <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">Call Us</div>
                <div class="col-md-12 col-sm-12 col-xs-12"><h4 class="h4bold" id="balance"><?php echo ($userData["walletbal"]>0)?$userData["walletbal"]:0 ?></h4></div>                                          
                </div>
                </div>
                 
                </div>
                </div>
                
                <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="row">

                <div class="col-md-4 col-sm-4 col-xs-12"><img src="/images/ic1.png" height="68" width="68"></div>
                <div class="col-md-6 col-sm-6 col-xs-12">Ask Us <br/> <b>listen@pay1.in</b></div>              
                </div>
                 
                </div>

             </div>
             </div>
              </div>
			
			</div>
		    </div>
<?php } ?>

				
<div id="transaction_form">
	
</div>
<div class="modal fade" id="basicModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    		<div class="modal-dialog">
      		  <div class="modal-content">
          		  <div class="modal-header">
 					 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
           				 <h4 class="modal-title" id="loginmsg" align="center"></h4>
           			 </div>
            <div class="modal-body">
            <h4 class="modal-title login_font_title" id="myModalLabel">LOGIN WITH PAY1</h4>
            <div class="row">
            
            <div class="col-sm-6 col-md-6 col-xs-12 bottomfb">
    		<div class="rightborder">
            <form method="post" id="login_modal" action = ''>       
         	 <div class="login_font">Login using mobile number
			 </div>
				 <div class="form-group">
					 <input type="text" class="form-control"  id ="mob-number"  name="mob-number" type="text" placeholder="Enter Username" autocomplete="off" maxlength="10">
				 </div>
				<div class="form-group">
         	 <input type="password" class="form-control" name="password" id="password"  placeholder="Enter Password" />      
				</div>
         	 <button class="btn btn-lg btn-primary btn-block"  type="submit">Login</button>   
             </form>
             </div>
 			 </div>
           <div class="col-sm-6 col-md-6 col-xs-12 topspbtn">
            <div class="login_fb">
            <a class="btn btn-block btn-social btn-facebook">
            <i class="fa fa-facebook"></i> Sign in with facebook
              </a>
              </div>
              
              <div class="login_fb"><a class="btn btn-block btn-social btn-google-plus">
                <i class="fa fa-facebook"></i> Sign in with Google+
              </a>
              </div>
			    
 			             <button class="btn btn-lg btn-primary btn-block topsignup" type="submit">Sign up</button>   
              </div>
 			 
             </div>
            </div> 
				</div>
				</div>
			    </div>
<div class="modal fade" id="basicModal1" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    		<div class="modal-dialog">
      		  <div class="modal-content">
          		  <div class="modal-header">
 					 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                     <h4 class="modal-title modal-title signup_font_title" id="myModalLabel" align="center">SIGNUP WITH PAY1</h4>
           			 </div>
            <div class="modal-body">
            <h4 class="" id="myModalLabel"></h4>
            <div class="row">
            
            <div class="col-sm-6 col-md-6 col-xs-12 bottomfb">
    		<div class="rightborder">
            <form> 
				<span id ="sign_msg" style="color:red;"></span>
         	 <div class="login_font">Signup using mobile number</div>
			  
                 <div class="input-group mobno">
                 <div class="input-group-addon">+91</div>
                  <input class="form-control" onkeypress="return isNumberKey(event)" maxlength="10" type="text" id="mobnumber" name="mobnumber">
                </div>
			 
			  <div class="input-group otp" style="display: none;">
                  <input class="form-control" type="password" maxlength="10" type="text" id="otp" name="otp">
            </div>
			 <br/>
         	 <button class="btn btn-lg btn-primary btn-block" type="button" id="register_user">Create an account</button>
			 <button class="btn btn-lg btn-primary btn-block" id="check_otp" style="display: none;" type="button">Submit</button>
             <p class="agree_txt">By Signing Up, you agree to our <span class="agree_bluetxt">T&C </span> and that
             you have read our <span class="agree_bluetxt">Privacy policy</span></p>
             </form>
             </div>
 			 </div>
             
           <div class="col-sm-6 col-md-6 col-xs-12 topspbtn">
           
            <div class="login_fb">
            <a class="btn btn-block btn-social btn-facebook">
            <i class="fa fa-facebook"></i> Sign in with facebook
              </a>
              </div>
			   <br/>
              
              <div class="login_fb"><a class="btn btn-block btn-social btn-google-plus">
                <i class="fa fa-facebook"></i> Sign in with Google+
              </a>
              </div>
 			
              </div>
 			 
             </div>
				<!--row ends-->
            </div> 
    </div>
  </div>
</div>


         
<div class="row collapse" id="rechargemenu" >
    <div class="col-md-12">


        <div class="tabbable">
            <div class="tabbable-line">
                <ul class="nav nav-tabs ">
                    <li class="active mob">
                        <a href="#prepaid-mobile" data-toggle="tab">
                           Prepaid Mobile
                        </a>
                    </li>
                    <li class="post">
                        <a href="#postpaid-mobile" data-toggle="tab">
                           Postpaid Mobile </a>
                    </li>
                    <li class="dth">
                        <a href="#dth" data-toggle="tab">
                          DTH </a>
                    </li>
					<li class="data">
						<a href="#data" data-toggle ="tab">Data</a>
					</li>
					<li>
						<a href="#quickpay" data-toggle ="tab" id="getQuickPaylist">Quick Pay</a>
					</li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="prepaid-mobile">
						
						<div class="col-md-4 col-sm-4 col-xs-12 mob" id="mobile">
			         <form method="post" id="mob_form" action="">
                        <input type="hidden" id="mob_flag" name="mob_flag" value="1">
                        <input type="hidden" id="mob_circle" name="mob_circle" value="">
                        <input type="hidden" id="mob_max" name="mob_max" value="">
                        <input type="hidden" id="mob_min" name="mob_min" value="">
                         <input type="hidden" id="mob_id" name="mob_id" value="">
                          <input type="hidden" id="mob_product" name="mob_product" value="">

						  <h4 class="recharge_header">Recharge Your Prepaid Mobile</h4>
            <div class="form-group">           
                <div class="form-group" style="width:280px;"><input type="text" size="30" class="form-control" placeholder="Enter your mobile no" name="mob_number" value="" id="mob_number" maxlength="10" onkeypress="getnumberDetails(this.value,'mob_provider','mob_amount');return isNumberKey(event)" onchange="getnumberDetails(this.value,'mob_provider','mob_amount');return isNumberKey(event)"></div>           
            </div>
            <br/>
            <div class="form-group">
            <div class="form-group" style="width:280px;"> 
            <select class="form-control" name="mob_provider" id="mob_provider" onchange="getplanoperatorchange('mob')">
                <option value="">Select Service Provider</option>
                                                <?php foreach($arrayval["Mobile"] as $key => $val){?>
                                                <option value="<?php echo $val["opr_code"] ?>"><?php echo $val["name"] ?></option>
                                                <?php } ?>
            </select></div>
            </div>
            <br/>
            <div class="form-group">            
            <div class="form-group" style="width:280px;"><input type="text" class="form-control" placeholder="Enter Amount" value="" name="mob_amount" id="mob_amount"  onkeypress="return isNumberKey(event)"></div>                 
            </div>
            <br/>
            <div class="form-group">           
            <div class="form-group">
				<button class="btn btn-success orderinfo" style="width:280px;" value="mob"  data-toggle="modal" type="button" data-target=".orderconfirmation">Continue to Payment
				</button> 
			</div>
             </div>
			   </form>
             
             </div>
				<div class="col-md-8 col-sm-8 col-xs-12 mob" style="display: none;">
					<div class="checkplans" id="showmobplan" >
                                              <div class="sliderowbar">
						<ul class="nav nav-tabs" role="tablist" id ="mob_plan">
							
						</ul>
                                              </div>
                                            <table class="form-control" id="mobile_circle"  style="display:none;width:873px;height:50px;">
                                                
                                                <tr width="670">
<!--                                                    <th>Select Circle</th>-->
                                                    
                                                    <td><select class="form-control" name="mob_cir" id="mob_cir" onchange="getplanoperatorchange('mob')">
						<option value="">Select Circle</option>
                                                <?php foreach($getcircle as $key => $val){ if($val["code"]!="SC"){?>
                                                <option value="<?php echo $val["code"] ?>"><?php echo $val["name"] ?></option>
                                                <?php }} ?>
						
					</select></td>
                                                </tr>
                                            </table>

						<!-- Tab panes -->
						<div class="tab-content mT20"  id ="mob_plandetails" style="">
    
						</div>
						 <div class="text-right">
                         <a href="javascript:void(0);" class="hidecheckplans"  title="Close">Close &nbsp;</a>
			             </div>	
					</div>
				
                   </div>

                    </div>
                    <div class="tab-pane" id="postpaid-mobile">
						<div class="col-md-4 col-sm-4 col-xs-12 mob">
                        <form method="post" id="post_form" action="login.php">  
                        <input type="hidden" name="post_flag" id="post_flag" value="4">
                         <input type="hidden" name="post_id" id="post_id" value="">
                         <input type="hidden" name="post_min" id="post_min" value="">
                         <input type="hidden" name="post_max" id="post_max" value="">
                         <input type="hidden" name="post_charge" id="post_charge" value="">
                         <input type="hidden" name="post_servicecharge" id="post_servicecharge" value="">

            <h4 class="recharge_header">Recharge Your Postpaid Mobile</h4>
            <div class="form-group">           
                <div class="form-group" style="width:280px;"><input type="text" size="30" class="form-control" placeholder="Enter  PostPaid Number" value="" maxlength="10"  id="post_number" name="post_number" onkeypress="getdataoperatorvalue(this.value,'post_provider','post_amount');return isNumberKey(event)"></div>           
            </div>
            <br/>
            <div class="form-group">
            <div class="form-group" style="width:280px;"> 
            <select class="form-control" name="post_provider" id="post_provider" onchange="getplanoperatorchange('post');">
               	<option value="">Select Service Provider</option>
                                                <?php foreach($arrayval["postpaid"] as $key => $val){?>
                                                <option value="<?php echo $val["opr_code"] ?>"><?php echo $val["name"] ?></option>
                                                <?php } ?>
            </select></div>
            </div>
            <br/>
            <div class="form-group">            
            <div class="form-group" style="width:280px;"><input type="text"   value="" class="form-control" name="post_amount" id="post_amount" onkeypress="return isNumberKey(event);" placeholder="Enter Amount"></div>                 
            </div>
            <br/>
            <div class="form-group">           
            <div class="form-group"><button class="btn btn-success orderinfo" value="post" type="button" data-toggle="modal" data-target=".orderconfirmation" class="btn btn-primary btn-lg btn-block" style="width:280px;">Continue to Payment</button>  </div>
             </div>
					</form>
                    </div>
					</div>
               <div class="tab-pane" id="dth">
				<div class="col-md-4 col-sm-4 col-xs-12">
               <form method="post" id="dth_form" action="login.php">
               <input type="hidden" name="dth_flag" id="dth_flag" value="2">
               <input type="hidden" name="dth_max" id="dth_max" value="">
               <input type="hidden" name="dth_min" id="dth_min" value="">
               <input type="hidden" name="dth_id" id="dth_id" value="">
                <input type="hidden" name="dth_product" id="dth_product" value="">
                <input type="hidden" name="dth_circle" id="dth_circle" value="">

				<h4 class="recharge_header">Recharge Your DTH</h4>
				<div class="form-group">
					<div class="form-group" style="width:280px;"> 
						<select class="form-control" name="dth_provider" id="dth_provider" onchange="getplanoperatorchange('dth');">
							<option value="">Select Operator</option>
							<?php foreach ($arrayval["dth"] as $key => $val) { ?>
								<option value="<?php echo $val["opr_code"] ?>" ><?php echo $val["name"] ?></option>
							<?php } ?>
						</select></div>
				</div>
				<br/>
				<div class="form-group">           
					<div class="form-group" style="width:280px;"><input type="text" size="30" class="form-control" value="" onkeypress="return isNumberKey(event)" name="dth_number" id="dth_number"></div>           
				</div>


				<br/>
				<div class="form-group">            
					<div class="form-group" style="width:280px;"><input type="text" class="form-control" placeholder="Enter Amount" name="dth_amount" placeholder="Enter Amount" id="dth_amount" value="" onkeypress="return isNumberKey(event)" class="form-control"></div>                 
				</div>
				<br/>
				<div class="form-group">           
					<div class="form-group"><button class="btn btn-success orderinfo" type="button" value="dth" data-toggle="modal"  data-target=".orderconfirmation"  style="width:280px;">Continue to Payment</button>  </div>
				</div>
			   </form>
               </div>
			   <div class="col-md-8 col-sm-8 col-xs-12 dth" style="display:none;">
				   <div class="checkplans" id="showdthplan">
					   <div class="sliderowbar">
						   <ul class="nav nav-tabs" id="dth_plan" role="tablist">

						   </ul>
					   </div>
					   <div class="tab-content mT20" id="dth_plandetails">

					   </div>

					   <div class="text-right">
						   <a href="javascript:void(0);" class="hidecheckplans"  title="Close">Close &nbsp;</a>
					   </div>						
				   </div>

                    
			   </div>
			   </div>
					
			   <div id="data" class="tab-pane">
				   <div class="col-md-4 col-sm-4 col-xs-12">
				   <form method="post" id="data_form" action="login.php">
                    <input type="hidden" name="data_id" id="data_id" value="">
                    <input type="hidden" name="data_max" id="data_max" value="">
                    <input type="hidden" name="data_min" id="data_min" value="">
                    <input type="hidden" id="data_flag" name="data_flag" value="3">
                    <input type="hidden" id="data_product" name="data_product" value="">
                    <input type="hidden" id="data_circle" name="data_circle" value="">
					<h4 class="recharge_header">Recharge Your Data card</h4>
					<div class="form-group">           
						<div class="form-group" style="width:280px;"><input type="text" size="30" value="" name="data_number" id="data_number" onkeypress="getdataoperatorvalue(this.value,'data_provider','data_amount')" class="form-control input" placeholder="Enter your mobile no"></div>           
					</div>
					<br/>
				<div class="form-group">
				<div class="form-group" style="width:280px;"> 
				<select class="form-control" id="data_provider" name="data_provider" onchange="getplanoperatorchange('data')">
					<option value="">Select Service Provider</option>
					<?php foreach ($arrayval["data"] as $key => $val) { ?>
						<option value="<?php echo $val["opr_code"] ?>"><?php echo $val["name"] ?></option>
					<?php } ?>
				</select></div>
				</div>
				<br/>
				<div class="form-group">            
				<div class="form-group" style="width:280px;"><input type="text" name="data_amount"  value=""  id="data_amount" class="form-control"  onkeypress="return isNumberKey(event)"  placeholder="Enter Amount"></div>                 
				</div>
				<br/>
				<div class="form-group">           
				<div class="form-group"><button class="btn btn-success orderinfo" type="button" data-toggle="modal" value="data"  data-target=".orderconfirmation"  style="width:280px;">Continue to Payment</button>  </div>
				 </div>
				   </form>
             
             </div>
				   <div class="col-md-8 col-sm-8 col-xs-12 data" style="display:none;">
				   <div class="checkplans" id="showdataplan">
                                            <div class="sliderowbar">
						<ul class="nav nav-tabs"  id ="data_plan" role="tablist">
						  
						</ul>
                                            </div>

                                             <table class="form-control" id="dat_circle"  style="display:none;width:873px;height:50px;">
                                                
                                                <tr>
<!--                                                    <th style="border:none;">Select Circle</th>-->
                                                    <td style="border:none;"><select class="form-control" name="data_cir" id="data_cir" onchange="getplanoperatorchange('data')">
						<option value="">Select Circle</option>
                                                <?php foreach($getcircle as $key => $val){ if($val["code"]!="SC"){?>
                                                <option value="<?php echo $val["code"] ?>"><?php echo $val["name"] ?></option>
                                                <?php }} ?>
						
					</select></th>
                                                </tr>
                                            </table>

						<!-- Tab panes -->
						<div class="tab-content mT20" id="data_plandetails">
						  
						</div>
						<!-- close -->
						<div class="text-right">
							<a href="javascript:void(0)" class="hidecheckplans" title="Close">Close &nbsp;</a>
						</div>						
					</div>
			</div>
					</div>
					<div id="quickpay" class="tab-pane">
                        <div  class="row">
                            <div class="col-lg-8">
                                <h3 class="quickpayheading">Recharge your mobile instantly</h3>
								<h4 id="quickmsg" style="color:red;" align="center"></h4>
                            </div>
                        </div>
                                                            <div class="row">
                                                                <div class="col-lg-8 pull-left" id="quickpaydata">
				
                                                                    <div class="col-lg-6">
                                                                    <table class="table table-bordered table-striped quickpaytable">
                                                                            <tr>
                                                                                <td rowspan="2" class="quickpayno"><div class="">&#8377; 250</div></td>
                                                                            <td >Vodafone Postpaid</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><div align="center" class="pull-left">9325684613</div></td>
                                                                            </tr>
                                                                     </table>
                                                                    </div>
                                                                    
                                                                    <div class="col-lg-6">     
                                                                    <table class="table table-bordered table-striped quickpaytable">
                                                                            <tr>
                                                                                <td rowspan="2" class="quickpayno"><div class="">&#8377; 250</div></td>
                                                                            <td>Vodafone Postpaid</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><div align="center" class="pull-left">9325684613</div></td>
                                                                            </tr>
                                                                     </table>
                                                                    </div>
                                                                </div>
                                                            </div>
					</div>

                    </div>
		<div class="" id="orderconfirmation" style="display:none;" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    		<div class="modal-dialog">
				<form method="post" action="" id="payment_form">
      		  <div class="modal-content">
          		  <div class="modal-header order_bottomgreenline">
 					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
           		    <h4 class="modal-title" id="myModalLabel">Order Confirmation</h4>
           			</div>
            <div class="modal-body">
				    <input type="hidden" name="rec_amount" id="rec_amount" value="">
                    <input type="hidden" name="rec_operator" id="rec_operator" value="">
                    <input type="hidden" name="rec_flag" id="rec_flag" value="">
                    <input type ="hidden" name="rec_number" id="rec_number" value="">
                    <input type ="hidden" name="payment_option" value="" id="payment_option">
                    <input type="hidden" name="service_charge" id ="service_charge" value="">
                    <input type="hidden" name="service_charge_percent" id ="service_charge_percent" value="">
                    <input type="hidden" name="stv" id ="stv" value="">
                    <input type="hidden" name="total_amount" id ="total_amount" value="">
					<input type="hidden" name="walletbal" id="walletbal" value="<?php echo $userData['walletbal']; ?>">
					<input type="hidden" name="isloggedin" id="isloggedin" value="<?php echo $isLoggedIn; ?>">
					<input type="hidden" name="max_amount" id ="max_amount" value="">
                    <input type="hidden" name="min_amount" id ="min_amount" value="">
					<input type="hidden" name="charge_slab" id ="charge_slab" value="">
            <h4 class="modal-title login_font_title" id="myModalLabel"></h4>
            <div class="row">
            
            <div class="col-sm-6 col-md-6 col-xs-12 ">      		
                   <div class="row order_left">
                        
                 <div class="order_font">Mobile number</div>
                <h4 class="h4bold" id ="number"></h4>         
              <div class="service_provider">Service Provider</div>
                <h4 class="h4bold" id="provider_name"></h4>    
                </div>     	          
             </div>  
 		             
           <div class="col-sm-6 col-md-6 col-xs-12">  
                <div class="order_font">Amount</div>
                <h4 class="h4bold" id ="trans_amount"></h4>
				<p id="quickpay_amount" style="display:none;"><input type="text" name="quickpay_amount" onkeypress="return isNumberKey(event);" id="postpaid_amount"><input type="button" onclick="validateamount();" id="validatequickpay"  class="common-btn-submit" value="submit"></p>
				<div class="blue_ordertxt" id="service_msg" style="display: none;"></div>  
              </div>
             </div>
        
				<!--row ends-->
         
                  <div class="modal-header order_bottomgreenline">
             <h4>Pay now via</h4>         
             </div>
               <div class="row pay_via">
             	<div class="col-md-6 col-sm-6 col-xs-12">                             
                <div class="row">                
                <div class="col-md-4 col-sm-4 col-xs-12">
                <img src="/images/wallet.svg" height="68" width="68">
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">         
                <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">Balance</div>
                <div class="col-md-12 col-sm-12 col-xs-12"><h4 class="h4bold" id="balance"><?php echo ($userData["walletbal"]>0)?$userData["walletbal"]:0 ?></h4></div>                                          
                </div>
                </div>
                 <div class="col-md-10 col-sm-10 col-xs-12 btn_via">
					 <input type="button" class="btn btn-lg btn-success btn-block payment"  value="Pay with wallet" id="wallet">
					 <span style="color:red;" id="walletmsg"></span>
                </div>
                </div>
                </div>
                
                <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="row">

                <div class="col-md-4 col-sm-4 col-xs-12"><img src="/images/creditcard.svg" height="68" width="68"></div>
                <div class="col-md-6 col-sm-6 col-xs-12">Pay with your Debit or Credit Card</div>              
                </div>
                 <div class="col-md-10 col-sm-10 col-xs-12 btn_via">
					 <input type="button" class="btn btn-lg btn-success btn-block payment" value="Payment Gateway"></button>
                 
                </div>
                </div>
             </div>
             </div>
              </div>
				</form>
			</div>
		    </div>
					</div>
				</div>
			</div>
	<div class="loader" style="display:none;">
			<center>
			<img class="loading-image" src="/images/bx_loader.gif" alt="loading..">
			</center>
	</div>
    </div>
</div>
<script>
	
  function getnumberDetails(value,opertatortype,amount)
        {
			
            var mobileno = value;
			//alert(mobileno.length);
			var serviceurl ="<?php echo PANEL_URL; ?>/apis/receiveWeb/mindsarray/mindsarray/json?method?";
            var no = '';
            var methodtype = "getMobileDetails";
            var plandetails = "getPlanDetails";
            var spitoperatortype = (opertatortype.split("_"));
            var operatorobj = JSON.parse(<?php echo json_encode($prepaiddata); ?>);
            var limit = JSON.parse(<?php echo json_encode($prepaidlimit); ?>);
            if(mobileno.length < 4){
                $("#mob_plan").html('');
                $("#mob_plandetails").html('');  
            }
			
            if((mobileno.length == 9) || (mobileno.length==10 && amount == '')){
            $.ajax({
					url:serviceurl,
					data:{method :methodtype,mobile: value},
					type: "POST",
					dataType: 'jsonp',
					jsonp: 'root',
					success:function(data) {
					 if(data[0].status=="success") {
					  $("#"+opertatortype).val(data[0].details.operator);
					  $("#"+amount).focus();
					  var operatorid = operatorobj[data[0].details.operator];
					  var productid = limit[data[0].details.operator][4];
					  var circle =  data[0].details.area;
					  $("#mob_cir").val(circle);
					  $("#mob_circle").val(circle);
					  $("#mob_max").val(limit[data[0].details.operator][0]);
					  $("#mob_min").val(limit[data[0].details.operator][1]);
					  $("#mob_id").val(operatorid);
				      $("#mob_product").val(productid);
					  var specialrecharge =limit[data[0].details.operator][2];
					  if(specialrecharge==1){
							$("#spl_rchg").show();
					   }
					   if(circle!=''){
							 $("#mob_checkplan").show();
						}
				      if(mobileno.length == 9){
					  $("#"+amount).focus();
				      }
					  b2c.core.browseplan(plandetails,productid, circle, spitoperatortype[0]);
          }
          else
          {
              $("#"+opertatortype).val('');
          }
        }, error: function (xhr,error) {
              console.log(error);
          },
        });
       }
       
    }
	
	  function getplanoperatorchange(type){
	  
      var operatorid = $("#"+type+"_provider").val();
      var plandetails = "getPlanDetails";
      var number = $("#"+type+"_number").val();
      if(type=="dth"){
          
      if(operatorid=="AD"){
          $("#dth_number").attr("placeholder", "Enter Customer ID").blur();
      }
      else if(operatorid=="BD" || operatorid=="SD" ){
          $("#dth_number").attr("placeholder", "Enter Smart Card Number").blur();
      }
      else if(operatorid=="DD"){
          $("#dth_number").attr("placeholder", "Enter Viewing Card Number").blur();
      }
      
      else if(operatorid=="TD"){
          $("#dth_number").attr("placeholder", "Enter Subscriber ID").blur();
      }
      else if(operatorid=="VD"){
          $("#dth_number").attr("placeholder", "Enter DTH Number").blur();
      }
      
      var circle = "all";
      $("#dth_plan").html('');
      $("#dth_plandetails").html('');
       var limit = JSON.parse(<?php echo json_encode($dthlimit); ?>);
       var operatorobj = JSON.parse(<?php echo json_encode($dthdata); ?>);
	   $("#"+type+"_max").val(limit[operatorid][0]);
       $("#"+type+"_min").val(limit[operatorid][1]);
       $("#"+type+"_id").val(operatorobj[operatorid]);
        $("#"+type+"_product").val(limit[operatorid][4]);
        $("#"+type+"_circle").val(circle);
        if(circle!=''){
        $("#dth_checkplan").show();
        }
        b2c.core.browseplan(plandetails,limit[operatorid][4],circle,type);
      }
       else if(type=="mob"){
            $("#mob_plan").html('');
            $("#mob_plandetails").html('');  
            var circle = $("#mob_cir").val();
            var limit = JSON.parse(<?php echo json_encode($prepaidlimit); ?>);
            var operatorobj = JSON.parse(<?php echo json_encode($prepaiddata); ?>);
            b2c.core.checkcircle(number,plandetails,limit[operatorid][4],circle,type);
            $("#"+type+"_max").val(limit[operatorid][0]);
            $("#"+type+"_min").val(limit[operatorid][1]);
            $("#"+type+"_id").val(operatorobj[operatorid]);
            $("#"+type+"_product").val(limit[operatorid][4]);
             $("#mob_circle").val(circle);
            var specialrecharge =limit[operatorid][2];
            if(specialrecharge==1){
                 $("#spl_rchg").show();
             } else {
                  $("#spl_rchg").hide();
             }
             if(circle!=''){
                  $("#mob_checkplan").show();
             }

       }
       else if(type=="data"){
            $("#data_plan").html('');
            $("#data_plandetails").html('');  
            var circle = $("#data_cir").val();
            var limit = JSON.parse(<?php echo json_encode($datalimit); ?>);
            var prepaidlimit = JSON.parse(<?php echo json_encode($prepaidlimit); ?>);
            var operatorobj = JSON.parse(<?php echo json_encode($data); ?>);
            b2c.core.checkcircle(number,plandetails,prepaidlimit[operatorid][4],circle,type);
            $("#"+type+"_id").val(operatorobj[operatorid]);
            $("#"+type+"_max").val(limit[operatorid][0]);
            $("#"+type+"_min").val(limit[operatorid][1]);
            $("#"+type+"_product").val(prepaidlimit[operatorid][4]);
            $("#data_circle").val(circle);
             if(circle!=''){
                  $("#data_checkplan").show();
             }
     
       }
       else if(type=="post")
       {
            var limit = JSON.parse(<?php echo json_encode($postpaidlimit); ?>);
            var operatorobj = JSON.parse(<?php echo json_encode($postpaiddata); ?>);
            $("#"+type+"_id").val(operatorobj[operatorid]);
            $("#"+type+"_max").val(limit[operatorid][0]);
            $("#"+type+"_min").val(limit[operatorid][1]);
            $("#"+type+"_charge").val(limit[operatorid][3]);
            $("#"+type+"_servicecharge").val(limit[operatorid][4]);
            $("#service_charge_percent").val(limit[operatorid][4]);
       }
       
     }
	
	      function getdataoperatorvalue(value,opertatortype,amount)
           {
				var spitoperatortype = (opertatortype.split("_"));
				if(spitoperatortype[0]=="post"){
					var operatorobj = JSON.parse(<?php echo json_encode($postpaiddata); ?>);
					var limit = JSON.parse(<?php echo json_encode($postpaidlimit); ?>);
				}
				else{
					var operatorobj = JSON.parse(<?php echo json_encode($data); ?>);
					var limit = JSON.parse(<?php echo json_encode($datalimit); ?>);
					var prepaidlimit = JSON.parse(<?php echo json_encode($prepaidlimit); ?>);
					}
					var mobileno = value;
					var methodtype = "getMobileDetails";
					var plandetails = "getPlanDetails";
					var serviceurl = "<?php echo PANEL_URL; ?>/apis/receiveWeb/mindsarray/mindsarray/json?method?";
					if(mobileno.length == 9){
					$.ajax({
							url:serviceurl,
							data:{method :methodtype,mobile: value},
							type: "POST",
							dataType: 'jsonp',
							jsonp: 'root',
							success:function(data)
							{
							  if(data[0].status=="success"){
								var operatorval = data[0].details.operator;
								$("#"+opertatortype).val(operatorval);
								$("#"+spitoperatortype[0]+"_id").val(operatorobj[operatorval]);
								$("#"+spitoperatortype[0]+"_max").val(limit[operatorval][0]);
								$("#"+spitoperatortype[0]+"_min").val(limit[operatorval][1]);
								$("#"+spitoperatortype[0]+"_charge").val(limit[operatorval][3])
								var operatorid = operatorobj[operatorval];
								$("#"+amount).focus();
								var circle =  data[0].details.area;
								$("#data_circle").val(circle);
								$("#data_cir").val(circle);
								if(spitoperatortype[0]!='post'){
								var productid = prepaidlimit[operatorval][4];
								  $("#data_product").val(productid);
								 b2c.core.browseplan(plandetails,productid,circle,spitoperatortype[0]);
							   }
							  }
							  else
							  {
								  $("#"+opertatortype).val('');
							  }
							}
						});
					}
					   }


    $('#login_modal').on('submit', function (event) {
      var mobileNo = $("#mob-number").val();
      var password = $("#password").val();
      if (mobileNo == ''){
        alert("Please Enter mobile no.");
        $("#mob-number").focus();
        return false;
      }
      else if (password == '') {
        alert("Password can not be blank");
        $("#password").focus();
        return false;
      }
      else {
        var url = b2cUrl+"/index.php/api_new/action/actiontype/signin/api/true/?";
        $.ajax({
            url: url,
            type: "GET",
            data: {username: mobileNo,
                   password: password,
                   uuid: "",
                   latitude: "",
                   logitude: "",
                   device_type: "",
				   api_version:"3",
                   res_format: "jsonp"
            },
            timeout: 50000,
            dataType: "jsonp",
            crossDomain: true,
            success: function(data) {
                if (data.status == "failure"){
				    alert(data.description);
                }
                else{
					$("#loginmsg").html("Logged in successfully!!!!");
					localStorage.setItem('passflag',password);
					b2c.core.setUserData(data);
                }
            },
              beforeSend: function(){
              $('.loader').show();
              },
             complete: function(){
             $('.loader').hide();
             },
            error: function(xhr, error) {
				
           },
        });
    }
	return false;

});

   $('.payment').click(function(){
   b2c.core.payment('rec_amount','rec_operator','rec_flag','rec_number',this.value);
   });
   
   $('.orderinfo').click(function(){
	   
	   b2c.core.orderinformation(this.value);
   });
   
   $("#check_otp").click(function(){
	   b2c.core.checkotp();
   });
   
   $("#register_user").click(function(){
	   b2c.core.register();
   });
   
   $("#logout").click(function(){
	   b2c.core.logout();
   });
   
   $("#validatequickpay").click(function(){
	   b2c.core.validateQuickpayamount();
   });
   
   $("#getQuickPaylist").click(function(){
	   b2c.core.getQuickPaylist('1');
   });
   
   
   <?php 
       if(isset($parameters['status']) && ($parameters['status']== 'success')){ ?>
	  var mobnumber = localStorage.getItem('number');
	  var bal = "<?php echo $parameters['balance']; ?>";
	  if(mobnumber!=''){	
	   $("#trans_mobile").html(localStorage.getItem('number'));
	   $("#transaction_amount").html(localStorage.getItem('amount'));
	   $("#trans_balance").html("<?php echo $parameters['balance']; ?>");
	   $("#service_provider").html(localStorage.getItem('provider'));
	   $("#trans_id").html("<?php echo $parameters['txnid']; ?>");
	   var balance = {'wallet_balance' :bal};
	   b2c.core.updateprofile(balance);
       $("#ordersuccess").modal('toggle');
	   localStorage.setItem('number','');
    }
    <?php } else if(isset($parameters['status']) && ($parameters['status']== 'error')){?>
		
		var mobnumber = localStorage.getItem('number'); 
		var bal = "<?php echo $parameters['balance']; ?>";
	  if(mobnumber!=''){	
	   $("#trans_mobile").html(localStorage.getItem('number'));
	   $("#transaction_amount").html(localStorage.getItem('amount'));
	   $("#trans_balance").html("<?php echo $parameters['balance']; ?>");
	   $("#service_provider").html(localStorage.getItem('provider'));
	   $("#trans_id").html("<?php echo $parameters['txnid']; ?>");
	   var balance = {'wallet_balance' :bal};
	   b2c.core.updateprofile(balance);
	   localStorage.setItem('number','');
       $("#ordererror").modal('toggle');
   }
		
	<?php }?>
	function showquickpay(number, name, amount, operator, delegate_flag, confirmation, flag, id, stv, missedcall, operatorname, type, operatorcode){
		$("#orderconfirmation" ).addClass("modal fade orderconfirmation");
		$("#orderconfirmation").show();
        var wallet_bal = $("#walletbal").val();
        if (parseInt(wallet_bal) < parseInt(amount) || (parseInt(wallet_bal) == 0)) {
            $("#wallet").hide();
            $("#walletmsg").html("Insufficient balance in wallet");
        }
        else {
            $("#walletmsg").html("");
            $("#wallet").show();
        }
        if (type == "postpaid"){
            var operatorobj = JSON.parse(<?php echo json_encode($postpaiddata); ?>);
            var limit = JSON.parse(<?php echo json_encode($postpaidlimit); ?>);
            $("#service_charge_percent").val(limit[operatorcode][4]);
            $("#charge_slab").val(limit[operatorcode][3])
            $("#max_amount").val(limit[operatorcode][0]);
            $("#min_amount").val(limit[operatorcode][1]);
            $("#quickpay_amount").show();
            $("#trans_amount").hide();
            $("#postpaid_amount").val(amount);
        }
        else {
              $("#quickpay_amount").hide();
              $("#trans_amount").show();
            }
        $("#number").html(number);
        $("#trans_amount").html(amount);
        $("#provider_name").html(operatorname);
        $("#rec_amount").val(amount);
        $("#rec_number").val(number);
        $("#rec_operator").val(operator);
        $("#rec_flag").val(flag);
    }
	
	$(".hidecheckplans").click(function(){
                $(".checkplans").slideUp();
                $(".opencheckplans").prop('disabled', false);
   });
   
   $(document).ready(function(){
	   var url = document.URL;
	   url = url.split("/");
	   if(url.length == 4){
		   var array = JSON.parse(<?php echo json_encode($array); ?>);
		   url = url[3].split("-");
		   if(url.length==4 && url[2]!='dth'&& url[2]!='postpaid' ){
		   var oprId = url[1];
		   var type = url[2];
		   }
		   else if(url.length==4 && url[2]=='dth' || url[2]=='postpaid'){
			   var oprId = url[1]+"-"+url[2];
		       var type = url[2];
		   }
	        else if(url.length==5 && url[3]!='mobile') {
			   var oprId = url[1]+"-"+url[2]+"-"+url[3];
		       var type = url[3];
		   }
		   else if(url.length==5 && url[3]=='mobile'){
			   var oprId = url[1]+"-"+url[2];
		       var type = url[3];
		   }
		   else {
			    var oprId = url[1]+"-"+url[2]+"-"+url[3];
		        var type = url[4];
		   }
		  
		   if(type=='mobile'){
			  type = 'mob';
		   }  else if(type=='postpaid'){
			  type = 'post';
		   }else {
			    type = type;
		   }
		   
		   $("#"+type+"_provider").val(array[oprId]);
		   if(type=='mob'){
		   $("."+type).addClass('active');
		   $(".dth").removeClass('active');
		   $(".data").removeClass('active');
		   $(".post").removeClass('active');
		   $("#prepaid_mobile").show();
		   $("#dth").hide();
		   $("#data").hide();
		   $("#postpaid-mobile").hide();
		   $("#rechargemenu").toggle(); 
		   } else if(type=='post'){
		   $("#postpaid-mobile").show();
		   $("#dth").hide();
		   $("#data").hide();
		   $("#prepaid-mobile").hide();
		   $("."+type).addClass('active');
		   $(".dth").removeClass('active');
		   $(".data").removeClass('active');
		   $(".mob").removeClass('active');
		   $("#rechargemenu").toggle(); 
		   }
		   else if(type=='data'){
		   $("#data").show();
		   $("#dth").hide();
		   $("#postpaid-mobile").hide();
		   $("#prepaid-mobile").hide();
		   $("."+type).addClass('active');
		   $(".dth").removeClass('active');
		   $(".post").removeClass('active');
		   $(".mob").removeClass('active');
		   $("#rechargemenu").toggle(); 
		   }
		   else if(type=='dth'){
		   $("#postpaid-mobile").hide();
		   $("#dth").show();
		   $("#data").hide();
		   $("#prepaid-mobile").hide();
		   $("."+type).addClass('active');
		   $(".data").removeClass('active');
		   $(".post").removeClass('active');
		   $(".mob").removeClass('active');
		   $("#rechargemenu").toggle(); 
		   }
		   //if(type!='') 
		   if(type!='bill' || type!=''){
		   getplanoperatorchange(type);
	       }
	   
	   }
	   
   });
	
		
</script>
